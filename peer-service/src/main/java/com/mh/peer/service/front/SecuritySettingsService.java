package com.mh.peer.service.front;

import com.mh.peer.model.message.*;

import java.util.List;

/**
 * Created by cuibin on 2016/5/4.
 */
public interface SecuritySettingsService {
    /**
     * 验证旧密码是否存在
     */
    public MemberInfoMessageBean checkOldPassword(String passWord);
    /**
     * 更新密码
     */
    public MemberInfoMessageBean updatePassWord(String passWord);
    /**
     * 保存紧急联系人
     */
    public MemberLinkmanMessageBean saveContact(MemberLinkmanMessageBean memberLinkmanMessageBean);

    /**
     * 查询一个紧急联系人
     */
    public MemberLinkmanMessageBean queryOnlyContact(String memberId);

    /**
     * 保存邮箱
     */
    public MemberInfoMessageBean saveEmail(String email);
    /**
     * 保存邮箱验证标识
     */
    public MemberInfoMessageBean saveEmailMessage(String emailMessage, String email);
    /**
     * 更改验证状态
     */
    public MemberInfoMessageBean updateEmailMessage(String id);
    /**
     * 验证邮箱标识
     */
    public MemberInfoMessageBean checkEmailMessage(String message, String id);
    /**
     * 验证是否认证成功
     */
    public MemberInfoMessageBean checkEmailIsSuccess(String email);
    /**
     * 查询一个问题（所有问题表）
     */
    public SysSecurityQuestionMessageBean queryOnlyquestion(int id);
    /**
     * 查询是否设置安全保护问题
     */
    public boolean queryIsQuestion(String userId);
    /**
     * 保存问题
     */
    public MemberSetupQuestionsMessageBean saveQuestion(MemberSetupQuestionsMessageBean memberSetupQuestionsMessageBean);
    /**
     * 保存交易密码
     */
    public MemberInfoMessageBean saveTransactionPwd(MemberInfoMessageBean memberInfoMessageBean);
    /**
     * 更新手机号
     */
    public MemberInfoMessageBean updateTelephone(MemberInfoMessageBean memberInfoMessageBean);
    /**
     * 获取一个问题提问和答案
     */
    public String[] getOneQuestion(String userId);

    /**
     * 查询一个省
     */
    public SysProvinceMessageBean queryProvinceById(String id);

    /**
     * 查询一个市
     */
    public SysCityMessagebean queryCityById(String id);

    /**
     * 忘记密码 重置密码
     */
    public MemberInfoMessageBean reSetPw(MemberInfoMessageBean memberInfoMessageBean);

    /**
     * 验证是否有此手机号
     */
    public String checkIsTelephone(String telephone);

    /**
     * 保存紧急联系人 手机端
     */
    public MemberLinkmanMessageBean appSaveContact(MemberLinkmanMessageBean memberLinkmanMessageBean);

}
