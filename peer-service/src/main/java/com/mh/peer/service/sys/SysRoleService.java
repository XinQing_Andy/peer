package com.mh.peer.service.sys;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.SysRole;
import com.mh.peer.model.entity.SysRoleMenu;
import com.mh.peer.model.message.SysMenuMessageBean;
import com.mh.peer.model.message.SysRoleMenuMessBean;
import com.mh.peer.model.message.SysRoleMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016/1/27.
 */
public interface SysRoleService {
    /**
     * 查询角色信息
     * @return
     */
    List<SysRole> getRoleInfo();

    /**
     * 查询用户组信息(根据分页查询)(zhangerxin)
     * @param sysRolMese
     * @return
     */
    List<SysRoleMessageBean> getRoleInfoByPage(SysRoleMessageBean sysRolMese);

    /**
     * 根据id查询用户组信息(zhangerxin)
     * @param sysRolMese
     * @return
     */
    SysRoleMessageBean queryRoleById(SysRoleMessageBean sysRolMese);

    /**
     * 查询菜单信息
     * @return
     */
    List<SysRoleMenuMessBean> getMenuInfo();

    /**
     * 新增用户组
     * @param role
     * @return
     */
    LoginBusinessBean addRole(SysRoleMessageBean role);

    /**
     * 修改角色信息
     * @param role
     * @return
     */
    LoginBusinessBean updateRole(SysRoleMessageBean role);

    /**
     * 删除角色信息
     * @param role
     * @return
     */
    LoginBusinessBean delRole(SysRoleMessageBean role);

    /**
     * 根据角色查询菜单信息
     * @return
     */
    List<SysRoleMenuMessBean> getRoleMenuInfo(SysRoleMenuMessBean roleMenuMessBean);

    /**
     * 保存权限信息
     * @param roleMenuList
     * @return
     */
    SysRoleMenuMessBean saveRoleMenu(List<SysRoleMenu> roleMenuList);

    /**
     * 获取角色数量
     * @param sysRoleMes
     * @return
     */
    int getCountSysRoleByPage(SysRoleMessageBean sysRoleMes);

    /**
     * 获取所有菜单
     */
    List<SysMenuMessageBean> queryAllMenu();

    /**
     * 根据父id查询所有菜单
     * @param fid
     * @return
     */
    List<SysMenuMessageBean> queryAllMenuByFid(String fid);

    /**
     * 根据父菜单查询所有菜单数量
     * @param fid
     * @return
     */
    int queryAllMenuByFidCount(String fid);

    /**
     * 根据角色id查询都有哪些菜单
     * @return
     */
    List<SysRoleMenuMessBean> getAllRoleMenu(String roleId);

    /**
     * 保存用户角色菜单
     * @param sysRoleMenuMes
     * @return
     */
    SysRoleMenuMessBean saveRolePower(SysRoleMenuMessBean sysRoleMenuMes);
}
