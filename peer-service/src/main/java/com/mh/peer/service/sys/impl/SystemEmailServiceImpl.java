package com.mh.peer.service.sys.impl;

import com.mh.peer.dao.manager.SystemEmailDao;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.SysEmailSetting;
import com.mh.peer.model.message.SysEmailSettingMessageBean;
import com.mh.peer.service.sys.SystemEmailService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

/**
 * Created by cuibin on 2016/4/25.
 */
@Service
public class SystemEmailServiceImpl implements SystemEmailService{
    @Autowired
    private SystemEmailDao systemEmailDao;
    @Autowired
    private ServletUtil servletUtil;
    /**
     *查询邮件设置
     */
    @Override
    public SysEmailSettingMessageBean querySystemEmail() {
        DMap dMap = systemEmailDao.querySystemEmail();
        SysEmailSettingMessageBean sesm = new SysEmailSettingMessageBean();
        if(dMap.getCount() > 0){
            sesm.setId(TypeTool.getInt(dMap.getValue("ID", 0)));
            sesm.setAddress(TypeTool.getString(dMap.getValue("ADDRESS", 0)));
            sesm.setEmail(TypeTool.getString(dMap.getValue("EMAIL", 0)));
            sesm.setPassword(TypeTool.getString(dMap.getValue("PASSWORD", 0)));
            sesm.setPop(TypeTool.getString(dMap.getValue("POP", 0)));
            sesm.setSmtp(TypeTool.getString(dMap.getValue("SMTP", 0)));
            sesm.setCreatuser(TypeTool.getString(dMap.getValue("CREATUSER", 0)));
            sesm.setCreatetime(TypeTool.getString(dMap.getValue("CREATETIME", 0)));
            sesm.setInfo("查询成功");
            sesm.setResult("success");
        }else{
            sesm.setInfo("查询失败");
            sesm.setResult("error");
        }

        return sesm;
    }
    /**
     *  添加邮件设置
     */
    @Override
    public SysEmailSettingMessageBean addSystemEmail(SysEmailSettingMessageBean sysEmailSettingMessageBean) {
        SysEmailSetting sysEmailSetting = new SysEmailSetting();
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        int userId = bean.getSysUser().getId();
        if(sysEmailSettingMessageBean != null){
            sysEmailSetting.setAddress(sysEmailSettingMessageBean.getAddress());
            sysEmailSetting.setEmail(sysEmailSettingMessageBean.getEmail());
            sysEmailSetting.setPassword(sysEmailSettingMessageBean.getPassword());
            sysEmailSetting.setPop(sysEmailSettingMessageBean.getPop());
            sysEmailSetting.setSmtp(sysEmailSettingMessageBean.getSmtp());
            sysEmailSetting.setCreatuser(userId+"");
            sysEmailSetting.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss"));
        }
        DaoResult daoResult = systemEmailDao.addSystemEmail(sysEmailSetting);
        if(daoResult.getCode() < 0 || sysEmailSettingMessageBean == null){
            sysEmailSettingMessageBean.setInfo("添加失败");
            sysEmailSettingMessageBean.setResult("error");
        }
        sysEmailSettingMessageBean.setInfo("添加成功");
        sysEmailSettingMessageBean.setResult("success");
        return sysEmailSettingMessageBean;
    }
    /**
     *  更新邮件设置
     */
    @Override
    public SysEmailSettingMessageBean updateSystemEmail(SysEmailSettingMessageBean sysEmailSettingMessageBean) {
        SysEmailSetting sysEmailSetting = new SysEmailSetting();
        if(sysEmailSettingMessageBean != null){
            sysEmailSetting.setId(sysEmailSettingMessageBean.getId());
            sysEmailSetting.setAddress(sysEmailSettingMessageBean.getAddress());
            sysEmailSetting.setEmail(sysEmailSettingMessageBean.getEmail());
            sysEmailSetting.setPassword(sysEmailSettingMessageBean.getPassword());
            sysEmailSetting.setPop(sysEmailSettingMessageBean.getPop());
            sysEmailSetting.setSmtp(sysEmailSettingMessageBean.getSmtp());
        }
        DaoResult daoResult = systemEmailDao.updateSystemEmail(sysEmailSetting);
        if(daoResult.getCode() < 0 || sysEmailSettingMessageBean == null){
            sysEmailSettingMessageBean.setInfo("更新失败");
            sysEmailSettingMessageBean.setResult("error");
        }
        sysEmailSettingMessageBean.setInfo("更新成功");
        sysEmailSettingMessageBean.setResult("success");
        return sysEmailSettingMessageBean;
    }

}
