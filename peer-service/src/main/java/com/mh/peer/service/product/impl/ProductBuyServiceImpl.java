package com.mh.peer.service.product.impl;

import com.mh.peer.dao.product.ProductBuyDao;
import com.mh.peer.dao.product.ProductDao;
import com.mh.peer.model.business.ProductInvestBusinessBean;
import com.mh.peer.model.entity.ProductBuyInfo;
import com.mh.peer.model.entity.ProductContent;
import com.mh.peer.model.message.*;
import com.mh.peer.service.product.ProductBuyService;
import com.mh.peer.util.HandleTool;
import com.mh.peer.util.ProductAttorn;
import com.mh.peer.util.PublicsTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import com.sun.glass.ui.CommonDialogs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Administrator on 2016-5-9.
 */
@Service
public class ProductBuyServiceImpl implements ProductBuyService {

    @Autowired
    private ProductBuyDao productBuyDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 新增产品投资部分
     * @param productInvestMes
     * @return
     */
    @Override
    public ProductInvestBusinessBean saveProductInvest(ProductInvestMessageBean productInvestMes) {
        ProductInvestBusinessBean productInvestBus = new ProductInvestBusinessBean();
        ProductBuyInfo productBuyInfo = new ProductBuyInfo(); //购买产品
        ProductMessageBean productMessageBean = new ProductMessageBean(); //产品(消息对象)
        ProductContent productContent = new ProductContent(); //产品(实体类)
        String loadFee = "0"; //借款金额
        String yInvestFee = "0"; //已投资金额

        /********** 实体类部分Start **********/
        productBuyInfo.setId(productInvestMes.getId()); //主键
        productBuyInfo.setProductid(productInvestMes.getProductId()); //产品id
        productBuyInfo.setBuyer(productInvestMes.getBuyer()); //购买人id
        productBuyInfo.setBuytime(productInvestMes.getBuyTime()); //购买时间
        productBuyInfo.setPrice(TypeTool.getDouble(productInvestMes.getPrice())); //价格
        productBuyInfo.setType(productInvestMes.getType()); //类型 0-持有 1-转让中 2-已转让
        productBuyInfo.setCreatetime(productInvestMes.getCreateTime()); //操作时间
        /*********** 实体类部分End ***********/

        DaoResult resultProductBuy = productBuyDao.saveProductBuyInfo(productBuyInfo); //投资

        if(resultProductBuy.getCode() < 0){ //失败
            productInvestBus.setResult("error");
            productInvestBus.setInfo(resultProductBuy.getErrText());
            return productInvestBus;
        }

        productMessageBean.setId(productInvestMes.getProductId()); //产品主键
        DMap dMap = productDao.queryProductById(productMessageBean); //产品信息
        HandleTool handleTool = new HandleTool();
        if(dMap.getCount() > 0){
            loadFee = handleTool.getParamById(dMap.getValue("LOADFEE")); //借款金额
            yInvestFee = handleTool.getParamById(dMap.getValue("YINVESTFEE")); //已投资金额
            if(loadFee != null && !loadFee.equals("") && yInvestFee != null && !yInvestFee.equals("")){
                if(TypeTool.getDouble(loadFee) <= TypeTool.getDouble(yInvestFee)){
                    productContent.setId(productInvestMes.getProductId()); //主键
                    productContent.setFullscale("0"); //是否满标(0-满标、1-未满标)
                    productContent.setFulltime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd")); //满标日期
                    DaoResult resultProduct = productDao.updateProduct(productContent);
                    if(resultProduct.getCode() < 0){ //失败
                        productInvestBus.setResult("error");
                        productInvestBus.setInfo(resultProduct.getErrText());
                        return productInvestBus;
                    }
                }
            }
        }
        productInvestBus.setResult("success");
        productInvestBus.setInfo("投资成功！");
        return productInvestBus;
    }


    /**
     * 根据条件获取投资数量
     * @param productInvestMes
     * @return
     */
    @Override
    public int getCountBySome(ProductInvestMessageBean productInvestMes) {
        DMap parm = productBuyDao.getAllListBySome(productInvestMes);
        int count = parm.getCount(); //数量
        return count;
    }


    /**
     * 产品投资集合
     * @param productInvestMes
     * @return
     */
    @Override
    public List<ProductInvestMessageBean> getProductInvestList(ProductInvestMessageBean productInvestMes) {
        DMap dMap = productBuyDao.getAllListBySome(productInvestMes); //产品购买集合
        List<ProductInvestMessageBean> listProductInvestMes = new ArrayList<ProductInvestMessageBean>();
        String loadLength = ""; //贷款期限
        String buyTime = ""; //投资时间

        if(dMap.getCount() > 0){
            for(int i=0;i<dMap.getCount();i++){
                ProductInvestMessageBean productInvestMessageBean = new ProductInvestMessageBean();
                productInvestMessageBean.setId(dMap.getValue("ID",i)); //主键
                productInvestMessageBean.setProductId(dMap.getValue("PRODUCTID",i)); //产品id
                productInvestMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                productInvestMessageBean.setCode(dMap.getValue("CODE",i)); //编码
                productInvestMessageBean.setFullTime(dMap.getValue("FULLTIME",i)); //满标时间
                productInvestMessageBean.setLoadFee(dMap.getValue("LOADFEE",i)); //借款金额
                productInvestMessageBean.setBuyer(dMap.getValue("BUYER",i)); //购买人id
                productInvestMessageBean.setPrice(dMap.getValue("PRICE",i)); //购买金额
                productInvestMessageBean.setType(dMap.getValue("TYPE",i)); //类型(0-持有 1-转让中 2-已转让)
                productInvestMessageBean.setBuyerName(dMap.getValue("REALNAME",i)); //购买人姓名

                /********** 投资时间Start **********/
                buyTime = dMap.getValue("BUYTIME",i); //投资时间
                if (buyTime != null && !buyTime.equals("")){
                    buyTime = buyTime.substring(0,10);
                }
                productInvestMessageBean.setBuyTime(buyTime);
                /*********** 投资时间End ***********/

                productInvestMessageBean.setWaitPrice(dMap.getValue("WAITINGPRICE",i)); //待收本息
                productInvestMessageBean.setYsPrice(dMap.getValue("YSPRICE",i)); //已收金额
                productInvestMessageBean.setShouldPrice(dMap.getValue("SHOULDPRICE",i)); //应收金额
                productInvestMessageBean.setNextReceiveTime(dMap.getValue("NEXTRECEIVETIME",i)); //下次还款日期
                productInvestMessageBean.setCreateTime(dMap.getValue("CREATETIME",i)); //创建时间

                /*************** 贷款期限(Start) ***************/
                loadLength = "";
                if(dMap.getValue("LOANYEAR", i) != null && !dMap.getValue("LOANYEAR", i).equals("0")){
                    loadLength = loadLength + dMap.getValue("LOANYEAR", i) + "年";
                }
                if(dMap.getValue("LOANMONTH", i) != null && !dMap.getValue("LOANMONTH", i).equals("0")){
                    loadLength = loadLength + dMap.getValue("LOANMONTH", i) + "个月";
                }
                if(dMap.getValue("LOADDAY", i) != null && !dMap.getValue("LOADDAY", i).equals("0")){
                    loadLength = loadLength + dMap.getValue("LOADDAY", i) + "天";
                }
                productInvestMessageBean.setLoadLength(loadLength); //贷款期限
                /**************** 贷款期限(End) ****************/

                productInvestMessageBean.setSquareDate(dMap.getValue("SQUAREDATE",i)); //结清日期
                productInvestMessageBean.setLoadLength(loadLength); //还款时长
                productInvestMessageBean.setLoanrate(dMap.getValue("LOANRATE",i)); //还款利率
                productInvestMessageBean.setRepayment(dMap.getValue("REPAYMENT",i)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
                productInvestMessageBean.setInvestProgress(dMap.getValue("INVESTPROGRESS",i)); //投资进度
                productInvestMessageBean.setWaitingpricipal(dMap.getValue("WAITINGPRICIPAL", i)); //待收本金
                productInvestMessageBean.setWaitinginterest(dMap.getValue("WAITINGINTEREST",i)); //待收利息 应收利息 剩余本金
                productInvestMessageBean.setEndTime(dMap.getValue("ENDTIME",i));//到期时间
                productInvestMessageBean.setInvestFlag(dMap.getValue("INVESTFLAG",i));// 投资状态(0-回收中、1-投标中、2-已结清)
                productInvestMessageBean.setReceivedatesum(dMap.getValue("RECEIVEDATESUM",i));//剩余期数
                productInvestMessageBean.setNextReceiveDate(dMap.getValue("NEXTRECEIVETIME",i));//下次还款日

                listProductInvestMes.add(productInvestMessageBean);

            }
        }
        return listProductInvestMes;
    }


    /**
     * 产品投标信息数量
     * @param productInvestMes
     * @return
     */
    @Override
    public int getProductInvestCount(ProductInvestMessageBean productInvestMes) {
        DMap dMap = productBuyDao.getProductInvestCount(productInvestMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 根据id获取投资信息(zhangerxin)
     * @param productMyInvestMes
     * @return
     */
    @Override
    public ProductMyInvestMessageBean getInvestInfoById(ProductMyInvestMessageBean productMyInvestMes) {
        ProductMyInvestMessageBean productMyInvestMessageBean = new ProductMyInvestMessageBean();
        DMap dMap = productBuyDao.getInvestInfoById(productMyInvestMes);
        String endTime = ""; //到期时间

        if(dMap != null && dMap.getCount() > 0){
            productMyInvestMessageBean.setId(dMap.getValue("ID",0)); //主键
            productMyInvestMessageBean.setTitle(dMap.getValue("TITLE",0)); //题目
            productMyInvestMessageBean.setCode(dMap.getValue("CODE",0)); //编码
            productMyInvestMessageBean.setLoanRate(dMap.getValue("LOANRATE", 0)); //年利率
            productMyInvestMessageBean.setWaitingPricipal(dMap.getValue("WAITINGPRICIPAL", 0)); //待收本金
            productMyInvestMessageBean.setWaitingInterest(dMap.getValue("WAITINGINTEREST", 0)); //待收利息
            productMyInvestMessageBean.setCurrentInterest(dMap.getValue("CURRENTINTEREST", 0)); //当前应收利息
            productMyInvestMessageBean.setSurplusPeriods(dMap.getValue("SURPLUSPERIODS", 0)); //剩余期数

            endTime = dMap.getValue("ENDTIME", 0); //到期日期
            if (endTime != null && !endTime.equals("")){
                endTime = endTime.substring(0,10);
            }

            productMyInvestMessageBean.setExpireDate(endTime); //到期日期
            productMyInvestMessageBean.setReceiveFlag(dMap.getValue("RECEIVEFLAG", 0)); //回款状态
            productMyInvestMessageBean.setPriceMin(dMap.getValue("PRICEMIN", 0)); //最低转让价格
            productMyInvestMessageBean.setPriceMax(dMap.getValue("PRICEMAX", 0)); //最高转让价格
            productMyInvestMessageBean.setNextReceiveTime(dMap.getValue("NEXTRECEIVETIME", 0)); //下次还款日期
        }
        return productMyInvestMessageBean;
    }


    /**
     * 根据条件获取回收金额信息
     * @param productReceiveMes
     * @return
     */
    @Override
    public ProductReceiveMessageBean getReceivePriceSum(ProductReceiveMessageBean productReceiveMes) {
        DMap dMap = productBuyDao.getReceivePriceSum(productReceiveMes);
        ProductReceiveMessageBean productReceiveMessageBean = new ProductReceiveMessageBean(); //回收信息
        productReceiveMessageBean.setDsPrincipalTotal(dMap.getValue("PRINCIPAL",0)); //待收本金合计
        productReceiveMessageBean.setDsInterestTotal(dMap.getValue("INTEREST",0)); //待收利息合计
        productReceiveMessageBean.setDsPriceTotal(dMap.getValue("DRECEIVEPRICE",0)); //待收总额合计
        return productReceiveMessageBean;
    }

    /**
     * 根据条件获取回收数量
     * @param productReceiveMes
     * @return
     */
    @Override
    public String getReceiveCount(ProductReceiveMessageBean productReceiveMes) {
        DMap dMap = productBuyDao.getReceiveCount(productReceiveMes);
        String receiveCount = dMap.getValue("RECEIVECOUNT",0); //回收数量
        return receiveCount;
    }


    /**
     * 根据条件获取投资信息数量
     * @param productInvestMes
     * @return
     */
    @Override
    public String getInvestCount(ProductInvestMessageBean productInvestMes) {
        return null;
    }


    /**
     * 昨天新增投资数量 by cuibin
     */
    @Override
    public String[] queryLastDayMoney() {
        DMap dMap = productBuyDao.queryLastDayMoney();

        String[] arr = new String[12];
        List<String> sumList = new ArrayList<String>();
        List<String> buyTimeList = new ArrayList<String>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                sumList.add(dMap.getValue("BUYSUM", i)); //注册总数
                buyTimeList.add(dMap.getValue("BUYTIME", i)); //注册时间
            }
        }

        int temp = 0;
        int index = 0;
        for (int j = 0; j < arr.length; j++) {
            boolean mes = true;
            for (int i = 0; i < buyTimeList.size(); i++) {
                temp = Integer.parseInt(buyTimeList.get(i).substring(11, 13));

                if (temp >= index && temp < index + 2 && temp < 22) {
                    if (arr[j] != null) {
                        int sum = Integer.parseInt(arr[j]) + Integer.parseInt(sumList.get(i));
                        arr[j] = sum + "";
                    } else {
                        arr[j] = sumList.get(i);
                    }
                    mes = false;
                } else if (temp >= 22) {
                    if (arr[j] != null) {
                        int sum = Integer.parseInt(arr[j]) + Integer.parseInt(sumList.get(i));
                        arr[j] = sum + "";
                    } else {
                        arr[j] = sumList.get(i);
                    }
                    mes = false;
                }
            }
            if (mes) {
                arr[j] = "0";
            }
            index = index + 2;
        }

        return arr;
    }

    /**
     * 过去七天新增投资数量 by cuibin
     */
    @Override
    public String[] queryLastWeekMoney() {
        String[] arr = new String[7];
        int day = -7;
        GregorianCalendar cal = new GregorianCalendar();
        for (int i = 0; i < 7; i++) {
            cal.setTime(new Date());
            cal.add(Calendar.DATE, day);
            String time = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            arr[i] = productBuyDao.queryInvestSumByDay(time).getValue("INVESTSUM",0);
            day += 1;
        }

        return arr;
    }

    /**
     * 过去30天新增投资数量 by cuibin
     */
    @Override
    public String[] queryLastMonthMoney() {
        String[] arr = new String[7];
        arr[0] = "0";
        int day = -31;
        GregorianCalendar cal = new GregorianCalendar();
        for (int i = 1; i < 7; i++) {
            cal.setTime(new Date());
            cal.add(Calendar.DATE, day);
            String startTime = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            cal.setTime(new Date());
            cal.add(Calendar.DATE, day + 5);
            String endTime = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            arr[i] = productBuyDao.queryInvestSumByDay(startTime, endTime).getValue("INVESTSUM",0);
            day += 5;
        }

        return arr;
    }

    /**
     * 昨天投资金额 by cuibin
     */
    @Override
    public String[] queryLastDayInvest() {
        DMap dMap = productBuyDao.queryLastDayInvest();
        List<String> priceList = new ArrayList<String>();
        List<String> buyTimeList = new ArrayList<String>();
        //System.out.println(dMap);
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                priceList.add(TypeTool.getString(dMap.getValue("PRICE", i)));
                buyTimeList.add(TypeTool.getString(dMap.getValue("BUYTIME", i)));
            }
        }

        String[] arr = new String[12];
        int tempTime = 0;
        int realityTime = 0;

        for (int i = 0; i < arr.length; i++) {
            boolean message = true;
            for (int j = 0; j < priceList.size(); j++) {

                tempTime = Integer.parseInt(buyTimeList.get(j).substring(11, 13));
                if (tempTime >= realityTime && tempTime < realityTime + 2 && tempTime < 22) {

                    if (arr[i] != null) {
                        double sum = Double.parseDouble(arr[i]) + Double.parseDouble(priceList.get(j));
                        arr[i] = sum + "";
                    } else {
                        arr[i] = priceList.get(j);
                    }
                    message = false;

                } else if (tempTime >= 22) {

                    if (arr[i] != null) {
                        double sum = Double.parseDouble(arr[i]) + Double.parseDouble(priceList.get(j));
                        arr[i] = sum + "";
                    } else {
                        arr[i] = priceList.get(j);
                    }
                    message = false;

                }
            }
            if (message) {
                arr[i] = "0";
            }
            realityTime = realityTime + 2;
        }

        return arr;
    }

    /**
     * 最近七天投资金额 by cuibin
     */
    @Override
    public String[] queryLastWeekInvest() {
        String[] arr = new String[7];
        int day = -7;
        GregorianCalendar cal = new GregorianCalendar();
        for (int i = 0; i < 7; i++) {
            cal.setTime(new Date());
            cal.add(Calendar.DATE, day);
            String time = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            arr[i] = productBuyDao.queryInvestByDay(time).getValue("INVESTMONEY",0);
            day += 1;
        }

        return arr;
    }

    /**
     * 过去30天新增投资金额 by cuibin
     */
    @Override
    public String[] queryLastMonthInvest() {
        String[] arr = new String[7];
        arr[0] = "0";
        int day = -31;
        GregorianCalendar cal = new GregorianCalendar();
        for (int i = 1; i < 7; i++) {
            cal.setTime(new Date());
            cal.add(Calendar.DATE, day);
            String startTime = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            cal.setTime(new Date());
            cal.add(Calendar.DATE, day + 5);
            String endTime = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            arr[i] = productBuyDao.queryInvestByDay(startTime, endTime).getValue("INVESTMONEY",0);
            day += 5;
        }

        return arr;
    }


    /**
     * 获取投资项目(App部分)
     * @param memberId
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<AppProductBuyMessageBean> getInvestProject(String memberId, String typeId, String pageIndex, String pageSize) {
        List<AppProductBuyMessageBean> listAppProductBuyMes = new ArrayList<AppProductBuyMessageBean>();
        DMap dMap = productBuyDao.getInvestProject(memberId,typeId,pageIndex,pageSize);
        if (dMap != null && dMap.getCount()>0){
            for (int i=0;i<dMap.getCount();i++){
                AppProductBuyMessageBean appProductBuyMessageBean = new AppProductBuyMessageBean();
                appProductBuyMessageBean.setId(dMap.getValue("ID",i)); //主键
                appProductBuyMessageBean.setTypeId(dMap.getValue("TYPEID",i)); //产品类型id
                appProductBuyMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                appProductBuyMessageBean.setLoanRate(dMap.getValue("LOANRATE",i)); //年利率
                appProductBuyMessageBean.setEndTime(dMap.getValue("ENDTIME",i)); //到期时间
                appProductBuyMessageBean.setPrice(dMap.getValue("PRICE",i)); //购买金额
                appProductBuyMessageBean.setProductId(dMap.getValue("PRODUCTID",i)); //产品id
                listAppProductBuyMes.add(appProductBuyMessageBean);
            }
        }
        return listAppProductBuyMes;
    }


    /**
     * 获取投资产品详细信息
     * @param productId
     * @return
     */
    @Override
    public ProductInfoMessageBean queryProductDetails(String productId) {
        DMap dMap = productBuyDao.queryProductDetails(productId);
        String loanYear = ""; //贷款期限(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(日)
        String loanLength = ""; //贷款时长
        String bidFee = ""; //最低投标金额(Stirng型)
        Double bidFeeDou = 0.0d; //最低投标金额(Double型)
        String split = ""; //最高拆分份数(Stirng型)
        int splitin = 0; //最高拆分份数(int型)
        Double loanPrice = 0.0d; //贷款金额
        String repayment = ""; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        String repaymentName = ""; //还款方式名称
        String fullTime = ""; //满标时间
        String applyTime = ""; //申请时间
        ProductInfoMessageBean productInfoMessageBean = new ProductInfoMessageBean();

        if (dMap != null && dMap.getCount() > 0){
            productInfoMessageBean.setId(dMap.getValue("ID",0)); //主键
            productInfoMessageBean.setCode(dMap.getValue("CODE",0)); //编码
            productInfoMessageBean.setTitle(dMap.getValue("TITLE",0)); //题目
            productInfoMessageBean.setApplyMemberName(dMap.getValue("APPLYMEMBERNAME",0)); //出借人(申请人)
            productInfoMessageBean.setTypeName(dMap.getValue("TYPENAME",0)); //类型名称

            fullTime = dMap.getValue("FULLTIME",0); //满标日期
            if (fullTime != null && !fullTime.equals("")){
                fullTime = fullTime.substring(0,10);
            }
            productInfoMessageBean.setFullTime(fullTime);

            applyTime = dMap.getValue("APPLYTIME",0); //申请日期
            if (applyTime != null && !applyTime.equals("")){
                applyTime = applyTime.substring(0,10);
            }
            productInfoMessageBean.setApplyTime(applyTime);

            productInfoMessageBean.setLoadRate(dMap.getValue("LOANRATE",0)); //年利率
            loanYear = dMap.getValue("LOANYEAR",0); //贷款期限(年)
            loanMonth = dMap.getValue("LOANMONTH",0); //贷款期限(月)
            loanDay = dMap.getValue("LOADDAY",0); //贷款期限(日)
            if (loanYear != null && !loanYear.equals("")){
                if (!loanYear.equals("0")){
                    loanLength = loanLength + loanYear + "年";
                }
            }
            if (loanMonth != null && !loanMonth.equals("")){
                if (!loanMonth.equals("0")){
                    loanLength = loanLength + loanMonth + "个月";
                }
            }
            if (loanDay != null && !loanDay.equals("")){
                if (!loanDay.equals("0")){
                    loanLength = loanLength + loanDay + "日";
                }
            }
            productInfoMessageBean.setLoanLength(loanLength); //贷款时长

            bidFee = dMap.getValue("BIDFEE",0); //最低投标金额
            if (bidFee != null && !bidFee.equals("")){
                bidFeeDou = TypeTool.getDouble(bidFee);
            }
            split = dMap.getValue("SPLITCOUNT",0); //最高拆分份数
            if (split != null && !split.equals("")){
                splitin = TypeTool.getInt(split);
            }
            loanPrice = bidFeeDou * splitin; //贷款金额
            productInfoMessageBean.setLoanPrice(TypeTool.getString(loanPrice)); //贷款金额
            repayment = dMap.getValue("REPAYMENT",0); //还款方式
            if (repayment != null && !repayment.equals("")){
                if (repayment.equals("0")){
                    repaymentName = "按月还款、等额本息";
                }else if (repayment.equals("1")){
                    repaymentName = "按月付息、到期还本";
                }else if (repayment.equals("2")){
                    repaymentName = "一次性还款";
                }
            }
            productInfoMessageBean.setRepayment(repaymentName); //还款方式
            productInfoMessageBean.setShFlag(dMap.getValue("SHFLAG",0)); //审核状态
        }
        return productInfoMessageBean;
    }


    /**
     * 获取已回款统计信息
     * @param productBuyId
     * @return
     */
    @Override
    public ProductReceiveInfoMessageBean queryYReceiveInfo(String productBuyId) {
        DMap dMap = productBuyDao.queryYReceiveInfo(productBuyId);
        String principal = "0"; //应回款本金(String型)
        Double principalDou = 0.0d; //应回款本金(Double型)
        String interest = "0"; //应回款利息(String型)
        Double interestDou = 0.0d; //应回款利息(Double型)
        String yreceivePrice = ""; //应回款金额(String型)
        Double yreceivePriceDou = 0.0d; //应回款金额(Double型)
        String receivePrice = "0"; //实际回款金额(String型)
        Double receivePriceDou = 0.0d; //实际回款金额(Double型)
        String sinterest = ""; //实际回款利息(String型)
        Double sinterestDou = 0.0d; //实际回款利息(Double型)
        Double managePrice = 0.0d; //管理费
        ProductReceiveInfoMessageBean productReceiveInfoMessageBean = new ProductReceiveInfoMessageBean(); //投资回款信息

        if (dMap != null){
            principal = dMap.getValue("PRINCIPAL",0); //应还款本金(String型)
            if (principal != null && !principal.equals("")){
                principalDou = TypeTool.getDouble(principal); //应还款本金(Double型)
            }
            productReceiveInfoMessageBean.setPrincipal(principal); //应还款本金

            interest = dMap.getValue("INTEREST",0); //应还款利息(String型)
            if (interest != null && !interest.equals("")){
                interestDou = TypeTool.getDouble(interest); //应还款利息(Double型)
            }
            productReceiveInfoMessageBean.setInterest(interest); //应还款利息

            yreceivePriceDou = principalDou + interestDou; //应回款金额
            yreceivePriceDou = PublicsTool.round(yreceivePriceDou,2);
            productReceiveInfoMessageBean.setYreceivementPrice(TypeTool.getString(yreceivePriceDou));

            receivePrice = dMap.getValue("RECEIVEPRICE",0); //实际还款金额(String型)
            if (receivePrice != null && !receivePrice.equals("")){
                receivePriceDou = TypeTool.getDouble(receivePrice); //实际回款金额(Double型)
            }

            sinterestDou = receivePriceDou - principalDou; //实际回款利息
            sinterestDou = PublicsTool.round(sinterestDou,2);
            if (sinterestDou < 0){
                sinterestDou = 0.0d;
            }
            productReceiveInfoMessageBean.setSinterest(TypeTool.getString(sinterestDou)); //实际还款利息

            managePrice = yreceivePriceDou - receivePriceDou; //管理费(Double型)
            managePrice = PublicsTool.round(managePrice,2);
            productReceiveInfoMessageBean.setManagePrice(TypeTool.getString(managePrice)); //管理费(String型)
        }
        return productReceiveInfoMessageBean;
    }


    /**
     * 获取未还款统计信息
     * @param productBuyId
     * @return
     */
    @Override
    public ProductReceiveInfoMessageBean queryWReceiveInfo(String productBuyId) {
        DMap dMap = productBuyDao.queryWReceiveInfo(productBuyId);
        String principal = "0"; //应还款本金(String型)
        Double principalDou = 0.0d; //应还款本金(Double型)
        String interest = "0"; //应还款利息(String型)
        Double interestDou = 0.0d; //应还款利息(Double型)
        String yreceivePrice = ""; //应回款金额(String型)
        Double yreceivePriceDou = 0.0d; //应回款金额(Double型)
        ProductReceiveInfoMessageBean productReceiveInfoMessageBean = new ProductReceiveInfoMessageBean(); //投资回款信息

        if (dMap != null){
            principal = dMap.getValue("PRINCIPAL",0); //应还款本金(String型)
            if (principal != null && !principal.equals("")){
                principalDou = TypeTool.getDouble(principal); //应还款本金(Double型)
            }
            productReceiveInfoMessageBean.setPrincipal(principal);

            interest = dMap.getValue("INTEREST",0); //应还款利息(Stirng型)
            if (interest != null && !interest.equals("")){
                interestDou = TypeTool.getDouble(interest); //应还款利息(Double型)
            }
            productReceiveInfoMessageBean.setInterest(interest);

            yreceivePriceDou = principalDou + interestDou; //应还款金额(Double型)
            yreceivePriceDou = PublicsTool.round(yreceivePriceDou,2);
            productReceiveInfoMessageBean.setYreceivementPrice(TypeTool.getString(yreceivePriceDou)); //应还款金额(String型)
        }
        return productReceiveInfoMessageBean;
    }


}
