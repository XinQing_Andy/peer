package com.mh.peer.service.platform.impl;

import com.mh.peer.dao.platfrom.QuestionDao;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.entity.QuestionContent;
import com.mh.peer.model.message.QuestionContentMessageBean;
import com.mh.peer.service.platform.QuestionService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cuibin on 2016/4/19.
 */
@Service
public class QuestionServiceImpl implements QuestionService{

    @Autowired
    private QuestionDao questionDao;
    @Autowired
    private ServletUtil servletUtil;
    /**
     *查询所有常见问题
     */
    @Override
    public Paging queryQuestionByPage(Paging paging) {
        DMap dMap = questionDao.queryQuestionByPage(paging);
        int count = questionDao.queryQuestionByPageCount(paging).getCount();
        List list = new ArrayList();
        if(dMap.getCount() > 0){
            for (int i = 0; i < dMap.getCount() ; i++) {
                QuestionContentMessageBean qcm = new QuestionContentMessageBean();
                qcm.setId(TypeTool.getInt(dMap.getValue("ID", i)));
                qcm.setQuestion(TypeTool.getString(dMap.getValue("QUESTION", i)));
                qcm.setAnswer(TypeTool.getString(dMap.getValue("ANSWER", i)));
                qcm.setAutor(TypeTool.getString(dMap.getValue("AUTOR", i)));
                qcm.setSort(TypeTool.getInt(dMap.getValue("SORT", i)));
                qcm.setCreateuser(TypeTool.getString(dMap.getValue("CREATEUSER", i)));
                qcm.setCreatetime(TypeTool.getString(dMap.getValue("CREATETIME", i)));
                qcm.setUpdatetime(TypeTool.getString(dMap.getValue("UPDATETIME", i)));
                qcm.setFlag(TypeTool.getString(dMap.getValue("FLAG", i)));
                list.add(qcm);
            }
        }
        paging.setList(list);
        paging.setCount(count);
        return paging;
    }
    /**
     *删除常见问题
     */
    @Override
    public QuestionContentMessageBean deleteQuestion(QuestionContentMessageBean questionContentMessageBean) {
        QuestionContent qc = new QuestionContent();
        qc.setId(questionContentMessageBean.getId());
        DaoResult result = questionDao.deleteQuestion(qc);
        if(result.getCode() > 0){
            questionContentMessageBean.setInfo("删除成功");
            questionContentMessageBean.setResult("success");
        }else{
            questionContentMessageBean.setInfo("删除失败");
            questionContentMessageBean.setResult("error");
        }
        return questionContentMessageBean;
    }

    /**
     *添加常见问题
     */
    @Override
    public QuestionContentMessageBean addQuestion(QuestionContentMessageBean questionContentMessageBean) {
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean loginBusinessBean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        int userId = loginBusinessBean.getSysUser().getId();
        QuestionContent qc = new QuestionContent();
        if(questionContentMessageBean != null){
            qc.setQuestion(questionContentMessageBean.getQuestion());
            qc.setAnswer(questionContentMessageBean.getAnswer());
            qc.setAutor(questionContentMessageBean.getAutor());
            qc.setSort(questionContentMessageBean.getSort());
            qc.setCreateuser(userId+"");
            qc.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss"));
            qc.setUpdatetime("0");
            qc.setFlag("0");
            qc.setUpdateuser("0");
        }
        DaoResult result = questionDao.addQuestion(qc);
        if(result.getCode() < 0){
            questionContentMessageBean.setInfo("添加失败");
            questionContentMessageBean.setResult("error");
        }else{
            questionContentMessageBean.setInfo("添加成功");
            questionContentMessageBean.setResult("success");
        }
        return questionContentMessageBean;
    }
    /**
     *查询一个
     */
    @Override
    public QuestionContentMessageBean queryOnlyQuestion(QuestionContentMessageBean questionContentMessageBean) {
        DMap dMap = questionDao.queryOnlyQuestion(questionContentMessageBean.getId());
        QuestionContentMessageBean qcm = null;
        if(dMap.getCount() > 0){
            qcm = new QuestionContentMessageBean();
            qcm.setId(TypeTool.getInt(dMap.getValue("ID", 0)));
            qcm.setQuestion(TypeTool.getString(dMap.getValue("QUESTION", 0)));
            qcm.setAnswer(TypeTool.getString(dMap.getValue("ANSWER", 0)));
            qcm.setAutor(TypeTool.getString(dMap.getValue("AUTOR", 0)));
            qcm.setSort(TypeTool.getInt(dMap.getValue("SORT", 0)));
            qcm.setCreateuser(TypeTool.getString(dMap.getValue("CREATEUSER", 0)));
            qcm.setCreatetime(TypeTool.getString(dMap.getValue("CREATETIME", 0)));
            qcm.setUpdatetime(TypeTool.getString(dMap.getValue("UPDATETIME", 0)));
            qcm.setFlag(TypeTool.getString(dMap.getValue("FLAG", 0)));
        }
        return qcm;
    }
    /**
     *修改常见问题
     */
    @Override
    public QuestionContentMessageBean updateQuestion(QuestionContentMessageBean questionContentMessageBean) {
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean loginBusinessBean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        int userId = loginBusinessBean.getSysUser().getId();
        QuestionContent qc = new QuestionContent();
        if(questionContentMessageBean != null){
            qc.setId(questionContentMessageBean.getId());
            qc.setQuestion(questionContentMessageBean.getQuestion());
            qc.setAnswer(questionContentMessageBean.getAnswer());
            qc.setAutor(questionContentMessageBean.getAutor());
            qc.setSort(questionContentMessageBean.getSort());
            qc.setUpdatetime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss"));
            qc.setFlag("0");
            qc.setUpdateuser(userId+"");
        }
        DaoResult result = questionDao.updateQuestion(qc);
        if(result.getCode() < 0){
            questionContentMessageBean.setInfo("修改失败");
            questionContentMessageBean.setResult("error");
        }else{
            questionContentMessageBean.setInfo("修改成功");
            questionContentMessageBean.setResult("success");
        }
        return questionContentMessageBean;
    }
    /**
     *常见问题是否启用
     */
    @Override
    public QuestionContentMessageBean updateStart(QuestionContentMessageBean questionContentMessageBean) {
        QuestionContent qc = new QuestionContent();
        if(questionContentMessageBean != null){
            qc.setId(questionContentMessageBean.getId());
            qc.setFlag(questionContentMessageBean.getFlag());
        }
        DaoResult result = questionDao.updateQuestion(qc);
        if(result.getCode() < 0){
            questionContentMessageBean.setInfo("修改失败");
            questionContentMessageBean.setResult("error");
        }else{
            questionContentMessageBean.setInfo("修改成功");
            questionContentMessageBean.setResult("success");
        }
        return questionContentMessageBean;
    }
}
