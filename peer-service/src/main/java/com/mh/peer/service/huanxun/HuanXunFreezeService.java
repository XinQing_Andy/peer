package com.mh.peer.service.huanxun;

import com.mh.peer.model.entity.HuanxunFreezeAttorn;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.salon.frame.data.DMap;
import java.util.List;

/**
 * Created by Cuibin on 2016/7/29.
 */
public interface HuanXunFreezeService {


    /**
     * 查询冻结金额
     */
    public String queryFreeze(String memberId);

    /**
     * 产品投资部分
     * @param huanXunFreezeMessageBean
     * @return
     */
    public List<HuanXunFreezeMessageBean> getHuanXunFreezeInfoList(HuanXunFreezeMessageBean huanXunFreezeMessageBean);

    /**
     * 查询查询此项目所有投资记录 bycuibin
     */
    public List<HuanXunFreezeMessageBean> queryAllfreeze(String productId);

    /**
     * 按照冻结表id查询记录 bycuibin
     */
    public HuanXunFreezeMessageBean queryOnlyfreeze(String id);

    /**
     * 更新环迅冻结状态 bycuibin
     */
    public boolean updateHuanXunFreeze(String id);

    /**
     * 更改产品审核状态 bycuibin
     */
    public boolean updateProductFlag(String productId);

    /**
     * 按照id查询债权转让记录 bycuibin
     */
    public HuanXunFreezeMessageBean queryOnlyFreezeAttorn(String attornid);

    /**
     * 更新债权转让冻结状态 bycuibin
     */
    public boolean updateUnfreezeAttorn(String id);

    /**
     * 查询债权转让冻结记录 bycuibin
     */
    public HuanXunFreezeMessageBean queryFreezeAttorn(String attornid);

    /**
     * 查询债权转让记录 bycuibin
     */
    public int queryFreezeAttornCount(String id);

    /**
     * 冻结还款部分id
     * @param freezeRepayId
     * @return
     */
    HuanXunFreezeMessageBean queryFreezeRepay(String freezeRepayId);

}
