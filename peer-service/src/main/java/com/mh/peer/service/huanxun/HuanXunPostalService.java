package com.mh.peer.service.huanxun;

import com.mh.peer.model.entity.HuanxunPostal;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.HuanXunPostalMessageBean;

/**
 * Created by zhangerxin on 2016-7-26.
 */
public interface HuanXunPostalService {

    /**
     * 环迅部分提现
     * @param huanxunPostal 环迅提现
     * @param memberMessage 我的消息
     * @param tradeDetail 交易记录
     */
    public void postalhxResult(HuanxunPostal huanxunPostal,MemberMessage memberMessage,TradeDetail tradeDetail);

    /**
     * 获取提现信息
     * @param huanXunPostalMes
     * @return
     */
    public HuanXunPostalMessageBean getPostalBfInfo(HuanXunPostalMessageBean huanXunPostalMes);
}
