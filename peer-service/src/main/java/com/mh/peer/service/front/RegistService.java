package com.mh.peer.service.front;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.MemberInfoMessageBean;

/**
 * Created by cuibin on 2016/4/23.
 */
public interface RegistService {
    /**
     * 注册
     */
    public MemberInfoMessageBean regist(MemberInfo memberInfo);
    /**
     * 激活
     */
    public MemberInfoMessageBean activateUser(MemberInfo memberInfo);
    /**
     * 验证用户名是否存在
     */
    public MemberInfoMessageBean checkUsername(String username);

}
