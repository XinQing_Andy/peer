package com.mh.peer.service.front;

import com.mh.peer.model.message.MyAccountMessageBean;
import com.salon.frame.data.DMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-6-14.
 */
public interface FrontService {

    /**
     * 根据年份和月份获取当前月第一天
     * @return
     */
    public DMap queryFirstDay(String rq);

    /**
     * 获取上个月最后一天
     * @param rq
     * @return
     */
    public int queryLastDayLm(String rq);

    /**
     * 根据日期和会员id判断是否存在还款
     * @param receiveYear
     * @param receiveMonth
     * @param receiveDay
     * @param memberId
     * @return
     */
    public List<MyAccountMessageBean> getReceiveFlag(String receiveYear,String receiveMonth,String receiveDay,String memberId);

    /**
     * 根据日期和会员id判断是否存在回款
     * @param repayYear
     * @param repayMonth
     * @param repayDay
     * @param memberId
     * @return
     */
    public List<MyAccountMessageBean> getRepayFlag(String repayYear,String repayMonth,String repayDay,String memberId);

    /**
     * 根据日期和会员id获取回款信息
     * @param receiveYear
     * @param receiveMonth
     * @param memberId
     * @param receiveFlg
     * @return
     */
    MyAccountMessageBean getReceiveInfo(String receiveYear,String receiveMonth,String memberId,String receiveFlg);

    /**
     * 根据日期和会员id获取还款信息
     * @param repayYear
     * @param repayMonth
     * @param memberId
     * @param repayFlg
     * @return
     */
    MyAccountMessageBean getRepayInfo(String repayYear,String repayMonth,String memberId,String repayFlg);
 }
