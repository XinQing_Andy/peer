package com.mh.peer.service.bank.impl;

import com.mh.peer.dao.bank.BankDao;
import com.mh.peer.dao.member.AllMemberDao;
import com.mh.peer.model.entity.BankWh;
import com.mh.peer.model.entity.MemberBankno;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.BankWhMessageBean;
import com.mh.peer.model.message.MemberBanknoMessageBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.service.bank.BankService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Cuibin on 2016516.
 */


@Service
public class BankServiceImpl implements BankService{

    @Autowired
    private BankDao bankDao;
    @Autowired
    private AllMemberDao allMemberDao;
    @Autowired
    private ServletUtil servletUtil;


    /**
     * 查询所有银行
     */
    @Override
    public List<BankWhMessageBean> queryAllBank(BankWhMessageBean bankWhMessageBean) {
        DMap dMap = bankDao.queryAllBank(bankWhMessageBean);
        List<BankWhMessageBean> list = new ArrayList<BankWhMessageBean>();
        if(dMap.getCount() > 0){
            for (int i = 0; i < dMap.getCount() ; i++) {
                BankWhMessageBean temp = new BankWhMessageBean();
                temp.setId(dMap.getValue("ID",i));
                temp.setBankname(dMap.getValue("BANKNAME",i));
                temp.setBz(dMap.getValue("BZ", i));
                temp.setOperatime(dMap.getValue("OPERATIME", i));
                list.add(temp);
            }
        }
        return list;
    }


    //查询所有银行总数

    @Override
    public int queryAllBankCount(BankWhMessageBean bankWhMessageBean) {
        if(bankWhMessageBean != null){
            return bankDao.queryAllBankCount(bankWhMessageBean).getCount();
        }
        return 0;
    }

    //保存银行

    @Override
    public BankWhMessageBean saveBank(BankWhMessageBean bankWhMessageBean) {
        BankWhMessageBean bmb = new BankWhMessageBean();
        if(bankWhMessageBean == null){
            bmb.setResult("error");
            bmb.setInfo("保存失败");
            return bmb;
        }
        BankWh bankWh = new BankWh();
        bankWh.setId(UUID.randomUUID().toString().replaceAll("\\-",""));
        bankWh.setBankname(bankWhMessageBean.getBankname());
        bankWh.setBz(bankWhMessageBean.getBz());
        bankWh.setOperatime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd hh:mm:ss"));
        DaoResult daoResult = bankDao.saveBank(bankWh);
        if(daoResult.getCode() < 0){
            bmb.setResult("error");
            bmb.setInfo("保存失败");
        }else{
            bmb.setResult("success");
            bmb.setInfo("保存成功");
        }
        return bmb;
    }


    //查询一个银行

    @Override
    public BankWhMessageBean queryOnlyBank(BankWhMessageBean bankWhMessageBean) {
        BankWhMessageBean bmb = new BankWhMessageBean();
        if(bankWhMessageBean == null){
            bmb.setResult("error");
            bmb.setInfo("查询失败");
            return bmb;
        }
        DMap dMap = bankDao.queryOnlyBank(bankWhMessageBean.getId());
        if(dMap.getCount() > 0){
            bmb.setId(dMap.getValue("ID",0));
            bmb.setBankname(dMap.getValue("BANKNAME",0));
            bmb.setBz(dMap.getValue("BZ", 0));
            bmb.setOperatime(dMap.getValue("OPERATIME", 0));
            bmb.setResult("success");
            bmb.setInfo("查询成功");
        }else{
            bmb.setResult("error");
            bmb.setInfo("查询失败");
        }
        return bmb;
    }

   // 更新银行

    @Override
    public BankWhMessageBean updateBank(BankWhMessageBean bankWhMessageBean) {
        BankWhMessageBean bmb = new BankWhMessageBean();
        if(bankWhMessageBean == null){
            bmb.setResult("error");
            bmb.setInfo("更新失败");
            return bmb;
        }
        BankWh bankWh = new BankWh();
        bankWh.setId(bankWhMessageBean.getId());
        bankWh.setBankname(bankWhMessageBean.getBankname());
        bankWh.setBz(bankWhMessageBean.getBz());
        bankWh.setOperatime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd hh:mm:ss"));

        DaoResult daoResult = bankDao.updateBank(bankWh);
        if(daoResult.getCode() < 0){
            bmb.setResult("error");
            bmb.setInfo("更新失败");
        }else{
            bmb.setResult("success");
            bmb.setInfo("更新成功");
        }
        return bmb;
    }

    //删除银行

    @Override
    public BankWhMessageBean deleteBank(BankWhMessageBean bankWhMessageBean) {
        BankWhMessageBean bmb = new BankWhMessageBean();
        if(bankWhMessageBean == null){
            bmb.setResult("error");
            bmb.setInfo("删除失败");
            return bmb;
        }
        BankWh bankWh = new BankWh();
        bankWh.setId(bankWhMessageBean.getId());
        DaoResult daoResult = bankDao.deleteBank(bankWh);
        if(daoResult.getCode() < 0){
            bmb.setResult("error");
            bmb.setInfo("删除失败");
        }else{
            bmb.setResult("success");
            bmb.setInfo("删除成功");
        }
        return bmb;
    }

    /**
     * 保存银行（前台）
     */
    @Override
    public MemberBanknoMessageBean saveFrontBank(MemberBanknoMessageBean memberBanknoMessageBean) {
        MemberBanknoMessageBean bmb = new MemberBanknoMessageBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if (memberBanknoMessageBean == null || memberInfo == null) {
            bmb.setResult("error");
            bmb.setInfo("保存失败");
            return bmb;
        }
        MemberBankno memberBankno = new MemberBankno();
        //主键
        memberBankno.setId(UUID.randomUUID().toString().replaceAll("\\-", ""));
        //用户id
        String userId = memberInfo.getId();
        memberBankno.setMemberid(userId);
        //银行卡号
        memberBankno.setCardno("-");
        //银行id StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd hh:mm:ss")
        memberBankno.setBankid(memberBanknoMessageBean.getBankid());
        //省id
        memberBankno.setProvince(memberBanknoMessageBean.getProvince());
        //城市id
        memberBankno.setCity(memberBanknoMessageBean.getCity());
        //开户银行
        memberBankno.setBankaddress(memberBanknoMessageBean.getBankaddress());
        //创建用户
        memberBankno.setCreateuser(memberInfo.getUsername());
        //添加时间
        memberBankno.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd hh:mm:ss"));
        //更新人
        memberBankno.setUpdateuser(memberInfo.getUsername());
        //最后更新时间
        memberBankno.setUpdatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd hh:mm:ss"));
        DaoResult daoResult = bankDao.saveFrontBank(memberBankno);
        if (daoResult.getCode() < 0) {
            bmb.setResult("error");
            bmb.setInfo("保存失败");
        } else {
            bmb.setResult("success");
            bmb.setInfo("保存成功");
        }
        return bmb;
    }

    /**
     * 更新银行（前台）
     */
    @Override
    public MemberBanknoMessageBean frontUpdatebank(MemberBanknoMessageBean memberBanknoMessageBean) {

        MemberBanknoMessageBean bmb = new MemberBanknoMessageBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if (memberBanknoMessageBean == null || memberInfo == null) {
            bmb.setResult("error");
            bmb.setInfo("更新失败");
            return bmb;
        }

        DMap dMap = bankDao.queryUserBank(memberInfo.getId());
        //银行卡ID
        String yhid =  dMap.getValue("ID", 0);

        MemberBankno memberBankno = new MemberBankno();
        //主键
        memberBankno.setId(yhid);
        //用户id
        String userId = memberInfo.getId();
        memberBankno.setMemberid(userId);
        //银行卡号
        memberBankno.setCardno("-");
        //银行id
        memberBankno.setBankid(memberBanknoMessageBean.getBankid());
        //省id
        memberBankno.setProvince(memberBanknoMessageBean.getProvince());
        //城市id
        memberBankno.setCity(memberBanknoMessageBean.getCity());
        //开户银行
        memberBankno.setBankaddress(memberBanknoMessageBean.getBankaddress());
        //更新人
        memberBankno.setUpdateuser(memberInfo.getUsername());
        //最后更新时间StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd hh:mm:ss")
        memberBankno.setUpdatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd hh:mm:ss"));

        DaoResult daoResult = bankDao.frontUpdatebank(memberBankno);
        if(daoResult.getCode() < 0){
            bmb.setResult("error");
            bmb.setInfo("更新失败");
        }else{
            bmb.setResult("success");
            bmb.setInfo("更新成功");
        }

        return bmb;
    }



    /**
     * 查询当前银行卡信息（前台）
     */
    @Override
    public MemberBanknoMessageBean queryUserBank(String userId) {
        DMap dMap = bankDao.queryUserBank(userId);
        MemberBanknoMessageBean memberBanknoMessageBean = new MemberBanknoMessageBean();
        if(dMap.getCount() == 1){
            memberBanknoMessageBean.setId(dMap.getValue("ID",0));
            memberBanknoMessageBean.setMemberid(dMap.getValue("MEMBERID",0));
            memberBanknoMessageBean.setCardno(dMap.getValue("CARDNO",0));
            memberBanknoMessageBean.setBankid(dMap.getValue("BANKID",0));
            memberBanknoMessageBean.setProvince(dMap.getValue("PROVINCE",0));
            memberBanknoMessageBean.setCity(dMap.getValue("CITY",0));
            memberBanknoMessageBean.setBankaddress(dMap.getValue("BANKADDRESS", 0));
            memberBanknoMessageBean.setCreateuser(dMap.getValue("CREATEUSER", 0));
            memberBanknoMessageBean.setCreatetime(dMap.getValue("CREATETIME",0));
            memberBanknoMessageBean.setUpdateuser(dMap.getValue("UPDATEUSER",0));
            memberBanknoMessageBean.setUpdatetime(dMap.getValue("UPDATETIME", 0));
        }
        return memberBanknoMessageBean;
    }


    /**
     * 查询银行信息
     * @param memberBanknoMes
     * @return
     */
    @Override
    public List<MemberBanknoMessageBean> getMemberBankInfoBySome(MemberBanknoMessageBean memberBanknoMes) {
        List<MemberBanknoMessageBean> listMemberBanknoMes = new ArrayList<MemberBanknoMessageBean>();
        DMap dMap = bankDao.getMemberBankInfoBySome(memberBanknoMes);
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                MemberBanknoMessageBean memberBanknoMessageBean = new MemberBanknoMessageBean();
                memberBanknoMessageBean.setId(dMap.getValue("ID",i)); //主键
                memberBanknoMessageBean.setCardno(dMap.getValue("CARDNO",i)); //银行卡号
                memberBanknoMessageBean.setBankNo(dMap.getValue("BANKNO",i)); //银行编号
                memberBanknoMessageBean.setBankName(dMap.getValue("BANKNAME",i)); //银行名称
                memberBanknoMessageBean.setRealName(dMap.getValue("REALNAME",i)); //真实姓名
                memberBanknoMessageBean.setBalance(dMap.getValue("BALANCE",i)); //余额
                memberBanknoMessageBean.setCity(dMap.getValue("CITY",i));//城市
                memberBanknoMessageBean.setProvince(dMap.getValue("PROVINCE",i));//省
                memberBanknoMessageBean.setBankid(dMap.getValue("BANKID",i));//银行id
                memberBanknoMessageBean.setBankaddress(dMap.getValue("BANKADDRESS",i));//支行名称
                listMemberBanknoMes.add(memberBanknoMessageBean);
            }
        }
        return listMemberBanknoMes;
    }
}
