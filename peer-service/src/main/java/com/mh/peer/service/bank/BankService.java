package com.mh.peer.service.bank;

import com.mh.peer.model.message.BankWhMessageBean;
import com.mh.peer.model.message.MemberBanknoMessageBean;

import java.util.List;

/**
 * Created by Cuibin on 2016/5/16.
 */
public interface BankService {
    /**
     * 查询所有银行
     */
    List<BankWhMessageBean> queryAllBank(BankWhMessageBean bankWhMessageBean);
    /**
     * 查询所有银行总数
     */
    int queryAllBankCount(BankWhMessageBean bankWhMessageBean);
    /**
     * 保存银行
     */
    BankWhMessageBean saveBank(BankWhMessageBean bankWhMessageBean);
    /**
     * 查询一个银行
     */
    BankWhMessageBean queryOnlyBank(BankWhMessageBean bankWhMessageBean);
    /**
     * 更新银行
     */
    BankWhMessageBean updateBank(BankWhMessageBean bankWhMessageBean);
    /**
     * 删除银行
     */
    BankWhMessageBean deleteBank(BankWhMessageBean bankWhMessageBean);
    /**
     * 保存银行（前台）
     */
    MemberBanknoMessageBean saveFrontBank(MemberBanknoMessageBean memberBanknoMessageBean);
    /**
     * 更新银行（前台）
     */
    MemberBanknoMessageBean frontUpdatebank(MemberBanknoMessageBean memberBanknoMessageBean);
    /**
     * 查询当前银行卡信息
     */
    MemberBanknoMessageBean queryUserBank(String userId);

    /**
     * 查询银行卡信息
     * @param memberBanknoMes
     * @return
     */
    List<MemberBanknoMessageBean> getMemberBankInfoBySome(MemberBanknoMessageBean memberBanknoMes);
}
