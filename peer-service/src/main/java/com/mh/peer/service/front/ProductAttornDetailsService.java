package com.mh.peer.service.front;

import com.mh.peer.model.message.ProductAttornDetailsMes;

import java.util.List;

/**
 * Created by cuibin on 2016/6/29.
 */
public interface ProductAttornDetailsService {

    /**
     * 查询 成功的债权转让标 详情
     */
    public ProductAttornDetailsMes queryProductAttornDetails(ProductAttornDetailsMes productAttornDetailsMessageBean);

    /**
     * 查询竞拍记录
     */
    public List<ProductAttornDetailsMes> queryAuctionHistory(String productId);

}
