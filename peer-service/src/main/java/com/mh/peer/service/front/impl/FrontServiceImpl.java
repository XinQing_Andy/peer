package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.FrontDao;
import com.mh.peer.model.message.MyAccountMessageBean;
import com.mh.peer.service.front.FrontService;
import com.mh.peer.util.PublicsTool;
import com.salon.frame.data.DMap;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-6-14.
 */
@Service
public class FrontServiceImpl implements FrontService{

    @Autowired
    private FrontDao frontDao;

    /**
     * 根据年份和月份获取第一天
     * @return
     */
    @Override
    public DMap queryFirstDay(String rq) {
        DMap dMap = frontDao.queryFirstDay(rq);
        return dMap;
    }


    /**
     * 获取上个月最后一天
     * @param rq
     * @return
     */
    @Override
    public int queryLastDayLm(String rq) {
        DMap dMap = frontDao.queryLastDayLm(rq);
        Long lastDaylmll = 0L; //上个月的最后一天日期(长整型)
        int lastDaylm = 0; //上个月的最后一天日期(整型)
        if(dMap.getValue("LASTDAYLM") != null){
            lastDaylmll = TypeTool.getLong(dMap.getValue("LASTDAYLM",0));
            lastDaylm = new Long(lastDaylmll).intValue();
        }
        return lastDaylm;
    }


    /**
     * 根据日期和会员id判断是否存在回款
     * @param receiveYear
     * @param receiveMonth
     * @param receiveDay
     * @param memberId
     * @return
     */
    @Override
    public List<MyAccountMessageBean> getReceiveFlag(String receiveYear, String receiveMonth, String receiveDay, String memberId) {
        DMap dMap = frontDao.getReceiveFlag(receiveYear,receiveMonth,receiveDay,memberId);
        List<MyAccountMessageBean> listMyAccountMes = new ArrayList<MyAccountMessageBean>();
        String receivePrice = "0"; //回款金额(String型)
        Double receivePriceDou = 0.0d; //回款金额(Double型)
        if(dMap != null){
            for (int i=0;i<dMap.getCount();i++){
                MyAccountMessageBean myAccountMessageBean = new MyAccountMessageBean();
                myAccountMessageBean.setReceiveCount(dMap.getValue("RECEIVECOUNT",i)); //回款数量
                myAccountMessageBean.setDay(dMap.getValue("YRECEIVETIMEDAY",i)); //天

                receivePrice = dMap.getValue("RECEIVEPRICE",i); //回款金额(String型)
                if (receivePrice != null && !receivePrice.equals("")){
                    receivePriceDou = TypeTool.getDouble(receivePrice); //回款金额(Double型)
                    receivePriceDou = PublicsTool.round(receivePriceDou,2);
                }
                receivePrice = TypeTool.getString(receivePriceDou);
                myAccountMessageBean.setReceivePrice(receivePrice);

                listMyAccountMes.add(myAccountMessageBean);
            }
        }
        return listMyAccountMes;
    }


    /**
     * 根据日期和会员id判断是否存在还款
     * @param repayYear
     * @param repayMonth
     * @param memberId
     * @return
     */
    @Override
    public List<MyAccountMessageBean> getRepayFlag(String repayYear, String repayMonth, String repayDay, String memberId) {
        DMap dMap = frontDao.getRepayFlag(repayYear,repayMonth,repayDay,memberId);
        List<MyAccountMessageBean> listMyAccountMes = new ArrayList<MyAccountMessageBean>();
        if(dMap != null){
            for(int i=0;i<dMap.getCount();i++){
                MyAccountMessageBean myAccountMessageBean = new MyAccountMessageBean();
                myAccountMessageBean.setRepayCount(dMap.getValue("REPAYCOUNT",i)); //还款数量
                myAccountMessageBean.setDay(dMap.getValue("YREPAYMENTTIMEDAY",i)); //天
                myAccountMessageBean.setRepayPrice(dMap.getValue("REPAYPRICE",i)); //还款金额
                listMyAccountMes.add(myAccountMessageBean);
            }
        }
        return listMyAccountMes;
    }

    /**
     * 根据日期和会员id获取回款信息
     * @param receiveYear
     * @param receiveMonth
     * @param memberId
     * @param receiveFlg
     * @return
     */
    @Override
    public MyAccountMessageBean getReceiveInfo(String receiveYear, String receiveMonth, String memberId, String receiveFlg) {
        DMap dMap = frontDao.getReceiveInfo(receiveYear,receiveMonth,memberId,receiveFlg);
        MyAccountMessageBean myAccountMessageBean = new MyAccountMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            myAccountMessageBean.setYsPrice(dMap.getValue("YRECEIVEPRICE",0)); //已回收金额
            myAccountMessageBean.setYsCount(dMap.getValue("YRECEIVECOUNT",0)); //已回收数量
        }
        return myAccountMessageBean;
    }

    /**
     * 根据日期和会员id获取还款信息
     * @param repayYear
     * @param repayMonth
     * @param memberId
     * @param repayFlg
     * @return
     */
    @Override
    public MyAccountMessageBean getRepayInfo(String repayYear, String repayMonth, String memberId, String repayFlg) {
        DMap dMap = frontDao.getRepayInfo(repayYear,repayMonth,memberId,repayFlg);
        MyAccountMessageBean myAccountMessageBean = new MyAccountMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            myAccountMessageBean.setYhPrice(dMap.getValue("YREPAYMENTPRICE",0)); //已还金额
            myAccountMessageBean.setYhCount(dMap.getValue("YREPAYCOUNT",0)); //已还数量
        }
        return myAccountMessageBean;
    }


}
