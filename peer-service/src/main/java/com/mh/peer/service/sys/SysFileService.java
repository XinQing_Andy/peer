package com.mh.peer.service.sys;

import com.mh.peer.model.business.SysFileBusinessBean;
import com.mh.peer.model.entity.ProductContent;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.AppSysFileMessageBean;
import com.mh.peer.model.message.SysFileMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-10.
 */
public interface SysFileService {

    /**
     * 添加附件
     * @param sysFile
     * @return
     */
    SysFileBusinessBean saveSysFile(SysFile sysFile);

    /**
     * 删除附件
     * @param sysFile
     * @return
     */
    SysFileBusinessBean deleteSysFile(SysFile sysFile);

    /**
     * 根据关联id查询附件
     * @param glId
     * @return
     */
    List<SysFileMessageBean> getAllFile(String glId);

    /**
     * 根据主键查询附件信息
     * @param sysFileMes
     * @return
     */
    SysFileMessageBean getSysFileById(SysFileMessageBean sysFileMes);

    /**
     * 根据关联id获取附件地址(App部分)
     * @param glId
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    AppSysFileMessageBean getAppSysFile(String glId,String orderColumn,String orderType,String pageIndex,String pageSize);
}
