package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunAttornDao;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.mh.peer.service.huanxun.HuanXunAttornService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-8-2.
 * 债权转让资金冻结
 */
@Service
public class HuanXunAttornServiceImpl implements HuanXunAttornService{

    @Autowired
    private HuanXunAttornDao huanXunAttornDao;

    /**
     * 环迅债权转让资金冻结
     * @param huanxunFreezeAttorn
     * @param tradeDetail
     * @param memberMessage
     * @return
     */
    @Override
    public void attornhxResult(Map<String,Object> mapException,HuanxunFreezeAttorn huanxunFreezeAttorn, TradeDetail tradeDetail, MemberMessage memberMessage) {
        DaoResult daoResult = huanXunAttornDao.attornhxResult(mapException,huanxunFreezeAttorn,tradeDetail,memberMessage);
        if (daoResult.getCode() < 0){
            System.out.println("债权转让资金冻结失败！");
        }
    }


    /**
     * 获取债权冻结信息
     * @param huanXunFreezeMes
     * @return
     */
    @Override
    public List<HuanXunFreezeMessageBean> getFreezeAttornInfo(HuanXunFreezeMessageBean huanXunFreezeMes) {
        DMap dMap = huanXunAttornDao.getFreezeAttornInfo(huanXunFreezeMes);
        List<HuanXunFreezeMessageBean> listHuanXunFreezeMes = new ArrayList<HuanXunFreezeMessageBean>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                HuanXunFreezeMessageBean huanXunFreezeMessageBean = new HuanXunFreezeMessageBean();
                huanXunFreezeMessageBean.setId(dMap.getValue("ID",i)); //主键
                huanXunFreezeMessageBean.setProjectNo(dMap.getValue("PROJECTNO",i)); //项目ID号
                huanXunFreezeMessageBean.setIpsBillNo(dMap.getValue("IPSBILLNO",i)); //IPS原冻结订单号
                huanXunFreezeMessageBean.setIpsAcctNo(dMap.getValue("IPSACCTNO",i)); //冻结账号
                huanXunFreezeMessageBean.setOtherIpsAcctNo(dMap.getValue("OTHERIPSACCTNO",i)); //它方账号
                huanXunFreezeMessageBean.setTrdAmt(dMap.getValue("IPSTRDAMT",i)); //IPS冻结金额
                listHuanXunFreezeMes.add(huanXunFreezeMessageBean);
            }
        }
        return listHuanXunFreezeMes;
    }


    /**
     * 债权解冻
     * @param memberInfo
     * @param huanxunFreeze
     */
    @Override
    public void unfreezeAttornResult(Map<String,Object> mapException, MemberInfo memberInfo, HuanxunFreeze huanxunFreeze,
                                     ProductAttornRecord productAttornRecord,ProductBuyInfo productBuyInfo) {
        huanXunAttornDao.unfreezeAttornResult(mapException,memberInfo,huanxunFreeze,productAttornRecord,productBuyInfo);
    }
}
