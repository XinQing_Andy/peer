package com.mh.peer.service.fundSetting;

import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.message.*;

import java.util.List;

/**
 * Created by Cuibin on 2016/4/12.
 */
public interface FundSettingService {
    /**
     *添加字典
     */
    SysDictionaryMessageBean addDictionary(SysDictionaryMessageBean sysDictionaryMessageBean);
    /**
     * 删除字典
     */
    SysDictionaryMessageBean deleteDictionary(SysDictionaryMessageBean sysDictionaryMessageBean);
    /**
     * 修改字典
     */
    SysDictionaryMessageBean updateDictionary(SysDictionaryMessageBean sysDictionaryMessageBean);
    /**
     * 查询所有字典
     */
    Paging queryAllDictionary(Paging paging);
    /**
     * 查询单个字典
     */
    SysDictionaryMessageBean queryOnlyDictionary(SysDictionaryMessageBean sysDictionaryMessageBean);

    /**
     * 添加取号原则TakeNo
     */
    SysTakeNoMessageBean addTakeNo(SysTakeNoMessageBean sysTakeNoMessageBean);
    /**
     * 删除取号原则
     */
    SysTakeNoMessageBean deleteTakeNo(SysTakeNoMessageBean sysTakeNoMessageBean);
    /**
     * 更改取号原则
     */
    SysTakeNoMessageBean updateTakeNo(SysTakeNoMessageBean sysTakeNoMessageBean);
    /**
     * 查询所有取号原则
     */
    Paging queryAllTakeNo(Paging paging);

    /**
     * 查询单个取号原则
     */
    SysTakeNoMessageBean queryOnlyTakeNo(SysTakeNoMessageBean sysTakeNoMessageBean);

    /**
     *添加vip等级维护
     */
    MemberLevelMessageBean addVipMaintain(MemberLevelMessageBean memberLevelMessageBean);
    /**
     *删除vip等级维护
     */
    MemberLevelMessageBean deleteVipMaintain(MemberLevelMessageBean memberLevelMessageBean);
    /**
     *修改vip等级维护
     */
    MemberLevelMessageBean updateVipMaintain(MemberLevelMessageBean memberLevelMessageBean);
    /**
     *查询所有vip等级维护
     */
    Paging queryAllVipMaintain(Paging paging);
    /**
     *查询所有vip等级维护  2
     */
    List<MemberLevelMessageBean> queryAllVipMaintain2();
    /**
     *查询单个vip等级维护
     */
    MemberLevelMessageBean queryOnlyVipMaintain(MemberLevelMessageBean memberLevelMessageBean);

    /**
     *增加 按会员等级费用维护
     */
    MemberFeeLevelMessageBean addMemberFeeLevel(MemberFeeLevelMessageBean memberFeeLevelMessageBean);
    /**
     *删除 按会员等级费用维护
     */
    MemberFeeLevelMessageBean deleteMemberFeeLevel(MemberFeeLevelMessageBean memberFeeLevelMessageBean);
    /**
     *更改 按会员等级费用维护
     */
    MemberFeeLevelMessageBean updateMemberFeeLevel(MemberFeeLevelMessageBean memberFeeLevelMessageBean);
    /**
     *查询所有 按会员等级费用维护
     */
    Paging queryAllMemberFeeLevel(Paging paging);
    /**
     *查询一个 按会员等级费用维护
     */
    MemberFeeLevelMessageBean queryOnlyMemberFeeLevel(MemberFeeLevelMessageBean memberFeeLevelMessageBean);

    /**
     *增加 会员费用关联表
     */
    MemberFeeRelationMessageBean addMemberFeeRelation(MemberFeeRelationMessageBean memberFeeRelationMessageBean);
    /**
     *删除 会员费用关联表
     */
    MemberFeeRelationMessageBean deleteMemberFeeRelation(MemberFeeRelationMessageBean memberFeeRelationMessageBean);
    /**
     *更改 会员费用关联表
     */
    MemberFeeRelationMessageBean updateMemberFeeRelation(MemberFeeRelationMessageBean memberFeeRelationMessageBean);
    /**
     *查询一个 会员费用关联表
     */
    MemberFeeRelationMessageBean queryOnlyMemberFeeRelation(MemberFeeRelationMessageBean memberFeeRelationMessageBean);
    /**
     *查询所有 会员费用关联表
     */
    Paging queryAllMemberFeeRelation(Paging paging);
    List<MemberFeeRelationMessageBean> queryAllMemberFeeRelation2();


    /**
     *增加 固定费用过账表
     */
    SysManageAmtMessageBean addSysManageAmt(SysManageAmtMessageBean sysManageAmtMessageBeans);
    /**
     *删除 固定费用过账表
     */
    SysManageAmtMessageBean deleteSysManageAmt(SysManageAmtMessageBean sysManageAmtMessageBean);
    /**
     *修改 固定费用过账表
     */
    SysManageAmtMessageBean updateSysManageAmt(SysManageAmtMessageBean sysManageAmtMessageBean);
    /**
     *查询所有 固定费用过账表
     */
    Paging queryAllSysManageAmt(Paging paging);
    /**
     *查询一个 固定费用过账表
     */
    SysManageAmtMessageBean queryOnlySysManageAmt(SysManageAmtMessageBean sysManageAmtMessageBean);
    List<MemberFeeLevelMessageBean> queryAllMemberFeeLevel2();
}
