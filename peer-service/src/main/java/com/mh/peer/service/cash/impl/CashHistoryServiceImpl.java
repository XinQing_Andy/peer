package com.mh.peer.service.cash.impl;

import com.mh.peer.dao.cash.CashHistoryDao;
import com.mh.peer.model.message.HuanXunPostalMessageBean;
import com.mh.peer.service.cash.CashHistoryService;
import com.salon.frame.data.DMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cuibin on 2016/7/28.
 */

@Service
public class CashHistoryServiceImpl implements CashHistoryService {

    @Autowired
    private CashHistoryDao cashHistoryDao;


    /**
     * 查询提现统计
     */
    @Override
    public List<HuanXunPostalMessageBean> queryCashHistory(HuanXunPostalMessageBean huanXunPostalMessageBean) {
        DMap dMap = cashHistoryDao.queryCashHistory(huanXunPostalMessageBean);
        List<HuanXunPostalMessageBean> list = new ArrayList<HuanXunPostalMessageBean>();

        for (int i = 0; i < dMap.getCount(); i++) {
            HuanXunPostalMessageBean temp = new HuanXunPostalMessageBean();

            double ipsfeeSum = Double.parseDouble(dMap.getValue("IPSFEE", i)) +
                    Double.parseDouble(dMap.getValue("MERFEE", i)); //手续费

            //保留小数点后两位
            DecimalFormat df = new DecimalFormat("0.00");
            String ipsfeeSumStr = df.format(ipsfeeSum);
            temp.setIpsfeeSum(ipsfeeSumStr);

            temp.setCashSum(dMap.getValue("CASHSUM", i));//提现总额
            temp.setMerfee(dMap.getValue("MERFEE", i)); //平台手续费
            temp.setIpsfee(dMap.getValue("IPSFEE", i)); //IPS手续费
            temp.setIpstrdamt(dMap.getValue("IPSTRDAMT", i));//用户到账金额
            temp.setIpsdotime(dMap.getValue("IPSDOTIME", i));//IPS处理时间
            temp.setCreatetime(dMap.getValue("CREATETIME", i));//创建时间
            temp.setTrdstatus(dMap.getValue("TRDSTATUS", i));//提现状态
            temp.setRealname(dMap.getValue("REALNAME", i)); //姓名
            temp.setIdcard(dMap.getValue("IDCARD", i));//身份证号
            temp.setSource(dMap.getValue("SOURCE", i)); //充值平台
            list.add(temp);
        }

        return list;
    }

    /**
     * 查询提现统计 总数
     */
    @Override
    public String queryCashHistoryCount(HuanXunPostalMessageBean huanXunPostalMessageBean) {
        DMap dMap = cashHistoryDao.queryCashHistoryCount(huanXunPostalMessageBean);
        return dMap.getValue("COUNT", 0);
    }
}
