package com.mh.peer.service.sys.impl;

import com.mh.peer.dao.manager.SysSecurityQuestionDao;
import com.mh.peer.model.business.SysSecurityQuestionBusinessBean;
import com.mh.peer.model.entity.SysSecurityQuestionParameter;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.mh.peer.model.message.SysSecurityQuestionMessageBean;
import com.mh.peer.service.sys.SysSecurityQuestionService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-1.
 */
@Service
public class SysSecurityQuestionServiceImpl implements SysSecurityQuestionService {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private SysSecurityQuestionDao sysSecurityQuestionDao;

    /**
     * 获取安全问题信息(根据分页查询)
     * @param sysSecurityQuestionMes
     * @return
     */
    @Override
    public List<SysSecurityQuestionMessageBean> querySecurityQuestionByPage(SysSecurityQuestionMessageBean sysSecurityQuestionMes) {
        List<SysSecurityQuestionMessageBean> sysSecurityQuestionMess = new ArrayList<SysSecurityQuestionMessageBean>(); //安全问题集合
        DMap parm = sysSecurityQuestionDao.querySecurityQuestionByPage(sysSecurityQuestionMes); //安全问题
        String num = "1"; //序号
        if(parm.getCount() > 0){
            for (int i = 0;i < parm.getCount();i++){
                SysSecurityQuestionMessageBean sysSecurityQuestionMessageBean = new SysSecurityQuestionMessageBean();

                sysSecurityQuestionMessageBean.setId(parm.getValue("ID", i)); //主键
                sysSecurityQuestionMessageBean.setQuestion(parm.getValue("QUESTION", i)); //问题
                sysSecurityQuestionMessageBean.setFormat(parm.getValue("FORMAT",i)); //格式
                sysSecurityQuestionMessageBean.setActive_flg(parm.getValue("ACTIVE_FLG",i)); //是否启用名称
                sysSecurityQuestionMessageBean.setCreateUser(parm.getValue("CREATEUSER", i)); //创建用户
                sysSecurityQuestionMessageBean.setCreateUser(parm.getValue("CREATETIME", i)); //创建时间
                sysSecurityQuestionMessageBean.setUpdateUser(parm.getValue("UPDATEUSER", i)); //更新人
                sysSecurityQuestionMessageBean.setUpdateTime(parm.getValue("UPDATETIME", i)); //最后更新时间
                sysSecurityQuestionMessageBean.setNum(num); //序号
                num = String.valueOf(Integer.parseInt(num) + 1);
                sysSecurityQuestionMess.add(sysSecurityQuestionMessageBean);
            }
        }
        return sysSecurityQuestionMess;
    }

    /**
     * 新增安全问题
     * @param sysSecurityQuestionMess
     * @return
     */
    @Override
    public SysSecurityQuestionBusinessBean addSysSecurityQuestion(SysSecurityQuestionMessageBean sysSecurityQuestionMess) {
        //创建业务对象
        SysSecurityQuestionBusinessBean sysSecurityQuestionBus = new SysSecurityQuestionBusinessBean();

        //创建实体对象(安全问题)
        SysSecurityQuestionParameter sysSecurityQuestion = new SysSecurityQuestionParameter();
        sysSecurityQuestion.setQuestion(sysSecurityQuestionMess.getQuestion()); //问题
        sysSecurityQuestion.setFormat(sysSecurityQuestionMess.getFormat()); //问题格式
        sysSecurityQuestion.setActiveFlg(sysSecurityQuestionMess.getActive_flg()); //是否启用
        sysSecurityQuestion.setCreateuser(sysSecurityQuestionMess.getCreateUser()); //创建用户
        sysSecurityQuestion.setCreatetime(sysSecurityQuestionMess.getCreateTime()); //创建时间
        sysSecurityQuestion.setUpdateuser(sysSecurityQuestionMess.getUpdateUser()); //更新用户
        sysSecurityQuestion.setUpdatetime(sysSecurityQuestionMess.getUpdateTime()); //最后更新时间

        DaoResult result = sysSecurityQuestionDao.addSysSecurityQuestion(sysSecurityQuestion); //新增安全问题

        if(result.getCode() < 0){
            sysSecurityQuestionBus.setResult("error");
            sysSecurityQuestionBus.setInfo(result.getErrText());
            return sysSecurityQuestionBus;
        }
        sysSecurityQuestionBus.setResult("success");
        sysSecurityQuestionBus.setInfo("增加成功");
        return sysSecurityQuestionBus;
    }


    /**
     * 修改安全问题
     * @param sysSecurityQuestionMess
     * @return
     */
    @Override
    public SysSecurityQuestionBusinessBean updateSysSecurityQuestion(SysSecurityQuestionMessageBean sysSecurityQuestionMess) {

        //创建业务对象
        SysSecurityQuestionBusinessBean  sysSecurityQuestionBus = new  SysSecurityQuestionBusinessBean();
        //创建实体对象
        SysSecurityQuestionParameter sysSecurityQuestion = new SysSecurityQuestionParameter();
        sysSecurityQuestion.setId(TypeTool.getInt(sysSecurityQuestionMess.getId())); //主键
        sysSecurityQuestion.setActiveFlg(sysSecurityQuestionMess.getActive_flg()); //是否启用

        //修改安全问题
        DaoResult result = sysSecurityQuestionDao.updateSysSecurityQuestion(sysSecurityQuestion);

        if(result.getCode() < 0){
            sysSecurityQuestionBus.setResult("error");
            sysSecurityQuestionBus.setInfo(result.getErrText());
            return sysSecurityQuestionBus;
        }
        sysSecurityQuestionBus.setResult("success");
        sysSecurityQuestionBus.setInfo("设置成功");

        return sysSecurityQuestionBus;
    }

    /**
     * 安全问题数量
     * @param sysSecurityQuestionMess
     * @return
     */
    @Override
    public int getCountSecurityQuestion(SysSecurityQuestionMessageBean sysSecurityQuestionMess) {
        DMap parm = sysSecurityQuestionDao.getCountSecurityQuestion(sysSecurityQuestionMess); //安全问题
        int count = parm.getCount();
        return count;
    }

}
