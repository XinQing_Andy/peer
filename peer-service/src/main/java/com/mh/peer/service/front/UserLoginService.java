package com.mh.peer.service.front;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.message.AppMemberMessageBean;
import com.mh.peer.model.message.LoginMessageBean;

/**
 * Created by cuibin on 2016/4/22.
 */
public interface UserLoginService {
    /**
     * 用户登录
     */
    public LoginBusinessBean userLogin(LoginMessageBean user);

    /**
     * 登录操作(App部分)
     * @param appMemberMes
     * @return
     */
    AppMemberMessageBean login(AppMemberMessageBean appMemberMes);

    /**
     * 注册操作(App部分)
     * @param appMemberMes
     * @return
     */
    AppMemberMessageBean regist(AppMemberMessageBean appMemberMes);

    /**
     * 获取会员数量(App部分)
     * @param appMemberMes
     * @return
     */
    String getMemberCount(AppMemberMessageBean appMemberMes);

    /**
     * 获取会员信息
     * @param appMemberMes
     * @return
     */
    AppMemberMessageBean getMemberInfo(AppMemberMessageBean appMemberMes);

    /**
     * 修改密码
     * @param memberId
     * @param password
     * @return
     */
    AppMemberMessageBean updatePassword(String memberId,String password);
}
