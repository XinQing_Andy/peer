package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunRegistDao;
import com.mh.peer.model.entity.HuanxunRegist;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.java2d.d3d.D3DScreenUpdateManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${zhangerxin} on 2016-7-23.
 */
@Service
public class HuanXunRegistServiceImpl implements HuanXunRegistService {

    @Autowired
    private HuanXunRegistDao huanXunRegistDao;

    /**
     * 环迅注册
     * @param huanxunRegist
     * @param memberMessage
     */
    @Override
    public void registerhxResult(HuanxunRegist huanxunRegist, MemberMessage memberMessage){
        DaoResult daoResult = huanXunRegistDao.registerhxResult(huanxunRegist,memberMessage);
    }

    /**
     * 根据条件查询会员注册信息
     * @param huanXunRegistMes
     * @return
     */
    @Override
    public List<HuanXunRegistMessageBean> getRegisterhxBySome(HuanXunRegistMessageBean huanXunRegistMes) {
        DMap dMap = huanXunRegistDao.getRegisterhxBySome(huanXunRegistMes);
        List<HuanXunRegistMessageBean> listHuanXunRegistMes = new ArrayList<HuanXunRegistMessageBean>();
        if (dMap != null && dMap.getCount()>0){
            for (int i=0;i<dMap.getCount();i++){
                HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean();
                huanXunRegistMessageBean.setId(dMap.getValue("ID",i)); //主键
                huanXunRegistMessageBean.setUserName(dMap.getValue("USERNAME",i)); //用户名
                huanXunRegistMessageBean.setIpsAcctNo(dMap.getValue("IPSACCTNO",i)); //IPS虚拟账号
                huanXunRegistMessageBean.setBalance(dMap.getValue("BALANCE",i)); //余额
                huanXunRegistMessageBean.setMemberId(dMap.getValue("MEMBERID",i)); //会员id
                listHuanXunRegistMes.add(huanXunRegistMessageBean);
            }
        }
        return listHuanXunRegistMes;
    }


}
