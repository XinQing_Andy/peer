package com.mh.peer.service.product.impl;

import com.mh.peer.dao.product.ProductDao;
import com.mh.peer.dao.product.ProductTypeDao;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.ProductType;
import com.mh.peer.model.message.ProductMessageBean;
import com.mh.peer.model.message.ProductTypeMessage;
import com.mh.peer.service.product.ProductTypeService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cuibin on 2016/4/29.
 */
@Service
public class ProductTypeServiceImpl implements ProductTypeService {
    @Autowired
    private ProductTypeDao productTypeDao;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 查询所有产品类型
     */
    @Override
    public List<ProductTypeMessage> queryAllProductType(ProductTypeMessage productTypeMessage) {
        List<ProductTypeMessage> productTypeList = new ArrayList<ProductTypeMessage>();
        DMap parm = productTypeDao.queryAllProductType(productTypeMessage); //查询产品
        String num = "1"; //序号
        if(parm.getCount() > 0){
            for (int i = 0;i < parm.getCount();i++){
                ProductTypeMessage temp = new ProductTypeMessage();
                temp.setId(parm.getValue("ID", i));
                temp.setTitle(parm.getValue("TITLE", i));
                temp.setTypeName(parm.getValue("TYPENAME", i));
                temp.setCreateUser(parm.getValue("CREATEUSER", i));
                temp.setCreateTime(parm.getValue("CREATETIME", i));
                productTypeList.add(temp);
            }
        }
        return productTypeList;
    }


    /**
     * 查询所有产品类型数量
     */
    @Override
    public int queryAllProductTypeCount(ProductTypeMessage productTypeMessage) {
        return productTypeDao.queryAllProductTypeCount(productTypeMessage).getCount();
    }

    /**
     * 删除产品类型
     */
    @Override
    public ProductTypeMessage deleteProductType(ProductTypeMessage productTypeMessage) {
        ProductType productType = new ProductType();
        if(productTypeMessage != null && !productTypeMessage.equals("")){
            productType.setId(Integer.parseInt(productTypeMessage.getId()));
        }
        DaoResult daoResult = productTypeDao.deleteProductType(productType);
        if(daoResult.getCode() < 0){
            productTypeMessage.setInfo("删除失败");
            productTypeMessage.setResult("error");
        }else{
            productTypeMessage.setInfo("删除成功");
            productTypeMessage.setResult("success");
        }
        return productTypeMessage;
    }

    /**
     * 添加产品类型
     */
    @Override
    public ProductTypeMessage addProductType(ProductTypeMessage productTypeMessage) {
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        int userId = bean.getSysUser().getId();
        ProductType productType = new ProductType();
        if(productTypeMessage != null && !productTypeMessage.equals("")){
            String createDate = StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss");
            if(createDate.length() >= 14){
                createDate =  createDate.substring(0,4)+"-"+createDate.substring(4,6) + "-"+createDate.substring(6,8) +" " +createDate.substring(8,10) +":"+
                        createDate.substring(10,12)+":"+createDate.substring(12,14);
            }
            productType.setCreatetime(createDate);
            productType.setCreateuser(userId);
            productType.setTypename(productTypeMessage.getTypeName());
        }
        DaoResult daoResult = productTypeDao.addProductType(productType);
        if(daoResult.getCode() < 0){
            productTypeMessage.setInfo("添加失败");
            productTypeMessage.setResult("error");
        }else{
            productTypeMessage.setInfo("添加成功");
            productTypeMessage.setResult("success");
        }
        return productTypeMessage;
    }

    /**
     * 更新产品类型
     */
    @Override
    public ProductTypeMessage updateProductType(ProductTypeMessage productTypeMessage) {
        ProductType productType = new ProductType();
        if(productTypeMessage != null && !productTypeMessage.equals("")){
            productType.setId(Integer.parseInt(productTypeMessage.getId()));
            productType.setTypename(productTypeMessage.getTypeName());
        }
        DaoResult daoResult = productTypeDao.updateProductType(productType);
        if(daoResult.getCode() < 0){
            productTypeMessage.setInfo("更新失败");
            productTypeMessage.setResult("error");
        }else{
            productTypeMessage.setInfo("更新成功");
            productTypeMessage.setResult("success");
        }
        return productTypeMessage;
    }
}
