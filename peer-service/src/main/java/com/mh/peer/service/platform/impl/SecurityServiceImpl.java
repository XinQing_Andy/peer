package com.mh.peer.service.platform.impl;

import com.mh.peer.dao.manager.SysFileDao;
import com.mh.peer.dao.platfrom.SecurityDao;
import com.mh.peer.model.business.AdvertContentBusinessBean;
import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.message.AdvertContentMessageBean;
import com.mh.peer.service.platform.AdvertService;
import com.mh.peer.service.platform.SecurityService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-11.
 */
@Service
public class SecurityServiceImpl implements SecurityService {

    @Autowired
    private SecurityDao securityDao;
    @Autowired
    private SysFileDao sysFileDao;

    /**
     * 根据分页获取广告
     * @param advertContentMes
     * @return
     */
    @Override
    public List<AdvertContentMessageBean> queryAdvertByPage(AdvertContentMessageBean advertContentMes) {
        List<AdvertContentMessageBean> advertContentMessageBeans = new ArrayList<AdvertContentMessageBean>();
        DMap parm = securityDao.queryAdvertByPage(advertContentMes); //查询广告
        String content = ""; //内容
        int len = 0; //内容长度

        if(parm.getCount() > 0){
            for (int i = 0;i < parm.getCount();i++){
                AdvertContentMessageBean advertContentMessageBean = new AdvertContentMessageBean();
                advertContentMessageBean.setId(parm.getValue("ID", i)); //主键
                advertContentMessageBean.setTypeId(parm.getValue("TYPEID", i)); //类型id
                advertContentMessageBean.setTitle(parm.getValue("TITLE", i)); //题目
                content = parm.getValue("CONTENT", i); //内容
                len = content.length();
                if(len > 15){
                    content = content.substring(0,15) + " ......";
                }
                advertContentMessageBean.setContent(content);
                advertContentMessageBean.setAutor(parm.getValue("AUTOR", i)); //作者
                advertContentMessageBean.setSort(parm.getValue("SORT", i)); //排序
                advertContentMessageBean.setFlag(parm.getValue("FLAG", i)); //状态(0-有效、1-无效)
                advertContentMessageBean.setLook_Count(parm.getValue("CREATEUSER", i)); //创建人
                advertContentMessageBean.setCreateTime(parm.getValue("CREATETIME", i)); //创建时间
                advertContentMessageBeans.add(advertContentMessageBean);
            }
        }
        return advertContentMessageBeans;
    }

    /**
     * 新增广告
     * @param advertContent
     * @return
     */
    @Override
    public AdvertContentBusinessBean saveAdvert(AdvertContent advertContent) {
        AdvertContentBusinessBean advertContentBus = new AdvertContentBusinessBean();
        DaoResult result = securityDao.saveAdvert(advertContent); //新增产品
        if(result.getCode() < 0){
            advertContentBus.setResult("error");
            advertContentBus.setInfo(result.getErrText());
            return advertContentBus;
        }
        advertContentBus.setResult("success");
        advertContentBus.setInfo("保存成功");
        return advertContentBus;
    }


    /**
     * 根据id获取广告信息
     * @param advertContentMes
     * @return
     */
    @Override
    public AdvertContentMessageBean queryProductById(AdvertContentMessageBean advertContentMes) {
        AdvertContent advertContent = new AdvertContent(); //广告
        DMap dMap = new DMap();

        advertContent.setId(advertContentMes.getId()); //广告主键
        advertContent = securityDao.getAdvertById(advertContent);
        dMap = sysFileDao.querySysFileById(advertContentMes.getId()); //附件信息

        if(advertContent != null){
            advertContentMes.setId(advertContent.getId()); //主键
            advertContentMes.setTypeId(advertContent.getTypeid()); //类型id
            advertContentMes.setTitle(advertContent.getTitle()); //题目
            advertContentMes.setFlag(advertContent.getFlag()); //状态(0-启用、1-暂停)
            advertContentMes.setContent(advertContent.getContent()); //内容
            advertContentMes.setLook_Count(TypeTool.getString(advertContent.getLookCount())); //浏览次数
            advertContentMes.setAutor(advertContent.getAutor()); //作者
            advertContentMes.setSort(TypeTool.getString(advertContent.getSort())); //排序
            advertContentMes.setCreateUser(advertContent.getCreateuser()); //创建人
            advertContentMes.setCreateTime(advertContent.getCreatetime()); //创建时间
        }

        if(dMap.getCount() > 0){
            advertContentMes.setFileId(dMap.getValue("ID").substring(1, dMap.getValue("ID").length() - 1)); //附件id
            advertContentMes.setImgUrlJd(dMap.getValue("IMGURLJD").substring(1, dMap.getValue("IMGURLJD").length() - 1)); //附件地址(绝对地址)
            advertContentMes.setImgUrlXd(dMap.getValue("IMGURL").substring(1, dMap.getValue("IMGURL").length() - 1)); //附件地址(相对地址)
        }
        return advertContentMes;
    }

    /**
     * 更新广告
     * @param advertContent
     * @return
     */
    @Override
    public AdvertContentBusinessBean updateAdvert(AdvertContent advertContent) {
        AdvertContentBusinessBean advertContentBusinessBean = new AdvertContentBusinessBean();
        DaoResult result = securityDao.updateAdvert(advertContent);
        if(result.getCode() < 0){
            advertContentBusinessBean.setResult("error");
            advertContentBusinessBean.setInfo(result.getErrText());
        }else{
            advertContentBusinessBean.setResult("success");
            advertContentBusinessBean.setInfo("修改成功");
        }
        return advertContentBusinessBean;
    }

    /**
     * 获取保障数量
     * @param advertContent
     * @return
     */
    @Override
    public int getCountAdvertByPage(AdvertContentMessageBean advertContent) {
        DMap parm = securityDao.getCountAdvertByPage(advertContent); //查询广告
        int totalCount = parm.getCount(); //广告数量
        return totalCount;
    }

}
