package com.mh.peer.service.sys;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.SysDept;
import com.mh.peer.model.entity.SysRole;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.mh.peer.model.message.SysUserMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016/1/27.
 */
public interface SysUserService {

    /**
     * 查询角色
     * @return
     */
    //public List<SysRole> getSysRole();

    /**
     * 查询用户信息(根据分页查询)(zhangerxin)
     * @param sysUserMese
     * @return
     */
    List<SysUserMessageBean> getUserInfoByPage(SysUserMessageBean sysUserMese);

    /**
     * 根据id查询用户信息(zhangerxin)
     * @param sysUserMese
     * @return
     */
    SysUserMessageBean queryUserById(SysUserMessageBean sysUserMese);

    /**
     * 查询用户数量
     * @param sysUserMese
     * @return
     */
    int getCountUserInfoByPage(SysUserMessageBean sysUserMese);

    /**
     * 新增部门信息
     * @param user
     * @return
     */
    LoginBusinessBean addUser(SysUserMessageBean user);

    /**
     * 修改部门信息
     * @param user
     * @return
     */
    LoginBusinessBean updateUser(SysUserMessageBean user);

    /**
     * 删除用户信息
     * @param user
     * @return
     */
    //public LoginBusinessBean delUser(SysUserMessageBean user);

    /**
     * 设置用户状态
     * @param user
     * @return
     */
    LoginBusinessBean settingSysFlag(SysUserMessageBean user);

    /**
     * 重置用户密码
     * @param user
     * @return
     */
    LoginBusinessBean resetPwd(SysUserMessageBean user);

    /**
     * 检查用户唯一性
     * @param user
     * @return
     */
    int checkUser(SysUserMessageBean user);
}
