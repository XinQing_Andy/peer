package com.mh.peer.service.huanxun;

import com.mh.peer.model.entity.HuanxunRegproject;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.ProductHuanXunMessageBean;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-27.
 */
public interface HuanXunRegProjectService {

    /**
     * 项目登记部分
     * @param mapException
     * @param huanxunRegProject
     * @param memberMessage
     * @param tradeDetail
     * @return
     */
    public ProductHuanXunMessageBean regProjecthxResult(Map<String,Object> mapException,HuanxunRegproject huanxunRegProject,MemberMessage memberMessage,TradeDetail tradeDetail);

    /**
     * 根据条件获取登记项目
     * @param productHuanXunMessageBean
     * @return
     */
    public List<ProductHuanXunMessageBean> getRegProjectInfoList(ProductHuanXunMessageBean productHuanXunMessageBean);
}
