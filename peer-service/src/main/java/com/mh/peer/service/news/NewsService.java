package com.mh.peer.service.news;

import com.mh.peer.model.business.NewsContentBusinessBean;
import com.mh.peer.model.business.NewsTypeBusinessBean;
import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.*;

import java.util.List;

/**
 * Created by zhangerxin on 2016/4/5.
 */
public interface NewsService {

    /**
     * 内容管理(查询所有新闻内容 带分页)
     */
    Paging queryNewsByPage(NewsContentMessageBean newsContentM);

    /**
     * 类别管理(查询所有类别)
     */
    List<NewsTypeMessageBean> queryType();
    /**
     * 类别管理-编辑后保存
     */
    NewsTypeBusinessBean saveType(NewsTypeMessageBean newTypeM);
    /**
     * 内容管理 删除新闻
     */
    NewsContentBusinessBean deleteNews(NewsContentMessageBean newsContentM);

    /**
     * 内容管理 添加新闻
     */
    NewsContentBusinessBean saveNews(NewsContentMessageBean newsContentMes);

    /**
     * 内容管理 保存图片信息
     */
    void saveImg(String path, String absPath, String uuid);
    /**
     * 内容管理 修改新闻 查询单个新闻
     */
    NewsContentMessageBean queryNewsById(NewsContentMessageBean newsContent);
    /**
     * 内容管理 修改新闻 查询图片信息
     */
    SysFileMessageBean queryImg(String newsId);
    /**
     * 内容管理 修改新闻 提交新闻
     */
    NewsContentBusinessBean updateNews(NewsContentMessageBean newsConM);
    /**
     * 内容管理 删除图片
     */
    void deleteImg(String imgId);

    /**
     * 内容管理 查询新闻分类
     */
    List<NewsTypeMessageBean> queryNewsType(NewsTypeMessageBean newsTypeMes);

    /**
     * 修改新闻是否显示
     */
    NewsContentMessageBean updateShow(NewsContentMessageBean newsContentMessageBean);
    /**
     * 修改新闻是否推荐
     */
    NewsContentMessageBean updateRecommend(NewsContentMessageBean newsContentMessageBean);
    /**
     * 添加二级类别
     */
    NewsTypeMessageBean addTwoType(NewsTypeMessageBean newsTypeMessageBean);
    /**
     * 删除二级类别
     */
    NewsTypeMessageBean deleteTwoNewsType(NewsTypeMessageBean newsTypeMessageBean);
    /**
     * 查询一级类别
     */
    List<NewsTypeMessageBean> queryOneType();
    /**
     * 查询新闻数量
     */
    int queryNewsByPageCount(NewsContentMessageBean newsContentM);

    /**
     * 查询新闻内容集合
     * @param newsContentMes
     * @return
     */
    List<NewsContentMessageBean> queryNewsByP(NewsContentMessageBean newsContentMes);

    /**
     * 查询新闻内容数量
     * @param newsContentMes
     * @return
     */
    int queryNewsCount(NewsContentMessageBean newsContentMes);

    /**
     * 更新新闻访问次数(用户端)
     */
    void updateNewsVisit(NewsContentMessageBean newsContentMessageBean);

    /**
     * 获取app首页部分新闻(App端)
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<AppHomeMessageBean> getAppNews(String orderColumn,String orderType,String pageIndex,String pageSize);

    /**
     * 获取新闻列表(App端)
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<AppNewsContentMessageBean> getAppNewsList(String orderColumn,String orderType,String pageIndex,String pageSize);

    /**
     * 获取新闻详情
     * @param newsId
     * @return
     */
    AppNewsContentMessageBean getAppNewsDetails(String newsId);

    /**
     * 获取新闻列表(微信) by cuibin
     */
    public List<WeixinNewsMes> queryNewsList(WeixinNewsMes weixinNewsMes);

    /**
     * 查询一个新闻(微信) by cuibin
     */
    public WeixinNewsMes queryOnlyNews(String id);

    /**
     * 获取新闻列表总数(微信) by cuibin
     */
    public String queryNewsCount(WeixinNewsMes weixinNewsMes);

    /**
     * 搜索财富资讯集合
     * @param weathInformationMes
     * @return
     */
    List<WeathInformationMessageBean> queryWeathByPage(WeathInformationMessageBean weathInformationMes);

    /**
     * 搜索财富资讯数量
     * @param weathInformationMes
     * @return
     */
    int queryWeathCount(WeathInformationMessageBean weathInformationMes);

    /**
     * 搜索财富资讯详细信息
     * @param weathInformationMes
     * @return
     */
    WeathInformationMessageBean queryWeathDetails(WeathInformationMessageBean weathInformationMes);
}
