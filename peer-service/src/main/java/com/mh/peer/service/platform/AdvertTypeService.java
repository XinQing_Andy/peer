package com.mh.peer.service.platform;

import com.mh.peer.model.entity.AdvertType;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-12.
 */
public interface AdvertTypeService {

    /**
     * 查询所有广告类别
     * @return
     */
    List<AdvertType> getAllAdvertType();
}
