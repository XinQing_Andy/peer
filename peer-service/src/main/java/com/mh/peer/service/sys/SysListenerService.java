package com.mh.peer.service.sys;

import com.mh.peer.dao.manager.SysListenerDao;
import com.mh.peer.model.message.SysListenerMessageBean;

/**
 * Created by zhangerxin on 2016-5-19.
 */
public interface SysListenerService {

    /**
     * 获取监听信息
     * @return
     */
    SysListenerMessageBean getSysListenerInfo();
}
