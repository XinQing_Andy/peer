package com.mh.peer.service.huanxun;

import com.mh.peer.model.entity.HuanxunRegist;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016-7-23.
 */
public interface HuanXunRegistService {

    /**
     * 环迅注册
     * @param huanxunRegist
     * @param memberMessage
     */
    public void registerhxResult(HuanxunRegist huanxunRegist, MemberMessage memberMessage);

    /**
     * 根据条件查询会员注册信息
     * @param huanXunRegistMes
     * @return
     */
    public List<HuanXunRegistMessageBean> getRegisterhxBySome(HuanXunRegistMessageBean huanXunRegistMes);
}
