package com.mh.peer.service.product.impl;

import com.mh.peer.dao.manager.SysFileDao;
import com.mh.peer.dao.product.ProductDao;
import com.mh.peer.model.business.ProductBusinessBean;
import com.mh.peer.model.business.SysFileBusinessBean;
import com.mh.peer.model.entity.ProductContent;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.*;
import com.mh.peer.service.product.ProductService;
import com.mh.peer.util.HandleTool;
import com.mh.peer.util.ProductSh;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by zhangerxin on 2016-4-10.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;
    @Autowired
    private SysFileDao sysFileDao;

    /**
     * 根据分页查询产品
     *
     * @param sysUserMese
     * @return
     */
    @Override
    public List<ProductMessageBean> queryProductByPage(ProductMessageBean sysUserMese) {
        List<ProductMessageBean> productMessageBeans = new ArrayList<ProductMessageBean>();
        DMap parm = productDao.queryProductByPage(sysUserMese); //查询产品
        Double loadFee = 0.0d; //贷款金额
        String num = "1"; //序号
        if (parm.getCount() > 0) {
            for (int i = 0; i < parm.getCount(); i++) {
                ProductMessageBean productMessageBean = new ProductMessageBean();
                productMessageBean.setId(parm.getValue("ID", i)); //主键
                productMessageBean.setTitle(parm.getValue("TITLE", i)); //标题
                productMessageBean.setCreatetime(parm.getValue("CREATETIME", i)); //创建时间
                productMessageBean.setLoanrate(parm.getValue("LOANRATE", i)); //贷款利率
                productMessageBean.setCreditstart(parm.getValue("CREDITSTART", i)); //额度范围(起始)
                productMessageBean.setCreditend(parm.getValue("CREDITEND", i)); //额度范围(截止)
                productMessageBean.setFlag(parm.getValue("FLAG", i)); //状态(0-上架、1-下架)
                productMessageBean.setShflag(parm.getValue("SHFLAG", i)); //审核状态(0-审核通过、1-审核不通过、2-未审核)
                productMessageBean.setShyj(parm.getValue("SHYJ", i)); //审核意见
                productMessageBean.setFullScale(parm.getValue("FULLSCALE", i)); //是否满标(0-是、1-否)
                productMessageBean.setSort(parm.getValue("SORT", i)); //排序
                productMessageBean.setBidfee(parm.getValue("BIDFEE", i)); //投标金额
                productMessageBean.setSplitcount(parm.getValue("SPLITCOUNT", i)); //拆分份数
                productMessageBean.setType(parm.getValue("HUANXUNFLAG", i)); //是否在环迅注册过
                productMessageBean.setInfo(parm.getValue("NEWSHARE",i)); //是否设为新手专享(0-是、1-否)

                /********** 贷款金额Start **********/
                if (parm.getValue("BIDFEE", i) != null && !parm.getValue("BIDFEE", i).equals("") && parm.getValue("SPLITCOUNT", i) != null && !parm.getValue("SPLITCOUNT", i).equals("")) {
                    loadFee = TypeTool.getDouble(parm.getValue("BIDFEE", i)) * TypeTool.getDouble(parm.getValue("SPLITCOUNT", i)); //贷款金额
                    productMessageBean.setLoadFee(TypeTool.getString(loadFee)); //贷款金额
                }
                /*********** 贷款金额End ***********/

                /********** 序号部分Start **********/
                productMessageBean.setNum(num); //序号
                num = String.valueOf(Integer.parseInt(num) + 1);
                /*********** 序号部分End ***********/

                productMessageBeans.add(productMessageBean);
            }
        }
        return productMessageBeans;
    }


    /**
     * 获取产品数量(前台)
     *
     * @param productMes
     * @return
     */
    @Override
    public int queryProductCount(ProductMessageBean productMes) {
        DMap dMap = productDao.queryProductCount(productMes);
        int count = dMap.getCount();
        return count;
    }


    /**
     * 新增产品
     *
     * @param productMes
     * @return
     */
    @Override
    public ProductBusinessBean saveProduct(ProductMessageBean productMes) {
        ProductBusinessBean productBus = new ProductBusinessBean(); //产品
        String productId = UUID.randomUUID().toString().replaceAll("\\-", ""); //产品id
        String sysFileId = ""; //附件主键
        String pathPj = ""; //附件地址(相对地址,绝对地址;相对地址,绝对地址)
        String[] arr = null; //用于存放附件地址
        int num = 0; //用于获取相对路径和绝对路径之间的逗号位置
        String xdPath = ""; //相对路径
        String jdPath = ""; //绝对路径
        productMes.setId(productId); //主键

        /********** 实体类赋值(产品)Start **********/
        ProductContent productContent = new ProductContent(); //产品
        productContent.setId(productMes.getId()); //主键
        productContent.setTitle(productMes.getTitle()); //题目
        productContent.setCode(productMes.getCode()); //编码
        productContent.setType(productMes.getProductType()); //产品类型id
        productContent.setFlag("1"); //状态(0-上架、1-下架)
        productContent.setShflag("2"); //审核状态(0-审核通过、1-审核未通过、2-未审核)
        productContent.setCrowds(productMes.getCrowds()); //适合人群
        productContent.setHighpoint(productMes.getHighpoint()); //亮点
        productContent.setCreditstart(TypeTool.getDouble(productMes.getCreditstart())); //额度范围(起始)
        productContent.setCreditend(TypeTool.getDouble(productMes.getCreditend())); //额度范围(截止)
        productContent.setLoanrate(TypeTool.getDouble(productMes.getLoanrate())); //贷款利率
        productContent.setLoanyear(TypeTool.getInt(productMes.getLoanyear())); //贷款期限(年)
        productContent.setLoanmonth(TypeTool.getInt(productMes.getLoanmonth())); //贷款期限(月)
        productContent.setLoadday(TypeTool.getInt(productMes.getLoadday())); //贷款期限(日)
        productContent.setBidday(productMes.getBidday()); //投标时间(日)
        productContent.setFullscale(productMes.getFullScale()); //是否满标(0-是、1-否)
        productContent.setChecktime(TypeTool.getInt(productMes.getChecktime())); //审核时间
        productContent.setRepayment(productMes.getRepayment()); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        productContent.setBidfee(TypeTool.getDouble(productMes.getBidfee())); //投标金额
        productContent.setSplitcount(TypeTool.getInt(productMes.getSplitcount())); //拆分份数
        productContent.setCounterfee(productMes.getCounterfee()); //手续费
        productContent.setApplymember(productMes.getApplyMember()); //申请人
        productContent.setApplycondition(productMes.getApplycondition()); //申请人条件
        productContent.setOnlinetime(productMes.getOnlinetime()); //上线时间
        productContent.setSponsor(productMes.getSponsor()); //担保人
        productContent.setSort(TypeTool.getInt(productMes.getSort())); //排序
        productContent.setCreateuser(productMes.getCreateuser()); //创建人
        productContent.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
        productContent.setUpdateuser(productMes.getUpdateuser()); //更新人
        productContent.setUpdatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //更新时间
        /*********** 实体类赋值产品)End ***********/

        DaoResult resultProduct = productDao.saveProduct(productContent); //新增产品

        if (resultProduct.getCode() < 0) { //失败
            productBus.setResult("error");
            productBus.setInfo(resultProduct.getErrText());
            return productBus;
        } else {
            /********** 附件部分Start **********/
            pathPj = productMes.getImgUrl(); //附件地址拼接
            if (pathPj != null && !pathPj.equals("")) {
                arr = pathPj.split(";");
                if (arr != null && arr.length > 0) {
                    for (int i = 0; i < arr.length; i++) {
                        sysFileId = UUID.randomUUID().toString().replaceAll("\\-", ""); //附件主键
                        num = arr[i].indexOf(",");
                        xdPath = arr[i].substring(0, num); //相对路径
                        jdPath = arr[i].substring(num + 1); //绝对路径
                        SysFile sysFile = new SysFile();
                        SysFileBusinessBean sysFileBus = new SysFileBusinessBean(); //附件
                        sysFile.setId(sysFileId); //主键
                        sysFile.setGlid(productId); //关联id
                        sysFile.setImgurl(xdPath); //相对路径
                        sysFile.setImgurljd(jdPath); //绝对路径
                        sysFile.setType("1"); //类型(1-产品)
                        sysFile.setSort(0); //排序
                        sysFile.setCreateuser(productMes.getCreateuser()); //创建人
                        sysFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                        DaoResult resultSysFile = sysFileDao.saveProduct(sysFile);
                        if (resultSysFile.getCode() < 0) {
                            productBus.setResult("error");
                            productBus.setInfo(resultSysFile.getErrText());
                            return productBus;
                        }
                    }
                }
            }
            /*********** 附件部分End ***********/
        }

        productBus.setId(productId); //产品主键
        productBus.setResult("success");
        productBus.setInfo("保存成功");
        return productBus;
    }


    /**
     * 根据id获取产品信息
     *
     * @param productMessageBean
     * @return
     */
    @Override
    public ProductMessageBean queryProductById(ProductMessageBean productMessageBean) {
        ProductMessageBean productMes = new ProductMessageBean(); //产品
        MemberInfoMessageBean memberInfoMes = new MemberInfoMessageBean();
        DMap dMap = productDao.queryProductById(productMessageBean); //根据id获取产品信息
        HandleTool handleTool = new HandleTool();
        String productId = ""; //产品id
        String bidfee = ""; //最高投标金额
        String splitcount = ""; //最低拆分份数
        Double loadFee = 0.0d; //贷款金额
        String loadLength = ""; //贷款期限
        String xdUrl = ""; //相对路径
        String xdUrlPj = ""; //相对路径拼接
        String ybiddingFee = ""; //已投标金额
        String bidProgress = "0"; //投标进度

        if (dMap != null) {
            /****************************** 产品部分Start ******************************/
            productId = handleTool.getParamById(dMap.getValue("ID")); //主键
            productMes.setId(handleTool.getParamById(dMap.getValue("ID"))); //主键
            productMes.setTitle(handleTool.getParamById(dMap.getValue("TITLE"))); //题目
            productMes.setCode(handleTool.getParamById(dMap.getValue("CODE"))); //借款标编码
            productMes.setProductType(handleTool.getParamById(dMap.getValue("TYPE"))); //类型id
            productMes.setFlag(handleTool.getParamById(dMap.getValue("FLAG"))); //状态(0-上架、1-下架)
            productMes.setShflag(handleTool.getParamById(dMap.getValue("SHFLAG"))); //审核状态(0-审核通过、1-审核未通过、2-未审核)
            productMes.setCrowds(handleTool.getParamById(dMap.getValue("CROWDS"))); //适合人群
            productMes.setHighpoint(handleTool.getParamById(dMap.getValue("HIGHPOINT"))); //亮点
            productMes.setCreditstart(handleTool.getParamById(dMap.getValue("CREDITSTART"))); //额度范围(起始)
            productMes.setCreditend(handleTool.getParamById(dMap.getValue("CREDITEND"))); //额度范围(截止)
            productMes.setLoanrate(handleTool.getParamById(dMap.getValue("LOANRATE"))); //贷款利率
            productMes.setLoanyear(handleTool.getParamById(dMap.getValue("LOANYEAR"))); //贷款期限(年)
            productMes.setLoanmonth(handleTool.getParamById(dMap.getValue("LOANMONTH"))); //贷款期限(月)
            productMes.setLoadday(handleTool.getParamById(dMap.getValue("LOADDAY"))); //贷款期限(日)
            productMes.setBalance(handleTool.getParamById(dMap.getValue("BALANCE")));//账户余额
            productMes.setFullTime(handleTool.getParamById(dMap.getValue("FULLTIME")));//满标日期
            productMes.setCreatetime(handleTool.getParamById(dMap.getValue("CREATETIME")));//创建时间

            /************************* 贷款期限Start *************************/
            if (handleTool.getParamById(dMap.getValue("LOANYEAR")) != null && !handleTool.getParamById(dMap.getValue("LOANYEAR")).equals("0")) {
                loadLength = loadLength + handleTool.getParamById(dMap.getValue("LOANYEAR")) + "年";
            }
            if (handleTool.getParamById(dMap.getValue("LOANMONTH")) != null && !handleTool.getParamById(dMap.getValue("LOANMONTH")).equals("0")) {
                loadLength = loadLength + handleTool.getParamById(dMap.getValue("LOANMONTH")) + "个月";
            }
            if (handleTool.getParamById(dMap.getValue("LOADDAY")) != null && !handleTool.getParamById(dMap.getValue("LOADDAY")).equals("0")) {
                loadLength = loadLength + handleTool.getParamById(dMap.getValue("LOADDAY")) + "天";
            }
            /************************** 贷款期限End **************************/

            productMes.setLoadLength(loadLength); //贷款期限
            productMes.setBidday(handleTool.getParamById(dMap.getValue("BIDDAY"))); //投标时间(日)
            productMes.setChecktime(handleTool.getParamById(dMap.getValue("CHECKTIME"))); //审核时间
            productMes.setRepayment(handleTool.getParamById(dMap.getValue("REPAYMENT"))); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)

            /******************** 贷款金额部分Start ********************/
            productMes.setBidfee(handleTool.getParamById(dMap.getValue("BIDFEE"))); //投标金额
            productMes.setSplitcount(handleTool.getParamById(dMap.getValue("SPLITCOUNT"))); //拆分份数
            productMes.setLoadFee(handleTool.getParamById(dMap.getValue("LOADFEE"))); //贷款金额
            /********************* 贷款金额部分End *********************/

            productMes.setBiddingFee(handleTool.getParamById(dMap.getValue("YINVESTFEE"))); //已投标金额
            productMes.setWbiddingFee(handleTool.getParamById(dMap.getValue("WINVESTFEE"))); //未投标金额
            productMes.setBidProgress(handleTool.getParamById(dMap.getValue("INVESTPROGRESS"))); //投标进度
            productMes.setFullScale(handleTool.getParamById(dMap.getValue("FULLSCALE"))); //是否满标(0-是、1-否)
            productMes.setFullTime(handleTool.getParamById(dMap.getValue("FULLTIME"))); //满标时间
            productMes.setCounterfee(handleTool.getParamById(dMap.getValue("COUNTERFEE"))); //手续费
            productMes.setApplyMember(handleTool.getParamById(dMap.getValue("APPLYMEMBER"))); //申请人
            productMes.setApplycondition(handleTool.getParamById(dMap.getValue("APPLYCONDITION"))); //申请人条件
            productMes.setOnlinetime(handleTool.getParamById(dMap.getValue("ONLINETIME"))); //上线时间
            productMes.setSponsor(handleTool.getParamById(dMap.getValue("SPONSOR"))); //担保人
            productMes.setSort(handleTool.getParamById(dMap.getValue("SORT"))); //排序
            productMes.setApplyMemberName(handleTool.getParamById(dMap.getValue("TYPENAME"))); //类型名称
            productMes.setApplyMemberName(handleTool.getParamById(dMap.getValue("REALNAME"))); //申请人名称
            productMes.setCreatetime(handleTool.getParamById(dMap.getValue("CREATETIME")).substring(0, 10)); //创建时间
            productMes.setType(dMap.getValue("NEWSHARE")); //是否为新手专享(0-是、1-否)

            DMap dMapFile = sysFileDao.queryAllSysFile(productId); //附件部分
            if (dMapFile.getCount() > 0) {
                for (int i = 0; i < dMapFile.getCount(); i++) {
                    xdUrl = dMap.getValue("IMGURL", i); //相对路径
                    if (xdUrlPj == null || xdUrlPj.equals("")) {
                        xdUrlPj = xdUrl;
                    } else {
                        xdUrlPj = xdUrlPj + "," + xdUrl;
                    }
                }
                productMes.setImgUrl(xdUrlPj); //路径拼接(相对路径,绝对路径)
            }
            /******************************* 产品部分End *******************************/

            /************************* 会员部分Start *************************/
            memberInfoMes.setNickname(handleTool.getParamById(dMap.getValue("NICKNAME"))); //昵称
            memberInfoMes.setRealname(handleTool.getParamById(dMap.getValue("REALNAME"))); //真实姓名
            memberInfoMes.setIdcard(handleTool.getParamById(dMap.getValue("IDCARD"))); //身份证号
            memberInfoMes.setTelephone(handleTool.getParamById(dMap.getValue("TELEPHONE"))); //手机号
            memberInfoMes.setEmail(handleTool.getParamById(dMap.getValue("EMAIL"))); //邮箱地址
            memberInfoMes.setSex(handleTool.getParamById(dMap.getValue("SEX"))); //性别(0-男、1-女)
            memberInfoMes.setBirthday(handleTool.getParamById(dMap.getValue("BIRTHDAY"))); //出生日期
            memberInfoMes.setProvince(handleTool.getParamById(dMap.getValue("PROVINCE"))); //省id
            memberInfoMes.setCity(handleTool.getParamById(dMap.getValue("CITY"))); //城市id
            memberInfoMes.setDetailAddress(handleTool.getParamById(dMap.getValue("DETAIL_ADDRESS"))); //详细地址
            memberInfoMes.setDregree(handleTool.getParamById(dMap.getValue("DREGREE"))); //学历
            memberInfoMes.setSchool(handleTool.getParamById(dMap.getValue("SCHOOL"))); //毕业院校
            memberInfoMes.setMarryStatus(handleTool.getParamById(dMap.getValue("MARRY_STATUS"))); //婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶)
            memberInfoMes.setChildren(handleTool.getParamById(dMap.getValue("CHILDREN"))); //子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上)
            memberInfoMes.setMonthSalary(handleTool.getParamById(dMap.getValue("MONTH_SALARY"))); //月收入
            memberInfoMes.setSocialSecurity(handleTool.getParamById(dMap.getValue("SOCIAL_SECURITY"))); //社保情况(0-无、1-未缴满6个月、2-缴满6个月以上)
            memberInfoMes.setHouseCondition(handleTool.getParamById(dMap.getValue("HOUSE_CONDITION"))); //住房条件
            memberInfoMes.setCarFlg(handleTool.getParamById(dMap.getValue("CAR_FLG"))); //是否有车
            productMes.setMemberInfoMessageBean(memberInfoMes);
            /************************** 会员部分End **************************/
        }

        return productMes;
    }


    /**
     * 根据id获取投资产品信息
     *
     * @param productMessageBean
     * @return
     */
    @Override
    public InvestProductMessageBean queryInvestProductById(ProductMessageBean productMessageBean) {
        InvestProductMessageBean investProductMessageBean = new InvestProductMessageBean(); //产品
        MemberInfoMessageBean memberInfoMes = new MemberInfoMessageBean();
        DMap dMap = productDao.queryProductById(productMessageBean); //根据id获取产品信息
        String productId = ""; //产品id
        String fullTime = ""; //满标时间
        String loadLength = ""; //贷款期限
        Double pricipalInterest = 0.0d; //需还本息
        String xdUrl = ""; //相对路径
        String xdUrlPj = ""; //相对路径拼接
        String ybiddingFee = ""; //已投标金额
        String bidProgress = "0"; //投标进度

        if (dMap != null) {

            /****************************** 产品部分Start ******************************/
            productId = dMap.getValue("ID", 0); //主键
            investProductMessageBean.setId(dMap.getValue("ID", 0)); //主键
            investProductMessageBean.setTitle(dMap.getValue("TITLE", 0)); //题目
            investProductMessageBean.setCode(dMap.getValue("CODE", 0)); //借款标编码
            investProductMessageBean.setProductType(dMap.getValue("TYPE", 0)); //类型id
            investProductMessageBean.setCrowds(dMap.getValue("CROWDS", 0)); //适合人群
            investProductMessageBean.setCreditstart(dMap.getValue("CREDITSTART", 0)); //额度范围(起始)
            investProductMessageBean.setCreditend(dMap.getValue("CREDITEND", 0)); //额度范围(截止)
            investProductMessageBean.setLoanRate(dMap.getValue("LOANRATE", 0)); //贷款利率

            fullTime = dMap.getValue("FULLTIME", 0); //满标时间
            if (fullTime != null && !fullTime.equals("")){
                fullTime = fullTime.substring(0,10);
            }
            investProductMessageBean.setFullTime(fullTime);//满标日期
            investProductMessageBean.setCrowds(dMap.getValue("CROWDS", 0)); //适合人群
            investProductMessageBean.setSponsor(dMap.getValue("SPONSOR", 0)); //担保人
            investProductMessageBean.setBidProgress(dMap.getValue("INVESTPROGRESS", 0)); //投标进度
            investProductMessageBean.setWbiddingFee(dMap.getValue("WINVESTFEE", 0)); //项目可投金额
            investProductMessageBean.setFullScale(dMap.getValue("FULLSCALE", 0)); //满标状态
            investProductMessageBean.setShFlag(dMap.getValue("SHFLAG", 0)); //审核状态
            investProductMessageBean.setRepayTime(dMap.getValue("NEXTRECEIVETIME", 0)); //下期还款日
            investProductMessageBean.setSurplusDay(dMap.getValue("SURPLUSDAYS", 0)); //剩余天数
            investProductMessageBean.setInvestCount(dMap.getValue("INVESTCOUNT", 0)); //投资数量

            /******************** 贷款期限Start ********************/
            if (dMap.getValue("LOANYEAR",0) != null && !dMap.getValue("LOANYEAR",0).equals("0")) {
                loadLength = loadLength + dMap.getValue("LOANYEAR",0) + "年";
            }
            if (dMap.getValue("LOANMONTH",0) != null && !dMap.getValue("LOANMONTH",0).equals("0")) {
                loadLength = loadLength + dMap.getValue("LOANMONTH",0) + "个月";
            }
            if (dMap.getValue("LOADDAY",0) != null && !dMap.getValue("LOADDAY",0).equals("0")) {
                loadLength = loadLength + dMap.getValue("LOADDAY",0) + "天";
            }
            /********************* 贷款期限End *********************/

            investProductMessageBean.setLoanLength(loadLength); //贷款期限
            //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
            investProductMessageBean.setRepayment(dMap.getValue("REPAYMENT", 0));
            investProductMessageBean.setLoanFee(dMap.getValue("LOADFEE", 0)); //贷款金额

            /******************** 需还本息Start*********************/
            pricipalInterest = TypeTool.getDouble(dMap.getValue("LOADFEE",0))
                    + TypeTool.getDouble(dMap.getValue("INTEREST", 0));
            investProductMessageBean.setPricipalInterest(TypeTool.getString(pricipalInterest)); //需还本息
            /********************* 需还本息End *********************/

            investProductMessageBean.setBalance(dMap.getValue("BALANCE", 0)); //账户余额
            investProductMessageBean.setWaitingRepayPrice(dMap.getValue("WAITINGPRICE", 0)); //待还本息
            investProductMessageBean.setRepayFlg(dMap.getValue("REPAYFLG", 0)); //还款状态(0-还款中、1-借款中、2-已还清)
            /******************************* 产品部分End *******************************/

            /************************* 会员部分Start *************************/
            memberInfoMes.setNickname(dMap.getValue("NICKNAME",0)); //昵称
            memberInfoMes.setRealname(dMap.getValue("REALNAME",0)); //真实姓名
            memberInfoMes.setIdcard(dMap.getValue("IDCARD",0)); //身份证号
            memberInfoMes.setTelephone(dMap.getValue("TELEPHONE",0)); //手机号
            memberInfoMes.setEmail(dMap.getValue("EMAIL",0)); //邮箱地址
            memberInfoMes.setSex(dMap.getValue("SEX",0)); //性别(0-男、1-女)
            memberInfoMes.setBirthday(dMap.getValue("BIRTHDAY",0)); //出生日期
            memberInfoMes.setProvince(dMap.getValue("PROVINCE",0)); //省id
            memberInfoMes.setCity(dMap.getValue("CITY",0)); //城市id
            memberInfoMes.setDetailAddress(dMap.getValue("DETAIL_ADDRESS",0)); //详细地址
            memberInfoMes.setDregree(dMap.getValue("DREGREE",0)); //学历
            memberInfoMes.setSchool(dMap.getValue("SCHOOL",0)); //毕业院校
            memberInfoMes.setMarryStatus(dMap.getValue("MARRY_STATUS",0)); //婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶)
            memberInfoMes.setChildren(dMap.getValue("CHILDREN",0)); //子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上)
            memberInfoMes.setMonthSalary(dMap.getValue("MONTH_SALARY",0)); //月收入
            memberInfoMes.setSocialSecurity(dMap.getValue("SOCIAL_SECURITY",0)); //社保情况(0-无、1-未缴满6个月、2-缴满6个月以上)
            memberInfoMes.setHouseCondition(dMap.getValue("HOUSE_CONDITION",0)); //住房条件
            memberInfoMes.setCarFlg(dMap.getValue("CAR_FLG",0)); //是否有车
            investProductMessageBean.setMemberInfoMessageBean(memberInfoMes);
            /************************** 会员部分End **************************/
        }

        return investProductMessageBean;
    }


    /**
     * 设置产品状态(上架、下架)
     *
     * @param productMes
     * @return
     */
    @Override
    public ProductBusinessBean settingProduct(ProductMessageBean productMes) {
        String id = productMes.getId(); //主键
        String flag = productMes.getFlag(); //状态(0-上架、1-下架)
        ProductBusinessBean productBus = new ProductBusinessBean();
        ProductContent productContent = new ProductContent(); //产品
        productContent.setId(productMes.getId()); //主键
        productContent.setFlag(flag); //状态(0-上架、1-下架)
        productContent.setUpdatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //最后更新时间

        DaoResult resultProduct = productDao.updateProduct(productContent); //设置状态
        if (resultProduct.getCode() < 0) {
            productBus.setResult("error");
            productBus.setInfo(resultProduct.getErrText());
            return productBus;
        }

        productBus.setResult("success");
        productBus.setInfo("设置成功");
        return productBus;
    }


    /**
     * 设置产品审核状态
     *
     * @param productMes
     * @return
     */
    @Override
    public ProductBusinessBean setProductShFlag(ProductMessageBean productMes) throws Exception {
        //相应值接收
        String id = productMes.getId(); //主键
        String shFlag = productMes.getShflag(); //审核状态(0-审核通过、1-审核不通过、2-未审核)
        String shyj = productMes.getShyj(); //审核意见

        ProductBusinessBean productBus = new ProductBusinessBean();
        ProductContent productContent = new ProductContent();
        DMap dMapProduct = new DMap(); //产品部分
        HandleTool handleTool = new HandleTool();
        productContent.setId(id); //主键
        productContent.setShflag(shFlag); //审核状态(0-审核通过、1-审核不通过、2-未审核)
        productContent.setShyj(shyj); //审核意见
        productContent.setUpdateuser(productMes.getUpdateuser()); //更新人
        productContent.setUpdatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //最后更新时间

        DaoResult resultProduct = productDao.updateProduct(productContent); //产品审核

        if (resultProduct.getCode() < 0) { //审核失败
            productBus.setResult("error");
            productBus.setInfo(resultProduct.getErrText());
            return productBus;
        } else {
            ProductSh productSh = new ProductSh();
            productSh.execute(id);
        }

        productBus.setResult("success");
        productBus.setInfo("审核成功");

        return productBus;
    }


    /**
     * 修改产品
     *
     * @param productMes
     * @return
     */
    @Override
    public ProductBusinessBean updateProduct(ProductMessageBean productMes) {
        ProductBusinessBean productBus = new ProductBusinessBean();
        String productId = ""; //产品主键
        String sysFileId = ""; //附件主键
        String pathPj = ""; //附件地址(相对地址,绝对地址;相对地址,绝对地址)
        String[] arr = null; //用于存放附件地址
        int num = 0; //用于获取相对路径和绝对路径之间的逗号位置
        String xdPath = ""; //相对路径
        String jdPath = ""; //绝对路径

        /********** 实体类赋值(产品)Start **********/
        ProductContent productContent = new ProductContent(); //产品
        productId = productMes.getId();
        productContent.setId(productMes.getId()); //主键
        productContent.setTitle(productMes.getTitle()); //题目
        productContent.setCode(productMes.getCode()); //编码
        productContent.setType(productMes.getProductType()); //产品类型id
        productContent.setCrowds(productMes.getCrowds()); //适合人群
        productContent.setHighpoint(productMes.getHighpoint()); //亮点
        productContent.setCreditstart(TypeTool.getDouble(productMes.getCreditstart())); //额度范围(起始)
        productContent.setCreditend(TypeTool.getDouble(productMes.getCreditend())); //额度范围(截止)
        productContent.setLoanrate(TypeTool.getDouble(productMes.getLoanrate())); //贷款利率
        productContent.setLoanyear(TypeTool.getInt(productMes.getLoanyear())); //贷款期限(年)
        productContent.setLoanmonth(TypeTool.getInt(productMes.getLoanmonth())); //贷款期限(月)
        productContent.setLoadday(TypeTool.getInt(productMes.getLoadday())); //贷款期限(日)
        productContent.setBidday(productMes.getBidday()); //投标时间(日)
        productContent.setChecktime(TypeTool.getInt(productMes.getChecktime())); //审核时间
        productContent.setRepayment(productMes.getRepayment()); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        productContent.setBidfee(TypeTool.getDouble(productMes.getBidfee())); //投标金额
        productContent.setSplitcount(TypeTool.getInt(productMes.getSplitcount())); //拆分份数
        productContent.setCounterfee(productMes.getCounterfee()); //手续费
        productContent.setApplymember(productMes.getApplyMember()); //申请人
        productContent.setApplycondition(productMes.getApplycondition()); //申请人条件
        productContent.setOnlinetime(productMes.getOnlinetime()); //上线时间
        productContent.setSponsor(productMes.getSponsor()); //担保人
        productContent.setSort(TypeTool.getInt(productMes.getSort())); //排序
        productContent.setUpdateuser(productMes.getUpdateuser()); //更新人
        productContent.setUpdatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //更新时间
        /********** 实体类名称(产品)Start **********/

        DaoResult resultProduct = productDao.updateProduct(productContent); //修改产品
        if (resultProduct.getCode() < 0) { //失败
            productBus.setResult("error");
            productBus.setInfo(resultProduct.getErrText());
            return productBus;
        } else {
            /********** 附件部分Start **********/
            pathPj = productMes.getImgUrl(); //附件地址拼接
            if (pathPj != null && !pathPj.equals("")) {
                arr = pathPj.split(";");
                for (int i = 0; i < arr.length; i++) {
                    sysFileId = UUID.randomUUID().toString().replaceAll("\\-", ""); //附件主键
                    num = arr[i].indexOf(",");
                    xdPath = arr[i].substring(0, num); //相对路径
                    jdPath = arr[i].substring(num + 1); //绝对路径
                    SysFile sysFile = new SysFile();
                    SysFileBusinessBean sysFileBus = new SysFileBusinessBean(); //附件
                    sysFile.setId(sysFileId); //主键
                    sysFile.setGlid(productId); //关联id
                    sysFile.setImgurl(xdPath); //相对路径
                    sysFile.setImgurljd(jdPath); //绝对路径
                    sysFile.setType("1"); //类型(1-产品)
                    sysFile.setSort(0); //排序
                    sysFile.setCreateuser(productMes.getCreateuser()); //创建人
                    sysFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                    DaoResult resultSysFile = sysFileDao.saveProduct(sysFile);
                    if (resultSysFile.getCode() < 0) {
                        productBus.setResult("error");
                        productBus.setInfo(resultSysFile.getErrText());
                        return productBus;
                    }
                }
                /*********** 附件部分End ***********/
            }
        }
        productBus.setResult("success");
        productBus.setInfo("保存成功");
        return productBus;
    }

    /**
     * 获取产品数量(后台)
     *
     * @param productMes
     * @return
     */
    @Override
    public int getCountProductByPage(ProductMessageBean productMes) {
        DMap parm = productDao.getCountProductByPage(productMes); //查询产品
        int totalCount = parm.getCount(); //广告数量
        return totalCount;
    }


    /**
     * 产品列表(前台部分)
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<ProductMessageBean> getListBySome(String orderColumn, String orderType, String pageIndex, String pageSize, String typeId) {
        List<ProductMessageBean> productMessageBeans = new ArrayList<ProductMessageBean>();
        DMap parm = productDao.getListBySome(orderColumn, orderType, pageIndex, pageSize, typeId);
        int num = 1; //序号部分
        String loadLength = ""; //贷款期限
        Double loadFee = 0d; //贷款金额
        String updateTime = ""; //最后更新时间

        if (parm.getCount() > 0) {
            for (int i = 0; i < parm.getCount(); i++) {
                loadLength = ""; //贷款期限
                ProductMessageBean productMessageBean = new ProductMessageBean();
                productMessageBean.setId(parm.getValue("ID", i)); //主键
                productMessageBean.setTitle(parm.getValue("TITLE", i)); //题目
                productMessageBean.setFlag(parm.getValue("FLAG", i)); //状态(0-上架、1-下架)
                productMessageBean.setCrowds(parm.getValue("CROWDS", i)); //适合人群
                productMessageBean.setHighpoint(parm.getValue("HIGHPOINT", i)); //亮点
                productMessageBean.setCreditstart(parm.getValue("CREDITSTART", i)); //额度范围(起始)
                productMessageBean.setCreditend(parm.getValue("CREDITEND", i)); //额度范围(截止)
                productMessageBean.setLoanrate(parm.getValue("LOANRATE", i)); //贷款利率
                productMessageBean.setLoanyear(parm.getValue("LOANYEAR", i)); //贷款期限(年)
                productMessageBean.setLoanmonth(parm.getValue("LOANMONTH", i)); //贷款期限(月)
                productMessageBean.setLoadday(parm.getValue("LOADDAY", i)); //贷款期限(日)
                if (parm.getValue("LOANYEAR", i) != null && !parm.getValue("LOANYEAR", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOANYEAR", i) + "年";
                }
                if (parm.getValue("LOANMONTH", i) != null && !parm.getValue("LOANMONTH", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOANMONTH", i) + "个月";
                }
                if (parm.getValue("LOADDAY", i) != null && !parm.getValue("LOADDAY", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOADDAY", i) + "天";
                }
                productMessageBean.setLoadLength(loadLength); //贷款期限
                productMessageBean.setBidday(parm.getValue("BIDDAY", i)); //投标时间
                productMessageBean.setChecktime(parm.getValue("CHECKTIME", i)); //审核时间
                productMessageBean.setRepayment(parm.getValue("REPAYMENT", i)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
                productMessageBean.setBidfee(parm.getValue("BIDFEE", i)); //投标金额
                productMessageBean.setSplitcount(parm.getValue("SPLITCOUNT", i)); //拆分份数
                if (parm.getValue("BIDFEE", i) != null && !parm.getValue("BIDFEE", i).equals("0") && parm.getValue("SPLITCOUNT", i) != null && !parm.getValue("SPLITCOUNT", i).equals("0")) {
                    loadFee = Double.parseDouble(parm.getValue("BIDFEE", i)) * Double.parseDouble(parm.getValue("SPLITCOUNT", i));
                    productMessageBean.setLoadFee(TypeTool.getString(loadFee));
                }
                productMessageBean.setCounterfee(parm.getValue("COUNTERFEE", i)); //手续费
                productMessageBean.setApplycondition(parm.getValue("APPLYCONDITION", i)); //申请人条件
                productMessageBean.setOnlinetime(parm.getValue("ONLINETIME", i)); //上线时间
                productMessageBean.setSponsor(parm.getValue("SPONSOR", i)); //担保人
                productMessageBean.setSort(parm.getValue("SORT", i)); //排序
                productMessageBean.setCreateuser(parm.getValue("CREATEUSER", i)); //创建用户
                productMessageBean.setCreatetime(parm.getValue("CREATETIME", i)); //创建时间
                productMessageBean.setUpdateuser(parm.getValue("UPDATEUSER", i)); //更新用户
                productMessageBean.setProductType(parm.getValue("TYPE", i));
                updateTime = parm.getValue("UPDATETIME", i); //最后更新时间
                if (updateTime != null && !updateTime.equals("")) {
                    updateTime = updateTime.substring(0, 10);
                }
                productMessageBean.setUpdatetime(updateTime); //最后更新时间
                productMessageBean.setNum(TypeTool.getString(num)); //序号
                num++;
                productMessageBeans.add(productMessageBean);
            }
        }
        return productMessageBeans;
    }

    /**
     * 查询产品列表 带分页 by cuibin
     */
    @Override
    public List<ProductMessageBean> queryProductListByPage(ProductMessageBean productMessageBean) {
        List<ProductMessageBean> productMessageBeans = new ArrayList<ProductMessageBean>();
        DMap parm = productDao.queryProductListByPage(productMessageBean);
        int num = 1; //序号部分
        String loadLength = ""; //贷款期限
        Double loadFee = 0d; //贷款金额
        String updateTime = ""; //最后更新时间

        if (parm.getCount() > 0) {
            for (int i = 0; i < parm.getCount(); i++) {
                loadLength = ""; //贷款期限
                ProductMessageBean temp = new ProductMessageBean();
                temp.setId(parm.getValue("ID", i)); //主键
                temp.setTitle(parm.getValue("TITLE", i)); //题目
                temp.setFlag(parm.getValue("FLAG", i)); //状态(0-上架、1-下架)
                temp.setCrowds(parm.getValue("CROWDS", i)); //适合人群
                temp.setHighpoint(parm.getValue("HIGHPOINT", i)); //亮点
                temp.setCreditstart(parm.getValue("CREDITSTART", i)); //额度范围(起始)
                temp.setCreditend(parm.getValue("CREDITEND", i)); //额度范围(截止)
                temp.setLoanrate(parm.getValue("LOANRATE", i)); //贷款利率
                temp.setLoanyear(parm.getValue("LOANYEAR", i)); //贷款期限(年)
                temp.setLoanmonth(parm.getValue("LOANMONTH", i)); //贷款期限(月)
                temp.setLoadday(parm.getValue("LOADDAY", i)); //贷款期限(日)
                if (parm.getValue("LOANYEAR", i) != null && !parm.getValue("LOANYEAR", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOANYEAR", i) + "年";
                }
                if (parm.getValue("LOANMONTH", i) != null && !parm.getValue("LOANMONTH", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOANMONTH", i) + "个月";
                }
                if (parm.getValue("LOADDAY", i) != null && !parm.getValue("LOADDAY", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOADDAY", i) + "天";
                }
                temp.setLoadLength(loadLength); //贷款期限
                temp.setBidday(parm.getValue("BIDDAY", i)); //投标时间
                temp.setChecktime(parm.getValue("CHECKTIME", i)); //审核时间
                temp.setRepayment(parm.getValue("REPAYMENT", i)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
                temp.setBidfee(parm.getValue("BIDFEE", i)); //投标金额
                temp.setSplitcount(parm.getValue("SPLITCOUNT", i)); //拆分份数
                if (parm.getValue("BIDFEE", i) != null && !parm.getValue("BIDFEE", i).equals("0") && parm.getValue("SPLITCOUNT", i) != null && !parm.getValue("SPLITCOUNT", i).equals("0")) {
                    loadFee = Double.parseDouble(parm.getValue("BIDFEE", i)) * Double.parseDouble(parm.getValue("SPLITCOUNT", i));
                    temp.setLoadFee(TypeTool.getString(loadFee));
                }
                temp.setCounterfee(parm.getValue("COUNTERFEE", i)); //手续费
                temp.setApplycondition(parm.getValue("APPLYCONDITION", i)); //申请人条件
                temp.setOnlinetime(parm.getValue("ONLINETIME", i)); //上线时间
                temp.setSponsor(parm.getValue("SPONSOR", i)); //担保人
                temp.setSort(parm.getValue("SORT", i)); //排序
                temp.setCreateuser(parm.getValue("CREATEUSER", i)); //创建用户
                temp.setCreatetime(parm.getValue("CREATETIME", i)); //创建时间
                temp.setUpdateuser(parm.getValue("UPDATEUSER", i)); //更新用户
                temp.setProductType(parm.getValue("TYPE", i));
                updateTime = parm.getValue("UPDATETIME", i); //最后更新时间
                if (updateTime != null && !updateTime.equals("")) {
                    updateTime = updateTime.substring(0, 10);
                }
                temp.setUpdatetime(updateTime); //最后更新时间
                temp.setNum(TypeTool.getString(num)); //序号
                num++;
                productMessageBeans.add(temp);
            }
        }
        return productMessageBeans;
    }

    /**
     * 查询产品列表 总数 by cuibin
     */
    @Override
    public String queryProductListByPageCount(ProductMessageBean productMessageBean) {
        DMap dMap = productDao.queryProductListByPageCount(productMessageBean);
        return dMap.getValue("COUNT", 0);
    }


    /**
     * 项目登机前查询
     *
     * @param productHuanXunMes
     * @return
     */
    @Override
    public List<ProductHuanXunMessageBean> queryProductDjInfo(ProductHuanXunMessageBean productHuanXunMes) {
        DMap dMap = productDao.queryProductDjInfo(productHuanXunMes);
        List<ProductHuanXunMessageBean> listProductHuanXunMes = new ArrayList<ProductHuanXunMessageBean>();
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                ProductHuanXunMessageBean productHuanXunMessageBean = new ProductHuanXunMessageBean();
                productHuanXunMessageBean.setId(dMap.getValue("ID", i)); //主键
                productHuanXunMessageBean.setCode(dMap.getValue("CODE", i)); //项目ID号(项目编号)
                productHuanXunMessageBean.setTitle(dMap.getValue("TITLE", i)); //项目名称
                productHuanXunMessageBean.setBidFee(dMap.getValue("BIDFEE", i)); //最小投标金额
                productHuanXunMessageBean.setSplitCount(dMap.getValue("SPLITCOUNT", i)); //最大拆分份数
                productHuanXunMessageBean.setLoanRate(dMap.getValue("LOANRATE", i)); //贷款利率
                productHuanXunMessageBean.setLoanYear(dMap.getValue("LOANYEAR", i)); //贷款期限(年)
                productHuanXunMessageBean.setLoanMonth(dMap.getValue("LOANMONTH", i)); //贷款期限(月)
                productHuanXunMessageBean.setLoadDay(dMap.getValue("LOADDAY", i)); //贷款期限(日)
                productHuanXunMessageBean.setApplyMember(dMap.getValue("APPLYMEMBER", i)); //融资人id
                productHuanXunMessageBean.setRealName(dMap.getValue("REALNAME", i)); //融资人姓名
                productHuanXunMessageBean.setIdCard(dMap.getValue("IDCARD", i)); //融资方身份证号
                listProductHuanXunMes.add(productHuanXunMessageBean);
            }
        }
        return listProductHuanXunMes;
    }


    /**
     * 查询产品信息(前台部分)
     *
     * @param orderColumn
     * @param orderType
     * @param productMes
     * @return
     */
    @Override
    public List<ProductMessageBean> getFrontList(String orderColumn, String orderType, ProductMessageBean productMes) {
        List<ProductMessageBean> productMessageBeans = new ArrayList<ProductMessageBean>();
        DMap parm = productDao.getFrontList(orderColumn,orderType,productMes); //产品集合
        String loadLength = ""; //贷款期限
        Double loadFee = 0d; //贷款金额
        String updateTime = ""; //最后更新时间

        if (parm.getCount() > 0) {
            for (int i = 0; i < parm.getCount(); i++) {
                loadLength = ""; //贷款期限
                ProductMessageBean productMessageBean = new ProductMessageBean();
                productMessageBean.setId(parm.getValue("ID", i)); //主键
                productMessageBean.setTitle(parm.getValue("TITLE", i)); //题目
                productMessageBean.setFlag(parm.getValue("FLAG", i)); //状态(0-上架、1-下架)
                productMessageBean.setCrowds(parm.getValue("CROWDS", i)); //适合人群
                productMessageBean.setHighpoint(parm.getValue("HIGHPOINT", i)); //亮点
                productMessageBean.setCreditstart(parm.getValue("CREDITSTART", i)); //额度范围(起始)
                productMessageBean.setCreditend(parm.getValue("CREDITEND", i)); //额度范围(截止)
                productMessageBean.setLoanrate(parm.getValue("LOANRATE", i)); //贷款利率
                productMessageBean.setLoanyear(parm.getValue("LOANYEAR", i)); //贷款期限(年)
                productMessageBean.setLoanmonth(parm.getValue("LOANMONTH", i)); //贷款期限(月)
                productMessageBean.setLoadday(parm.getValue("LOADDAY", i)); //贷款期限(日)

                /******************** 贷款期限Start ********************/
                if (parm.getValue("LOANYEAR", i) != null && !parm.getValue("LOANYEAR", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOANYEAR", i) + "年";
                }
                if (parm.getValue("LOANMONTH", i) != null && !parm.getValue("LOANMONTH", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOANMONTH", i) + "个月";
                }
                if (parm.getValue("LOADDAY", i) != null && !parm.getValue("LOADDAY", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOADDAY", i) + "天";
                }
                /********************* 贷款期限End *********************/

                productMessageBean.setLoadLength(loadLength); //贷款期限
                productMessageBean.setBidday(parm.getValue("BIDDAY", i)); //投标时间
                productMessageBean.setChecktime(parm.getValue("CHECKTIME", i)); //审核时间
                productMessageBean.setRepayment(parm.getValue("REPAYMENT", i)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
                productMessageBean.setBidfee(parm.getValue("BIDFEE", i)); //投标金额
                productMessageBean.setSplitcount(parm.getValue("SPLITCOUNT", i)); //拆分份数
                productMessageBean.setLoadFee(parm.getValue("LOADFEE", i)); //借款金额
                productMessageBean.setFullScale(parm.getValue("FULLSCALE", i)); //是否满标(0-是、1-否)
                productMessageBean.setBidProgress(parm.getValue("INVESTPROGRESS", i)); //投资进度
                productMessageBean.setCounterfee(parm.getValue("COUNTERFEE", i)); //手续费
                productMessageBean.setApplycondition(parm.getValue("APPLYCONDITION", i)); //申请人条件
                productMessageBean.setOnlinetime(parm.getValue("ONLINETIME", i)); //上线时间
                productMessageBean.setSponsor(parm.getValue("SPONSOR", i)); //担保人
                productMessageBean.setSort(parm.getValue("SORT", i)); //排序
                productMessageBean.setCreateuser(parm.getValue("CREATEUSER", i)); //创建用户
                productMessageBean.setCreatetime(parm.getValue("CREATETIME", i)); //创建时间
                productMessageBean.setUpdateuser(parm.getValue("UPDATEUSER", i)); //更新用户

                /******************** 最后更新时间Start ********************/
                updateTime = parm.getValue("UPDATETIME", i);
                if (updateTime != null && !updateTime.equals("")) {
                    updateTime = updateTime.substring(0, 10);
                }
                productMessageBean.setUpdatetime(updateTime);
                /********************* 最后更新时间End *********************/

                productMessageBeans.add(productMessageBean);
            }
        }
        return productMessageBeans;
    }


    /**
     * 查询推荐项目(前台)
     *
     * @return
     */
    @Override
    public ProductRecommendMessageBean getRecommend() {
        ProductRecommendMessageBean productRecommendMes = new ProductRecommendMessageBean(); //推荐产品
        String loanLength = ""; //贷款时长
        String loanYear = ""; //贷款期限(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(天)
        String loanRate = ""; //贷款利率
        String repayment = ""; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        String repaymentName = ""; //还款方式名称
        String loanPrice = "0"; //借款金额
        String surplusRepayPrice = "0"; //剩余还款
        DMap dMap = productDao.getRecommend(); //推荐产品部分
        if (dMap != null && dMap.getCount() > 0){
            productRecommendMes.setId(dMap.getValue("ID",0)); //主键
            productRecommendMes.setTitle(dMap.getValue("TITLE",0)); //题目

            loanYear = dMap.getValue("LOANYEAR",0); //贷款期限(年)
            loanMonth = dMap.getValue("LOANMONTH",0); //贷款期限(月)
            loanDay = dMap.getValue("LOADDAY",0); //贷款期限(天)
            if (loanYear != null && !loanYear.equals("")){
                if (!loanYear.equals("0")){
                    loanLength = loanLength + loanYear + "年";
                }
            }
            if (loanMonth != null && !loanMonth.equals("")){
                if (!loanMonth.equals("0")){
                    loanLength = loanLength + loanMonth + "个月";
                }
            }
            if (loanDay != null && !loanDay.equals("")){
                if (!loanDay.equals("0")){
                    loanLength = loanLength + loanDay + "天";
                }
            }
            productRecommendMes.setLoadLenth(loanLength); //贷款时长

            loanRate = dMap.getValue("LOANRATE",0); //贷款利率
            if (loanRate != null && !loanRate.equals("")){
                loanRate = loanRate + "%";
            }
            productRecommendMes.setLoanRate(loanRate);

            repayment = dMap.getValue("REPAYMENT",0); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
            if (repayment != null && !repayment.equals("")){
                if (repayment.equals("0")){
                    repaymentName = "按月还款、等额本息";
                }else if (repayment.equals("1")){
                    repaymentName = "按月付息、到期还本";
                }else if (repayment.equals("2")){
                    repaymentName = "一次性还款";
                }
            }
            productRecommendMes.setRepaymentName(repaymentName); //还款方式名称

            loanPrice = dMap.getValue("LOADFEE",0); //借款金额
            if (loanPrice != null && !loanPrice.equals("")){
                loanPrice = loanPrice + "元";
            }
            productRecommendMes.setLoanPrice(loanPrice);

            productRecommendMes.setInvestProgress(dMap.getValue("INVESTPROGRESS",0)); //投资进度

            surplusRepayPrice = dMap.getValue("WAITINGPRICE",0); //剩余还款
            if (surplusRepayPrice != null && !surplusRepayPrice.equals("")){
                surplusRepayPrice = surplusRepayPrice + "元";
            }
            productRecommendMes.setSurplusRepayPrice(surplusRepayPrice);

            productRecommendMes.setFullScale(dMap.getValue("FULLSCALE",0)); //是否满标(0-是、1-否)
        }
        return productRecommendMes;
    }


    /**
     * 按照类别获取产品
     */
    @Override
    public List<ProductMessageBean> queryProductByType(String productType) {
        List<ProductMessageBean> productMessageBeans = new ArrayList<ProductMessageBean>();
        DMap parm = productDao.queryProductByType(productType);
        Double loadFee = 0.0d; //贷款金额
        String num = "1"; //序号
        String loadLength = ""; //贷款期限

        if (parm.getCount() > 0) {
            for (int i = 0; i < parm.getCount(); i++) {
                ProductMessageBean productMessageBean = new ProductMessageBean();
                productMessageBean.setId(parm.getValue("ID", i)); //主键
                productMessageBean.setTitle(parm.getValue("TITLE", i)); //标题
                productMessageBean.setCreatetime(parm.getValue("CREATETIME", i)); //创建时间
                productMessageBean.setLoanrate(parm.getValue("LOANRATE", i)); //贷款利率
                productMessageBean.setCreditstart(parm.getValue("CREDITSTART", i)); //额度范围(起始)
                productMessageBean.setCreditend(parm.getValue("CREDITEND", i)); //额度范围(截止)
                productMessageBean.setFlag(parm.getValue("FLAG", i)); //状态(0-上架、1-下架)
                productMessageBean.setShflag(parm.getValue("SHFLAG", i)); //审核状态(0-审核通过、1-审核不通过、2-未审核)
                productMessageBean.setShyj(parm.getValue("SHYJ", i)); //审核意见
                productMessageBean.setSort(parm.getValue("SORT", i)); //排序
                productMessageBean.setBidfee(parm.getValue("BIDFEE", i)); //投标金额
                productMessageBean.setSplitcount(parm.getValue("SPLITCOUNT", i)); //拆分份数
                productMessageBean.setLoadFee(parm.getValue("LOADFEE", i)); //借款金额
                productMessageBean.setBidProgress(parm.getValue("INVESTPROGRESS", i)); //投资进度
                productMessageBean.setFullScale(parm.getValue("FULLSCALE", i)); //是否满编(0-满标、1-未满标)
                loadLength = "";
                if (parm.getValue("LOANYEAR", i) != null && !parm.getValue("LOANYEAR", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOANYEAR", i) + "年";
                }
                if (parm.getValue("LOANMONTH", i) != null && !parm.getValue("LOANMONTH", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOANMONTH", i) + "个月";
                }
                if (parm.getValue("LOADDAY", i) != null && !parm.getValue("LOADDAY", i).equals("0")) {
                    loadLength = loadLength + parm.getValue("LOADDAY", i) + "天";
                }
                productMessageBean.setLoadLength(loadLength); //贷款时长

                /********** 序号部分Start **********/
                productMessageBean.setNum(num); //序号
                num = String.valueOf(Integer.parseInt(num) + 1);
                productMessageBeans.add(productMessageBean);
                /*********** 序号部分End ***********/
            }
        }
        return productMessageBeans;
    }


    /**
     * 获取交易金额总和
     *
     * @param productMes
     * @return
     */
    @Override
    public ProductMessageBean getTradePriceSum(ProductMessageBean productMes) {
        ProductMessageBean productMessageBean = new ProductMessageBean();
        HandleTool handleTool = new HandleTool();
        DMap parm = productDao.getTradePriceSum(productMes); //获取交易金额总和
        productMessageBean.setLoadFeeSum(handleTool.getParamById(parm.getValue("LOADFEESUM"))); //交易总额
        productMessageBean.setInterestSum(handleTool.getParamById(parm.getValue("INTERESTSUM"))); //赚取总额
        productMessageBean.setBiddingFee(handleTool.getParamById(parm.getValue("YINVESTFEE"))); //已投资金额
        productMessageBean.setWbiddingFee(handleTool.getParamById(parm.getValue("WINVESTFEE"))); //未投资金额
        return productMessageBean;
    }

    /**
     * 将要到期借款标数量 by cuibin
     */
    @Override
    public int getCountDueDate() {
        return productDao.getCountDueDate().getCount();
    }


    /**
     * 获取推荐项目(App端)
     *
     * @return
     */
    @Override
    public AppHomeMessageBean getAppRecommentProduct() {
        AppHomeMessageBean appHomeMessageBean = new AppHomeMessageBean();
        DMap dMap = productDao.getRecommend();
        String loanYear = ""; //贷款期限(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(天)
        String loanLength = ""; //贷款时长
        if (dMap != null && dMap.getCount() > 0) {
            appHomeMessageBean.setProductId(dMap.getValue("ID", 0)); //产品id
            appHomeMessageBean.setLoanPrice(dMap.getValue("LOADFEE", 0)); //贷款金额

            loanYear = dMap.getValue("LOANYEAR", 0); //贷款期限(年)
            loanMonth = dMap.getValue("LOANMONTH", 0); //贷款期限(月)
            loanDay = dMap.getValue("LOADDAY", 0); //贷款期限(天)
            if (loanYear != null && !loanYear.equals("")) { //年份
                if (!loanYear.equals("0")) {
                    loanLength = loanLength + loanYear + "年";
                }
            }
            if (loanMonth != null && !loanMonth.equals("")) { //月份
                if (!loanMonth.equals("0")) {
                    loanLength = loanLength + loanMonth + "月";
                }
            }
            if (loanDay != null && !loanDay.equals("")) {
                if (!loanDay.equals("0")) {
                    loanLength = loanLength + loanDay + "天";
                }
            }

            appHomeMessageBean.setLoanTimeLength(loanLength); //贷款总时长
            appHomeMessageBean.setLoanRate(dMap.getValue("LOANRATE", 0)); //贷款利率
        }
        return appHomeMessageBean;
    }


    /**
     * 获取产品列表(App部分)
     *
     * @param appProductMes
     * @return
     */
    @Override
    public List<AppProductMessageBean> getAppProductList(AppProductMessageBean appProductMes) {
        List<AppProductMessageBean> listAppProductMes = new ArrayList<AppProductMessageBean>();
        DMap dMap = productDao.getAppProductList(appProductMes);
        String loanYear = ""; //贷款期限(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(日)
        String loanLength = ""; //贷款时长
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                AppProductMessageBean appProductMessageBean = new AppProductMessageBean();
                appProductMessageBean.setId(dMap.getValue("ID", i)); //主键
                appProductMessageBean.setTypeId(dMap.getValue("TYPE", i)); //产品类型
                appProductMessageBean.setFlag(dMap.getValue("FLAG", i)); //状态(0-上架、1-下架)
                appProductMessageBean.setTitle(dMap.getValue("TITLE", i)); //题目
                appProductMessageBean.setRepayment(dMap.getValue("REPAYMENT", i)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
                appProductMessageBean.setLoadFee(dMap.getValue("LOADFEE", i)); //贷款金额
                appProductMessageBean.setLoanRate(dMap.getValue("LOANRATE", i)); //贷款利率
                appProductMessageBean.setFullScale(dMap.getValue("FULLSCALE", i)); //是否满标(0-是、1-否)
                loanYear = dMap.getValue("LOANYEAR", i); //贷款期限(年)
                loanMonth = dMap.getValue("LOANMONTH", i); //贷款期限(月)
                loanDay = dMap.getValue("LOADDAY", i); //贷款期限(日)
                loanLength = ""; //贷款时长
                if (loanYear != null && !loanYear.equals("")) {
                    if (!loanYear.equals("0")) {
                        loanLength = "<span class=\"ft_zhi\">" + loanYear + "</span>年";
                    }
                }
                if (loanMonth != null && !loanMonth.equals("")) {
                    if (!loanMonth.equals("0")) {
                        loanLength = "<span class=\"ft_zhi\">" + loanMonth + "</span>个月";
                    }
                }
                if (loanDay != null && !loanDay.equals("")) {
                    if (!loanDay.equals("0")) {
                        loanLength = "<span class=\"ft_zhi\">" + loanDay + "</span>日";
                    }
                }
                appProductMessageBean.setLoanLength(loanLength); //贷款时长
                listAppProductMes.add(appProductMessageBean);
            }
        }
        return listAppProductMes;
    }

    /**
     * 根据产品id获取产品详情(App部分)
     *
     * @param productId
     * @return
     */
    @Override
    public AppProductDetailMessageBean getProductDetail(String productId) {
        AppProductDetailMessageBean appProductDetailMessageBean = new AppProductDetailMessageBean();
        DMap dMap = productDao.getProductDetail(productId);
        String loanYear = ""; //贷款期限(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(天)
        String loanLength = ""; //贷款时长
        if (dMap != null) {
            appProductDetailMessageBean.setId(dMap.getValue("ID", 0)); //主键
            appProductDetailMessageBean.setTitle(dMap.getValue("TITLE", 0)); //题目
            appProductDetailMessageBean.setLoanRate(dMap.getValue("LOANRATE", 0)); //贷款利率
            appProductDetailMessageBean.setSurplusPrice(dMap.getValue("SURPLUSPRICE", 0)); //剩余投资金额
            appProductDetailMessageBean.setInvestProgress(dMap.getValue("INVESTPROGRESS", 0)); //产品投资进度

            /*************** 贷款时长Start ***************/
            loanYear = dMap.getValue("LOANYEAR", 0); //贷款期限(年)
            loanMonth = dMap.getValue("LOANMONTH", 0); //贷款期限(月)
            loanDay = dMap.getValue("LOADDAY", 0); //贷款期限(日)
            loanLength = ""; //贷款时长
            if (loanYear != null && !loanYear.equals("")) {
                if (!loanYear.equals("0")) {
                    loanLength = loanLength + "<b style=\"color:#FF2021;\">" + loanYear + "</b>年";
                }
            }
            if (loanMonth != null && !loanMonth.equals("")) {
                if (!loanMonth.equals("0")) {
                    loanLength = loanLength + "<b style=\"color:#FF2021;\">" + loanMonth + "</b>个月";
                }
            }
            if (loanDay != null && !loanDay.equals("")) {
                if (!loanDay.equals("0")) {
                    loanLength = loanLength + "<b style=\"color:#FF2021;\">" + loanDay + "</b>天";
                }
            }
            /**************** 贷款时长End ****************/

            appProductDetailMessageBean.setLoanLength(loanLength); //贷款时长
            appProductDetailMessageBean.setFullScale(dMap.getValue("FULLSCALE", 0)); //是否满标
            appProductDetailMessageBean.setCode(dMap.getValue("CODE", 0)); //编号
            appProductDetailMessageBean.setRepayPrice(dMap.getValue("REPAYPRICE", 0)); //需还本息
            appProductDetailMessageBean.setFullTime(dMap.getValue("FULLTIME", 0)); //发标日期
            appProductDetailMessageBean.setLoanFee(dMap.getValue("LOANFEE", 0)); //借款金额
            appProductDetailMessageBean.setRepayment(dMap.getValue("REPAYMENT", 0)); //还款方式
            appProductDetailMessageBean.setProjectNo(dMap.getValue("PROJECTNO", 0)); //项目ID号
        }
        return appProductDetailMessageBean;
    }


    /**
     * 获取推荐产品(微信部分) by cuibin
     */
    @Override
    public List<WeixinProductMessageBean> getWeixinProductList(WeixinProductMessageBean weixinProductMes) {

        DMap dMap = productDao.getWeixinProductList(weixinProductMes);
        String loanYear = ""; //贷款期限(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(日)
        String loanLength = ""; //贷款时长
        List<WeixinProductMessageBean> list = new ArrayList<WeixinProductMessageBean>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                WeixinProductMessageBean temp = new WeixinProductMessageBean();
                temp.setId(dMap.getValue("ID", i)); //主键
                temp.setTypeId(dMap.getValue("TYPE", i)); //产品类型
                temp.setFlag(dMap.getValue("FLAG", i)); //状态(0-上架、1-下架)
                temp.setTitle(dMap.getValue("TITLE", i)); //题目
                temp.setRepayment(dMap.getValue("REPAYMENT", i)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
                temp.setLoadFee(dMap.getValue("LOADFEE", i)); //贷款金额
                temp.setLoanRate(dMap.getValue("LOANRATE", i)); //贷款利率
                loanYear = dMap.getValue("LOANYEAR", i); //贷款期限(年)
                loanMonth = dMap.getValue("LOANMONTH", i); //贷款期限(月)
                loanDay = dMap.getValue("LOADDAY", i); //贷款期限(日)
                loanLength = ""; //贷款时长
                if (loanYear != null && !loanYear.equals("")) {
                    if (!loanYear.equals("0")) {
                        loanLength = "<span class=\"ft_zhi\">" + loanYear + "</span>年";
                    }
                }
                if (loanMonth != null && !loanMonth.equals("")) {
                    if (!loanMonth.equals("0")) {
                        loanLength = "<span class=\"ft_zhi\">" + loanMonth + "</span>个月";
                    }
                }
                if (loanDay != null && !loanDay.equals("")) {
                    if (!loanDay.equals("0")) {
                        loanLength = "<span class=\"ft_zhi\">" + loanDay + "</span>日";
                    }
                }
                temp.setLoanLength(loanLength); //贷款时长
                list.add(temp);
            }
        }
        return list;
    }


    /**
     * 查看产品详情(微信部分) by cuibin
     */
    @Override
    public WeixinProductMessageBean productDetails(WeixinProductMessageBean weixinProductMes) {

        WeixinProductMessageBean weixinProductMessageBean = new WeixinProductMessageBean();
        DMap dMap = productDao.productDetails(weixinProductMes);
        String loanYear = ""; //贷款期限(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(天)
        String loanLength = ""; //贷款时长
        if (dMap != null) {
            weixinProductMessageBean.setId(dMap.getValue("ID", 0)); //主键
            weixinProductMessageBean.setTitle(dMap.getValue("TITLE", 0)); //题目
            weixinProductMessageBean.setLoanRate(dMap.getValue("LOANRATE", 0)); //贷款利率
            weixinProductMessageBean.setSurplusPrice(dMap.getValue("SURPLUSPRICE", 0)); //剩余投资金额
            weixinProductMessageBean.setInvestProgress(dMap.getValue("INVESTPROGRESS", 0)); //产品投资进度
            loanYear = dMap.getValue("LOANYEAR", 0); //贷款期限(年)
            loanMonth = dMap.getValue("LOANMONTH", 0); //贷款期限(月)
            loanDay = dMap.getValue("LOADDAY", 0); //贷款期限(日)
            loanLength = ""; //贷款时长
            if (loanYear != null && !loanYear.equals("")) {
                if (!loanYear.equals("0")) {
                    loanLength = loanLength + "<b style=\"color:#FF2021;\">" + loanYear + "</b>年";
                }
            }
            if (loanMonth != null && !loanMonth.equals("")) {
                if (!loanMonth.equals("0")) {
                    loanLength = loanLength + "<b style=\"color:#FF2021;\">" + loanMonth + "</b>个月";
                }
            }
            if (loanDay != null && !loanDay.equals("")) {
                if (!loanDay.equals("0")) {
                    loanLength = loanLength + "<b style=\"color:#FF2021;\">" + loanDay + "</b>天";
                }
            }
            weixinProductMessageBean.setLoanLength(loanLength); //贷款时长
            weixinProductMessageBean.setFullScale(dMap.getValue("FULLSCALE", 0)); //是否满标
            weixinProductMessageBean.setCode(dMap.getValue("CODE", 0)); //编号
            weixinProductMessageBean.setRepayPrice(dMap.getValue("REPAYPRICE", 0)); //需还本息
            weixinProductMessageBean.setFullTime(dMap.getValue("FULLTIME", 0)); //发标日期
            weixinProductMessageBean.setLoanFee(dMap.getValue("LOANFEE", 0)); //借款金额
            weixinProductMessageBean.setRepayment(dMap.getValue("REPAYMENT", 0)); //还款方式
        }
        return weixinProductMessageBean;
    }

    /**
     * 查看借款信息(微信部分) by cuibin
     */
    @Override
    public WeixinProductMemberMes openProductBorrow(WeixinProductMemberMes weixinProductMemberMes) {

        DMap dMap = productDao.openProductBorrow(weixinProductMemberMes);
        WeixinProductMemberMes temp = new WeixinProductMemberMes();
        if (dMap.getCount() > 0) {
            temp.setId(dMap.getValue("ID", 0));//主键
            temp.setApplymember(dMap.getValue("APPLYMEMBER", 0));//申请人
            temp.setFullscale(dMap.getValue("FULLSCALE", 0));//是否满标(0-是、1-否)
            temp.setShflag(dMap.getValue("SHFLAG", 0));//审核状态(0-审核通过、1-审核未通过、2-未审核)
            temp.setLoadfee(dMap.getValue("LOADFEE", 0));//
            temp.setRealname(dMap.getValue("REALNAME", 0));//真实姓名
            temp.setSex(dMap.getValue("SEX", 0));//性别(0-男、1-女)
            temp.setDregree(dMap.getValue("DREGREE", 0));//学历(0-小学、1-初中、2-高中、3-大专、4-本科、5-硕士、6-博士、7-博士后、8-其他)
            temp.setTelephone(dMap.getValue("TELEPHONE", 0));//手机号
            temp.setChildren(dMap.getValue("CHILDREN", 0));//子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上)
            temp.setCarFlg(dMap.getValue("CAR_FLG", 0));//是否有车(0-有、1-无)
            temp.setSocialSecurity(dMap.getValue("SOCIAL_SECURITY", 0));//社保情况(0-无、1-未缴满6个月、2-缴满6个月以上)
            temp.setMarryStatus(dMap.getValue("MARRY_STATUS", 0));//婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶)
            temp.setHouseCondition(dMap.getValue("HOUSE_CONDITION", 0));//住房条件(0-有商品房(无贷款)、1-有商品房(有贷款)、2-有其他(非商品)房、3-与父母同住、4-租房)
            temp.setMonthSalary(dMap.getValue("MONTH_SALARY", 0));//月收入(0-2000元以下、1-2000元-5000元、2-5000元-10000元、3-10000元以上)
            temp.setEmail(dMap.getValue("EMAIL", 0));//邮箱地址
            temp.setDetailAddress(dMap.getValue("DETAIL_ADDRESS", 0));//详细地址
        }
        return temp;
    }

    /**
     * 查看投资记录(微信部分) by cuibin
     */
    @Override
    public List<WeixinProductBuyMes> openLoanDetails(WeixinProductBuyMes weixinProductBuyMes) {

        DMap dMap = productDao.openLoanDetails(weixinProductBuyMes);
        List<WeixinProductBuyMes> mylist = new ArrayList<WeixinProductBuyMes>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                WeixinProductBuyMes temp = new WeixinProductBuyMes();
                temp.setId(dMap.getValue("ID", i));
                temp.setProductid(dMap.getValue("PRODUCTID", i));
                temp.setBuyer(dMap.getValue("BUYER", i));
                temp.setBuyername(dMap.getValue("BUYERNAME", i));
                temp.setPrice(dMap.getValue("PRICE", i));
                temp.setBuytime(dMap.getValue("BUYTIME", i));
                temp.setType(dMap.getValue("TYPE", i));
                String createtime = dMap.getValue("CREATETIME", i);
                if (!("").equals(createtime) && null != createtime) {
                    createtime = createtime.substring(0, 10);
                }
                temp.setCreatetime(createtime);
                mylist.add(temp);
            }
        }
        return mylist;
    }


    /**
     * 获得产品总数 (微信部分) by cuibin
     */
    @Override
    public String getWeixinProductCount(WeixinProductMessageBean weixinProductMes) {
        DMap dMap = productDao.getWeixinProductCount(weixinProductMes);
        return dMap.getValue("COUNT", 0);
    }

    /**
     * 查询债权转让列表 (微信部分) by cuibin
     */
    @Override
    public List<WeixinProductAttornDetail> queryProductAttornByType(WeixinProductAttornDetail weixinProductAttornDetail) {
        DMap dMap = productDao.queryProductAttornByType(weixinProductAttornDetail);
        List<WeixinProductAttornDetail> list = new ArrayList<WeixinProductAttornDetail>();

        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                WeixinProductAttornDetail temp = new WeixinProductAttornDetail();
                temp.setId(dMap.getValue("ID", i));
                temp.setProductbuyid(dMap.getValue("PRODUCTBUYID", i));
                temp.setAttornprice(dMap.getValue("ATTORNPRICE", i));
                temp.setExpectedrate(dMap.getValue("EXPECTEDRATE", i));
                temp.setProductid(dMap.getValue("PRODUCTID", i));
                temp.setTitle(dMap.getValue("TITLE", i));
                temp.setLoanrate(dMap.getValue("LOANRATE", i));
                temp.setSurplusdays(dMap.getValue("SURPLUSDAYS", i));
                temp.setSurplusprincipal(dMap.getValue("SURPLUSPRINCIPAL", i));
                temp.setReceivedprice(dMap.getValue("RECEIVEDPRICE", i));
                temp.setNextreceivetime(dMap.getValue("NEXTRECEIVETIME", i));
                temp.setCode(dMap.getValue("CODE", i));
                temp.setLoanfee(dMap.getValue("LOANFEE", i));
                temp.setFulltime(dMap.getValue("FULLTIME", i));
                temp.setLoanyear(dMap.getValue("LOANYEAR", i));
                temp.setLoanmonth(dMap.getValue("LOANMONTH", i));
                temp.setLoadday(dMap.getValue("LOADDAY", i));
                temp.setRepayment(dMap.getValue("REPAYMENT", i));
                temp.setTypeId(dMap.getValue("TYPEID", i));

                list.add(temp);

            }

        }
        return list;
    }

    /**
     * 查询债权转让列表总数 (微信部分) by cuibin
     */
    public String queryProductAttornByTypeCount(WeixinProductAttornDetail weixinProductAttornDetail) {
        DMap dMap = productDao.queryProductAttornByType(weixinProductAttornDetail);
        return dMap.getValue("COUNT", 0);
    }

    /**
     * 查看债权转让详情(微信部分) by cuibin
     */
    @Override
    public WeixinProductAttornDetail queryAttornDetails(String id) {
        DMap dMap = productDao.queryAttornDetails(id);
        WeixinProductAttornDetail temp = new WeixinProductAttornDetail();
        String loanYear = ""; //贷款期限(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(天)
        String loanLength = ""; //贷款时长
        if (dMap.getCount() > 0) {
            temp.setId(dMap.getValue("ID", 0));
            temp.setProductbuyid(dMap.getValue("PRODUCTBUYID", 0));
            temp.setAttornprice(dMap.getValue("ATTORNPRICE", 0));
            temp.setExpectedrate(dMap.getValue("EXPECTEDRATE", 0));
            temp.setProductid(dMap.getValue("PRODUCTID", 0));
            temp.setTitle(dMap.getValue("TITLE", 0));
            temp.setLoanrate(dMap.getValue("LOANRATE", 0));
            temp.setSurplusdays(dMap.getValue("SURPLUSDAYS", 0));
            temp.setSurplusprincipal(dMap.getValue("SURPLUSPRINCIPAL", 0));
            temp.setReceivedprice(dMap.getValue("RECEIVEDPRICE", 0));
            temp.setNextreceivetime(dMap.getValue("NEXTRECEIVETIME", 0));
            temp.setCode(dMap.getValue("CODE", 0));
            temp.setLoanfee(dMap.getValue("LOANFEE", 0));
            temp.setFulltime(dMap.getValue("FULLTIME", 0));
            temp.setLoanyear(dMap.getValue("LOANYEAR", 0));
            temp.setLoanmonth(dMap.getValue("LOANMONTH", 0));
            temp.setLoadday(dMap.getValue("LOADDAY", 0));
            temp.setRepayment(dMap.getValue("REPAYMENT", 0));
            temp.setTypeId(dMap.getValue("TYPEID", 0));

            loanYear = dMap.getValue("LOANYEAR", 0); //贷款期限(年)
            loanMonth = dMap.getValue("LOANMONTH", 0); //贷款期限(月)
            loanDay = dMap.getValue("LOADDAY", 0); //贷款期限(日)
            loanLength = ""; //贷款时长
            if (loanYear != null && !loanYear.equals("")) {
                if (!loanYear.equals("0")) {
                    loanLength = "<span class=\"ft_zhi\">" + loanYear + "</span>年";
                }
            }
            if (loanMonth != null && !loanMonth.equals("")) {
                if (!loanMonth.equals("0")) {
                    loanLength = "<span class=\"ft_zhi\">" + loanMonth + "</span>个月";
                }
            }
            if (loanDay != null && !loanDay.equals("")) {
                if (!loanDay.equals("0")) {
                    loanLength = "<span class=\"ft_zhi\">" + loanDay + "</span>日";
                }
            }
            temp.setLoanLength(loanLength); //贷款时长
        }
        return temp;
    }


    /**
     * 根据产品id查询产品申请人信息(App部分)
     *
     * @param productId
     * @return
     */
    @Override
    public AppProductMemberMessageBean getProductMemberInfo(String productId) {
        AppProductMemberMessageBean appProductMemberMessageBean = new AppProductMemberMessageBean();
        DMap dMap = productDao.getProductMemberInfo(productId); //产品申请人信息
        if (dMap != null) {
            appProductMemberMessageBean.setId(dMap.getValue("ID", 0)); //主键
            appProductMemberMessageBean.setFullScale(dMap.getValue("FULLSCALE", 0)); //是否满标(0-是、1-否)
            appProductMemberMessageBean.setLoadFee(dMap.getValue("LOADFEE", 0)); //贷款金额
            appProductMemberMessageBean.setApplyMember(dMap.getValue("APPLYMEMBER", 0)); //申请人id
            appProductMemberMessageBean.setRealName(dMap.getValue("REALNAME", 0)); //会员姓名
            appProductMemberMessageBean.setSex(dMap.getValue("SEX", 0)); //性别
            appProductMemberMessageBean.setDregree(dMap.getValue("DREGREE", 0)); //学历
            appProductMemberMessageBean.setTelephone(dMap.getValue("TELEPHONE", 0)); //电话
            appProductMemberMessageBean.setChildren(dMap.getValue("CHILDREN", 0)); //子女状况
            appProductMemberMessageBean.setCarFlg(dMap.getValue("CAR_FLG", 0)); //是否有车
            appProductMemberMessageBean.setSocailSecurity(dMap.getValue("SOCIAL_SECURITY", 0)); //社保情况
            appProductMemberMessageBean.setMarryStatus(dMap.getValue("MARRY_STATUS", 0)); //婚姻状态
            appProductMemberMessageBean.setHouseCondition(dMap.getValue("HOUSE_CONDITION", 0)); //住房条件
            appProductMemberMessageBean.setMonthSalary(dMap.getValue("MONTH_SALARY", 0)); //月收入
            appProductMemberMessageBean.setEmail(dMap.getValue("EMAIL", 0)); //邮箱地址
            appProductMemberMessageBean.setDetailAddress(dMap.getValue("DETAIL_ADDRESS", 0)); //详细地址
        }
        return appProductMemberMessageBean;
    }


    /**
     * 获取产品投资情况(App部分)
     *
     * @param productId
     * @return
     */
    @Override
    public AppProductBuyTjMessageBean getProductBuyTj(String productId) {
        AppProductBuyTjMessageBean appProductBuyTjMessageBean = new AppProductBuyTjMessageBean();
        DMap dMap = productDao.getProductBuyTj(productId);
        if (dMap != null) {
            appProductBuyTjMessageBean.setId(dMap.getValue("ID", 0)); //主键
            appProductBuyTjMessageBean.setLoadFee(dMap.getValue("LOADFEE", 0)); //贷款金额
            appProductBuyTjMessageBean.setInvestSum(dMap.getValue("INVESTSUM", 0)); //投资总额
            appProductBuyTjMessageBean.setWinvestSum(dMap.getValue("WINVESTSUM", 0)); //未投资金额
        }
        return appProductBuyTjMessageBean;
    }

    /**
     * 获取产品投资记录(App部分)
     *
     * @param productId
     * @return
     */
    @Override
    public List<AppProductBuyMessageBean> getProductBuyList(String productId, String pageIndex, String pageSize) {
        List<AppProductBuyMessageBean> listAppProductBuyMes = new ArrayList<AppProductBuyMessageBean>();
        DMap dMap = productDao.getProductBuyList(productId, pageIndex, pageSize); //投资记录
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                AppProductBuyMessageBean appProductBuyMessageBean = new AppProductBuyMessageBean();
                appProductBuyMessageBean.setId(dMap.getValue("ID", i)); //主键
                appProductBuyMessageBean.setBuyerName(dMap.getValue("BUYERNAME", i)); //投标人姓名
                appProductBuyMessageBean.setPrice(dMap.getValue("PRICE", i)); //投标金额
                appProductBuyMessageBean.setBuyTime(dMap.getValue("BUYTIME", i)); //投标时间
                listAppProductBuyMes.add(appProductBuyMessageBean);
            }
        }
        return listAppProductBuyMes;
    }

    /**
     * 查询投资记录 by cuibin
     */
    @Override
    public List<ProductBuyMessageBean> queryInvestHistory(ProductBuyMessageBean productBuyMessageBean) {
        DMap dMap = productDao.queryInvestHistory(productBuyMessageBean);
        List<ProductBuyMessageBean> list = new ArrayList<ProductBuyMessageBean>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                ProductBuyMessageBean temp = new ProductBuyMessageBean();
                temp.setId(dMap.getValue("ID", i));
                temp.setShFlag(dMap.getValue("SHFLAG", i)); //审核状态(0-审核通过、1-审核未通过、2-待审核)
                temp.setProductId(dMap.getValue("PRODUCTID", i));
                temp.setBuyer(dMap.getValue("BUYER", i));
                temp.setPrice(dMap.getValue("PRICE", i));
                temp.setBuytime(dMap.getValue("BUYTIME", i));
                temp.setType(dMap.getValue("TYPE", i));
                temp.setCreatetime(dMap.getValue("CREATETIME", i));
                temp.setTitle(dMap.getValue("TITLE", i));
                temp.setCode(dMap.getValue("CODE", i));
                temp.setLoanrate(dMap.getValue("LOANRATE", i));
                temp.setBuyerName(dMap.getValue("BUYERNAME", i));
                list.add(temp);
            }
        }
        return list;
    }

    /**
     * 查询投资记录 总数 by cuibin
     */
    public String queryInvestHistoryCount(ProductBuyMessageBean productMessageBean) {
        DMap d = productDao.queryInvestHistoryCount(productMessageBean);
        return d.getValue("COUNT", 0);
    }


    /**
     * 根据产品id查询需投资产品信息
     * @param productId
     * @return
     */
    @Override
    public AppProductDetailMessageBean getInvestedProductInfo(String productId) {
        DMap dMap = productDao.getInvestedProductInfo(productId);
        AppProductDetailMessageBean appProductDetailMessageBean = new AppProductDetailMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            appProductDetailMessageBean.setBidFee(dMap.getValue("BIDFEE",0)); //最低投标金额
            appProductDetailMessageBean.setApplyMember(dMap.getValue("APPLYMEMBER",0)); //申请人
            appProductDetailMessageBean.setSplitCount(dMap.getValue("SPLITCOUNT",0)); //最大拆分份数
        }
        return appProductDetailMessageBean;
    }


    /**
     * 设置为推荐项目
     * @param newShare
     * @return
     */
    @Override
    public String setNewShare(String productId,String newShare) {
        String result = "0"; //执行成功
        DaoResult daoResult = productDao.setNewShare(productId,newShare);
        if (daoResult.getCode() < 0){
            result = "1"; //执行失败
        }
        return result;
    }


    /**
     * 获取推荐项目集合
     * @param newShsre
     * @return
     */
    @Override
    public List<Map<String, String>> getNewShareList(String newShsre) {
        DMap dMap = productDao.getNewShareList(newShsre); //推荐项目集合
        List<Map<String, String>> listMap = new ArrayList<Map<String, String>>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                Map<String, String> map = new HashMap<String, String>();
                map.put("productId",dMap.getValue("ID",i)); //产品主键
                listMap.add(map);
            }
        }
        return listMap;
    }

}
