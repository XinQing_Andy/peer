package com.mh.peer.service.front;

import com.mh.peer.model.message.*;

import java.util.List;

/**
 * Created by Cuibin on 2016/6/20.
 */
public interface CashService {

    /**
     * 按照id查询城市
     */
    public SysCityMessagebean queryCityById(String id);

    /**
     * 按照id查询城市
     */
    public SysProvinceMessageBean queryProvinceById(String id);

    /**
     * 提现
     */
    public boolean userCash(PostalRecordMessageBean postalRecordMessageBean,TradeDetailMessageBean tradeDetailMessageBean);

    /**
     * 查询所有提现记录
     */
    public List<HuanXunPostalMessageBean> queryPostalRecordByPage(HuanXunPostalMessageBean huanXunPostalMessageBean);

    /**
     * 查询提现金额 到账金额 手续费用
     */
    public String[] querySum(String memberId);

    /**
     * 提现记录数量
     * @param huanXunPostalMes
     * @return
     */
    public int queryPostalRecordByPageCount(HuanXunPostalMessageBean huanXunPostalMes);

    /**
     * 获取可提现金额
     * @param memberId
     * @return
     */
    public String getPostaledPrice(String memberId);
}
