package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.UserLoginDao;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.AppMemberMessageBean;
import com.mh.peer.model.message.LoginMessageBean;
import com.mh.peer.service.front.UserLoginService;
import com.mh.peer.util.Encrypt;
import com.mh.peer.util.PublicsTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by cuibin on 2016/4/22.
 */
@Service
public class UserLoginServiceImpl implements UserLoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserLoginServiceImpl.class);
    /**
     * 用户
     */
    @Autowired
    private UserLoginDao userLoginDao;

    /**
     * servlet工具类后台
     */
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 用户登录
     */
    @Override
    public LoginBusinessBean userLogin(LoginMessageBean user) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        // 验证用户名
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setUsername(user.getUserName());
        memberInfo = userLoginDao.userLogin(memberInfo);
        if(memberInfo == null){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo("用户不存在");
            return loginBusinessBean;
        }
        //判断密码
        String loginPassword = user.getPassword();
        String loginPasswordJm = null;
        if(loginPassword != null && !loginPassword.equals("")){
            Encrypt encrypt = new Encrypt();
            loginPasswordJm = encrypt.handleMd5(loginPassword);
        }
        String password = PublicsTool.isPassword(memberInfo.getPassword());
        if(!loginPasswordJm.equals(password)){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo("密码不正确");
            return loginBusinessBean;
        }
        if ("0".equals(memberInfo.getLockFlg())) {
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo("用户已被锁定");
            return loginBusinessBean;
        }
        loginBusinessBean.setResult("success");
        loginBusinessBean.setInfo("登录成功");
        // 用户
        loginBusinessBean.setMemberInfo(memberInfo);
        // 保存登陆时间
        MemberInfo loginTimeMemberInfo = new MemberInfo();
        loginTimeMemberInfo.setId(memberInfo.getId());
        loginTimeMemberInfo.setLogintime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"));
        userLoginDao.saveLoginTime(loginTimeMemberInfo);
        return loginBusinessBean;
    }


    /**
     * 登录操作(App端)
     * @param appMemberMes
     * @return
     */
    @Override
    public AppMemberMessageBean login(AppMemberMessageBean appMemberMes) {
        DMap dMap = userLoginDao.login(appMemberMes);
        AppMemberMessageBean appMemberMessageBean = new AppMemberMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            appMemberMessageBean.setResultCode("0"); //登录成功
            appMemberMessageBean.setId(dMap.getValue("ID",0)); //主键
            appMemberMessageBean.setMemberUserName(dMap.getValue("USERNAME",0)); //登录名
        }
        return appMemberMessageBean;
    }

    /**
     * 注册部分(App端)
     * @param appMemberMes
     * @return
     */
    @Override
    public AppMemberMessageBean regist(AppMemberMessageBean appMemberMes) {
        AppMemberMessageBean appMemberMessageBean = new AppMemberMessageBean();
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(appMemberMes.getId()); //主键
        memberInfo.setUsername(appMemberMes.getMemberUserName()); //用户名
        memberInfo.setTelephone(appMemberMes.getTelephone()); //手机
        memberInfo.setPassword(appMemberMes.getPassword()); //密码
        memberInfo.setLogintype(appMemberMes.getLoginType()); //登录类型(0-电脑端、1-手机端)
        memberInfo.setRealFlg("1"); //是否实名认证(0-是、1-否)
        memberInfo.setPasswdFlg("1"); //是否更改密码(0-是、1-否)
        memberInfo.setTelephoneFlg("1"); //是否手机认证(0-是、1-否)
        memberInfo.setMailFlg("1"); //是否邮件认证(0-是、1-否)
        memberInfo.setLockFlg("1"); //是否锁定(0-是、1-否)
        memberInfo.setDelFlg("1"); //是否删除(0-是、1-否)
        memberInfo.setBalance(0.0d); //账户余额
        memberInfo.setPoints(0); //积分
        memberInfo.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //注册时间
        DaoResult daoResult = userLoginDao.regist(memberInfo); //注册部分
        if(daoResult.getCode() < 0){
            appMemberMessageBean.setInfo("注册失败!");
            appMemberMessageBean.setResultCode("error");
        }
        appMemberMessageBean.setInfo("注册成功！");
        appMemberMessageBean.setResultCode("success");

        return appMemberMessageBean;
    }

    /**
     * 获取会员数量
     * @param appMemberMes
     * @return
     */
    @Override
    public String getMemberCount(AppMemberMessageBean appMemberMes) {
        String memberCount = "0"; //会员数量
        DMap dMap = userLoginDao.getMemberCount(appMemberMes);
        if(dMap != null){
            memberCount = dMap.getValue("MEMBERCOUNT",0); //会员数量
        }
        return memberCount;
    }


    /**
     * 获取会员信息
     * @param appMemberMes
     * @return
     */
    @Override
    public AppMemberMessageBean getMemberInfo(AppMemberMessageBean appMemberMes) {
        DMap dMap = userLoginDao.getMemberInfo(appMemberMes);
        AppMemberMessageBean appMemberMessageBean = new AppMemberMessageBean();
        if (dMap != null){
            appMemberMessageBean.setId(dMap.getValue("ID",0)); //主键
        }
        return appMemberMessageBean;
    }

    /**
     * 修改密码
     * @param memberId
     * @param password
     * @return
     */
    @Override
    public AppMemberMessageBean updatePassword(String memberId, String password) {
        AppMemberMessageBean appMemberMessageBean = new AppMemberMessageBean();
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(memberId); //会员id
        memberInfo.setPassword(password); //密码
        DaoResult daoResult = userLoginDao.updatePassword(memberId,password);
        if (daoResult.getCode() < 0){
            appMemberMessageBean.setInfo("修改密码失败！");
            appMemberMessageBean.setResultCode("error");
            return appMemberMessageBean;
        }
        appMemberMessageBean.setInfo("修改密码成功！");
        appMemberMessageBean.setResultCode("success");
        return appMemberMessageBean;
    }


}
