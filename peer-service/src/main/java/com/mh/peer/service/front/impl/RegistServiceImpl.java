package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.RegistDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.service.front.RegistService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cuibin on 2016/4/23.
 */
@Service
public class RegistServiceImpl implements RegistService{
    @Autowired
    private RegistDao registDao;
    /**
     * 注册
     */
    @Override
    public MemberInfoMessageBean regist(MemberInfo memberInfo) {
        if(memberInfo != null){
            memberInfo.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd hh:mm:ss"));
            memberInfo.setLockFlg("1"); //是否锁定
            memberInfo.setDelFlg("1"); //是否删除
            memberInfo.setRealFlg("1"); //是否实名化认证
            memberInfo.setPasswdFlg("1"); //是否更改密码
            memberInfo.setBalance(0.0);
        }
        DaoResult daoResult = registDao.regist(memberInfo);
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(daoResult.getCode() < 0){
            memberInfoMessageBean.setInfo("注册失败");
            memberInfoMessageBean.setResult("error");
        }else{
            memberInfoMessageBean.setInfo("注册成功");
            memberInfoMessageBean.setResult("success");
        }
        return memberInfoMessageBean;
    }

    /**
     * 激活
     */
    @Override
    public MemberInfoMessageBean activateUser(MemberInfo memberInfo) {
        DaoResult daoResult = registDao.activateUser(memberInfo);
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(daoResult.getCode() < 0){
            memberInfoMessageBean.setInfo("激活失败");
            memberInfoMessageBean.setResult("error");
        }else{
            memberInfoMessageBean.setInfo("激活成功");
            memberInfoMessageBean.setResult("success");
        }
        return memberInfoMessageBean;
    }

    /**
     * 验证用户名是否存在
     */
    @Override
    public MemberInfoMessageBean checkUsername(String username) {
        DMap dMap = registDao.checkUsername(username);
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(dMap.getCount() > 0){
            memberInfoMessageBean.setInfo("用户名已被注册");
            memberInfoMessageBean.setResult("error");
        }else{
            memberInfoMessageBean.setInfo("用户名可以被使用");
            memberInfoMessageBean.setResult("success");
        }
        return memberInfoMessageBean;
    }
}
