package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.ProductAttornDetailsDao;
import com.mh.peer.model.message.ProductAttornDetailsMes;
import com.mh.peer.service.front.ProductAttornDetailsService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cuibin on 2016/6/29.
 */
@Service
public class ProductAttornDetailsServiceImpl implements ProductAttornDetailsService {

    @Autowired
    private ServletUtil servletUtil;

    @Autowired
    private ProductAttornDetailsDao productAttornDetailsDao;

    /**
     * 查询 成功的债权转让标 详情
     */
    @Override
    public ProductAttornDetailsMes queryProductAttornDetails(ProductAttornDetailsMes productAttornDetailsMessageBean) {

        DMap dMap = productAttornDetailsDao.queryProductAttornDetails(productAttornDetailsMessageBean);
        ProductAttornDetailsMes result = new ProductAttornDetailsMes();
        if (dMap.getCount() > 0) {

            result.setId(dMap.getValue("ID", 0)); //主键
            result.setProductbuyid(dMap.getValue("PRODUCTBUYID", 0));//产品购买id
            result.setPrice(dMap.getValue("PRICE", 0));//转让金额
            result.setExpectedrate(dMap.getValue("EXPECTEDRATE", 0));//预期收益率
            result.setShflag(dMap.getValue("SHFLAG", 0)); //审核状态(0-审核通过、1-审核不通过、2-待审核)
            result.setShyj(dMap.getValue("SHYJ", 0)); //审核意见
            result.setUndertakemember(dMap.getValue("UNDERTAKEMEMBER", 0)); //承接人id
            result.setUndertaketime(dMap.getValue("UNDERTAKETIME", 0)); //承接时间
            result.setCreatetime(dMap.getValue("CREATETIME", 0)); //创建时间
            result.setBuyer(dMap.getValue("BUYER", 0)); //购买人id
            result.setProductid(dMap.getValue("PRODUCTID", 0)); //产品id
            result.setTitle(dMap.getValue("TITLE", 0)); //题目
            result.setCode(dMap.getValue("CODE", 0)); //借款标编号
            result.setLoanrate(dMap.getValue("LOANRATE", 0)); //贷款利率
            result.setType(dMap.getValue("TYPE", 0)); //类型id
            result.setFulltime(dMap.getValue("FULLTIME", 0)); //满标日期
            result.setRepayment(dMap.getValue("REPAYMENT", 0)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
            result.setLoanyear(dMap.getValue("LOANYEAR", 0)); //贷款期限(年)
            result.setLoanmonth(dMap.getValue("LOANMONTH", 0)); //贷款期限(月)
            result.setLoadday(dMap.getValue("LOADDAY", 0)); //贷款期限(日)
            result.setLoadfee(dMap.getValue("LOADFEE", 0)); //借款金额
            result.setApplymember(dMap.getValue("APPLYMEMBER", 0)); //申请人
            result.setRealname(dMap.getValue("REALNAME", 0)); //真实姓名
            result.setSex(dMap.getValue("SEX", 0)); //性别(0-男、1-女)
            result.setTelephone(dMap.getValue("TELEPHONE", 0)); //手机号
            result.setEmail(dMap.getValue("EMAIL", 0)); //邮箱地址
            result.setDregree(dMap.getValue("DREGREE", 0)); //学历
            result.setMarryStatus(dMap.getValue("MARRY_STATUS", 0)); //婚姻状况
            result.setChildren(dMap.getValue("CHILDREN", 0)); //子女状况
            result.setMonthSalary(dMap.getValue("MONTH_SALARY", 0)); //月收入
            result.setSocialSecurity(dMap.getValue("SOCIAL_SECURITY", 0)); //社保情况
            result.setHouseCondition(dMap.getValue("HOUSE_CONDITION", 0)); //住房条件
            result.setCarFlg(dMap.getValue("CAR_FLG", 0)); //是否有车(0-有、1-无)
            result.setDetailAddress(dMap.getValue("DETAIL_ADDRESS", 0)); //详细地址
            result.setBalance(dMap.getValue("BALANCE", 0)); //账户余额
            result.setWaitprice(dMap.getValue("WAITPRICE", 0)); //待收本息
            result.setEndtime(dMap.getValue("ENDTIME", 0)); //到期时间
            result.setSurplusdays(dMap.getValue("SURPLUSDAYS", 0)); //剩余日期
            result.setSurplusperiods(dMap.getValue("SURPLUSPERIODS", 0)); //剩余期数
            result.setWaitingpricipal(dMap.getValue("WAITINGPRICIPAL", 0)); //待收本金
            result.setFulltime(dMap.getValue("FULLTIME", 0)); //发标日期
            result.setCrowds(dMap.getValue("CROWDS", 0)); //适合人群
            result.setSponsor(dMap.getValue("SPONSOR", 0)); //担保人
            result.setCreditstart(dMap.getValue("CREDITSTART", 0)); //额度范围(起始)
            result.setCreditend(dMap.getValue("CREDITEND", 0)); //额度范围(截止)
            result.setInvestprogress(dMap.getValue("INVESTPROGRESS", 0)); //产品投标进度
            result.setInvestprice(dMap.getValue("INVESTPRICE", 0)); //产品投资金额总和
            result.setApplycondition(dMap.getValue("APPLYCONDITION", 0)); //借款人条件说明

            String loadLength = ""; //贷款期限
            if (dMap.getValue("LOANYEAR", 0) != null && !dMap.getValue("LOANYEAR", 0).equals("0")) {
                loadLength = loadLength + dMap.getValue("LOANYEAR", 0) + "年";
            }
            if (dMap.getValue("LOANMONTH", 0) != null && !dMap.getValue("LOANMONTH", 0).equals("0")) {
                loadLength = loadLength + dMap.getValue("LOANMONTH", 0) + "个月";
            }
            if (dMap.getValue("LOADDAY", 0) != null && !dMap.getValue("LOADDAY", 0).equals("0")) {
                loadLength = loadLength + dMap.getValue("LOADDAY", 0) + "天";
            }
            result.setLoadLength(loadLength); //借款期限


        }
        return result;
    }

    /**
     * 查询竞拍记录
     */
    @Override
    public List<ProductAttornDetailsMes> queryAuctionHistory(String productId) {
        DMap dMap = productAttornDetailsDao.queryAuctionHistory(productId);
        List<ProductAttornDetailsMes> list = new ArrayList<ProductAttornDetailsMes>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                ProductAttornDetailsMes temp = new ProductAttornDetailsMes();
                temp.setAuctionPrice(dMap.getValue("PRICE", i));
                temp.setBuytime(dMap.getValue("BUYTIME", i));
                temp.setReceiveflag(dMap.getValue("RECEIVEFLAG", i));
                list.add(temp);
            }

        }
        return list;
    }
}
