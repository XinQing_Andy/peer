package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.ConTractDao;
import com.mh.peer.model.message.ConTractMessageBean;
import com.mh.peer.service.front.ConTractService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-8-31.
 * 合同部分
 */
@Service
public class ConTractServiceImpl implements ConTractService{

    @Autowired
    private ConTractDao conTractDao;

    /**
     * 获取产品信息
     * @param productId
     * @return
     */
    @Override
    public ConTractMessageBean getProductInfo(String productId) {
        DMap dMap = conTractDao.getProductInfo(productId);
        String bidFee = ""; //最低投标金额
        String splitCount = ""; //最大拆分份数
        Double loanPrice = 0.0d; //借款金额
        String loanYear = ""; //借款期限(年)
        String loanMonth = ""; //借款期限(月)
        String loanDay = ""; //借款期限(日)
        String loanLength = ""; //借款期限(时间长度)
        String repayment = ""; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        String repaymentName = ""; //还款方式名称
        String fullTime = ""; //起息日
        ConTractMessageBean conTractMessageBean = new ConTractMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            conTractMessageBean.setApplyMemberName(dMap.getValue("APPLYMEMBERNAME",0)); //借款人姓名

            bidFee = dMap.getValue("BIDFEE",0); //最低投标金额
            splitCount = dMap.getValue("SPLITCOUNT",0); //最高拆分份数
            if (bidFee != null && !bidFee.equals("") && splitCount != null && !splitCount.equals("")){
                loanPrice = TypeTool.getDouble(bidFee) * TypeTool.getInt(splitCount);
            }
            conTractMessageBean.setLoanPrice(TypeTool.getString(loanPrice)); //借款金额

            loanYear = dMap.getValue("LOANYEAR",0); //借款期限(年)
            loanMonth = dMap.getValue("LOANMONTH",0); //借款期限(月)
            loanDay = dMap.getValue("LOADDAY",0); //借款期限(日)

            if (loanYear != null && !loanYear.equals("")){
                if (!loanYear.equals("0")){
                    loanLength = loanLength + loanYear + "年";
                }
            }
            if (loanMonth != null && !loanMonth.equals("")){
                if (!loanMonth.equals("0")){
                    loanLength = loanLength + loanMonth + "个月";
                }
            }
            if (loanDay != null && !loanDay.equals("")){
                if (!loanDay.equals("0")){
                    loanLength = loanLength + loanDay + "日";
                }
            }
            conTractMessageBean.setLoanLength(loanLength); //贷款长度

            fullTime = dMap.getValue("FULLTIME",0); //满标日期(起息日)
            if (fullTime != null && !fullTime.equals("")){
                fullTime = fullTime.substring(0,10);
            }
            conTractMessageBean.setFullTime(fullTime);

            repayment = dMap.getValue("REPAYMENT",0); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
            if (repayment != null && !repayment.equals("")){
                if (repayment.equals("0")){
                    repaymentName = "按月还款、等额本息";
                }else if (repayment.equals("1")){
                    repaymentName = "按月付息、到期还本";
                }else if (repayment.equals("2")){
                    repaymentName = "一次性还款";
                }
            }
            conTractMessageBean.setRepayment(repaymentName); //还款方式
            conTractMessageBean.setLoanRate(dMap.getValue("LOANRATE",0)); //借款利率
            conTractMessageBean.setLoanPurpose(dMap.getValue("LOANPURPOSE",0)); //借款用途
            conTractMessageBean.setConTractTime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd")); //合同签订日期
        }
        return conTractMessageBean;
    }

    /**
     * 获取产品投资信息
     * @param productId
     * @return
     */
    @Override
    public List<ConTractMessageBean> getProductBuyInfoList(String productId) {
        List<ConTractMessageBean> listConTractMes = new ArrayList<ConTractMessageBean>();
        DMap dMap = conTractDao.getProductBuyInfoList(productId);
        String userName = ""; //用户名
        String IDCard = ""; //身份证号
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ConTractMessageBean conTractMessageBean = new ConTractMessageBean();

                userName = dMap.getValue("USERNAME",i); //用户名
                if (userName != null && !userName.equals("")){
                    if (userName.indexOf("@") > 0){ //邮箱
                        int num = userName.indexOf("@");
                        userName = userName.substring(0,1) + "***" + userName.substring(num);
                    }else{ //电话
                        userName = userName.substring(0,3) + "***" + userName.substring(7);
                    }
                }
                conTractMessageBean.setBuyerUserName(userName); //用户名
                conTractMessageBean.setBuyerXm(dMap.getValue("REALNAME",i)); //姓名

                IDCard = dMap.getValue("IDCARD",i); //身份证号
                IDCard = IDCard.substring(0,3) + "******" + IDCard.substring(9);
                conTractMessageBean.setBuyerIDcard(IDCard); //身份证号
                conTractMessageBean.setBuyPrice(dMap.getValue("PRICE",i)); //投资金额
                listConTractMes.add(conTractMessageBean);
            }
        }
        return listConTractMes;
    }


    /**
     * 获取债权信息
     * @param attornId
     * @return
     */
    @Override
    public ConTractMessageBean getAttornInfo(String attornId) {
        DMap dMap = conTractDao.getAttornInfo(attornId);
        String attornTime = ""; //承接时间
        String loanYear = ""; //贷款时间(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(天)
        String loanLength = ""; //贷款期限(长度)
        ConTractMessageBean conTractMessageBean = new ConTractMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            conTractMessageBean.setBuyerXm(dMap.getValue("BUYERNAME",0)); //购买人(转让人)姓名
            conTractMessageBean.setBuyerIDcard(dMap.getValue("BUYERIDCARD",0)); //购买人(转让人)身份证号
            conTractMessageBean.setBuyerUserName(dMap.getValue("BUYERUSERNAME",0)); //购买人(转让人)用户名
            conTractMessageBean.setAttornerXm(dMap.getValue("UNDERTAKEMEMBERNAME",0)); //承接人(受让人)姓名
            conTractMessageBean.setAttornerIDcard(dMap.getValue("UNDERTAKEMEMBERIDCARD",0)); //承接人(受让人)身份证号
            conTractMessageBean.setAttornerUserName(dMap.getValue("UNDERTAKEMEMBERUSERNAME", 0)); //承接人(受让人)用户名
            conTractMessageBean.setLoanRate(dMap.getValue("LOANRATE",0)); //贷款利率
            attornTime = dMap.getValue("UNDERTAKETIME",0); //承接时间
            if (attornTime != null && !attornTime.equals("")){
                attornTime = attornTime.substring(0,10);
            }
            conTractMessageBean.setAttornTime(attornTime); //债权转让时间
            conTractMessageBean.setApplyMemberName(dMap.getValue("APPLYMEMBERNAME",0)); //借款人姓名
            conTractMessageBean.setApplyMemberUserName(dMap.getValue("APPLYMEMBERUSERNAME",0)); //借款人用户名
            conTractMessageBean.setLoanRate(dMap.getValue("LOANRATE",0)); //贷款利率

            loanYear = dMap.getValue("LOANYEAR",0); //贷款期限(年)
            loanMonth = dMap.getValue("LOANMONTH",0); //贷款期限(月)
            loanDay = dMap.getValue("LOADDAY",0); //贷款期限(日)

            if (loanYear != null && !loanYear.equals("")){ //年
                if (!loanYear.equals("0")){
                    loanLength = loanYear + "年";
                }
            }
            if (loanMonth != null && !loanMonth.equals("")){
                if (!loanMonth.equals("0")){
                    loanLength = loanLength + loanMonth + "个月";
                }
            }
            if (loanDay != null && !loanDay.equals("")){
                if (!loanDay.equals("0")){
                    loanLength = loanLength + loanDay + "天";
                }
            }
            conTractMessageBean.setLoanLength(loanLength); //贷款期限(时长)
            conTractMessageBean.setAttornPrincipal(dMap.getValue("ATTORNPRINCIPAL",0)); //债权本金
            conTractMessageBean.setAttornPrice(dMap.getValue("PRICE",0)); //转让价格
            conTractMessageBean.setConTractTime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd")); //签订时间
            System.out.println(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"));
        }
        return conTractMessageBean;
    }

}
