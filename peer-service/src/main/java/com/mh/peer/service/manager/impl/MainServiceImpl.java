package com.mh.peer.service.manager.impl;

import com.mh.peer.dao.manager.MainDao;
import com.mh.peer.dao.manager.UserDao;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.SysRoleUser;
import com.mh.peer.model.entity.SysUser;
import com.mh.peer.model.message.MenuBeanMessage;
import com.mh.peer.service.manager.MainService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by WangMeng on 2016/1/13.
 */
@Service
public class MainServiceImpl implements MainService {
    @Autowired
    private MainDao mainDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 获取顶部菜单(一级菜单)
     * @param menuBeanMessage
     * @return
     */
    @Override
    public List<MenuBeanMessage> getMenu(MenuBeanMessage menuBeanMessage) {
        List<MenuBeanMessage> menuBeanMessages = new ArrayList<MenuBeanMessage>();
        //查看用户角色
        SysRoleUser sysRoleUser = new SysRoleUser();
        sysRoleUser.setUserId(TypeTool.getInt(menuBeanMessage.getUserId())); //用户id

        SysUser sysUser = new SysUser(); //用户
        sysUser.setId(TypeTool.getInt(menuBeanMessage.getUserId())); //用户id
        sysUser = userDao.getUserById(sysUser); //用户集合
        int roleId = sysUser.getRoleId(); //角色id
        DMap parm = mainDao.getTopMenu(menuBeanMessage.getUserId(),TypeTool.getString(roleId)); //获取顶部菜单

        //业务对象转消息对象
        for(int i = 0; i < parm.getCount(); i++){
            MenuBeanMessage beanMessage = new MenuBeanMessage();
            beanMessage.setRoleId(parm.getValue("ROLE_ID", i)); //角色id
            beanMessage.setUserId(parm.getValue("USER_ID", i)); //用户id
            beanMessage.setMenuId(parm.getValue("MENU_ID", i)); //菜单id
            beanMessage.setMenuName(parm.getValue("MENU_NAME", i)); //菜单名称
            beanMessage.setMenuUrl(parm.getValue("MENU_URL", i)); //模板名称
            beanMessage.setControlUrl(parm.getValue("CONTROL_URL", i)); //控制类URL
            beanMessage.setParentId(parm.getValue("PARENT_ID", i)); //父级菜单ID
            beanMessage.setIconStyle(parm.getValue("ICON_STYLE", i)); //图标样式
            beanMessage.setChildCount(parm.getValue("CHILDREN",i)); //子菜单个数
            beanMessage.setParentId(parm.getValue("PARENT_ID", i)); //父级菜单ID
            menuBeanMessages.add(beanMessage); //List集合
        }
        return menuBeanMessages;
    }

    /**
     * 获取二级菜单
     * @param menuBeanMessage
     * @return
     */
    @Override
    public List<MenuBeanMessage> getLevelMenu(MenuBeanMessage menuBeanMessage) {
        List<MenuBeanMessage> menuBeanMessages = new ArrayList<MenuBeanMessage>();

        //获取session
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean loginBusinessBean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        List<SysRoleUser> sysRoleUserList = loginBusinessBean.getRoleUserListr();

        /********** 角色用户关联部分循环Start **********/
        for(SysRoleUser temp : sysRoleUserList){
            DMap parm = mainDao.getMeun(TypeTool.getString(temp.getUserId()), TypeTool.getString(temp.getRoleId()), menuBeanMessage.getParentId());
            if (parm.getErrCode() < 0){
                break;
            }
            int count = parm.getCount(); //获取到的菜单数量
            /********** 菜单循环Start **********/
            for (int i = 0; i < count; i++) {
                MenuBeanMessage menu = new MenuBeanMessage();
                menu.setRoleId(parm.getValue("ROLE_ID", i)); //角色id
                menu.setUserId(parm.getValue("USER_ID", i)); //用户id
                menu.setMenuId(parm.getValue("MENU_ID", i)); //菜单id
                menu.setMenuName(parm.getValue("MENU_NAME", i)); //菜单名称
                menu.setMenuUrl(parm.getValue("MENU_URL", i)); //菜单的URL
                menu.setControlUrl(parm.getValue("CONTROL_URL", i)); //控制类的URL
                menu.setParentId(parm.getValue("PARENT_ID", i)); //父级菜单id
                menu.setIconStyle(parm.getValue("ICON_STYLE", i)); //图标样式
                menu.setChildCount(parm.getValue("CHILDREN",i)); //子菜单个数
                menu.setParentId(parm.getValue("PARENT_ID", i)); //父级菜单id
                menuBeanMessages.add(menu);
            }
            /********** 菜单循环End **********/
        }
        /*********** 角色用户关联部分循环End ***********/
        return menuBeanMessages;
    }
}
