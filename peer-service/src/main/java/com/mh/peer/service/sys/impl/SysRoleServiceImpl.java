package com.mh.peer.service.sys.impl;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.dao.manager.RoleDao;
import com.mh.peer.model.entity.SysRole;
import com.mh.peer.model.entity.SysRoleMenu;
import com.mh.peer.model.message.SysMenuMessageBean;
import com.mh.peer.model.message.SysRoleMenuMessBean;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.mh.peer.service.sys.SysRoleService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016/1/27.
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private RoleDao roleDao;

    @Override
    public List<SysRole> getRoleInfo() {
        SysRole role = new SysRole();
        List<SysRole> sysRoleList = roleDao.getRoleInfo(role);
        //System.out.println("sysRoleList == " + sysRoleList);
        if (sysRoleList.size() > 0){
            for (int i = 0; i < sysRoleList.size(); i++) {
                if ("0".equals(sysRoleList.get(i).getState())){
                    sysRoleList.get(i).setState("启用");
                }else if ("1".equals(sysRoleList.get(i).getState())){
                    sysRoleList.get(i).setState("停用");
                }
            }
        }
        return sysRoleList;
    }

    /**
     * 查询用户组信息(根据分页)
     * @param sysRolMese
     * @return
     */
    @Override
    public List<SysRoleMessageBean> getRoleInfoByPage(SysRoleMessageBean sysRolMese) {
        List<SysRoleMessageBean> roleMessageBeans = new ArrayList<SysRoleMessageBean>();
        DMap parm = roleDao.getRoleInfoByPage(sysRolMese); //查询用户组
        String num = "1";
        if(parm.getCount() > 0){
            for (int i = 0;i < parm.getCount();i++){
                SysRoleMessageBean roleMessageBean = new SysRoleMessageBean();
                roleMessageBean.setId(parm.getValue("ID", i)); //主键
                roleMessageBean.setRoleName(parm.getValue("ROLE_NAME", i)); //用户组名称
                roleMessageBean.setRoleDescription(parm.getValue("ROLE_DESCRIPTION", i)); //用户组描述
                roleMessageBean.setUserCount(parm.getValue("USERCOUNT", i)); //用户数量
                roleMessageBean.setNum(num); //序号
                num = String.valueOf(Integer.parseInt(num) + 1);
                roleMessageBeans.add(roleMessageBean);
            }
        }
        return roleMessageBeans;
    }

    /**
     * 根据id查询用户组信息
     * @param sysRolMese
     * @return
     */
    @Override
    public SysRoleMessageBean queryRoleById(SysRoleMessageBean sysRolMese) {
        SysRoleMessageBean sysRolMes = new SysRoleMessageBean();
        DMap dMap = roleDao.queryRoleById(sysRolMese);
        if(dMap.getCount() > 0){
            sysRolMes.setId(dMap.getValue("ID").substring(1,dMap.getValue("ID").length()-1)); //主键
            sysRolMes.setRoleName(dMap.getValue("ROLE_NAME").substring(1, dMap.getValue("ROLE_NAME").length()-1)); //用户组名称
            sysRolMes.setRoleDescription(dMap.getValue("ROLE_DESCRIPTION").substring(1, dMap.getValue("ROLE_DESCRIPTION").length()-1)); //用户组描述
        }

        return sysRolMes;
    }

    /**
     * 查询菜单权限
     * @return
     */
    @Override
    public List<SysRoleMenuMessBean> getMenuInfo() {
        DMap result = roleDao.getMenuInfo();
        List<SysRoleMenuMessBean> roleMenuMessBeanList = new ArrayList<>();
        for (int i = 0; i < result.getCount(); i++) {
            SysRoleMenuMessBean message = new SysRoleMenuMessBean();
            message.setFlg(result.getBoolean("flg", i));
            message.setMenuId(result.getValue("id", i));
            message.setParentId(result.getValue("PARENT_ID", i));
            message.setMenuSeq(result.getValue("MENU_SEQ", i));
            message.setLevelMenu(result.getValue("LEVEL_MENU", i));
            message.setTopMenu(result.getValue("topMenu", i));
            message.setSecondMenu(result.getValue("secondMenu", i));
            message.setThirdMenu(result.getValue("thirdMenu", i));
            roleMenuMessBeanList.add(message);
        }
        return roleMenuMessBeanList;
    }

    /**
     * 新增用户角色
     * @param role 用户角色
     * @return
     */
    @Override
    public LoginBusinessBean addRole(SysRoleMessageBean role) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        SysRole sysRole = new SysRole(); //用户角色
        sysRole.setRoleName(role.getRoleName()); //角色名称
        sysRole.setRoleCode(role.getRoleCode()); //角色编码
        sysRole.setIsDelete("0"); //是否删除(0-未删除、1-已逻辑删除)
        sysRole.setRoleDescription(role.getRoleDescription()); //角色描述
        if ("启用".equals(role.getState())){
            role.setState("0");
        } else if ("停用".equals(role.getState())) {
            role.setState("1");
        }
        sysRole.setState(role.getState()); //状态（0：启用；1：禁用）
        sysRole.setCreateUserId(role.getCreateUserId()); //创建用户
        sysRole.setCreateTime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss")); //创建时间
        DaoResult result = roleDao.addRole(sysRole); //新增用户角色
        if(result.getCode() < 0){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo(result.getErrText());
        }else{
            loginBusinessBean.setResult("success");
            loginBusinessBean.setInfo("添加成功");
        }
        return loginBusinessBean;
    }

    /**
     * 修改用户角色
     * @param role
     * @return
     */
    @Override
    public LoginBusinessBean updateRole(SysRoleMessageBean role) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        SysRole sysRole = new SysRole(); //用户角色
        sysRole.setId(TypeTool.getInt(role.getId())); //角色id
        sysRole.setRoleName(role.getRoleName()); //角色名称
        sysRole.setRoleCode(role.getRoleCode()); //角色编码
        sysRole.setRoleDescription(role.getRoleDescription()); //角色描述
        if ("启用".equals(role.getState())){
            role.setState("0");
        } else if ("停用".equals(role.getState())) {
            role.setState("1");
        }
        //sysRole.setState(role.getState()); //角色状态(0：启用；1：禁用)
        sysRole.setUpdateUserId(role.getCreateUserId()); //创建用户id
        sysRole.setUpdateTime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss")); //更新时间
        DaoResult result = roleDao.updateRole(sysRole); //更新用户
        if(result.getCode() < 0){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo(result.getErrText());
        }else{
            loginBusinessBean.setResult("success");
            loginBusinessBean.setInfo("修改成功");
        }
        return loginBusinessBean;
    }

    /**
     * 删除角色
     * @param role
     * @return
     */
    @Override
    public LoginBusinessBean delRole(SysRoleMessageBean role) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        SysRole sysRole = new SysRole(); //角色
        sysRole.setId(TypeTool.getInt(role.getId())); //角色id
        DaoResult result = roleDao.delRole(sysRole);
        if(result.getCode() < 0){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo(result.getErrText());
        }else{
            loginBusinessBean.setResult("success");
            loginBusinessBean.setInfo("删除成功");
        }
        return loginBusinessBean;
    }

    /**
     * 根据角色查询菜单信息
     * @return
     */
    @Override
    public List<SysRoleMenuMessBean> getRoleMenuInfo(SysRoleMenuMessBean roleMenuMessBean) {
        List<SysRoleMenuMessBean> roleMenuList = new ArrayList<>(); //用户角色集合
        SysRole role = new SysRole(); //角色
        role.setId(TypeTool.getInt(roleMenuMessBean.getRoleId())); //角色id
        DMap result = roleDao.getRoleMenuInfo(role);
        /********** 角色信息循环Start **********/
        for (int i = 0; i < result.getCount(); i++) {
            SysRoleMenuMessBean message = new SysRoleMenuMessBean();
            message.setRoleId(roleMenuMessBean.getRoleId()); //角色id
            message.setFlg(result.getBoolean("FLG", i)); //角色是否选中
            message.setMenuId(result.getValue("ID", i)); //菜单ID
            message.setParentId(result.getValue("PARENT_ID", i)); //父级ID
            message.setMenuSeq(result.getValue("MENU_SEQ", i)); //菜单序号
            message.setLevelMenu(result.getValue("LEVEL_MENU", i)); //菜单层级
            message.setTopMenu(result.getValue("TOPMENU", i)); //一级菜单
            message.setSecondMenu(result.getValue("SECONDMENU", i)); //二级菜单
            message.setThirdMenu(result.getValue("THIRDMENU", i)); //三级菜单
            roleMenuList.add(message);
        }
        /*********** 角色信息循环End ***********/
        return roleMenuList;
    }

    /**
     * 保存权限信息
     * @param roleMenuList
     * @return
     */
    @Override
    public SysRoleMenuMessBean saveRoleMenu(List<SysRoleMenu> roleMenuList) {
        SysRoleMenuMessBean message = new SysRoleMenuMessBean();
        DaoResult result = new DaoResult();
        if (roleMenuList.size() <= 0){
            message.setResult("error");
            message.setInfo("没有需要保存的数据");
        }
        // 删除同一角色的权限
        SysRoleMenu menu = new SysRoleMenu();
        menu.setRoleId(roleMenuList.get(0).getRoleId());
        result = roleDao.delRoleMenu(menu);
        if (result.getCode() < 0){
            message.setResult("error");
            message.setInfo(result.getErrText());
        }
        // 将该角色的权限填入表中
        for (int i = 0; i < roleMenuList.size(); i++) {
            SysRoleMenu roleMenu = new SysRoleMenu(); //角色菜单
            roleMenu.setMenuId(roleMenuList.get(i).getMenuId()); //菜单id
            roleMenu.setRoleId(roleMenuList.get(i).getRoleId()); //角色id
            roleMenu.setCreateTime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss")); //创建时间
            result = roleDao.addRoleMenu(roleMenu);
            if (result.getCode() < 0){
                message.setResult("error");
                message.setInfo(result.getErrText());
                break;
            }else{
                message.setResult("success");
                message.setInfo("保存成功");
            }
        }
        return message;
    }

    /**
     * 获取用户角色数量
     * @param sysRoleMes
     * @return
     */
    @Override
    public int getCountSysRoleByPage(SysRoleMessageBean sysRoleMes) {
        DMap parm = roleDao.getCountSysRoleByPage(sysRoleMes); //查询角色
        int totalCount = parm.getCount(); //角色数量
        return totalCount;
    }

    /**
     * 获取所有菜单
     */
    @Override
    public List<SysMenuMessageBean> queryAllMenu() {
        DMap dMap = roleDao.queryAllMenu();
        List<SysMenuMessageBean> listSysMenuMes = new ArrayList<SysMenuMessageBean>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                SysMenuMessageBean sysMenuMes = new SysMenuMessageBean();
                sysMenuMes.setId(StringTool.getInt(dMap.getValue("ID", i))); //主键
                sysMenuMes.setParentId(dMap.getValue("PARENT_ID", i)); //父菜单
                sysMenuMes.setMenuName(dMap.getValue("MENU_NAME", i)); //菜单名称
                sysMenuMes.setLevelMenu(dMap.getValue("LEVEL_MENU", i)); //菜单登记
                listSysMenuMes.add(sysMenuMes);
            }
        }
        return listSysMenuMes;
    }


    /**
     * 根据父id查询所有菜单
     * @param fid
     * @return
     */
    @Override
    public List<SysMenuMessageBean> queryAllMenuByFid(String fid) {
        DMap dMap = roleDao.queryAllMenuByFid(fid);
        List<SysMenuMessageBean> listSysMenuMes = new ArrayList<SysMenuMessageBean>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                SysMenuMessageBean sysMenuMessageBean = new SysMenuMessageBean();
                sysMenuMessageBean.setId(TypeTool.getInt(dMap.getValue("ID", i))); //主键
                sysMenuMessageBean.setMenuName(dMap.getValue("MENU_NAME", i));  //菜单名称
                listSysMenuMes.add(sysMenuMessageBean);
            }
        }
        return listSysMenuMes;
    }

    /**
     * 根据父id查询所有菜单数量
     * @param fid
     * @return
     */
    @Override
    public int queryAllMenuByFidCount(String fid) {
        DMap dMap = roleDao.queryAllMenuByFid(fid);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 根据角色id查询都有哪些菜单
     * @return
     */
    @Override
    public List<SysRoleMenuMessBean> getAllRoleMenu(String roleId) {
        DMap dMap = roleDao.getAllRoleMenu(roleId);
        List<SysRoleMenuMessBean> listSysRoleMenuMes = new ArrayList<SysRoleMenuMessBean>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                SysRoleMenuMessBean sysRoleMenuMessBean = new SysRoleMenuMessBean();
                sysRoleMenuMessBean.setMenuId(dMap.getValue("MENU_ID",i)); //菜单id
                listSysRoleMenuMes.add(sysRoleMenuMessBean);
            }
        }
        return listSysRoleMenuMes;
    }


    /**
     * 保存用户角色权限
     * @param sysRoleMenuMes
     * @return
     */
    @Override
    public SysRoleMenuMessBean saveRolePower(SysRoleMenuMessBean sysRoleMenuMes) {
        SysRoleMenuMessBean sysRoleMenuMessBean = roleDao.saveRolePower(sysRoleMenuMes); //保存角色权限
        return sysRoleMenuMessBean;
    }
}
