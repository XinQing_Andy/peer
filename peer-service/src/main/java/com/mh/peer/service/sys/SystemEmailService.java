package com.mh.peer.service.sys;

import com.mh.peer.model.message.SysEmailSettingMessageBean;

/**
 * Created by cuibin on 2016/4/25.
 */
public interface SystemEmailService {
    /**
     *查询邮件设置
     */
    SysEmailSettingMessageBean querySystemEmail();
    /**
     *  添加邮件设置
     */
    SysEmailSettingMessageBean addSystemEmail(SysEmailSettingMessageBean sysEmailSettingMessageBean);
    /**
     *  更新邮件设置
     */
    SysEmailSettingMessageBean updateSystemEmail(SysEmailSettingMessageBean sysEmailSettingMessageBean);

}
