package com.mh.peer.service.front;

import com.mh.peer.model.message.AppTradeMessageBean;
import com.mh.peer.model.message.TradeDetailMessageBean;
import java.util.List;

/**
 * Created by Cuibin on 2016/6/20.
 */
public interface TradeDetailService {

    /**
     * 查询资金明细
     * @param tradeDetailMes
     * @return
     */
    public List<TradeDetailMessageBean> queryTradeDetail(TradeDetailMessageBean tradeDetailMes);

    /**
     * 查询总记录数
     */
    public int queryTradeDetailCount(TradeDetailMessageBean tradeDetailMes);

    /**
     * 获取交易记录(App端)
     * @param memberId
     * @return
     */
    public List<AppTradeMessageBean> getTradeRecordList(String memberId,String pageIndex,String pageSize,String tradeType,String tradeTime);
}
