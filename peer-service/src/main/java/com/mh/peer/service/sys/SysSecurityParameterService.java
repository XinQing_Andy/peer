package com.mh.peer.service.sys;

import com.mh.peer.model.business.SysSecurityParameterBusinessBean;
import com.mh.peer.model.message.SysSecurityParameterMessageBean;

/**
 * Created by zhangerxin on 2016/3/31 0025.
 */
public interface SysSecurityParameterService {

    /**
     * 新增安全参数
     * @param sysSecurityParameterMess
     * @return
     */
   SysSecurityParameterBusinessBean addSysSecurityParameter(SysSecurityParameterMessageBean sysSecurityParameterMess);

    /**
     * 修改安全参数
     * @param sysSecurityParameterMess
     * @return
     */
    SysSecurityParameterBusinessBean updateSysSecurityParameter(SysSecurityParameterMessageBean sysSecurityParameterMess);

    /**
     * 获取安全问题
     * @param sysSecurityParameterMes
     * @return
     */
    SysSecurityParameterMessageBean querySecurityParameter(SysSecurityParameterMessageBean sysSecurityParameterMes);
}
