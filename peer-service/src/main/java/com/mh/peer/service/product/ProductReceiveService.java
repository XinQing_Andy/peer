package com.mh.peer.service.product;

import com.mh.peer.model.message.*;
import java.util.List;

/**
 * Created by cuibin on 2016/5/25.
 */
public interface ProductReceiveService {
    /**
     * 项目详情 已收详情
     */
    public ProductReceiveMessageBean alreadyReceive(ProductReceiveMessageBean productReceiveMessageBean, String userId);
    /**
     * 项目详情 未收详情
     */
    public ProductReceiveMessageBean notReceive(ProductReceiveMessageBean productReceiveMessageBean, String userId);
    /**
     * 项目详情 期数详情
     */
    public List<ProductReceiveMessageBean> phaseDetails(ProductMessageBean productMessageBean, String userId);

    /**
     *
     * @param productReceiveMessageBean
     * @return
     */
    public ProductReceiveMessageBean getProductReceiveDetail(ProductReceiveMessageBean productReceiveMessageBean);

    /**
     * 根据条件查询收款情况
     * @param productReceiveMes
     * @return
     */
    public List<ProductReceiveMessageBean> getProductReceiveListBySome(ProductReceiveMessageBean productReceiveMes);

    /**
     * 投资总额
     */
    public String investTotal(String userId);

    /**
     * 投标中本金
     */
    public String inaBid(String userId);

    /**
     * 待回收本金
     */
    public String toBeRecycled(String userId);

    /**
     *已回收本金
     */
    public String retired(String userId);

    /**
     * 待回收金额 本金 利息
     */
    public String[] moneyRecycled(String userId);

    /**
     * 平均受益率
     */
    public String avgBenefit(String userId);

    /**
     * 获取昨天收益(App端)
     * @param appHomeMes
     * @return
     */
    AppHomeMessageBean getLastDayPrice(AppHomeMessageBean appHomeMes);

    /**
     * 获取待收(已收)金额、待收(已收)期数、待收(已收)投资数量(App端)
     * @param memberId
     * @param receiveFlag
     * @return
     */
    AppProductReceiveTjMessageBean getAppReceiveInfo(String memberId,String receiveFlag);

    /**
     * 获取待收(已收)列表(App端)
     * @param memberId
     * @param receiveFlag
     * @return
     */
    List<AppProductReceiveMessageBean> getAppReceiveList(String memberId,String receiveFlag,String pageIndex,String pageSize);

    /**
     * 根据id获取回款信息
     * @param receiveId
     * @return
     */
    AppProductReceiveMessageBean getAppReceiveInfoById(String receiveId);

    /**
     * 查询需自动还款信息
     * @param productAutoRepayMes
     * @return
     */
    List<ProductAutoRepayMessageBean> getReceiveInfoList(ProductAutoRepayMessageBean productAutoRepayMes);
}
