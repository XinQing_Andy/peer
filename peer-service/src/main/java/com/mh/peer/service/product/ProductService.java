package com.mh.peer.service.product;

import com.mh.peer.model.business.ProductBusinessBean;
import com.mh.peer.model.entity.ProductContent;
import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-4-10.
 */
public interface ProductService {

    /**
     * 根据分页查询产品(By zhangerxin)
     * @param productMes
     * @return
     */
    public List<ProductMessageBean> queryProductByPage(ProductMessageBean productMes);

    /**
     * 获取产品数量(前台)
     * @param productMes
     * @return
     */
    public int queryProductCount(ProductMessageBean productMes);

    /**
     * 根据id获取投资产品信息(By zhangerxin)
     * @param productMessageBean
     * @return
     */
    public ProductMessageBean queryProductById(ProductMessageBean productMessageBean);

    /**
     * 根据id获取投资产品信息(App端)
     * @param productMessageBean
     * @return
     */
    public InvestProductMessageBean queryInvestProductById(ProductMessageBean productMessageBean);

    /**
     * 新增产品(By zhangerxin)
     * @param productMes
     * @return
     */
    public ProductBusinessBean saveProduct(ProductMessageBean productMes);

    /**
     * 修改产品(By zhangerxin)
     * @param productMes
     * @return
     */
    public ProductBusinessBean updateProduct(ProductMessageBean productMes);

    /**
     * 设置状态(上架、下架)(By zhangerxin)
     * @param productMes
     * @return
     */
    public ProductBusinessBean settingProduct(ProductMessageBean productMes);

    /**
     * 设置产品审核状态(By zhangerxin)
     * @param productMes
     * @return
     */
    public ProductBusinessBean setProductShFlag(ProductMessageBean productMes) throws Exception;

    /**
     * 获取产品数量后台(By zhangerxin)
     * @param productMes
     * @return
     */
    public int getCountProductByPage(ProductMessageBean productMes);

    /**
     * 查询产品(前台)(By zhangerxin)
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public List<ProductMessageBean> getListBySome(String orderColumn,String orderType,String pageIndex,String pageSize,String typeId);

    /**
     * 查询产品信息(前台)(By zhangerxin)
     * @param orderColumn
     * @param orderType
     * @param productMes
     * @return
     */
    public List<ProductMessageBean> getFrontList(String orderColumn,String orderType,ProductMessageBean productMes);


    /**
     * 查询推荐产品(By zhangerxin)
     * @return
     */
    public ProductRecommendMessageBean getRecommend();

    /**
     * 按照类别获取产品
     */
    public List<ProductMessageBean> queryProductByType(String productType);

    /**
     * 获取交易金额总和
     * @param productMessageBean
     * @return
     */
    public ProductMessageBean getTradePriceSum(ProductMessageBean productMessageBean);

    /**
     *将要到期借款标数量 by cuibin
     */
    public int getCountDueDate();

    /**
     * 获取推荐的产品(App部分)
     * @return
     */
    public AppHomeMessageBean getAppRecommentProduct();

    /**
     * 获取产品列表(App部分)
     * @param appProductMessageBean
     * @return
     */
    public List<AppProductMessageBean> getAppProductList(AppProductMessageBean appProductMessageBean);

    /**
     * 获取推荐产品(微信部分) by cuibin
     */
    public List<WeixinProductMessageBean> getWeixinProductList(WeixinProductMessageBean weixinProductMes);

    /**
     * 查看产品详情(微信部分) by cuibin
     */
    public WeixinProductMessageBean productDetails(WeixinProductMessageBean weixinProductMes);

    /**
     * 查看借款信息(微信部分) by cuibin
     */
    public WeixinProductMemberMes openProductBorrow(WeixinProductMemberMes weixinProductMemberMes);

    /**
     * 查看投资记录(微信部分) by cuibin
     */
    public List<WeixinProductBuyMes> openLoanDetails(WeixinProductBuyMes weixinProductBuyMes);

    /**
     * 根据产品id获取产品详情(App部分)
     * @param productId
     * @return
     */
    public AppProductDetailMessageBean getProductDetail(String productId);

    /**
     * 获得产品总数
     */
    public String getWeixinProductCount(WeixinProductMessageBean weixinProductMes);

    /**
     * 根据产品id获取产品申请人信息(App部分)
     * @param productId
     * @return
     */
    public AppProductMemberMessageBean getProductMemberInfo(String productId);

    /**
     * 获取产品投资情况(App部分)
     * @param productId
     * @return
     */
    public AppProductBuyTjMessageBean getProductBuyTj(String productId);

    /**
     * 获取产品投资记录(App部分)
     * @param productId
     * @return
     */
    public List<AppProductBuyMessageBean> getProductBuyList(String productId,String pageIndex,String pageSize);


    /**
     * 查询债权转让列表 (微信部分) by cuibin
     */
    public List<WeixinProductAttornDetail> queryProductAttornByType(WeixinProductAttornDetail weixinProductAttornDetail);

    /**
     * 查询债权转让列表总数 (微信部分) by cuibin
     */
    public String queryProductAttornByTypeCount(WeixinProductAttornDetail weixinProductAttornDetail);

    /**
     * 查看债权转让详情(微信部分) by cuibin
     */
    public WeixinProductAttornDetail queryAttornDetails(String id);
    /**
     * 查询产品列表 带分页 by cuibin
     */
    public List<ProductMessageBean> queryProductListByPage(ProductMessageBean productMessageBean);
    /**
     * 查询产品列表 总数 by cuibin
     */
    public String queryProductListByPageCount(ProductMessageBean productMessageBean);

    /**
     * 项目登记前查询
     * @param productHuanXunMessageBean
     * @return
     */
    public List<ProductHuanXunMessageBean> queryProductDjInfo(ProductHuanXunMessageBean productHuanXunMessageBean);

    /**
     * 查询投资记录 by cuibin
     */
    public List<ProductBuyMessageBean> queryInvestHistory(ProductBuyMessageBean productBuyMessageBean);

    /**
     * 查询投资记录 总数 by cuibin
     */
    public String queryInvestHistoryCount(ProductBuyMessageBean productMessageBean);

    /**
     * 根据产品id查询需投资产品信息
     * @param productId
     * @return
     */
    AppProductDetailMessageBean getInvestedProductInfo(String productId);

    /**
     * 设置为推荐项目
     * @param newShare
     * @return
     */
    String setNewShare(String productId,String newShare);

    /**
     * 获取推荐项目集合
     * @param newShsre
     * @return
     */
    List<Map<String,String>> getNewShareList(String newShsre);
}
