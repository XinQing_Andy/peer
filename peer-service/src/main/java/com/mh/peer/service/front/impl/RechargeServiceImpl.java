package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.RechargeDao;
import com.mh.peer.dao.member.AllMemberDao;
import com.mh.peer.model.entity.ChargeRecord;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.SerialnumberWh;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.ChargeRecordMessageBean;
import com.mh.peer.model.message.RechargeHistoryMessageBean;
import com.mh.peer.model.message.TradeDetailMessageBean;
import com.mh.peer.model.message.ViewHuanxunRechargeMes;
import com.mh.peer.service.front.RechargeService;
import com.mh.peer.util.ServletUtil;
import com.mh.peer.util.TradeHandle;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Cuibin on 2016/6/17.
 */

@Service
public class RechargeServiceImpl implements RechargeService {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private AllMemberDao allMemberDao;
    @Autowired
    private RechargeDao rechargeDao;


    /**
     * 充值
     */
    @Override
    public boolean userRecharge(ChargeRecordMessageBean chargeRecordMes, TradeDetailMessageBean tradeDetailMessageBean) {

        String jyTime = StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss"); // 交易时间
        String memberid = chargeRecordMes.getMemberid();//会员id

        /*************** 充值表 ***************/
        ChargeRecord chargeRecord = new ChargeRecord();
        String chargeRecordId = UUID.randomUUID().toString().replaceAll("\\-", "");
        chargeRecord.setId(chargeRecordId); //主键
        chargeRecord.setMerbillno(chargeRecordMes.getMerbillno()); //商户订单号 环迅
        chargeRecord.setProcedurefee(Double.parseDouble(chargeRecordMes.getProcedurefee())); //实际充值金额 环迅
        chargeRecord.setFlag(chargeRecordMes.getFlag());//充值状态(0-充值成功、1-充值失败) 环迅
        chargeRecord.setSource(chargeRecordMes.getSource());//来源(0-电脑端、1-手机端)
        chargeRecord.setTime(jyTime);//充值时间
        chargeRecord.setMemberid(memberid);//会员id
        chargeRecord.setCreatetime(jyTime); //创建时间
        chargeRecord.setCreateuser(memberid);//会员id
        chargeRecord.setSprice(Double.parseDouble(chargeRecordMes.getSprice()));//实际充值金额
        chargeRecord.setProcedurefee(Double.parseDouble(chargeRecordMes.getProcedurefee()));//手续费
        /*************** 充值表 ***************/

        /*************** 流水表 ***************/
        TradeHandle tradeHandle = new TradeHandle();//流水号工具类
        int serialNumber = tradeHandle.getSerialNumber(); //流水号
        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setId(UUID.randomUUID().toString().replaceAll("\\-", "")); //主键
        tradeDetail.setGlid(chargeRecordId);//关联id
        tradeDetail.setSerialnum(serialNumber);//流水号
        tradeDetail.setType("0");//交易类型(0-充值、1-提现)
        tradeDetail.setSprice(Double.parseDouble(chargeRecordMes.getSprice()));//实际充值金额 环迅
        tradeDetail.setProcedurefee(Double.parseDouble(tradeDetailMessageBean.getProcedurefee()));//手续费
        tradeDetail.setFlag(chargeRecordMes.getFlag());//充值状态(0-充值成功、1-充值失败) 环迅
        tradeDetail.setMemberid(memberid);//会员id
        tradeDetail.setSource(chargeRecordMes.getSource());//来源(0-电脑端、1-手机端)
        tradeDetail.setTime(jyTime);//交易时间
        tradeDetail.setBz("环迅支付充值");//备注
        tradeDetail.setCreateuser(memberid);//会员id
        tradeDetail.setCreatetime(jyTime);//创建时间
        tradeDetail.setBalance(Double.parseDouble(tradeDetailMessageBean.getBalance()));//本次余额
        /*************** 流水表 ***************/

        /*************** 流水号维护表 ***************/
        SerialnumberWh serialnumberWh = new SerialnumberWh();
        serialnumberWh.setNumber(serialNumber);
        serialnumberWh.setId("6d82cfe3347511e69b6d00163e0063ab");
        /*************** 流水号维护表 ***************/

        /*************** 会员表 ***************/
        DMap dMap = allMemberDao.queryOnlyMember(memberid);
        double newBalance = 0;
        if (dMap.getCount() > 0) {
            String balance = dMap.getValue("BALANCE", 0);
            newBalance = Double.parseDouble(balance) + Double.parseDouble(chargeRecordMes.getSprice());
        }

        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(memberid);
        memberInfo.setBalance(newBalance);
        /*************** 会员表 ***************/


        return rechargeDao.userRecharge(chargeRecord, tradeDetail, memberInfo, serialnumberWh);
    }

    /**
     * 查询充值记录
     */
    @Override
    public List<RechargeHistoryMessageBean> queryRechargeHistory(RechargeHistoryMessageBean rechargeHistoryMessageBean) {
        DMap dMap = rechargeDao.queryRechargeHistory(rechargeHistoryMessageBean);
        List<RechargeHistoryMessageBean> list = new ArrayList<RechargeHistoryMessageBean>();

        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                RechargeHistoryMessageBean temp = new RechargeHistoryMessageBean();
                temp.setMerbillno(dMap.getValue("MERBILLNO", i));
                temp.setSprice(dMap.getValue("SPRICE", i));
                temp.setProcedurefee(dMap.getValue("PROCEDUREFEE", i));
                temp.setSpriceSum(dMap.getValue("SPRICESUM", i));
                temp.setFlag(dMap.getValue("FLAG", i));
                temp.setMemberid(dMap.getValue("MEMBERID", i));
                temp.setSource(dMap.getValue("SOURCE", i));
                String time = dMap.getValue("TIME", i);
                if (time.length() > 0) {
                    time = time.substring(0, 4) + "-" + time.substring(4, 6) + "-" + time.substring(6, 8);
                }
                temp.setTime(time);
                temp.setCreateuser(dMap.getValue("CREATEUSER", i));
                temp.setCreatetime(dMap.getValue("CREATETIME", i));
                list.add(temp);
            }
        }
        return list;
    }

    /**
     * 充值记录查询 重做by zhangerxin
     * @param viewHuanxunRechargeMes
     * @return
     */
    @Override
    public List<ViewHuanxunRechargeMes> queryViewRechargeHistory(ViewHuanxunRechargeMes viewHuanxunRechargeMes) {
        DMap dMap = rechargeDao.queryViewRechargeHistory(viewHuanxunRechargeMes);
        List<ViewHuanxunRechargeMes> listHuanxunRechargeMes = new ArrayList<ViewHuanxunRechargeMes>();
        String ipsDoTime = ""; //IPS处理时间
        if (dMap != null &&dMap.getCount() > 0) {
            for (int i=0;i<dMap.getCount();i++) {
                ViewHuanxunRechargeMes huanxunRechargeMes = new ViewHuanxunRechargeMes();
                huanxunRechargeMes.setId(dMap.getValue("ID",i)); //充值主键
                huanxunRechargeMes.setIpsbillno(dMap.getValue("IPSBILLNO",i)); //充值订单编号
                huanxunRechargeMes.setIpstrdamt(dMap.getValue("IPSTRDAMT",i)); //用户IPS实际到账金额

                ipsDoTime = dMap.getValue("IPSDOTIME",i); //IPS处理时间
                if (ipsDoTime != null && !ipsDoTime.equals("")){
                    ipsDoTime = ipsDoTime.substring(0,10);
                }
                huanxunRechargeMes.setIpsdotime(ipsDoTime);

                huanxunRechargeMes.setResultcode(dMap.getValue("RESULTCODE",i)); //响应代码
                huanxunRechargeMes.setResultmsg(dMap.getValue("RESULTMSG",i)); //充值信息
                listHuanxunRechargeMes.add(huanxunRechargeMes);
            }
        }
        return listHuanxunRechargeMes;
    }

    /**
     * 充值记录数量
     * @param viewHuanxunRechargeMes
     * @return
     */
    @Override
    public int queryViewRechargeHistoryCount(ViewHuanxunRechargeMes viewHuanxunRechargeMes) {
        DMap dMap = rechargeDao.queryViewRechargeHistoryCount(viewHuanxunRechargeMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }

    /**
     * 查询充值记录
     */
    @Override
    public String queryRechargeHistoryCount(RechargeHistoryMessageBean rechargeHistoryMessageBean) {
        DMap dMap = rechargeDao.queryRechargeHistoryCount(rechargeHistoryMessageBean);
        return dMap.getValue("COUNT", 0);
    }

    /**
     * 查询 实际金额 和 充值金额
     */
    @Override
    public String[] queryRechargeSum(String memberId) {
        DMap dMap = rechargeDao.queryRechargeSum(memberId);
        String[] arr = new String[2];

        if (dMap.getCount() > 0) {
            arr[0] = dMap.getValue("SPRICESUM", 0);
            arr[1] = dMap.getValue("PROCEDUREFEESUM", 0);
        } else {
            arr[0] = "读取失败";
            arr[1] = "读取失败";
        }
        return arr;
    }

}
