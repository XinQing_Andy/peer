package com.mh.peer.service.front;

import com.mh.peer.model.message.ConTractMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016-8-31.
 * 合同部分
 */
public interface ConTractService {

    /**
     * 获取产品信息
     * @param productId
     * @return
     */
    ConTractMessageBean getProductInfo(String productId);

    /**
     * 获取产品投资信息
     * @param productId
     * @return
     */
    List<ConTractMessageBean> getProductBuyInfoList(String productId);

    /**
     * 债权部分id
     * @param attornId
     * @return
     */
    ConTractMessageBean getAttornInfo(String attornId);
}
