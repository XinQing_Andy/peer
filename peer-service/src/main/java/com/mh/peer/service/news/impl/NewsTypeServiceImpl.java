package com.mh.peer.service.news.impl;

import com.mh.peer.dao.news.NewsTypeDao;
import com.mh.peer.model.business.NewsTypeBusinessBean;
import com.mh.peer.model.entity.NewsType;
import com.mh.peer.model.message.NewsTypeMessageBean;
import com.mh.peer.service.news.NewsTypeService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016/4/4.
 */
@Service
public class NewsTypeServiceImpl implements NewsTypeService {

    @Autowired
    private  NewsTypeDao newsTypeDao;

    /**
     * 获取新闻类别
     * @return
     */
    @Override
    public List<NewsType> getNewsType() {
        NewsType newsType = new NewsType(); //新闻类别
        List<NewsType> ListNewType = new ArrayList<NewsType>(); //新闻类别集合
        ListNewType = newsTypeDao.getNewsType(newsType);
        return ListNewType;
    }

    /**
     * 修改新闻类别
     * @param newsTypeMes
     * @return
     */
    @Override
    public NewsTypeBusinessBean updateNewsType(NewsTypeMessageBean newsTypeMes) {
        NewsTypeBusinessBean newsTypeBusinessBean = new NewsTypeBusinessBean();
        NewsType newsType = new NewsType(); //新闻类别
        newsType.setId(TypeTool.getInt(newsTypeMes.getId())); //主键
        newsType.setName(newsTypeMes.getName()); //名称
        newsType.setFid(TypeTool.getInt(newsTypeMes.getFid())); //父id
        DaoResult result = newsTypeDao.updateNewsType(newsType);
        if(result.getCode() < 0){
            newsTypeBusinessBean.setResult("error");
            newsTypeBusinessBean.setInfo(result.getErrText());
        }else{
            newsTypeBusinessBean.setResult("success");
            newsTypeBusinessBean.setInfo("修改新闻类别成功");
        }
        return newsTypeBusinessBean;
    }


    /**
     * 获取所有一级类别
     * @param newsTypeMes
     * @return
     */
    @Override
    public List<NewsTypeMessageBean> getAllNewsFirstType(NewsTypeMessageBean newsTypeMes) {
        DMap dMap = newsTypeDao.getAllNewsFirstType(newsTypeMes);
        List<NewsTypeMessageBean> listNewsTypeMes = new ArrayList<NewsTypeMessageBean>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                NewsTypeMessageBean newsTypeMessageBean = new NewsTypeMessageBean();
                newsTypeMessageBean.setId(dMap.getValue("ID",i)); //主键
                newsTypeMessageBean.setName(dMap.getValue("NAME",i)); //题目
                newsTypeMessageBean.setFid(dMap.getValue("FID",i)); //父id
                listNewsTypeMes.add(newsTypeMessageBean);
            }
        }
        return listNewsTypeMes;
    }


    /**
     * 根据一级类别获取所有二级类别
     * @param newsTypeMes
     * @return
     */
    @Override
    public List<NewsTypeMessageBean> getAllNewsSecondType(NewsTypeMessageBean newsTypeMes) {
        DMap dMap = newsTypeDao.getAllNewsSecondType(newsTypeMes);
        List<NewsTypeMessageBean> listNewsTypeMes = new ArrayList<NewsTypeMessageBean>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                NewsTypeMessageBean newsTypeMessageBean = new NewsTypeMessageBean();
                newsTypeMessageBean.setId(dMap.getValue("ID",i)); //主键
                newsTypeMessageBean.setName(dMap.getValue("NAME",i)); //题目
                newsTypeMessageBean.setFid(dMap.getValue("FID",i)); //父id
                listNewsTypeMes.add(newsTypeMessageBean);
            }
        }
        return listNewsTypeMes;
    }
}
