package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.ReceiveDao;
import com.mh.peer.model.message.ProductReceiveInfoMessageBean;
import com.mh.peer.model.message.ProductReceiveMessageBean;
import com.mh.peer.service.front.ReceiveService;
import com.mh.peer.util.PublicsTool;
import com.salon.frame.data.DMap;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-8-31.
 */
@Service
public class ReceiveServiceImpl implements ReceiveService{

    @Autowired
    private ReceiveDao receiveDao;

    /**
     * 获取还款信息列表
     * @param buyId
     * @return
     */
    @Override
    public List<ProductReceiveInfoMessageBean> getReceiveInfoList(String buyId,String orderColumn,String orderType,String pageIndex,String pageSize) {
        List<ProductReceiveInfoMessageBean> listProductReceiveInfoMes = new ArrayList<ProductReceiveInfoMessageBean>();
        DMap dMap = receiveDao.getReceiveInfoList(buyId,orderColumn,orderType,pageIndex,pageSize);
        String principal = ""; //本金(String型)
        Double principalDou = 0.0d; //本金(Double型)
        String interest = ""; //利息(String型)
        Double interestDou = 0.0d; //利息(Double型)
        String yReceivePrice; //应还款金额(String型)
        Double yReceivePriceDou = 0.0d; //应还款金额
        String receiveFlag = ""; //回款状态
        String sReceiveTime = ""; //实际回款日期

        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ProductReceiveInfoMessageBean productReceiveInfoMessageBean = new ProductReceiveInfoMessageBean();
                productReceiveInfoMessageBean.setId(dMap.getValue("ID",i)); //主键
                productReceiveInfoMessageBean.setyReceiveTime(dMap.getValue("YRECEIVETIME",i)); //应回款日期
                sReceiveTime = dMap.getValue("SRECEIVETIME",i); //实际回款日期
                if (sReceiveTime != null && !sReceiveTime.equals("")){
                    sReceiveTime = sReceiveTime.substring(0,10);
                }
                productReceiveInfoMessageBean.setSreceivementTime(sReceiveTime); //实际回款日期
                productReceiveInfoMessageBean.setReceivementPrice(dMap.getValue("RECEIVEPRICE",i)); //实际回款金额

                principal = dMap.getValue("PRINCIPAL",i); //本金(String型)
                if (principal != null && !principal.equals("")){
                    principalDou = TypeTool.getDouble(principal); //本金(Double型)
                }
                interest = dMap.getValue("INTEREST",i); //利息(String型)
                if (interest != null && !interest.equals("")){
                    interestDou = TypeTool.getDouble(interest); //利息(Double型)
                }
                yReceivePriceDou = principalDou + interestDou; //应还款金额
                yReceivePriceDou = PublicsTool.round(yReceivePriceDou,2);
                productReceiveInfoMessageBean.setYreceivementPrice(TypeTool.getString(yReceivePriceDou)); //应还款金额

                if (dMap.getValue("SRECEIVETIME",i) != null && !dMap.getValue("SRECEIVETIME",i).equals("")){
                    receiveFlag = "已回款";
                }else{
                    receiveFlag = "待还款";
                }
                productReceiveInfoMessageBean.setReceiveFlag(receiveFlag); //还款状态
                listProductReceiveInfoMes.add(productReceiveInfoMessageBean);
            }
        }
        return listProductReceiveInfoMes;
    }


    /**
     * 根据投资id获取已还款信息数量
     * @param buyId
     * @return
     */
    @Override
    public int getReceiveInfoCount(String buyId) {
        DMap dMap = receiveDao.getReceiveInfoCount(buyId);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 根据条件获取待回款(已回收)明细
     * @param productReceiveMes
     * @return
     */
    @Override
    public List<ProductReceiveMessageBean> getProductReceiveList(ProductReceiveMessageBean productReceiveMes) {
        DMap dMap = receiveDao.getProductReceiveList(productReceiveMes);
        String sumPrice = "0"; //应回款金额(String型)
        Double sumPriceDou = 0.0d; //应回款金额(Double型)
        String receivePrice = "0"; //实际回款金额(String型)
        Double receivePriceDou = 0.0d; //实际回款金额(Double型)
        Double managePriceDou = 0.0d; //管理费
        String sreceiveTime = ""; //实际回款日期
        List<ProductReceiveMessageBean> listProductReceiveMes = new ArrayList<ProductReceiveMessageBean>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ProductReceiveMessageBean productReceiveMessageBean = new ProductReceiveMessageBean();
                productReceiveMessageBean.setId(dMap.getValue("ID",i)); //主键
                productReceiveMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                productReceiveMessageBean.setCode(dMap.getValue("CODE",i)); //编码
                productReceiveMessageBean.setYreceivetime(dMap.getValue("YRECEIVETIME",i)); //应回款日期
                productReceiveMessageBean.setPrincipal(dMap.getValue("PRINCIPAL",i)); //本金
                productReceiveMessageBean.setInterest(dMap.getValue("INTEREST",i)); //利息

                sumPrice = dMap.getValue("SUMPRICE",i); //应回款金额合计
                if (sumPrice != null && !sumPrice.equals("")){
                    sumPriceDou = TypeTool.getDouble(sumPrice);
                }
                productReceiveMessageBean.setSumPrice(sumPrice);

                receivePrice = dMap.getValue("RECEIVEPRICE",i); //实际回款金额(String型)
                if (receivePrice != null && !receivePrice.equals("")){
                    receivePriceDou = TypeTool.getDouble(receivePrice);
                }
                productReceiveMessageBean.setReceiveprice(receivePrice);

                managePriceDou = sumPriceDou - receivePriceDou; //管理费(Double型)
                managePriceDou = PublicsTool.round(managePriceDou,2);
                productReceiveMessageBean.setManagePrice(TypeTool.getString(managePriceDou)); //管理费(String型)

                sreceiveTime = dMap.getValue("SRECEIVETIME",i); //实际回款日期
                if (sreceiveTime != null && !sreceiveTime.equals("")){
                    sreceiveTime = sreceiveTime.substring(0,16);
                }
                productReceiveMessageBean.setSreceivetime(sreceiveTime); //实际回款日期
                productReceiveMessageBean.setOverduedays(dMap.getValue("OVERDUEDAYS",i)); //逾期天数
                listProductReceiveMes.add(productReceiveMessageBean);
            }
        }
        return listProductReceiveMes;
    }


    /**
     * 根据条件获取待还款信息数量
     * @param productReceiveMes
     * @return
     */
    @Override
    public int getProductReceiveListCount(ProductReceiveMessageBean productReceiveMes) {
        DMap dMap = receiveDao.getProductReceiveListCount(productReceiveMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }
}
