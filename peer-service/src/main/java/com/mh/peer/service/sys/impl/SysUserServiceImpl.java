package com.mh.peer.service.sys.impl;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.dao.manager.DeptDao;
import com.mh.peer.dao.manager.RoleDao;
import com.mh.peer.dao.manager.UserDao;
import com.mh.peer.model.entity.SysDept;
import com.mh.peer.model.entity.SysRole;
import com.mh.peer.model.entity.SysUser;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.mh.peer.model.message.SysUserMessageBean;
import com.mh.peer.service.sys.SysUserService;
import com.mh.peer.util.HandleTool;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016/1/27.
 */
@Service
public class SysUserServiceImpl implements SysUserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private DeptDao deptDao;

    /**
     * 获取用户角色
     * @return
     */
    /*@Override
    public List<SysRole> getSysRole() {
        SysRole sysRole = new SysRole();
        List<SysRole> userList = roleDao.getRoleInfo(sysRole);
        return userList;
    }*/

    /**
     * 查询用户信息(根据分页查询)(zhangerxin)
     * @param sysUserMese
     * @return
     */
    @Override
    public List<SysUserMessageBean> getUserInfoByPage(SysUserMessageBean sysUserMese) {
        List<SysUserMessageBean> userMessageBeans = new ArrayList<SysUserMessageBean>();
        DMap parm = userDao.getUserInfoByPage(sysUserMese); //查询用户
        String createTime = ""; //创建时间
        String num = "1";
        if(parm.getCount() > 0){
            for(int i = 0;i < parm.getCount(); i++){
                SysUserMessageBean userMessageBean = new SysUserMessageBean();
                userMessageBean.setId(parm.getValue("ID",i)); //主键
                userMessageBean.setUserName(parm.getValue("USER_NAME",i)); //登录名
                userMessageBean.setRealName(parm.getValue("REAL_NAME",i)); //真实姓名
                userMessageBean.setCphone(parm.getValue("CPHONE",i)); //手机
                userMessageBean.setCode(parm.getValue("CODE",i)); //云盾编码
                userMessageBean.setRoleName(parm.getValue("ROLE_NAME",i)); //用户组名称
                createTime = parm.getValue("CREATE_TIME",i); //创建时间
                if(createTime != null && !createTime.equals("")){
                    createTime = createTime.substring(0,10);
                    userMessageBean.setCreateTime(createTime); //创建时间
                }

                userMessageBean.setIsOnline(parm.getValue("IS_ONLINE",i));
                userMessageBean.setEmail(parm.getValue("EMAIL",i)); //邮箱地址
                userMessageBean.setNum(num); //序号
                num = String.valueOf(Integer.parseInt(num) + 1);
                userMessageBeans.add(userMessageBean);
            }
        }
        return userMessageBeans;
    }


    /**
     * 根据id查询用户
     * @param sysUserMese
     * @return
     */
    @Override
    public SysUserMessageBean queryUserById(SysUserMessageBean sysUserMese) {
        SysUserMessageBean sysUserMes = new SysUserMessageBean();
        DMap dMap = userDao.queryUserById(sysUserMese);
        HandleTool handleTool = new HandleTool();
        String id = handleTool.getParamById(dMap.getValue("ID")); //主键
        String userName = handleTool.getParamById(dMap.getValue("USER_NAME")); //用户名
        String realName = handleTool.getParamById(dMap.getValue("REAL_NAME")); //真实姓名
        String cphone = handleTool.getParamById(dMap.getValue("CPHONE")); //手机
        String roleId = handleTool.getParamById(dMap.getValue("ROLE_ID")); //角色id
        String roleName = handleTool.getParamById(dMap.getValue("ROLE_NAME")); //角色名称
        String isAdmin = handleTool.getParamById(dMap.getValue("IS_ADMIN")); //是否为管理员
        String email = handleTool.getParamById(dMap.getValue("EMAIL")); //邮箱
        String code = handleTool.getParamById(dMap.getValue("CODE")); //云盾编码
        String createTime = handleTool.getParamById(dMap.getValue("CREATE_TIME")); //创建时间

        sysUserMes.setId(id);
        sysUserMes.setUserName(userName);
        sysUserMes.setRealName(realName);
        sysUserMes.setCphone(cphone);
        sysUserMes.setRoleId(roleId);
        sysUserMes.setRoleName(roleName);
        sysUserMes.setIsAdmin(isAdmin);
        sysUserMes.setEmail(email);
        sysUserMes.setCode(code);
        sysUserMes.setCreateTime(createTime);

        return sysUserMes;
    }

    /**
     * 查询用户数量
     * @return
     */
    @Override
    public int getCountUserInfoByPage(SysUserMessageBean sysUserMese) {
        DMap parm = userDao.getCountUserInfoByPage(sysUserMese); //查询用户
        int totalCount = parm.getCount(); //用户数量
        return totalCount;
    }


    /* @Override
    public List<SysDept> getDeptRole() {
        SysDept sysDept = new SysDept();
        List<SysDept> deptList = deptDao.getDeptInfo(sysDept);
        return deptList;
    }*/

    /**
     * 获取用户信息
     * @return
     */
   /* @Override
    public List<SysUserMessageBean> getUserInfo() {
        SysUser user = new SysUser();
        List<SysUserMessageBean> sysUserList = new ArrayList<>(); //用户集合
        DMap result = userDao.getUserInfo(); //用户信息
        *//********** 用户信息集合Start **********//*
        for (int i = 0; i < result.getCount(); i++) {
            SysUserMessageBean message = new SysUserMessageBean();
            message.setId(result.getValue("ID", i)); //用户id
            message.setUserName(result.getValue("USER_NAME", i)); //用户姓名
            message.setPassWord(result.getValue("PASS_WORD", i)); //密码
            message.setDepeId(result.getValue("DEPE_ID", i)); //部门id
            message.setDeptName(result.getValue("DEPT_NAME", i)); //部门名称
            message.setCphone(result.getValue("CPHONE", i)); //手机号码
            message.setOphone(result.getValue("OPHONE", i)); //办公电话号码
            message.setEmail(result.getValue("EMAIL", i)); //邮件地址
            message.setRoleId(result.getValue("ROLE_ID", i)); //角色ID
            message.setRoleName(result.getValue("ROLE_NAME", i)); //角色名称
            sysUserList.add(message);
        }
        *//*********** 用户信息集合End ***********//*
        return sysUserList;
    }*/

    /**
     * 增加用户
     * @param user
     * @return
     */
    @Override
    public LoginBusinessBean addUser(SysUserMessageBean user) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        SysUser sysUser = new SysUser();
        sysUser.setUserName(user.getUserName()); //用户名称
        sysUser.setPassWord(user.getPassWord()); //用户密码
        sysUser.setRealName(user.getRealName()); //真实姓名
        sysUser.setIsAdmin(user.getIsAdmin()); //管理级别(0-超级管理员、1-普通管理员)
        sysUser.setCphone(user.getCphone()); //手机号码
        sysUser.setEmail(user.getEmail()); //邮件地址
        sysUser.setDelFlg(user.getDelFlg()); //是否删除
        sysUser.setIsOnline(user.getIsOnline());
        sysUser.setRoleId(TypeTool.getInt(user.getRoleId())); //角色id
        sysUser.setCreateUserId(user.getCreateUserId()); //创建用户
        sysUser.setCreateTime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

        DaoResult result = userDao.addUser(sysUser);
        if(result.getCode() < 0){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo(result.getErrText());
        }else{
            loginBusinessBean.setResult("success");
            loginBusinessBean.setInfo("添加成功");
        }
        return loginBusinessBean;
    }

    /**
     * 更新用户
     * @param user
     * @return
     */
    @Override
    public LoginBusinessBean updateUser(SysUserMessageBean user) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        SysUser sysUser = new SysUser();
        sysUser.setId(TypeTool.getInt(user.getId())); //用户id
        sysUser.setUserName(user.getUserName()); //用户名称
        sysUser.setCphone(user.getCphone()); //手机号码
        sysUser.setEmail(user.getEmail()); //邮件地址
        sysUser.setRoleId(TypeTool.getInt(user.getRoleId())); //角色id
        sysUser.setUpdateUserId(user.getCreateUserId()); //更新用户
        sysUser.setUpdateTime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss")); //更新时间
        sysUser.setRealName(user.getRealName()); //真实姓名
        sysUser.setIsAdmin(user.getIsAdmin()); //是否为管理员

        DaoResult result = userDao.updateUser(sysUser);
        if(result.getCode() < 0){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo(result.getErrText());
        }else{
            loginBusinessBean.setResult("success");
            loginBusinessBean.setInfo("修改成功");
        }
        return loginBusinessBean;
    }

    /**
     * 删除用户
     * @param user
     * @return
     */
   /* @Override
    public LoginBusinessBean delUser(SysUserMessageBean user) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        SysUser sysUser = new SysUser();
        sysUser.setId(TypeTool.getInt(user.getId())); //用户
        DaoResult result = userDao.delUser(sysUser);
        if(result.getCode() < 0){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo(result.getErrText());
        }else{
            loginBusinessBean.setResult("success");
            loginBusinessBean.setInfo("删除成功");
        }
        return loginBusinessBean;
    }*/


    /**
     * 设置用户状态
     * @param user
     * @return
     */
    @Override
    public LoginBusinessBean settingSysFlag(SysUserMessageBean user) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        SysUser sysUser = new SysUser();
        sysUser.setId(TypeTool.getInt(user.getId())); //主键
        sysUser.setIsOnline(user.getIsOnline()); //状态(0-启动、1-锁定)
        DaoResult result = userDao.settingSysFlag(sysUser);
        if(result.getCode() < 0){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo(result.getErrText());
        }else{
            loginBusinessBean.setResult("success");
            loginBusinessBean.setInfo("设置成功");
        }
        return loginBusinessBean;
    }


    /**
     * 重置用户密码
     * @param user
     * @return
     */
    @Override
    public LoginBusinessBean resetPwd(SysUserMessageBean user) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        SysUser sysUser = new SysUser();
        sysUser.setId(TypeTool.getInt(user.getId())); //主键
        sysUser.setPassWord(user.getPassWord()); //密码

        DaoResult result = userDao.resetPwd(sysUser);
        if(result.getCode() < 0){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo(result.getErrText());
        }else{
            loginBusinessBean.setResult("success");
            loginBusinessBean.setInfo("重置密码成功！");
        }

        return loginBusinessBean;
    }

    /**
     * 验证用户唯一性
     * @param user
     * @return
     */
    @Override
    public int checkUser(SysUserMessageBean user) {
        DMap parm = userDao.checkUser(user); //查询用户
        int totalCount = parm.getCount(); //用户数量
        return totalCount;
    }

}
