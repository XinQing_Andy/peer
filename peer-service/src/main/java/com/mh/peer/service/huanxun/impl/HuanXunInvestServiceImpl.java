package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunInvestDao;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.mh.peer.service.huanxun.HuanXunInvestService;
import com.mh.peer.util.TradeHandle;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-28.
 */
@Service
public class HuanXunInvestServiceImpl implements HuanXunInvestService{

    @Autowired
    private HuanXunInvestDao huanXunInvestDao;

    /**
     * 产品投资
     * @param huanxunFreeze
     * @param productBuyInfo
     * @param tradeDetail
     * @param memberMessage
     */
    @Override
    public void investhxResult(Map<String,Object> mapException,HuanxunFreeze huanxunFreeze, ProductBuyInfo productBuyInfo, TradeDetail tradeDetail, MemberMessage memberMessage) {
        DaoResult daoResult = huanXunInvestDao.investhxResult(mapException,huanxunFreeze,productBuyInfo,tradeDetail,memberMessage);
        if (daoResult.getCode() < 0){
            System.out.println("产品投资失败！");
        }else{
            System.out.println("产品投资成功！");
        }
    }

    /**
     * 获取环迅投资列表信息
     * @param huanXunFreezeMes
     * @return
     */
    @Override
    public List<HuanXunFreezeMessageBean> getHuanXunInvestBySome(HuanXunFreezeMessageBean huanXunFreezeMes) {
        List<HuanXunFreezeMessageBean> listHuanXunFreezeMes = new ArrayList<HuanXunFreezeMessageBean>();
        DMap dMap = huanXunInvestDao.getHuanXunInvestBySome(huanXunFreezeMes);
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                HuanXunFreezeMessageBean huanXunFreezeMessageBean = new HuanXunFreezeMessageBean();
                huanXunFreezeMessageBean.setProjectNo(dMap.getValue("PROJECTNO",i)); //项目ID号
                huanXunFreezeMessageBean.setIpsBillNo(dMap.getValue("IPSBILLNO",i)); //IPS订单号
                huanXunFreezeMessageBean.setIpsAcctNo(dMap.getValue("OUTIPSACCTNO",i)); //转出方IPS账号
                huanXunFreezeMessageBean.setInIpsAcctNo(dMap.getValue("INIPSACCTNO",i)); //转入方IPS账号
                huanXunFreezeMessageBean.setTrdAmt(dMap.getValue("IPSTRDAMT",i)); //冻结金额
                huanXunFreezeMessageBean.setMemberId(dMap.getValue("MEMBERID",i)); //会员id
                listHuanXunFreezeMes.add(huanXunFreezeMessageBean);
            }
        }
        return listHuanXunFreezeMes;
    }

    /**
     * 解冻部分(投资)
     * @param memberInfo
     * @param huanxunFreeze
     */
    @Override
    public void unfreezeResult(Map<String,Object> mapException,MemberInfo memberInfo, HuanxunFreeze huanxunFreeze) {
        huanXunInvestDao.unfreezeResult(mapException,memberInfo,huanxunFreeze); //更新冻结信息
    }
}
