package com.mh.peer.service.platform.impl;

import com.mh.peer.dao.manager.SysFileDao;
import com.mh.peer.dao.platfrom.AdvertDao;
import com.mh.peer.model.business.AdvertContentBusinessBean;
import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.AdvertContentMessageBean;
import com.mh.peer.model.message.AppAdvertMessageBean;
import com.mh.peer.model.message.AppHomeMessageBean;
import com.mh.peer.service.platform.AdvertService;
import com.mh.peer.util.HandleTool;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-11.
 */
@Service
public class AdvertServiceImpl implements AdvertService {

    @Autowired
    private AdvertDao advertDao;
    @Autowired
    private SysFileDao sysFileDao;

    /**
     * 根据分页获取广告
     *
     * @param advertContentMes
     * @return
     */
    @Override
    public List<AdvertContentMessageBean> queryAdvertByPage(AdvertContentMessageBean advertContentMes) {
        List<AdvertContentMessageBean> advertContentMessageBeans = new ArrayList<AdvertContentMessageBean>();
        DMap parm = advertDao.queryAdvertByPage(advertContentMes); //查询广告
        String content = ""; //内容
        String createTime = ""; //创建时间
        int len = 0; //内容长度

        if (parm.getCount() > 0) {
            for (int i = 0; i < parm.getCount(); i++) {
                AdvertContentMessageBean advertContentMessageBean = new AdvertContentMessageBean();
                advertContentMessageBean.setId(parm.getValue("ID", i)); //主键
                advertContentMessageBean.setTypeId(parm.getValue("TYPEID", i)); //类型id
                advertContentMessageBean.setTitle(parm.getValue("TITLE", i)); //题目
                content = parm.getValue("CONTENT", i); //内容
                len = content.length();
                if (len > 15) {
                    content = content.substring(0, 15) + " ......";
                }
                advertContentMessageBean.setContent(content);
                advertContentMessageBean.setAutor(parm.getValue("AUTOR", i)); //作者
                advertContentMessageBean.setSort(parm.getValue("SORT", i)); //排序
                advertContentMessageBean.setFlag(parm.getValue("FLAG", i)); //状态(0-有效、1-无效)
                advertContentMessageBean.setLook_Count(parm.getValue("CREATEUSER", i)); //创建人

                createTime = parm.getValue("CREATETIME", i); //创建时间
                if (createTime != null && !createTime.equals("")){
                    createTime = createTime.substring(0,16);
                }
                advertContentMessageBean.setCreateTime(createTime); //创建时间
                advertContentMessageBeans.add(advertContentMessageBean);
            }
        }
        return advertContentMessageBeans;
    }

    /**
     * 新增广告
     *
     * @param advertContent
     * @return
     */
    @Override
    public AdvertContentBusinessBean saveAdvert(AdvertContent advertContent) {
        AdvertContentBusinessBean advertContentBus = new AdvertContentBusinessBean();
        DaoResult result = advertDao.saveAdvert(advertContent); //新增产品
        if (result.getCode() < 0) {
            advertContentBus.setResult("error");
            advertContentBus.setInfo(result.getErrText());
            return advertContentBus;
        }
        advertContentBus.setResult("success");
        advertContentBus.setInfo("保存成功");
        return advertContentBus;
    }


    /**
     * 根据id获取广告信息
     *
     * @param advertContentMes
     * @return
     */
    @Override
    public AdvertContentMessageBean queryProductById(AdvertContentMessageBean advertContentMes) {
        AdvertContent advertContent = new AdvertContent(); //广告
        DMap dMap = new DMap();

        advertContent.setId(advertContentMes.getId()); //广告主键
        advertContent = advertDao.getAdvertById(advertContent);
        dMap = sysFileDao.querySysFileById(advertContentMes.getId()); //附件信息

        if (advertContent != null) {
            advertContentMes.setId(advertContent.getId()); //主键
            advertContentMes.setTypeId(advertContent.getTypeid()); //类型id
            advertContentMes.setTitle(advertContent.getTitle()); //题目
            advertContentMes.setFlag(advertContent.getFlag()); //状态(0-启用、1-暂停)
            advertContentMes.setContent(advertContent.getContent()); //内容
            advertContentMes.setLook_Count(TypeTool.getString(advertContent.getLookCount())); //浏览次数
            advertContentMes.setAutor(advertContent.getAutor()); //作者
            advertContentMes.setSort(TypeTool.getString(advertContent.getSort())); //排序
            advertContentMes.setCreateUser(advertContent.getCreateuser()); //创建人
            advertContentMes.setCreateTime(advertContent.getCreatetime()); //创建时间
        }

        if (dMap.getCount() > 0) {
            advertContentMes.setFileId(dMap.getValue("ID").substring(1, dMap.getValue("ID").length() - 1)); //附件id
            advertContentMes.setImgUrlJd(dMap.getValue("IMGURLJD").substring(1, dMap.getValue("IMGURLJD").length() - 1)); //附件地址(绝对地址)
            advertContentMes.setImgUrlXd(dMap.getValue("IMGURL").substring(1, dMap.getValue("IMGURL").length() - 1)); //附件地址(相对地址)
        }
        return advertContentMes;
    }

    /**
     * 更新广告
     *
     * @param advertContent
     * @return
     */
    @Override
    public AdvertContentBusinessBean updateAdvert(AdvertContent advertContent) {
        AdvertContentBusinessBean advertContentBusinessBean = new AdvertContentBusinessBean();
        DaoResult result = advertDao.updateAdvert(advertContent);
        if (result.getCode() < 0) {
            advertContentBusinessBean.setResult("error");
            advertContentBusinessBean.setInfo(result.getErrText());
        } else {
            advertContentBusinessBean.setResult("success");
            advertContentBusinessBean.setInfo("修改成功");
        }
        return advertContentBusinessBean;
    }

    /**
     * 查询广告数量
     *
     * @param advertContent
     * @return
     */
    @Override
    public int getCountAdvertByPage(AdvertContentMessageBean advertContent) {
        DMap parm = advertDao.getCountAdvertByPage(advertContent); //查询广告
        int totalCount = parm.getCount(); //广告数量
        return totalCount;
    }

    /**
     * 查询广告(前台)
     *
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<AdvertContentMessageBean> getListBySome(String orderColumn, String orderType, String pageIndex, String pageSize, String typeId) {
        List<AdvertContentMessageBean> advertContentMessageBeans = new ArrayList<AdvertContentMessageBean>(); //广告集合
        DMap parm = advertDao.getListBySome(orderColumn, orderType, pageIndex, pageSize, typeId);
        DMap parmFile = new DMap(); //附件部分
        HandleTool handleTool = new HandleTool();

        if (parm.getCount() > 0) {
            for (int i = 0; i < parm.getCount(); i++) {
                AdvertContentMessageBean advertContentMessageBean = new AdvertContentMessageBean();
                advertContentMessageBean.setId(parm.getValue("ID", i)); //主键
                advertContentMessageBean.setTypeId(parm.getValue("TYPEID", i)); //类型id
                advertContentMessageBean.setTitle(parm.getValue("TITLE", i)); //题目
                advertContentMessageBean.setContent(parm.getValue("CONTENT", i)); //内容
                advertContentMessageBean.setAutor(parm.getValue("AUTOR", i)); //作者
                advertContentMessageBean.setSort(parm.getValue("SORT", i)); //排序
                advertContentMessageBean.setFlag(parm.getValue("FLAG", i)); //状态(0-有效、1-无效)
                advertContentMessageBean.setLook_Count(parm.getValue("LOOK_COUNT", i)); //浏览次数
                advertContentMessageBean.setCreateUser(parm.getValue("CREATEUSER", i)); //创建用户
                advertContentMessageBean.setCreateTime(parm.getValue("CREATETIME", i)); //创建时间
                parmFile = sysFileDao.querySysFileById(parm.getValue("ID", i));
                if (parmFile != null && parmFile.getCount() > 0) {
                    advertContentMessageBean.setImgUrlXd(parmFile.getValue("IMGURL",0));
                } else {
                    advertContentMessageBean.setImgUrlXd("");
                }
                advertContentMessageBeans.add(advertContentMessageBean);
            }
        }
        return advertContentMessageBeans;
    }

    /**
     * 查询广告总数(前台) by cuibin
     */
    @Override
    public String queryAdvertCount(String typeId) {
        DMap dMap = advertDao.queryAdvertCount(typeId);
        return dMap.getValue("COUNT", 0);
    }


    /**
     * 查询更多公告
     * @param advertContentMes
     * @return
     */
    @Override
    public List<AdvertContentMessageBean> queryFrontAdvertList(AdvertContentMessageBean advertContentMes) {
        DMap dMap = advertDao.queryFrontAdvertList(advertContentMes);
        List<AdvertContentMessageBean> listAdvertContentMes = new ArrayList<AdvertContentMessageBean>();
        String createTime = ""; //创建时间
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                AdvertContentMessageBean advertContentMessageBean = new AdvertContentMessageBean();
                advertContentMessageBean.setId(dMap.getValue("ID",i)); //主键
                advertContentMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                createTime = dMap.getValue("CREATETIME",i); //创建时间
                if (createTime != null && !createTime.equals("")){
                    createTime = createTime.substring(0,10);
                }
                advertContentMessageBean.setCreateTime(createTime);
                listAdvertContentMes.add(advertContentMessageBean);
            }
        }
        return listAdvertContentMes;
    }


    /**
     * 前台查询广告数量
     * @param advertContentMes
     * @return
     */
    @Override
    public int queryFrontAdvertCount(AdvertContentMessageBean advertContentMes) {
        DMap dMap = advertDao.queryFrontAdvertCount(advertContentMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 前台查询广告详情
     * @param advertContentMes
     * @return
     */
    @Override
    public AdvertContentMessageBean queryFrontAdvertDetails(AdvertContentMessageBean advertContentMes) {
        DMap dMap = advertDao.queryFrontAdvertDetails(advertContentMes); //查询广告详情
        AdvertContentMessageBean advertContentMessageBean = new AdvertContentMessageBean();
        String createTime = ""; //创建时间
        if (dMap != null && dMap.getCount() > 0){
            advertContentMessageBean.setId(dMap.getValue("ID",0)); //主键
            advertContentMessageBean.setTitle(dMap.getValue("TITLE",0)); //题目
            createTime = dMap.getValue("CREATETIME",0); //创建时间
            if (createTime != null && !createTime.equals("")){
                createTime = createTime.substring(0,10);
            }
            advertContentMessageBean.setCreateTime(createTime);
            advertContentMessageBean.setContent(dMap.getValue("CONTENT",0)); //内容
        }
        return advertContentMessageBean;
    }


    /**
     * 获取首页广告(App端)
     *
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<AppHomeMessageBean> getHomeAdvert(String orderColumn, String orderType, String pageIndex, String pageSize, String typeId) {
        List<AppHomeMessageBean> listAppHomeMes = new ArrayList<AppHomeMessageBean>();
        DMap dMap = advertDao.getHomeAdvert(orderColumn, orderType, pageIndex, pageSize, typeId); //广告集合
        String advertId = ""; //广告id
        String advertFileId = ""; //广告附件id
        String advertFileUrl = ""; //广告附件路径
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                AppHomeMessageBean appHomeMessageBean = new AppHomeMessageBean();
                advertId = dMap.getValue("ID", i); //广告id
                appHomeMessageBean.setAdvertId(advertId); //主键
                appHomeMessageBean.setAdvertTitle(dMap.getValue("TITLE", i)); //题目

                /*************** 广告附件部分Start ***************/
                DMap dMapFile = sysFileDao.querySysFileById(advertId);
                if (dMapFile != null) {
                    advertFileId = dMapFile.getValue("ID", 0); //广告附件id
                    advertFileUrl = dMapFile.getValue("IMGURL", 0); //广告附件地址
                    appHomeMessageBean.setAdvertFileId(advertFileId);
                    appHomeMessageBean.setAdvertFileUrl(advertFileUrl);
                }
                /**************** 广告附件部分End ****************/

                listAppHomeMes.add(appHomeMessageBean);
            }
        }
        return listAppHomeMes;
    }

    /**
     * 获取广告列表(App端)
     *
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @param orderColumn
     * @param orderType
     * @return
     */
    @Override
    public List<AppAdvertMessageBean> getAdvert(String typeId, String pageIndex, String pageSize, String orderColumn, String orderType) {
        List<AppAdvertMessageBean> listAppAdvertMes = new ArrayList<AppAdvertMessageBean>();
        DMap dMap = advertDao.getAdvert(typeId, pageIndex, pageSize, orderColumn, orderType);
        String title = ""; //广告题目
        String content = ""; //广告内容
        String createTime = ""; //创建时间
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                AppAdvertMessageBean appAdvertMessageBean = new AppAdvertMessageBean();
                appAdvertMessageBean.setId(dMap.getValue("ID", i)); //主键
                appAdvertMessageBean.setTypeId(dMap.getValue("TYPEID", i)); //产品类型id

                title = dMap.getValue("TITLE", i); //题目
                if (title != null && !title.equals("")) {
                    if (title.length() > 10) {
                        title = title.substring(0, 8) + "..";
                    }
                }
                appAdvertMessageBean.setTitle(title); //题目

                content = dMap.getValue("CONTENT", i); //广告内容
                if (content != null && !content.equals("")) {
                    if (content.length() > 10) {
                        content = content.substring(0, 8) + "..";
                    }
                }
                appAdvertMessageBean.setContent(content); //内容

                createTime = dMap.getValue("CREATETIME", i); //创建时间
                if (createTime != null && !createTime.equals("")) {
                    if (createTime.length() > 10) {
                        createTime = createTime.substring(0, 10);
                    }
                }
                appAdvertMessageBean.setCreateTime(createTime);
                listAppAdvertMes.add(appAdvertMessageBean);
            }
        }
        return listAppAdvertMes;
    }

    /**
     * 获取广告详情(App端)
     *
     * @param advertId
     * @return
     */
    @Override
    public AppAdvertMessageBean getAdvertDetail(String advertId) {
        DMap dMap = advertDao.getAdvertDetail(advertId); //广告详情
        AppAdvertMessageBean appAdvertMessageBean = new AppAdvertMessageBean(); //广告部分
        String createTime = ""; //题目
        if (dMap != null) {
            appAdvertMessageBean.setId(dMap.getValue("ID", 0)); //主键
            appAdvertMessageBean.setTitle(dMap.getValue("TITLE", 0)); //题目

            createTime = dMap.getValue("CREATETIME", 0); //创建时间
            if (createTime != null && !createTime.equals("")) {
                createTime = createTime.substring(0, 10);
            }
            appAdvertMessageBean.setCreateTime(createTime); //创建时间
            appAdvertMessageBean.setContent(dMap.getValue("CONTENT", 0)); //内容
        }
        return appAdvertMessageBean;
    }

}
