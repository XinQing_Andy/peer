package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.PersonageSettingDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.model.message.SysCityMessagebean;
import com.mh.peer.model.message.SysFileMessageBean;
import com.mh.peer.model.message.SysProvinceMessageBean;
import com.mh.peer.service.front.PersonageSettingService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Cuibin on 2016/5/1.
 */
@Service
public class PersonageSettingServiceImpl implements PersonageSettingService{
    @Autowired
    private PersonageSettingDao personageSettingDao;
    @Autowired
    private ServletUtil servletUtil;
    /**
     * 查询一个用户基本资料
     */
    @Override
    public MemberInfoMessageBean queryOnlyMemberInfo() {
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        String userId = memberInfo.getId();
        DMap dMap = personageSettingDao.queryOnlyMemberInfo(userId);
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(dMap.getCount() > 0){
            memberInfoMessageBean.setId(dMap.getValue("ID",0));
            memberInfoMessageBean.setUsername(dMap.getValue("USERNAME",0));
            memberInfoMessageBean.setNickname(dMap.getValue("NICKNAME", 0));
            memberInfoMessageBean.setRealname(dMap.getValue("REALNAME", 0));
            memberInfoMessageBean.setPassword(dMap.getValue("PASSWORD", 0));
            memberInfoMessageBean.setIdcard(dMap.getValue("IDCARD", 0));
            memberInfoMessageBean.setTelephone(dMap.getValue("TELEPHONE", 0));
            memberInfoMessageBean.setEmail(dMap.getValue("EMAIL", 0));
            memberInfoMessageBean.setSex(dMap.getValue("SEX", 0));
            memberInfoMessageBean.setBirthday(dMap.getValue("BIRTHDAY", 0));
            memberInfoMessageBean.setProvince(dMap.getValue("PROVINCE", 0)); //省名称
            memberInfoMessageBean.setCity(dMap.getValue("CITY", 0));
            memberInfoMessageBean.setDetailAddress(dMap.getValue("DETAIL_ADDRESS", 0));
            memberInfoMessageBean.setDregree(dMap.getValue("DREGREE", 0));
            memberInfoMessageBean.setSchool(dMap.getValue("SCHOOL", 0));
            memberInfoMessageBean.setMarryStatus(dMap.getValue("MARRY_STATUS", 0));
            memberInfoMessageBean.setChildren(dMap.getValue("CHILDREN", 0));
            memberInfoMessageBean.setMonthSalary(dMap.getValue("MONTH_SALARY", 0));
            memberInfoMessageBean.setCarFlg(dMap.getValue("CAR_FLG", 0));
            memberInfoMessageBean.setHouseCondition(dMap.getValue("HOUSE_CONDITION", 0));
            memberInfoMessageBean.setSocialSecurity(dMap.getValue("SOCIAL_SECURITY", 0));
        }
        return memberInfoMessageBean;
    }
    /**
     * 查询所有省份
     */
    @Override
    public List queryAllProvince() {
        DMap dMap = personageSettingDao.queryAllProvince();
        List list = new ArrayList();
        SysProvinceMessageBean sysProvinceMessageBean = null;
        if(dMap.getCount() > 0){
            for (int i = 0; i < dMap.getCount() ; i++) {
                sysProvinceMessageBean = new SysProvinceMessageBean();
                sysProvinceMessageBean.setId(dMap.getValue("ID",i));
                sysProvinceMessageBean.setProvinceName(dMap.getValue("PROVINCE_NAME",i));
                sysProvinceMessageBean.setSort(dMap.getValue("SORT",i));
                list.add(sysProvinceMessageBean);
            }
        }
        return list;
    }

    /**
     * 查询城市
     */
    @Override
    public List<SysCityMessagebean> queryCity(String provinceid) {
        DMap dMap = personageSettingDao.queryCity(provinceid);
        List<SysCityMessagebean> list = new ArrayList<SysCityMessagebean>();
        if(dMap.getCount() > 0){
            for (int i = 0; i < dMap.getCount(); i++) {
                SysCityMessagebean sysCityMessagebean = new SysCityMessagebean();
                sysCityMessagebean.setId(dMap.getValue("ID",i));
                sysCityMessagebean.setProvinceid(dMap.getValue("PROVINCEID",i));
                sysCityMessagebean.setCityname(dMap.getValue("CITYNAME", i));
                sysCityMessagebean.setAreacode(dMap.getValue("AREACODE", i));
                list.add(sysCityMessagebean);
            }
        }
        return list;
    }

    /**
     * 更新个人基本资料
     */
    @Override
    public MemberInfoMessageBean updatePersona(MemberInfo memberInfo) {
        HttpSession session = servletUtil.getSession();
        MemberInfo m = (MemberInfo) session.getAttribute("sessionMemberInfo");
        String userId = m.getId();
        memberInfo.setId(userId);
        DaoResult daoResult = personageSettingDao.updatePersona(memberInfo);
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(daoResult.getCode() < 0){
            memberInfoMessageBean.setResult("error");
            memberInfoMessageBean.setInfo("更新失败");
        }else{
            memberInfoMessageBean.setResult("success");
            memberInfoMessageBean.setInfo("更新成功");
        }
        return memberInfoMessageBean;
    }




    /**
     * 保存头像
     */
    @Override
    public void updatePortrait(String path,String absPath,String uuid) {
        SysFile sysFile = new SysFile();
        sysFile.setId(UUID.randomUUID().toString().replaceAll("\\-",""));
        sysFile.setGlid(uuid);
        sysFile.setImgurl(path);
        sysFile.setImgurljd(absPath);
        sysFile.setCreateuser(uuid);
        sysFile.setType("0");
        sysFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyyMMdd"));
        sysFile.setSort(0);
        DaoResult result = personageSettingDao.updatePortrait(sysFile);
    }
    /**
     * 删除头像
     */
    @Override
    public void deletePortrait(String imgId) {
        SysFile sysFile = new SysFile();
        sysFile.setId(imgId);
        personageSettingDao.deletePortrait(sysFile);
    }

    /**
     * 查询头像信息
     */
    @Override
    public SysFileMessageBean queryOnlyPortrait(String userId) {
        DMap dMap = personageSettingDao.queryOnlyPortrait(userId);
        SysFileMessageBean sfmb = new SysFileMessageBean();
        if(dMap.getCount() > 0){
            sfmb.setImgurljd(dMap.getValue("IMGURLJD",0));
            sfmb.setId(dMap.getValue("ID",0));
            sfmb.setImgurl(dMap.getValue("IMGURL", 0));
        }
        return sfmb;
    }
}
