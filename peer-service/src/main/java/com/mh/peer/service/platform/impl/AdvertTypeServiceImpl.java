package com.mh.peer.service.platform.impl;

import com.mh.peer.dao.platfrom.AdvertTypeDao;
import com.mh.peer.model.entity.AdvertType;
import com.mh.peer.service.platform.AdvertTypeService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-12.
 */
@Service
public class AdvertTypeServiceImpl implements AdvertTypeService {

    @Autowired
    private AdvertTypeDao advertTypeDao;

    /**
     * 查询所有广告类别
     * @return
     */
    @Override
    public List<AdvertType> getAllAdvertType() {
        DMap parm = advertTypeDao.getAllAdvertType();
        String createTime = ""; //创建时间

        List<AdvertType> listAdvertTypes = new ArrayList<AdvertType>();
        if(parm.getCount() > 0){
            for (int i=0;i<parm.getCount();i++){
                AdvertType advertType = new AdvertType();
                advertType.setId(TypeTool.getInt(parm.getValue("ID", i))); //主键
                advertType.setName(parm.getValue("NAME", i)); //广告类别
                advertType.setFid(parm.getValue("FID", i)); //父id
                advertType.setCreateuser(TypeTool.getInt(parm.getValue("CREATEUSER", i))); //创建用户

                createTime = parm.getValue("CREATETIME", i);
                if (createTime != null && !createTime.equals("")){
                    createTime = createTime.substring(0,16);
                }
                advertType.setCreatetime(createTime); //创建时间
                listAdvertTypes.add(advertType);
            }
        }
        return listAdvertTypes;
    }
}
