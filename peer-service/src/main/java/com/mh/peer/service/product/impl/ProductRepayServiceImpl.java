package com.mh.peer.service.product.impl;

import com.mh.peer.dao.product.ProductRepayDao;
import com.mh.peer.model.message.*;
import com.mh.peer.service.product.ProductRepayService;
import com.mh.peer.util.PublicsTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-5-27.
 * 回款列表
 */
@Service
public class ProductRepayServiceImpl implements ProductRepayService {

    @Autowired
    private ProductRepayDao productRepayDao;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 根据分页获取还款列表
     * @param productRepayMessageBean
     * @return
     */
    @Override
    public List<ProductRepayMessageBean> getRepayListByPage(ProductRepayMessageBean productRepayMessageBean) {
        List<ProductRepayMessageBean> listProductRepayMes = new ArrayList<ProductRepayMessageBean>(); //还款集合
        DMap dMap = productRepayDao.getRepayListBySome(productRepayMessageBean); //还款集合
        if(dMap.getCount() > 0){
            for(int i=0;i<dMap.getCount();i++){
                ProductRepayMessageBean productRepayMes = new ProductRepayMessageBean();
                productRepayMes.setId(dMap.getValue("ID",i)); //主键
                productRepayMes.setProductId(dMap.getValue("PRODUCTID",i)); //产品id
                productRepayMes.setPrincipal(dMap.getValue("PRINCIPAL",i)); //本金
                productRepayMes.setInterest(dMap.getValue("INTEREST",i)); //利息
                productRepayMes.setYrepaymentTime(dMap.getValue("YREPAYMENTTIME",i)); //应还款日期
                productRepayMes.setYprice(dMap.getValue("YPRICE",i)); //应还本息
                productRepayMes.setRepaymentPrice(dMap.getValue("REPAYMENTPRICE",i)); //实际还款价格
                productRepayMes.setSrepaymentTime(dMap.getValue("SREPAYMENTTIME",i)); //实际还款日期
                productRepayMes.setCreateTime(dMap.getValue("CREATETIME",i)); //创建日期
                productRepayMes.setUpdateTime(dMap.getValue("UPDATETIME",i)); //最后更新日期
                listProductRepayMes.add(productRepayMes);
            }
        }
        return listProductRepayMes;
    }


    /**
     * 获取还款部分数量
     * @param productRepayMessageBean
     * @return
     */
    @Override
    public int getRepayCount(ProductRepayMessageBean productRepayMessageBean) {
        DMap dMap = productRepayDao.getRepayCount(productRepayMessageBean);
        int totalCount = dMap.getCount();
        return totalCount;
    }

    /**
     * 获取还款总额
     * @param productRepayMessageBean
     * @return
     */
    @Override
    public ProductRepayMessageBean getRepayPriceSum(ProductRepayMessageBean productRepayMessageBean) {
        String priceSum = "0"; //应还款总额
        DMap dMap = productRepayDao.getRepayPriceSum(productRepayMessageBean);
        if(dMap.getValue("YPRICESUM",0) != null){
            productRepayMessageBean.setPriceSum(dMap.getValue("YPRICESUM",0)); //还款总额
        }
        return productRepayMessageBean;
    }

    /**
     * 待还总额 & 到期还款金额 by cuibin
     */
    @Override
    public String getRepayedPrice(ProductRepayMessageBean productRepayMessageBean, String repayFlg) {
        return productRepayDao.getRepayedPrice(productRepayMessageBean, repayFlg).getValue("ZS",0);
    }

    /**
     * 获取待还(已还)金额、待还(已还)期数(App端)
     * @param memberId
     * @param repayFlag
     * @return
     */
    @Override
    public AppProductRepayTjMessageBean getAppRepayInfo(String memberId, String repayFlag) {
        AppProductRepayTjMessageBean appProductRepayTjMessageBean = productRepayDao.getAppRepayInfo(memberId,repayFlag);
        return appProductRepayTjMessageBean;
    }


    /**
     * 获取待还(已还)列表(App端)
     * @param memberId
     * @param repayFlag
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<AppProductRepayMessageBean> getAppRepayList(String memberId, String repayFlag, String pageIndex, String pageSize) {
        List<AppProductRepayMessageBean> listAppProductRepayMes = new ArrayList<AppProductRepayMessageBean>();
        DMap dMap = productRepayDao.getAppRepayList(memberId,repayFlag,pageIndex,pageSize);
        String sRepayTime = ""; //实际还款日期

        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                AppProductRepayMessageBean appProductRepayMessageBean = new AppProductRepayMessageBean();
                appProductRepayMessageBean.setId(dMap.getValue("ID",i)); //主键
                appProductRepayMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                appProductRepayMessageBean.setRepayPrice(dMap.getValue("REPAYPRICE",i)); //应还款金额
                appProductRepayMessageBean.setYrepaymentTime(dMap.getValue("YREPAYMENTTIME",i)); //应还款时间
                appProductRepayMessageBean.setRepaymentPrice(dMap.getValue("REPAYMENTPRICE",i)); //实际还款金额

                sRepayTime = dMap.getValue("SREPAYMENTTIME",i); //实际还款日期
                if (sRepayTime != null && !sRepayTime.equals("")){
                    sRepayTime = sRepayTime.substring(0,10);
                }
                appProductRepayMessageBean.setRepaymentTime(sRepayTime);
                listAppProductRepayMes.add(appProductRepayMessageBean);
            }
        }
        return listAppProductRepayMes;
    }

    /**
     * 获取产品还款详细信息
     * @param productRepayId
     * @return
     */
    @Override
    public AppProductRepayMessageBean getAppRepayDetails(String productRepayId) {
        DMap dMap = productRepayDao.getAppRepayDetails(productRepayId);
        AppProductRepayMessageBean appProductRepayMessageBean = new AppProductRepayMessageBean();
        String sRepayTime = ""; //实际还款日期

        if (dMap != null) {
            appProductRepayMessageBean.setId(dMap.getValue("ID",0)); //主键
            appProductRepayMessageBean.setTitle(dMap.getValue("TITLE",0)); //题目
            appProductRepayMessageBean.setCode(dMap.getValue("CODE", 0)); //编码
            appProductRepayMessageBean.setPrincipal(dMap.getValue("PRINCIPAL", 0)); //待还本金
            appProductRepayMessageBean.setInterest(dMap.getValue("INTEREST", 0)); //待还利息
            appProductRepayMessageBean.setRepayPrice(dMap.getValue("REPAYPRICE",0)); //待还总额
            appProductRepayMessageBean.setYrepaymentTime(dMap.getValue("YREPAYMENTTIME",0)); //应还款日期
            appProductRepayMessageBean.setRepaymentPrice(dMap.getValue("REPAYMENTPRICE",0)); //实际还款金额

            sRepayTime = dMap.getValue("SREPAYMENTTIME",0); //实际还款日期
            if (sRepayTime != null && !sRepayTime.equals("")){
                sRepayTime = sRepayTime.substring(0,10);
            }
            appProductRepayMessageBean.setRepaymentTime(sRepayTime);
        }
        return appProductRepayMessageBean;
    }


    /**
     * 获取逾期信息集合
     * @param overRepayMes
     * @return
     */
    @Override
    public List<ProductOverRepayMessageBean> getOverDueRepayList(ProductOverRepayMessageBean overRepayMes) {
        DMap dMap = productRepayDao.getOverDueRepayList(overRepayMes);
        List<ProductOverRepayMessageBean> listOverDueRepayMes = new ArrayList<ProductOverRepayMessageBean>();
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                ProductOverRepayMessageBean productOverRepayMessageBean = new ProductOverRepayMessageBean();
                productOverRepayMessageBean.setId(dMap.getValue("ID", i)); //主键
                productOverRepayMessageBean.setLoanman(dMap.getValue("REALNAME", i)); //项目ID号(项目编号)
                productOverRepayMessageBean.setProductId(dMap.getValue("CODE", i)); //项目名称
                productOverRepayMessageBean.setProductTitle(dMap.getValue("TITLE", i)); //项目名称
                productOverRepayMessageBean.setYrepaymentTime(dMap.getValue("YREPAYMENTTIME", i)); //最小投标金额
                productOverRepayMessageBean.setOverDay(dMap.getValue("OVERDAYS", i)); //最大拆分份数
                productOverRepayMessageBean.setYremoney(dMap.getValue("YPRICE", i)); //贷款利率
                productOverRepayMessageBean.setLoanPrice(dMap.getValue("LOANPRICE", i)); //贷款期限(年)
                productOverRepayMessageBean.setIdcardNumber(dMap.getValue("IDCARD", i)); //贷款期限(年)
                listOverDueRepayMes.add(productOverRepayMessageBean);
            }
        }
        return listOverDueRepayMes;
    }

    /**
     * 查询逾期还款列表总数 by buyatao
     */
    @Override
    public int getOverRepayByPage(ProductOverRepayMessageBean overRepayMes) {
        DMap parm = productRepayDao.getOverRepayByPage(overRepayMes); //查询产品
        int totalCount = parm.getCount(); //广告数量
        return totalCount;
    }

    /**
     * 查询还款明细列表（前台） by buyatao
     * @param productRepayMes
     * @return
     */
    @Override
    public List<ProductRepayMessageBean> getRepayList(ProductRepayMessageBean productRepayMes) {
        DMap dMap = productRepayDao.getRepayDetail(productRepayMes);
        List<ProductRepayMessageBean> repayList = new ArrayList<ProductRepayMessageBean>();
        String sRepaymentTime = ""; //实际还款日期
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                ProductRepayMessageBean productRepayMessageBean = new ProductRepayMessageBean();
                productRepayMessageBean.setId(dMap.getValue("ID", i)); //主键
                productRepayMessageBean.setProductId(dMap.getValue("CODE", i)); //项目编码
                productRepayMessageBean.setProductTitle(dMap.getValue("TITLE", i)); //项目名称
                productRepayMessageBean.setYprice(dMap.getValue("YPRICE", i));
                productRepayMessageBean.setYrepaymentTime(dMap.getValue("YREPAYMENTTIME", i));

                sRepaymentTime = dMap.getValue("SREPAYMENTTIME", i); //实际还款日期
                if (sRepaymentTime != null && !sRepaymentTime.equals("")){
                    sRepaymentTime = sRepaymentTime.substring(0,10);
                }else{
                    sRepaymentTime = "";
                }
                productRepayMessageBean.setSrepaymentTime(sRepaymentTime);

                productRepayMessageBean.setRepaymentPrice( dMap.getValue("REPAYMENTPRICE", i));
                productRepayMessageBean.setRepayflg(dMap.getValue("RECEIVEFLG", i)); //还款状态(0-正常、1-逾期)
                repayList.add(productRepayMessageBean);
            }
        }
        return repayList;
    }

    /**
     * 获取环迅部分应还款信息
     * @param repayId
     * @return
     */
    @Override
    public HuanXunFreezeMessageBean getRepayInfoById(String repayId){
        DMap dMap = productRepayDao.getRepayInfoById(repayId); //获取还款信息
        String principal = ""; //本金(String型)
        Double principalDou = 0.0d; //本金(Double型)
        String interest = ""; //利息(String型)
        Double interestDou = 0.0d; //利息(Double型)
        Double trdAmtDou = 0.0d; //还款金额(Double型)
        HuanXunFreezeMessageBean huanXunFreezeMessageBean = new HuanXunFreezeMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            principal = dMap.getValue("PRINCIPAL",0); //本金(String型)
            if (principal != null && !principal.equals("")){
                principalDou = TypeTool.getDouble(principal); //本金(Double型)
            }
            interest = dMap.getValue("INTEREST",0); //利息(String型)
            if (interest != null && !interest.equals("")){
                interestDou = TypeTool.getDouble(interest); //利息(Double型)
            }
            trdAmtDou = principalDou + interestDou; //还款金额(Double型)
            trdAmtDou = PublicsTool.round(trdAmtDou,2);
            huanXunFreezeMessageBean.setTrdAmt(TypeTool.getString(trdAmtDou)); //还款金额(String型)
            huanXunFreezeMessageBean.setProjectNo(dMap.getValue("PROJECTNO",0)); //项目ID号
            huanXunFreezeMessageBean.setIpsAcctNo(dMap.getValue("REPAYIPSACCTNO",0)); //还款方IPS账号
        }
        return huanXunFreezeMessageBean;
    }


    /**
     * 获取还款中产品信息集合
     * @param repayingMes
     * @return
     */
    @Override
    public List<ProductRepayMessageBean> getRepayingList(ProductRepayMessageBean repayingMes) {
        DMap dMap = productRepayDao.getRepayingList(repayingMes);
        List<ProductRepayMessageBean> listRepayingMes = new ArrayList<ProductRepayMessageBean>();
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                ProductRepayMessageBean productRepayMessageBean = new ProductRepayMessageBean();
                productRepayMessageBean.setId(dMap.getValue("ID", i)); //主键
                productRepayMessageBean.setProductId(dMap.getValue("CODE", i)); //产品编号
                productRepayMessageBean.setProductTitle(dMap.getValue("TITLE", i)); //产品名称
                productRepayMessageBean.setYrepaymentTime(dMap.getValue("YREPAYMENTTIME", i)); //应还款日期
                productRepayMessageBean.setRepayment(dMap.getValue("REPAYMENT", i)); //还款方式
                productRepayMessageBean.setLoadfee(dMap.getValue("LOADFEE", i)); //借款金额
                productRepayMessageBean.setRepayperiods(dMap.getValue("REPAYPERIODS", i)); //还款期数
                productRepayMessageBean.setRealName(dMap.getValue("REALNAME", i)); //真实姓名
                productRepayMessageBean.setLoanrate(dMap.getValue("LOANRATE", i)); //借款费用
                productRepayMessageBean.setNextreceivetime(dMap.getValue("NEXTRECEIVETIME", i)); //下次还款日期
                listRepayingMes.add(productRepayMessageBean);
            }
        }
        return listRepayingMes;
    }


    /**
     * 获取还款中产品信息集合总数 by buyatao
     */
    @Override
    public int getRepayingByPage(ProductRepayMessageBean repayingMes) {
        DMap parm = productRepayDao.getRepayingByPage(repayingMes); //查询产品
        int totalCount = parm.getCount(); //广告数量
        return totalCount;
    }

    /**
     * 获取已结清产品信息集合
     * @param finishMes
     * @return
     */
    @Override
    public List<ProductRepayMessageBean> getFinishList(ProductRepayMessageBean finishMes) {
        DMap dMap = productRepayDao.getFinishList(finishMes);
        List<ProductRepayMessageBean> listFinishMes = new ArrayList<ProductRepayMessageBean>();
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                ProductRepayMessageBean productRepayMessageBean = new ProductRepayMessageBean();
                productRepayMessageBean.setId(dMap.getValue("ID", i)); //主键
                productRepayMessageBean.setProductId(dMap.getValue("CODE", i)); //产品编号
                productRepayMessageBean.setProductTitle(dMap.getValue("TITLE", i)); //产品名称
                productRepayMessageBean.setRealName(dMap.getValue("REALNAME", i)); //还款人
                productRepayMessageBean.setLoanrate(dMap.getValue("LOANRATE", i)); //年利率
                productRepayMessageBean.setRepayperiods(dMap.getValue("REPAYPERIODS", i)); //已还期数
                productRepayMessageBean.setLoadfee(dMap.getValue("LOADFEE", i)); //借款金额
                listFinishMes.add(productRepayMessageBean);
            }
        }
        return listFinishMes;
    }

    /**
     * 获取已结清产品信息集合总数 by buyatao
     */
    @Override
    public int getFinishByPage(ProductRepayMessageBean finishMes) {
        DMap parm = productRepayDao.getFinishByPage(finishMes); //查询产品
        int totalCount = parm.getCount(); //广告数量
        return totalCount;
    }
}
