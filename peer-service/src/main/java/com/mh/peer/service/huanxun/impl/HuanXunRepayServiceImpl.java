package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunRepayDao;
import com.mh.peer.model.entity.HuanxunFreezeRepay;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunRepayMessageBean;
import com.mh.peer.service.huanxun.HuanXunRepayService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-8-7.
 * 还款部分
 */
@Service
public class HuanXunRepayServiceImpl implements HuanXunRepayService{

    @Autowired
    private HuanXunRepayDao huanXunRepayDao;

    /**
     * 获取会员信息
     * @param huanXunRepayMes
     * @return
     */
    @Override
    public List<HuanXunRepayMessageBean> queryMemberInfo(HuanXunRepayMessageBean huanXunRepayMes) {
        List<HuanXunRepayMessageBean> listHuanXunRepayMes = new ArrayList<HuanXunRepayMessageBean>();
        String repayer = ""; //还款id
        String balance = ""; //余额
        DMap dMap = huanXunRepayDao.queryMemberInfo(huanXunRepayMes);
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                HuanXunRepayMessageBean huanXunRepayMessageBean = new HuanXunRepayMessageBean();
                huanXunRepayMessageBean.setRepayId(dMap.getValue("ID", i)); //还款id
                repayer = dMap.getValue("APPLYMEMBER", i); //还款人id
                if (repayer != null && !repayer.equals("")){
                    huanXunRepayMessageBean.setRepayer(repayer);
                    balance = huanXunRepayDao.getMemberInfo(repayer).getValue("BALANCE",0); //余额(String型)
                    huanXunRepayMessageBean.setBalance(balance); //余额
                }
                listHuanXunRepayMes.add(huanXunRepayMessageBean);
            }
        }
        return listHuanXunRepayMes;
    }

    /**
     * 环迅还款金额冻结部分
     * @param huanxunFreezeRepay
     * @param memberMessage
     * @param memberInfo
     * @return
     */
    @Override
    public String repayhxResult(HuanxunFreezeRepay huanxunFreezeRepay, MemberMessage memberMessage, MemberInfo memberInfo, Map<String,Object> mapException) {
        String result = "0"; //执行结果(0-成功、1-失败)
        DaoResult daoResult = huanXunRepayDao.repayhxResult(huanxunFreezeRepay,memberMessage,memberInfo,mapException);
        if (daoResult.getCode() >= 0){
            result = "0";
        }else{
            result = "1";
        }
        return result;
    }


    /**
     * 根据还款id获取数量
     * @param repayId
     * @return
     */
    @Override
    public int queryFreezeRepayCount(String repayId) {
        DMap dMap = huanXunRepayDao.queryFreezeRepayCount(repayId);
        int count = dMap.getCount();
        return count;
    }
}
