package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunMotionSignDao;
import com.mh.peer.model.entity.HuanxunAutosign;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.service.huanxun.HuanXunMotionSignService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Cuibin on 2016/8/4.
 */
@Service
public class HuanXunMotionSignServiceImpl implements HuanXunMotionSignService {

    @Autowired
    private HuanXunMotionSignDao huanXunMotionSignDao;

    /**
     * 查询环迅注册记录
     *
     * @param memberId
     * @return
     */
    @Override
    public HuanXunRegistMessageBean queryHuanXunReg(String memberId) {

        DMap dMap = huanXunMotionSignDao.queryHuanXunReg(memberId);
        HuanXunRegistMessageBean temp = new HuanXunRegistMessageBean();
        if (dMap.getCount() > 0) {
            temp.setId(dMap.getValue("ID", 0));
            temp.setIpsAcctNo(dMap.getValue("IPSACCTNO", 0));
        }
        return temp;
    }

    /**
     * 保存自动签约返回信息
     */
    @Override
    public boolean saveMotionSignResult(HuanxunAutosign huanxunAutosign) {
        DaoResult daoResult = huanXunMotionSignDao.saveMotionSignResult(huanxunAutosign);
        if(daoResult.getCode() < 0){
            return false;
        }
        return true;
    }

    /**
     * 查询自动签约返回信息是否存在
     */
    @Override
    public boolean queryIsResult(String memberId,String signedType) {
        DMap dMap = huanXunMotionSignDao.queryIsResult(memberId,signedType);
        if(dMap.getCount() > 0){
            return false;
        }
        return true;
    }
}
