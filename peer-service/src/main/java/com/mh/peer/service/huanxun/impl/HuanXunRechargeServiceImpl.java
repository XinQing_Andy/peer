package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunRechargeDao;
import com.mh.peer.model.entity.HuanxunRecharge;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.service.huanxun.HuanXunRechargeService;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zhangerxin on 2016-7-24.
 */
@Service
public class HuanXunRechargeServiceImpl implements HuanXunRechargeService{

    @Autowired
    private HuanXunRechargeDao huanXunRechargeDao;

    /**
     * 环迅部分充值
     * @param huanxunRecharge 环迅充值结果
     * @param tradeDetail 交易记录
     * @param memberMessage 我的消息
     */
    @Override
    public void rechargehxResult(HuanxunRecharge huanxunRecharge, TradeDetail tradeDetail, MemberMessage memberMessage) {
        DaoResult daoResult = huanXunRechargeDao.rechargehxResult(huanxunRecharge,tradeDetail,memberMessage);
        System.out.println("执行结果======"+daoResult.getCode());
    }
}
