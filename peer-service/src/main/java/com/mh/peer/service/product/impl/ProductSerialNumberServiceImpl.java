package com.mh.peer.service.product.impl;

import com.mh.peer.dao.product.ProductSerialNumberDao;
import com.mh.peer.service.product.ProductSerialNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zhangerxin on 2016-7-31.
 */
@Service
public class ProductSerialNumberServiceImpl implements ProductSerialNumberService{

    @Autowired
    private ProductSerialNumberDao productSerialNumberDao;

    /**
     * 获取下一个项目流水号
     * @return
     */
    @Override
    public String getNextNumber() {
        String serialNumber = productSerialNumberDao.getNextNumber(); //下一个项目流水号
        return serialNumber;
    }
}
