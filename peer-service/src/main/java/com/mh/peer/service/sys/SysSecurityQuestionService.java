package com.mh.peer.service.sys;

import com.mh.peer.model.business.SysSecurityParameterBusinessBean;
import com.mh.peer.model.business.SysSecurityQuestionBusinessBean;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.mh.peer.model.message.SysSecurityParameterMessageBean;
import com.mh.peer.model.message.SysSecurityQuestionMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-1.
 */
public interface SysSecurityQuestionService {

    /**
     * 查询安全问题信息(根据分页查询)(zhangerxin)
     * @param sysSecurityQuestionMes
     * @return
     */
    List<SysSecurityQuestionMessageBean> querySecurityQuestionByPage(SysSecurityQuestionMessageBean sysSecurityQuestionMes);

    /**
     * 新增安全问题
     * @param sysSecurityQuestionMess
     * @return
     */
    SysSecurityQuestionBusinessBean addSysSecurityQuestion(SysSecurityQuestionMessageBean sysSecurityQuestionMess);

    /**
     * 修改安全问题
     * @param sysSecurityQuestionMess
     * @return
     */
    SysSecurityQuestionBusinessBean updateSysSecurityQuestion(SysSecurityQuestionMessageBean sysSecurityQuestionMess);

    /**
     * 安全问题数量
     * @param sysSecurityQuestionMess
     * @return
     */
    int getCountSecurityQuestion(SysSecurityQuestionMessageBean sysSecurityQuestionMess);

}
