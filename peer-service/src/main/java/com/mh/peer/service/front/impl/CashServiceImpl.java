package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.CashDao;
import com.mh.peer.dao.member.AllMemberDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.PostalRecord;
import com.mh.peer.model.entity.SerialnumberWh;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.*;
import com.mh.peer.service.front.CashService;
import com.mh.peer.util.PublicsTool;
import com.mh.peer.util.TradeHandle;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * Created by Cuibin on 2016/6/20.
 */
@Service
public class CashServiceImpl implements CashService {

    @Autowired
    private CashDao cashDao;
    @Autowired
    private AllMemberDao allMemberDao;

    /**
     * 按照id查询城市
     */
    @Override
    public SysCityMessagebean queryCityById(String id) {
        DMap d = cashDao.queryCityById(id);
        SysCityMessagebean sysCityMessagebean = new SysCityMessagebean();
        if (d.getCount() > 0) {
            sysCityMessagebean.setId(d.getValue("ID", 0));
            sysCityMessagebean.setProvinceid(d.getValue("PROVINCEID", 0));
            sysCityMessagebean.setCityname(d.getValue("CITYNAME", 0));
            sysCityMessagebean.setAreacode(d.getValue("AREACODE", 0));
        }
        return sysCityMessagebean;
    }

    /**
     * 按照id查询省
     */
    @Override
    public SysProvinceMessageBean queryProvinceById(String id) {
        DMap d = cashDao.queryProvinceById(id);
        SysProvinceMessageBean sysCityMessagebean = new SysProvinceMessageBean();
        if (d.getCount() > 0) {
            sysCityMessagebean.setId(d.getValue("ID", 0));
            sysCityMessagebean.setProvinceName(d.getValue("PROVINCE_NAME", 0));
            sysCityMessagebean.setSort(d.getValue("SORT", 0));
        }
        return sysCityMessagebean;
    }

    /**
     * 提现
     */
    @Override
    public boolean userCash(PostalRecordMessageBean postalRecordMessageBean, TradeDetailMessageBean tradeDetailMessageBean) {
        String jyTime = StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss"); // 交易时间
        String memberid = postalRecordMessageBean.getMemberid();//会员id
        String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
        /*************** 提现表 ***************/
        PostalRecord postalRecord = new PostalRecord();
        postalRecord.setId(uuid);//主键
        postalRecord.setMerbillno(postalRecordMessageBean.getMerbillno());//商户订单号
        postalRecord.setSprice(Double.parseDouble(postalRecordMessageBean.getSprice()));//实际提现金额
        postalRecord.setProcedurefee(Double.parseDouble(postalRecordMessageBean.getProcedurefee()));//手续费
        postalRecord.setFlag(postalRecordMessageBean.getFlag()); //状态
        postalRecord.setMemberid(memberid);//会员id
        postalRecord.setBankid(postalRecordMessageBean.getBankid());//提现银行
        postalRecord.setSource(postalRecordMessageBean.getSource());//来源(0-电脑端、1-手机端)
        postalRecord.setTime(jyTime);//提现时间
        postalRecord.setCreateuser(memberid);//创建人
        postalRecord.setCreatetime(jyTime);//创建时间
        /*************** 提现表 ***************/

        /*************** 流水表 ***************/
        TradeHandle tradeHandle = new TradeHandle();//流水号工具类
        int serialNumber = tradeHandle.getSerialNumber(); //流水号
        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setId(UUID.randomUUID().toString().replaceAll("\\-", "")); //主键
        tradeDetail.setGlid(uuid);//关联id
        tradeDetail.setSerialnum(serialNumber);//流水号
        tradeDetail.setType("1");//交易类型(0-充值、1-提现)
        tradeDetail.setSprice(Double.parseDouble(postalRecordMessageBean.getSprice()));//实际充值金额 环迅
        tradeDetail.setProcedurefee(Double.parseDouble(tradeDetailMessageBean.getProcedurefee()));//手续费
        tradeDetail.setFlag(postalRecordMessageBean.getFlag());//充值状态(0-充值成功、1-充值失败) 环迅
        tradeDetail.setMemberid(memberid);//会员id
        tradeDetail.setSource(postalRecordMessageBean.getSource());//来源(0-电脑端、1-手机端)
        tradeDetail.setTime(jyTime);//交易时间
        tradeDetail.setBz("环迅提现");//备注
        tradeDetail.setCreateuser(memberid);//会员id
        tradeDetail.setCreatetime(jyTime);//创建时间
        /*************** 流水表 ***************/

        /*************** 流水号维护表 ***************/
        SerialnumberWh serialnumberWh = new SerialnumberWh();
        serialnumberWh.setNumber(serialNumber);
        serialnumberWh.setId("6d82cfe3347511e69b6d00163e0063ab");
        /*************** 流水号维护表 ***************/

        /*************** 会员表 ***************/
        DMap dMap = allMemberDao.queryOnlyMember(memberid);
        double newBalance = 0;
        if (dMap.getCount() > 0) {
            String balance = dMap.getValue("BALANCE", 0);
            newBalance = Double.parseDouble(balance) - Double.parseDouble(postalRecordMessageBean.getUserSprice());
        }

        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(memberid);
        memberInfo.setBalance(newBalance);

        tradeDetail.setBalance(newBalance); //本次余额
        /*************** 会员表 ***************/

        return cashDao.userCash(postalRecord, tradeDetail, serialnumberWh, memberInfo);
    }


    /**
     * 查询提现记录
     * @param huanXunPostalMes
     * @return
     */
    @Override
    public List<HuanXunPostalMessageBean> queryPostalRecordByPage(HuanXunPostalMessageBean huanXunPostalMes) {
        DMap dMap = cashDao.queryPostalRecordByPage(huanXunPostalMes);
        List<HuanXunPostalMessageBean> listHuanXunPostalMes = new ArrayList<HuanXunPostalMessageBean>();
        String merFee = ""; //平台服务费(String型)
        Double merFeeDou = 0.0d; //平台服务费(Double型)
        String ipsFee = ""; //IPS服务费(String型)
        Double ipsFeeDou = 0.0d; //IPS服务费(Double型)
        String ipsTrdAmt = ""; //IPS提现金额(String型)
        Double ipsTrdAmtDou = 0.0; //IPS提现金额(Double型)
        Double sumFee = 0.0d; //总提现金额
        Double endFee = 0.0d; //最后到账金额
        String ipsDoTime = ""; //申请提现时间

        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                HuanXunPostalMessageBean huanXunPostalMessageBean = new HuanXunPostalMessageBean();
                huanXunPostalMessageBean.setId(dMap.getValue("ID",i)); //主键

                merFee = dMap.getValue("MERFEE",i); //平台服务费
                if (merFee != null && !merFee.equals("")){
                    merFeeDou = TypeTool.getDouble(merFee);
                }
                huanXunPostalMessageBean.setMerfee(merFee); //平台服务费

                ipsFee = dMap.getValue("IPSFEE",i); //IPS手续费
                if (ipsFee != null && !ipsFee.equals("")){
                    ipsFeeDou = TypeTool.getDouble(ipsFee);
                }
                huanXunPostalMessageBean.setIpsfee(ipsFee);

                ipsTrdAmt = dMap.getValue("IPSTRDAMT",i); //IPS提现金额(String型)
                if (ipsTrdAmt != null && !ipsTrdAmt.equals("")){
                    ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //IPS提现金额(Double型)
                }

                sumFee = ipsTrdAmtDou + merFeeDou; //总提现金额(Double型)
                sumFee = PublicsTool.round(sumFee,2);
                huanXunPostalMessageBean.setSumFee(TypeTool.getString(sumFee)); //总提现金额(String型)

                endFee = ipsTrdAmtDou - 2; //最后到账金额
                endFee = PublicsTool.round(endFee,2);
                huanXunPostalMessageBean.setEndFee(TypeTool.getString(endFee)); //最后到账金额

                ipsDoTime = dMap.getValue("IPSDOTIME",i); //申请提现时间
                if (ipsDoTime != null && !ipsDoTime.equals("")){
                    ipsDoTime = ipsDoTime.substring(0,10);
                }
                huanXunPostalMessageBean.setIpsdotime(ipsDoTime);
                listHuanXunPostalMes.add(huanXunPostalMessageBean);

                huanXunPostalMessageBean.setResultmsg(dMap.getValue("RESULTMSG",i)); //处理信息
            }
        }

        return listHuanXunPostalMes;
    }


    /**
     * 提现记录数量
     * @param huanXunPostalMes
     * @return
     */
    @Override
    public int queryPostalRecordByPageCount(HuanXunPostalMessageBean huanXunPostalMes) {
        DMap dMap = cashDao.queryPostalRecordByPageCount(huanXunPostalMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 获取可提现金额
     * @param memberId
     * @return
     */
    @Override
    public String getPostaledPrice(String memberId) {
        DMap dMap = cashDao.getPostaledPrice(memberId);
        String postaledPrice = dMap.getValue("POSTALEDPRICE",0); //可提现金额
        return postaledPrice;
    }

    /**
     * 查询提现金额 到账金额 手续费用
     */
    @Override
    public String[] querySum(String memberId) {
        DMap dMap = cashDao.querySum(memberId);
        String[] arr = new String[3];
        if (dMap.getCount() > 0) {
            arr[0] = dMap.getValue("SPRICESUM", 0);
            arr[1] = dMap.getValue("PROCEDUREFEESUM", 0);
            arr[2] = dMap.getValue("POSTALPRICESUM", 0);
        }

        return arr;
    }
}
