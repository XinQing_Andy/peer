package com.mh.peer.service.member;

import com.mh.peer.model.message.*;

import java.util.List;

/**
 * Created by cuibin on 2016/4/20.
 */
public interface AllMemberService {

    /**
     * 根据分页查询会员(By Zhangerxin)
     * @param memberInfoMesn
     * @return
     */
    public List<MemberInfoMessageBean> queryMemberBysome(MemberInfoMessageBean memberInfoMesn);

    /**
     * 获取会员数量
     */
    public int queryCountMember();

    /**
     * 查询所有会员
     */
    public List<MemberInfoMessageBean> queryAllMember(MemberInfoMessageBean memberInfoMessageBean);

    /**
     *查询所有会员 数量
     */
    public int queryAllMemberCount(MemberInfoMessageBean memberInfoMessageBean);

    /**
     * 查询一个 会员
     */
    public MemberInfoMessageBean queryOnlyMember(MemberInfoMessageBean memberInfoMessageBean);

    /**
     * 根据主键获取会员信息
     * @param id
     * @return
     */
    public MemberInfoMessageBean queryMemberInfoById(String id);

    /**
     * 锁定 会员
     */
    public MemberInfoMessageBean lockMember(MemberInfoMessageBean memberInfoMessageBean);
    /**
     * 更新密码
     */
    public MemberInfoMessageBean updateMemberPassWord(MemberInfoMessageBean memberInfoMessageBean);

    /**
     * 更新信息
     */
    MemberInfoMessageBean updateMemberInfo(MemberInfoMessageBean memberInfoMessageBean);

    /**
     * 查询所有理财集合
     */
    public List<FinancingMessageBean> queryAllManageMoney(FinancingMessageBean financingMes);

    /**
     * 查询所有理财数量
     * @param financingMes
     * @return
     */
    public int queryAllManageMoneyCount(FinancingMessageBean financingMes);

    /**
     * 后台首页查询新增会员 昨天
     */
    public String[] queryYesterdayReg();

    /**
     * 后台首页查询新增理财会员 过去7天
     */
    public String[] queryLastWeekReg();

    /**
     * 后台首页查询新增注册会员 过去30天
     */
    public String[] queryLastMonthReg();

    /**
     * 求自定义天数之前 到今天的日期 by cuibin
     */
    public String[] queryDate(int day);

    /**
     * 求最近30天日期
     */
    public String[] queryDateMonth();

    /**
     * 获取我的账户信息
     * @return
     */
    public MyAccountMessageBean getMemberInfo(MyAccountMessageBean myAccountMes);

    /**
     * 获取会员详细信息(App端)
     * @param memberId
     * @return
     */
    public AppMemberInfoMessageBean getAppMemberInfo(String memberId);

    /**
     * 资产统计部分(App端)
     * @param memberId
     * @return
     */
    public AppMyAccountMessageBean getPriceTj(String memberId);

    /**
     * 获取投资实力榜(App端)
     * @param pageIndex
     * @param pageSize
     * @param orderColumn
     * @param orderType
     * @return
     */
    public List<AppMemberInvestMessageBean> getMemberByInvest(String pageIndex,String pageSize,String orderColumn,String orderType);

    /**
     * 获取个人信息资料
     * @param memberId
     * @return
     */
    public AppPersonInfoMessageBean getAppPersonInfo(String memberId);

    /**
     * 更新余额
     */
    public boolean updateBalance(MemberInfoMessageBean memberInfoMes);

    /**
     * 查询一个 会员余额
     */
    public String queryOnlyMemberBalance(String memberId);
}
