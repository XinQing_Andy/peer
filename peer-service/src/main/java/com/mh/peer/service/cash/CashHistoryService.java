package com.mh.peer.service.cash;

import com.mh.peer.model.message.HuanXunPostalMessageBean;

import java.util.List;

/**
 * Created by Cuibin on 2016/7/28.
 */
public interface CashHistoryService {
    /**
     * 查询提现统计
     */
    public List<HuanXunPostalMessageBean> queryCashHistory(HuanXunPostalMessageBean huanXunPostalMessageBean);

    /**
     * 查询提现统计 总数
     */
    public String queryCashHistoryCount(HuanXunPostalMessageBean huanXunPostalMessageBean);
}
