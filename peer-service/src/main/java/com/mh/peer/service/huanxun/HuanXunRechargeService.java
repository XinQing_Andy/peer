package com.mh.peer.service.huanxun;

import com.mh.peer.dao.huanxun.HuanXunRechargeDao;
import com.mh.peer.model.entity.HuanxunRecharge;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;

/**
 * Created by zhangerxin on 2016-7-24.
 * 充值
 */
public interface HuanXunRechargeService {

    /**
     * 环迅部分充值记录
     * @param huanxunRecharge 环迅充值结果
     * @param tradeDetail 交易记录
     * @param memberMessage 我的消息
     */
    public void rechargehxResult(HuanxunRecharge huanxunRecharge,TradeDetail tradeDetail,MemberMessage memberMessage);
}
