package com.mh.peer.service.fundSetting.impl;

import com.mh.peer.dao.fundSetting.FundSettingDao;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.*;
import com.mh.peer.service.fundSetting.FundSettingService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cuibin on 2016/4/12.
 */
@Service
public class FundSettingServiceImpl implements FundSettingService{
    @Autowired
    private FundSettingDao fundSettingDao;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 添加大字典
     */
    @Override
    public SysDictionaryMessageBean addDictionary(SysDictionaryMessageBean sysDictionaryMessageBean) {
        SysDictionary sysD = new SysDictionary();
        sysD.setGroupType(sysDictionaryMessageBean.getGroupType());
        sysD.setMemberId(sysDictionaryMessageBean.getMemberId());
        sysD.setMemberName(sysDictionaryMessageBean.getMemberName());
        sysD.setActiveFlg(sysDictionaryMessageBean.getActiveFlg());
        sysD.setRemark(sysDictionaryMessageBean.getRemark());
        DaoResult daoResult = fundSettingDao.addDictionary(sysD);
        if(daoResult.getCode() > 0){
            sysDictionaryMessageBean.setResult("success");
        }else{
            sysDictionaryMessageBean.setResult("error");
            sysDictionaryMessageBean.setInfo("添加失败");
        }
        return sysDictionaryMessageBean;
    }

    /**
     * 添加 按会员等级费用维护
     */
    @Override
    public MemberFeeLevelMessageBean addMemberFeeLevel(MemberFeeLevelMessageBean memberFeeLevelMessageBean) {
        MemberFeeLevel memberFeeLevel = new MemberFeeLevel();
        memberFeeLevel.setFeeName(memberFeeLevelMessageBean.getFeeName());
        memberFeeLevel.setActiveFlg(memberFeeLevelMessageBean.getActiveFlg());
        DaoResult daoResult = fundSettingDao.addMemberFeeLevel(memberFeeLevel);
        if(daoResult.getCode() > 0){
            memberFeeLevelMessageBean.setResult("success");
        }else{
            memberFeeLevelMessageBean.setResult("error");
            memberFeeLevelMessageBean.setInfo("添加失败");
        }
        return memberFeeLevelMessageBean;
    }

    /**
     *添加 会员费用关联表
     */
    @Override
    public MemberFeeRelationMessageBean addMemberFeeRelation(MemberFeeRelationMessageBean memberFeeRelationMessageBean) {
        MemberFeeRelation memberFeeRelation = new MemberFeeRelation();
        memberFeeRelation.setFeeId(memberFeeRelationMessageBean.getFeeId());
        memberFeeRelation.setLevelId(memberFeeRelationMessageBean.getLevelId());
        memberFeeRelation.setLevelProportion(memberFeeRelationMessageBean.getLevelProportion());
        DaoResult daoResult = fundSettingDao.addMemberFeeRelation(memberFeeRelation);
        if(daoResult.getCode() > 0){
            memberFeeRelationMessageBean.setResult("success");
        }else{
            memberFeeRelationMessageBean.setResult("error");
            memberFeeRelationMessageBean.setInfo("添加失败");
        }
        return memberFeeRelationMessageBean;
    }
    /**
     * 添加固定费用过账
     */
    @Override
    public SysManageAmtMessageBean addSysManageAmt(SysManageAmtMessageBean sysManageAmtMessageBeans) {
        SysManageAmt sysManageAmt = new SysManageAmt();
        sysManageAmt.setFeeAmt(sysManageAmtMessageBeans.getFeeAmt());
        sysManageAmt.setFeeName(sysManageAmtMessageBeans.getFeeName());
        sysManageAmt.setActiveFlg(sysManageAmtMessageBeans.getActiveFlg());
        DaoResult daoResult = fundSettingDao.addSysManageAmt(sysManageAmt);
        if(daoResult.getCode() > 0){
            sysManageAmtMessageBeans.setResult("success");
        }else{
            sysManageAmtMessageBeans.setResult("error");
            sysManageAmtMessageBeans.setInfo("添加失败");
        }
        return sysManageAmtMessageBeans;
    }

    @Override
    public SysTakeNoMessageBean addTakeNo(SysTakeNoMessageBean sysTakeNoMessageBean) {
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean loginBusinessBean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        int userId = loginBusinessBean.getSysUser().getId();
        SysTakeNo stn = new SysTakeNo();
        stn.setCodeType(sysTakeNoMessageBean.getCodeType());
        stn.setStartNo(sysTakeNoMessageBean.getStartNo());
        stn.setCurrNo(sysTakeNoMessageBean.getCurrNo());
        stn.setNoLength(sysTakeNoMessageBean.getNoLength());
        stn.setStartNo(sysTakeNoMessageBean.getStartNo());
        stn.setMaxNo(sysTakeNoMessageBean.getMaxNo());
        stn.setNoDesc(sysTakeNoMessageBean.getNoDesc());
        stn.setNoFormat(sysTakeNoMessageBean.getNoFormat());
        stn.setCreateDate(StringTool.getString(DaoTool.getTime(), "yyyyMMdd"));
        stn.setCreateUser(userId);
        stn.setStatusMethod(sysTakeNoMessageBean.getStatusMethod());
        DaoResult daoResult = fundSettingDao.addTakeNo(stn);
        if(daoResult.getCode() > 0){
            sysTakeNoMessageBean.setResult("success");
        }else{
            sysTakeNoMessageBean.setResult("error");
            sysTakeNoMessageBean.setInfo("添加失败");
        }
        return sysTakeNoMessageBean;
    }
    /**
     * 添加vip等级维护
     */
    @Override
    public MemberLevelMessageBean addVipMaintain(MemberLevelMessageBean memberLevelMessageBean) {
        MemberLevel ml = new MemberLevel();
        ml.setLevelRequest(memberLevelMessageBean.getLevelRequest());
        ml.setLevelName(memberLevelMessageBean.getLevelName());
        DaoResult daoResult = fundSettingDao.addVipMaintain(ml);
        if(daoResult.getCode() > 0){
            memberLevelMessageBean.setResult("success");
        }else{
            memberLevelMessageBean.setResult("error");
            memberLevelMessageBean.setInfo("添加失败");
        }
        return memberLevelMessageBean;
    }

    /**
     * 删除大字典
     */
    @Override
    public SysDictionaryMessageBean deleteDictionary(SysDictionaryMessageBean sysDictionaryMessageBean) {
        SysDictionary sysD = new SysDictionary();
        sysD.setId(sysDictionaryMessageBean.getId());
        DaoResult result = fundSettingDao.deleteDictionary(sysD);
        if(result.getCode() < 0){
            sysDictionaryMessageBean.setResult("error");
            sysDictionaryMessageBean.setInfo(result.getErrText());
        }else{
            sysDictionaryMessageBean.setResult("success");
            sysDictionaryMessageBean.setInfo("删除成功");
        }
        return sysDictionaryMessageBean;
    }

    @Override
    public MemberFeeLevelMessageBean deleteMemberFeeLevel(MemberFeeLevelMessageBean memberFeeLevelMessageBean) {
        MemberFeeLevel mfl = new MemberFeeLevel();
        mfl.setId(memberFeeLevelMessageBean.getId());
        DaoResult result = fundSettingDao.deleteMemberFeeLevel(mfl);
        if(result.getCode() < 0){
            memberFeeLevelMessageBean.setResult("error");
            memberFeeLevelMessageBean.setInfo(result.getErrText());
        }else{
            memberFeeLevelMessageBean.setResult("success");
            memberFeeLevelMessageBean.setInfo("删除成功");
        }
        return memberFeeLevelMessageBean;
    }

    @Override
    public MemberFeeRelationMessageBean deleteMemberFeeRelation(MemberFeeRelationMessageBean memberFeeRelationMessageBean) {
        MemberFeeRelation mbfr = new MemberFeeRelation();
        mbfr.setId(memberFeeRelationMessageBean.getId());
        DaoResult result = fundSettingDao.deleteMemberFeeRelation(mbfr);
        if(result.getCode() < 0){
            memberFeeRelationMessageBean.setResult("error");
            memberFeeRelationMessageBean.setInfo(result.getErrText());
        }else{
            memberFeeRelationMessageBean.setResult("success");
            memberFeeRelationMessageBean.setInfo("删除成功");
        }
        return memberFeeRelationMessageBean;
    }

    @Override
    public SysManageAmtMessageBean deleteSysManageAmt(SysManageAmtMessageBean sysManageAmtMessageBean) {
        SysManageAmt sma = new SysManageAmt();
        sma.setId(sysManageAmtMessageBean.getId());
        DaoResult result = fundSettingDao.deleteSysManageAmt(sma);
        if(result.getCode() < 0){
            sysManageAmtMessageBean.setResult("error");
            sysManageAmtMessageBean.setInfo(result.getErrText());
        }else{
            sysManageAmtMessageBean.setResult("success");
            sysManageAmtMessageBean.setInfo("删除成功");
        }
        return sysManageAmtMessageBean;
    }

    @Override
    public SysTakeNoMessageBean deleteTakeNo(SysTakeNoMessageBean sysTakeNoMessageBean) {
        SysTakeNo stn = new SysTakeNo();
        stn.setId(sysTakeNoMessageBean.getId());
        DaoResult result = fundSettingDao.deleteTakeNo(stn);
        if(result.getCode() < 0){
            sysTakeNoMessageBean.setResult("error");
            sysTakeNoMessageBean.setInfo(result.getErrText());
        }else{
            sysTakeNoMessageBean.setResult("success");
            sysTakeNoMessageBean.setInfo("删除成功");
        }
        return sysTakeNoMessageBean;
    }

    @Override
    public MemberLevelMessageBean deleteVipMaintain(MemberLevelMessageBean memberLevelMessageBean) {
        MemberLevel ml = new MemberLevel();
        ml.setId(memberLevelMessageBean.getId());
        DaoResult result = fundSettingDao.deleteVipMaintain(ml);
        if(result.getCode() < 0){
            memberLevelMessageBean.setResult("error");
            memberLevelMessageBean.setInfo(result.getErrText());
        }else{
            memberLevelMessageBean.setResult("success");
            memberLevelMessageBean.setInfo("删除成功");
        }
        return memberLevelMessageBean;
    }

    /**
     * 查询所有大字典
     * @return
     */
    @Override
    public Paging queryAllDictionary(Paging paging) {
        DMap dmap = fundSettingDao.queryAllDictionary(paging);
        int count = fundSettingDao.queryAllDictionaryCount(paging).getCount();
        List<SysDictionaryMessageBean> list = new ArrayList<SysDictionaryMessageBean>();
        SysDictionaryMessageBean sysDM = null;
        if(dmap.getCount() > 0){
            for (int i = 0; i < dmap.getCount(); i++) {
                sysDM = new SysDictionaryMessageBean();
                sysDM.setId(TypeTool.getInt(dmap.getValue("ID", i)));
                sysDM.setGroupType(TypeTool.getString(dmap.getValue("GROUP_TYPE", i)));
                sysDM.setMemberName(TypeTool.getString(dmap.getValue("MEMBER_NAME", i)));
                sysDM.setMemberId(TypeTool.getInt(dmap.getValue("MEMBER_ID", i)));
                sysDM.setActiveFlg(TypeTool.getString(dmap.getValue("ACTIVE_FLG", i)));
                sysDM.setRemark(TypeTool.getString(dmap.getValue("REMARK", i)));
                list.add(sysDM);
            }
        }
        Paging newPag = new Paging();
        newPag.setList(list);
        newPag.setCount(count);
        return newPag;
    }

    /**
     *查询所有 按会员等级费用维护
     */
    @Override
    public Paging queryAllMemberFeeLevel(Paging paging) {
        DMap dmap = fundSettingDao.queryAllMemberFeeLevel(paging);
        int count = fundSettingDao.queryMemberFeeLevelCount(paging).getCount();
        List<MemberFeeLevelMessageBean> list = new ArrayList<MemberFeeLevelMessageBean>();
        MemberFeeLevelMessageBean m = null;
        if(dmap.getCount() > 0){
            for (int i = 0; i < dmap.getCount(); i++) {
                m = new MemberFeeLevelMessageBean();
                m.setId(TypeTool.getInt(dmap.getValue("ID",i)));
                m.setActiveFlg(TypeTool.getString(dmap.getValue("ACTIVE_FLG",i)));
                m.setFeeName(TypeTool.getString(dmap.getValue("FEE_NAME", i)));
                list.add(m);
            }
        }
        Paging newPag = new Paging();
        newPag.setList(list);
        newPag.setCount(count);
        return newPag;
    }
    /**
     *查询所有 按会员等级费用维护
     */
    @Override
    public List<MemberFeeLevelMessageBean> queryAllMemberFeeLevel2() {
        DMap dmap = fundSettingDao.queryAllMemberFeeLevel2();
        List<MemberFeeLevelMessageBean> list = new ArrayList<MemberFeeLevelMessageBean>();
        MemberFeeLevelMessageBean m = null;
        if(dmap.getCount() > 0) {
            for (int i = 0; i < dmap.getCount(); i++) {
                m = new MemberFeeLevelMessageBean();
                m.setId(TypeTool.getInt(dmap.getValue("ID", i)));
                m.setActiveFlg(TypeTool.getString(dmap.getValue("ACTIVE_FLG", i)));
                m.setFeeName(TypeTool.getString(dmap.getValue("FEE_NAME", i)));
                list.add(m);
            }
        }
        return list;
    }
    /**
     * 查询所有会员费用关联
     */
    @Override
    public Paging queryAllMemberFeeRelation(Paging paging) {
        DMap dmap = fundSettingDao.queryAllMemberFeeRelation(paging);
        int count = fundSettingDao.queryAllMemberFeeRelation(paging).getCount();
        List<MemberFeeRelationMessageBean> list = new ArrayList<MemberFeeRelationMessageBean>();
        MemberFeeRelationMessageBean mfrm = null;
        if(dmap.getCount() > 0){
            for (int i = 0; i < dmap.getCount(); i++) {
                mfrm = new MemberFeeRelationMessageBean();
                mfrm.setId(TypeTool.getInt(dmap.getValue("ID", i)));
                mfrm.setLevelId(TypeTool.getInt(dmap.getValue("LEVEL_ID", i)));
                mfrm.setFeeId(TypeTool.getInt(dmap.getValue("FEE_ID", i)));
                mfrm.setLevelProportion(TypeTool.getDouble(dmap.getValue("LEVEL_PROPORTION", i)));
                list.add(mfrm);
            }
        }
        Paging newPag = new Paging();
        newPag.setList(list);
        newPag.setCount(count);
        return newPag;
    }
    @Override
    public List<MemberFeeRelationMessageBean> queryAllMemberFeeRelation2() {
        DMap dmap = fundSettingDao.queryAllMemberFeeRelation2();
        List<MemberFeeRelationMessageBean> list = new ArrayList<MemberFeeRelationMessageBean>();
        MemberFeeRelationMessageBean mfrm = null;
        if(dmap.getCount() > 0){
            for (int i = 0; i < dmap.getCount(); i++) {
                mfrm = new MemberFeeRelationMessageBean();
                mfrm.setId(TypeTool.getInt(dmap.getValue("ID", i)));
                mfrm.setLevelId(TypeTool.getInt(dmap.getValue("LEVEL_ID", i)));
                mfrm.setFeeId(TypeTool.getInt(dmap.getValue("FEE_ID", i)));
                mfrm.setLevelProportion(TypeTool.getDouble(dmap.getValue("LEVEL_PROPORTION", i)));
                list.add(mfrm);
            }
        }
        return list;
    }
    /**
     * 查询所有固定费用过账
     */
    @Override
    public Paging queryAllSysManageAmt(Paging paging) {
        DMap dmap = fundSettingDao.queryAllSysManageAmt(paging);
        int count = fundSettingDao.queryAllSysManageAmtCount(paging).getCount();
        List<SysManageAmtMessageBean> list = new ArrayList<SysManageAmtMessageBean>();
        SysManageAmtMessageBean smam = null;
        if(dmap.getCount() > 0){
            for (int i = 0; i < dmap.getCount(); i++) {
                smam = new SysManageAmtMessageBean();
                smam.setId(TypeTool.getInt(dmap.getValue("ID", i)));
                smam.setFeeName(TypeTool.getString(dmap.getValue("FEE_NAME", i)));
                smam.setFeeAmt(TypeTool.getDouble(dmap.getValue("FEE_AMT", i)));
                smam.setActiveFlg(TypeTool.getString(dmap.getValue("ACTIVE_FLG", i)));
                list.add(smam);
            }
        }
        Paging newPag = new Paging();
        newPag.setList(list);
        newPag.setCount(count);
        return newPag;
    }
    /**
     * 查询所有取号原则
     */
    @Override
    public Paging queryAllTakeNo(Paging paging) {
        DMap dmap = fundSettingDao.queryAllTakeNo(paging);
        int count = fundSettingDao.queryAllTakeNoCount(paging).getCount();
        List<SysTakeNoMessageBean> list = new ArrayList<SysTakeNoMessageBean>();
        SysTakeNoMessageBean stm = null;
        if(dmap.getCount() > 0){
            for (int i = 0; i < dmap.getCount(); i++) {
                stm = new SysTakeNoMessageBean();
                stm.setId(TypeTool.getInt(dmap.getValue("ID", i)));
                stm.setCodeType(TypeTool.getString(dmap.getValue("CODE_TYPE", i)));
                stm.setStartNo(TypeTool.getString(dmap.getValue("START_NO", i)));
                stm.setCurrNo(TypeTool.getString(dmap.getValue("CURR_NO", i)));
                stm.setNoLength(TypeTool.getString(dmap.getValue("NO_LENGTH", i)));
                stm.setStartNo(TypeTool.getString(dmap.getValue("START_NO", i)));
                stm.setMaxNo(TypeTool.getString(dmap.getValue("MAX_NO", i)));
                stm.setNoDesc(TypeTool.getString(dmap.getValue("NO_DESC", i)));
                stm.setNoFormat(TypeTool.getString(dmap.getValue("NO_FORMAT", i)));
                stm.setCreateDate(TypeTool.getString(dmap.getValue("CREATE_DATE", i)));
                stm.setCreateUser(TypeTool.getInt(dmap.getValue("CREATE_USER", i)));
                stm.setStatusMethod(TypeTool.getString(dmap.getValue("STATUS_METHOD", i)));
                list.add(stm);
            }
        }
        Paging newPag = new Paging();
        newPag.setList(list);
        newPag.setCount(count);
        return newPag;
    }
    /**
     *查询所有vip等级维护
     */
    @Override
    public Paging queryAllVipMaintain(Paging paging) {
        DMap dmap = fundSettingDao.queryAllVipMaintain(paging);
        int count = fundSettingDao.queryAllVipMaintainCount(paging).getCount();
        List<MemberLevelMessageBean> list = new ArrayList<MemberLevelMessageBean>();
        MemberLevelMessageBean mlM = null;
        if(dmap.getCount() > 0){
            for (int i = 0; i < dmap.getCount(); i++) {
                mlM = new MemberLevelMessageBean();
                mlM.setId(TypeTool.getInt(dmap.getValue("ID", i)));
                mlM.setLevelName(TypeTool.getString(dmap.getValue("LEVEL_NAME", i)));
                mlM.setLevelRequest(TypeTool.getString(dmap.getValue("LEVEL_REQUEST", i)));
                list.add(mlM);
            }
        }
        Paging newPag = new Paging();
        newPag.setList(list);
        newPag.setCount(count);
        return newPag;
    }
    /**
     *查询所有vip等级维护 2
     */
    @Override
    public List<MemberLevelMessageBean> queryAllVipMaintain2() {
        DMap dmap = fundSettingDao.queryAllVipMaintain2();
        List<MemberLevelMessageBean> list = new ArrayList<MemberLevelMessageBean>();
        MemberLevelMessageBean mlM = null;
        if(dmap.getCount() > 0){
            for (int i = 0; i < dmap.getCount(); i++) {
                mlM = new MemberLevelMessageBean();
                mlM.setId(TypeTool.getInt(dmap.getValue("ID", i)));
                mlM.setLevelName(TypeTool.getString(dmap.getValue("LEVEL_NAME", i)));
                mlM.setLevelRequest(TypeTool.getString(dmap.getValue("LEVEL_REQUEST", i)));
                list.add(mlM);
            }
        }
        return list;
    }


    /**
     * 查询一个大字典
     */
    @Override
    public SysDictionaryMessageBean queryOnlyDictionary(SysDictionaryMessageBean sysDictionaryMessageBean) {
        SysDictionary sysD = new SysDictionary();
        sysD.setId(sysDictionaryMessageBean.getId());
        DMap dmap = fundSettingDao.queryOnlyDictionary(sysD);
        SysDictionaryMessageBean sysDM = new SysDictionaryMessageBean();
        if(dmap.getCount() > 0){
            sysDM.setId(TypeTool.getInt(dmap.getValue("ID", 0)));
            sysDM.setGroupType(TypeTool.getString(dmap.getValue("GROUP_TYPE", 0)));
            sysDM.setMemberName(TypeTool.getString(dmap.getValue("MEMBER_NAME", 0)));
            sysDM.setMemberId(TypeTool.getInt(dmap.getValue("MEMBER_ID", 0)));
            sysDM.setActiveFlg(TypeTool.getString(dmap.getValue("ACTIVE_FLG", 0)));
            sysDM.setRemark(TypeTool.getString(dmap.getValue("REMARK", 0)));
        }
        return sysDM;
    }

    @Override
    public MemberFeeLevelMessageBean queryOnlyMemberFeeLevel(MemberFeeLevelMessageBean memberFeeLevelMessageBean) {
        MemberFeeLevel mfl = new MemberFeeLevel();
        mfl.setId(memberFeeLevelMessageBean.getId());
        DMap dmap = fundSettingDao.queryOnlyMemberFeeLevel(mfl);
        MemberFeeLevelMessageBean mflm = new MemberFeeLevelMessageBean();
        if(dmap.getCount() > 0){
            mflm = new MemberFeeLevelMessageBean();
            mflm.setId(TypeTool.getInt(dmap.getValue("ID", 0)));
            mflm.setActiveFlg(TypeTool.getString(dmap.getValue("ACTIVE_FLG", 0)));
            mflm.setFeeName(TypeTool.getString(dmap.getValue("FEE_NAME", 0)));
        }
        return mflm;
    }

    @Override
    public MemberFeeRelationMessageBean queryOnlyMemberFeeRelation(MemberFeeRelationMessageBean memberFeeRelationMessageBean) {
        MemberFeeRelation mfrt = new MemberFeeRelation();
        mfrt.setId(memberFeeRelationMessageBean.getId());
        DMap dmap = fundSettingDao.queryOnlyMemberFeeRelation(mfrt);
        MemberFeeRelationMessageBean mfrm = new MemberFeeRelationMessageBean();
        if(dmap.getCount() > 0){
            mfrm.setId(TypeTool.getInt(dmap.getValue("ID", 0)));
            mfrm.setFeeId(TypeTool.getInt(dmap.getValue("FEE_ID", 0)));
            mfrm.setLevelProportion(TypeTool.getDouble(dmap.getValue("LEVEL_PROPORTION", 0)));
            mfrm.setLevelId(TypeTool.getInt(dmap.getValue("LEVEL_ID", 0)));
        }
        return mfrm;
    }

    @Override
    public SysManageAmtMessageBean queryOnlySysManageAmt(SysManageAmtMessageBean sysManageAmtMessageBean) {
        SysManageAmt sysma = new SysManageAmt();
        sysma.setId(sysManageAmtMessageBean.getId());
        DMap dmap = fundSettingDao.queryOnlySysManageAmt(sysma);
        SysManageAmtMessageBean smam = new SysManageAmtMessageBean();
        if(dmap.getCount() > 0){
            smam.setId(TypeTool.getInt(dmap.getValue("ID", 0)));
            smam.setFeeName(TypeTool.getString(dmap.getValue("FEE_NAME", 0)));
            smam.setFeeAmt(TypeTool.getDouble(dmap.getValue("FEE_AMT", 0)));
            smam.setActiveFlg(TypeTool.getString(dmap.getValue("ACTIVE_FLG", 0)));
        }
        return smam;
    }
    /**
     *查询一个取号
     */
    @Override
    public SysTakeNoMessageBean queryOnlyTakeNo(SysTakeNoMessageBean sysTakeNoMessageBean) {
        SysTakeNo stn = new SysTakeNo();
        stn.setId(sysTakeNoMessageBean.getId());
        DMap dmap = fundSettingDao.queryOnlyTakeNo(stn);
        SysTakeNoMessageBean stm = new SysTakeNoMessageBean();
        if(dmap.getCount() > 0){
            stm.setId(TypeTool.getInt(dmap.getValue("ID", 0)));
            stm.setCodeType(TypeTool.getString(dmap.getValue("CODE_TYPE", 0)));
            stm.setStartNo(TypeTool.getString(dmap.getValue("START_NO", 0)));
            stm.setCurrNo(TypeTool.getString(dmap.getValue("CURR_NO", 0)));
            stm.setNoLength(TypeTool.getString(dmap.getValue("NO_LENGTH", 0)));
            stm.setStartNo(TypeTool.getString(dmap.getValue("START_NO", 0)));
            stm.setMaxNo(TypeTool.getString(dmap.getValue("MAX_NO", 0)));
            stm.setNoDesc(TypeTool.getString(dmap.getValue("NO_DESC", 0)));
            stm.setNoFormat(TypeTool.getString(dmap.getValue("NO_FORMAT", 0)));
            stm.setCreateDate(TypeTool.getString(dmap.getValue("CREATE_DATE", 0)));
            stm.setCreateUser(TypeTool.getInt(dmap.getValue("CREATE_USER", 0)));
            stm.setStatusMethod(TypeTool.getString(dmap.getValue("STATUS_METHOD", 0)));
        }
        return stm;
    }
    /**
     *查询一个vip等级维护
     */
    @Override
    public MemberLevelMessageBean queryOnlyVipMaintain(MemberLevelMessageBean memberLevelMessageBean) {
        MemberLevel ml = new MemberLevel();
        ml.setId(memberLevelMessageBean.getId());
        DMap dmap = fundSettingDao.queryOnlyVipMaintain(ml);
        MemberLevelMessageBean mlM = new MemberLevelMessageBean();
        if(dmap.getCount() > 0){
            mlM.setId(TypeTool.getInt(dmap.getValue("ID", 0)));
            mlM.setLevelName(TypeTool.getString(dmap.getValue("LEVEL_NAME", 0)));
            mlM.setLevelRequest(TypeTool.getString(dmap.getValue("LEVEL_REQUEST", 0)));
        }
        return mlM;
    }

    /**
     * 更新大字典
     * @param sysDictionaryMessageBean
     * @return
     */
    @Override
    public SysDictionaryMessageBean updateDictionary(SysDictionaryMessageBean sysDictionaryMessageBean) {
        SysDictionary sysD = new SysDictionary();
        sysD.setId(sysDictionaryMessageBean.getId());
        sysD.setGroupType(sysDictionaryMessageBean.getGroupType());
        sysD.setMemberId(sysDictionaryMessageBean.getMemberId());
        sysD.setMemberName(sysDictionaryMessageBean.getMemberName());
        sysD.setActiveFlg(sysDictionaryMessageBean.getActiveFlg());
        sysD.setRemark(sysDictionaryMessageBean.getRemark());
        DaoResult result = fundSettingDao.updateDictionary(sysD);
        if(result.getCode() < 0){
            sysDictionaryMessageBean.setResult("error");
            sysDictionaryMessageBean.setInfo(result.getErrText());
        }else{
            sysDictionaryMessageBean.setResult("success");
            sysDictionaryMessageBean.setInfo("修改成功");
        }
        return sysDictionaryMessageBean;
    }

    @Override
    public MemberFeeLevelMessageBean updateMemberFeeLevel(MemberFeeLevelMessageBean memberFeeLevelMessageBean) {
        MemberFeeLevel sbfl = new MemberFeeLevel();
        sbfl.setActiveFlg(memberFeeLevelMessageBean.getActiveFlg());
        sbfl.setFeeName(memberFeeLevelMessageBean.getFeeName());
        sbfl.setId(memberFeeLevelMessageBean.getId());
        DaoResult result = fundSettingDao.updateMemberFeeLevel(sbfl);
        if(result.getCode() < 0){
            memberFeeLevelMessageBean.setResult("error");
            memberFeeLevelMessageBean.setInfo(result.getErrText());
        }else{
            memberFeeLevelMessageBean.setResult("success");
            memberFeeLevelMessageBean.setInfo("修改成功");
        }
        return memberFeeLevelMessageBean;
    }

    /**
     *
     * 更新会员费用关联表
     */
    @Override
    public MemberFeeRelationMessageBean updateMemberFeeRelation(MemberFeeRelationMessageBean memberFeeRelationMessageBean) {
        MemberFeeRelation memberFeeRelation = new MemberFeeRelation();
        memberFeeRelation.setId(memberFeeRelationMessageBean.getId());
        memberFeeRelation.setFeeId(memberFeeRelationMessageBean.getFeeId());
        memberFeeRelation.setLevelId(memberFeeRelationMessageBean.getLevelId());
        memberFeeRelation.setLevelProportion(memberFeeRelationMessageBean.getLevelProportion());
        DaoResult daoResult = fundSettingDao.updateMemberFeeRelation(memberFeeRelation);
        if(daoResult.getCode() > 0){
            memberFeeRelationMessageBean.setResult("success");
        }else{
            memberFeeRelationMessageBean.setResult("error");
            memberFeeRelationMessageBean.setInfo("修改失败");
        }
        return memberFeeRelationMessageBean;
    }
    /**
     * 更新固定费用过账
     */
    @Override
    public SysManageAmtMessageBean updateSysManageAmt(SysManageAmtMessageBean sysManageAmtMessageBean) {
        SysManageAmt sysManageAmt = new SysManageAmt();
        sysManageAmt.setId(sysManageAmtMessageBean.getId());
        sysManageAmt.setFeeAmt(sysManageAmtMessageBean.getFeeAmt());
        sysManageAmt.setFeeName(sysManageAmtMessageBean.getFeeName());
        sysManageAmt.setActiveFlg(sysManageAmtMessageBean.getActiveFlg());
        DaoResult daoResult = fundSettingDao.updateSysManageAmt(sysManageAmt);
        if(daoResult.getCode() > 0){
            sysManageAmtMessageBean.setResult("success");
        }else{
            sysManageAmtMessageBean.setResult("error");
            sysManageAmtMessageBean.setInfo("修改失败");
        }
        return sysManageAmtMessageBean;
    }

    @Override
    public SysTakeNoMessageBean updateTakeNo(SysTakeNoMessageBean sysTakeNoMessageBean) {
        SysTakeNo stn = new SysTakeNo();
        stn.setId(sysTakeNoMessageBean.getId());
        stn.setCodeType(sysTakeNoMessageBean.getCodeType());
        stn.setStartNo(sysTakeNoMessageBean.getStartNo());
        stn.setCurrNo(sysTakeNoMessageBean.getCurrNo());
        stn.setNoLength(sysTakeNoMessageBean.getNoLength());
        stn.setStartNo(sysTakeNoMessageBean.getStartNo());
        stn.setMaxNo(sysTakeNoMessageBean.getMaxNo());
        stn.setNoDesc(sysTakeNoMessageBean.getNoDesc());
        stn.setNoFormat(sysTakeNoMessageBean.getNoFormat());
        DaoResult daoResult = fundSettingDao.updateTakeNo(stn);
        if(daoResult.getCode() > 0){
            sysTakeNoMessageBean.setResult("success");
        }else{
            sysTakeNoMessageBean.setResult("error");
            sysTakeNoMessageBean.setInfo("修改失败");
        }
        return sysTakeNoMessageBean;
    }

    @Override
    public MemberLevelMessageBean updateVipMaintain(MemberLevelMessageBean memberLevelMessageBean) {
        MemberLevel ml = new MemberLevel();
        ml.setId(memberLevelMessageBean.getId());
        ml.setLevelName(memberLevelMessageBean.getLevelName());
        ml.setLevelRequest(memberLevelMessageBean.getLevelRequest());
        DaoResult daoResult = fundSettingDao.updateVipMaintain(ml);
        if(daoResult.getCode() > 0){
            memberLevelMessageBean.setResult("success");
        }else{
            memberLevelMessageBean.setResult("error");
            memberLevelMessageBean.setInfo("修改失败");
        }
        return memberLevelMessageBean;
    }
}
