package com.mh.peer.service.dataStatistic;

import com.mh.peer.model.message.PlatIncomeMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016-9-18.
 * 平台收入管理
 */
public interface PlatIncomeService {

    /**
     * 平台收入统计集合
     * @param platIncomeMes
     * @return
     */
    List<PlatIncomeMessageBean> getPlatIncomeList(PlatIncomeMessageBean platIncomeMes);

    /**
     * 平台收入统计数量
     * @param platIncomeMes
     * @return
     */
    int getPlatIncomeCount(PlatIncomeMessageBean platIncomeMes);
}
