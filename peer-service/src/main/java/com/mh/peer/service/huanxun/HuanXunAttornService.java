package com.mh.peer.service.huanxun;

import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-8-2.
 * 债权转让(环迅)
 */
public interface HuanXunAttornService {

    /**
     * 债权转让资金冻结
     * @param huanxunFreezeAttorn
     * @param tradeDetail
     * @param memberMessage
     * @return
     */
    public void attornhxResult(Map<String,Object> mapException,HuanxunFreezeAttorn huanxunFreezeAttorn,TradeDetail tradeDetail,MemberMessage memberMessage);

    /**
     * 获取债权冻结信息
     * @param huanXunFreezeMes
     * @return
     */
    public List<HuanXunFreezeMessageBean> getFreezeAttornInfo(HuanXunFreezeMessageBean huanXunFreezeMes);

    /**
     * 解冻
     * @param memberInfo
     * @param huanxunFreeze
     */
    public void unfreezeAttornResult(Map<String,Object> mapException,MemberInfo memberInfo,HuanxunFreeze huanxunFreeze,
                                     ProductAttornRecord productAttornRecord,ProductBuyInfo productBuyInfo);
}
