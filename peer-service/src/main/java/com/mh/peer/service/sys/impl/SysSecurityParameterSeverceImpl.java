package com.mh.peer.service.sys.impl;

import com.mh.peer.dao.manager.SysSecurityParameterDao;
import com.mh.peer.model.business.SysSecurityParameterBusinessBean;
import com.mh.peer.model.entity.SysSecurityParameter;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.mh.peer.model.message.SysSecurityParameterMessageBean;
import com.mh.peer.service.sys.SysSecurityParameterService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-3-31.
 */
@Service
public class SysSecurityParameterSeverceImpl implements SysSecurityParameterService {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private SysSecurityParameterDao sysSecurityParameterDao;

    /**
     * 新增安全参数
     * @param sysSecurityParameterMess
     * @return
     */
    @Override
    public SysSecurityParameterBusinessBean addSysSecurityParameter(SysSecurityParameterMessageBean sysSecurityParameterMess) {
        //创建业务对象
        SysSecurityParameterBusinessBean sysSecurityParameterBus = new SysSecurityParameterBusinessBean();

        //创建实体对象
        SysSecurityParameter sysSecurityParameter = new SysSecurityParameter();
        sysSecurityParameter.setKeyword(sysSecurityParameterMess.getKeyword()); //安全参数
        sysSecurityParameter.setActiveFlg(sysSecurityParameterMess.getActiveFlg()); //是否启用密码错误次数超限锁定
        sysSecurityParameter.setTimes(TypeTool.getInt(sysSecurityParameterMess.getTimes())); //密码错误次数
        sysSecurityParameter.setTime(TypeTool.getInt(sysSecurityParameterMess.getTime())); //密码输入锁定时长
        DaoResult result = sysSecurityParameterDao.addSysSecurityParameter(sysSecurityParameter); //新增安全问题
        if(result.getCode() < 0){
            sysSecurityParameterBus.setResult("error");
            sysSecurityParameterBus.setInfo(result.getErrText());
            return sysSecurityParameterBus;
        }
        sysSecurityParameterBus.setResult("success");
        sysSecurityParameterBus.setInfo("设置成功");
        return sysSecurityParameterBus;
    }

    /**
     * 修改安全参数部分
     * @param sysSecurityParameterMess
     * @return
     */
    @Override
    public SysSecurityParameterBusinessBean updateSysSecurityParameter(SysSecurityParameterMessageBean sysSecurityParameterMess) {

        //创建业务对象
        SysSecurityParameterBusinessBean  sysSecurityParameterBus = new  SysSecurityParameterBusinessBean();
        //创建实体对象
        SysSecurityParameter sysSecurityParameter = new SysSecurityParameter();
        sysSecurityParameter.setId(TypeTool.getInt(sysSecurityParameterMess.getId())); //主键
        sysSecurityParameter.setKeyword(sysSecurityParameterMess.getKeyword()); //关键字
        sysSecurityParameter.setActiveFlg(sysSecurityParameterMess.getActiveFlg()); //是否启用密码错误次数超限锁定
        sysSecurityParameter.setTimes(TypeTool.getInt(sysSecurityParameterMess.getTimes())); //密码错误次数
        sysSecurityParameter.setTime(TypeTool.getInt(sysSecurityParameterMess.getTime())); //密码输入锁定时长

        DaoResult result = sysSecurityParameterDao.updateSecurityParameter(sysSecurityParameter); //修改安全问题
        if(result.getCode() < 0){
            sysSecurityParameterBus.setResult("error");
            sysSecurityParameterBus.setInfo(result.getErrText());
            return sysSecurityParameterBus;
        }
        sysSecurityParameterBus.setResult("success");
        sysSecurityParameterBus.setInfo("设置成功");

        return sysSecurityParameterBus;
    }


    /**
     * 获取安全问题信息
     * @param sysSecurityParameterMes
     * @return
     */
    @Override
    public SysSecurityParameterMessageBean querySecurityParameter(SysSecurityParameterMessageBean sysSecurityParameterMes) {
        List<SysSecurityParameter> sysSecurityParameterList = new ArrayList<SysSecurityParameter>(); //安全参数集合
        SysSecurityParameter sysSecurityParameter = new SysSecurityParameter(); //安全参数
        sysSecurityParameterList = sysSecurityParameterDao.querySecurityParameter(sysSecurityParameter);

        if(sysSecurityParameterList != null && sysSecurityParameterList.size() > 0){
            sysSecurityParameterMes.setId(TypeTool.getString(sysSecurityParameterList.get(0).getId())); //主键
            sysSecurityParameterMes.setKeyword(sysSecurityParameterList.get(0).getKeyword()); //关键字
            sysSecurityParameterMes.setActiveFlg(sysSecurityParameterList.get(0).getActiveFlg()); //是否启用密码错误次数超限锁定
            sysSecurityParameterMes.setTimes(TypeTool.getString(sysSecurityParameterList.get(0).getTimes())); //密码错误次数
            sysSecurityParameterMes.setTime(TypeTool.getString(sysSecurityParameterList.get(0).getTime())); //密码输入锁定时长
        }

        return sysSecurityParameterMes;
    }

}
