package com.mh.peer.service.huanxun;

import com.mh.peer.model.entity.HuanxunTransfer;
import com.mh.peer.model.entity.HuanxunTransferDetail;
import com.mh.peer.model.message.HuanXunTransferMessage;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-31.
 */
public interface HuanXunTransferService {

    /**
     * 根据条件获取转账信息
     * @param huanXunTransferMes
     * @return
     */
    List<HuanXunTransferMessage> getTransferInfoBySome(HuanXunTransferMessage huanXunTransferMes);

    /**
     * 投资转账
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     */
    void transferBuyResult(Map<String,Object> mapException,HuanxunTransfer huanxunTransfer,List<HuanxunTransferDetail> listHuanxunTransferDetail,String productId);

    /**
     * 投资转账详细信息
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     * @param attornId
     */
    void transferAttornResult(Map<String,Object> mapException,HuanxunTransfer huanxunTransfer,List<HuanxunTransferDetail> listHuanxunTransferDetail,String attornId);

    /**
     * 保存转账信息 bycuibin
     */
    public boolean saveHuanxunTransfer(HuanxunTransfer huanxunTransfer,List<HuanxunTransferDetail> listHuanxunTransferDetail,String receiveId);

    /**
     * 查询转账信息是否存在 bycuibin
     */
    public boolean queryIsTransfer(String uuid);

    /**
     * 根据产品id和应还款日期获取需转账信息集合
     * @param productId
     * @param yrepaymentTime
     * @return
     */
    List<HuanXunTransferMessage> getRepayTransferAcctDetail(String productId,String yrepaymentTime);

    /**
     * 根据冻结信息还款
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     * @param mapException
     * @param repayId
     * @param yRepayTime
     * @return
     */
    String repayhxPriceTransfer(HuanxunTransfer huanxunTransfer,List<HuanxunTransferDetail> listHuanxunTransferDetail,
                                Map<String,Object> mapException,String repayId,String yRepayTime);
}
