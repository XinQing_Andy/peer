package com.mh.peer.service.platform;

import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.message.QuestionContentMessageBean;

/**
 * Created by Cuibin on 2016/4/19.
 */
public interface QuestionService {
    /**
     *查询所有常见问题
     */
    Paging queryQuestionByPage(Paging paging);
    /**
     *删除常见问题
     */
    QuestionContentMessageBean deleteQuestion(QuestionContentMessageBean questionContentMessageBean);
    /**
     *添加常见问题
     */
    QuestionContentMessageBean addQuestion(QuestionContentMessageBean questionContentMessageBean);
    /**
     *查询一个
     */
    QuestionContentMessageBean queryOnlyQuestion(QuestionContentMessageBean questionContentMessageBean);
    /**
     *修改常见问题
     */
    QuestionContentMessageBean updateQuestion(QuestionContentMessageBean questionContentMessageBean);
    /**
     *常见问题是否启用
     */
    QuestionContentMessageBean updateStart(QuestionContentMessageBean questionContentMessageBean);
}
