package com.mh.peer.service.recharge;

import com.mh.peer.model.message.ViewHuanxunRechargeMes;

import java.util.List;

/**
 * Created by Cuibin on 2016/7/27.
 */
public interface RechargeHistoryService {

    /**
     * 查询充值统计
     */
    public List<ViewHuanxunRechargeMes> queryRechargeHistory(ViewHuanxunRechargeMes viewHuanxunRechargeMes);

    /**
     * 查询充值统计 总数
     */
    public String queryRechargeHistoryCount(ViewHuanxunRechargeMes viewHuanxunRechargeMes);
}
