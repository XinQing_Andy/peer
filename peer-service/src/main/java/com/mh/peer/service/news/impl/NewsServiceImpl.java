package com.mh.peer.service.news.impl;

import com.mh.peer.dao.news.NewsDao;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.business.NewsContentBusinessBean;
import com.mh.peer.model.business.NewsTypeBusinessBean;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.*;
import com.mh.peer.service.news.NewsService;
import com.mh.peer.util.HtmlRegexpUtil;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Cuibin on 2016/4/7.
 */
@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDao newsDao;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 内容管理(查询所有新闻内容 带分页)
     */
    @Override
    public Paging queryNewsByPage(NewsContentMessageBean newsContentM) {
        DMap dmap = newsDao.queryNewsByPage(newsContentM);
        int count = newsDao.queryNewsByPageCount(newsContentM).getCount();
        List<NewsContentMessageBean> list = new ArrayList<NewsContentMessageBean>();
        if (dmap.getCount() > 0) {
            String firstDate = null;
            String createDate = null;
            String showDate = null;
            for (int i = 0; i < dmap.getCount(); i++) {
                NewsContentMessageBean nem = new NewsContentMessageBean();
                nem.setId(TypeTool.getString(dmap.getValue("ID", i)));
                nem.setTitle(TypeTool.getString(dmap.getValue("TITLE", i)));
                nem.setContent(TypeTool.getString(dmap.getValue("CONTENT", i)));
                createDate = TypeTool.getString(dmap.getValue("CREATE_DATE", i));
                if (createDate.length() >= 14) {
                    createDate = createDate.substring(0, 4) + "-" + createDate.substring(4, 6) + "-" + createDate.substring(6, 8) + " " + createDate.substring(8, 10) + ":" +
                            createDate.substring(10, 12) + ":" + createDate.substring(12, 14);
                }
                createDate = createDate.substring(0, 10);
                nem.setCreateDate(createDate);
                nem.setCreateUser(TypeTool.getString(dmap.getValue("CREATE_USER", i)));
                nem.setType(TypeTool.getString(dmap.getValue("TYPE", i)));
                nem.setAvailability(TypeTool.getString(dmap.getValue("AVAILABILITY", i)));
                nem.setFirst(TypeTool.getString(dmap.getValue("FIRST", i)));
                firstDate = TypeTool.getString(dmap.getValue("FIRST_DATE", i));
                if (firstDate.length() >= 8) {
                    firstDate = firstDate.substring(0, 4) + "-" + firstDate.substring(4, 6) + "-" + firstDate.substring(6, 8);
                }
                nem.setFirstDate(firstDate);
                nem.setLookCount(TypeTool.getString(dmap.getValue("LOOK_COUNT", i)));
                nem.setRid(TypeTool.getString(dmap.getValue("RID", i)));
                nem.setKindId(TypeTool.getString(dmap.getValue("KIND_ID", i)));
                nem.setIsRecommend(TypeTool.getString(dmap.getValue("IS_RECOMMEND", i)));
                showDate = TypeTool.getString(dmap.getValue("SHOW_DATE", i));
                if (showDate.length() >= 8) {
                    showDate = showDate.substring(0, 4) + "-" + showDate.substring(4, 6) + "-" + showDate.substring(6, 8);
                }
                nem.setShowDate(showDate);
                list.add(nem);
            }

        }
        Paging newPag = new Paging();
        newPag.setList(list);
        newPag.setCount(count);
        return newPag;
    }


    /**
     * 内容管理(查询所有新闻内容 带分页)
     */
    @Override
    public List<NewsContentMessageBean> queryNewsByP(NewsContentMessageBean newsContentMes) {
        DMap dmap = newsDao.queryNewsByPage(newsContentMes);
        List<NewsContentMessageBean> listNewsContentMes = new ArrayList<NewsContentMessageBean>();
        if (dmap != null && dmap.getCount() > 0) {
            for (int i=0;i<dmap.getCount();i++){
                NewsContentMessageBean newsContentMessageBean = new NewsContentMessageBean();
                newsContentMessageBean.setId(dmap.getValue("ID",i)); //主键
                newsContentMessageBean.setTitle(dmap.getValue("TITLE",i)); //题目
                newsContentMessageBean.setContent(dmap.getValue("CONTENT",i)); //内容
                newsContentMessageBean.setCreateDate(dmap.getValue("CREATE_DATE",i)); //创建时间
                newsContentMessageBean.setShowDate(dmap.getValue("SHOW_DATE",i)); //显示时间
                newsContentMessageBean.setbName(dmap.getValue("SECONDTYPENAME",i)); //二级类别名称
                listNewsContentMes.add(newsContentMessageBean);
            }
        }
        return listNewsContentMes;
    }

    /**
     * 查询新闻内容数量
     * @param newsContentMes
     * @return
     */
    @Override
    public int queryNewsCount(NewsContentMessageBean newsContentMes) {
        DMap dMap = newsDao.queryNewsCount(newsContentMes);
        int totalCount = dMap.getCount(); //数量
        return totalCount;
    }


    /**
     * 查询新闻数量
     */
    @Override
    public int queryNewsByPageCount(NewsContentMessageBean newsContentM) {
        if (newsContentM == null) {
            return 0;
        }
        return newsDao.queryNewsByPageCount(newsContentM).getCount();
    }


    /**
     * 类别管理(查询所有类别)
     */
    @Override
    public List<NewsTypeMessageBean> queryType() {
        DMap dmap = newsDao.queryType();
        List<NewsTypeMessageBean> list = new ArrayList<NewsTypeMessageBean>();
        if (dmap.getCount() > 0) {
            for (int i = 0; i < dmap.getCount(); i++) {
                NewsTypeMessageBean newsTypeM = new NewsTypeMessageBean();
                newsTypeM.setId(dmap.getValue("ID", i));
                newsTypeM.setName(TypeTool.getString(dmap.getValue("NAME", i)));
                newsTypeM.setFid(TypeTool.getString(dmap.getValue("FID", i)));
                newsTypeM.setDescription(TypeTool.getString(dmap.getValue("DESCRIPTION", i)));
                list.add(newsTypeM);
            }
        }
        return list;
    }

    /**
     * 类别管理-编辑后保存
     */
    @Override
    public NewsTypeBusinessBean saveType(NewsTypeMessageBean newTypeM) {
        NewsTypeBusinessBean newsTypeB = new NewsTypeBusinessBean();
        NewsType newsType = new NewsType();
        newsType.setId(TypeTool.getInt(newTypeM.getId()));
        newsType.setName(newTypeM.getName());
        newsType.setFid(TypeTool.getInt(newTypeM.getFid()));
        newsType.setDescription(newTypeM.getDescription());
        DaoResult result = newsDao.saveType(newsType);
        if (result.getCode() < 0) {
            newsTypeB.setResult("error");
            newsTypeB.setInfo(result.getErrText());
        } else {
            newsTypeB.setResult("success");
            newsTypeB.setInfo("修改新闻类别成功");
        }
        return newsTypeB;
    }

    /**
     * 内容管理 删除新闻
     */
    @Override
    public NewsContentBusinessBean deleteNews(NewsContentMessageBean newsContentM) {
        NewsContent newsC = new NewsContent();
        newsC.setId(newsContentM.getId());
        DaoResult result = newsDao.deleteNews(newsC);
        NewsContentBusinessBean newsConB = new NewsContentBusinessBean();
        if (result.getCode() < 0) {
            newsConB.setResult("error");
            newsConB.setInfo(result.getErrText());
        } else {
            newsConB.setResult("success");
            newsConB.setInfo("删除新闻成功");
        }
        return newsConB;
    }


    /**
     * 内容管理 添加新闻
     */
    @Override
    public NewsContentBusinessBean saveNews(NewsContentMessageBean newsContentMes) {
        NewsContentBusinessBean newsContentBus = new NewsContentBusinessBean();
        NewsContent newsContent = new NewsContent();
        newsContent.setId(UUID.randomUUID().toString().replaceAll("-","")); //主键
        newsContent.setKindId(TypeTool.getInt(newsContentMes.getKindId())); //类型id
        newsContent.setTitle(newsContentMes.getTitle()); //题目
        newsContent.setCreateUser(newsContentMes.getCreateUser()); //作者
        newsContent.setShowDate(newsContentMes.getShowDate()); //显示时间
        newsContent.setContent(newsContentMes.getContent()); //内容
        newsContent.setAvailability("1"); //有效标识 0：无效  1：有效
        newsContent.setIsRecommend("0"); //是否推荐 1：推荐 0：不推荐
        newsContent.setCreateDate(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //添加时间
        DaoResult result = newsDao.saveNews(newsContent);

        if (result.getCode() < 0) {
            newsContentBus.setResult("error");
            newsContentBus.setInfo(result.getErrText());
        } else {
            newsContentBus.setResult("success");
            newsContentBus.setInfo("添加新闻成功");
        }
        return newsContentBus;
    }


    /**
     * 保存图片信息
     */
    @Override
    public void saveImg(String path, String absPath, String uuid) {
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        int userId = bean.getSysUser().getId();
        SysFile sysFile = new SysFile();
        sysFile.setId(UUID.randomUUID().toString().replaceAll("\\-", ""));
        sysFile.setGlid(uuid);
        sysFile.setImgurl(path);
        sysFile.setImgurljd(absPath);
        sysFile.setCreateuser(userId + "");
        sysFile.setType("0");
        sysFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyyMMdd"));
        sysFile.setSort(0);
        DaoResult result = newsDao.saveImg(sysFile);
    }

    /**
     * 内容管理 修改新闻 查询单个新闻
     */
    @Override
    public NewsContentMessageBean queryNewsById(NewsContentMessageBean newsContent) {
        DMap dmap = newsDao.queryNewsById(newsContent);
        NewsContentMessageBean newsConM = new NewsContentMessageBean();
        if (dmap.getCount() > 0) {
            newsConM.setKindId(TypeTool.getString(dmap.getValue("SECONDTYPE", 0))); //二级类别id
            newsConM.setbName(dmap.getValue("SECONDTYPENAME", 0)); //二级类别名城
            newsConM.setFirstType(TypeTool.getString(dmap.getValue("FIRSTTYPE", 0))); //一级类别
            newsConM.setId(TypeTool.getString(dmap.getValue("ID", 0))); //主键
            newsConM.setTitle(TypeTool.getString(dmap.getValue("TITLE", 0)));
            newsConM.setCreateUser(TypeTool.getString(dmap.getValue("CREATE_USER", 0)));
            String showDate = TypeTool.getString(dmap.getValue("SHOW_DATE", 0));
            newsConM.setShowDate(showDate);
            System.out.println("content======"+dmap.getValue("CONTENT", 0));
            newsConM.setContent(TypeTool.getString(dmap.getValue("CONTENT", 0))); //内容
        }
        return newsConM;
    }

    /**
     * 内容管理 修改新闻 查询图片信息
     */
    @Override
    public SysFileMessageBean queryImg(String newsId) {
        DMap dmap = newsDao.queryImg(newsId);
        SysFileMessageBean sysFileM = new SysFileMessageBean();
        if (dmap.getCount() > 0) {
            sysFileM.setId(TypeTool.getString(dmap.getValue("ID", 0)));
            sysFileM.setImgurljd(TypeTool.getString(dmap.getValue("IMGURLJD", 0)));
            String url = TypeTool.getString(dmap.getValue("IMGURL", 0));
            sysFileM.setImgurl(url);
            if (url != null && !url.equals("")) {
                url = url.replace("\\", "/");
                int j = url.lastIndexOf("/");
                if (j >= 0) {
                    url = url.substring(j + 1);
                }
            }
            sysFileM.setImgName(url);
        }
        return sysFileM;
    }


    /**
     * 修改新闻
     */
    @Override
    public NewsContentBusinessBean updateNews(NewsContentMessageBean newsContentMes) {
        NewsContentBusinessBean newsContentBus = new NewsContentBusinessBean();
        NewsContent newsContent = new NewsContent();
        newsContent.setId(newsContentMes.getId()); //主键
        newsContent.setTitle(newsContentMes.getTitle()); //题目
        newsContent.setContent(newsContentMes.getContent()); //内容
        newsContent.setCreateUser(newsContentMes.getCreateUser()); //作者
        newsContent.setShowDate(newsContentMes.getShowDate()); //显示时间
        DaoResult daoResult = newsDao.updateNews(newsContent);

        if (daoResult.getCode() < 0) {
            newsContentBus.setResult("error");
            newsContentBus.setInfo(daoResult.getErrText());
        } else {
            newsContentBus.setResult("success");
            newsContentBus.setInfo("修改新闻成功");
        }
        return newsContentBus;
    }

    /**
     * 内容管理 删除图片
     */
    @Override
    public void deleteImg(String imgId) {
        SysFile sysFile = new SysFile();
        sysFile.setId(imgId);
        newsDao.deleteImg(sysFile);
    }

    /**
     * 内容管理 查询新闻分类
     */
    public List<NewsTypeMessageBean> queryNewsType(NewsTypeMessageBean newsTypeMes) {
        DMap dmap = newsDao.queryNewsType(newsTypeMes);
        List<NewsTypeMessageBean> listNewsType = new ArrayList<NewsTypeMessageBean>(); //新闻类别
        if (dmap != null && dmap.getCount() > 0){
            for (int i=0;i<dmap.getCount();i++){
                NewsTypeMessageBean newsTypeMessageBean = new NewsTypeMessageBean();
                newsTypeMessageBean.setId(dmap.getValue("ID", i)); //主键
                newsTypeMessageBean.setName(dmap.getValue("NAME",i)); //名称
                newsTypeMessageBean.setFid(dmap.getValue("FID",i)); //父id
                newsTypeMessageBean.setDescription(dmap.getValue("DESCRIPTION",i)); //描述
                listNewsType.add(newsTypeMessageBean);
            }
        }
        return listNewsType;
    }

    /**
     * 修改新闻是否显示
     */
    public NewsContentMessageBean updateShow(NewsContentMessageBean newsContentMessageBean) {
        NewsContent newsC = new NewsContent();
        newsC.setId(newsContentMessageBean.getId());
        newsC.setAvailability(newsContentMessageBean.getAvailability());
        DaoResult result = newsDao.updateShow(newsC);
        if (result.getCode() < 0) {
            newsContentMessageBean.setResult("error");
            newsContentMessageBean.setInfo(result.getErrText());
        } else {
            newsContentMessageBean.setResult("success");
            newsContentMessageBean.setInfo("修改成功");
        }
        return newsContentMessageBean;
    }

    /**
     * 修改新闻是否推荐
     */
    public NewsContentMessageBean updateRecommend(NewsContentMessageBean newsContentMessageBean) {
        NewsContent newsC = new NewsContent();
        newsC.setId(newsContentMessageBean.getId());
        newsC.setIsRecommend(newsContentMessageBean.getIsRecommend());
        DaoResult result = newsDao.updateRecommend(newsC);
        if (result.getCode() < 0) {
            newsContentMessageBean.setResult("error");
            newsContentMessageBean.setInfo(result.getErrText());
        } else {
            newsContentMessageBean.setResult("success");
            newsContentMessageBean.setInfo("修改成功");
        }
        return newsContentMessageBean;
    }

    /**
     * 添加二级类别
     */
    @Override
    public NewsTypeMessageBean addTwoType(NewsTypeMessageBean newsTypeMessageBean) {
        NewsType newsT = new NewsType();
        newsT.setName(newsTypeMessageBean.getName());
        newsT.setFid(TypeTool.getInt(newsTypeMessageBean.getFid()));
        DaoResult result = newsDao.addTwoType(newsT);
        if (result.getCode() < 0) {
            newsTypeMessageBean.setResult("error");
            newsTypeMessageBean.setInfo(result.getErrText());
        } else {
            newsTypeMessageBean.setResult("success");
            newsTypeMessageBean.setInfo("添加成功");
        }
        return newsTypeMessageBean;
    }

    /**
     * 删除二级类别
     */
    @Override
    public NewsTypeMessageBean deleteTwoNewsType(NewsTypeMessageBean newsTypeMessageBean) {
        NewsType newsType = new NewsType();
        newsType.setId(TypeTool.getInt(newsTypeMessageBean.getId()));
        DaoResult daoResult = newsDao.deleteTwoNewsType(newsType);
        if (daoResult.getCode() < 0) {
            newsTypeMessageBean.setResult("error");
            newsTypeMessageBean.setInfo(daoResult.getErrText());
        } else {
            newsTypeMessageBean.setResult("success");
            newsTypeMessageBean.setInfo("删除成功");
        }
        return newsTypeMessageBean;
    }

    /**
     * 查询一级类别
     */
    @Override
    public List<NewsTypeMessageBean> queryOneType() {
        DMap dmap = newsDao.queryType();
        List<NewsTypeMessageBean> list = new ArrayList<NewsTypeMessageBean>();
        if (dmap.getCount() > 0) {
            for (int i = 0; i < dmap.getCount(); i++) {
                int fid = TypeTool.getInt(dmap.getValue("FID", i));
                if (fid == 0) {
                    NewsTypeMessageBean newsTypeM = new NewsTypeMessageBean();
                    newsTypeM.setId(dmap.getValue("ID", i));
                    newsTypeM.setName(TypeTool.getString(dmap.getValue("NAME", i)));
                    newsTypeM.setFid(TypeTool.getString(fid));
                    newsTypeM.setDescription(TypeTool.getString(dmap.getValue("DESCRIPTION", i)));
                    list.add(newsTypeM);
                }
            }
        }
        return list;
    }

    /**
     * 更新新闻访问次数(用户端)
     */
    @Override
    public void updateNewsVisit(NewsContentMessageBean newsContentMessageBean) {
        int temp = Integer.parseInt(newsContentMessageBean.getLookCount());
        temp++;
        NewsContent newsContent = new NewsContent();
        newsContent.setLookCount(temp);
        newsContent.setId(newsContentMessageBean.getId());
        newsDao.updateNewsVisit(newsContent);
    }


    /**
     * 获取app端新闻(App端)
     *
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<AppHomeMessageBean> getAppNews(String orderColumn, String orderType, String pageIndex, String pageSize) {
        DMap dMap = newsDao.getAppNews(orderColumn, orderType, pageIndex, pageSize);
        List<AppHomeMessageBean> listAppHomeMes = new ArrayList<AppHomeMessageBean>();
        String title = ""; //题目
        String showTime = ""; //展示时间
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                AppHomeMessageBean appHomeMessageBean = new AppHomeMessageBean();
                appHomeMessageBean.setNewsId(dMap.getValue("ID", i)); //新闻id
                title = dMap.getValue("TITLE", i); //新闻题目
                if (title.length() > 15){
                    title = title.substring(0,15) + "...";
                }
                appHomeMessageBean.setNewsTitle(title);
                showTime = dMap.getValue("SHOW_DATE", i); //显示时间
                appHomeMessageBean.setNewsShowTime(showTime); //显示时间
                listAppHomeMes.add(appHomeMessageBean);
            }
        }
        return listAppHomeMes;
    }

    /**
     * 获取新闻列表(App端)
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<AppNewsContentMessageBean> getAppNewsList(String orderColumn,String orderType,String pageIndex, String pageSize) {
        List<AppNewsContentMessageBean> listAppNewsContent = new ArrayList<AppNewsContentMessageBean>();
        DMap dMap = newsDao.getAppNews(orderColumn,orderType,pageIndex,pageSize);
        String title = ""; //题目
        String content = ""; //内容
        String showDate = ""; //显示时间
        if (dMap != null && dMap.getCount()>0){
            for (int i=0;i<dMap.getCount();i++){
                AppNewsContentMessageBean appNewsContentMessageBean = new AppNewsContentMessageBean();
                appNewsContentMessageBean.setId(dMap.getValue("ID",i)); //主键

                title = dMap.getValue("TITLE",i); //题目
                if (title.length() > 20){
                    title = title.substring(0,20);
                }
                appNewsContentMessageBean.setTitle(title);

                content = dMap.getValue("CONTENT",i); //内容
                if (content.length() > 20){
                    content = content.substring(0,20);
                }
                appNewsContentMessageBean.setContent(content);

                showDate = dMap.getValue("SHOW_DATE",i); //显示时间
                appNewsContentMessageBean.setShowDate(showDate);
                listAppNewsContent.add(appNewsContentMessageBean);
            }
        }
        return listAppNewsContent;
    }

    /**
     * 获取新闻部分详情
     * @param newsId
     * @return
     */
    @Override
    public AppNewsContentMessageBean getAppNewsDetails(String newsId) {
        AppNewsContentMessageBean appNewsContentMessageBean = new AppNewsContentMessageBean();
        DMap dMap = newsDao.getAppNewsDetails(newsId);
        String title = ""; //题目
        String showDate = ""; //显示时间
        if (dMap != null){
            appNewsContentMessageBean.setId(dMap.getValue("ID",0)); //主键
            appNewsContentMessageBean.setAvailability(dMap.getValue("AVAILABILITY",0)); //有效标识 0：无效  1：有效
            title = dMap.getValue("TITLE",0); //题目
            if (title != null && !title.equals("")){
                if (title.length() > 15){
                    title = title.substring(0,15) + "...";
                }
            }
            appNewsContentMessageBean.setTitle(title);
            appNewsContentMessageBean.setContent(dMap.getValue("CONTENT",0)); //内容
            showDate = dMap.getValue("SHOWDATE",0); //显示时间
            if (showDate != null && !showDate.equals("")){
                showDate = showDate.substring(0,4) + "-" + showDate.substring(4,6) + "-" + showDate.substring(6,8);
            }
            appNewsContentMessageBean.setShowDate(showDate);
        }
        return appNewsContentMessageBean;
    }


    /**
     * 获取新闻列表(微信) by cuibin
     */
    @Override
    public List<WeixinNewsMes> queryNewsList(WeixinNewsMes weixinNewsMes) {
        DMap dMap = newsDao.queryNewsList(weixinNewsMes);
        List<WeixinNewsMes> list = new ArrayList<WeixinNewsMes>();
        HtmlRegexpUtil htmlRegexpUtil = new HtmlRegexpUtil();
        String showTime = ""; //展示时间
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                WeixinNewsMes temp = new WeixinNewsMes();
                String id = dMap.getValue("ID", i);
                temp.setId(id); //新闻id
                String title = dMap.getValue("TITLE", i);
                if (title != null && !title.equals("")) {
                    if (title.length() > 20) {
                        title = title.substring(0, 20) + "...";
                    }
                }
                temp.setTitle(title); //新闻题目
                showTime = dMap.getValue("SHOW_DATE", i); //显示时间
                if (showTime != null && !showTime.equals("")) {
                    showTime = showTime.substring(0, 4) + "-" + showTime.substring(4, 6) + "-" + showTime.substring(6, 8);
                }
                temp.setShowDate(showTime); //显示时间
                String content = htmlRegexpUtil.filterHtml(dMap.getValue("CONTENT", i));

                if (content != null && !content.equals("")) {
                    if (content.length() > 15) {
                        content = content.substring(0, 15) + "......";
                    }
                }

                temp.setContent(content);
                temp.setImg(newsDao.queryImg(id).getValue("IMGURL", 0)); //图片路径

                list.add(temp);
            }
        }
        return list;
    }

    /**
     * 查询一个新闻(微信) by cuibin
     */
    @Override
    public WeixinNewsMes queryOnlyNews(String id) {
        DMap dMap = newsDao.queryOnlyNews(id);
        String showTime = "";
        WeixinNewsMes temp = new WeixinNewsMes();
        if (dMap.getCount() > 0) {
            String newId = dMap.getValue("ID", 0);
            temp.setId(newId); //新闻id
            temp.setTitle(dMap.getValue("TITLE", 0)); //新闻题目
            showTime = dMap.getValue("SHOW_DATE", 0); //显示时间
            if (showTime != null && !showTime.equals("")) {
                showTime = showTime.substring(0, 4) + "-" + showTime.substring(4, 6) + "-" + showTime.substring(6, 8);
            }
            temp.setShowDate(showTime); //显示时间
            temp.setContent(dMap.getValue("CONTENT", 0));
            temp.setImg(newsDao.queryImg(newId).getValue("IMGURL", 0)); //图片路径
        }
        return temp;
    }

    /**
     * 获取新闻列表总数(微信) by cuibin
     */
    @Override
    public String queryNewsCount(WeixinNewsMes weixinNewsMes) {
        DMap dMap = newsDao.queryNewsCount(weixinNewsMes);
        return dMap.getValue("COUNT", 0);
    }


    /**
     * 搜索财富资讯
     * @param weathInformationMes
     * @return
     */
    @Override
    public List<WeathInformationMessageBean> queryWeathByPage(WeathInformationMessageBean weathInformationMes) {
        List<WeathInformationMessageBean> listWeathInformationMes = new ArrayList<WeathInformationMessageBean>();
        DMap dMap = newsDao.queryWeathByPage(weathInformationMes);
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                WeathInformationMessageBean weathInformationMessageBean = new WeathInformationMessageBean(); //财富资讯
                weathInformationMessageBean.setId(dMap.getValue("ID",i)); //主键
                weathInformationMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                weathInformationMessageBean.setContent(dMap.getValue("CONTENT",i)); //内容
                weathInformationMessageBean.setShowDate(dMap.getValue("SHOW_DATE",i)); //显示时间
                listWeathInformationMes.add(weathInformationMessageBean);
            }
        }
        return listWeathInformationMes;
    }


    /**
     * 搜索财富资讯数量
     * @param weathInformationMes
     * @return
     */
    @Override
    public int queryWeathCount(WeathInformationMessageBean weathInformationMes) {
        DMap dMap = newsDao.queryWeathCount(weathInformationMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 搜索财富资讯详细信息
     * @param weathInformationMes
     * @return
     */
    @Override
    public WeathInformationMessageBean queryWeathDetails(WeathInformationMessageBean weathInformationMes) {
        DMap dMap = newsDao.queryWeathDetails(weathInformationMes);
        WeathInformationMessageBean weathInformationMessageBean = new WeathInformationMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            weathInformationMessageBean.setId(dMap.getValue("ID",0)); //主键
            weathInformationMessageBean.setTitle(dMap.getValue("TITLE",0)); //题目
            weathInformationMessageBean.setContent(dMap.getValue("CONTENT",0)); //内容
            weathInformationMessageBean.setShowDate(dMap.getValue("SHOW_DATE",0)); //显示日期
            weathInformationMessageBean.setFirstTypeId(dMap.getValue("FIRSTTYPE",0)); //一级类别
            weathInformationMessageBean.setSecondTypeId(dMap.getValue("SECONDTYPE",0)); //二级类别
        }
        return weathInformationMessageBean;
    }
}
