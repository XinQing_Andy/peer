package com.mh.peer.service.product.impl;

import com.mh.peer.dao.product.ProductReceiveDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.product.ProductReceiveService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cuibin on 2016/5/25.
 */
@Service
public class ProductReceiveServiceImpl implements ProductReceiveService{

    @Autowired
    private ProductReceiveDao productReceiveDao;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 项目详情 已收详情
     */
    @Override
    public ProductReceiveMessageBean alreadyReceive(ProductReceiveMessageBean productReceiveMessageBean,String userId) {
        DMap dMap = productReceiveDao.alreadyReceive(productReceiveMessageBean,userId);
        ProductReceiveMessageBean productReceiveMessageBean1 = new ProductReceiveMessageBean();
        double interest = 0; //应收利息
        double principal = 0; //应收本金
        double receiveprice = 0; //回款金额
        if(dMap.getCount() > 0){
            /*************** 还款部分循环Start ***************/
            for (int k = 0; k < dMap.getCount(); k++) {
                interest = interest + TypeTool.getDouble(dMap.getValue("INTEREST",k));
                principal = principal + TypeTool.getDouble(dMap.getValue("PRINCIPAL",k));
                receiveprice = receiveprice + TypeTool.getDouble(dMap.getValue("RECEIVEPRICE",k));
            }
            /**************** 还款部分循环End ****************/
        }
        DecimalFormat df = new DecimalFormat("#.00");
        productReceiveMessageBean1.setInterest(df.format(interest));
        productReceiveMessageBean1.setPrincipal(df.format(principal));
        productReceiveMessageBean1.setReceiveprice(df.format(receiveprice));
        return productReceiveMessageBean1;
    }


    /**
     * 项目详情 未收详情
     */
    @Override
    public ProductReceiveMessageBean notReceive(ProductReceiveMessageBean productReceiveMessageBean,String userId) {
        DMap dMap = productReceiveDao.notReceive(productReceiveMessageBean, userId);
        ProductReceiveMessageBean productReceiveMessageBean1 = new ProductReceiveMessageBean();
        double interest = 0; //应收利息
        double principal = 0; //应收本金
        double receiveprice = 0; //回款金额
        if(dMap.getCount() > 0){
            for (int k = 0; k < dMap.getCount(); k++) {
                interest = interest + TypeTool.getDouble(dMap.getValue("INTEREST",k));
                principal = principal + TypeTool.getDouble(dMap.getValue("PRINCIPAL",k));
                receiveprice = interest + principal;
            }
        }
        DecimalFormat df = new DecimalFormat("#.00");
        productReceiveMessageBean1.setInterest(df.format(interest));
        productReceiveMessageBean1.setPrincipal(df.format(principal));
        productReceiveMessageBean1.setReceiveprice(df.format(receiveprice));
        return productReceiveMessageBean1;
    }

    /**
     * 项目详情 期数详情
     */
    @Override
    public List<ProductReceiveMessageBean> phaseDetails(ProductMessageBean productMessageBean,String userId) {
        List<ProductReceiveMessageBean> list = new ArrayList<ProductReceiveMessageBean>();
        DMap dMap = productReceiveDao.phaseDetails(productMessageBean, userId);
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                ProductReceiveMessageBean prvm = new ProductReceiveMessageBean();
                prvm.setId(dMap.getValue("ID", i));
                prvm.setBuyid(dMap.getValue("BUYID", i));
                prvm.setPrincipal(dMap.getValue("PRINCIPAL", i));
                prvm.setInterest(dMap.getValue("INTEREST", i));
                prvm.setYreceivetime(dMap.getValue("YRECEIVETIME", i));
                prvm.setReceiveprice(dMap.getValue("RECEIVEPRICE", i));
                String sreceivetime = dMap.getValue("SRECEIVETIME", i);
                if(sreceivetime.length() > 10){
                    sreceivetime = sreceivetime.substring(0,11);
                }
                prvm.setSreceivetime(sreceivetime);
                prvm.setCteatetime(dMap.getValue("CTEATETIME", i));
                prvm.setUpdatetime(dMap.getValue("UPDATETIME", i));
                prvm.setProductid(dMap.getValue("PRODUCTID", i));
                prvm.setBuyer(dMap.getValue("BUYER", i));
                prvm.setBalance(dMap.getValue("BALANCE", i));
                prvm.setTitle(dMap.getValue("TITLE", i));
                prvm.setCode(dMap.getValue("CODE", i));
                prvm.setOverduedays(dMap.getValue("OVERDUEDAYS", i));
                prvm.setReceiveflag(dMap.getValue("RECEIVEFLAG", i));
                //应收本息
                double receivepriceTotal = TypeTool.getDouble(dMap.getValue("PRINCIPAL", i)) +
                        TypeTool.getDouble(dMap.getValue("INTEREST", i));
                prvm.setReceivepriceTotal(receivepriceTotal+"");
                list.add(prvm);

            }

        }
        return list;
    }


    /**
     * 回款详情
     * @param productReceiveMes
     * @return
     */
    @Override
    public ProductReceiveMessageBean getProductReceiveDetail(ProductReceiveMessageBean productReceiveMes) {
        ProductReceiveMessageBean productReceiveMessageBean = new ProductReceiveMessageBean();
        Map map = productReceiveDao.getReceiveMessageBean(productReceiveMes);
        if(map != null){
            productReceiveMessageBean.setYsPrincipalTotal((String) map.get("ysPrincipal")); //已收本金
            productReceiveMessageBean.setYsInterestTotal((String) map.get("ysInterest")); //已收利息
            productReceiveMessageBean.setYsPriceTotal((String) map.get("ysPrice")); //已收总额
            productReceiveMessageBean.setDsPrincipalTotal((String) map.get("dsPrincipal")); //待收本金
            productReceiveMessageBean.setDsInterestTotal((String) map.get("dsInterest")); //待收利息
            productReceiveMessageBean.setDsPriceTotal((String) map.get("dsPrice")); //待收总额
            productReceiveMessageBean.setManagePrice((String) map.get("managePrice")); //管理费
            productReceiveMessageBean.setProgress((String) map.get("receiveProgress")); //进度
        }
        return productReceiveMessageBean;
    }


    /**
     * 根据条件查询收款情况
     * @param productReceiveMes
     * @return
     */
    @Override
    public List<ProductReceiveMessageBean> getProductReceiveListBySome(ProductReceiveMessageBean productReceiveMes) {
        List<ProductReceiveMessageBean> listProductReceiveMes = new ArrayList<ProductReceiveMessageBean>();
        DMap dMap = productReceiveDao.getProductReceiveListBySome(productReceiveMes); //回款情况详情
        if(dMap.getCount() > 0){
            for(int i=0;i<dMap.getCount();i++){
                ProductReceiveMessageBean productReceiveMessageBean = new ProductReceiveMessageBean();
                productReceiveMessageBean.setId(dMap.getValue("ID",i)); //主键
                productReceiveMessageBean.setBuyid(dMap.getValue("BUYID",i)); //购买id
                productReceiveMessageBean.setPrincipal(dMap.getValue("PRINCIPAL", i)); //应收本金
                productReceiveMessageBean.setInterest(dMap.getValue("INTEREST", i)); //应收利息
                productReceiveMessageBean.setReceivepriceTotal(dMap.getValue("SUMPRICE", i)); //应收本息
                //System.out.println(productReceiveMessageBean.getReceivepriceTotal());
                productReceiveMessageBean.setYreceivetime(dMap.getValue("YRECEIVETIME", i)); //应回款日期
                productReceiveMessageBean.setReceiveprice(dMap.getValue("RECEIVEPRICE", i)); //回款金额
                productReceiveMessageBean.setSreceivetime(dMap.getValue("SRECEIVETIME", i)); //实际回款日期
                productReceiveMessageBean.setOverduedays(dMap.getValue("OVERDUEDAYS", i)); //逾期天
                productReceiveMessageBean.setReceiveflag(dMap.getValue("RECEIVEFLAG", i)); //回款状态

                listProductReceiveMes.add(productReceiveMessageBean);
            }
        }
        return listProductReceiveMes;
    }

    /**
     * 投资总额
     */
    public String investTotal(String userId) {
        DMap dMap = productReceiveDao.investTotal(userId);
        if (dMap.getCount() > 0) {
            return dMap.getValue("INVESTTOTAL", 0);
        }
        return "0";
    }

    /**
     * 投标中本金
     */
    @Override
    public String inaBid(String userId) {
        DMap dMap = productReceiveDao.inaBid(userId);
        if (dMap.getCount() > 0) {
            return dMap.getValue("BIDING", 0);
        }
        return "0";
    }

    /**
     * 待回收本金
     */
    @Override
    public String toBeRecycled(String userId) {
        DMap dMap = productReceiveDao.toBeRecycled(userId);
        if (dMap.getCount() > 0) {
            return dMap.getValue("TOBERECYCLED", 0);
        }
        return "0";
    }

    /**
     *已回收本金
     */
    @Override
    public String retired(String userId) {
        DMap dMap = productReceiveDao.retired(userId);
        if (dMap.getCount() > 0) {
            return dMap.getValue("RETIRED", 0);
        }
        return "0";
    }

    /**
     * 待回收金额 本金 利息
     */
    @Override
    public String[] moneyRecycled(String userId) {
        DMap dMap = productReceiveDao.moneyRecycled(userId);
        String[] arr = new String[3];
        if (dMap.getCount() > 0) {
            arr[0] = dMap.getValue("PRINCIPALRECYCLED", 0);
            arr[1] = dMap.getValue("INTERESTRECYCLED", 0);
            arr[2] = dMap.getValue("MONEYRECYCLED", 0);
        }
        return arr;
    }


    /**
     * 平均受益率
     */
    @Override
    public String avgBenefit(String userId) {
        DMap dMap = productReceiveDao.avgBenefit(userId);
        if (dMap.getCount() > 0) {
            return dMap.getValue("AVGBENEFIT", 0).substring(0,5);
        }
        return "0";
    }

    /**
     * 获取昨日收益(App端)
     * @param appHomeMes
     * @return
     */
    @Override
    public AppHomeMessageBean getLastDayPrice(AppHomeMessageBean appHomeMes) {
        AppHomeMessageBean appHomeMessageBean = new AppHomeMessageBean();
        DMap dMap = productReceiveDao.getLastDayPrice(appHomeMes); //获取昨日收益
        if (dMap != null && dMap.getCount() > 0){
            appHomeMessageBean.setLastDayPrice(dMap.getValue("RECEIVEPRICESUM",0)); //昨日收益
        }
        return appHomeMessageBean;
    }


    /**
     * 获取待收(已收)金额、待收(已收)期数、待收(已收)投资数量(App端)
     * @param memberId
     * @param receiveFlag
     * @return
     */
    @Override
    public AppProductReceiveTjMessageBean getAppReceiveInfo(String memberId, String receiveFlag) {
        AppProductReceiveTjMessageBean appProductReceiveTjMessageBean = productReceiveDao.getAppReceiveInfo(memberId,receiveFlag);
        return appProductReceiveTjMessageBean;
    }


    /**
     * 获取待收(已收)列表(App端)
     * @param memberId
     * @param receiveFlag
     * @return
     */
    @Override
    public List<AppProductReceiveMessageBean> getAppReceiveList(String memberId, String receiveFlag,String pageIndex,String pageSize) {
        List<AppProductReceiveMessageBean> listAppProductReceiveMes = new ArrayList<AppProductReceiveMessageBean>();
        DMap dMap = productReceiveDao.getAppReceiveList(memberId,receiveFlag,pageIndex,pageSize);
        String sReceiveTime = ""; //实际还款时间
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                AppProductReceiveMessageBean appProductReceiveMessageBean = new AppProductReceiveMessageBean();
                appProductReceiveMessageBean.setId(dMap.getValue("ID",i)); //主键
                appProductReceiveMessageBean.setBuyId(dMap.getValue("BUYID",i)); //产品购买id
                appProductReceiveMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                appProductReceiveMessageBean.setYreceivePrice(dMap.getValue("YRECEIVEPRICE",i)); //应还款金额
                appProductReceiveMessageBean.setYreceiveTime(dMap.getValue("YRECEIVETIME",i)); //应还款日期
                appProductReceiveMessageBean.setReceivePrice(dMap.getValue("RECEIVEPRICE",i)); //实际还款金额

                sReceiveTime = dMap.getValue("SRECEIVETIME",i); //实际还款时间
                if (sReceiveTime != null && !sReceiveTime.equals("")){
                    sReceiveTime = sReceiveTime.substring(0,10);
                }
                appProductReceiveMessageBean.setSreceiveTime(sReceiveTime);
                listAppProductReceiveMes.add(appProductReceiveMessageBean);
            }
        }
        return listAppProductReceiveMes;
    }


    /**
     * 根据id获取回款信息
     * @param receiveId
     * @return
     */
    @Override
    public AppProductReceiveMessageBean getAppReceiveInfoById(String receiveId) {
        AppProductReceiveMessageBean appProductReceiveMessageBean = new AppProductReceiveMessageBean();
        DMap dMap = productReceiveDao.getAppReceiveInfoById(receiveId);
        String sReceiveTime = ""; //实际还款日期
        if (dMap != null){
            appProductReceiveMessageBean.setId(dMap.getValue("ID",0)); //主键
            appProductReceiveMessageBean.setBuyId(dMap.getValue("BUYID",0)); //产品购买id
            appProductReceiveMessageBean.setPrincipal(dMap.getValue("PRINCIPAL",0)); //应回款本金
            appProductReceiveMessageBean.setInterest(dMap.getValue("INTEREST",0)); //应回款利息
            appProductReceiveMessageBean.setYreceivePrice(dMap.getValue("YRECEIVEPRICE",0)); //应回款金额
            appProductReceiveMessageBean.setYreceiveTime(dMap.getValue("YRECEIVETIME",0)); //应回款日期
            appProductReceiveMessageBean.setReceivePrice(dMap.getValue("RECEIVEPRICE",0)); //实际回款金额

            sReceiveTime = dMap.getValue("SRECEIVETIME",0); //实际回款日期
            if (sReceiveTime != null && !sReceiveTime.equals("")){
                sReceiveTime = sReceiveTime.substring(0,10);
            }
            appProductReceiveMessageBean.setSreceiveTime(sReceiveTime);

            appProductReceiveMessageBean.setTitle(dMap.getValue("TITLE",0)); //题目
            appProductReceiveMessageBean.setCode(dMap.getValue("CODE",0)); //编码
        }
        return appProductReceiveMessageBean;
    }


    /**
     * 查询需自动还款信息
     * @param productAutoRepayMes
     * @return
     */
    @Override
    public List<ProductAutoRepayMessageBean> getReceiveInfoList(ProductAutoRepayMessageBean productAutoRepayMes) {
        DMap dMap = productReceiveDao.getReceiveInfoList(productAutoRepayMes); //查询自动还款信息
        List<ProductAutoRepayMessageBean> listProductAutoRepayMes = new ArrayList<ProductAutoRepayMessageBean>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ProductAutoRepayMessageBean productAutoRepayMessageBean = new ProductAutoRepayMessageBean();
                productAutoRepayMessageBean.setId(dMap.getValue("ID",i)); //主键
                productAutoRepayMessageBean.setBuyId(dMap.getValue("BUYID",i)); //产品购买主键
                productAutoRepayMessageBean.setPrincipal(dMap.getValue("PRINCIPAL", i)); //本金
                productAutoRepayMessageBean.setInterest(dMap.getValue("INTEREST",i)); //利息
                productAutoRepayMessageBean.setYreceiveTime(dMap.getValue("YRECEIVETIME",i)); //应回款日期
                productAutoRepayMessageBean.setReceivePrice(dMap.getValue("RECEIVEPRICE",i)); //实际回款金额
                productAutoRepayMessageBean.setSreceiveTime(dMap.getValue("SRECEIVETIME",i)); //实际回款日期
                productAutoRepayMessageBean.setBuyer(dMap.getValue("BUYER",i)); //投资人
                productAutoRepayMessageBean.setApplyMember(dMap.getValue("APPLYMEMBER",i)); //借款人
                productAutoRepayMessageBean.setProjectNo(dMap.getValue("PROJECTNO",i)); //项目ID号
                productAutoRepayMessageBean.setOutIpsAcctNo(dMap.getValue("OUTIPSACCTNO",i)); //转出方IPS账号
                productAutoRepayMessageBean.setInIpsAcctNo(dMap.getValue("INIPSACCTNO",i)); //转入方IPS账号
                productAutoRepayMessageBean.setOutAutoSignBillNo(dMap.getValue("OUTAUTOSIGNBILLNO",i)); //转出方自动签约订单号
                productAutoRepayMessageBean.setOutAutoSignAuthNo(dMap.getValue("OUTAUTOSIGNAUTHNO",i)); //转出方自动签约授权号
                listProductAutoRepayMes.add(productAutoRepayMessageBean);
            }
        }
        return listProductAutoRepayMes;
    }
}
