package com.mh.peer.service.sys.impl;

import com.mh.peer.dao.manager.SysFileDao;
import com.mh.peer.model.business.ProductBusinessBean;
import com.mh.peer.model.business.SysFileBusinessBean;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.AppSysFileMessageBean;
import com.mh.peer.model.message.SysFileMessageBean;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.HandleTool;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-10.
 */
@Service
public class SysFileServiceImpl implements SysFileService {

    @Autowired
    private SysFileDao sysFileDao;

    /**
     * 新增附件
     * @param sysFile
     * @return
     */
    @Override
    public SysFileBusinessBean saveSysFile(SysFile sysFile) {
        SysFileBusinessBean fileBus = new SysFileBusinessBean();
        DaoResult result = sysFileDao.saveProduct(sysFile); //新增附件
        if(result.getCode() < 0){
            fileBus.setResult("error");
            fileBus.setInfo(result.getErrText());
            return fileBus;
        }
        fileBus.setResult("success");
        fileBus.setInfo("保存成功");
        return fileBus;
    }

    /**
     * 删除附件
     * @param sysFile
     * @return
     */
    @Override
    public SysFileBusinessBean deleteSysFile(SysFile sysFile) {
        SysFileBusinessBean sysFileBusinessBean = new SysFileBusinessBean();
        DaoResult result = sysFileDao.delProduct(sysFile);

        if(result.getCode() < 0){
            sysFileBusinessBean.setResult("error");
            sysFileBusinessBean.setInfo(result.getErrText());
        }else{
            sysFileBusinessBean.setResult("success");
            sysFileBusinessBean.setInfo("删除成功");
        }
        return sysFileBusinessBean;
    }

    /**
     * 附件集合
     * @param glId
     * @return
     */
    @Override
    public List<SysFileMessageBean> getAllFile(String glId) {
        List<SysFileMessageBean> sysFileList = new ArrayList<SysFileMessageBean>(); //附件集合
        DMap parm = sysFileDao.queryAllSysFile(glId);
        if(parm.getCount() > 0){
            for(int i = 0; i < parm.getCount(); i++){
                SysFileMessageBean sysFileMessageBean = new SysFileMessageBean();
                sysFileMessageBean.setId(parm.getValue("ID", i)); //主键
                sysFileMessageBean.setGlid(parm.getValue("GLID", i)); //关联id
                sysFileMessageBean.setImgurl(parm.getValue("IMGURL", i)); //图片地址(相对地址)
                sysFileMessageBean.setGlid(parm.getValue("IMGURLJD", i)); //图片地址(绝对地址)
                sysFileMessageBean.setType(parm.getValue("TYPE", i)); //类型
                sysFileMessageBean.setCreateuser(parm.getValue("CREATEUSER", i)); //创建用户
                sysFileMessageBean.setCreatetime(parm.getValue("CREATETIME", i)); //创建时间
                sysFileList.add(sysFileMessageBean);
            }
        }
        return sysFileList;
    }

    /**
     * 根据主键查询附件信息
     * @param sysFileMes
     * @return
     */
    @Override
    public SysFileMessageBean getSysFileById(SysFileMessageBean sysFileMes) {
        SysFileMessageBean sysFileMessageBean = new SysFileMessageBean(); //附件部分
        HandleTool handleTool = new HandleTool();
        DMap dMap = sysFileDao.getOnlySysFile(sysFileMes);
        if(dMap.getCount() > 0){
            sysFileMessageBean.setId(handleTool.getParamById(dMap.getValue("ID"))); //主键
            sysFileMessageBean.setGlid(handleTool.getParamById(dMap.getValue("GLID"))); //关联id
            sysFileMessageBean.setImgurl(handleTool.getParamById(dMap.getValue("IMGURL"))); //图片地址(相对)
            sysFileMessageBean.setImgurljd(handleTool.getParamById(dMap.getValue("IMGURLJD"))); //图片地址(绝对)
            sysFileMessageBean.setType(handleTool.getParamById(dMap.getValue("TYPE"))); //类型
            sysFileMessageBean.setSort(handleTool.getParamById(dMap.getValue("SORT"))); //排序
            sysFileMessageBean.setCreateuser(handleTool.getParamById(dMap.getValue("CREATEUSER"))); //创建用户
            sysFileMessageBean.setCreatetime(handleTool.getParamById(dMap.getValue("CREATETIME"))); //创建时间
        }
        return sysFileMessageBean;
    }

    /**
     * 根据关联id获取附件地址(App部分)
     * @param glId
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public AppSysFileMessageBean getAppSysFile(String glId, String orderColumn, String orderType, String pageIndex, String pageSize) {
        DMap dMap = sysFileDao.getAppSysFile(glId,orderColumn,orderType,pageIndex,pageSize);
        AppSysFileMessageBean appSysFileMessageBean = new AppSysFileMessageBean();
        if (dMap != null){
            appSysFileMessageBean.setId(dMap.getValue("ID",0)); //主键
            appSysFileMessageBean.setGlId(dMap.getValue("GLID",0)); //关联id
            appSysFileMessageBean.setImgUrl(dMap.getValue("IMGURL",0)); //图片地址
            appSysFileMessageBean.setCreateTime(dMap.getValue("CREATETIME",0)); //创建时间
        }
        return appSysFileMessageBean;
    }


}
