package com.mh.peer.service.front;

import com.mh.peer.model.message.ProductApplyDetailsMessageBean;
import com.mh.peer.model.message.ProductApplyMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016-9-7.
 * 申请项目
 */
public interface ApplyProductService {

    /**
     * 查询申请项目吗
     * @param productApplyMes
     * @return
     */
    List<ProductApplyMessageBean> getApplyProductList(ProductApplyMessageBean productApplyMes);

    /**
     * 已申请项目数量
     * @param productApplyMes
     * @return
     */
    int getApplyProductListCount(ProductApplyMessageBean productApplyMes);

    /**
     * 获取已还金额
     * @param productId
     * @param applyMemberId
     * @return
     */
    ProductApplyDetailsMessageBean getyRepay(String productId,String applyMemberId);

    /**
     * 获取待还金额
     * @param productId
     * @param applyMemberId
     * @return
     */
    ProductApplyDetailsMessageBean getdRepay(String productId,String applyMemberId);

    /**
     * 还款列表集合
     * @param productId
     * @return
     */
    List<ProductApplyDetailsMessageBean> getRepayList(String productId,String orderColumn,String orderType,String pageIndex,String pageSize);

    /**
     * 还款列表数量
     * @param productId
     * @return
     */
    int getRepayListCount(String productId);
}
