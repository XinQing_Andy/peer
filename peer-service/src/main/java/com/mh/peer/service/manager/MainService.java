package com.mh.peer.service.manager;

import com.mh.peer.model.message.MenuBeanMessage;
import java.util.List;

/**
 * Created by WangMeng on 2016/1/13.
 */
public interface MainService {
    /**
     * 得到菜单
     * @param menuBeanMessage
     * @return
     */
    List<MenuBeanMessage> getMenu(MenuBeanMessage menuBeanMessage);

    /**
     * 得到子菜单
     * @param menuBeanMessage
     * @return
     */
    List<MenuBeanMessage> getLevelMenu(MenuBeanMessage menuBeanMessage);
}
