package com.mh.peer.service.product;

/**
 * Created by zhangerxin on 2016-7-31.
 */
public interface ProductSerialNumberService {

    /**
     * 获取下一个项目流水号
     * @return
     */
    String getNextNumber();
}
