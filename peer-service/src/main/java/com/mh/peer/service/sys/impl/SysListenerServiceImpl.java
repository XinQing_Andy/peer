package com.mh.peer.service.sys.impl;

import com.mh.peer.dao.manager.SysListenerDao;
import com.mh.peer.model.message.SysListenerMessageBean;
import com.mh.peer.util.HandleTool;
import com.salon.frame.data.DMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zhangerxin on 2016-5-19.
 */
@Service
public class SysListenerServiceImpl {

    @Autowired
    private SysListenerDao sysListenerDao;

    /**
     * 获取监听信息
     * @return
     */
    public SysListenerMessageBean getListenerInfo(){
        SysListenerMessageBean sysListenerMes = new SysListenerMessageBean(); //监听信息
        DMap dMap = sysListenerDao.getListenerInfo();
        HandleTool handleTool = new HandleTool();
        if(dMap.getCount() > 0){
            sysListenerMes.setListenerTime(handleTool.getParamById(dMap.getValue("LISTENERTIME"))); //监听时间
        }

        return sysListenerMes;
    }
}
