package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.MyMessageDao;
import com.mh.peer.dao.front.PersonageSettingDao;
import com.mh.peer.model.message.MemberMessageMesBean;
import com.mh.peer.service.front.MyMessageService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cuibin on 2016/7/21.
 */
@Service
public class MyMessageServiceImpl implements MyMessageService {
    @Autowired
    private MyMessageDao myMessageDao;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 分页查询我的消息
     */
    @Override
    public List<MemberMessageMesBean> queryMyMessageByPage(MemberMessageMesBean memberMessageMesBean) {
        String pageIndex = memberMessageMesBean.getPageIndex(); //起始数(字符型)
        int pageIndexIn = 0; //起始数(整型)
        int num = 0;
        if (pageIndex != null && !pageIndex.equals("")){
            pageIndexIn = TypeTool.getInt(pageIndex);
            num = pageIndexIn;
        }
        DMap dMap = myMessageDao.queryMyMessageByPage(memberMessageMesBean);
        List<MemberMessageMesBean> list = new ArrayList<MemberMessageMesBean>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                MemberMessageMesBean temp = new MemberMessageMesBean();
                temp.setId(dMap.getValue("ID", i));
                temp.setGlid(dMap.getValue("GLID", i));//关联id
                temp.setType(dMap.getValue("TYPE", i));//类型(0-系统消息)
                temp.setMemberid(dMap.getValue("MEMBERID", i));//类型(0-系统消息)
                temp.setReadflag(dMap.getValue("READFLAG", i));//读取状态(0-已读、1-未读)
                temp.setTitle(dMap.getValue("TITLE", i));//题目
                temp.setContent(dMap.getValue("CONTENT", i));//内容
                temp.setSendtime(dMap.getValue("SENDTIME", i));//发送时间
                temp.setCreatetime(dMap.getValue("CREATETIME", i));//发送时间
                temp.setNum(TypeTool.getString(num)); //编号
                num++;
                list.add(temp);
            }
        }

        return list;
    }


    /**
     * 查询充值记录总数
     */
    public int queryMyMessageCount(MemberMessageMesBean memberMessageMesBean) {
        DMap dMap = myMessageDao.queryMyMessageCount(memberMessageMesBean);
        int count = dMap.getCount();
        return count;
    }

    /**
     * 获取详细信息
     * @param memberMessageMes
     * @return
     */
    @Override
    public MemberMessageMesBean getMyMessageById(MemberMessageMesBean memberMessageMes) {
        MemberMessageMesBean memberMessageMesBean = new MemberMessageMesBean();
        DaoResult daoResult = myMessageDao.upReadFlg(memberMessageMes);
        if (daoResult.getCode() >= 0){
            DMap dMap = myMessageDao.getMyMessageById(memberMessageMes);
            if (dMap != null){
                memberMessageMesBean.setId(dMap.getValue("ID",0)); //主键
                memberMessageMesBean.setTitle(dMap.getValue("TITLE",0)); //标题
                memberMessageMesBean.setContent(dMap.getValue("CONTENT",0)); //内容
                memberMessageMesBean.setSendtime(dMap.getValue("SENDTIME",0)); //发送时间
            }
        }
        return memberMessageMesBean;
    }

    /**
     * 查询下一条详细信息
     * @param memberMessageMes
     * @return
     */
    @Override
    public MemberMessageMesBean getNextMyMessageById(MemberMessageMesBean memberMessageMes) {
        MemberMessageMesBean memberMessageMesBean = new MemberMessageMesBean();
        String id = ""; //主键
        DMap dMap = myMessageDao.queryMyMessageByPage(memberMessageMes);
        if (dMap != null){
            id = dMap.getValue("ID",0); //主键
            memberMessageMesBean.setId(id);
            memberMessageMesBean.setTitle(dMap.getValue("TITLE",0)); //标题
            memberMessageMesBean.setContent(dMap.getValue("CONTENT",0)); //内容
            memberMessageMesBean.setSendtime(dMap.getValue("SENDTIME",0)); //发送时间
        }
        DaoResult daoResult = myMessageDao.upReadFlg(memberMessageMesBean); //更新读取状态
        return memberMessageMesBean;
    }

    /**
     * 删除我的消息
     * @param memberMessageMes
     * @return
     */
    @Override
    public MemberMessageMesBean delMyMessageById(MemberMessageMesBean memberMessageMes) {
        MemberMessageMesBean memberMessageMesBean = new MemberMessageMesBean();
        DaoResult daoResult = myMessageDao.delMyMessageById(memberMessageMes);
        if (daoResult.getCode() < 0){
            memberMessageMesBean.setCode("-1");
            memberMessageMesBean.setInfo("删除失败！");
            return memberMessageMesBean;
        }
        memberMessageMesBean.setCode("0");
        memberMessageMesBean.setInfo("删除成功！");
        return memberMessageMesBean;
    }


    /**
     * 批量删除我的消息
     * @param memberMessageMes
     * @return
     */
    @Override
    public MemberMessageMesBean updateBatchReadFlg(MemberMessageMesBean memberMessageMes) {
        DaoResult daoResult = myMessageDao.upReadFlg(memberMessageMes); //更新读取状态
        if (daoResult.getCode() < 0){
            memberMessageMes.setCode("-1"); //失败
            memberMessageMes.setInfo("设置失败");
        }
        return memberMessageMes;
    }


}
