package com.mh.peer.service.product;

import com.mh.peer.model.business.ProductAttornBusiness;
import com.mh.peer.model.business.ProductMyAttornBusinessBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhangerxin on 2016-5-23.
 * 债权转让
 */
public interface ProductAttornService {

    /**
     * 获取可转让的债权集合
     * @param productInvestMes
     * @return
     */
    public List<ProductInvestMessageBean> getAttornList(ProductInvestMessageBean productInvestMes);

    /**
     * 获取可转让债权数量
     * @param productInvestMes
     * @return
     */
    public int getAttornListCount(ProductInvestMessageBean productInvestMes);

    /**
     * 获取更多转让债权
     * @param productAttornMoreMes
     * @return
     */
    List<ProductAttornMoreMessageBean> getAttornMoreList(ProductAttornMoreMessageBean productAttornMoreMes);

    /**
     * 获取债权转让数量
     * @param productAttornMoreMes
     * @return
     */
    int getAttornMoreCount(ProductAttornMoreMessageBean productAttornMoreMes);

    /**
     * 根据条件获取相应的债权
     * @param productAttornRecordMes
     * @return
     */
    List<ProductAttornRecordMessageBean> queryMyAttornByPage(ProductAttornRecordMessageBean productAttornRecordMes);

    /**
     * 债权数量
     * @param productAttornRecordMes
     * @return
     */
    int queryMyAttornCount(ProductAttornRecordMessageBean productAttornRecordMes);

    public List<ProductAttornRecordMessageBean> queryTransferByPage(ProductAttornRecordMessageBean productAttornRecordMessageBean);

    /**
     * 转让申请数量 by cuibin
     */
    public String getCountByType(ProductAttornRecordMessageBean productAttornRecordMessageBean);

    /**
     * 获取债权转让数量(前台)
     * @param poductAttornRecordMes
     * @return
     */
    public int getFrontAttornCount(ProductAttornRecordMessageBean poductAttornRecordMes);

    /**
     * 获取所有未被转让的债权
     * @param poductAttornRecordMes
     * @return
     */
    public List<ProductAttornRecordMessageBean> getFrontAttornList(String orderColumn, String orderType,ProductAttornRecordMessageBean poductAttornRecordMes);

    /**
     * 获取可转让的债权详情
     * @param productAttornRecordMes
     * @return
     */
    public ProductAttornRecordMessageBean getAttornDetail(ProductAttornRecordMessageBean productAttornRecordMes);

    /**
     * 获取债权详情
     * @param attornProductMessageBean
     * @return
     */
    public AttornProductMessageBean getAttornProductDetail(AttornProductMessageBean attornProductMessageBean);

    /**
     * 新增债权转让数据更新
     * @param attornProductMes,memberInfo
     * @return
     */
    public AttornProductMessageBean saveProductAttorn(AttornProductMessageBean attornProductMes,MemberInfo memberInfo);

    /**
     * 添加转让记录（发起转让）
     * @param productMyAttornMes
     * @returns
     */
    public ProductMyAttornBusinessBean confirmAttornProduct(ProductMyAttornMessageBean productMyAttornMes);

    /**
     * 获取债权转让标(后台)By zhangerxin
     * @param productAttornMes
     * @return
     */
    public List<ProductAttornMessage> queryProductAttornByPage(ProductAttornMessage productAttornMes);

    /**
     * 获取债权转让标数量
     * @param productAttornMes
     * @return
     */
    public int queryProductAttornCount(ProductAttornMessage productAttornMes);

    /**
     * 设置债权审核状态(后台) zhangerxin
     * @param productAttornMes
     * @return
     */
    public ProductAttornBusiness setProductAttornShFlag(ProductAttornMessage productAttornMes);

    /**
     * 获取债权转让集合(App端)
     * @param memberId
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public List<AppProductAttornMessageBean> getAppAttornList(String memberId,String typeId,String pageIndex,String pageSize,String orderColumn,String orderType);

    /**
     * 获取债权转让标详情(App端)
     * @param attornId
     * @return
     */
    public AppProductAttornDetailMessageBean getAppAttornDetail(String attornId);

    /**
     * 获取已投资(购买)的债权
     * @param timeStyle
     * @param sTime
     * @param eTime
     * @param keyWord
     * @param value
     * @return
     */
    List<AttornInvestedMessageBean> queryInvestedAttornByPage(String memberId,String timeStyle,String sTime,String eTime,String keyWord,String value,String orderColumn,String orderType,String pageIndex,String pageSize);

    /**
     * 已购买的债权数量
     * @param memberId
     * @param timeStyle
     * @param sTime
     * @param eTime
     * @param keyWord
     * @param value
     * @return
     */
    int queryInvestedAttornCount(String memberId,String timeStyle,String sTime,String eTime,String keyWord,String value);

    /**
     * 获取待承接债权集合
     * @param productAttornMes
     * @return
     */
    List<ProductAttornMessage> queryProductAttornUnderTaked(ProductAttornMessage productAttornMes);

    /**
     * 获取待承接债权数量
     * @param productAttornMes
     * @return
     */
    int queryProductAttornUnderTakedCount(ProductAttornMessage productAttornMes);

    /**
     * 获取失败的债权
     * @param productAttornMes
     * @return
     */
    List<ProductAttornMessage> queryProductAttornFail(ProductAttornMessage productAttornMes);

    /**
     * 获取失败的债权数量
     * @param productAttornMes
     * @return
     */
    int queryProductAttornFailCount(ProductAttornMessage productAttornMes);
}
