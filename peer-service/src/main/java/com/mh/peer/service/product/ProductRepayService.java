package com.mh.peer.service.product;

import com.mh.peer.model.message.*;
import java.util.List;

/**
 * Created by zhangerxin on 2016-5-27.
 */
public interface ProductRepayService {

    /**
     * 根据分页获取还款列表
     * @param productRepayMessageBean
     * @return
     */
    List<ProductRepayMessageBean> getRepayListByPage(ProductRepayMessageBean productRepayMessageBean);

    /**
     * 获取还款列表数量
     * @param productRepayMessageBean
     * @return
     */
    int getRepayCount(ProductRepayMessageBean productRepayMessageBean);

    /**
     * 获取还款数量
     * @param productRepayMessageBean
     * @return
     */
    ProductRepayMessageBean getRepayPriceSum(ProductRepayMessageBean productRepayMessageBean);

    /**
     * 待还总额 & 到期还款金额 by cuibin
     */
    public String getRepayedPrice(ProductRepayMessageBean productRepayMessageBean,String repayFlg);

    /**
     * 获取待还(已还)金额、待还(已还)期数(App端)
     * @param memberId
     * @param repayFlag
     * @return
     */
    public AppProductRepayTjMessageBean getAppRepayInfo(String memberId,String repayFlag);

    /**
     * 获取待还(已还)列表(App端)
     * @param memberId
     * @param repayFlag
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public List<AppProductRepayMessageBean> getAppRepayList(String memberId,String repayFlag,String pageIndex,String pageSize);

    /**
     * 获取产品还款详细信息(App端)
     * @param productRepayId
     * @return
     */
    public AppProductRepayMessageBean getAppRepayDetails(String productRepayId);

    /**ProductOverRepayMessageBean overRepayMes
     * 获取逾期信息集合
     * @param overRepayMes
     * @return
     */
    public List<ProductOverRepayMessageBean> getOverDueRepayList(ProductOverRepayMessageBean overRepayMes);

    /**
     * 获取逾期列表数量(By buyatao)
     * @param overRepayMes
     * @return
     */
    public int getOverRepayByPage(ProductOverRepayMessageBean overRepayMes);

    /**
     * 获取还款明细信息(By buyatao)
     * @param productRepayMes
     * @return
     */
    List<ProductRepayMessageBean> getRepayList(ProductRepayMessageBean productRepayMes);

    /**
     * 获取环迅部分应还款信息
     * @param repayId
     * @return
     */
    HuanXunFreezeMessageBean getRepayInfoById(String repayId);

    /**ProductOverRepayMessageBean overRepayMes
     * 还款中产品信息集合
     * @param repayingMes
     * @return
     */
    public List<ProductRepayMessageBean> getRepayingList(ProductRepayMessageBean repayingMes);

    /**
     * 还款中产品信息集合数量(By buyatao)
     * @param repayingMes
     * @return
     */
    public int getRepayingByPage(ProductRepayMessageBean repayingMes);

    /**
     * 已结清产品信息集合
     * @param finishMes
     * @return
     */
    public List<ProductRepayMessageBean> getFinishList(ProductRepayMessageBean finishMes);

    /**
     * 已结清产品信息集合数量(By buyatao)
     * @param finishMes
     * @return
     */
    public int getFinishByPage(ProductRepayMessageBean finishMes);
}
