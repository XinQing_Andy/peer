package com.mh.peer.service.huanxun;

import com.mh.peer.model.entity.HuanxunFreezeRepay;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunRepayMessageBean;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-8-7.
 * 还款部分
 */
public interface HuanXunRepayService {


    /**
     * 获取还款信息
     * @param huanXunRepayMessageBean
     * @return
     */
    List<HuanXunRepayMessageBean> queryMemberInfo(HuanXunRepayMessageBean huanXunRepayMessageBean);

    /**
     * 环迅还款金额冻结部分
     * @param huanxunFreezeRepay
     * @param memberMessage
     * @param memberInfo
     * @return
     */
    String repayhxResult(HuanxunFreezeRepay huanxunFreezeRepay,MemberMessage memberMessage,MemberInfo memberInfo,Map<String,Object> mapException);

    /**
     * 根据还款id获取数量
     * @param repayId
     * @return
     */
    int queryFreezeRepayCount(String repayId);
}
