package com.mh.peer.service.sys;

import com.mh.peer.model.message.MessageSetMessageBean;

/**
 * Created by cuibin on 2016/5/30.
 */
public interface MessageSetService {

    /**
     * 查询一个短信设置
     */
    MessageSetMessageBean queryOnlyMessage();

    /**
     * 更新短信设置
     */
    MessageSetMessageBean updateMessageSet(MessageSetMessageBean messageSetMessageBean);
    /**
     * 读取开关
     */
    String loadSwitch();
}
