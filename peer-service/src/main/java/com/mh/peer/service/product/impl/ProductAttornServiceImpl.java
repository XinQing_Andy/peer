package com.mh.peer.service.product.impl;

import com.mh.peer.dao.manager.SysFileDao;
import com.mh.peer.dao.product.ProductAttornDao;
import com.mh.peer.model.business.ProductAttornBusiness;
import com.mh.peer.model.business.ProductMyAttornBusinessBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.ProductAttornRecord;
import com.mh.peer.model.entity.ProductBuyInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.product.ProductAttornService;
import com.mh.peer.util.HandleTool;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-5-23.
 * 债权转让
 */
@Service
public class ProductAttornServiceImpl implements ProductAttornService{

    @Autowired
    private ProductAttornDao productAttornDao;

    @Autowired
    private SysFileDao sysFileDao;

    /**
     * 获取所有可转让的债权
     * @param productInvestMes
     * @return
     */
    @Override
    public List<ProductInvestMessageBean> getAttornList(ProductInvestMessageBean productInvestMes) {
        DMap dMap = productAttornDao.getAttornList(productInvestMes); //可转让的债权
        List<ProductInvestMessageBean> listProductInvestMes = new ArrayList<ProductInvestMessageBean>();
        String endTime = ""; //到期时间

        if(dMap.getCount() > 0){
            for(int i=0;i<dMap.getCount();i++){
                ProductInvestMessageBean productInvestMessageBean = new ProductInvestMessageBean();
                productInvestMessageBean.setId(dMap.getValue("ID",i)); //主键
                productInvestMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                productInvestMessageBean.setCode(dMap.getValue("CODE",i)); //编码
                productInvestMessageBean.setLoanrate(dMap.getValue("LOANRATE",i)); //贷款利率
                productInvestMessageBean.setRepayment(dMap.getValue("REPAYMENT",i)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
                productInvestMessageBean.setWaitPrice(dMap.getValue("WAITINGPRICE",i)); //待收本息

                endTime = dMap.getValue("ENDTIME",i); //到期时间
                if (endTime != null && !endTime.equals("")){
                    endTime = endTime.substring(0,10);
                    productInvestMessageBean.setEndTime(endTime);
                }

                productInvestMessageBean.setReceiveFlag(dMap.getValue("RECEIVEFLAG",i)); //回款状态
                productInvestMessageBean.setType(dMap.getValue("TYPE", i)); //类型(0-持有 1-转让中 2-已转让)
                productInvestMessageBean.setProductId(dMap.getValue("PRODUCTID", i));//产品id
                listProductInvestMes.add(productInvestMessageBean);
            }
        }
        return listProductInvestMes;
    }

    /**
     * 获取可转让债权数量
     * @param productInvestMes
     * @return
     */
    @Override
    public int getAttornListCount(ProductInvestMessageBean productInvestMes) {
        DMap dMap = productAttornDao.getAttornCount(productInvestMes); //可转让的债权
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 获取更多转让债权
     * @param productAttornMoreMes
     * @return
     */
    @Override
    public List<ProductAttornMoreMessageBean> getAttornMoreList(ProductAttornMoreMessageBean productAttornMoreMes) {
        List<ProductAttornMoreMessageBean> listProductAttornMoreMes = new ArrayList<ProductAttornMoreMessageBean>();
        DMap dMap = productAttornDao.getAttornMoreList(productAttornMoreMes);
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ProductAttornMoreMessageBean productAttornMoreMessageBean = new ProductAttornMoreMessageBean();
                productAttornMoreMessageBean.setId(dMap.getValue("ID",i)); //主键
                productAttornMoreMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                listProductAttornMoreMes.add(productAttornMoreMessageBean);
            }
        }
        return listProductAttornMoreMes;
    }


    /**
     * 获取债权转让数量
     * @param productAttornMoreMes
     * @return
     */
    @Override
    public int getAttornMoreCount(ProductAttornMoreMessageBean productAttornMoreMes) {
        List<ProductAttornMoreMessageBean> listProductAttornMoreMes = new ArrayList<ProductAttornMoreMessageBean>();
        DMap dMap = productAttornDao.getAttornMoreCount(productAttornMoreMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 根据条件获取相应的债权(重做 byzhangerxin)
     * @param productAttornRecordMes
     * @return
     */
    @Override
    public List<ProductAttornRecordMessageBean> queryMyAttornByPage(ProductAttornRecordMessageBean productAttornRecordMes) {
        DMap dMap = productAttornDao.queryMyAttornByPage(productAttornRecordMes);
        List<ProductAttornRecordMessageBean> listProductAttornRecordMes = new ArrayList<ProductAttornRecordMessageBean>();
        String attornTime = ""; //转让时间
        String underTakeTime = ""; //承接时间(购买时间)

        if(dMap != null && dMap.getCount() > 0){
            for(int i=0;i<dMap.getCount();i++){
                ProductAttornRecordMessageBean productAttornRecordMessageBean = new ProductAttornRecordMessageBean();
                productAttornRecordMessageBean.setId(dMap.getValue("ID",i)); //主键
                productAttornRecordMessageBean.setProductbuyId(dMap.getValue("PRODUCTBUYID",i)); //产品购买id
                productAttornRecordMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                productAttornRecordMessageBean.setCode(dMap.getValue("CODE",i)); //编码
                productAttornRecordMessageBean.setLoanrate(dMap.getValue("LOANRATE",i)); //年利率
                productAttornRecordMessageBean.setWaitPrice(dMap.getValue("WAITPRICE", i)); //待收本息
                productAttornRecordMessageBean.setProductid(dMap.getValue("PRODUCTID", i)); //产品id
                productAttornRecordMessageBean.setPrice(dMap.getValue("PRICE",i)); //转让价格

                attornTime = dMap.getValue("CREATETIME",i); //转让时间
                if (attornTime != null && !attornTime.equals("")){
                    attornTime = attornTime.substring(0,10);
                }
                productAttornRecordMessageBean.setCreateTime(attornTime);

                productAttornRecordMessageBean.setRepayment(dMap.getValue("REPAYMENT",i)); //还款方式(0-按月还款、等额本息,1-按月付息、到期还本,2-一次性还款)

                underTakeTime = dMap.getValue("UNDERTAKETIME",i); //承接时间
                if (underTakeTime != null && !underTakeTime.equals("")){
                    underTakeTime = underTakeTime.substring(0,16);
                }
                productAttornRecordMessageBean.setUndertakeTime(underTakeTime);
                listProductAttornRecordMes.add(productAttornRecordMessageBean);
            }
        }
        return listProductAttornRecordMes;
    }


    /**
     * 根据条件获取相应的债权数量
     * @param productAttornRecordMes
     * @return
     */
    @Override
    public int queryMyAttornCount(ProductAttornRecordMessageBean productAttornRecordMes) {
        DMap dMap = productAttornDao.queryMyAttornCount(productAttornRecordMes);
        int totalCount = dMap.getCount(); //数量
        return totalCount;
    }


    /**
     * 根据条件获取转让中债权
     * @param productAttornRecordMes
     * @return
     */
    @Override
    public List<ProductAttornRecordMessageBean> queryTransferByPage(ProductAttornRecordMessageBean productAttornRecordMes) {
        DMap dMap = productAttornDao.queryMyAttornByPage(productAttornRecordMes); //获取转让中的债权
        List<ProductAttornRecordMessageBean> list = new ArrayList<ProductAttornRecordMessageBean>();

        if(dMap.getCount() > 0){
            for(int i=0;i<dMap.getCount();i++){
                ProductAttornRecordMessageBean temp = new ProductAttornRecordMessageBean();
                temp.setId(dMap.getValue("ID",i)); //主键
                temp.setTitle(dMap.getValue("TITLE",i)); //题目
                temp.setCode(dMap.getValue("CODE",i)); //编码
                temp.setLoanrate(dMap.getValue("LOANRATE",i)); //贷款利率
                temp.setWaitprice(dMap.getValue("WAITPRICE", i)); //待收本息
                temp.setPrice(dMap.getValue("PRICE", i)); //转让价格
                temp.setUndertakeTime(dMap.getValue("UNDERTAKETIME", i)); //承接时间
                temp.setShFlag(dMap.getValue("SHFLAG",i)); //审核状态
                temp.setCreateTime(dMap.getValue("ATTORNTIME",i)); //发起债权时间
                temp.setProductid(dMap.getValue("PRODUCTID",i)); //产品id
                list.add(temp);
            }
        }
        return list;
    }

    /**
     * 转让申请数量 by cuibin
     */
    @Override
    public String getCountByType(ProductAttornRecordMessageBean productAttornRecordMessageBean) {
        String sum = productAttornDao.getCountByType(productAttornRecordMessageBean).getValue("zs",0);
        return sum.equals("") ? "0" : sum;
    }


    /**
     * 获取债权转让数量(前台)
     * @param poductAttornRecordMes
     * @return
     */
    @Override
    public int getFrontAttornCount(ProductAttornRecordMessageBean poductAttornRecordMes) {
        DMap dMap = productAttornDao.getFrontAttornCount(poductAttornRecordMes);
        int count = dMap.getCount();
        return count;
    }


    /**
     * 获取所有未被转让的债权
     * @param poductAttornRecordMes
     * @return
     */
    @Override
    public List<ProductAttornRecordMessageBean> getFrontAttornList(String orderColumn, String orderType,ProductAttornRecordMessageBean poductAttornRecordMes) {
        DMap dMap = productAttornDao.getFrontAttornList(orderColumn,orderType,poductAttornRecordMes);
        List<ProductAttornRecordMessageBean> listProductAttornRecordMes = new ArrayList<ProductAttornRecordMessageBean>();
        if(dMap.getCount() > 0){
            for(int i=0;i<dMap.getCount();i++){
                ProductAttornRecordMessageBean productAttornRecordMessageBean = new ProductAttornRecordMessageBean();
                productAttornRecordMessageBean.setId(dMap.getValue("ID", i)); //主键
                productAttornRecordMessageBean.setTitle(dMap.getValue("TITLE", i)); //借款标题
                productAttornRecordMessageBean.setPrice(dMap.getValue("PRICE", i)); //转让价格
                productAttornRecordMessageBean.setSurplusdays(dMap.getValue("SURPLUSDAYS", i)); //剩余期限
                productAttornRecordMessageBean.setSurplusperiods(dMap.getValue("SURPLUSPERIODS", i)); //剩余期数
                productAttornRecordMessageBean.setWaitprice(dMap.getValue("WAITPRICE", i)); //待收本息
                productAttornRecordMessageBean.setExpectedrate(dMap.getValue("EXPECTEDRATE", i)); //预期收益率
                productAttornRecordMessageBean.setShFlag(dMap.getValue("SHFLAG", i)); //审核状态(0-审核通过、1-审核不通过、2-待审核)
                productAttornRecordMessageBean.setUndertakeTime(dMap.getValue("UNDERTAKETIME", i)); //承接时间
                productAttornRecordMessageBean.setEndtime(dMap.getValue("ENDTIME", i)); //到期时间
                productAttornRecordMessageBean.setShyj(dMap.getValue("SHYJ", i)); //审核意见
                productAttornRecordMessageBean.setUndertakeMember(dMap.getValue("UNDERTAKEMEMBER", i)); //承接人
                productAttornRecordMessageBean.setProductid(dMap.getValue("PRODUCTID", i)); //产品id
                listProductAttornRecordMes.add(productAttornRecordMessageBean);
            }
        }
        return listProductAttornRecordMes;
    }


    /**
     * 获取可转让的债权详情
     * @param productAttornRecordMes
     * @return
     */
    @Override
    public ProductAttornRecordMessageBean getAttornDetail(ProductAttornRecordMessageBean productAttornRecordMes) {
        DMap dMap = productAttornDao.getAttornDetail(productAttornRecordMes);
        MemberInfoMessageBean memberInfoMes = new MemberInfoMessageBean();
        HandleTool handleTool = new HandleTool();
        String xdUrl = ""; //相对路径
        String xdUrlPj = ""; //相对路径拼接
        String loadLength = ""; //贷款期限

        if(dMap != null){
            /******************** 产品部分Start ********************/
            DMap dMapFile = sysFileDao.queryAllSysFile(productAttornRecordMes.getProductid()); //附件部分 productId
            if(dMapFile.getCount() > 0){
                for(int i=0;i<dMapFile.getCount();i++){
                    xdUrl = dMapFile.getValue("IMGURL",i); //相对路径
                    if(xdUrlPj == null || xdUrlPj.equals("")){
                        xdUrlPj = xdUrl;
                    }else{
                        xdUrlPj = xdUrlPj + "," + xdUrl;
                    }
                }
                productAttornRecordMes.setImgUrl(xdUrlPj); //路径拼接(相对路径,绝对路径)
            }
            memberInfoMes.setRealname(dMap.getValue("REALNAME",0)); //真实姓名
            memberInfoMes.setTelephone(dMap.getValue("TELEPHONE",0)); //手机号
            memberInfoMes.setEmail(dMap.getValue("EMAIL",0)); //邮箱地址
            memberInfoMes.setSex(dMap.getValue("SEX",0)); //性别(0-男、1-女)
            memberInfoMes.setDetailAddress(dMap.getValue("DETAIL_ADDRESS",0)); //详细地址
            memberInfoMes.setDregree(dMap.getValue("DREGREE",0)); //学历
            memberInfoMes.setMarryStatus(dMap.getValue("MARRY_STATUS",0)); //婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶)
            memberInfoMes.setChildren(dMap.getValue("CHILDREN",0)); //子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上)
            memberInfoMes.setMonthSalary(dMap.getValue("MONTH_SALARY",0)); //月收入
            memberInfoMes.setSocialSecurity(dMap.getValue("SOCIAL_SECURITY",0)); //社保情况(0-无、1-未缴满6个月、2-缴满6个月以上)
            memberInfoMes.setHouseCondition(dMap.getValue("HOUSE_CONDITION",0)); //住房条件
            memberInfoMes.setCarFlg(dMap.getValue("CAR_FLG",0)); //是否有车
            productAttornRecordMes.setMemberInfoMessageBean(memberInfoMes);
            if(dMap.getValue("LOANYEAR",0) != null && !dMap.getValue("LOANYEAR",0).equals("0")){
                loadLength = loadLength + dMap.getValue("LOANYEAR",0) + "年";
            }
            if(dMap.getValue("LOANMONTH",0) != null && !dMap.getValue("LOANMONTH",0).equals("0")){
                loadLength = loadLength + dMap.getValue("LOANMONTH",0) + "个月";
            }
            if(dMap.getValue("LOADDAY",0) != null && !dMap.getValue("LOADDAY",0).equals("0")){
                loadLength = loadLength + dMap.getValue("LOADDAY",0) + "天";
            }
            productAttornRecordMes.setLoadLength(loadLength); //借款期限
            /********************* 会员部分End *********************/

            productAttornRecordMes.setRepayment(dMap.getValue("REPAYMENT",0)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
            productAttornRecordMes.setLoadFee(dMap.getValue("LOADFEE",0)); //借款金额
            productAttornRecordMes.setTitle(dMap.getValue("TITLE",0)); //借款标题
            productAttornRecordMes.setPrice(dMap.getValue("PRICE",0)); //转让价格
            productAttornRecordMes.setSurplusdays(dMap.getValue("SURPLUSDAYS",0)); //剩余期限
            productAttornRecordMes.setSurplusperiods(dMap.getValue("surplusPeriods",0)); //剩余期数
            productAttornRecordMes.setWaitprice(dMap.getValue("WAITPRICE",0)); //待收本息
            productAttornRecordMes.setLoanrate(dMap.getValue("LOANRATE", 0)); //年利率
            productAttornRecordMes.setExpectedrate(dMap.getValue("EXPECTEDRATE",0)); //预期收益率
            productAttornRecordMes.setShFlag(dMap.getValue("SHFLAG",0)); //审核状态(0-审核通过、1-审核不通过、2-待审核)
            productAttornRecordMes.setUndertakeTime(dMap.getValue("UNDERTAKETIME",0)); //承接时间
            productAttornRecordMes.setEndtime(dMap.getValue("ENDTIME",0)); //到期时间
            productAttornRecordMes.setShyj(dMap.getValue("SHYJ",0)); //审核意见
            productAttornRecordMes.setUndertakeMember(dMap.getValue("UNDERTAKEMEMBER",0)); //承接人
            productAttornRecordMes.setId(dMap.getValue("ID",0)); //主键
            productAttornRecordMes.setBuyer(dMap.getValue("BUYER",0)); //投资人
            productAttornRecordMes.setCode(dMap.getValue("CODE",0)); //借款标编码
            productAttornRecordMes.setShFlag(dMap.getValue("SHFLAG", 0)); //审核状态(0-审核通过、1-审核未通过、2-未审核)
            productAttornRecordMes.setBalance(dMap.getValue("BALANCE",0));//账户余额
            productAttornRecordMes.setFullTime(dMap.getValue("FULLTIME",0)); //发飙日期 满标日期
            productAttornRecordMes.setProductbuyId(dMap.getValue("PRODUCTBUYID",0));//购买人id
            productAttornRecordMes.setProductid(dMap.getValue("PRODUCTID",0));//产品id
        }
        return productAttornRecordMes;
    }

    /**
     * 获取债权详情
     * @param attornProductMessageBean
     * @return
     */
    @Override
    public AttornProductMessageBean getAttornProductDetail(AttornProductMessageBean attornProductMessageBean) {
        DMap dMap = productAttornDao.getAttornProductDetai(attornProductMessageBean);
        MemberInfoMessageBean memberInfoMes = new MemberInfoMessageBean();
        String loadLength = ""; //贷款期限
        String fullTime = ""; //满标时间
        String underTakeTime = ""; //承接时间

        if(dMap != null){
            memberInfoMes.setRealname(dMap.getValue("REALNAME",0)); //真实姓名
            memberInfoMes.setTelephone(dMap.getValue("TELEPHONE",0)); //手机号
            memberInfoMes.setEmail(dMap.getValue("EMAIL",0)); //邮箱地址
            memberInfoMes.setSex(dMap.getValue("SEX",0)); //性别(0-男、1-女)
            memberInfoMes.setDetailAddress(dMap.getValue("DETAIL_ADDRESS",0)); //详细地址
            memberInfoMes.setDregree(dMap.getValue("DREGREE",0)); //学历
            memberInfoMes.setMarryStatus(dMap.getValue("MARRY_STATUS",0)); //婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶)
            memberInfoMes.setChildren(dMap.getValue("CHILDREN",0)); //子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上)
            memberInfoMes.setMonthSalary(dMap.getValue("MONTH_SALARY",0)); //月收入
            memberInfoMes.setSocialSecurity(dMap.getValue("SOCIAL_SECURITY",0)); //社保情况(0-无、1-未缴满6个月、2-缴满6个月以上)
            memberInfoMes.setHouseCondition(dMap.getValue("HOUSE_CONDITION",0)); //住房条件
            memberInfoMes.setCarFlg(dMap.getValue("CAR_FLG",0)); //是否有车
            attornProductMessageBean.setMemberInfoMessageBean(memberInfoMes);

            if(dMap.getValue("LOANYEAR",0) != null && !dMap.getValue("LOANYEAR",0).equals("0")){
                loadLength = loadLength + dMap.getValue("LOANYEAR",0) + "年";
            }
            if(dMap.getValue("LOANMONTH",0) != null && !dMap.getValue("LOANMONTH",0).equals("0")){
                loadLength = loadLength + dMap.getValue("LOANMONTH",0) + "个月";
            }
            if(dMap.getValue("LOADDAY",0) != null && !dMap.getValue("LOADDAY",0).equals("0")){
                loadLength = loadLength + dMap.getValue("LOADDAY",0) + "天";
            }
            attornProductMessageBean.setLoadLength(loadLength); //借款期限

            attornProductMessageBean.setId(dMap.getValue("ID",0)); //主键
            attornProductMessageBean.setBuyId(dMap.getValue("PRODUCTBUYID",0)); //购买id
            attornProductMessageBean.setTitle(dMap.getValue("TITLE",0)); //题目
            attornProductMessageBean.setSurplusdays(dMap.getValue("SURPLUSDAYS",0)); //剩余期限
            attornProductMessageBean.setSurplusPrincipal(dMap.getValue("WAITINGPRICIPAL",0)); //待收本金
            attornProductMessageBean.setWaitprice(dMap.getValue("WAITPRICE",0)); //待收本息
            attornProductMessageBean.setEndtime(dMap.getValue("ENDTIME",0)); //到期时间
            attornProductMessageBean.setCode(dMap.getValue("CODE",0)); //编号
            attornProductMessageBean.setTitle(dMap.getValue("TITLE",0)); //题目

            fullTime = dMap.getValue("FULLTIME",0); //满标日期(发标日期)
            if (fullTime != null && !fullTime.equals("")){
                fullTime = fullTime.substring(0,16);
            }
            attornProductMessageBean.setFullTime(fullTime); //满标日期(发标日期)

            attornProductMessageBean.setRepayment(dMap.getValue("REPAYMENT",0)); //还款方式
            attornProductMessageBean.setLoadFee(dMap.getValue("LOADFEE",0)); //借款金额
            attornProductMessageBean.setLoanrate(dMap.getValue("LOANRATE",0)); //借款年利率
            attornProductMessageBean.setExpectedrate(dMap.getValue("EXPECTEDRATE",0)); //预期收益率
            attornProductMessageBean.setBalance(dMap.getValue("BALANCE",0)); //账户余额
            attornProductMessageBean.setShFlag(dMap.getValue("SHFLAG",0)); //审核状态
            attornProductMessageBean.setPrice(dMap.getValue("PRICE",0)); //转让价格
            attornProductMessageBean.setUndertakeMember(dMap.getValue("UNDERTAKEMEMBER",0)); //承接人

            underTakeTime = dMap.getValue("UNDERTAKETIME",0); //承接时间
            if (underTakeTime != null && !underTakeTime.equals("")){
                underTakeTime = underTakeTime.substring(0,16);
            }
            attornProductMessageBean.setUndertakeTime(underTakeTime); //承接时间

            attornProductMessageBean.setProductId(dMap.getValue("PRODUCTID",0)); //产品id
            attornProductMessageBean.setBuyer(dMap.getValue("BUYER",0)); //投资人
            attornProductMessageBean.setApplyMemberId(dMap.getValue("APPLYMEMBER",0)); //借款产品申请人
        }
        return attornProductMessageBean;
    }


    /**
     * 新增债权转让数据更新
     * @param attornProductMes
     * @param memberInfo
     * @return
     */
    @Override
    public AttornProductMessageBean saveProductAttorn(AttornProductMessageBean attornProductMes,MemberInfo memberInfo) {

        String createTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //创建时间
        String buyId = attornProductMes.getBuyId(); //购买id
        String underMemberId = "";//承接人id
        String balance = attornProductMes.getBalance(); //申请人余额(String型)
        Double balancedou = 0.0d; //申请人余额(Doouble型)
        String attornPrice = attornProductMes.getPrice(); //转让价格(String型)
        Double attornPricedou = 0.0d; //转让价格(Double型)

        if(memberInfo != null){
            underMemberId = memberInfo.getId(); //承接人id
        }

        if(attornPrice != null && !attornPrice.equals("")){ //转让价格
            attornPricedou = TypeTool.getDouble(attornPrice);
        }

        if(balance != null && !balance.equals("")){ //余额
            balancedou = TypeTool.getDouble(balance);
            if(balancedou < attornPricedou){
                attornProductMes.setResult("error");
                attornProductMes.setInfo("当前余额不足，不能进行承接!");
                return attornProductMes;
            }else{
                balancedou = balancedou - attornPricedou;
                memberInfo.setBalance(balancedou);
            }
        }

        /******************** 债权转让部分Start ********************/
        ProductAttornRecord productAttornRecord = new ProductAttornRecord(); //转让信息
        productAttornRecord.setId(attornProductMes.getId());//转让记录表id
        productAttornRecord.setUndertakemember(underMemberId);//承接人
        productAttornRecord.setUndertaketime(createTime); //承接时间
        /********************* 债权转让部分End *********************/


        //执行语句
        DaoResult result = productAttornDao.updateAttornUpdate(productAttornRecord,memberInfo);

        if(result.getCode()<0){
            attornProductMes.setResult("error");
            attornProductMes.setInfo(result.getErrText());
        }
        else{
            attornProductMes.setResult("success");
            attornProductMes.setInfo("承接成功！");
        }
        return attornProductMes;
    }



    /**
     * 添加转让记录（发起转让）
     * @param productMyAttornMes
     * @return
     */
    @Override
    public ProductMyAttornBusinessBean confirmAttornProduct(ProductMyAttornMessageBean productMyAttornMes) {
        ProductMyAttornBusinessBean productMyAttornBusinessBean = new ProductMyAttornBusinessBean();
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
        String buyId = productMyAttornMes.getProductBuyId(); //产品购买id
        String price = productMyAttornMes.getPrice(); //转让价格(String型)
        Double priceDou = 0.0d; //转让价格(Double型)
        String expectedRate = productMyAttornMes.getExpectedRate(); //预期收益率
        String shFlag = "2"; //审核状态(0-审核通过、1-审核不通过、2-待审核)
        String createTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //创建时间
        ProductAttornRecord productAttornRecord = new ProductAttornRecord(); //债权转让
        ProductBuyInfo productBuyInfo = new ProductBuyInfo(); //产品购买
        MemberInfo memberInfo = new MemberInfo(); //会员

        try{

            if(price != null && !price.equals("")){ //转让价格
                priceDou = TypeTool.getDouble(price);
            }

            /*************** 债权部分Start ***************/
            productAttornRecord.setId(Uid); //主键
            productAttornRecord.setProductbuyid(buyId); //产品购买id
            productAttornRecord.setPrice(priceDou); //转让价格(String型)
            productAttornRecord.setExpectedrate(expectedRate); //预期收益率
            productAttornRecord.setShflag(shFlag); //审核状态(0-审核通过、1-审核不通过、2-待审核)
            productAttornRecord.setCreatetime(createTime); //创建时间
            /**************** 债权部分End ****************/

            /*************** 产品购买Start ***************/
            productBuyInfo.setId(buyId); //主键
            productBuyInfo.setType("1"); //类型 0-持有 1-转让中 2-已转让
            /**************** 产品购买End ****************/

            DaoResult result = productAttornDao.confirmAttornProduct(productAttornRecord,productBuyInfo); //发起债权
            if(result.getCode() < 0){
                productMyAttornBusinessBean.setInfo("发起债权失败！");
                productMyAttornBusinessBean.setCode("error");
            }
            productMyAttornBusinessBean.setInfo("发起债权成功！");
            productMyAttornBusinessBean.setCode("success");
        }catch(Exception e){
            e.printStackTrace();
        }

        return productMyAttornBusinessBean;

    }


    /**
     * 获取债权转让标(后台)By zhangerxin
     * @param productAttornMes
     * @return
     */
    @Override
    public List<ProductAttornMessage> queryProductAttornByPage(ProductAttornMessage productAttornMes) {
        List<ProductAttornMessage> listProductAttornMes = new ArrayList<ProductAttornMessage>();
        int loanYear = 0; //贷款期限(年)
        int loanMonth = 0; //贷款期限(月)
        int loanDay = 0; //贷款期限(天)
        String loanLength = ""; //贷款期限(时长)
        String underTakeTime = ""; //承接时间
        String title = ""; //借款标题目
        DMap dMap = productAttornDao.queryProductAttornByPage(productAttornMes);

        if(dMap.getCount() > 0){
            for(int i=0;i<dMap.getCount();i++){
                ProductAttornMessage productAttornMessage = new ProductAttornMessage();
                productAttornMessage.setId(dMap.getValue("ID",i)); //主键
                productAttornMessage.setProductBuyId(dMap.getValue("PRODUCTBUYID", i)); //产品购买id
                productAttornMessage.setProductId(dMap.getValue("PRODUCTID", i)); //产品id
                productAttornMessage.setPrice(dMap.getValue("PRICE",i)); //转让价格
                productAttornMessage.setExpectedRate(dMap.getValue("EXPECTEDRATE",i)); //预期收益率
                productAttornMessage.setShFlag(dMap.getValue("SHFLAG",i)); //审核状态(0-审核通过、1-审核不通过、2-待审核)
                productAttornMessage.setShyj(dMap.getValue("SHYJ",i)); //审核意见
                productAttornMessage.setUnderTakeMember(dMap.getValue("UNDERTAKEMEMBER",i)); //承接人id

                underTakeTime = dMap.getValue("UNDERTAKETIME",i); //承接时间
                if (underTakeTime != null && !underTakeTime.equals("")){
                    underTakeTime = underTakeTime.substring(0,16);
                }
                productAttornMessage.setUnderTakeTime(underTakeTime);

                productAttornMessage.setCreateTime(dMap.getValue("CREATETIME",i)); //创建时间

                title = dMap.getValue("TITLE",i); //借款标题目
                if (title != null){
                    if (title.length() > 13){
                        title = title.substring(0,13) + "...";
                    }
                }
                productAttornMessage.setTitle(title);

                productAttornMessage.setCode(dMap.getValue("CODE",i)); //编码
                productAttornMessage.setBuyerName(dMap.getValue("BUYERNAME",i)); //购买人姓名
                productAttornMessage.setLoanRate(dMap.getValue("LOANRATE",i)); //贷款利率

                /*************** 贷款时长Start ***************/
                loanYear = TypeTool.getInt(dMap.getValue("LOANYEAR", i)); //贷款期限(年)
                loanMonth = TypeTool.getInt(dMap.getValue("LOANMONTH",i)); //贷款期限(月)
                loanDay = TypeTool.getInt(dMap.getValue("LOADDAY",i)); //贷款期限(天)
                loanLength = ""; //贷款时长
                if(loanYear != 0){
                    loanLength = loanLength + loanYear + "年";
                }
                if(loanMonth != 0){
                    loanLength = loanLength + loanMonth + "月";
                }
                if(loanDay != 0){
                    loanLength = loanLength + loanDay + "天";
                }
                productAttornMessage.setLoanLength(loanLength); //贷款时长
                /**************** 贷款时长End ****************/

                productAttornMessage.setSurplusQs(dMap.getValue("SURPLUSPERIODS",i)); //剩余期数
                productAttornMessage.setWaitingPrincipal(dMap.getValue("WAITINGPRINCIPAL",i)); //待收本金
                productAttornMessage.setSurplusDays(dMap.getValue("SURPLUSDAYS",i)); //剩余时间
                listProductAttornMes.add(productAttornMessage);
            }
        }
        return listProductAttornMes;
    }


    /**
     * 获取债权转让标数量
     * @param productAttornMes
     * @return
     */
    @Override
    public int queryProductAttornCount(ProductAttornMessage productAttornMes) {
        DMap dMap = productAttornDao.queryProductAttornCount(productAttornMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 设置债权审核状态(后台) zhangerxin
     * @param productAttornMes
     * @return
     */
    @Override
    public ProductAttornBusiness setProductAttornShFlag(ProductAttornMessage productAttornMes) {
        ProductAttornBusiness productAttornBusiness = new ProductAttornBusiness();
        DaoResult daoResult = productAttornDao.setProductAttornShFlag(productAttornMes);
        if(daoResult.getCode() < 0){
            productAttornBusiness.setResult("error");
            productAttornBusiness.setInfo(daoResult.getErrText());
            return productAttornBusiness;
        }
        productAttornBusiness.setResult("success");
        productAttornBusiness.setInfo("承接成功！");
        return productAttornBusiness;
    }


    /**
     * 获取债权转让集合(App端)
     * @param memberId
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<AppProductAttornMessageBean> getAppAttornList(String memberId, String typeId, String pageIndex, String pageSize,String orderColumn,String orderType) {
        List<AppProductAttornMessageBean> listAppProductAttornMes = new ArrayList<AppProductAttornMessageBean>();
        DMap dMap = productAttornDao.getAppAttornList(memberId,typeId,pageIndex,pageSize,orderColumn,orderType);
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                AppProductAttornMessageBean appProductAttornMessageBean = new AppProductAttornMessageBean();
                appProductAttornMessageBean.setId(dMap.getValue("ID",i)); //主键
                appProductAttornMessageBean.setProductBuyId(dMap.getValue("PRODUCTBUYID",i)); //产品购买id
                appProductAttornMessageBean.setTypeId(dMap.getValue("TYPEID",i)); //产品类型id
                appProductAttornMessageBean.setTitle(dMap.getValue("TITLE",i)); //产品题目
                appProductAttornMessageBean.setExpectedRate(dMap.getValue("EXPECTEDRATE",i)); //预期收益率
                appProductAttornMessageBean.setSurplusDays(dMap.getValue("SURPLUSDAYS",i)); //剩余期限
                appProductAttornMessageBean.setAttronPrice(dMap.getValue("ATTORNPRICE",i)); //转让价格
                appProductAttornMessageBean.setShFlag(dMap.getValue("SHFLAG",i)); //审核状态(0-审核通过、1-审核不通过、2-待审核)
                listAppProductAttornMes.add(appProductAttornMessageBean);
            }

        }
        return listAppProductAttornMes;
    }


    /**
     * 获取债权转让标详情(App端)
     * @param attornId
     * @return
     */
    @Override
    public AppProductAttornDetailMessageBean getAppAttornDetail(String attornId) {
        AppProductAttornDetailMessageBean appProductAttornDetailMessageBean = new AppProductAttornDetailMessageBean();
        DMap dMap = productAttornDao.getAppAttornDetail(attornId);
        String loanLength = ""; //贷款时长
        if (dMap != null){
            appProductAttornDetailMessageBean.setId(dMap.getValue("ID",0)); //主键
            appProductAttornDetailMessageBean.setProductBuyId(dMap.getValue("PRODUCTBUYID",0)); //产品购买id
            appProductAttornDetailMessageBean.setAttronPrice(dMap.getValue("ATTORNPRICE",0)); //转让价格
            appProductAttornDetailMessageBean.setExpectedRate(dMap.getValue("EXPECTEDRATE",0)); //预期收益率
            appProductAttornDetailMessageBean.setShFlag(dMap.getValue("SHFLAG",0)); //审核状态
            appProductAttornDetailMessageBean.setProductId(dMap.getValue("PRODUCTID",0)); //产品id
            appProductAttornDetailMessageBean.setTitle(dMap.getValue("TITLE",0)); //产品题目
            appProductAttornDetailMessageBean.setLoanRate(dMap.getValue("LOANRATE",0)); //年利率
            appProductAttornDetailMessageBean.setSurplusDays(dMap.getValue("SURPLUSDAYS",0)); //剩余期限
            appProductAttornDetailMessageBean.setSurplusPrincipal(dMap.getValue("SURPLUSPRINCIPAL",0)); //剩余本金
            appProductAttornDetailMessageBean.setReceivedPrice(dMap.getValue("RECEIVEDPRICE",0)); //待收本息
            appProductAttornDetailMessageBean.setNextReceiveTime(dMap.getValue("NEXTRECEIVETIME",0)); //下期还款日
            appProductAttornDetailMessageBean.setCode(dMap.getValue("CODE",0)); //产品编号
            appProductAttornDetailMessageBean.setLoanFee(dMap.getValue("LOANFEE",0)); //贷款金额
            appProductAttornDetailMessageBean.setFullTime(dMap.getValue("FULLTIME",0)); //发标日期
            if (dMap.getValue("LOANYEAR",0) != null && !dMap.getValue("LOANYEAR",0).equals("")){ //贷款期限(年)
                if (!dMap.getValue("LOANYEAR",0).equals("0")){
                    loanLength = loanLength + dMap.getValue("LOANYEAR",0) + "年";
                }
            }
            if (dMap.getValue("LOANMONTH",0) != null && !dMap.getValue("LOANMONTH",0).equals("")){ //贷款期限(月)
                if (!dMap.getValue("LOANMONTH",0).equals("0")){
                    loanLength = loanLength + dMap.getValue("LOANMONTH",0) + "个月";
                }
            }
            if (dMap.getValue("LOADDAY",0) != null && !dMap.getValue("LOADDAY",0).equals("")){
                if (!dMap.getValue("LOADDAY",0).equals("0")){
                    loanLength = loanLength + dMap.getValue("LOADDAY",0) + "天";
                }
            }
            appProductAttornDetailMessageBean.setLoanLength(loanLength); //贷款时长
            appProductAttornDetailMessageBean.setRepayment(dMap.getValue("REPAYMENT",0)); //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
            appProductAttornDetailMessageBean.setProjectNo(dMap.getValue("PROJECTNO",0)); //项目ID号
            appProductAttornDetailMessageBean.setOtherIpsAcctNo(dMap.getValue("BUYERIPSACCTNO",0)); //它方账户
            appProductAttornDetailMessageBean.setApplyMemberId(dMap.getValue("APPLYMEMBER",0)); //借款人id
            appProductAttornDetailMessageBean.setBuyer(dMap.getValue("BUYER",0)); //投资人
        }
        return appProductAttornDetailMessageBean;
    }

    /**
     * 购买已购买债券
     * @param timeStyle
     * @param sTime
     * @param eTime
     * @param keyWord
     * @param value
     * @return
     */
    @Override
    public List<AttornInvestedMessageBean> queryInvestedAttornByPage(String memberId, String timeStyle, String sTime, String eTime, String keyWord, String value, String orderColumn,String orderType, String pageIndex, String pageSize) {
        DMap dMap = productAttornDao.queryInvestedAttornByPage(memberId,timeStyle,sTime,eTime,keyWord,value,orderColumn,orderType,pageIndex,pageSize);
        List<AttornInvestedMessageBean> listAttornInvestMes = new ArrayList<AttornInvestedMessageBean>();
        String principal = ""; //本金(String型)
        Double principalDou = 0.0d; //本金(Double型)
        String interest = ""; //利息(String型)
        Double interestDou = 0.0d; //利息(Double型)
        Double priceDou = 0.0d; //价格(Double型)
        String underTime = ""; //承接时间
        String shFlag = ""; //审核状态(0-审核通过、1-审核未通过、2-未审核)
        String shFlagName = ""; //审核状态名称

        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                AttornInvestedMessageBean attornInvestedMessageBean = new AttornInvestedMessageBean();
                attornInvestedMessageBean.setId(dMap.getValue("ID",i)); //债权id
                attornInvestedMessageBean.setBuyId(dMap.getValue("PRODUCTBUYID",i)); //购买id
                attornInvestedMessageBean.setProductId(dMap.getValue("PRODUCTID",i)); //产品id
                attornInvestedMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                attornInvestedMessageBean.setCode(dMap.getValue("CODE",i)); //编码
                attornInvestedMessageBean.setLoanRate(dMap.getValue("LOANRATE",i)); //年利率
                principal = dMap.getValue("WAITINGPRINCIPAL",i); //待收本金
                interest = dMap.getValue("WAITINGINTEREST",i); //待收利息
                if (principal != null && !principal.equals("")){
                    principalDou = TypeTool.getDouble(principal);
                }
                if (interest != null && !interest.equals("")){
                    interestDou = TypeTool.getDouble(interest);
                }
                priceDou = principalDou + interestDou; //价格
                attornInvestedMessageBean.setPrice(TypeTool.getString(priceDou)); //待收本息
                attornInvestedMessageBean.setUnderPrice(dMap.getValue("PRICE",i)); //转让价格
                underTime = dMap.getValue("UNDERTAKETIME",i); //承接时间
                if (underTime != null && !underTime.equals("")){
                    underTime = underTime.substring(0,10);
                }
                attornInvestedMessageBean.setUnderTime(underTime); //承接时间
                shFlag = dMap.getValue("SHFLAG",i); //审核状态(0-审核成功、1-审核不成功、2-未审核)
                if (shFlag != null && !shFlag.equals("")){
                    if (shFlag.equals("0")){
                        shFlagName = "审核通过";
                    }else if (shFlag.equals("1")){
                        shFlagName = "审核未通过";
                    }else if (shFlag.equals("2")){
                        shFlagName = "未审核";
                    }
                }
                attornInvestedMessageBean.setShFlag(shFlagName); //审核状态
                listAttornInvestMes.add(attornInvestedMessageBean);
            }
        }
        return listAttornInvestMes;
    }


    /**
     * 已购买的债权数量
     * @param memberId
     * @param timeStyle
     * @param sTime
     * @param eTime
     * @param keyWord
     * @param value
     * @return
     */
    @Override
    public int queryInvestedAttornCount(String memberId, String timeStyle, String sTime, String eTime, String keyWord, String value) {
        DMap dMap = productAttornDao.queryInvestedAttornByPage(memberId,timeStyle,sTime,eTime,keyWord,value,null,null,null,null);
        int count = dMap.getCount();
        return count;
    }


    /**
     * 获取待承接债权
     * @param productAttornMes
     * @return
     */
    @Override
    public List<ProductAttornMessage> queryProductAttornUnderTaked(ProductAttornMessage productAttornMes) {
        DMap dMap = productAttornDao.queryProductAttornUnderTaked(productAttornMes); //获取待承接债权
        List<ProductAttornMessage> listProductAttorn = new ArrayList<ProductAttornMessage>();
        String loanLength = ""; //借款时长
        String loanYear = ""; //贷款时长(年份)
        String loanMonth = ""; //贷款时长(月份)
        String loanDay = ""; //贷款时长(天)
        String attornTime = ""; //发起债权时间
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ProductAttornMessage productAttornMessage = new ProductAttornMessage();
                productAttornMessage.setId(dMap.getValue("ID",i)); //主键
                productAttornMessage.setTitle(dMap.getValue("TITLE",i)); //题目
                productAttornMessage.setCode(dMap.getValue("CODE",i)); //编码
                productAttornMessage.setBuyerName(dMap.getValue("BUYNAME",i)); //投资人(债权发起人)姓名
                productAttornMessage.setPrice(dMap.getValue("PRICE",i)); //转让标价格
                productAttornMessage.setLoanRate(dMap.getValue("LOANRATE",i)); //年利率
                productAttornMessage.setExpectedRate(dMap.getValue("EXPECTEDRATE",i)); //预期收益率

                loanYear = dMap.getValue("LOANYEAR",i); //年份
                loanMonth = dMap.getValue("LOANMONTH",i); //月份
                loanDay = dMap.getValue("LOADDAY",i); //天
                if (loanYear != null && !loanYear.equals("")){ //年份
                    if (!loanYear.equals("0")){
                        loanLength = loanLength + loanYear + "年";
                    }
                }
                if (loanMonth != null && !loanMonth.equals("")){ //月份
                    if (!loanMonth.equals("0")){
                        loanLength = loanLength + loanMonth + "个月";
                    }
                }
                if (loanDay != null && !loanDay.equals("")){ //天
                    if (!loanDay.equals("0")){
                        loanLength = loanLength + loanDay + "天";
                    }
                }
                productAttornMessage.setLoanLength(loanLength); //贷款时长
                productAttornMessage.setSurplusQs(dMap.getValue("SURPLUSPERIODS",i)); //剩余期数
                productAttornMessage.setSurplusDays(dMap.getValue("SURPLUSDAYS",i)); //剩余天数
                productAttornMessage.setWaitingPrincipal(dMap.getValue("WAITINGPRICIPAL",i)); //待收本金

                attornTime = dMap.getValue("ATTORNTIME",i); //发起债权时间
                if (attornTime != null && !attornTime.equals("")){
                    attornTime = attornTime.substring(0,10);
                }
                productAttornMessage.setCreateTime(attornTime);
                listProductAttorn.add(productAttornMessage);
            }
        }
        return listProductAttorn;
    }

    /**
     * 获取待承接债权数量
     * @param productAttornMes
     * @return
     */
    @Override
    public int queryProductAttornUnderTakedCount(ProductAttornMessage productAttornMes) {
        DMap dMap = productAttornDao.queryProductAttornUnderTakedCount(productAttornMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 获取失败的债权
     * @param productAttornMes
     * @return
     */
    @Override
    public List<ProductAttornMessage> queryProductAttornFail(ProductAttornMessage productAttornMes) {
        DMap dMap = productAttornDao.queryProductAttornFail(productAttornMes); //获取待承接债权
        List<ProductAttornMessage> listProductAttorn = new ArrayList<ProductAttornMessage>();
        String loanLength = ""; //借款时长
        String loanYear = ""; //贷款时长(年份)
        String loanMonth = ""; //贷款时长(月份)
        String loanDay = ""; //贷款时长(天)
        String attornTime = ""; //发起债权时间
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ProductAttornMessage productAttornMessage = new ProductAttornMessage();
                productAttornMessage.setId(dMap.getValue("ID",i)); //主键
                productAttornMessage.setTitle(dMap.getValue("TITLE",i)); //题目
                productAttornMessage.setCode(dMap.getValue("CODE",i)); //编码
                productAttornMessage.setBuyerName(dMap.getValue("BUYNAME",i)); //投资人(债权发起人)姓名
                productAttornMessage.setPrice(dMap.getValue("PRICE",i)); //转让标价格
                productAttornMessage.setLoanRate(dMap.getValue("LOANRATE",i)); //年利率
                productAttornMessage.setExpectedRate(dMap.getValue("EXPECTEDRATE",i)); //预期收益率

                loanYear = dMap.getValue("LOANYEAR",i); //年份
                loanMonth = dMap.getValue("LOANMONTH",i); //月份
                loanDay = dMap.getValue("LOADDAY",i); //天
                if (loanYear != null && !loanYear.equals("")){ //年份
                    if (!loanYear.equals("0")){
                        loanLength = loanLength + loanYear + "年";
                    }
                }
                if (loanMonth != null && !loanMonth.equals("")){ //月份
                    if (!loanMonth.equals("0")){
                        loanLength = loanLength + loanMonth + "个月";
                    }
                }
                if (loanDay != null && !loanDay.equals("")){ //天
                    if (!loanDay.equals("0")){
                        loanLength = loanLength + loanDay + "天";
                    }
                }
                productAttornMessage.setLoanLength(loanLength); //贷款时长
                productAttornMessage.setSurplusQs(dMap.getValue("SURPLUSPERIODS",i)); //剩余期数
                productAttornMessage.setSurplusDays(dMap.getValue("SURPLUSDAYS",i)); //剩余天数
                productAttornMessage.setWaitingPrincipal(dMap.getValue("WAITINGPRICIPAL",i)); //待收本金

                attornTime = dMap.getValue("ATTORNTIME",i); //发起债权时间
                if (attornTime != null && !attornTime.equals("")){
                    attornTime = attornTime.substring(0,10);
                }
                productAttornMessage.setCreateTime(attornTime);
                listProductAttorn.add(productAttornMessage);
            }
        }
        return listProductAttorn;
    }


    /**
     * 获取失败的债权数量
     * @param productAttornMes
     * @return
     */
    @Override
    public int queryProductAttornFailCount(ProductAttornMessage productAttornMes) {
        DMap dMap = productAttornDao.queryProductAttornFailCount(productAttornMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


}
