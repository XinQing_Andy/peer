package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.SecuritySettingsDao;
import com.mh.peer.dao.member.AllMemberDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberLinkman;
import com.mh.peer.model.entity.MemberSetupQuestions;
import com.mh.peer.model.message.*;
import com.mh.peer.service.front.SecuritySettingsService;
import com.mh.peer.util.Encrypt;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Random;
import java.util.UUID;

/**
 * Created by cuibin on 2016/5/4.
 */
@Service
public class SecuritySettingsServiceImpl implements SecuritySettingsService{
    @Autowired
    SecuritySettingsDao securitySettingsDao;
    @Autowired
    AllMemberDao allMemberDao;
    @Autowired
    ServletUtil servletUtil;
    /**
     * 验证旧密码是否存在
     */
    @Override
    public MemberInfoMessageBean checkOldPassword(String passWord) {
        if(passWord != null && !passWord.equals("")){
            Encrypt encrypt = new Encrypt();
            passWord = encrypt.handleMd5(passWord);
        }
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        DMap dMap = securitySettingsDao.checkOldPassword(memberInfo.getId());
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(dMap.getCount() == 1 && dMap.getValue("PASSWORD",0).equals(passWord)){
            memberInfoMessageBean.setResult("success");
        }else{
            memberInfoMessageBean.setResult("error");
            memberInfoMessageBean.setInfo("当前密码错误");
        }
        return memberInfoMessageBean;
    }
    /**
     * 更新密码
     */
    @Override
    public MemberInfoMessageBean updatePassWord(String passWord) {
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(passWord != null && !passWord.equals("")){
            Encrypt encrypt = new Encrypt();
            passWord = encrypt.handleMd5(passWord);
        }else{
            memberInfoMessageBean.setResult("error");
            memberInfoMessageBean.setInfo("重置失败");
            return memberInfoMessageBean;
        }
        HttpSession session = servletUtil.getSession();
        MemberInfo sessionMemberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(sessionMemberInfo.getId());
        memberInfo.setPasswdFlg("0");
        memberInfo.setPassword(passWord);
        DaoResult daoResult = securitySettingsDao.updatePassWord(memberInfo);
        if(daoResult.getCode() < 0){
            memberInfoMessageBean.setResult("error");
            memberInfoMessageBean.setInfo("重置失败");
        }else{
            memberInfoMessageBean.setResult("success");
            memberInfoMessageBean.setInfo("重置成功");
        }
        return memberInfoMessageBean;
    }
    /**
     * 保存紧急联系人
     */
    @Override
    public MemberLinkmanMessageBean saveContact(MemberLinkmanMessageBean memberLinkmanMessageBean) {
        HttpSession session = servletUtil.getSession();
        MemberInfo sessionMemberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        MemberLinkman mlm = new MemberLinkman();
        if(memberLinkmanMessageBean != null){
            mlm.setId(UUID.randomUUID().toString().replaceAll("\\-",""));//主键
            mlm.setMemberid(sessionMemberInfo.getId());//会员id
            mlm.setXm(memberLinkmanMessageBean.getXm());//联系人姓名
            mlm.setTelephone(memberLinkmanMessageBean.getTelephone());//手机号
            mlm.setRelationship(memberLinkmanMessageBean.getRelationship());//与联系人关系
            mlm.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss"));//创建日期
        }
        DaoResult daoResult = securitySettingsDao.saveContact(mlm);
        MemberLinkmanMessageBean newMlmb = new MemberLinkmanMessageBean();
        if(daoResult.getCode() < 0){
            newMlmb.setInfo("添加失败");
            newMlmb.setResult("error");
        }else{
            newMlmb.setInfo("添加成功");
            newMlmb.setResult("success");
        }
        return newMlmb;
    }


    /**
     * 保存紧急联系人 手机端
     */
    @Override
    public MemberLinkmanMessageBean appSaveContact(MemberLinkmanMessageBean memberLinkmanMessageBean) {
        MemberLinkman mlm = new MemberLinkman();
        //查询一个联系人
        DMap dMap = securitySettingsDao.queryOnlyContact(memberLinkmanMessageBean.getMemberid());
        if(dMap.getCount() > 0){
            mlm.setId(dMap.getValue("ID",0));//主键
        }else{
            mlm.setId(UUID.randomUUID().toString().replaceAll("\\-",""));//主键
        }
        mlm.setMemberid(memberLinkmanMessageBean.getMemberid());//会员id
        mlm.setXm(memberLinkmanMessageBean.getXm());//联系人姓名
        mlm.setTelephone(memberLinkmanMessageBean.getTelephone());//手机号
        mlm.setRelationship(memberLinkmanMessageBean.getRelationship());//与联系人关系
        mlm.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss"));//创建日期

        DaoResult daoResult = null;
        if(dMap.getCount() > 0){
            daoResult = securitySettingsDao.updateContact(mlm);
        }else{
            daoResult = securitySettingsDao.saveContact(mlm);
        }

        MemberLinkmanMessageBean newMlmb = new MemberLinkmanMessageBean();
        if(daoResult.getCode() < 0){
            newMlmb.setInfo("设置失败");
            newMlmb.setResult("error");
        }else{
            newMlmb.setInfo("设置成功");
            newMlmb.setResult("success");
        }
        return newMlmb;
    }


    /**
     * 查询一个紧急联系人
     */
    @Override
    public MemberLinkmanMessageBean queryOnlyContact(String memberId) {
        MemberLinkmanMessageBean memberLinkmanMessageBean = new MemberLinkmanMessageBean();
        if(memberId != null){
            DMap dMap = securitySettingsDao.queryOnlyContact(memberId);
            if(dMap.getCount() == 1){
                memberLinkmanMessageBean.setId(dMap.getValue("ID",0));//主键
                memberLinkmanMessageBean.setMemberid(dMap.getValue("MEMBERID", 0));//会员id
                memberLinkmanMessageBean.setXm(dMap.getValue("XM", 0));//联系人姓名
                memberLinkmanMessageBean.setTelephone(dMap.getValue("TELEPHONE", 0));//手机号
                memberLinkmanMessageBean.setRelationship(dMap.getValue("RELATIONSHIP", 0));//与联系人关系
                memberLinkmanMessageBean.setCreatetime(dMap.getValue("CREATETIME",0));//创建日期
            }
        }
        return memberLinkmanMessageBean;
    }


    /**
     * 保存邮箱
     */
    @Override
    public MemberInfoMessageBean saveEmail(String email) {
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(email == null){
            memberInfoMessageBean.setInfo("认证失败");
            memberInfoMessageBean.setResult("error");
            return memberInfoMessageBean;
        }
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setEmail(email);
        HttpSession session = servletUtil.getSession();
        MemberInfo sessionMemberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        memberInfo.setId(sessionMemberInfo.getId());
        DaoResult daoResult = securitySettingsDao.saveEmail(memberInfo);
        if(daoResult.getCode() < 0){
            memberInfoMessageBean.setInfo("认证失败");
            memberInfoMessageBean.setResult("error");
        }else{
            memberInfoMessageBean.setInfo("认证成功");
            memberInfoMessageBean.setResult("success");
        }
        return memberInfoMessageBean;
    }
    /**
     * 保存邮箱验证标识
     */
    @Override
    public MemberInfoMessageBean saveEmailMessage(String mailFlg,String email) {
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(mailFlg == null || mailFlg.equals("") || email == null || email.equals("")){
            memberInfoMessageBean.setResult("error");
            return memberInfoMessageBean;
        }
        MemberInfo memberInfo = new MemberInfo();
        HttpSession session = servletUtil.getSession();
        MemberInfo sessionMemberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        memberInfo.setId(sessionMemberInfo.getId());
        memberInfo.setMailFlg(mailFlg);
        memberInfo.setEmail(email);
        DaoResult daoResult = securitySettingsDao.saveEmailMessage(memberInfo);

        if(daoResult.getCode() < 0){
            memberInfoMessageBean.setResult("error");
        }else{
            memberInfoMessageBean.setResult("success");
        }
        return memberInfoMessageBean;
    }
    /**
     * 更改验证状态
     */
    @Override
    public MemberInfoMessageBean updateEmailMessage(String id) {
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(id);
        memberInfo.setMailFlg("0");
        DaoResult daoResult = securitySettingsDao.updateEmailMessage(memberInfo);

        if(daoResult.getCode() < 0){
            memberInfoMessageBean.setResult("error");
        }else{
            memberInfoMessageBean.setResult("success");
        }
        return memberInfoMessageBean;
    }
    /**
     * 验证邮箱标识
     */
    @Override
    public MemberInfoMessageBean checkEmailMessage(String message, String id) {
        DMap dMap = securitySettingsDao.checkEmailMessage(message,id);
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(dMap.getCount() == 1){
            memberInfoMessageBean.setResult("success");
        }else{
            memberInfoMessageBean.setResult("error");
        }
        return memberInfoMessageBean;
    }
    /**
     * 验证是否认证成功
     */
    @Override
    public MemberInfoMessageBean checkEmailIsSuccess(String email) {
        DMap dMap = securitySettingsDao.checkEmailIsSuccess(email);
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if(dMap.getCount() == 1){
            memberInfoMessageBean.setResult("success");
        }else{
            memberInfoMessageBean.setResult("error");
        }
        return memberInfoMessageBean;
    }
    /**
     * 查询一个问题（所有问题表）
     */
    @Override
    public SysSecurityQuestionMessageBean queryOnlyquestion(int id) {
        DMap dMap = securitySettingsDao.queryOnlyquestion(id);
        SysSecurityQuestionMessageBean sqm = new SysSecurityQuestionMessageBean();
        if(dMap.getCount() == 1){
            sqm.setQuestion(dMap.getValue("QUESTION",0));
            sqm.setId(dMap.getValue("ID",0));
        }
        return sqm;
    }
    /**
     * 查询是否设置安全保护问题
     */
    @Override
    public boolean queryIsQuestion(String userId) {
        return securitySettingsDao.queryIsquestion(userId).getCount() == 1 ? true : false;
    }

    /**
     * 保存问题
     */
    @Override
    public MemberSetupQuestionsMessageBean saveQuestion(MemberSetupQuestionsMessageBean msqm) {
        MemberSetupQuestions memberSetupQuestions = new MemberSetupQuestions();
        MemberSetupQuestionsMessageBean msmb = new MemberSetupQuestionsMessageBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if(msqm != null){
            memberSetupQuestions.setMemberid(memberInfo.getId());//id

            memberSetupQuestions.setQuestionone(Integer.parseInt(msqm.getQuestionone()));//问题
            memberSetupQuestions.setQuestiontwo(Integer.parseInt(msqm.getQuestiontwo()));
            memberSetupQuestions.setQuestionthree(Integer.parseInt(msqm.getQuestionthree()));

            memberSetupQuestions.setAnswerone(msqm.getAnswerone());//答案
            memberSetupQuestions.setAnswertwo(msqm.getAnswertwo());
            memberSetupQuestions.setAnswerthree(msqm.getAnswerthree());
            //创建时间
            memberSetupQuestions.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss"));
        }
        DaoResult daoResult = securitySettingsDao.saveQuestion(memberSetupQuestions);
        if(daoResult.getCode() < 0){
            msmb.setResult("error");
            msmb.setInfo("保存失败");
        }else{
            msmb.setResult("success");
            msmb.setInfo("保存成功");
        }
        return msmb;
    }
    /**
     * 保存交易密码
     */
    @Override
    public MemberInfoMessageBean saveTransactionPwd(MemberInfoMessageBean memberInfoMessageBean) {
        MemberInfo memberInfo = new MemberInfo();
        MemberInfo sessionMemberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if(memberInfoMessageBean != null){
            memberInfo.setId(sessionMemberInfo.getId());
            memberInfo.setTraderpassword(memberInfoMessageBean.getTraderpassword());
        }
        DaoResult daoResult = securitySettingsDao.saveTransactionPwd(memberInfo);
        MemberInfoMessageBean memberInfoMessageBean1 = new MemberInfoMessageBean();
        if(daoResult.getCode() < 0){
            memberInfoMessageBean1.setResult("error");
            memberInfoMessageBean1.setInfo("更新失败");
        }else{
            memberInfoMessageBean1.setResult("success");
            memberInfoMessageBean1.setInfo("更新成功");
        }
        return memberInfoMessageBean1;
    }

    /**
     * 更新手机号
     */
    @Override
    public MemberInfoMessageBean updateTelephone(MemberInfoMessageBean memberInfoMessageBean) {
        MemberInfoMessageBean result = new MemberInfoMessageBean();
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(memberInfoMessageBean.getId()); // id
        memberInfo.setTelephone(memberInfoMessageBean.getTelephone()); //手机号
        memberInfo.setTelephoneFlg("0"); //是否已设置手机号
        DaoResult daoResult = allMemberDao.updateTelephone(memberInfo);
        if (daoResult.getCode() < 0) {
            result.setResult("error");
            result.setInfo("更新失败");
        } else {
            result.setResult("success");
            result.setInfo("更新成功");
        }
        return result;
    }

    /**
     * 获取一个问题提问和答案
     */
    @Override
    public String[] getOneQuestion(String userId) {
        String[] arr = new String[2];
        DMap dMap = securitySettingsDao.getOneQuestion(userId);
        if (dMap.getCount() > 0) {
            arr[0] = dMap.getValue("QUESTIONONE", 0);
            arr[1] = dMap.getValue("ANSWERONE", 0);
        }
        return arr;
    }

    /**
     * 查询一个省
     */
    @Override
    public SysProvinceMessageBean queryProvinceById(String id) {
        DMap dMap = securitySettingsDao.queryProvinceById(id);
        SysProvinceMessageBean result = new SysProvinceMessageBean();
        if (dMap.getCount() > 0) {
            result.setId(dMap.getValue("ID", 0));
            result.setProvinceName(dMap.getValue("PROVINCE_NAME", 0));
            result.setSort(dMap.getValue("SORT", 0));
        }
        return result;
    }

    /**
     * 查询一个市
     */
    @Override
    public SysCityMessagebean queryCityById(String id) {
        DMap dMap = securitySettingsDao.queryOnlyContact(id);
        SysCityMessagebean sysCityMessagebean = new SysCityMessagebean();
        if (dMap.getCount() > 0) {
            sysCityMessagebean.setId(dMap.getValue("ID", 0));
            sysCityMessagebean.setProvinceid(dMap.getValue("PROVINCEID", 0));
            sysCityMessagebean.setCityname(dMap.getValue("CITYNAME", 0));
            sysCityMessagebean.setAreacode(dMap.getValue("AREACODE", 0));
        }
        return sysCityMessagebean;
    }

    /**
     * 忘记密码 重置密码
     */
    @Override
    public MemberInfoMessageBean reSetPw(MemberInfoMessageBean memberInfoMessageBean) {
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(memberInfoMessageBean.getId());
        String password = memberInfoMessageBean.getPassword();
        if (password != null && !password.equals("")) {
            Encrypt encrypt = new Encrypt();
            password = encrypt.handleMd5(password);
        }
        memberInfo.setPassword(password);
        memberInfo.setPasswdFlg("0");
        DaoResult daoResult = securitySettingsDao.reSetPw(memberInfo);
        MemberInfoMessageBean result = new MemberInfoMessageBean();
        if (daoResult.getCode() < 0) {
            result.setInfo("更新失败");
            result.setResult("error");
        } else {
            result.setInfo("更新成功");
            result.setResult("success");
        }
        return result;
    }

    /**
     * 验证是否有此手机号
     */
    @Override
    public String checkIsTelephone(String telephone) {
        DMap dMap = securitySettingsDao.checkIsTelephone(telephone);
        if (dMap.getCount() > 0) {
            return dMap.getValue("ID", 0);
        }
        return null;
    }
}
