package com.mh.peer.service.product;

import com.mh.peer.model.business.ProductInvestBusinessBean;
import com.mh.peer.model.message.*;
import java.util.List;

/**
 * Created by zhangerxin on 2016-5-9.
 */
public interface ProductBuyService {

    /**
     * 新增投资部分
     * @param productInvestMes
     * @return
     */
    ProductInvestBusinessBean saveProductInvest(ProductInvestMessageBean productInvestMes);

    /**
     * 根据条件获取产品投资数量
     * @param productInvestMes
     * @return
     */
    int getCountBySome(ProductInvestMessageBean productInvestMes);

    /**
     * 产品投资列表部分
     * @param productInvestMes
     * @return
     */
    List<ProductInvestMessageBean> getProductInvestList(ProductInvestMessageBean productInvestMes);

    /**
     * 产品投资信息数量
     * @param productInvestMes
     * @return
     */
    int getProductInvestCount(ProductInvestMessageBean productInvestMes);

    /**
     * 根据id获取投资信息(zhangerxin)
     * @param productMyInvestMes
     * @return
     */
    ProductMyInvestMessageBean getInvestInfoById(ProductMyInvestMessageBean productMyInvestMes);

    /**
     * 根据条件获取回款金额总和(重做 by zhangerxin)
     * @param productReceiveMes
     * @return
     */
    public ProductReceiveMessageBean getReceivePriceSum(ProductReceiveMessageBean productReceiveMes);

    /**
     * 根据条件获取回款数量(重做 by zhangerxin)
     * @param productReceiveMes
     * @return
     */
    public String getReceiveCount(ProductReceiveMessageBean productReceiveMes);

    /**
     * 根据条件获取投资信息数量
     * @param productInvestMes
     * @return
     */
    public String getInvestCount(ProductInvestMessageBean productInvestMes);

    /**
     * 昨天新增投资数量 by cuibin
     */
    public String[] queryLastDayMoney();

    /**
     * 过去七天新增投资数量 by cuibin
     */
    public String[] queryLastWeekMoney();

    /**
     * 过去30天新增投资数量 by cuibin
     */
    public String[] queryLastMonthMoney();

    /**
     * 昨天投资金额 by cuibin
     */
    public String[] queryLastDayInvest();

    /**
     * 最近七天投资金额 by cuibin
     */
    public String[] queryLastWeekInvest();

    /**
     * 过去30天新增投资 by cuibin
     *
     */
    public String[] queryLastMonthInvest();

    /**
     * 获取投资项目(App部分)
     * @param memberId
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public List<AppProductBuyMessageBean> getInvestProject(String memberId,String typeId,String pageIndex,String pageSize);

    /**
     * 获取投资产品详细信息
     * @param productId
     * @return
     */
    public ProductInfoMessageBean queryProductDetails(String productId);

    /**
     * 获取已回款统计信息
     * @param productBuyId
     * @return
     */
    public ProductReceiveInfoMessageBean queryYReceiveInfo(String productBuyId);

    /**
     * 获取未还款统计信息
     * @param productBuyId
     * @return
     */
    public ProductReceiveInfoMessageBean queryWReceiveInfo(String productBuyId);
}
