package com.mh.peer.service.sys.impl;

import com.mh.peer.dao.manager.MessageSetDao;

import com.mh.peer.model.entity.SysMessageSetting;
import com.mh.peer.model.message.MessageSetMessageBean;
import com.mh.peer.service.sys.MessageSetService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cuibin on 2016/5/30.
 */
@Service
public class MessageSetServiceImpl implements MessageSetService{

    @Autowired
    private MessageSetDao messageSetDao;

    /**
     * 查询一个短信设置
     */
    @Override
    public MessageSetMessageBean queryOnlyMessage() {
        DMap dMap = messageSetDao.queryOnlyMessage();
        MessageSetMessageBean messageSetMessageBean = new MessageSetMessageBean();
        if(dMap.getCount() > 0){
            messageSetMessageBean.setId(dMap.getValue("ID",0));
            messageSetMessageBean.setUsername(dMap.getValue("USERNAME", 0));
            messageSetMessageBean.setPassword(dMap.getValue("PASSWORD", 0));
            messageSetMessageBean.setFlag(dMap.getValue("FLAG", 0));
        }
        return messageSetMessageBean;
    }

    /**
     * 更新短信设置
     */
    @Override
    public MessageSetMessageBean updateMessageSet(MessageSetMessageBean messageSetMessageBean) {
        SysMessageSetting sysMessageSetting = new SysMessageSetting();
        if(messageSetMessageBean != null){
            sysMessageSetting.setId(messageSetMessageBean.getId());
            sysMessageSetting.setPassword(messageSetMessageBean.getPassword());
            sysMessageSetting.setUsername(messageSetMessageBean.getUsername());
            sysMessageSetting.setFlag(messageSetMessageBean.getFlag());
        }
        DaoResult daoResult = messageSetDao.updateMessageSet(sysMessageSetting);
        MessageSetMessageBean messageSetMessageBean1 = new MessageSetMessageBean();
        if(daoResult.getCode() < 0){
            messageSetMessageBean1.setResult("error");
            messageSetMessageBean1.setInfo("更新失败");
        }else{
            messageSetMessageBean1.setResult("success");
            messageSetMessageBean1.setInfo("更新成功");
        }
        return messageSetMessageBean1;
    }
    /**
     * 读取开关
     */
    @Override
    public String loadSwitch() {
        DMap dMap = messageSetDao.queryOnlyMessage();
        if(dMap.getCount() > 0){
            return dMap.getValue("FLAG", 0);
        }
        return null;
    }
}
