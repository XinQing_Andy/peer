package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.TradeDetailDao;
import com.mh.peer.dao.member.AllMemberDao;
import com.mh.peer.model.message.AppTradeMessageBean;
import com.mh.peer.model.message.TradeDetailMessageBean;
import com.mh.peer.service.front.TradeDetailService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cuibin on 2016/6/20.
 */
@Service
public class TradeDetailServiceImpl implements TradeDetailService {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private TradeDetailDao tradeDetailDao;

    /**
     * 查询资金明细，重做by zhangerxin
     * @param tradeDetailMes
     * @return
     */
    @Override
    public List<TradeDetailMessageBean> queryTradeDetail(TradeDetailMessageBean tradeDetailMes) {
        DMap dMap = tradeDetailDao.queryTradeDetail(tradeDetailMes);
        List<TradeDetailMessageBean> listTradeDetailMes = new ArrayList<TradeDetailMessageBean>();
        String time = ""; //交易时间
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                TradeDetailMessageBean tradeDetailMessageBean = new TradeDetailMessageBean();
                tradeDetailMessageBean.setId(dMap.getValue("ID",i)); //主键

                time = dMap.getValue("TIME",i); //交易时间
                if (time != null && !time.equals("")){
                    time = time.substring(0, 10); //交易时间
                }
                tradeDetailMessageBean.setTime(time);

                tradeDetailMessageBean.setType(dMap.getValue("TYPE",i)); //交易类型(0-充值、1-提现、2-投资、3-回款、4-借款、5-还款、7-债权)
                tradeDetailMessageBean.setSerialnum(dMap.getValue("SERIALNUM",i)); //流水号
                tradeDetailMessageBean.setSprice(dMap.getValue("SPRICE",i)); //交易金额
                tradeDetailMessageBean.setBalance(dMap.getValue("BALANCE",i)); //账户余额
                listTradeDetailMes.add(tradeDetailMessageBean);
            }
        }
        return listTradeDetailMes;
    }


    /**
     * 获取交易记录(App端)
     * @param memberId
     * @return
     */
    @Override
    public List<AppTradeMessageBean> getTradeRecordList(String memberId,String pageIndex,String pageSize,String tradeType,String tradeTime) {
        List<AppTradeMessageBean> listAppTradeMes = new ArrayList<AppTradeMessageBean>();
        DMap dMap = tradeDetailDao.getTradeRecordList(memberId,pageIndex,pageSize,tradeType,tradeTime);
        String sprice = "0"; //实际金额
        String procedureFee = "0"; //手续费
        Double tradePrice = 0.0d; //交易金额
        String time = ""; //交易时间

        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                AppTradeMessageBean appTradeMessageBean = new AppTradeMessageBean();
                appTradeMessageBean.setId(dMap.getValue("ID",i)); //主键
                sprice = dMap.getValue("SPRICE",i); //实际金额
                procedureFee = dMap.getValue("PROCEDUREFEE",i); //手续费
                appTradeMessageBean.setSprice(sprice);
                appTradeMessageBean.setProcedureFee(procedureFee);
                if (sprice != null && !sprice.equals("") && procedureFee != null && !procedureFee.equals("")){
                    tradePrice = TypeTool.getDouble(sprice) + TypeTool.getDouble(procedureFee); //交易金额
                    appTradeMessageBean.setTradePrice(TypeTool.getString(tradePrice));
                }
                appTradeMessageBean.setMemberId(dMap.getValue("MEMBERID",i)); //会员id
                appTradeMessageBean.setType(dMap.getValue("TYPE",i)); //交易类型
                time = dMap.getValue("TIME",i); //交易时间
                if (time != null && !time.equals("")){
                    time = time.substring(0,10);
                }
                appTradeMessageBean.setTime(time); //交易时间
                listAppTradeMes.add(appTradeMessageBean);
            }
        }
        return listAppTradeMes;
    }


    /**
     * 查询交易记录数
     * @param tradeDetailMes
     * @return
     */
    @Override
    public int queryTradeDetailCount(TradeDetailMessageBean tradeDetailMes) {
        DMap dMap = tradeDetailDao.queryTradeDetailCount(tradeDetailMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }
}
