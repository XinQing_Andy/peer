package com.mh.peer.service.huanxun;

import com.mh.peer.model.entity.HuanxunAutosign;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.salon.frame.data.DMap;

/**
 * Created by Cuibin on 2016/8/4.
 */
public interface HuanXunMotionSignService {

    /**
     * 查询环迅注册记录
     * @param memberId
     * @return
     */
    public HuanXunRegistMessageBean queryHuanXunReg(String memberId);

    /**
     * 保存自动签约返回信息
     */
    public boolean saveMotionSignResult(HuanxunAutosign huanxunAutosign);

    /**
     * 查询自动签约返回信息是否存在
     */
    public boolean queryIsResult(String memberId,String signedType);

}
