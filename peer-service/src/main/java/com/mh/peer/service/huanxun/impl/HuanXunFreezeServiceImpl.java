package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunFreezeDao;
import com.mh.peer.model.entity.HuanxunFreezeAttorn;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.mh.peer.service.huanxun.HuanXunFreezeService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cuibin on 2016/7/29.
 */
@Service
public class HuanXunFreezeServiceImpl implements HuanXunFreezeService {

    @Autowired
    private HuanXunFreezeDao huanXunFreezeDao;

    /**
     * 查询冻结金额
     */
    @Override
    public String queryFreeze(String memberId) {
        DMap dMap = huanXunFreezeDao.queryFreeze(memberId);
        String ipsTrdAmt = "0"; //冻结金额
        if (dMap != null && dMap.getCount() > 0){
            ipsTrdAmt = dMap.getValue("IPSTRDAMT",0); //冻结金额总和
        }
        return ipsTrdAmt;
    }


    /**
     * 产品投资部分
     *
     * @param huanXunFreezeMes
     * @return
     */
    @Override
    public List<HuanXunFreezeMessageBean> getHuanXunFreezeInfoList(HuanXunFreezeMessageBean huanXunFreezeMes) {
        List<HuanXunFreezeMessageBean> listHuanXunFreezeMes = new ArrayList<HuanXunFreezeMessageBean>();
        DMap dMap = huanXunFreezeDao.getHuanXunFreezeInfoList(huanXunFreezeMes);
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                HuanXunFreezeMessageBean huanXunFreezeMessageBean = new HuanXunFreezeMessageBean();
                huanXunFreezeMessageBean.setProjectNo(dMap.getValue("PROJECTNO", i)); //项目id号
                huanXunFreezeMessageBean.setIpsAcctNo(dMap.getValue("OUTIPSACCTNO", i)); //转出方IPS账号
                huanXunFreezeMessageBean.setOtherIpsAcctNo(dMap.getValue("INIPSACCTNO", i)); //转入方IPS账号
                listHuanXunFreezeMes.add(huanXunFreezeMessageBean);
            }
        }
        return listHuanXunFreezeMes;
    }

    /**
     * 查询此项目所有投资记录 bycuibin
     */
    @Override
    public List<HuanXunFreezeMessageBean> queryAllfreeze(String productId) {
        DMap dMap = huanXunFreezeDao.queryAllfreeze(productId);
        List<HuanXunFreezeMessageBean> list = new ArrayList<HuanXunFreezeMessageBean>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                HuanXunFreezeMessageBean temp = new HuanXunFreezeMessageBean();
                temp.setProjectNo(dMap.getValue("PROJECTNO", i));
                temp.setIpsBillNo(dMap.getValue("IPSBILLNO", i));
                temp.setIpsAcctNo(dMap.getValue("OUTIPSACCTNO", i));
                temp.setTrdAmt(dMap.getValue("IPSTRDAMT", i));
                temp.setId(dMap.getValue("ID", i));
                list.add(temp);
            }

        }
        return list;
    }


    /**
     * 按照冻结表id查询记录 bycuibin
     */
    @Override
    public HuanXunFreezeMessageBean queryOnlyfreeze(String id) {
        DMap dMap = huanXunFreezeDao.queryOnlyfreeze(id);
        HuanXunFreezeMessageBean huanXunFreezeMes = new HuanXunFreezeMessageBean();
        if (dMap.getCount() > 0) {
            huanXunFreezeMes.setResultCode(dMap.getValue("RESULTCODE",0)); //响应码
            huanXunFreezeMes.setTrdAmt(dMap.getValue("IPSTRDAMT", 0)); //冻结金额
            huanXunFreezeMes.setMemberId(dMap.getValue("MEMBERID", 0)); //会员id
            huanXunFreezeMes.setId(dMap.getValue("ID", 0)); //主键
            huanXunFreezeMes.setFlag(dMap.getValue("FLAG", 0)); //状态
        }

        return huanXunFreezeMes;
    }


    /**
     * 更新环迅冻结状态 bycuibin
     */
    @Override
    public boolean updateHuanXunFreeze(String id) {
        DaoResult daoResult = huanXunFreezeDao.updateHuanXunFreeze(id);
        if (daoResult.getCode() < 0) {
            return false;
        }
        return true;
    }

    /**
     * 更新债权转让冻结状态 bycuibin
     */
    @Override
    public boolean updateUnfreezeAttorn(String id) {
        DaoResult daoResult = huanXunFreezeDao.updateUnfreezeAttorn(id);
        if (daoResult.getCode() < 0) {
            return false;
        }
        return true;
    }


    /**
     * 更改产品审核状态 bycuibin
     */
    @Override
    public boolean updateProductFlag(String productId) {
        DaoResult daoResult = huanXunFreezeDao.updateProductFlag(productId);
        if (daoResult.getCode() < 0) {
            return false;
        }
        return true;
    }

    /**
     * 按照id查询债权转让记录 bycuibin
     */
    @Override
    public HuanXunFreezeMessageBean queryOnlyFreezeAttorn(String attornid) {
        DMap dMap = huanXunFreezeDao.queryOnlyFreezeAttorn(attornid);
        HuanXunFreezeMessageBean temp = new HuanXunFreezeMessageBean();
        if (dMap.getCount() > 0) {
            temp.setProjectNo(dMap.getValue("PROJECTNO", 0));
            temp.setIpsBillNo(dMap.getValue("IPSBILLNO", 0));
            temp.setIpsAcctNo(dMap.getValue("IPSACCTNO", 0));
            temp.setTrdAmt(dMap.getValue("IPSTRDAMT", 0));
            temp.setId(dMap.getValue("ID", 0));
            temp.setFlag(dMap.getValue("FLAG", 0));
            temp.setMemberId(dMap.getValue("MEMBERID", 0));

        }
        return temp;
    }


    /**
     * 查询债权转让冻结记录 bycuibin，修改by zhangerxin
     */
    @Override
    public HuanXunFreezeMessageBean queryFreezeAttorn(String id) {
        DMap dMap = huanXunFreezeDao.queryFreezeAttorn(id);
        HuanXunFreezeMessageBean huanXunFreezeMessageBean = new HuanXunFreezeMessageBean();
        if (dMap.getCount() > 0) {
            huanXunFreezeMessageBean.setProjectNo(dMap.getValue("PROJECTNO",0)); //项目ID号
            huanXunFreezeMessageBean.setIpsBillNo(dMap.getValue("IPSBILLNO",0)); //ips冻结订单号
            huanXunFreezeMessageBean.setIpsAcctNo(dMap.getValue("IPSACCTNO",0)); //转入方IPS账号
            huanXunFreezeMessageBean.setTrdAmt(dMap.getValue("IPSTRDAMT",0)); //冻结金额
            huanXunFreezeMessageBean.setId(dMap.getValue("ID",0)); //冻结表主键
            huanXunFreezeMessageBean.setAttornId(dMap.getValue("ATTORNID",0)); //债权id
            huanXunFreezeMessageBean.setBuyId(dMap.getValue("PRODUCTBUYID",0)); //产品购买id
            huanXunFreezeMessageBean.setFlag(dMap.getValue("FLAG",0)); //状态(0-冻结、1-解冻、2-转账)
            huanXunFreezeMessageBean.setMemberId(dMap.getValue("MEMBERID",0)); //会员id
        }
        return huanXunFreezeMessageBean;
    }


    /**
     * 查询债权转让记录 bycuibin
     */
    @Override
    public int queryFreezeAttornCount(String id) {
        DMap dMap = huanXunFreezeDao.queryFreezeAttornCount(id);
        int count = dMap.getCount();
        return count;
    }


    /**
     * 冻结还款部分信息查询
     * @param freezeRepayId
     * @return
     */
    @Override
    public HuanXunFreezeMessageBean queryFreezeRepay(String freezeRepayId) {
        DMap dMap = huanXunFreezeDao.queryFreezeRepay(freezeRepayId);
        HuanXunFreezeMessageBean huanXunFreezeMessageBean = new HuanXunFreezeMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            huanXunFreezeMessageBean.setProductId(dMap.getValue("PRODUCTID",0)); //产品id
            huanXunFreezeMessageBean.setIpsBillNo(dMap.getValue("IPSBILLNO", 0)); //冻结账号
            huanXunFreezeMessageBean.setyRepaymentTime(dMap.getValue("YREPAYMENTTIME",0)); //应还款日期
            huanXunFreezeMessageBean.setProjectNo(dMap.getValue("PROJECTNO",0)); //项目ID号
            huanXunFreezeMessageBean.setRepayId(dMap.getValue("REPAYID",0)); //还款id
            huanXunFreezeMessageBean.setIpsAcctNo(dMap.getValue("IPSACCTNO",0)); //冻结账号
        }
        return huanXunFreezeMessageBean;
    }
}
