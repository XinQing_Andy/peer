package com.mh.peer.service.member.impl;

import com.mh.peer.dao.member.AllMemberDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.DateHandle;
import com.mh.peer.util.Encrypt;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by cuibin on 2016/4/20.
 */
@Service
public class AllMemberServiceImpl implements AllMemberService {
    @Autowired
    private AllMemberDao allMemberDao;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 根据分页查询会员(By Zhangerxin)
     *
     * @param memberInfoMes
     * @return
     */
    @Override
    public List<MemberInfoMessageBean> queryMemberBysome(MemberInfoMessageBean memberInfoMes) {
        List<MemberInfoMessageBean> memberInfoMessageBeans = new ArrayList<MemberInfoMessageBean>();
        DMap parm = allMemberDao.queryMemberBySome(memberInfoMes); //查询产品
        if (parm.getCount() > 0) {
            for (int i = 0; i < parm.getCount(); i++) {
                MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
                memberInfoMessageBean.setId(parm.getValue("ID", i)); //主键
                memberInfoMessageBean.setUsername(parm.getValue("USERNAME", i)); //用户名
                memberInfoMessageBean.setNickname(parm.getValue("NICKNAME", i)); //昵称
                memberInfoMessageBean.setRealname(parm.getValue("REALNAME", i)); //真实姓名
                memberInfoMessageBean.setPassword(parm.getValue("PASSWORD", i)); //密码
                memberInfoMessageBean.setIdcard(parm.getValue("IDCARD", i)); //身份证号
                memberInfoMessageBean.setTelephone(parm.getValue("TELEPHONE", i)); //手机号
                memberInfoMessageBean.setEmail(parm.getValue("EMAIL", i)); //邮箱地址
                memberInfoMessageBean.setSex(parm.getValue("EMAIL", i)); //性别(0-男、1-女)
                memberInfoMessageBean.setBirthday(parm.getValue("BIRTHDAY", i)); //出生日期
                memberInfoMessageBean.setCity(parm.getValue("CITY", i)); //城市id
                memberInfoMessageBean.setDetailAddress(parm.getValue("DETAIL_ADDRESS", i)); //详细地址
                memberInfoMessageBean.setDregree(parm.getValue("DREGREE", i)); //学历(0-小学、1-初中、2-高中、3-大专、4-本科、5-硕士、6-博士、7-博士后、8-其他)
                memberInfoMessageBean.setSchool(parm.getValue("SCHOOL", i)); //毕业院校
                memberInfoMessageBean.setMarryStatus(parm.getValue("MARRY_STATUS", i)); //婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶)
                memberInfoMessageBean.setChildren(parm.getValue("CHILDREN", i)); //子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上)
                memberInfoMessageBean.setMonthSalary(parm.getValue("MONTH_SALARY", i));//月收入(0-2000元以下、1-2000元-5000元、2-5000元-10000元、3-10000元以上)
                memberInfoMessageBean.setSocialSecurity(parm.getValue("SOCIAL_SECURITY", i));//社保情况(0-无、1-未缴满6个月、2-缴满6个月以上)
                memberInfoMessageBean.setHouseCondition(parm.getValue("HOUSE_CONDITION", i));//住房条件(0-有商品房(无贷款)、1-有商品房(有贷款)、2-有其他(非商品)房、3-与父母同住、4-租房)
                memberInfoMessageBean.setCarFlg(parm.getValue("CAR_FLG", i));//是否有车(0-有、1-无)
                memberInfoMessageBean.setRealFlg(parm.getValue("REAL_FLG", i));//是否实名认证
                memberInfoMessageBean.setPasswdFlg(parm.getValue("PASSWD_FLG", i));//是否更改密码
                memberInfoMessageBean.setTraderpassword(parm.getValue("TRADERPASSWORD", i));//交易密码
                memberInfoMessageBean.setTelephoneFlg(parm.getValue("MAIL_FLG", i));//是否手机认证(0-是、1-否)
                memberInfoMessageBean.setMailFlg(parm.getValue("MAIL_FLG", i));//是否邮件认证
                memberInfoMessageBean.setLockFlg(parm.getValue("LOCK_FLG", i));//是否锁定
                memberInfoMessageBean.setDelFlg(parm.getValue("DEL_FLG", i));//是否删除
                memberInfoMessageBean.setCreatetime(parm.getValue("CREATETIME", i));//创建时间
                memberInfoMessageBeans.add(memberInfoMessageBean);
            }
        }
        return memberInfoMessageBeans;
    }

    /**
     * 查询会员数量
     */
    @Override
    public int queryCountMember() {
        return allMemberDao.queryCountMember().getCount();
    }

    /**
     * 查询所有会员 数量
     */
    @Override
    public int queryAllMemberCount(MemberInfoMessageBean memberInfoMessageBean) {
        return allMemberDao.queryAllMemberCount(memberInfoMessageBean).getCount();
    }

    /**
     * 查询所有会员
     */
    @Override
    public List<MemberInfoMessageBean> queryAllMember(MemberInfoMessageBean memberInfoMes) {
        List<MemberInfoMessageBean> memberInfoMessageBeans = new ArrayList<MemberInfoMessageBean>();
        DMap parm = allMemberDao.queryAllMember(memberInfoMes); //查询产品
        if (parm.getCount() > 0) {
            for (int i = 0; i < parm.getCount(); i++) {
                MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
                memberInfoMessageBean.setId(parm.getValue("ID", i)); //主键
                memberInfoMessageBean.setUsername(parm.getValue("USERNAME", i)); //用户名
                memberInfoMessageBean.setNickname(parm.getValue("NICKNAME", i)); //昵称
                memberInfoMessageBean.setRealname(parm.getValue("REALNAME", i)); //真实姓名
                memberInfoMessageBean.setPassword(parm.getValue("PASSWORD", i)); //密码
                memberInfoMessageBean.setIdcard(parm.getValue("IDCARD", i)); //身份证号
                memberInfoMessageBean.setTelephone(parm.getValue("TELEPHONE", i)); //手机号
                memberInfoMessageBean.setEmail(parm.getValue("EMAIL", i)); //邮箱地址
                memberInfoMessageBean.setSex(parm.getValue("SEX", i)); //性别(0-男、1-女)
                memberInfoMessageBean.setBirthday(parm.getValue("BIRTHDAY", i)); //出生日期
                memberInfoMessageBean.setCity(parm.getValue("CITY", i)); //城市id
                memberInfoMessageBean.setDetailAddress(parm.getValue("DETAIL_ADDRESS", i)); //详细地址
                memberInfoMessageBean.setDregree(parm.getValue("DREGREE", i)); //学历(0-小学、1-初中、2-高中、3-大专、4-本科、5-硕士、6-博士、7-博士后、8-其他)
                memberInfoMessageBean.setSchool(parm.getValue("SCHOOL", i)); //毕业院校
                memberInfoMessageBean.setMarryStatus(parm.getValue("MARRY_STATUS", i)); //婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶)
                memberInfoMessageBean.setChildren(parm.getValue("CHILDREN", i)); //子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上)
                memberInfoMessageBean.setMonthSalary(parm.getValue("MONTH_SALARY", i));//月收入(0-2000元以下、1-2000元-5000元、2-5000元-10000元、3-10000元以上)
                memberInfoMessageBean.setSocialSecurity(parm.getValue("SOCIAL_SECURITY", i));//社保情况(0-无、1-未缴满6个月、2-缴满6个月以上)
                memberInfoMessageBean.setHouseCondition(parm.getValue("HOUSE_CONDITION", i));//住房条件(0-有商品房(无贷款)、1-有商品房(有贷款)、2-有其他(非商品)房、3-与父母同住、4-租房)
                memberInfoMessageBean.setCarFlg(parm.getValue("CAR_FLG", i));//是否有车(0-有、1-无)
                memberInfoMessageBean.setRealFlg(parm.getValue("REAL_FLG", i));//是否实名认证
                memberInfoMessageBean.setPasswdFlg(parm.getValue("PASSWD_FLG", i));//是否更改密码
                memberInfoMessageBean.setTraderpassword(parm.getValue("TRADERPASSWORD", i));//交易密码
                memberInfoMessageBean.setTelephoneFlg(parm.getValue("TELEPHONE_FLG", i));//是否手机认证(0-是、1-否)
                memberInfoMessageBean.setMailFlg(parm.getValue("MAIL_FLG", i));//是否邮件认证
                memberInfoMessageBean.setLockFlg(parm.getValue("LOCK_FLG", i));//是否锁定
                memberInfoMessageBean.setDelFlg(parm.getValue("DEL_FLG", i));//是否删除
                String createtime = parm.getValue("CREATETIME", i).substring(0, 10);
                memberInfoMessageBean.setCreatetime(createtime);//创建时间

                memberInfoMessageBean.setPoints(parm.getValue("POINTS", i));//积分
                memberInfoMessageBean.setInvestcount(parm.getValue("INVESTCOUNT", i));//累计投标数量
                memberInfoMessageBean.setInvestsum(parm.getValue("INVESTSUM", i));//累计投标金额

                memberInfoMessageBeans.add(memberInfoMessageBean);
            }
        }
        return memberInfoMessageBeans;
    }

    /**
     * 查询一个 会员
     */
    @Override
    public MemberInfoMessageBean queryOnlyMember(MemberInfoMessageBean mimb) {
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        DMap parm = allMemberDao.queryOnlyMember(mimb.getId());

        if (parm != null && parm.getCount() > 0) {
            memberInfoMessageBean.setId(parm.getValue("ID", 0));
            memberInfoMessageBean.setUsername(parm.getValue("USERNAME", 0)); //用户名
            memberInfoMessageBean.setNickname(parm.getValue("NICKNAME", 0)); //昵称
            memberInfoMessageBean.setRealname(parm.getValue("REALNAME", 0)); //真实姓名
            memberInfoMessageBean.setPassword(parm.getValue("PASSWORD", 0)); //密码
            memberInfoMessageBean.setIdcard(parm.getValue("IDCARD", 0)); //身份证号
            memberInfoMessageBean.setTelephone(parm.getValue("TELEPHONE", 0)); //手机号
            memberInfoMessageBean.setEmail(parm.getValue("EMAIL", 0)); //邮箱地址
            memberInfoMessageBean.setSex(parm.getValue("SEX", 0)); //性别(0-男、1-女)
            memberInfoMessageBean.setBirthday(parm.getValue("BIRTHDAY", 0)); //出生日期
            memberInfoMessageBean.setCity(parm.getValue("CITY", 0)); //城市id
            memberInfoMessageBean.setDetailAddress(parm.getValue("DETAIL_ADDRESS", 0)); //详细地址
            memberInfoMessageBean.setDregree(parm.getValue("DREGREE", 0)); //学历(0-小学、1-初中、2-高中、3-大专、4-本科、5-硕士、6-博士、7-博士后、8-其他)
            memberInfoMessageBean.setSchool(parm.getValue("SCHOOL", 0)); //毕业院校
            memberInfoMessageBean.setMarryStatus(parm.getValue("MARRY_STATUS", 0)); //婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶)
            memberInfoMessageBean.setChildren(parm.getValue("CHILDREN", 0)); //子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上)
            memberInfoMessageBean.setMonthSalary(parm.getValue("MONTH_SALARY", 0));//月收入(0-2000元以下、1-2000元-5000元、2-5000元-10000元、3-10000元以上)
            memberInfoMessageBean.setSocialSecurity(parm.getValue("SOCIAL_SECURITY", 0));//社保情况(0-无、1-未缴满6个月、2-缴满6个月以上)
            memberInfoMessageBean.setHouseCondition(parm.getValue("HOUSE_CONDITION", 0));//住房条件(0-有商品房(无贷款)、1-有商品房(有贷款)、2-有其他(非商品)房、3-与父母同住、4-租房)
            memberInfoMessageBean.setCarFlg(parm.getValue("CAR_FLG", 0));//是否有车(0-有、1-无)
            memberInfoMessageBean.setRealFlg(parm.getValue("REAL_FLG", 0));//是否实名认证
            memberInfoMessageBean.setPasswdFlg(parm.getValue("PASSWD_FLG", 0));//是否更改密码
            memberInfoMessageBean.setTraderpassword(parm.getValue("TRADERPASSWORD", 0));//交易密码
            memberInfoMessageBean.setTelephoneFlg(parm.getValue("TELEPHONE_FLG", 0));//是否手机认证(0-是、1-否)
            memberInfoMessageBean.setMailFlg(parm.getValue("MAIL_FLG", 0));//是否邮件认证
            memberInfoMessageBean.setLockFlg(parm.getValue("LOCK_FLG", 0));//是否锁定
            memberInfoMessageBean.setDelFlg(parm.getValue("DEL_FLG", 0));//是否删除
            memberInfoMessageBean.setBalance(parm.getValue("BALANCE", 0));//账户余额
            memberInfoMessageBean.setPoints(parm.getValue("POINTS", 0));//积分
            memberInfoMessageBean.setCurrentDate(parm.getValue("CURRENTDATE", 0));//当前mysql日期
            memberInfoMessageBean.setManagerate(parm.getValue("MANAGERATE", 0));//管理费率
            memberInfoMessageBean.setInterests(parm.getValue("INTERESTS", 0));//累计金额收益
            memberInfoMessageBean.setReceivedPrice(parm.getValue("RECEIVEDPRICE", 0));//待收总额
            memberInfoMessageBean.setRepayedPrice(parm.getValue("REPAYEDPRICE", 0));//待还总额
            memberInfoMessageBean.setCreatetime(parm.getValue("CREATETIME", 0));//创建时间
            memberInfoMessageBean.setProvince(parm.getValue("PROVINCE", 0));//省
            memberInfoMessageBean.setYear(parm.getValue("YEAR", 0));//mysql系统年
            memberInfoMessageBean.setMonth(parm.getValue("MONTH", 0));//mysql系统月
            memberInfoMessageBean.setInfo("查询成功");
            memberInfoMessageBean.setResult("success");
        }
        return memberInfoMessageBean;
    }


    /**
     * 查询一个 会员余额
     */
    @Override
    public String queryOnlyMemberBalance(String memberId) {
        DMap dMap = allMemberDao.queryOnlyMember(memberId);
        String balance = "0"; //余额
        if (dMap != null && dMap.getCount() > 0) {
            balance = dMap.getValue("BALANCE",0); //余额
        }
        return balance;
    }


    /**
     * 根据id获取会员信息
     *
     * @param id
     * @return
     */
    @Override
    public MemberInfoMessageBean queryMemberInfoById(String id) {
        DMap dMap = allMemberDao.queryMemberInfoById(id);
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        if (dMap != null) {
            memberInfoMessageBean.setId(dMap.getValue("ID", 0)); //主键
            memberInfoMessageBean.setUsername(dMap.getValue("USERNAME", 0)); //用户名
            memberInfoMessageBean.setIdcard(dMap.getValue("IDCARD", 0)); //身份证号
            memberInfoMessageBean.setRealname(dMap.getValue("REALNAME", 0)); //真实姓名
            memberInfoMessageBean.setTelephone(dMap.getValue("TELEPHONE", 0)); //手机号
            memberInfoMessageBean.setBalance(dMap.getValue("BALANCE", 0)); //余额
        }
        return memberInfoMessageBean;
    }


    /**
     * 锁定 会员
     */
    @Override
    public MemberInfoMessageBean lockMember(MemberInfoMessageBean memberInfoMessageBean) {
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(memberInfoMessageBean.getId());
        memberInfo.setLockFlg(memberInfoMessageBean.getLockFlg());
        if (memberInfoMessageBean.getBalance() != null) {
            memberInfo.setBalance(Double.parseDouble(memberInfoMessageBean.getBalance()));
        }
        DaoResult result = allMemberDao.lockMember(memberInfo);
        if (result.getCode() < 0) {
            memberInfoMessageBean.setResult("error");
            memberInfoMessageBean.setInfo("操作失败");
        } else {
            memberInfoMessageBean.setResult("success");
            memberInfoMessageBean.setInfo("操作成功");
        }
        return memberInfoMessageBean;
    }


    /**
     * 更新密码
     */
    @Override
    public MemberInfoMessageBean updateMemberPassWord(MemberInfoMessageBean memberInfoMessageBean) {
        MemberInfo memberInfo = new MemberInfo();
        if (memberInfoMessageBean != null) {
            memberInfo.setId(memberInfoMessageBean.getId());
            memberInfo.setPasswdFlg("0");
            String pwd = memberInfoMessageBean.getPassword();
            Encrypt encrypt = new Encrypt();
            pwd = encrypt.handleMd5(pwd);
            memberInfo.setPassword(pwd);
        }
        DaoResult daoResult = allMemberDao.updateMemberPassWord(memberInfo);
        MemberInfoMessageBean memberInfoMessageBean1 = new MemberInfoMessageBean();
        if (daoResult.getCode() < 0) {
            memberInfoMessageBean1.setResult("error");
            memberInfoMessageBean1.setInfo("修改失败");
        } else {
            memberInfoMessageBean1.setResult("success");
            memberInfoMessageBean1.setInfo("修改成功");
        }
        return memberInfoMessageBean1;
    }

    /**
     * 更新信息 by zhangerxin
     *
     * @param memberInfoMessageBean
     * @return
     */
    @Override
    public MemberInfoMessageBean updateMemberInfo(MemberInfoMessageBean memberInfoMessageBean) {
        MemberInfo memberInfo = new MemberInfo();
        if (memberInfoMessageBean != null) {
            memberInfo.setId(memberInfoMessageBean.getId()); //主键
            memberInfo.setBalance(TypeTool.getDouble(memberInfoMessageBean.getBalance())); //余额
        }
        DaoResult result = allMemberDao.updateMemberInfo(memberInfo);
        return memberInfoMessageBean;
    }

    /**
     * 查询所有理财集合
     * @param financingMes
     * @return
     */
    @Override
    public List<FinancingMessageBean> queryAllManageMoney(FinancingMessageBean financingMes) {
        DMap dMap = allMemberDao.queryAllManageMoney(financingMes);
        List<FinancingMessageBean> listFinancingMes = new ArrayList<FinancingMessageBean>();
        int num = 1; //排名
        String pageIndex = financingMes.getPageIndex(); //起始数
        if (pageIndex != null && !pageIndex.equals("")){
            num = TypeTool.getInt(pageIndex);
        }

        if (dMap != null && dMap.getCount() > 0) {
            for (int i=0;i<dMap.getCount();i++) {
                FinancingMessageBean financingMessageBean = new FinancingMessageBean();
                financingMessageBean.setMemberId(dMap.getValue("ID",i)); //会员id
                financingMessageBean.setUserName(dMap.getValue("USERNAME",i)); //用户名
                financingMessageBean.setNickName(dMap.getValue("NICKNAME",i)); //昵称
                financingMessageBean.setInvestPrice(dMap.getValue("INVESTPRICE",i)); //投资金额
                financingMessageBean.setNum(TypeTool.getString(num));
                listFinancingMes.add(financingMessageBean);
                num++;
            }
        }
        return listFinancingMes;
    }


    /**
     * 查询所有理财数量
     * @param financingMes
     * @return
     */
    @Override
    public int queryAllManageMoneyCount(FinancingMessageBean financingMes) {
        DMap dMap = allMemberDao.queryAllManageMoneyCount(financingMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 后台首页查询新增会员 昨天
     */
    public String[] queryYesterdayReg() {
        DMap dMap = allMemberDao.queryYesterdayReg();

        String[] arr = new String[12];
        List<String> sumList = new ArrayList<String>();
        List<String> regTimeList = new ArrayList<String>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                sumList.add(dMap.getValue("REGSUM", i)); //注册总数
                regTimeList.add(dMap.getValue("REGTIME", i)); //注册时间
            }
        }

        int temp = 0;
        int index = 0;
        for (int j = 0; j < arr.length; j++) {
            boolean mes = true;
            for (int i = 0; i < regTimeList.size(); i++) {
                temp = Integer.parseInt(regTimeList.get(i).substring(11, 13));
                if (temp >= index && temp < index + 2 && temp < 22) {
                    if (arr[j] != null) {
                        int sum = Integer.parseInt(arr[j]) + Integer.parseInt(sumList.get(i));
                        arr[j] = sum + "";
                    } else {
                        arr[j] = sumList.get(i);
                    }
                    mes = false;
                } else if (temp >= 22) {
                    if (arr[j] != null) {
                        int sum = Integer.parseInt(arr[j]) + Integer.parseInt(sumList.get(i));
                        arr[j] = sum + "";
                    } else {
                        arr[j] = sumList.get(i);
                    }
                    mes = false;
                }
            }
            if (mes) {
                arr[j] = "0";
            }
            index = index + 2;
        }

        return arr;
    }

    /**
     * 后台首页查询新增会员 过去7天
     */
    @Override
    public String[] queryLastWeekReg() {
        String[] arr = new String[7];
        int day = -7;
        GregorianCalendar cal = new GregorianCalendar();
        for (int i = 0; i < 7; i++) {
            cal.setTime(new Date());
            cal.add(Calendar.DATE, day);
            String time = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            arr[i] = allMemberDao.queryRegMemnerByDate(time).getValue("ZS", 0);
            day += 1;
        }

        return arr;
    }

    /**
     * 后台首页查询新增会员 过去30天
     */
    @Override
    public String[] queryLastMonthReg() {

        String[] arr = new String[7];
        arr[0] = "0";
        int day = -31;
        GregorianCalendar cal = new GregorianCalendar();
        for (int i = 1; i < 7; i++) {
            cal.setTime(new Date());
            cal.add(Calendar.DATE, day);
            String startTime = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            cal.setTime(new Date());
            cal.add(Calendar.DATE, day + 5);
            String endTime = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            arr[i] = allMemberDao.queryRegMemnerByDate(startTime, endTime).getValue("ZS", 0);
            day += 5;
        }

        return arr;
    }

    /**
     * 求自定义天数之前 到今天的日期 by cuibin
     */
    @Override
    public String[] queryDate(int day) {
        String[] arr = new String[day];
        int ago = -day;
        for (int i = 0; i < day; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, ago);
            arr[i] = new SimpleDateFormat("MM月dd日").format(cal.getTime());
            ago++;
        }

        return arr;
    }

    /**
     * 求最近30天日期 by cuibin
     */
    @Override
    public String[] queryDateMonth() {
        String[] arr = new String[7];
        int ago = -31;
        for (int i = 0; i < 7; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, ago);
            arr[i] = new SimpleDateFormat("MM月dd日").format(cal.getTime());
            ago += 5;
        }

        return arr;
    }


    /**
     * 获取我的账户信息
     *
     * @param myAccountMes
     * @return
     */
    @Override
    public MyAccountMessageBean getMemberInfo(MyAccountMessageBean myAccountMes) {
        DMap dMap = allMemberDao.getMemberInfo(myAccountMes);
        MyAccountMessageBean myAccountMessageBean = new MyAccountMessageBean();
        DateHandle dateHandle = new DateHandle(); //时间处理
        String nowTime = dateHandle.getNowTime(); //获取当前时间
        String manageRate = ""; //管理费率

        if (dMap != null && dMap.getCount() > 0) {
            myAccountMessageBean.setId(dMap.getValue("ID", 0)); //主键
            myAccountMessageBean.setNickname(dMap.getValue("NICKNAME", 0)); //昵称
            myAccountMessageBean.setPoints(dMap.getValue("POINTS", 0)); //积分
            myAccountMessageBean.setBalance(dMap.getValue("BALANCE", 0)); //余额
            myAccountMessageBean.setRealFlg(dMap.getValue("REAL_FLG", 0)); //是否实名化认证(0-是、1-否)
            myAccountMessageBean.setTraderPassword(dMap.getValue("TRADERPASSWORD", 0)); //交易密码
            myAccountMessageBean.setTelephoneFlg(dMap.getValue("TELEPHONE_FLG", 0)); //是否手机认证(0-是、1-否)
            myAccountMessageBean.setMailFlg(dMap.getValue("MAIL_FLG", 0)); //是否邮件认证(0-是、1-否)
            myAccountMessageBean.setNowTime(nowTime); //当前时间

            manageRate = dMap.getValue("MANAGERATE", 0); //管理费率
            if (manageRate != null && !manageRate.equals("")) {
                manageRate = manageRate + "%";
                myAccountMessageBean.setManageRate(manageRate);
            }

            myAccountMessageBean.setDsPrice(dMap.getValue("DSPRICE", 0)); //待收总额
            myAccountMessageBean.setDhPrice(dMap.getValue("DHPRICE", 0)); //待还总额
            myAccountMessageBean.setAccountPrice(dMap.getValue("ACCOUNTPRICE", 0)); //账户净资产
            myAccountMessageBean.setInvestTotal(dMap.getValue("INVESTCOUNT", 0)); //投资数量
            myAccountMessageBean.setAttornTotal(dMap.getValue("ATTORNCOUNT", 0)); //债权转让数量
            myAccountMessageBean.setProductTotal(dMap.getValue("PRODUCTCOUNT", 0)); //借款数量
            myAccountMessageBean.setCreateTime(dMap.getValue("CREATETIME", 0)); //创建时间
        }
        return myAccountMessageBean;
    }


    /**
     * 获取会员信息(App端)
     *
     * @param memberId
     * @return
     */
    @Override
    public AppMemberInfoMessageBean getAppMemberInfo(String memberId) {
        AppMemberInfoMessageBean appMemberInfoMessageBean = new AppMemberInfoMessageBean();
        DMap dMap = allMemberDao.getAppMemberInfo(memberId);
        if (dMap != null) {
            appMemberInfoMessageBean.setId(dMap.getValue("ID", 0)); //主键
            appMemberInfoMessageBean.setNickName(dMap.getValue("NICKNAME", 0)); //昵称
            appMemberInfoMessageBean.setPoints(dMap.getValue("POINTS", 0)); //积分
            appMemberInfoMessageBean.setBalance(dMap.getValue("BALANCE", 0)); //余额
            appMemberInfoMessageBean.setInterest(dMap.getValue("INTEREST", 0)); //累计收益
            appMemberInfoMessageBean.setReceivedPrice(dMap.getValue("RECEIVEDPRICE", 0)); //待收总额
            appMemberInfoMessageBean.setInvestCount(dMap.getValue("INVESTCOUNT", 0)); //投资笔数
            appMemberInfoMessageBean.setRepayedPrice(dMap.getValue("REPAYEDPRICE", 0)); //需还款金额
            appMemberInfoMessageBean.setAttornCount(dMap.getValue("ATTORNCOUNT", 0)); //发起转让项目数量
        }
        return appMemberInfoMessageBean;
    }


    /**
     * 资产统计部分(App端)
     *
     * @param memberId
     * @return
     */
    @Override
    public AppMyAccountMessageBean getPriceTj(String memberId) {
        AppMyAccountMessageBean appMyAccountMessageBean = new AppMyAccountMessageBean();
        DMap dMap = allMemberDao.getPriceTj(memberId);
        if (dMap != null) {
            appMyAccountMessageBean.setId(dMap.getValue("ID", 0)); //主键
            appMyAccountMessageBean.setBalance(dMap.getValue("BALANCE", 0)); //余额
            appMyAccountMessageBean.setReceivedPrice(dMap.getValue("RECEIVEDPRICE", 0)); //待收总额
            appMyAccountMessageBean.setAccountPrice(dMap.getValue("ACCOUNTPRICE", 0)); //账户总资产
        }
        return appMyAccountMessageBean;
    }


    /**
     * 投资实力榜(App端)
     *
     * @param pageIndex
     * @param pageSize
     * @param orderColumn
     * @param orderType
     * @return
     */
    @Override
    public List<AppMemberInvestMessageBean> getMemberByInvest(String pageIndex, String pageSize, String orderColumn, String orderType) {
        DMap dMap = allMemberDao.getMemberByInvest(pageIndex, pageSize, orderColumn, orderType);
        List<AppMemberInvestMessageBean> listAppMemberInvestMes = new ArrayList<AppMemberInvestMessageBean>();
        if (dMap != null && dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                AppMemberInvestMessageBean appMemberInvestMessageBean = new AppMemberInvestMessageBean();
                appMemberInvestMessageBean.setId(dMap.getValue("ID", i)); //主键
                appMemberInvestMessageBean.setNickName(dMap.getValue("NICKNAME", i)); //昵称
                appMemberInvestMessageBean.setInvestPrice(dMap.getValue("INVESTPRICE", i)); //累计投资金额
                listAppMemberInvestMes.add(appMemberInvestMessageBean);
            }
        }
        return listAppMemberInvestMes;
    }


    /**
     * 获取个人信息资料(App端)
     *
     * @param memberId
     * @return
     */
    @Override
    public AppPersonInfoMessageBean getAppPersonInfo(String memberId) {
        DMap dMap = allMemberDao.getAppMemberInfo(memberId);
        AppPersonInfoMessageBean appPersonInfoMessageBean = new AppPersonInfoMessageBean();
        if (dMap != null) {
            appPersonInfoMessageBean.setId(dMap.getValue("ID", 0)); //主键
            appPersonInfoMessageBean.setNickName(dMap.getValue("NICKNAME", 0)); //昵称
            appPersonInfoMessageBean.setRealFlg(dMap.getValue("REAL_FLG", 0)); //是否实名化认证(0-是、1-否)
            appPersonInfoMessageBean.setTelephoneFlg(dMap.getValue("TELEPHONE_FLG", 0)); //是否手机认证(0-是、1-否)
            appPersonInfoMessageBean.setRealName(dMap.getValue("REALNAME", 0)); //真实姓名
            appPersonInfoMessageBean.setTelePhone(dMap.getValue("TELEPHONE", 0)); //手机号
            appPersonInfoMessageBean.setIdCard(dMap.getValue("IDCARD", 0)); //身份证号
        }
        return appPersonInfoMessageBean;
    }

    /**
     * 更新余额
     */
    @Override
    public boolean updateBalance(MemberInfoMessageBean memberInfoMes) {
        MemberInfo memberInfo = new MemberInfo();
        if (memberInfoMes != null) {
            memberInfo.setId(memberInfoMes.getId());
            memberInfo.setBalance(Double.parseDouble(memberInfoMes.getBalance()));
        }
        DaoResult daoResult = allMemberDao.updateBalance(memberInfo);
        MemberInfoMessageBean memberInfoMessageBean1 = new MemberInfoMessageBean();
        if (daoResult.getCode() < 0) {
            return false;
        }
        return true;
    }
}
