package com.mh.peer.service.product;

import com.mh.peer.model.message.ProductTypeMessage;

import java.util.List;

/**
 * Created by Cuibin on 2016/4/29.
 */
public interface ProductTypeService {
    /**
     * 查询所有产品类型
     */
    List<ProductTypeMessage> queryAllProductType(ProductTypeMessage productTypeMessage);
    /**
     * 查询所有产品类型数量
     */
    int queryAllProductTypeCount(ProductTypeMessage productTypeMessage);
    /**
     * 删除产品类型
     */
    ProductTypeMessage deleteProductType(ProductTypeMessage productTypeMessage);
    /**
     * 添加产品类型
     */
    ProductTypeMessage addProductType(ProductTypeMessage productTypeMessage);
    /**
     * 更新产品类型
     */
    ProductTypeMessage updateProductType(ProductTypeMessage productTypeMessage);
}
