package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunPostalDao;
import com.mh.peer.model.entity.HuanxunPostal;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.HuanXunPostalMessageBean;
import com.mh.peer.service.huanxun.HuanXunPostalService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zhangerxin on 2016-7-26.
 */
@Service
public class HuanXunPostalServiceImpl implements HuanXunPostalService{

    @Autowired
    private HuanXunPostalDao huanXunPostalDao;

    /**
     * 环迅部分提现
     * @param huanxunPostal 环迅提现
     * @param memberMessage 我的消息
     * @param tradeDetail 交易记录
     */
    @Override
    public void postalhxResult(HuanxunPostal huanxunPostal, MemberMessage memberMessage, TradeDetail tradeDetail) {
        DaoResult daoResult = huanXunPostalDao.postalhxResult(huanxunPostal,memberMessage,tradeDetail);
        System.out.println("执行结果======"+daoResult.getCode());
    }

    /**
     * 获取提现信息
     * @param huanXunPostalMes
     * @return
     */
    @Override
    public HuanXunPostalMessageBean getPostalBfInfo(HuanXunPostalMessageBean huanXunPostalMes) {
        DMap dMap = huanXunPostalDao.getPostalBfInfo(huanXunPostalMes);
        HuanXunPostalMessageBean huanXunPostalMessageBean = new HuanXunPostalMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            huanXunPostalMessageBean.setMerbillno(dMap.getValue("MERBILLNO",0)); //商户订单号
        }
        return huanXunPostalMessageBean;
    }
}
