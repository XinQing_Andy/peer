package com.mh.peer.service.front;

import com.mh.peer.model.message.ProductReceiveInfoMessageBean;
import com.mh.peer.model.message.ProductReceiveMessageBean;

import java.util.List;

/**
 * Created by zhangerxin on 2016-8-31.
 * 还款部分
 */
public interface ReceiveService {

    /**
     * 根据投资id获取回款信息集合
     * @param buyId
     * @return
     */
    List<ProductReceiveInfoMessageBean> getReceiveInfoList(String buyId,String orderColumn,String orderType,String pageIndex,String pageSize);

    /**
     * 根据投资id获取回款信息数量
     * @param buyId
     * @return
     */
    int getReceiveInfoCount(String buyId);

    /**
     * 根据条件获取待还款信息集合
     * @param productReceiveMes
     * @return
     */
    List<ProductReceiveMessageBean> getProductReceiveList(ProductReceiveMessageBean productReceiveMes);

    /**
     * 根据条件获取待还款信息数量
     * @param productReceiveMes
     * @return
     */
    int getProductReceiveListCount(ProductReceiveMessageBean productReceiveMes);
}
