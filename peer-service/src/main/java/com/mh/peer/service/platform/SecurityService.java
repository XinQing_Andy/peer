package com.mh.peer.service.platform;

import com.mh.peer.model.business.AdvertContentBusinessBean;
import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.message.AdvertContentMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-11.
 */
public interface SecurityService {

    /**
     * 根据分页获取广告内容
     * @param advertContentMes
     * @return
     */
    List<AdvertContentMessageBean> queryAdvertByPage(AdvertContentMessageBean advertContentMes);

    /**
     * 新增广告
     * @param advertContent
     * @return
     */
    AdvertContentBusinessBean saveAdvert(AdvertContent advertContent);

    /**
     * 根据id获取保障信息
     * @param advertContentMes
     * @return
     */
    AdvertContentMessageBean queryProductById(AdvertContentMessageBean advertContentMes);

    /**
     * 更新保障信息
     * @param advertContent
     * @return
     */
    AdvertContentBusinessBean updateAdvert(AdvertContent advertContent);

    /**
     * 查询保障数量
     * @param advertContent
     * @return
     */
    int getCountAdvertByPage(AdvertContentMessageBean advertContent);
}
