package com.mh.peer.service.manager;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.message.LoginMessageBean;
import com.salon.frame.data.DMap;

/**
 * Created by zhaojiaming on 2016/1/14.
 */
public interface LoginService {
    /**
     * 登录
     * @param user
     * @return
     */
    LoginBusinessBean login(LoginMessageBean user);

    /**
     * 登入
     * @param sessionId
     * @param userId
     * @param userIp
     * @return
     */
    DMap loginLog(String sessionId, int userId, String userIp);

    /**
     * 登出
     * @param sessionId
     * @param userId
     * @param userIp
     * @return
     */
    DMap loginOutLog(String sessionId, int userId, String userIp);

}
