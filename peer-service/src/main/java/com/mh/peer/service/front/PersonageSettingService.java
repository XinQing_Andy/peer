package com.mh.peer.service.front;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.model.message.SysFileMessageBean;

import java.util.List;

/**
 * Created by Cuibin on 2016/5/1.
 */
public interface PersonageSettingService {
    /**
     * 查询一个用户基本资料
     */
    MemberInfoMessageBean queryOnlyMemberInfo();
    /**
     * 查询所有省份
     */
    List queryAllProvince();
    /**
     * 查询城市
     */
    List queryCity(String provinceid);

    /**
     * 更新个人基本资料
     */
    MemberInfoMessageBean updatePersona(MemberInfo memberInfo);

    /**
     * 保存头像
     */
    void updatePortrait(String path, String absPath, String uuid);
    /**
     * 删除头像
     */
    void deletePortrait(String imgId);
    /**
     * 查询头像信息
     */
    SysFileMessageBean queryOnlyPortrait(String userId);

}
