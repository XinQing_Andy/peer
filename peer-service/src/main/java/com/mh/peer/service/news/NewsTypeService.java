package com.mh.peer.service.news;

import com.mh.peer.model.business.NewsTypeBusinessBean;
import com.mh.peer.model.entity.NewsType;
import com.mh.peer.model.message.NewsTypeMessageBean;
import java.util.List;

/**
 * Created by zhangerxin on 2016/4/4.
 */
public interface NewsTypeService {

    /**
     * 获取新闻类别
     * @return
     */
    List<NewsType> getNewsType();

    /**
     * 修改新闻类别
     * @param newsTypeMes
     * @return
     */
    NewsTypeBusinessBean updateNewsType(NewsTypeMessageBean newsTypeMes);

    /**
     * 获取所有一级类别
     * @param newsTypeMes
     * @return
     */
    List<NewsTypeMessageBean> getAllNewsFirstType(NewsTypeMessageBean newsTypeMes);

    /**
     * 根据一级类别获取所有二级类别
     * @param newsTypeMes
     * @return
     */
    List<NewsTypeMessageBean> getAllNewsSecondType(NewsTypeMessageBean newsTypeMes);
}
