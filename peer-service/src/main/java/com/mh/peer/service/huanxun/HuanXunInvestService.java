package com.mh.peer.service.huanxun;

import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.mh.peer.util.TradeHandle;

import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-28.
 * zhangerxin
 */
public interface HuanXunInvestService {

    /**
     * 产品投资
     * @param huanxunFreeze
     * @param productBuyInfo
     * @param tradeDetail
     * @param memberMessage
     */
    public void investhxResult(Map<String,Object> mapException,HuanxunFreeze huanxunFreeze,ProductBuyInfo productBuyInfo,TradeDetail tradeDetail,MemberMessage memberMessage);

    /**
     * 获取环迅投资列表信息
     * @param huanXunFreezeMessageBean
     * @return
     */
    public List<HuanXunFreezeMessageBean> getHuanXunInvestBySome(HuanXunFreezeMessageBean huanXunFreezeMessageBean);

    /**
     * 解冻部分(投资)
     * @param memberInfo
     * @param huanxunFreeze
     */
    public void unfreezeResult(Map<String,Object> mapException,MemberInfo memberInfo,HuanxunFreeze huanxunFreeze);
}
