package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunTransferDao;
import com.mh.peer.model.entity.HuanxunTransfer;
import com.mh.peer.model.entity.HuanxunTransferDetail;
import com.mh.peer.model.message.HuanXunTransferMessage;
import com.mh.peer.service.huanxun.HuanXunTransferService;
import com.mh.peer.util.MerFeeHandle;
import com.mh.peer.util.ProductSh;
import com.mh.peer.util.PublicsTool;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-31.
 */
@Service
public class HuanXunTransferServiceImpl implements HuanXunTransferService{

    @Autowired
    private HuanXunTransferDao huanXunTransferDao;

    /**
     * 根据条件获取环迅信息
     * @param huanXunTransferMes
     * @return
     */
    @Override
    public List<HuanXunTransferMessage> getTransferInfoBySome(HuanXunTransferMessage huanXunTransferMes) {
        DMap dMap = huanXunTransferDao.getTransferInfoBySome(huanXunTransferMes);
        List<HuanXunTransferMessage> listHuanXunTransferMes = new ArrayList<HuanXunTransferMessage>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                HuanXunTransferMessage huanXunTransferMessage = new HuanXunTransferMessage();
                huanXunTransferMessage.setId(dMap.getValue("ID",i)); //主键
                huanXunTransferMessage.setFreezeId(dMap.getValue("FREEZEID",i)); //冻结id
                listHuanXunTransferMes.add(huanXunTransferMessage);
            }
        }
        return listHuanXunTransferMes;
    }


    /**
     * 投资转账
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     */
    @Override
    public void transferBuyResult(Map<String,Object> mapException,HuanxunTransfer huanxunTransfer, List<HuanxunTransferDetail> listHuanxunTransferDetail,String productId) {
        DaoResult daoResult = huanXunTransferDao.transferBuyResult(mapException, huanxunTransfer, listHuanxunTransferDetail, productId);

        try{

            if (daoResult.getCode() < 0){
            }else{
                ProductSh productSh = new ProductSh();
                productSh.execute(productId);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 投资转账详细信息
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     */
    @Override
    public void transferAttornResult(Map<String,Object> mapException,HuanxunTransfer huanxunTransfer, List<HuanxunTransferDetail> listHuanxunTransferDetail, String attornId) {
        DaoResult daoResult = huanXunTransferDao.transferAttornResult(mapException,huanxunTransfer,listHuanxunTransferDetail,attornId);
    }


    /**
     * 保存转账信息 bycuibin
     */
    @Override
    public boolean saveHuanxunTransfer(HuanxunTransfer huanxunTransfer,List<HuanxunTransferDetail> listHuanxunTransferDetail,String receiveId) {
        DaoResult daoResult = huanXunTransferDao.saveHuanxunTransfer(huanxunTransfer,listHuanxunTransferDetail,receiveId);
        if(daoResult.getCode() < 0){
            return false;
        }
        return true;
    }


    /**
     * 查询转账信息是否存在 bycuibin
     */
    @Override
    public boolean queryIsTransfer(String uuid) {
        DMap dMap = huanXunTransferDao.queryIsTransfer(uuid);
        if(dMap.getCount() > 0){
            return false;
        }
        return true;
    }

    /**
     * 根据产品id和应还款日期获取转账信息
     * @param productId
     * @param yrepaymentTime
     * @return
     */
    @Override
    public List<HuanXunTransferMessage> getRepayTransferAcctDetail(String productId, String yrepaymentTime) {
        DMap dMap = huanXunTransferDao.getRepayTransferAcctDetail(productId,yrepaymentTime);
        List<HuanXunTransferMessage> listHuanXunTransferMes = new ArrayList<HuanXunTransferMessage>();
        String principal = ""; //本金(String型)
        Double principalDou = 0.0d; //本金(Double型)
        String interest = ""; //利息(String型)
        Double interestDou = 0.0d; //利息(Double型)
        Double receivePriceDou = 0.0d; //需回款金额(Double型)
        String inMerFee = ""; //转入方平台服务费(String型)
        String buyer = ""; //投资人id

        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                HuanXunTransferMessage huanXunTransferMessage = new HuanXunTransferMessage(); //需转账信息
                huanXunTransferMessage.setInIpsAcctNo(dMap.getValue("BUYERIPSACCTNO",i)); //转入方IPS存管账户
                principal = dMap.getValue("PRINCIPAL",i); //应收本金
                if (principal != null && !principal.equals("")){
                    principalDou = TypeTool.getDouble(principal);
                }
                interest = dMap.getValue("INTEREST",i); //利息
                if (interest != null && !interest.equals("")){
                    interestDou = TypeTool.getDouble(interest);
                }
                receivePriceDou = principalDou + interestDou; //需还款金额
                receivePriceDou = PublicsTool.round(receivePriceDou,2);
                huanXunTransferMessage.setTrdAmt(TypeTool.getString(receivePriceDou));

                buyer = dMap.getValue("BUYER",i); //投资人id
                inMerFee = MerFeeHandle.getRepayInFee(buyer,interest); //还款冻结转入方手续费
                huanXunTransferMessage.setInMerFee(inMerFee); //转入方平台服务费
                listHuanXunTransferMes.add(huanXunTransferMessage);
            }
        }
        return listHuanXunTransferMes;
    }


    /**
     * 根据冻结信息还款
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     * @param mapException
     * @param repayId
     * @param yRepayTime
     * @return
     */
    @Override
    public String repayhxPriceTransfer(HuanxunTransfer huanxunTransfer, List<HuanxunTransferDetail> listHuanxunTransferDetail,
                                       Map<String, Object> mapException, String repayId, String yRepayTime) {
        String result = huanXunTransferDao.repayhxPriceTransfer(huanxunTransfer,listHuanxunTransferDetail,mapException,repayId,yRepayTime);
        return result;
    }
}
