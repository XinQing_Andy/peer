package com.mh.peer.service.dataStatistic;

/**
 * Created by zhangerxin on 2016-9-18.
 * 借款标销售情况
 */
public interface ProductSaleService {

    /**
     * 根据产品类型获取已放款数量
     * @param typeId
     * @return
     */
    String getGrantPriceCount(String typeId);

    /**
     * 根据产品类型获取已借款总额
     * @param typeId
     * @return
     */
    String getLoanPrice(String typeId);

    /**
     * 根据产品类型获取借款标数量
     * @param typeId
     * @return
     */
    String getLoanCount(String typeId);

    /**
     * 根据产品类型获取投标会员数
     * @param typeId
     * @return
     */
    String getInvestMemberCount(String typeId);
}
