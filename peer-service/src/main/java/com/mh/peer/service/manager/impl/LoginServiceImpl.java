package com.mh.peer.service.manager.impl;

import com.mh.peer.dao.manager.*;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.business.UserInfoBusiness;
import com.mh.peer.model.entity.SysDept;
import com.mh.peer.model.entity.SysLoginLog;
import com.mh.peer.model.entity.SysRoleUser;
import com.mh.peer.model.entity.SysUser;
import com.mh.peer.model.message.LoginMessageBean;
import com.mh.peer.service.manager.LoginService;
import com.mh.peer.session.User;
import com.mh.peer.util.PublicsTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhaojiaming on 2016/1/14.
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private UserDao userDao;
    @Autowired
    private DeptDao deptDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private UserLogDao userLogDao;
    @Autowired
    private MainDao mainDao;
    /**
     * 登录
     * @param user
     * @return
     */
    @Override
    public LoginBusinessBean login(LoginMessageBean user) {
        LoginBusinessBean loginBusinessBean = new LoginBusinessBean();
        // 得到前台验证码并验证是否一致
//        String verifyCode = user.getVerifyCode().toUpperCase();
//        String sessionVerifyCode = TypeTool.getString(servletUtil.getSessionValue("validateCode")).toUpperCase();
//        if(!verifyCode.equals(sessionVerifyCode)){
//            loginBusinessBean.setResult("error");
//            loginBusinessBean.setInfo("验证码不一致");
//            return loginBusinessBean;
//        }
        // 验证用户名
        SysUser sysUser = new SysUser();
        sysUser.setUserName(user.getUserName());
        sysUser = userDao.getUser(sysUser);
        if(sysUser == null){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo("用户不存在");
            return loginBusinessBean;
        }
        //判断密码
        String password = PublicsTool.isPassword(sysUser.getPassWord());
        if(!user.getPassword().equals(password)){
            loginBusinessBean.setResult("error");
            loginBusinessBean.setInfo("密码不正确");
            return loginBusinessBean;
        }
        loginBusinessBean.setResult("success");
        loginBusinessBean.setInfo("登录成功");
        // 用户
        loginBusinessBean.setSysUser(sysUser);
        // 部门
        SysDept sysDept = new SysDept();
        sysDept.setId(sysUser.getDepeId());
        loginBusinessBean.setSysDept(deptDao.getDept(sysDept));
        // 权限对用户
        SysRoleUser sysRoleUser = new SysRoleUser();
        sysRoleUser.setUserId(sysUser.getId());
        List<SysRoleUser> roleUserList = roleDao.getRoleUser(sysRoleUser);
        loginBusinessBean.setRoleUserListr(roleUserList);
        // 这个用户权限下对应的一级菜单
        List<UserInfoBusiness> userInfoList = new ArrayList<>();
        for (SysRoleUser temp:roleUserList){
            DMap parm = mainDao.getTopMenu(TypeTool.getString(sysUser.getId()), TypeTool.getString(temp.getRoleId()));
            //System.out.println("parm == " + parm);
            if(parm.getErrCode() < 0){
                break;
            }
            int count = parm.getCount();
            for (int i = 0; i < count; i++){
                UserInfoBusiness userInfo = new UserInfoBusiness();
                userInfo.setUserName(parm.getValue("USER_NAME", i));
                userInfo.setRoleId(parm.getValue("ROLE_ID", i));
                userInfo.setUserId(parm.getValue("USER_ID", i));
                userInfo.setRoleName(parm.getValue("ROLE_NAME", i));
                userInfo.setMenuId(parm.getValue("MENU_ID", i));
                userInfo.setMenuName(parm.getValue("MENU_NAME", i));
                userInfo.setMenuUrl(parm.getValue("MENU_URL", i));
                userInfo.setControlUrl(parm.getValue("CONTROL_URL", i));
                userInfo.setDeptId(parm.getValue("DEPT_ID", i));
                userInfo.setDeptName(parm.getValue("DEPT_NAME", i));
                userInfo.setParentId(parm.getValue("PARENT_ID", i));
                userInfo.setIconstyle(parm.getValue("ICON_STYLE", i));
                userInfo.setChildCount(parm.getValue("CHILDREN",i));
                userInfo.setFunctionId(parm.getValue("FUNCTION_ID", i));
                userInfo.setFunctionName(parm.getValue("FUNCTION_NAME", i));
                userInfo.setParentId(parm.getValue("PARENT_ID", i));
                userInfoList.add(userInfo);
            }
        }
        loginBusinessBean.setUserInfoList(userInfoList);
        //操作session
        User userSession = (User)servletUtil.getSession().getAttribute("user");
        String userName = sysUser.getUserName();
        if(null==userSession||!userName.equals(userSession.getName())){
            userSession = new User(userName,user.getMessageId(),sysUser.getId(),user.getIp(),this);
            servletUtil.setSessionValue("user",userSession);
        }
        return loginBusinessBean;
    }

    @Override
    public DMap loginLog(String sessionId, int userId, String userIp) {
        DMap result = new DMap();
        if(TypeTool.getString(sessionId).trim().length()==0){
            result.setErr(-1,"sessionId为空！");
            return result;
        }
        if(userId==0){
            result.setErr(-1,"userId为空！");
            return result;
        }
        if(TypeTool.getString(userIp).trim().length()==0){
            result.setErr(-1,"userIp为空！");
            return result;
        }
        //登录日志
        SysLoginLog loginLog = new SysLoginLog();
        String createTime = StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss");
        loginLog.setId(sessionId);
        loginLog.setUserId(userId);
        loginLog.setLoginTime(createTime);
        loginLog.setLoginIp(userIp);
        loginLog.setCreateTime(createTime);
        loginLog.setResulit("S");
        loginLog.setLogintype("I");
        DaoResult parm = userLogDao.loginLog(loginLog);
        result.setErrCode(parm.getCode());
        result.setErrText(parm.getErrText());
        return result;
    }

    @Override
    public DMap loginOutLog(String sessionId, int userId, String userIp) {
        DMap result = new DMap();
        if(TypeTool.getString(sessionId).trim().length()==0){
            result.setErr(-1,"sessionId为空！");
            return result;
        }
        if(userId==0){
            result.setErr(-1,"userId为空！");
            return result;
        }
        if(TypeTool.getString(userIp).trim().length()==0){
            result.setErr(-1,"userIp为空！");
            return result;
        }
        //登录日志
        SysLoginLog loginLog = new SysLoginLog();
        String outTime = StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss");
        loginLog.setId(sessionId);
        loginLog.setUserId(userId);
        loginLog.setLogoutTime(outTime);
        loginLog.setLoginIp(userIp);
        loginLog.setLogintype("O");
        DaoResult parm = userLogDao.loginOutLog(loginLog);
        result.setErrCode(parm.getCode());
        result.setErrText(parm.getErrText());
        return result;
    }
}
