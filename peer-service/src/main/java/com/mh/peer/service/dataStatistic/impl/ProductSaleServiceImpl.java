package com.mh.peer.service.dataStatistic.impl;

import com.mh.peer.dao.dataStatistic.ProductSaleDao;
import com.mh.peer.service.dataStatistic.ProductSaleService;
import com.salon.frame.data.DMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zhangerxin on 2016-9-18.
 * 借款标销售情况
 */
@Service
public class ProductSaleServiceImpl implements ProductSaleService{

    @Autowired
    private ProductSaleDao productSaleDao;

    /**
     * 根据产品类型获取已放款数量
     * @param typeId
     * @return
     */
    @Override
    public String getGrantPriceCount(String typeId) {
        DMap dMap = productSaleDao.getGrantPriceCount(typeId);
        String grantCount = dMap.getValue("GRANTCOUNT",0); //已放款数量
        return grantCount;
    }

    /**
     * 根据产品类型获取已借款总额
     * @param typeId
     * @return
     */
    @Override
    public String getLoanPrice(String typeId) {
        DMap dMap = productSaleDao.getLoanPrice(typeId);
        String loanPrice = dMap.getValue("LOANPRICE",0); //已借款总额
        return loanPrice;
    }

    /**
     * 根据产品类型获取借款标数量
     * @param typeId
     * @return
     */
    @Override
    public String getLoanCount(String typeId) {
        DMap dMap = productSaleDao.getLoanCount(typeId);
        String loanCount = dMap.getValue("LOANCOUNT",0); //借款标数量
        return loanCount;
    }

    /**
     * 根据产品类型获取投标会员数
     * @param typeId
     * @return
     */
    @Override
    public String getInvestMemberCount(String typeId) {
        DMap dMap = productSaleDao.getInvestMemberCount(typeId);
        String investMemberCount = dMap.getValue("INVESTMEMBERCOUNT",0); //已投资会员数
        return investMemberCount;
    }
}
