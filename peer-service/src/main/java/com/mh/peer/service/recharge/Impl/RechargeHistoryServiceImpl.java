package com.mh.peer.service.recharge.Impl;

import com.mh.peer.dao.recharge.RechargeHistoryDao;
import com.mh.peer.model.message.HuanXunRechargeMessageBean;
import com.mh.peer.model.message.ViewHuanxunRechargeMes;
import com.mh.peer.service.recharge.RechargeHistoryService;
import com.salon.frame.data.DMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cuibin on 2016/7/27.
 */
@Service
public class RechargeHistoryServiceImpl implements RechargeHistoryService {

    @Autowired
    private RechargeHistoryDao rechargeHistoryDao;

    /**
     * 查询充值统计
     */
    @Override
    public List<ViewHuanxunRechargeMes> queryRechargeHistory(ViewHuanxunRechargeMes viewHuanxunRechargeMes) {
        DMap dMap = rechargeHistoryDao.queryRechargeHistory(viewHuanxunRechargeMes);
        List<ViewHuanxunRechargeMes> list = new ArrayList<ViewHuanxunRechargeMes>();
        if (dMap.getCount() > 0) {
            for (int i = 0; i < dMap.getCount(); i++) {
                ViewHuanxunRechargeMes temp = new ViewHuanxunRechargeMes();
                temp.setId(dMap.getValue("ID", i));
                temp.setIpsbillno(dMap.getValue("IPSBILLNO", i));//订单编号
                temp.setIpstrdamt(dMap.getValue("IPSTRDAMT", i)); //用户IPS实际到账金额

                String ipsdotime = dMap.getValue("IPSDOTIME", i);
                if (ipsdotime != null && !ipsdotime.equals("")) {
                    ipsdotime = ipsdotime.substring(0, 10);
                }
                temp.setIpsdotime(ipsdotime);//IPS处理时间
                temp.setTrdstatus(dMap.getValue("TRDSTATUS", i));//充值状态(0-失败、1-成功、2-处理中)
                temp.setIdcard(dMap.getValue("IDCARD", i)); //身份证号
                temp.setRealname(dMap.getValue("REALNAME", i)); //真实姓名
                temp.setSource(dMap.getValue("SOURCE", i)); //充值平台
                list.add(temp);
            }
        }

        return list;
    }

    /**
     * 查询充值统计 总数
     */
    @Override
    public String queryRechargeHistoryCount(ViewHuanxunRechargeMes viewHuanxunRechargeMes) {
        DMap dMap = rechargeHistoryDao.queryRechargeHistoryCount(viewHuanxunRechargeMes);
        return dMap.getValue("COUNT", 0);
    }
}
