package com.mh.peer.service.front.impl;

import com.mh.peer.dao.front.ApplyProductDao;
import com.mh.peer.model.message.ProductApplyDetailsMessageBean;
import com.mh.peer.model.message.ProductApplyMessageBean;
import com.mh.peer.service.front.ApplyProductService;
import com.mh.peer.util.PublicsTool;
import com.salon.frame.data.DMap;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-9-7.
 */
@Service
public class ApplyProductServiceImpl implements ApplyProductService{

    @Autowired
    private ApplyProductDao applyProductDao;

    /**
     * 查询已申请项目
     * @param productApplyMes
     * @return
     */
    @Override
    public List<ProductApplyMessageBean> getApplyProductList(ProductApplyMessageBean productApplyMes) {
        DMap dMap = applyProductDao.getApplyProductList(productApplyMes);
        List<ProductApplyMessageBean> listProductApplyMes = new ArrayList<ProductApplyMessageBean>();
        String applyTime = ""; //申请日期
        String loanYear = ""; //贷款期限(年)
        String loanMonth = ""; //贷款期限(月)
        String loanDay = ""; //贷款期限(天)
        String loanLength = ""; //贷款时长
        String bidFee = ""; //最低投标金额(String型)
        Double bidFeeDou = 0.0d; //最低投标金额(Double型)
        String splitCount = ""; //在大拆分份数(String型)
        Double splitCountDou = 0.0d; //最大拆分份数(Double型)
        String loanPrice = ""; //贷款金额(String型)
        Double loanPriceDou = 0.0d; //贷款金额(Double型)
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ProductApplyMessageBean productApplyMessageBean = new ProductApplyMessageBean();
                productApplyMessageBean.setProductId(dMap.getValue("ID",i)); //主键
                productApplyMessageBean.setTitle(dMap.getValue("TITLE",i)); //题目
                productApplyMessageBean.setCode(dMap.getValue("CODE",i)); //编码

                applyTime = dMap.getValue("CREATETIME",i); //申请日期
                if (applyTime != null && !applyTime.equals("")){
                    applyTime = applyTime.substring(0,10);
                }
                productApplyMessageBean.setApplyTime(applyTime);

                loanYear = dMap.getValue("LOANYEAR",i); //贷款期限(年)
                loanMonth = dMap.getValue("LOANMONTH",i); //贷款期限(月)
                loanDay = dMap.getValue("LOADDAY",i); //贷款期限(天)
                loanLength = ""; //贷款期限(时长)
                if (loanYear != null && !loanYear.equals("")){
                    if (!loanYear.equals("0")){
                        loanLength = loanYear + "年";
                    }
                }
                if (loanMonth != null && !loanMonth.equals("")){
                    if (!loanMonth.equals("0")){
                        loanLength = loanLength + loanMonth + "个月";
                    }
                }
                if (loanDay != null && !loanDay.equals("")){
                    if (!loanDay.equals("0")){
                        loanLength = loanLength + loanDay + "天";
                    }
                }
                productApplyMessageBean.setLoanLength(loanLength); //贷款时长
                productApplyMessageBean.setLoanRate(dMap.getValue("LOANRATE",i)); //贷款利率

                bidFee = dMap.getValue("BIDFEE",i); //最低投标金额(String型)
                if (bidFee != null && !bidFee.equals("")){
                    bidFeeDou = TypeTool.getDouble(bidFee); //最低投标金额(Double型)
                }

                splitCount = dMap.getValue("SPLITCOUNT",i); //最大拆分份数(String型)
                if (splitCount != null && !splitCount.equals("")){
                    splitCountDou = TypeTool.getDouble(splitCount); //最大拆分份数(Double型)
                }

                loanPriceDou = bidFeeDou * splitCountDou; //贷款金额(Double型)
                loanPrice = TypeTool.getString(loanPriceDou); //贷款金额(String型)
                productApplyMessageBean.setLoanPrice(loanPrice);

                listProductApplyMes.add(productApplyMessageBean);
            }
        }
        return listProductApplyMes;
    }


    /**
     * 已申请项目数量
     * @param productApplyMes
     * @return
     */
    @Override
    public int getApplyProductListCount(ProductApplyMessageBean productApplyMes) {
        DMap dMap = applyProductDao.getApplyProductListCount(productApplyMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }


    /**
     * 获取已还金额
     * @param productId
     * @param applyMemberId
     * @return
     */
    @Override
    public ProductApplyDetailsMessageBean getyRepay(String productId, String applyMemberId) {
        DMap dMap = applyProductDao.getyRepay(productId,applyMemberId);
        String yRepayPrice = ""; //已还款金额(String型)
        Double yRepayPriceDou = 0.0d; //已还款金额(Double型)
        String yPrincipal = ""; //已还款本金(String型)
        Double yPrincipalDou = 0.0d; //已还款本金(Double型)
        String yInterest = ""; //已还款利息(String型)
        Double yInterestDou = 0.0; //已还款利息(Double型)
        String yPrice = ""; //应还款金额(String型)
        Double yPriceDou = 0.0d; //应还款金额(Double型)
        String managePrice = ""; //管理费(String型)
        Double managePriceDou = 0.0d; //管理费(Double型)
        ProductApplyDetailsMessageBean productApplyDetailsMessageBean = new ProductApplyDetailsMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            yRepayPrice = dMap.getValue("YREPAYPRICE",0); //已还款金额(String型)
            if (yRepayPrice != null && !yRepayPrice.equals("")){
                yRepayPriceDou = PublicsTool.round(TypeTool.getDouble(yRepayPrice),2); //已还款金额(Double型)
            }
            yRepayPrice = TypeTool.getString(yRepayPriceDou);
            productApplyDetailsMessageBean.setyRepayPrice(yRepayPrice);

            yPrincipal = dMap.getValue("YPRINCIPAL",0); //已还款本金(String型)
            if (yPrincipal != null && !yPrincipal.equals("")){
                yPrincipalDou = PublicsTool.round(TypeTool.getDouble(yPrincipal),2); //已还款本金(Double型)
            }
            yPrincipal = TypeTool.getString(yPrincipalDou);
            productApplyDetailsMessageBean.setyRepayPrincipal(yPrincipal);

            yInterestDou = yRepayPriceDou - yPrincipalDou; //利息(Double型)
            yInterestDou = PublicsTool.round(yInterestDou,2);
            if (yInterestDou < 0){
                yInterestDou = 0d;
            }
            yInterest = TypeTool.getString(yInterestDou); //利息(String型)
            productApplyDetailsMessageBean.setyRepayInterest(yInterest);

            yPrice = dMap.getValue("YPRICE",0); //应还款金额(String型)
            if (yPrice != null && !yPrice.equals("")){
                yPriceDou = TypeTool.getDouble(yPrice); //应还款金额(Double型)
                yPriceDou = PublicsTool.round(yPriceDou,2);
            }
            yPrice = TypeTool.getString(yPriceDou);
            productApplyDetailsMessageBean.setYprice(yPrice);

            managePriceDou = yPriceDou - yRepayPriceDou; //管理费(Double型)
            managePriceDou = PublicsTool.round(managePriceDou,2);
            if (managePriceDou < 0){
                managePriceDou = 0.0d;
            }
            managePrice = TypeTool.getString(managePriceDou); //管理费(String型)
            productApplyDetailsMessageBean.setManagePrice(managePrice);
        }
        return productApplyDetailsMessageBean;
    }


    /**
     * 获取待还金额
     * @param productId
     * @param applyMemberId
     * @return
     */
    @Override
    public ProductApplyDetailsMessageBean getdRepay(String productId, String applyMemberId) {
        DMap dMap = applyProductDao.getdRepay(productId, applyMemberId);
        String dRepayPrice = ""; //待还款金额(String型)
        Double dRepayPriceDou = 0.0d; //待还款金额(Double型)
        String dPrincipal = ""; //待还款本金(String型)
        Double dPrincipalDou = 0.0d; //待还款本金(Double型)
        String dInterest = ""; //待还款利息(String型)
        Double dInterestDou = 0.0; //待还款利息(Double型)
        String yPrice = ""; //应还款金额(String型)
        Double yPriceDou = 0.0d; //应还款金额(Double型)
        ProductApplyDetailsMessageBean productApplyDetailsMessageBean = new ProductApplyDetailsMessageBean();
        if (dMap != null && dMap.getCount() > 0){
            dRepayPrice = dMap.getValue("DREPAYPRICE",0); //待还款金额(String型)
            if (dRepayPrice != null && !dRepayPrice.equals("")){
                dRepayPriceDou = TypeTool.getDouble(dRepayPrice); //待还款金额(Double型)
            }
            productApplyDetailsMessageBean.setdRepayPrice(dRepayPrice);

            dPrincipal = dMap.getValue("DPRINCIPAL",0); //待还款本金(String型)
            if (dPrincipal != null && !dPrincipal.equals("")){
                dPrincipalDou = TypeTool.getDouble(dPrincipal); //待还款本金(Double型)
            }
            productApplyDetailsMessageBean.setdRepayPrincipal(dPrincipal);

            dInterestDou = dRepayPriceDou - dPrincipalDou; //利息(Double型)
            if (dInterestDou < 0){
                dInterestDou = 0d;
            }
            dInterest = TypeTool.getString(dInterestDou); //利息(String型)
            productApplyDetailsMessageBean.setdRepayInterest(dInterest);

            yPrice = dMap.getValue("YPRICE",0); //应还款金额(String型)
            if (yPrice != null && !yPrice.equals("")){
                yPriceDou = TypeTool.getDouble(yPrice); //应还款金额(Double型)
            }
            productApplyDetailsMessageBean.setYprice(yPrice);
        }
        return productApplyDetailsMessageBean;
    }


    /**
     * 还款列表
     * @param productId
     * @return
     */
    @Override
    public List<ProductApplyDetailsMessageBean> getRepayList(String productId,String orderColumn,String orderType,String pageIndex,String pageSize) {
        DMap dMap = applyProductDao.getRepayList(productId,orderColumn,orderType,pageIndex,pageSize);
        String principal = ""; //本金(String型)
        Double principalDou = 0.0d; //本金(Double型)
        String interest = ""; //利息(String型)
        Double interestDou = 0.0d; //利息(Double型)
        String yPrice = ""; //应还款金额(String型)
        Double yPriceDou = 0.0d; //应还款金额(double型)
        String sRepayTime = ""; //实际还款日期
        List<ProductApplyDetailsMessageBean> listProductApplyDetailsMes = new ArrayList<ProductApplyDetailsMessageBean>();

        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ProductApplyDetailsMessageBean productApplyDetailsMessageBean = new ProductApplyDetailsMessageBean();
                productApplyDetailsMessageBean.setRepayId(dMap.getValue("ID",i)); //主键
                productApplyDetailsMessageBean.setyRepayTime(dMap.getValue("YREPAYMENTTIME",i)); //应还款时间

                principal = dMap.getValue("PRINCIPAL",i); //应还款本金(String型)
                if (principal != null && !principal.equals("")){
                    principalDou = TypeTool.getDouble(principal); //应还款本金(Double型)
                }
                interest = dMap.getValue("INTEREST",i); //利息(String型)
                if (interest != null && !interest.equals("")){
                    interestDou = TypeTool.getDouble(interest); //利息(Double型)
                }
                yPriceDou = principalDou + interestDou; //应还款金额(Double型)
                yPriceDou = PublicsTool.round(yPriceDou,2);
                yPrice = TypeTool.getString(yPriceDou); //应还款金额(String型)
                productApplyDetailsMessageBean.setYprice(yPrice);

                productApplyDetailsMessageBean.setyRepayPrice(dMap.getValue("REPAYMENTPRICE",i)); //实际还款金额
                sRepayTime = dMap.getValue("SREPAYMENTTIME", i);
                if (sRepayTime != null && !sRepayTime.equals("")){
                    sRepayTime = sRepayTime.substring(0,10);
                }
                productApplyDetailsMessageBean.setdRepayTime(sRepayTime); //实际还款日期
                listProductApplyDetailsMes.add(productApplyDetailsMessageBean);
            }
        }
        return listProductApplyDetailsMes;
    }


    /**
     * 还款列表数量
     * @param productId
     * @return
     */
    @Override
    public int getRepayListCount(String productId) {
        DMap dMap = applyProductDao.getRepayListCount(productId);
        int totalCount = dMap.getCount();
        return totalCount;
    }
}
