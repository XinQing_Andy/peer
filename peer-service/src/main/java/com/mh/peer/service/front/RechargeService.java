package com.mh.peer.service.front;

import com.mh.peer.model.message.ChargeRecordMessageBean;
import com.mh.peer.model.message.RechargeHistoryMessageBean;
import com.mh.peer.model.message.TradeDetailMessageBean;
import com.mh.peer.model.message.ViewHuanxunRechargeMes;

import java.util.List;

/**
 * Created by Cuibin on 2016/6/17.
 */
public interface RechargeService {
    /**
     * 充值
     */
    public boolean userRecharge(ChargeRecordMessageBean chargeRecordMessageBean,TradeDetailMessageBean tradeDetailMessageBean);

    /**
     *  查询充值记录
     */
    public List<RechargeHistoryMessageBean> queryRechargeHistory(RechargeHistoryMessageBean rechargeHistoryMessageBean);

    /**
     * 查询 实际金额 和 充值金额
     */
    public String[] queryRechargeSum(String memberId);

    /**
     * 查询充值记录总数
     */
    public String queryRechargeHistoryCount(RechargeHistoryMessageBean rechargeHistoryMessageBean);

    /**
     * 充值记录集合
     * @param viewHuanxunRechargeMes
     * @return
     */
    public List<ViewHuanxunRechargeMes> queryViewRechargeHistory(ViewHuanxunRechargeMes viewHuanxunRechargeMes);

    /**
     * 充值记录数量
     */
    public int queryViewRechargeHistoryCount(ViewHuanxunRechargeMes viewHuanxunRechargeMes);
}
