package com.mh.peer.service.front;

import com.mh.peer.model.message.MemberMessageMesBean;

import java.util.List;

/**
 * Created by Cuibin on 2016/7/21.
 */
public interface MyMessageService {

    /**
     * 分页查询我的消息
     */
    public List<MemberMessageMesBean> queryMyMessageByPage(MemberMessageMesBean memberMessageMesBean);


    /**
     * 查询充值记录总数
     */
    public int queryMyMessageCount(MemberMessageMesBean memberMessageMesBean);

    /**
     * 获取详细信息
     * @param memberMessageMes
     * @return
     */
    public MemberMessageMesBean getMyMessageById(MemberMessageMesBean memberMessageMes);

    /**
     * 查询下一条详细信息
     * @param memberMessageMes
     * @return
     */
    public MemberMessageMesBean getNextMyMessageById(MemberMessageMesBean memberMessageMes);

    /**
     * 删除我的消息
     * @param memberMessageMes
     * @return
     */
    public MemberMessageMesBean delMyMessageById(MemberMessageMesBean memberMessageMes);

    /**
     * 批量删除我的消息
     * @param memberMessageMes
     * @return
     */
    public MemberMessageMesBean updateBatchReadFlg(MemberMessageMesBean memberMessageMes);
}
