package com.mh.peer.service.dataStatistic.impl;

import com.mh.peer.dao.dataStatistic.PlatIncomeDao;
import com.mh.peer.model.message.PlatIncomeMessageBean;
import com.mh.peer.service.dataStatistic.PlatIncomeService;
import com.salon.frame.data.DMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-9-18.
 * 平台收入管理
 */
@Service
public class PlatIncomeServiceImpl implements PlatIncomeService{

    @Autowired
    private PlatIncomeDao platIncomeDao;

    /**
     * 获取平台收入统计
     * @param platIncomeMes
     * @return
     */
    @Override
    public List<PlatIncomeMessageBean> getPlatIncomeList(PlatIncomeMessageBean platIncomeMes) {
        DMap dMap = platIncomeDao.getPlatIncomeList(platIncomeMes);
        List<PlatIncomeMessageBean> listPlatIncomeMes = new ArrayList<PlatIncomeMessageBean>();
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                PlatIncomeMessageBean platIncomeMessageBean = new PlatIncomeMessageBean();
                platIncomeMessageBean.setPlatIncome(dMap.getValue("PLATINCOME",i)); //平台收入
                platIncomeMessageBean.setUnInvestChargeFee(dMap.getValue("UNINVESTCHARGEFEE",i)); //未投资充值手续费
                platIncomeMessageBean.setReChargeMerFee(dMap.getValue("CHARGEFEE",i)); //充值手续费
                platIncomeMessageBean.setPostalMerFee(dMap.getValue("POSTALFEE",i)); //提现手续费
                platIncomeMessageBean.setAttornMerFee(dMap.getValue("ATTORNFEE",i)); //债权手续费
                platIncomeMessageBean.setDataTime(dMap.getValue("DATATIME",i)); //数据日期
                platIncomeMessageBean.setOperateTime(dMap.getValue("OPERATETIME",i)); //操作时间
                listPlatIncomeMes.add(platIncomeMessageBean);
            }
        }
        return listPlatIncomeMes;
    }


    /**
     * 平台收入统计数量
     * @param platIncomeMes
     * @return
     */
    @Override
    public int getPlatIncomeCount(PlatIncomeMessageBean platIncomeMes) {
        DMap dMap = platIncomeDao.getPlatIncomeList(platIncomeMes);
        int totalCount = dMap.getCount();
        return totalCount;
    }
}
