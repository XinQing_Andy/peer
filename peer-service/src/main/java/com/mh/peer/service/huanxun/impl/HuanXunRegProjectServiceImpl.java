package com.mh.peer.service.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunRegProjectDao;
import com.mh.peer.model.entity.HuanxunRegproject;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.ProductHuanXunMessageBean;
import com.mh.peer.service.huanxun.HuanXunRegProjectService;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ${zhangerxin} on 2016-7-27.
 */
@Service
public class HuanXunRegProjectServiceImpl implements HuanXunRegProjectService{

    @Autowired
    private HuanXunRegProjectDao huanXunRegProjectDao;

    /**
     * 项目登记部分
     * @param huanxunRegProject
     * @param memberMessage
     * @param tradeDetail
     * @return
     */
    @Override
    public ProductHuanXunMessageBean regProjecthxResult(Map<String,Object> mapException,HuanxunRegproject huanxunRegProject, MemberMessage memberMessage, TradeDetail tradeDetail) {
        DaoResult daoResult = huanXunRegProjectDao.regProjecthxResult(mapException,huanxunRegProject,memberMessage,tradeDetail);
        ProductHuanXunMessageBean productHuanXunMessageBean = new ProductHuanXunMessageBean();
        if (daoResult.getCode() < 0){
            productHuanXunMessageBean.setResult("error"); //程序执行状态(success-成功、error-失败)
            productHuanXunMessageBean.setInfo("环迅项目登记失败");
        }
        productHuanXunMessageBean.setResult("success");
        productHuanXunMessageBean.setInfo("环迅项目登记成功！");
        return productHuanXunMessageBean;
    }


    /**
     * 根据条件获取登记项目信息
     * @param productHuanXunMes
     * @return
     */
    @Override
    public List<ProductHuanXunMessageBean> getRegProjectInfoList(ProductHuanXunMessageBean productHuanXunMes) {
        List<ProductHuanXunMessageBean> listProductHuanXunMes = new ArrayList<ProductHuanXunMessageBean>();
        DMap dMap = huanXunRegProjectDao.getRegProjectInfoList(productHuanXunMes);
        if (dMap != null && dMap.getCount() > 0){
            for (int i=0;i<dMap.getCount();i++){
                ProductHuanXunMessageBean productHuanXunMessageBean = new ProductHuanXunMessageBean();
                productHuanXunMessageBean.setId(dMap.getValue("ID",i)); //主键
                productHuanXunMessageBean.setProjectNo(dMap.getValue("PROJECTNO", i)); //项目ID号
                listProductHuanXunMes.add(productHuanXunMessageBean);
            }
        }
        return listProductHuanXunMes;
    }
}
