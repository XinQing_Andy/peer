package com.mh.peer.service.platform;

import com.mh.peer.model.business.AdvertContentBusinessBean;
import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.message.AdvertContentMessageBean;
import com.mh.peer.model.message.AppAdvertMessageBean;
import com.mh.peer.model.message.AppHomeMessageBean;

import java.util.List;

/**
 * Created by zhangerxin on 2016-4-11.
 */
public interface AdvertService {

    /**
     * 根据分页获取广告内容
     * @param advertContentMes
     * @return
     */
    List<AdvertContentMessageBean> queryAdvertByPage(AdvertContentMessageBean advertContentMes);

    /**
     * 新增广告
     * @param advertContent
     * @return
     */
    AdvertContentBusinessBean saveAdvert(AdvertContent advertContent);

    /**
     * 根据id获取广告信息
     * @param advertContentMes
     * @return
     */
    AdvertContentMessageBean queryProductById(AdvertContentMessageBean advertContentMes);

    /**
     * 更新广告
     * @param advertContent
     * @return
     */
    AdvertContentBusinessBean updateAdvert(AdvertContent advertContent);

    /**
     * 查询广告数量
     * @param advertContent
     * @return
     */
    int getCountAdvertByPage(AdvertContentMessageBean advertContent);

    /**
     * 查询广告(前台部分)
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<AdvertContentMessageBean> getListBySome(String orderColumn, String orderType, String pageIndex, String pageSize, String typeId);

    /**
     * 获取首页广告(App端)
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<AppHomeMessageBean> getHomeAdvert(String orderColumn, String orderType, String pageIndex, String pageSize, String typeId);

    /**
     * 获取广告列表(App端)
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @param orderColumn
     * @param orderType
     * @return
     */
    List<AppAdvertMessageBean> getAdvert(String typeId,String pageIndex,String pageSize,String orderColumn,String orderType);

    /**
     * 获取广告详情(App端)
     * @param advertId
     * @return
     */
    AppAdvertMessageBean getAdvertDetail(String advertId);

    /**
     * 查询广告总数(前台) by cuibin
     */
    public String queryAdvertCount(String typeId);

    /**
     * 前台查询公告列表
     * @param advertContentMessageBean
     * @return
     */
    List<AdvertContentMessageBean> queryFrontAdvertList(AdvertContentMessageBean advertContentMessageBean);

    /**
     * 前台查询广告数量
     * @param advertContentMessageBean
     * @return
     */
    int queryFrontAdvertCount(AdvertContentMessageBean advertContentMessageBean);

    /**
     * 前台查询广告详情
     * @param advertContentMes
     * @return
     */
    AdvertContentMessageBean queryFrontAdvertDetails(AdvertContentMessageBean advertContentMes);
}
