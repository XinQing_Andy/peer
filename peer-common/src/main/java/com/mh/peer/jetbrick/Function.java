package com.mh.peer.jetbrick;

import com.salon.frame.util.TypeTool;
import jetbrick.template.JetAnnotations;
import jetbrick.template.runtime.InterpretContext;
import jetbrick.template.web.JetWebContext;

import javax.servlet.http.HttpSession;

/**
 * Created by WangMeng on 2015/12/4.
 */
@JetAnnotations.Functions
public class Function {
    /**
     * 得到CSS目录
     * @return
     */
    public static String getCssPath(){
        String cssPath = getWebRootPath().replace("\\","/")+"/css";
        return cssPath;
    }

    /**
     * 得到Image目录
     * @return
     */
    public static String getImagePath(){
        String imagePath = getWebRootPath().replace("\\","/")+"/image";
        return imagePath;
    }

    /**
     * 得到JS目录
     * @return
     */
    public static String getJsPath(){
        String imagePath = getWebRootPath().replace("\\","/")+"/js";
        return imagePath;
    }

    /**
     * 得到第三发插�?
     * @return
     */
    public static String getThirdauthPath(){
        String imagePath = getWebRootPath().replace("\\","/")+"/thirdauth";
        return imagePath;
    }
    /**
     * 得到web跟目�?
     * @return
     */
    public static String getWebRootPath() {
        InterpretContext ctx = InterpretContext.current();
        return TypeTool.getString(ctx.getValueStack().getValue(JetWebContext.WEBROOT_PATH));
    }

    /**
     * 得到回话
     * @return
     */
    public static HttpSession getSession() {
        InterpretContext ctx = InterpretContext.current();
        HttpSession session = (HttpSession) ctx.getValueStack().getValue(JetWebContext.SESSION);
        return session;
    }
}
