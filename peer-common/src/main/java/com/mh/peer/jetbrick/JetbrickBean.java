package com.mh.peer.jetbrick;

/**
 * Created by WangMeng on 2015/12/9.
 */
public class JetbrickBean {
    /**
     * 返回成功或失败(error,success)
     */
    private String result;
    /**
     * 错误消息
     */
    private String info;
    /**
     * HTML内容
     */
    private String htmlText;

    public JetbrickBean() {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }


    @Override
    public String toString() {
        return "JetbrickBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", htmlText='" + htmlText + '\'' +
                '}';
    }
}
