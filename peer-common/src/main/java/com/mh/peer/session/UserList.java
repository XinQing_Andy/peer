package com.mh.peer.session;

import com.salon.frame.data.DMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Enumeration;
import java.util.Vector;
/**
 * Created by WangMeng on 2015/12/7.
 */
public class UserList {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserList.class);
    private static final UserList userList = new UserList();
    private Vector<User> v;
    private UserList() {
        v = new Vector<User>();
    }

    public static UserList getInstance()
    {
        return userList;
    }


    public void addUser(User name)
    {
        if (name != null){
            v.addElement(name);
            LOGGER.info("登入log" + name + "==" + name.getSessionId());
            DMap parm = name.getService().loginLog(name.getSessionId(), name.getId(), name.getIp());
            if(parm.getErrCode()<0){
                LOGGER.error(parm.getErrText());
            }
        }
    }


    public void removeUser(User name) {
        if (name != null){
            v.remove(name);
            LOGGER.info("登出log" + name + "==" + name.getService());
            DMap parm = name.getService().loginOutLog(name.getSessionId(), name.getId(), name.getIp());
            if(parm.getErrCode()<0){
                LOGGER.error(parm.getErrText());
            }
        }
    }

    public Enumeration<User> getUserList()
    {
        return v.elements();
    }

    public int getUserCount()
    {
        return v.size();
    }
}
