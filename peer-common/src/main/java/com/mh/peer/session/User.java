package com.mh.peer.session;

import com.mh.peer.service.manager.LoginService;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/**
 * Created by WangMeng on 2015/12/7.
 */

public class User implements HttpSessionBindingListener {
    private String sessionId;
    private String name;
    private int id;
    private String ip;
    private LoginService service;
    private UserList ul = UserList.getInstance();

    public User()
    {

    }

   public User(String name,String sessionId,int id,String ip,LoginService service)
    {

        this.name = name;
        this.sessionId = sessionId;
        this.id = id;
        this.ip = ip;
        this.service = service;

    }

    public User(String name, int id, LoginService service) {
        this.name = name;
        this.id = id;
        this.service = service;
    }

    public void setName(String name)
    {

        this.name = name;

    }

    public String getName()
    {

        return name;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

   public LoginService getService() {
        return service;
    }

    public void setService(LoginService service) {
        this.service = service;
    }

    public void valueBound(HttpSessionBindingEvent event)
    {
        ul.addUser(this);

    }

    public void valueUnbound(HttpSessionBindingEvent event)
    {

        ul.removeUser(this);

    }

    @Override
    public String toString() {
        return "User{" +
                "sessionId='" + sessionId + '\'' +
                ", name='" + name + '\'' +
                ", id=" + id +
                ", ip='" + ip + '\'' +
                ", ul=" + ul +
                '}';
    }
}
