package com.mh.peer.page;

import com.salon.frame.util.TypeTool;

/**
 * Created by zhangerxin on 2016-6-29.
 */
public class FrontPage {

    /**
     * 获取分页部分
     * @param pageIndex
     * @param pageSize
     * @param count
     * @return
     */
    public String getPageBySome(String pageIndex,String pageSize,int count,String funcName){
        int pageIndexin = 0; //当前数(整型)
        int pageSizein = 0; //每次显示数量（整型）
        int currentPage = 0; //当前页
        int totalPage = 0; //总页数
        int lastPage = 0; //上一页
        int nextPage = 0; //下一页
        String pageStr = ""; //分页部分内容

        try{

            if(pageIndex != null && !pageIndex.equals("")){ //当前数(字符型)
                pageIndexin = TypeTool.getInt(pageIndex); //当前数(整型)
            }
            if(pageSize != null && !pageSize.equals("")){ //每次显示数量(字符型)
                pageSizein = TypeTool.getInt(pageSize); //每次显示数量（整型）
            }

            /********** 当前页Start **********/
            if (pageIndexin%pageSizein == 0){
                currentPage = pageIndexin/pageSizein;
            }else{
                currentPage = pageIndexin/pageSizein + 1;
            }
            /*********** 当前页End ***********/

            /********** 总页数Start **********/
            if(count%pageSizein == 0){
                totalPage = count/pageSizein;
            }else{
                totalPage = count%pageSizein + 1;
            }
            /*********** 总页数End ***********/

            if((currentPage+1) < totalPage){
                if(currentPage <= 1){
                    lastPage = 1; //上一页
                }else{
                    nextPage = currentPage + 1; //下一页
                }
            }

            pageStr = "<li>"
                    + "<input type=\"button\" value=\"首页\" class=\"home_btn\" onclick=\"frontStartPage('1','"+pageSize+"','"+funcName+"')\"/>"
                    + "</li>"
                    + "<li>";


        }catch(Exception e){
            e.printStackTrace();
        }
        return pageStr;
    }


}
