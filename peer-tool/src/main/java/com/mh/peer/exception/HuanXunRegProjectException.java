package com.mh.peer.exception;

import com.mh.peer.model.entity.HuanxunFile;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-8-9.
 * 项目登记处理异常类
 */
public class HuanXunRegProjectException extends Exception{

    /**
     * 处理项目登记异常
     * @param map
     */
    public static void handle(Map<String,Object> map){
        System.out.println("进入异常处理");
        Object productId = ""; //产品id
        Object resultCode = ""; //响应吗
        Object resultMsg = ""; //响应信息描述
        Object merchantID = ""; //商户存管交易账号
        String response = ""; //响应信息
        String responseJm = ""; //响应信息解密
        String merBillNo = ""; //商户订单号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS登记日期
        String projectNo = ""; //项目ID号
        String status = ""; //登记状态(0-失败、1-成功)
        String path = ""; //异常文件路径
        Map<String,String> mapRegProject = new HashMap<String,String>();
        String UidFile = UUID.randomUUID().toString().replaceAll("-",""); //文件主键
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
        boolean flag = true; //创建txt文件状态
        String filenameTemp = ""; //文件
        String exceptionStr = ""; //异常信息

        try{

            if (map != null){
                productId = map.get("productId"); //项目id
                resultCode = map.get("resultCode"); //响应吗
                resultMsg = map.get("resultMsg"); //响应信息描述
                merchantID = map.get("merchantID"); //商户存管交易账号
                response = (String) map.get("response"); //响应信息
            }

            if (response != null && !response.equals("")){ //响应信息
                responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                if (responseJm != null && !responseJm.equals("")){
                    mapRegProject = JsonHelper.getObjectToMap(responseJm); //项目登记
                    if (mapRegProject != null){
                        merBillNo = mapRegProject.get("merBillNo"); //商户订单号
                        ipsBillNo = mapRegProject.get("ipsBillNo"); //IPS订单号
                        ipsDoTime = mapRegProject.get("ipsDoTime"); //IPS订单处理时间
                        projectNo = mapRegProject.get("projectNo"); //项目ID号
                        status = mapRegProject.get("status"); //登记状态(0-失败、1-成功)
                    }
                }
            }

            ExceptionInfo exceptionInfo = new ExceptionInfo();
            path = exceptionInfo.getRegProjectPath(); //项目登记路径
            filenameTemp = path + UidFile + ".txt";

            flag = TxtHandle.creatTxtFile(UidFile,path); //创建txt文档状态
            if (flag){
                exceptionStr = "<tr>"
                             + "<td>"
                             + ""+resultCode+""
                             + "</td>"
                             + "<td>"
                             + ""+resultMsg+""
                             + "</td>"
                             + "<td>"
                             + ""+merchantID+""
                             + "</td>"
                             + "<td>"
                             + ""+productId+""
                             + "</td>"
                             + "<td>"
                             + ""+merBillNo+""
                             + "</td>"
                             + "<td>"
                             + ""+ipsBillNo+""
                             + "</td>"
                             + "<td>"
                             + ""+ipsDoTime+""
                             + "</td>"
                             + "<td>"
                             + ""+projectNo+""
                             + "</td>"
                             + "<td>"
                             + ""+status+""
                             + "</td>"
                             + "</tr>";

                TxtHandle.writeTxtFile(filenameTemp,exceptionStr); //写入文件

                HuanxunFile huanxunFile = new HuanxunFile(); //异常文件
                huanxunFile.setId(Uid); //主键
                huanxunFile.setType("4"); //类型
                huanxunFile.setUrl(filenameTemp); //文件路径
                huanxunFile.setCreateuser(""); //创建人
                huanxunFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

                HuanXunExceptionFile.saveInfo(huanxunFile); //保存文件路径
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
