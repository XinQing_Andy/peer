package com.mh.peer.exception;

import com.mh.peer.model.entity.HuanxunFile;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-8-29.
 * 环迅充值
 */
public class HuanXunReChargeException {

    /**
     * 环迅充值
     * @param map
     */
    public static void handle(Map<String,Object> map){
        Object memberId = ""; //会员id
        Object glId = ""; //关联id
        Object source = ""; //来源(0-电脑端、1-手机端)
        Object resultCode = ""; //返回编号(000000-成功、999999-失败)
        Object resultMsg = ""; //返回信息
        Object merchantID = ""; //商户存管交易账号
        Object sign = ""; //签名
        String response = ""; //响应信息
        String responseJm = ""; //响应信息(解密)
        Map<String,Object> mapChargeProject = new HashMap<String,Object>();
        Object merBillNo = ""; //商户订单号
        Object depositType = ""; //充值类型(1-普通充值、2-还款充值)
        Object channelType = ""; //渠道种类(1-个人网银、2-企业网银)
        Object bankCode = ""; //充值银行
        Object ipsBillNo = ""; //IPS订单号
        Object ipsDoTime = ""; //IPS处理时间
        Object ipsTrdAmt = ""; //充值金额
        Object ipsFee = ""; //IPS手续费金额
        Object merFee = ""; //平台手续费
        Object trdStatus = ""; //充值状态(0-失败、1-成功、2-处理中)
        String path = ""; //充值路径
        String UidFile = UUID.randomUUID().toString().replaceAll("-",""); //文件主键
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
        String filenameTemp = ""; //附件路径
        boolean flag = true; //创建txt文件状态
        String exceptionStr = ""; //异常信息

        try{

            if (map != null){
                memberId = map.get("memberId"); //会员id
                glId = map.get("glId"); //关联id
                source = map.get("source"); //来源(0-电脑端、1-手机端)
                resultCode = map.get("resultCode"); //返回编号(000000-成功、999999-失败)
                resultMsg = map.get("resultMsg"); //返回信息
                merchantID = map.get("merchantID"); //商户存管交易账号
                sign = map.get("sign"); //签名

                if (map.get("response") != null){
                    response = (String) map.get("response"); //响应信息
                    if (response != null && !response.equals("")){
                        responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                        if (responseJm != null && !responseJm.equals("")){
                            mapChargeProject = JsonHelper.getObjectToMap2(responseJm);
                            if (mapChargeProject != null) {
                                merBillNo = mapChargeProject.get("merBillNo"); //商户订单号
                                depositType = mapChargeProject.get("depositType"); //充值类型(1-普通充值、2-还款充值)
                                channelType = mapChargeProject.get("channelType"); //渠道种类(1-个人网银、2-企业网银)
                                bankCode = mapChargeProject.get("bankCode"); //充值银行
                                ipsBillNo = mapChargeProject.get("ipsBillNo"); //IPS订单号
                                ipsDoTime = mapChargeProject.get("ipsDoTime"); //IPS处理时间
                                ipsTrdAmt = mapChargeProject.get("ipsTrdAmt"); //IPS充值金额
                                ipsFee = mapChargeProject.get("ipsFee"); //IPS手续费金额
                                merFee = mapChargeProject.get("merFee"); //平台手续费
                                trdStatus = mapChargeProject.get("trdStatus"); //充值状态(0-失败、1-成功、2-处理中)
                            }
                        }
                    }
                }

                ExceptionInfo exceptionInfo = new ExceptionInfo();
                path = exceptionInfo.getReChargePath(); //充值路径
                filenameTemp = path + UidFile + ".txt";

                flag = TxtHandle.creatTxtFile(UidFile, path); //创建txt文档状态
                if (flag){
                    exceptionStr = "<tr>"
                                 + "<td>"
                                 + ""+memberId+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+glId+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+source+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+resultCode+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+resultMsg+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+merchantID+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+merBillNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+depositType+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+channelType+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+bankCode+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsBillNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsDoTime+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsTrdAmt+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsFee+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+merFee+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+trdStatus+""
                                 + "</td>"
                                 + "</tr>";

                    TxtHandle.writeTxtFile(filenameTemp,exceptionStr); //写入文件

                    HuanxunFile huanxunFile = new HuanxunFile(); //异常文件
                    huanxunFile.setId(Uid); //主键
                    huanxunFile.setType((String)depositType); //类型
                    huanxunFile.setUrl(filenameTemp); //文件路径
                    huanxunFile.setCreateuser((String)memberId); //创建人
                    huanxunFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

                    HuanXunExceptionFile.saveInfo(huanxunFile); //保存文件路径
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
