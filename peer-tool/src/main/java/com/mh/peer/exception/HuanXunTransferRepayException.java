package com.mh.peer.exception;

import com.mh.peer.model.entity.HuanxunFile;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import java.util.*;

/**
 * Created by zhangerxin on 2016-9-5.
 * 还款转账部分
 */
public class HuanXunTransferRepayException {

    /**
     * 还款转账部分异常信息
     * @param map
     */
    public static void handle(Map<String,Object> map){
        String freezeRepayId = ""; //还款冻结主键
        String resultCode = ""; //响应状态
        String resultMsg = ""; //响应信息描述
        String response = ""; //响应信息
        String responseJm = ""; //响应信息(加密)
        Object batchNo = ""; //商户转账批次号
        Object projectNo = ""; //项目ID号
        Object transferType = ""; //转账类型
        String transferAccDetail = ""; //转账明细集合
        Object merBillNo = ""; //商户订单号
        Object outIpsAcctNo = ""; //转出方IPS存管账户号
        Object inIpsAcctNo = ""; //转入方IPS存管账号
        Object ipsBillNo = ""; //IPS订单号
        Object ipsDoTime = ""; //IPS处理时间
        Object ipsTrdAmt = ""; //IPS转账金额(String型)
        Object trdStatus = ""; //转账状态(0-失败、1-成功)
        Map mapTransferRepay = new HashMap(); //还款转账部分
        List<Map<String,Object>> listTransferDetails = new ArrayList<Map<String,Object>>(); //转账详细信息集合
        String path = ""; //文件解冻目录
        String filenameTemp = ""; //文件路径
        boolean flag = true; //创建txt文档状态
        String exceptionStr = ""; //异常信息
        String UidFile = ""; //异常信息附件主键
        String Uid = ""; //异常信息附件表主键

        try{

            if (map != null){
                resultCode = (String) map.get("resultCode"); //响应状态
                resultMsg = (String) map.get("resultMsg"); //响应信息描述
                response = (String) map.get("response"); //响应信息
                freezeRepayId = (String) map.get("freezeRepayId"); //还款冻结主键
                listTransferDetails = (List<Map<String, Object>>) map.get("listTransferDetail"); //转账集合
                System.out.println("转账明细集合："+listTransferDetails);

                if (response != null && !response.equals("")){ //响应信息
                    responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                    if (responseJm != null && !responseJm.equals("")){
                        mapTransferRepay = JsonToMap.getMapFromJsonObjStr(responseJm); //投资转账
                        if (mapTransferRepay != null) {
                            batchNo = mapTransferRepay.get("batchNo"); //商户转账批次号
                            projectNo = mapTransferRepay.get("projectNo"); //项目ID号
                            transferType = mapTransferRepay.get("transferType"); //转账类型(3-还款)
                        }
                    }
                }

                if (listTransferDetails != null && listTransferDetails.size() > 0){
                    for (Map<String, Object> mapTransferDetail:listTransferDetails){
                        merBillNo = mapTransferDetail.get("merBillNo"); //商户订单号
                        outIpsAcctNo = mapTransferDetail.get("outIpsAcctNo"); //转出方IPS存管账户号
                        inIpsAcctNo = mapTransferDetail.get("inIpsAcctNo"); //转入方IPS存管账号
                        ipsBillNo = mapTransferDetail.get("ipsBillNo"); //IPS订单号
                        ipsDoTime = mapTransferDetail.get("ipsDoTime"); //IPS处理时间
                        ipsTrdAmt = mapTransferDetail.get("ipsTrdAmt"); //IPS转账金额
                        trdStatus = mapTransferDetail.get("trdStatus"); //转账状态(0-失败、1-成功)
                        exceptionStr = "<tr>"
                                + "<td>"
                                + ""+resultCode+""
                                + "</td>"
                                + "<td>"
                                + ""+resultMsg+""
                                + "</td>"
                                + "<td>"
                                + ""+freezeRepayId+""
                                + "</td>"
                                + "<td>"
                                + ""+outIpsAcctNo+""
                                + "</td>"
                                + "<td>"
                                + ""+inIpsAcctNo+""
                                + "</td>"
                                + "<td>"
                                + ""+ipsBillNo+""
                                + "</td>"
                                + "<td>"
                                + ""+ipsDoTime+""
                                + "</td>"
                                + "<td>"
                                + ""+ipsTrdAmt+""
                                + "</td>"
                                + "<td>"
                                + ""+trdStatus+""
                                + "</td>"
                                + "</tr>";
                    }
                }else{
                    exceptionStr = "<tr>"
                            + "<td>"
                            + ""+resultCode+""
                            + "</td>"
                            + "<td>"
                            + ""+resultMsg+""
                            + "</td>"
                            + "<td>"
                            + ""+freezeRepayId+""
                            + "</td>"
                            + "</tr>";
                }

                UidFile = UUID.randomUUID().toString().replaceAll("-",""); //异常信息附件表主键
                ExceptionInfo exceptionInfo = new ExceptionInfo();
                path = exceptionInfo.getRepayTransferPath(); //还款转账目录
                filenameTemp = path + UidFile + ".txt";

                flag = TxtHandle.creatTxtFile(UidFile, path); //创建txt文档状态
                TxtHandle.writeTxtFile(filenameTemp,exceptionStr); //写入文件
                HuanxunFile huanxunFile = new HuanxunFile(); //异常文件
                Uid = UUID.randomUUID().toString().replaceAll("-",""); //异常信息附件表主键
                huanxunFile.setId(Uid); //主键
                huanxunFile.setType("7"); //类型(转账)
                huanxunFile.setUrl(filenameTemp); //文件路径
                huanxunFile.setCreateuser(""); //创建人
                huanxunFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

                HuanXunExceptionFile.saveInfo(huanxunFile); //保存文件路径
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
