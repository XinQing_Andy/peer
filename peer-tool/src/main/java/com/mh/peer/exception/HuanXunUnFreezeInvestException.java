package com.mh.peer.exception;

import com.mh.peer.model.entity.HuanxunFile;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-8-10.
 * 解冻部分异常类
 */
public class HuanXunUnFreezeInvestException {


    /**
     * 处理项目解冻异常
     * @param map
     */
    public static void handle(Map<String,Object> map){
        System.out.println("进入解冻异常处理");
        Object id = ""; //表主键
        Object resultCode = ""; //响应码(000000-成功、000001~999999-失败)
        Object resultMsg = ""; //响应信息描述
        Object merchantID = ""; //商户存管交易账号
        String response = ""; //响应信息
        String responseJm = ""; //响应信息(解密)
        Object merBillNo = ""; //商户订单号
        Object projectNo = ""; //项目ID号
        Object freezeId = ""; //原IPS冻结订单号
        Object merFee = ""; //平台手续费
        Object ipsAcctNo = ""; //解冻账号
        Object ipsBillNo = ""; //IPS订单号
        Object ipsDoTime = ""; //IPS处理时间
        Object trdStatus = ""; //解冻状态
        String path = ""; //文件解冻目录
        String filenameTemp = ""; //文件路径
        boolean flag = true; //创建txt文档状态
        String exceptionStr = ""; //异常信息
        String UidFile = UUID.randomUUID().toString().replaceAll("-",""); //文件主键
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
        Map<String,String> mapUnFreezeInvest = new HashMap<String,String>();

        try{

            if (map != null){
                id = map.get("id"); //冻结表主键
                resultCode = map.get("resultCode"); //响应码(000000-成功、000001~999999-失败)
                resultMsg = map.get("resultMsg"); //响应信息描述
                merchantID = map.get("merchantID"); //商户存管交易账号
                response = (String) map.get("response"); //响应信息

                if (response != null && !response.equals("")){
                    responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                    if (responseJm != null && !responseJm.equals("")){
                        mapUnFreezeInvest = JsonHelper.getObjectToMap(responseJm); //投资解冻
                        if (mapUnFreezeInvest != null) {
                            merBillNo = mapUnFreezeInvest.get("merBillNo"); //商户订单号
                            projectNo = mapUnFreezeInvest.get("projectNo"); //项目ID号
                            freezeId = mapUnFreezeInvest.get("freezeId"); //原IPS冻结订单号
                            merFee = mapUnFreezeInvest.get("merFee"); //平台手续费
                            ipsAcctNo = mapUnFreezeInvest.get("ipsAcctNo"); //解冻账号
                            ipsBillNo = mapUnFreezeInvest.get("ipsBillNo"); //IPS订单号
                            ipsDoTime = mapUnFreezeInvest.get("ipsDoTime"); //IPS处理时间
                            trdStatus = mapUnFreezeInvest.get("trdStatus"); //解冻状态
                        }
                    }
                }

                ExceptionInfo exceptionInfo = new ExceptionInfo();
                path = exceptionInfo.getUnInvestFreezePath(); //产品解冻目录
                filenameTemp = path + UidFile + ".txt";

                flag = TxtHandle.creatTxtFile(UidFile, path); //创建txt文档状态
                if (flag){
                    exceptionStr = "<tr>"
                                 + "<td>"
                                 + ""+id+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+resultCode+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+resultMsg+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+merchantID+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+merBillNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+projectNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+freezeId+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+merFee+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsAcctNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsBillNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsDoTime+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+trdStatus+""
                                 + "</td>"
                                 + "</tr>";
                }
            }

            TxtHandle.writeTxtFile(filenameTemp,exceptionStr); //写入文件
            HuanxunFile huanxunFile = new HuanxunFile(); //异常文件
            huanxunFile.setId(Uid); //主键
            huanxunFile.setType("6"); //类型(解冻)
            huanxunFile.setUrl(filenameTemp); //文件路径
            huanxunFile.setCreateuser(""); //创建人
            huanxunFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

            HuanXunExceptionFile.saveInfo(huanxunFile); //保存文件路径
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
