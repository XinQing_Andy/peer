package com.mh.peer.exception;

import com.mh.peer.model.entity.HuanxunFile;
import com.mh.peer.util.ExceptionInfo;
import com.mh.peer.util.JsonHelper;
import com.mh.peer.util.Pdes;
import com.mh.peer.util.TxtHandle;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-8-12.
 * 债权部分异常
 */
public class HuanXunFreezeAttornException {

    /**
     * 债权冻结部分
     * @param map
     */
    public static void handle(Map<String,Object> map){
        System.out.println("进入债权冻结异常处理");
        String attornId = ""; //债权id
        Object resultCode = ""; //响应码
        Object resultMsg = ""; //响应信息描述
        String response = ""; //响应信息
        String responseJm = ""; //响应信息解密
        String projectNo = ""; //项目ID号
        String ipsTrdAmt = ""; //IPS冻结金额
        String ipsAcctNo = ""; //冻结账号
        String otherIpsAcctNo = ""; //它方账号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String trdStatus = ""; //冻结状态(0-失败、1-成功)
        boolean flag = true; //创建txt文件状态
        String path = ""; //异常文件路径
        String filenameTemp = ""; //文件
        String exceptionStr = ""; //异常信息
        String UidFile = UUID.randomUUID().toString().replaceAll("-",""); //文件主键
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
        Map<String,String> mapRegFreezeAttorn = new HashMap<String,String>(); //债权部分

        try{

            if (map != null){
                resultCode = map.get("resultCode"); //响应码
                resultMsg = map.get("resultMsg"); //响应信息描述
                response = (String) map.get("response"); //响应信息
                if (response != null && !response.equals("")){
                    responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                    if (responseJm != null && !responseJm.equals("")){
                        mapRegFreezeAttorn = JsonHelper.getObjectToMap(responseJm); //债权冻结
                        if (mapRegFreezeAttorn != null){
                            projectNo = mapRegFreezeAttorn.get("projectNo"); //项目ID号
                            ipsTrdAmt = mapRegFreezeAttorn.get("ipsTrdAmt"); //IPS冻结金额
                            ipsAcctNo = mapRegFreezeAttorn.get("ipsAcctNo"); //冻结账号
                            otherIpsAcctNo = mapRegFreezeAttorn.get("otherIpsAcctNo"); //它方账号
                            ipsBillNo = mapRegFreezeAttorn.get("ipsBillNo"); //IPS订单号
                            ipsDoTime = mapRegFreezeAttorn.get("ipsDoTime"); //IPS处理时间
                            trdStatus = mapRegFreezeAttorn.get("trdStatus"); //冻结状态(0-失败、1-成功)
                        }
                    }
                }

                ExceptionInfo exceptionInfo = new ExceptionInfo();
                path = exceptionInfo.getAttornFreezePath(); //债权冻结路径
                filenameTemp = path + UidFile + ".txt";

                flag = TxtHandle.creatTxtFile(UidFile, path); //创建txt文档状态
                if (flag){
                    exceptionStr = "<tr>"
                                 + "<td>"
                                 + ""+resultCode+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+resultMsg+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+projectNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsTrdAmt+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsAcctNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+otherIpsAcctNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsBillNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsDoTime+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+trdStatus+""
                                 + "</td>"
                                 + "</tr>";
                }

                TxtHandle.writeTxtFile(filenameTemp,exceptionStr); //写入文件

                HuanxunFile huanxunFile = new HuanxunFile(); //异常文件
                huanxunFile.setId(Uid); //主键
                huanxunFile.setType("8"); //类型
                huanxunFile.setUrl(filenameTemp); //文件路径
                huanxunFile.setCreateuser(""); //创建人
                huanxunFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
