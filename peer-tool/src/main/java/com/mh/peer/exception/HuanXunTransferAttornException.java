package com.mh.peer.exception;

import com.mh.peer.model.entity.HuanxunFile;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import java.util.*;

/**
 * Created by zhangerxin on 2016-8-12.
 * 债权部分转账
 */
public class HuanXunTransferAttornException {

    /**
     * 债权转账部分
     * @param map
     */
    public static void handle(Map<String,Object> map){
        System.out.println("进入债权转账异常处理");
        Object attornId = ""; //债权id
        String resultCode = ""; //响应状态
        String resultMsg = ""; //响应信息描述
        String response = ""; //响应信息
        String responseJm = ""; //响应信息(加密)
        Object batchNo = ""; //商户转账批次号
        Object projectNo = ""; //项目ID号
        Object transferType = ""; //转账类型
        String transferAccDetail = ""; //转账明细集合
        Object merBillNo = ""; //商户订单号
        Object outIpsAcctNo = ""; //转出方IPS存管账户号
        Object inIpsAcctNo = ""; //转入方IPS存管账号
        Object ipsBillNo = ""; //IPS订单号
        Object ipsDoTime = ""; //IPS处理时间
        Object ipsTrdAmt = ""; //IPS转账金额(String型)
        Object trdStatus = ""; //转账状态(0-失败、1-成功)
        Map mapTransferAttorn = new HashMap(); //投资转账部分
        List<Map<String,Object>> listTransferDetails = new ArrayList<Map<String,Object>>(); //转账详细信息集合
        String path = ""; //文件解冻目录
        String filenameTemp = ""; //文件路径
        boolean flag = true; //创建txt文档状态
        String exceptionStr = ""; //异常信息
        String UidFile = UUID.randomUUID().toString().replaceAll("-",""); //文件主键
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键

        try{

            if (map != null){
                resultCode = (String) map.get("resultCode"); //响应状态
                resultMsg = (String) map.get("resultMsg"); //响应信息描述
                response = (String) map.get("response"); //响应信息
                if (response != null && !response.equals("")){
                    responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                    mapTransferAttorn = JsonToMap.getMapFromJsonObjStr(responseJm); //债权转账
                    if (mapTransferAttorn != null) {
                        batchNo = mapTransferAttorn.get("batchNo"); //商户转账批次号
                        projectNo = mapTransferAttorn.get("projectNo"); //项目ID号
                        transferType = mapTransferAttorn.get("transferType"); //转账类型
                        attornId = mapTransferAttorn.get("attornId"); //债权id
                        listTransferDetails = (List<Map<String, Object>>) map.get("listTransferDetail"); //债权转账集合
                    }
                }

                if (listTransferDetails != null && listTransferDetails.size() > 0){
                    for (Map<String, Object> mapTransferDetail:listTransferDetails){
                        merBillNo = mapTransferDetail.get("merBillNo"); //商户订单号
                        outIpsAcctNo = mapTransferDetail.get("outIpsAcctNo"); //转出方IPS存管账户号
                        inIpsAcctNo = mapTransferDetail.get("inIpsAcctNo"); //转入方IPS存管账号
                        ipsBillNo = mapTransferDetail.get("ipsBillNo"); //IPS订单号
                        ipsDoTime = mapTransferDetail.get("ipsDoTime"); //IPS处理时间
                        ipsTrdAmt = mapTransferDetail.get("ipsTrdAmt"); //IPS转账金额
                        trdStatus = mapTransferDetail.get("trdStatus"); //转账状态(0-失败、1-成功)
                        exceptionStr = "<tr>"
                                     + "<td>"
                                     + ""+resultCode+""
                                     + "</td>"
                                     + "<td>"
                                     + ""+resultMsg+""
                                     + "</td>"
                                     + "<td>"
                                     + ""+attornId+""
                                     + "</td>"
                                     + "<td>"
                                     + ""+outIpsAcctNo+""
                                     + "</td>"
                                     + "<td>"
                                     + ""+inIpsAcctNo+""
                                     + "</td>"
                                     + "<td>"
                                     + ""+ipsBillNo+""
                                     + "</td>"
                                     + "<td>"
                                     + ""+ipsDoTime+""
                                     + "</td>"
                                     + "<td>"
                                     + ""+ipsTrdAmt+""
                                     + "</td>"
                                     + "<td>"
                                     + ""+trdStatus+""
                                     + "</td>"
                                     + "</tr>";
                    }
                }else{
                    exceptionStr = "<tr>"
                                 + "<td>"
                                 + ""+resultCode+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+resultMsg+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+attornId+""
                                 + "</td>"
                                 + "</tr>";
                }

                ExceptionInfo exceptionInfo = new ExceptionInfo();
                path = exceptionInfo.getAttornTransferPath(); //债权转账目录
                filenameTemp = path + UidFile + ".txt";

                flag = TxtHandle.creatTxtFile(UidFile, path); //创建txt文档状态
                TxtHandle.writeTxtFile(filenameTemp,exceptionStr); //写入文件
                HuanxunFile huanxunFile = new HuanxunFile(); //异常文件
                huanxunFile.setId(Uid); //主键
                huanxunFile.setType("10"); //类型(转账)
                huanxunFile.setUrl(filenameTemp); //文件路径
                huanxunFile.setCreateuser(""); //创建人
                huanxunFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

                HuanXunExceptionFile.saveInfo(huanxunFile); //保存文件路径
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
