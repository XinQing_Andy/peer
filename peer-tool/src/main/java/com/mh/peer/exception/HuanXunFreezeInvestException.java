package com.mh.peer.exception;

import com.mh.peer.model.entity.HuanxunFile;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-8-10.
 * 投资冻结部分
 */
public class HuanXunFreezeInvestException extends Exception{

    /**
     * 进入投资冻结异常处理
     * @param map
     */
    public static void handle(Map<String,Object> map){
        Object productId = map.get("productId"); //产品id
        Object memberId = map.get("memberId"); //会员id
        Object resultCode = map.get("resultCode"); //响应码
        Object resultMsg = map.get("resultMsg"); //响应内容
        String response = (String) map.get("response"); //响应信息
        String responseJm = ""; //响应信息(解密)
        Object merchantID = ""; //商户存管交易账号
        Object merBillNo = ""; //商户订单号
        Object projectNo = ""; //项目ID号
        Object ipsTrdAmt = ""; //冻结金额
        Object ipsAcctNo = ""; //冻结账号
        Object otherIpsAcctNo = ""; //它方账号
        Object ipsBillNo = ""; //IPS订单号
        Object ipsDoTime = ""; //IPS处理时间
        Object trdStatus = ""; //冻结状态
        boolean flag = true; //创建txt文件状态
        String filenameTemp = ""; //文件
        String exceptionStr = ""; //异常信息
        String path = ""; //异常文件路径
        String UidFile = UUID.randomUUID().toString().replaceAll("-",""); //文件主键
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
        Map<String,String> mapRegFreezeInvest = new HashMap<String,String>();

        try{

            if (response != null && !response.equals("")){
                responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                if (responseJm != null && !responseJm.equals("")){
                    mapRegFreezeInvest = JsonHelper.getObjectToMap(responseJm); //投资冻结
                    if (mapRegFreezeInvest != null){
                        merchantID = mapRegFreezeInvest.get("merchantID"); //商户存管交易账号
                        merBillNo = mapRegFreezeInvest.get("merBillNo"); //商户订单号
                        projectNo = mapRegFreezeInvest.get("projectNo"); //项目ID号
                        ipsTrdAmt = mapRegFreezeInvest.get("ipsTrdAmt"); //冻结金额
                        ipsAcctNo = mapRegFreezeInvest.get("ipsAcctNo"); //冻结账号
                        otherIpsAcctNo = mapRegFreezeInvest.get("otherIpsAcctNo"); //它方账号
                        ipsBillNo = mapRegFreezeInvest.get("ipsBillNo"); //IPS订单号
                        ipsDoTime = mapRegFreezeInvest.get("ipsDoTime"); //IPS处理时间
                        trdStatus = mapRegFreezeInvest.get("trdStatus"); //冻结状态
                    }
                }
            }

            ExceptionInfo exceptionInfo = new ExceptionInfo();
            path = exceptionInfo.getInvestFreezePath(); //产品冻结路径路径
            filenameTemp = path + UidFile + ".txt";

            flag = TxtHandle.creatTxtFile(UidFile, path); //创建txt文档状态
            if (flag){
                exceptionStr = "<tr>"
                             + "<td>"
                             + ""+resultCode+""
                             + "</td>"
                             + "<td>"
                             + ""+resultMsg+""
                             + "</td>"
                             + "<td>"
                             + ""+productId+""
                             + "</td>"
                             + "<td>"
                             + ""+memberId+""
                             + "</td>"
                             + "<td>"
                             + ""+merchantID+""
                             + "</td>"
                             + "<td>"
                             + ""+merBillNo+""
                             + "</td>"
                             + "<td>"
                             + ""+projectNo+""
                             + "</td>"
                             + "<td>"
                             + ""+ipsTrdAmt+""
                             + "</td>"
                             + "<td>"
                             + ""+ipsAcctNo+""
                             + "</td>"
                             + "<td>"
                             + ""+otherIpsAcctNo+""
                             + "</td>"
                             + "<td>"
                             + ""+ipsBillNo+""
                             + "</td>"
                             + "<td>"
                             + ""+ipsDoTime+""
                             + "</td>"
                             + "<td>"
                             + ""+trdStatus+""
                             + "</td>"
                             + "</tr>";
            }

            TxtHandle.writeTxtFile(filenameTemp,exceptionStr); //写入文件

            HuanxunFile huanxunFile = new HuanxunFile(); //异常文件
            huanxunFile.setId(Uid); //主键
            huanxunFile.setType("5"); //类型
            huanxunFile.setUrl(filenameTemp); //文件路径
            huanxunFile.setCreateuser((String) memberId); //创建人
            huanxunFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

            HuanXunExceptionFile.saveInfo(huanxunFile); //保存文件路径
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
