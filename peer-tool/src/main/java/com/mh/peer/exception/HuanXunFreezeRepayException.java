package com.mh.peer.exception;

import com.mh.peer.model.entity.HuanxunFile;
import com.mh.peer.util.ExceptionInfo;
import com.mh.peer.util.HuanXunExceptionFile;
import com.mh.peer.util.TxtHandle;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-9-5.
 * 还款冻结部分
 */
public class HuanXunFreezeRepayException {

    public static void handle(Map<String,Object> map){
        Object resultCode = ""; //响应码
        Object resultMsg = ""; //响应信息
        Object merBillNo = ""; //商户订单号
        Object projectNo = ""; //项目ID号
        Object ipsTrdAmt = ""; //冻结金额
        Object ipsAcctNo = ""; //IPS冻结账号
        Object otherIpsAcctNo = ""; //它方账号
        Object ipsBillNo = ""; //IPS订单号
        Object ipsDoTime = ""; //IPS处理时间
        String filenameTemp = ""; //附件路径
        boolean flag = true; //创建txt文件状态
        String exceptionStr = ""; //异常信息
        String path = ""; //文件路径
        String UidFile = UUID.randomUUID().toString().replaceAll("-",""); //文件主键
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
        String depositType = "11"; //类型

        try{

            if (map != null){
                resultCode = map.get("resultCode"); //响应码
                resultMsg = map.get("resultMsg"); //响应信息
                merBillNo = map.get("merBillNo"); //商户订单号
                projectNo = map.get("projectNo"); //项目ID号
                ipsTrdAmt = map.get("ipsTrdAmt"); //冻结金额
                ipsAcctNo = map.get("ipsAcctNo"); ///IPS冻结账号
                otherIpsAcctNo = map.get("otherIpsAcctNo"); //它方账号
                ipsBillNo = map.get("ipsBillNo"); //IPS订单号
                ipsDoTime = map.get("ipsDoTime"); //IPS处理时间

                ExceptionInfo exceptionInfo = new ExceptionInfo();
                path = exceptionInfo.getRepayFreezePath(); //还款冻结路径
                filenameTemp = path + UidFile + ".txt";

                flag = TxtHandle.creatTxtFile(UidFile, path); //创建txt文档状态
                if (flag){
                    exceptionStr = "<tr>"
                                 + "<td>"
                                 + ""+resultCode+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+resultMsg+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+merBillNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+projectNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsTrdAmt+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsAcctNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+otherIpsAcctNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsBillNo+""
                                 + "</td>"
                                 + "<td>"
                                 + ""+ipsDoTime+""
                                 + "</td>"
                                 + "</tr>";

                    TxtHandle.writeTxtFile(filenameTemp,exceptionStr); //写入文件

                    HuanxunFile huanxunFile = new HuanxunFile(); //异常文件
                    huanxunFile.setId(Uid); //主键
                    huanxunFile.setType((String)depositType); //类型
                    huanxunFile.setUrl(filenameTemp); //文件路径
                    huanxunFile.setCreateuser(""); //创建人
                    huanxunFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

                    HuanXunExceptionFile.saveInfo(huanxunFile); //保存文件路径
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
