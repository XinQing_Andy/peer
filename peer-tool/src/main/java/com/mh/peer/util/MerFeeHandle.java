package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;

/**
 * Created by zhangerxin on 2016-9-12.
 * 服务费部分扣取
 */
public class MerFeeHandle {

    /**
     * 提现时扣取服务费金额
     * @param memberId
     * @param trdAmt
     * @return
     */
    public static String getPostalFee(String memberId,String trdAmt){
        Double trdAmtDou = 0.0d; //提现金额(Double型)
        String chagrePrice = ""; //充值金额(String型)
        Double chargePriceDou = 0.0d; //充值金额(Double型)
        String investPrice = ""; //投资金额(String型)
        Double investPriceDou = 0.0d; //投资金额(Double型)
        String merFee = ""; //手续费(String型)
        Double merFeeDou = 0.0d; //手续费(Double型)
        //充值金额查询
        String sqlCharge = "SELECT IFNULL(SUM(IPSTRDAMT),0)CHARGEPRICE FROM huanxun_recharge WHERE 1 = 1"
                         + " AND MEMBERID = '"+memberId+"' AND RESULTCODE = '000000' AND DEPOSITTYPE = '1'";
        //投资金额查询
        String sqlInvest = "SELECT IFNULL(SUM(PRICE),0)INVESTPRICE FROM view_product_buy_min WHERE 1 = 1"
                         + " AND BUYER = '"+memberId+"' AND SHFLAG = '0'";

        try{

            if (trdAmt != null && !trdAmt.equals("")){ //提现金额(String型)
                trdAmtDou = TypeTool.getDouble(trdAmt); //提现金额(Double型)
            }

            DMap dMapCharge = DaoTool.select(sqlCharge);
            if (dMapCharge != null && dMapCharge.getCount() > 0){
                chagrePrice = dMapCharge.getValue("CHARGEPRICE",0); //充值金额(String型)
                if (chagrePrice != null && !chagrePrice.equals("")){
                    chargePriceDou = TypeTool.getDouble(chagrePrice); //充值金额(Double型)
                }
                DMap dMapInvest = DaoTool.select(sqlInvest); //投资金额(String型)
                if (dMapInvest != null && dMapInvest.getCount() > 0){
                    investPrice = dMapInvest.getValue("INVESTPRICE",0); //投资金额(String型)
                    if (investPrice != null && !investPrice.equals("")){
                        investPriceDou = TypeTool.getDouble(investPrice); //投资金额(Double型)
                    }
                }
                //手续费(Double型)
                merFeeDou = (chargePriceDou - investPriceDou) *  2.5 / 1000 * trdAmtDou / (chargePriceDou - investPriceDou);
                merFeeDou = PublicsTool.round(merFeeDou,2);
                if (merFeeDou < 0){
                    merFeeDou = 0.0d;
                }
                merFee = TypeTool.getString(merFeeDou); //手续费(String型)
            }else{
                merFee = "0"; //手续费(String型)
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return merFee;
    }


    /**
     * 获取还款时转入方的服务费
     * @param memberId
     * @param interest
     * @return
     */
    public static String getRepayInFee(String memberId,String interest){
        String sqlMember = "SELECT POINTS FROM member_info WHERE 1 = 1"; //会员信息获取
        String sqlPoints = "SELECT * FROM points_wh"; //积分信息获取
        Double interestDou = 0.0d; //利息(Double型)
        String points = ""; //积分(String型)
        Double pointsDou = 0.0d; //积分(Double型)
        String spoints = ""; //起始积分(String型)
        Double spointsDou = 0.0d; //起始积分(Double型)
        String epoints = ""; //截止积分(String型)
        Double epointsDou = 0.0d; //截止积分(Double型)
        String standard = ""; //标准(String型)
        Double standardDou = 0.0d; //标准(Double型)
        String inMerFee = ""; //还款转入方服务费(String型)
        Double inMerFeeDou = 0.0d; //还款转入方服务费(Double型)

        try{

            if (memberId != null && !memberId.equals("")){ //会员id
                sqlMember = sqlMember + " AND ID = '"+memberId+"'";
                DMap dMapMember = DaoTool.select(sqlMember); //会员信息
                if (dMapMember != null && dMapMember.getCount() > 0){
                    points = dMapMember.getValue("POINTS",0); //积分(String型)
                    if (points != null && !points.equals("")){
                        pointsDou = TypeTool.getDouble(points); //积分(Double型)
                    }
                    DMap dMapPoints = DaoTool.select(sqlPoints); //积分信息
                    if (dMapPoints != null && dMapPoints.getCount() > 0){
                        for (int i=0;i<dMapPoints.getCount();i++){
                            spoints = dMapPoints.getValue("SPOINTS",i); //起积分(String型)
                            if (spoints != null && !spoints.equals("")){
                                spointsDou = TypeTool.getDouble(spoints); //起积分(Double型)
                            }
                            epoints = dMapPoints.getValue("EPOINTS",i); //止积分(String型)
                            if (epoints != null && !epoints.equals("")){
                                epointsDou = TypeTool.getDouble(epoints);
                            }

                            if (epointsDou < 120000){
                                if (pointsDou >= spointsDou && spointsDou <= epointsDou){
                                    standard = dMapPoints.getValue("STANDARD",0); //标准(String型)
                                }
                            }else{
                                if (pointsDou >= epointsDou){
                                    standard = dMapPoints.getValue("STANDARD",0); //标准(String型)
                                }
                            }
                        }

                        if (standard != null && !standard.equals("")){ //标准(String型)
                            standardDou = TypeTool.getDouble(standard); //标准(Double型)
                        }

                        System.out.println("标准为standard======"+standardDou);

                        if (interest != null && !interest.equals("")){ //利息(String型)
                            interestDou = TypeTool.getDouble(interest); //利息(Double型)
                        }
                        System.out.println("利息为interestDou======"+interestDou);
                        inMerFeeDou = interestDou * standardDou; //转入方服务费(Double型)
                        inMerFeeDou = PublicsTool.round(inMerFeeDou,2);
                        inMerFee = TypeTool.getString(inMerFeeDou); //转入方服务费(String型)
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return inMerFee;
    }
}
