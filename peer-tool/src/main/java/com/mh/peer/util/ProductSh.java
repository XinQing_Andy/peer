package com.mh.peer.util;

import java.io.IOException;
import java.sql.*;

/**
 * Created by zhangerxin on 2016-5-19.
 */
public class ProductSh {
    public Connection conn = null;
    public String url;
    public String user;
    public String password;
    public String driver;

    public ProductSh() throws IOException, ClassNotFoundException {
        driver = "com.mysql.jdbc.Driver";
        url = "jdbc:mysql://120.76.137.147/peer?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true";
        user = "peer";
        password = "peerandy";
        Class.forName(driver);
    }

    /**
     * 获取批次编号
     *
     * @author 张尔鑫
     */
    public String execute(String productId) throws Exception {
        String pcbh = "";
        Connection conn = null;
        CallableStatement cs = null;
        try {
            conn = this.getConnection();
            cs = conn.prepareCall("{?=call function_ProductSh(?)}");
            cs.setString(2, productId);
            cs.registerOutParameter(1, Types.VARCHAR);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pcbh;
    }

    /**
     * 连接数据库
     *
     * @return
     * @throws SQLException
     */
    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }
}
