package com.mh.peer.util;

import com.salon.frame.util.TypeTool;
import sun.misc.BASE64Decoder;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * 工具类
 * Created by WangMeng on 2015-05-14.
 */
public class PublicsTool {
    /**
     * 密码转换
     *
     * @param @param  pw
     * @param @return 设定文件
     * @return String 返回类型
     * @throws
     * @Title: tranPassword
     * @Description: TODO
     * @author WangMeng
     */
    public static String tranPassword(String pw) {
        if (pw.endsWith("↑")) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < pw.length() / 3; i++)
                sb.append((char) Integer.parseInt(pw.substring(i * 3,
                        (i + 1) * 3)));
            return sb.toString();
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < pw.length(); i++)
            sb.append(fill0("" + (pw.charAt(i) & 0xFF), 3));
        return sb.toString() + "↑";
    }

    /**
     * 密码校验
     *
     * @param @param  pw
     * @param @return 设定文件
     * @return String 返回类型
     * @throws
     * @Title: isPassword
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @author WangMeng
     */
    public static String isPassword(String pw) {
        if (pw.endsWith("↑")) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < pw.length() / 3; i++)
                sb.append((char) Integer.parseInt(pw.substring(i * 3,
                        (i + 1) * 3)));
            return sb.toString();
        }
        return pw;
    }

    /**
     * 零填充
     * fill("12",4) return "0012";
     *
     * @param s     String
     * @param index int
     * @return String
     */
    public static String fill0(String s, int index) {
        if (s.length() > index)
            return s.substring(s.length() - index);
        return fill("0", index - s.length()) + s;
    }

    /**
     * 产生填充字串<BR>
     * 例如 fill("0",3) return "000"
     *
     * @param s     String 填充字串
     * @param index int 个数
     * @return String
     */
    public static String fill(String s, int index) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < index; i++)
            sb.append(s);
        return sb.toString();
    }

    /**
     * 拿时间格式
     *
     * @param type
     * @return
     */
    public static String getDateFormat(int type) {
        Date aDate = new Date(System.currentTimeMillis());
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(aDate);
        return TypeTool.getString(Calendar.MONTH == type ? calendar.get(type) + 1 : calendar.get(type));
    }

    /**
     * 创建文件
     *
     * @param destFileName
     * @return
     */
    public static boolean createFile(String destFileName) {
        //linux下路径规则
        destFileName = destFileName.replace("\\", "/");
        File file = new File(destFileName);
        if (file.exists()) {
            //System.out.println("创建单个文件" + destFileName + "失败，目标文件已存在！");
            return false;
        }
        if (destFileName.endsWith(File.separator)) {
            //System.out.println("创建单个文件" + destFileName + "失败，目标文件不能为目录！");
            return false;
        }
        //判断目标文件所在的目录是否存在
        if (!file.getParentFile().exists()) {
            //如果目标文件所在的目录不存在，则创建父目录
            //System.out.println("目标文件所在目录不存在，准备创建它！");
            if (!file.getParentFile().mkdirs()) {
                //System.out.println("创建目标文件所在目录失败！");
                return false;
            }
        }
        //创建目标文件
        try {
            if (file.createNewFile()) {
                //System.out.println("创建单个文件" + destFileName + "成功！");
                return true;
            } else {
                //System.out.println("创建单个文件" + destFileName + "失败！");
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            //System.out.println("创建单个文件" + destFileName + "失败！" + e.getMessage());
            return false;
        }
    }

    /**
     * 判断bean对象是否为空
     *
     * @param obj
     */
    public static boolean isNullBean(Object obj) {
        boolean flag = true;
        if (obj == null)
            return flag;
        //得到此类所有属性
        Field[] fields = obj.getClass().getDeclaredFields();
        //遍历属性
        //通过属性拿到对应的set方法
        for (Field fTemp : fields) {
            try {
                PropertyDescriptor pd = new PropertyDescriptor(fTemp.getName(), obj.getClass());
                Method rm = pd.getReadMethod();
                //得到返回对象
                obj = rm.invoke(obj);
                if (TypeTool.getString(obj).length() > 0) {
                    flag = false;
                    break;
                }
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return flag;
    }

    /**
     * 将 s 进行 BASE64 编码
     */
    public static String getBASE64(String s) {
        if (s == null) return null;
        return (new sun.misc.BASE64Encoder()).encode(s.getBytes());
    }

    /**
     * 将 BASE64 编码的字符串 s 进行解码
     *
     * @param s
     * @return
     */
    public static String getFromBASE64(String s) {
        if (s == null) return null;
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            byte[] b = decoder.decodeBuffer(s);
            return new String(b);
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 四舍五入
     *
     * @param d double
     * @param p int
     * @return double
     */
    public static double round(double d, int p) {
        BigDecimal b = new BigDecimal(Double.toString(d));
        return b.divide(new BigDecimal("1"), p, 4).doubleValue();
    }


    /**
     * 获取随机6位数
     * @return
     */
    public static String getRandom(){
        return (Math.random() + "").substring(4, 10);
    }

}
