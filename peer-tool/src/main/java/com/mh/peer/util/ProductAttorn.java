package com.mh.peer.util;

import com.salon.frame.util.TypeTool;

/**
 * Created by zhangerxin on 2016-5-26.
 * 债权转让部分
 */
public class ProductAttorn {

    /**
     * 求预期收益率
     * @param surplusPricipal 剩余本金
     * @param loanRate 贷款利率
     * @param attornPrice //债权价格
     * @param nextReceiveTime //下期回款日
     * @return
     */
    public Double getExpectedRate(String surplusPricipal,String loanRate,String attornPrice,String nextReceiveTime){
        Double surplusPricipaldou = 0d; //剩余本金(double型)
        Double loanRatedou = 0d; //贷款利率
        Double attornPricedou = 0d; //承接价格
        long intevalDays = 0; //间隔天数
        Double expectedRate = 0d; //预期收益率
        String nowTime = ""; //当前时间
        nextReceiveTime = nextReceiveTime + " 00:00:00"; //下次还款日期
        DateHandle dateHandle = new DateHandle(); //时间处理

        try{

            if(surplusPricipal != null && !surplusPricipal.equals("")){ //剩余本金
                surplusPricipaldou = TypeTool.getDouble(surplusPricipal); //剩余本金(Double型)
            }
            if(loanRate != null && !loanRate.equals("")){ //贷款利率
                loanRatedou = TypeTool.getDouble(loanRate);
            }
            if(attornPrice != null && !attornPrice.equals("")){ //债权价格
                attornPricedou = TypeTool.getDouble(attornPrice);
            }

            nowTime = dateHandle.getNowTime(); //当前时间
            intevalDays = dateHandle.getDays(nowTime,nextReceiveTime); //间隔天数
            expectedRate = ((attornPricedou * loanRatedou * 0.01)/12 - (attornPricedou - surplusPricipaldou)) / (surplusPricipaldou * intevalDays / 31) *12;
        }catch(Exception e){
            e.printStackTrace();
        }
        return expectedRate;
    }
}
