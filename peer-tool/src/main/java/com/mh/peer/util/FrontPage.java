package com.mh.peer.util;

import com.salon.frame.util.TypeTool;

/**
 * Created by zhangerxin on 2016-7-18.
 */
public class FrontPage {

    /**
     * 获取分页信息(无关联参数)
     * @param pageIndex
     * @param pageSize
     * @param totalCount
     * @return
     */
    public String getPageInfo(String pageIndex,String pageSize,int totalCount,String pageName,String goPageName){
        String pageStr = ""; //分页部分拼接
        String pageCurrent = getCurrentPage(pageIndex,pageSize); //当前页
        int lastPage = 1; //上一页
        int nextPage = 1; //下一页
        String totalPage = getAllPage(pageSize,totalCount); //总页数

        try{

            if (pageCurrent != null && !pageCurrent.equals("")){
               if (TypeTool.getInt(pageCurrent) == 1){
                   lastPage = 1;
               }else{
                   lastPage = TypeTool.getInt(pageCurrent) - 1;
               }
            }

            if (totalPage != null && !totalPage.equals("")){
                if (TypeTool.getInt(totalPage) == 1){
                    nextPage = 1;
                }else if (TypeTool.getInt(pageCurrent) < TypeTool.getInt(totalPage)){
                    nextPage = TypeTool.getInt(pageCurrent) + 1;
                }else if (TypeTool.getInt(pageCurrent) == TypeTool.getInt(totalPage)){
                    nextPage = TypeTool.getInt(totalPage);
                }
            }

            pageStr = "<input id=\"pageIndex\" name=\"pageIndex\" type=\"hidden\" value=\""+pageIndex+"\"/>"
                    + "<input id=\"pageSize\" name=\"pageSize\" type=\"hidden\" value=\""+pageSize+"\"/>"
                    + "<li>"
                    + "<input type=\"button\" value=\"首页\" class=\"home_btn\" onclick=\""+pageName+"('1','"+pageSize+"')\"/>"
                    + "</li>"
                    + "<li>"
                    + "<input type=\"button\" value=\"上一页\" class=\"du_btn\" onclick=\""+pageName+"('"+lastPage+"','"+pageSize+"')\"/>"
                    + "</li>"
                    + "<li style=\"width:20px;color:#F25D5C;text-align:center;\">"+pageCurrent+"</li>"
                    + "<li>"
                    + "<input type=\"button\" value=\"下一页\" class=\"du_btn\" onclick=\""+pageName+"('"+nextPage+"','"+pageSize+"')\"/>"
                    + "</li>"
                    + "<li>"
                    + "<input type=\"button\" value=\"尾页\" class=\"mo_btn\" onclick=\""+pageName+"('"+totalPage+"','"+pageSize+"')\"/>"
                    + "</li>"
                    + "<li style=\"color:#343434;\">共"+totalPage+"页</li>"
                    + "<li>"
                    + "<input type=\"text\" class=\"page_text\" id=\"page_text\"/>"
                    + "</li>"
                    + "<li style=\"color:#F25D5C;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\""+goPageName+"('"+totalPage+"','"+pageSize+"')\">GO</a>"
                    + "</li>";

        }catch(Exception e){
            e.printStackTrace();
        }
        return pageStr;
    }


    /**
     * 获取分页信息(有一个关联参数)
     * @param pageIndex
     * @param pageSize
     * @param totalCount
     * @return
     */
    public String getPageInfo2(String pageIndex,String pageSize,int totalCount,String pageName,String goPageName,String glId){
        String pageStr = ""; //分页部分拼接
        String pageCurrent = getCurrentPage(pageIndex,pageSize); //当前页
        int lastPage = 1; //上一页
        int nextPage = 1; //下一页
        String totalPage = getAllPage(pageSize,totalCount); //总页数

        try{

            if (pageCurrent != null && !pageCurrent.equals("")){
                if (TypeTool.getInt(pageCurrent) == 1){
                    lastPage = 1;
                }else{
                    lastPage = TypeTool.getInt(pageCurrent) - 1;
                }
            }

            if (totalPage != null && !totalPage.equals("")){
                if (TypeTool.getInt(totalPage) == 1){
                    nextPage = 1;
                }else if (TypeTool.getInt(pageCurrent) < TypeTool.getInt(totalPage)){
                    nextPage = TypeTool.getInt(pageCurrent) + 1;
                }else if (TypeTool.getInt(pageCurrent) == TypeTool.getInt(totalPage)){
                    nextPage = TypeTool.getInt(totalPage);
                }
            }

            pageStr = "<input id=\"pageIndex\" name=\"pageIndex\" type=\"hidden\" value=\""+pageIndex+"\"/>"
                    + "<input id=\"pageSize\" name=\"pageSize\" type=\"hidden\" value=\""+pageSize+"\"/>"
                    + "<input type=\"button\" value=\"首页\" class=\"ye_first\" style=\"width:50px;\" onclick=\""+pageName+"('1','"+pageSize+"','"+glId+"')\">"
                    + "<input type=\"button\" value=\"上一页\" class=\"ye_up\" style=\"width:60px;\" onclick=\""+pageName+"('"+lastPage+"','"+pageSize+"','"+glId+"')\">"
                    + "<span class=\"p_fanye_num\">"+pageCurrent+"</span>"
                    + "<input type=\"button\" value=\"下一页\" class=\"ye_down\" style=\"width:60px;\" onclick=\""+pageName+"('"+nextPage+"','"+pageSize+"','"+glId+"')\">"
                    + "<input type=\"button\" value=\"尾页\" class=\"ye_last\" style=\"width: 50px;\" onclick=\""+pageName+"('"+totalPage+"','"+pageSize+"','"+glId+"')\">"
                    + "<span class=\"fanye_ye\">共"+totalPage+"页</span>"
                    + "<input type=\"text\" class=\"down\" style=\"width:40px;padding:2px 0;border:1px solid #222222;\" id=\"page_text\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\""+goPageName+"('"+totalPage+"','"+pageSize+"','"+glId+"')\">GO</a>";

        }catch(Exception e){
            e.printStackTrace();
        }
        return pageStr;
    }


    /**
     * 获取分页信息(有两个关联参数)
     * @param pageIndex
     * @param pageSize
     * @param totalCount
     * @return
     */
    public String getPageInfo3(String pageIndex,String pageSize,int totalCount,String pageName,String goPageName,String firstId, String secondId){
        String pageStr = ""; //分页部分拼接
        String pageCurrent = getCurrentPage(pageIndex,pageSize); //当前页
        int lastPage = 1; //上一页
        int nextPage = 1; //下一页
        String totalPage = getAllPage(pageSize,totalCount); //总页数

        try{

            if (pageCurrent != null && !pageCurrent.equals("")){
                if (TypeTool.getInt(pageCurrent) == 1){
                    lastPage = 1;
                }else{
                    lastPage = TypeTool.getInt(pageCurrent) - 1;
                }
            }

            if (totalPage != null && !totalPage.equals("")){
                if (TypeTool.getInt(totalPage) == 1){
                    nextPage = 1;
                }else if (TypeTool.getInt(pageCurrent) < TypeTool.getInt(totalPage)){
                    nextPage = TypeTool.getInt(pageCurrent) + 1;
                }else if (TypeTool.getInt(pageCurrent) == TypeTool.getInt(totalPage)){
                    nextPage = TypeTool.getInt(totalPage);
                }
            }

            pageStr = "<input id=\"pageIndex\" name=\"pageIndex\" type=\"hidden\" value=\""+pageIndex+"\"/>"
                    + "<input id=\"pageSize\" name=\"pageSize\" type=\"hidden\" value=\""+pageSize+"\"/>"
                    + "<input type=\"button\" value=\"首页\" class=\"ye_first\" style=\"width:50px;\" onclick=\""+pageName+"('1','"+pageSize+"','"+firstId+"','"+secondId+"')\">"
                    + "<input type=\"button\" value=\"上一页\" class=\"ye_up\" style=\"width:60px;\" onclick=\""+pageName+"('"+lastPage+"','"+pageSize+"','"+firstId+"','"+secondId+"')\">"
                    + "<span class=\"p_fanye_num\">"+pageCurrent+"</span>"
                    + "<input type=\"button\" value=\"下一页\" class=\"ye_down\" style=\"width:60px;\" onclick=\""+pageName+"('"+nextPage+"','"+pageSize+"','"+firstId+"','"+secondId+"')\">"
                    + "<input type=\"button\" value=\"尾页\" class=\"ye_last\" style=\"width: 50px;\" onclick=\""+pageName+"('"+totalPage+"','"+pageSize+"','"+firstId+"','"+secondId+"')\">"
                    + "<span class=\"fanye_ye\">共"+totalPage+"页</span>"
                    + "<input type=\"text\" class=\"down\" style=\"width:40px;padding:2px 0;border:1px solid #222222;\" id=\"page_text\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\""+goPageName+"('"+totalPage+"','"+pageSize+"','"+firstId+"','"+secondId+"')\">GO</a>";

        }catch(Exception e){
            e.printStackTrace();
        }
        return pageStr;
    }


    /**
     * 获取分页信息(有三个关联参数)
     * @param pageIndex
     * @param pageSize
     * @param totalCount
     * @return
     */
    public String getPageInfo4(String pageIndex,String pageSize,int totalCount,String pageName,String goPageName,String param1,String param2,String param3){
        String pageStr = ""; //分页部分拼接
        String pageCurrent = getCurrentPage(pageIndex,pageSize); //当前页
        int lastPage = 1; //上一页
        int nextPage = 1; //下一页
        String totalPage = getAllPage(pageSize,totalCount); //总页数

        try{

            if (pageCurrent != null && !pageCurrent.equals("")){
                if (TypeTool.getInt(pageCurrent) == 1){
                    lastPage = 1;
                }else{
                    lastPage = TypeTool.getInt(pageCurrent) - 1;
                }
            }

            if (totalPage != null && !totalPage.equals("")){
                if (TypeTool.getInt(totalPage) == 1){
                    nextPage = 1;
                }else if (TypeTool.getInt(pageCurrent) < TypeTool.getInt(totalPage)){
                    nextPage = TypeTool.getInt(pageCurrent) + 1;
                }else if (TypeTool.getInt(pageCurrent) == TypeTool.getInt(totalPage)){
                    nextPage = TypeTool.getInt(totalPage);
                }
            }

            pageStr = "<input id=\"pageIndex\" name=\"pageIndex\" type=\"hidden\" value=\""+pageIndex+"\"/>"
                    + "<input id=\"pageSize\" name=\"pageSize\" type=\"hidden\" value=\""+pageSize+"\"/>"
                    + "<input type=\"button\" value=\"首页\" class=\"ye_first\" style=\"width:50px;\" onclick=\""+pageName+"('1','"+pageSize+"','"+param1+"','"+param2+"','"+param3+"')\">"
                    + "<input type=\"button\" value=\"上一页\" class=\"ye_up\" style=\"width:60px;\" onclick=\""+pageName+"('"+lastPage+"','"+pageSize+"','"+param1+"','"+param2+"','"+param3+"')\">"
                    + "<span class=\"p_fanye_num\">"+pageCurrent+"</span>"
                    + "<input type=\"button\" value=\"下一页\" class=\"ye_down\" style=\"width:60px;\" onclick=\""+pageName+"('"+nextPage+"','"+pageSize+"','"+param1+"','"+param2+"','"+param3+"')\">"
                    + "<input type=\"button\" value=\"尾页\" class=\"ye_last\" style=\"width: 50px;\" onclick=\""+pageName+"('"+totalPage+"','"+pageSize+"','"+param1+"','"+param2+"','"+param3+"')\">"
                    + "<span class=\"fanye_ye\">共"+totalPage+"页</span>"
                    + "<input type=\"text\" class=\"down\" style=\"width:40px;padding:2px 0;border:1px solid #222222;\" id=\"page_text\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\""+goPageName+"('"+totalPage+"','"+pageSize+"','"+param1+"','"+param2+"','"+param3+"')\">GO</a>";

        }catch(Exception e){
            e.printStackTrace();
        }
        return pageStr;
    }


    /**
     * 获取总页数
     * @param pageSize
     * @param totalCount
     * @return
     */
    public String getAllPage(String pageSize,int totalCount){
        String totalPage = ""; //总页数(字符型)
        int totalPageIn = 0; //总页数(整型)
        int pageSizeIn = 0; //每页显示数量

        try{

            if (pageSize != null && !pageSize.equals("")){ //每次显示数量
                pageSizeIn = TypeTool.getInt(pageSize);
                if (totalCount%pageSizeIn == 0){
                    totalPageIn = totalCount/pageSizeIn;
                }else{
                    totalPageIn = (int)(totalCount/pageSizeIn) + 1;
                }
            }
            if (totalPageIn == 0){
                totalPageIn = 1;
            }
            totalPage = String.valueOf(totalPageIn);
        }catch(Exception e){
            e.printStackTrace();
        }
        return totalPage;
    }


    /**
     * 获取当前页
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public String getCurrentPage(String pageIndex,String pageSize){
        int pageIndexIn = 0; //当前数(整型)
        int pageSizeIn = 0; //每次显示数量(整型)
        String pageCurrent = ""; //当前页(字符型)
        int pageCurrentIn = 0; //当前页(整型)

        try{

            if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
                pageIndexIn = TypeTool.getInt(pageIndex);
                pageSizeIn = TypeTool.getInt(pageSize);
                pageCurrentIn = (int)(pageIndexIn/pageSizeIn) + 1;
                pageCurrent = String.valueOf(pageCurrentIn);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return pageCurrent;
    }
}
