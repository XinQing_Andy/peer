package com.mh.peer.util;

import sun.net.www.http.HttpClient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 对时间进行处理
 * Created by zhangerxin on 2016-5-21.
 */
public class DateHandle {


    /**
     * 获取当前时间
     *
     * @return
     */
    public String getNowTime() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR); //年
        int month = c.get(Calendar.MONTH) + 1; //月
        int date = c.get(Calendar.DATE); //日
        int hour = c.get(Calendar.HOUR_OF_DAY); //小时
        int minute = c.get(Calendar.MINUTE); //分钟
        int second = c.get(Calendar.SECOND); //秒
        String nowTime = ""; //当前日期
        nowTime = year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
        return nowTime;
    }


    /**
     * 根据起止时间获取相差天数
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public Long getDays(String startTime, String endTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        long day = 0; //相差天数

        try {

            Date now = df.parse(endTime); //截止时间
            Date date = df.parse(startTime); //起始时间
            long time = now.getTime() - date.getTime(); //相差时间(以毫秒为单位)
            day = time / (24 * 60 * 60 * 1000); //相差天数
        } catch (Exception e) {
            e.printStackTrace();
        }
        return day;
    }


    /**
     * 根据起止时间获取相差小时
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public Long getHours(String startTime, String endTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long day = 0; //相差天数
        long hour = 0; //相差小时

        try {

            Date now = df.parse(endTime); //截止时间
            Date date = df.parse(startTime); //起始时间
            long time = now.getTime() - date.getTime(); //相差时间(以毫秒为单位)
            day = time / (24 * 60 * 60 * 1000); //相差天数
            hour = time / (60 * 60 * 1000) - day * 24; //相差小时
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hour;
    }

    /**
     * 根据起止时间获取相差分钟
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public Long getMinutes(String startTime, String endTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long day = 0; //相差天数
        long hour = 0; //相差小时
        long minutes = 0; //相差分钟

        try {

            Date now = df.parse(endTime); //截止时间
            Date date = df.parse(startTime); //起始时间
            long time = now.getTime() - date.getTime(); //相差时间(以毫秒为单位)
            day = time / (24 * 60 * 60 * 1000); //相差天数
            hour = time / (60 * 60 * 1000) - day * 24; //相差小时
            minutes = time / (60 * 1000) - day * 24 * 60 - hour * 60; //相差分钟
        } catch (Exception e) {
            e.printStackTrace();
        }
        return minutes;
    }

    /**
     * 根据起止日期获取相差秒数
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public Long getSeconds(String startTime, String endTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long day = 0; //相差天数
        long hour = 0; //相差小时
        long minutes = 0; //相差分钟
        long seconds = 0; //相差秒数

        try {

            Date now = df.parse(endTime); //截止时间
            Date date = df.parse(startTime); //起始时间
            long time = now.getTime() - date.getTime(); //相差时间(以毫秒为单位)
            day = time / (24 * 60 * 60 * 1000); //相差天数
            hour = time / (60 * 60 * 1000) - day * 24; //相差小时
            minutes = time / (60 * 1000) - day * 24 * 60 - hour * 60; //相差分钟
            seconds = time / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - minutes * 60; //相差秒数
        } catch (Exception e) {
            e.printStackTrace();
        }
        return seconds;
    }


    /**
     * 获取指指定日期后的日期
     *
     * @param startTime
     * @param day
     * @return
     */
    public String getTimeAfterStart(String startTime, int day) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = "";

        try {

            Date date = sdf.parse(startTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.DATE, cal.get(Calendar.DATE) + day);
            dateStr = sdf.format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateStr;
    }
}
