package com.mh.peer.util;


import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-23.
 */
public class JsonHelper {

    /**
     * 将json格式的字符串转换成map(String)
     * @param str
     * @return
     */
    public static Map<String,String> getObjectToMap(String str){
        Map<String,String> map = new HashMap<String,String>();
        JSONObject jsonObject = JSONObject.fromObject(str);
        Iterator it = jsonObject.keys();
        while (it.hasNext()){
            String key = String.valueOf(it.next());
            String value = (String) jsonObject.get(key);
            map.put(key,value);
        }
        return map;
    }


    /**
     * 将json格式的字符串转换成map(Object)
     * @param str
     * @return
     */
    public static Map<String,Object> getObjectToMap2(String str){
        Map<String,Object> map = new HashMap<String,Object>();
        JSONObject jsonObject = JSONObject.fromObject(str);
        Iterator it = jsonObject.keys();
        while (it.hasNext()){
            String key = String.valueOf(it.next());
            Object value = (Object) jsonObject.get(key);
            map.put(key,value);
        }
        return map;
    }
}
