package com.mh.peer.util;

import com.mh.peer.model.entity.HuanxunFile;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;

/**
 * Created by zhangerxin on 2016-8-10.
 * 保存环迅异常文件
 */
public class HuanXunExceptionFile {

    /**
     * 将文件保存起来
     * @param huanxunFile
     */
    public static void saveInfo(HuanxunFile huanxunFile){
        String id = huanxunFile.getId(); //主键
        String url = huanxunFile.getUrl(); //文件地址
        String type = huanxunFile.getType(); //类型
        String createUser = huanxunFile.getCreateuser(); //创建人
        String createTime = huanxunFile.getCreatetime(); //创建时间
        String sql = "INSERT INTO huanxun_file(ID,URL,TYPE,CREATEUSER,CREATETIME)VALUES("
                + "<id>,<url>,<type>,<createUser>,'"+createTime+"')";
        DConnection conn = DaoTool.getConnection();
        DaoResult daoresult = new DaoResult();

        DMap dmap = new DMap(); //保存异常文件操作
        dmap.setData("id",id); //主键
        dmap.setData("url",url); //文件地址
        dmap.setData("type",type); //类型
        dmap.setData("createUser",createUser); //创建人
        dmap.setData("createTime",createTime); //创建时间
        daoresult = DaoTool.update(sql, dmap.getData(), conn);
        conn.commit();
        conn.close();
    }
}
