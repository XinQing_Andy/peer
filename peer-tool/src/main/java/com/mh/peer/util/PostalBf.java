package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-10-14.
 */
public class PostalBf {

    public static String postalHandle(Map<String,String> map){
        DConnection conn = DaoTool.getConnection();
        String result = "0"; //备份结果(0-成功、1-失败)
        String Uid = ""; //主键
        String memberId = ""; //会员id
        String merBillNo = ""; //商户订单号
        String merDate = ""; //提现日期
        String userType = ""; //用户类型(1-个人、2-企业)
        String trdAmt = ""; //提现金额
        String merFee = ""; //平台手续费
        String merFeeType = ""; //平台手续费类型(1-内扣、2-外扣)
        String ipsFeeType = ""; //IPS手续费承担方(1-平台承担、2-用户承担)
        String ipsAcctNo = ""; //IPS存管账户号
        String flag = "1"; //状态(0-已提现、1-待提现)
        String createTime = ""; //创建时间
        String sqlPostalBf = ""; //提现备份
        DaoResult daoResultPostal = new DaoResult();
        DMap dMapPostal = new DMap();

        try{

            Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
            createTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //创建日期

            if (map != null){
                memberId = map.get("memberId"); //会员id
                merBillNo = map.get("merBillNo"); //商户订单号
                merDate = map.get("merDate"); //提现日期
                userType = map.get("userType"); //用户类型(1-个人、2-企业)
                trdAmt = map.get("trdAmt"); //提现金额
                merFee = map.get("merFee"); //平台手续费
                merFeeType = map.get("merFeeType"); //平台手续费类型(1-内扣、2-外扣)
                ipsFeeType = map.get("ipsFeeType"); //IPS手续费承担方(1-平台承担、2-用户承担)
                ipsAcctNo = map.get("ipsAcctNo"); //IPS存管账户号
                sqlPostalBf = "INSERT INTO huanxun_postal_bf(ID,MEMBERID,MERBILLNO,MERDATE,USERTYPE,TRDAMT,MERFEE,MERFEETYPE,IPSFEETYPE,IPSACCTNO,FLAG,CREATETIME)"
                            + "VALUES(<Uid>,<memberId>,<merBillNo>,<merDate>,<userType>,<trdAmt>,<merFee>,<merFeeType>,<ipsFeeType>,<ipsAcctNo>,<flag>,<createTime>)";
                dMapPostal.setData("Uid",Uid); //主键
                dMapPostal.setData("memberId",memberId); //会员id
                dMapPostal.setData("merBillNo",merBillNo); //商户订单号
                dMapPostal.setData("merDate",merDate); //提现日期
                dMapPostal.setData("userType",userType); //用户类型(1-个人、2-企业)
                dMapPostal.setData("trdAmt",trdAmt); //提现金额
                dMapPostal.setData("merFee",merFee); //平台手续费
                dMapPostal.setData("merFeeType",merFeeType); //平台手续费类型(1-内扣、2-外扣)
                dMapPostal.setData("ipsFeeType",ipsFeeType); //IPS手续费承担方(1-平台承担、2-用户承担)
                dMapPostal.setData("ipsAcctNo",ipsAcctNo); //IPS存管账户号
                dMapPostal.setData("flag",flag); //状态(0-已提现、1-待提现)
                dMapPostal.setData("createTime",createTime); //创建日期

                daoResultPostal = DaoTool.update(sqlPostalBf,dMapPostal.getData(),conn);
                if (daoResultPostal.getCode() < 0){
                    result = "1";
                    conn.rollback();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            conn.rollback();
            result = "1";
        }finally {
            conn.commit();
            conn.close();
        }
        return result;
    }
}
