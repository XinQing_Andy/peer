package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-10-8.
 * 转账前备份
 */
public class TransferBf {

    /**
     * 转账结果
     * @return
     */
    public static String transferResult(Map<String,String> mapTransfer,List<Map<String,String>> listMapTransferDetail){
        DConnection conn = DaoTool.getConnection();
        String result = "0"; //结果(0-成功、1-失败)
        String transferId = ""; //转账主键
        String batchNo = ""; //商户转账批次号
        String merDate = ""; //转账日期
        String projectNo = ""; //项目ID号
        String transferType = ""; //转账类型(3-还款)
        String isAutoRepayment = ""; //是否自动还款(1-是、2-否)
        String transferMode = ""; //1-逐笔转账、2-批量转账
        String merBillNo = ""; //商户订单号
        String freezeId = ""; //IPS原冻结订单号
        String outIpsAcctNo = ""; //转出方IPS存管账户号
        String outMerFee = ""; //转出方平台手续费(String型)
        Double outMerFeeDou = 0.0d; //转出方平台手续费(Double型)
        String inIpsAcctNo = ""; //转入方IPS存管账户号
        String inMerFee = ""; //转入方平台手续费(String型)
        Double inMerFeeDou = 0.0d; //转入方平台手续费(Double型)
        String trdAmt = ""; //转账金额(String型)
        Double trdAmtDou = 0.0d; //转账金额(Double型)
        String Uid = ""; //主键(转账详情)
        String createTime = ""; //创建日期
        String flagTransfer = "2"; //转账状态(0-转账成功、1-转账失败、2-待转账)
        String flagTransferDetail = "2"; //转账明细状态(0-转账成功、1-转账失败、2-待转账)
        String sqlTransfer = ""; //转账执行语句
        String sqlTransferDetail = ""; //转账详情执行语句
        DaoResult daoResultTransfer = new DaoResult(); //转账部分
        DaoResult daoResultTransferDetail = new DaoResult(); //转账详情部分
        DMap dMapTransfer = new DMap(); //转账部分

        try{

            createTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //转账日期

            if (mapTransfer != null){
                transferId = mapTransfer.get("transferId"); //转账id
                batchNo = mapTransfer.get("batchNo"); //商户转账批次号
                merDate = mapTransfer.get("merDate"); //转账日期
                projectNo = mapTransfer.get("projectNo"); //项目ID号
                transferType = mapTransfer.get("transferType"); //转账类型(3-还款)
                isAutoRepayment = mapTransfer.get("isAutoRepayment"); //是否自动还款(1-是、2-否)
                transferMode = mapTransfer.get("transferMode"); //1-逐笔转账、2-批量转账
            }
            sqlTransfer = "INSERT INTO huanxun_transfer_bf(ID,BATCHNO,MERDATE,PROJECTNO,TRANSFERTYPE,ISAUTOREPAYMENT,TRANSFERMODE,FLAG,CREATETIME)VALUES("
                        + "<transferId>,<batchNo>,<merDate>,<projectNo>,<transferType>,<isAutoRepayment>,<transferMode>,<flagTransfer>,<createTime>)";
            dMapTransfer.setData("transferId",transferId); //转账主键
            dMapTransfer.setData("batchNo",batchNo); //商户转账批次号
            dMapTransfer.setData("merDate",merDate); //转账日期
            dMapTransfer.setData("projectNo",projectNo); //项目ID号
            dMapTransfer.setData("transferType",transferType); //转账类型(3-还款)
            dMapTransfer.setData("isAutoRepayment",isAutoRepayment); //是否自动还款(1-是、2-否)
            dMapTransfer.setData("transferMode",transferMode); //1-逐笔转账、2-批量转账
            dMapTransfer.setData("flagTransfer",flagTransfer); //转账状态(0-转账成功、1-转账失败、2-待转账)
            dMapTransfer.setData("createTime",createTime); //转账日期
            daoResultTransfer = DaoTool.update(sqlTransfer,dMapTransfer.getData(),conn);
            if (daoResultTransfer.getCode() < 0){
                conn.rollback();
                result = "1";
            }else{
                if (listMapTransferDetail != null && listMapTransferDetail.size() > 0){
                    for (Map<String,String> mapTransferDetail : listMapTransferDetail){
                        Uid = UUID.randomUUID().toString().replaceAll("-",""); //转账详情主键
                        merBillNo = mapTransferDetail.get("merBillNo"); //商户订单号
                        freezeId = mapTransferDetail.get("freezeId"); //IPS原冻结订单号
                        outIpsAcctNo = mapTransferDetail.get("outIpsAcctNo"); //转出方IPS存管账户号
                        outMerFee = mapTransferDetail.get("outMerFee"); //转出方平台手续费(String型)
                        if (outMerFee != null && !outMerFee.equals("")){
                            outMerFeeDou = TypeTool.getDouble(outMerFee); //转出方平台手续费(Double型)
                        }
                        inIpsAcctNo = mapTransferDetail.get("inIpsAcctNo"); //转入方IPS存管账户号
                        inMerFee = mapTransferDetail.get("inMerFee"); //转入方平台手续费(String型)
                        if (inMerFee != null && !inMerFee.equals("")){
                            inMerFeeDou = TypeTool.getDouble(inMerFee); //转入方平台手续费(Double型)
                        }
                        trdAmt = mapTransferDetail.get("trdAmt"); //转账金额(String型)
                        if (trdAmt != null && !trdAmt.equals("")){
                            trdAmtDou = TypeTool.getDouble(trdAmt);
                        }
                        sqlTransferDetail = "INSERT INTO huanxun_transfer_detail_bf(ID,TRANSFERID,MERBILLNO,FREEZEID,OUTIPSACCTNO,OUTMERFEE,INIPSACCTNO,INMERFEE,TRDAMT,FLAG,CREATETIME)VALUES("
                                          + "<Uid>,<transferId>,<merBillNo>,<freezeId>,<outIpsAcctNo>,<outMerFee>,<inIpsAcctNo>,<inMerFee>,<trdAmt>,<flagTransferDetail>,<createTime>)";
                        DMap dMapTransferDetail = new DMap(); //转账部分
                        dMapTransferDetail.setData("Uid",Uid); //主键
                        dMapTransferDetail.setData("transferId",transferId); //转账主键
                        dMapTransferDetail.setData("merBillNo",merBillNo); //商户订单号
                        dMapTransferDetail.setData("freezeId",freezeId); //IPS原冻结订单号
                        dMapTransferDetail.setData("outIpsAcctNo",outIpsAcctNo); //转出方IPS存管账户号
                        dMapTransferDetail.setData("outMerFee",outMerFeeDou); //转出方平台手续费(Double型)
                        dMapTransferDetail.setData("inIpsAcctNo",inIpsAcctNo); //转入方IPS存管账户号
                        dMapTransferDetail.setData("inMerFee",inMerFeeDou); //转入方平台手续费(String型)
                        dMapTransferDetail.setData("trdAmt",trdAmtDou); //转账金额(Double型)
                        dMapTransferDetail.setData("flagTransferDetail",flagTransferDetail); //转账明细状态(0-转账成功、1-转账失败、2-待转账)
                        dMapTransferDetail.setData("createTime",createTime); //创建时间
                        daoResultTransferDetail = DaoTool.update(sqlTransferDetail,dMapTransferDetail.getData(),conn);
                        if (daoResultTransferDetail.getCode() < 0){
                            conn.rollback();
                            result = "1";
                        }
                    }
                }
            }
        }catch (Exception e){
            result = "1";
            e.printStackTrace();
            conn.rollback();
        }finally {
            conn.commit();
            conn.close();
        }
        return result;
    }
}
