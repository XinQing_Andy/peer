package com.mh.peer.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-9-16.
 * 获取指定目录下文件
 */
public class SearchFiles {

    public static List<String> queryFiles(String path,String fileBz) {
        File file=new File(path);
        File[] tempList = file.listFiles();
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < tempList.length; i++) {
            if (fileBz.equals("0")){ //文件
                if (tempList[i].isFile()) {
                    list.add(tempList[i].toString());
                }
            }else if (fileBz.equals("1")){ //文件夹
                if (tempList[i].isDirectory()) {
                    list.add(tempList[i].toString());
                }
            }
        }
        return list;
    }
}
