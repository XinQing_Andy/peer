package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-10-11.
 */
public class UnFreezeBf {

    /**
     * 解冻备份
     * @param mapUnFreeze
     * @return
     */
    public static String unFreezeResult(Map<String,String> mapUnFreeze){
        DConnection conn = DaoTool.getConnection();
        String result = "0"; //结果(0-成功、1-失败)
        String Uid = ""; //解冻主键
        String huanxunFreezeId = ""; //环迅冻结表主键
        String merBillNo = ""; //商户订单号
        String merDate = ""; //解冻日期
        String projectNo = ""; //项目ID号
        String freezeId = ""; //原IPS冻结订单号
        String bizType = ""; //业务类型(1-流标、2-撤标、3-解冻保证金、4-解冻红包)
        String merFee = "0"; //手续费(Stirng型)
        Double merFeeDou = 0.0d; //手续费(Double型)
        String ipsAcctNo = ""; //解冻账号
        String trdAmt = "0"; //解冻金额(String型)
        Double trdAmtDou = 0.0d; //解冻金额(Double型)
        String flag = "2"; //状态(0-解冻成功、1-解冻失败、2-待解冻)
        String createTime = ""; //解冻时间
        String sqlUnFreeze = ""; //解冻部分
        DMap dMapUnFreeze = new DMap();
        DaoResult daoResultUnFreeze = new DaoResult(); //解冻

        try{

            createTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //解冻时间
            Uid = UUID.randomUUID().toString().replaceAll("-",""); //解冻表主键

            if (mapUnFreeze != null){
                huanxunFreezeId = mapUnFreeze.get("huanxunFreezeId"); //冻结表主键
                merBillNo = mapUnFreeze.get("merBillNo"); //商户订单号
                merDate = mapUnFreeze.get("merDate"); //解冻日期
                projectNo = mapUnFreeze.get("projectNo"); //项目ID号
                freezeId = mapUnFreeze.get("freezeId"); //原IPS冻结订单号
                bizType = mapUnFreeze.get("bizType"); //业务类型(1-流标、2-撤标、3-解冻保证金、4-解冻红包)
                merFee = mapUnFreeze.get("merFee"); //手续费(Stirng型)
                if (merFee != null && !merFee.equals("")){
                    merFeeDou = TypeTool.getDouble(merFee); //手续费(Double型)
                }
                ipsAcctNo = mapUnFreeze.get("ipsAcctNo"); //解冻账号
                trdAmt = mapUnFreeze.get("trdAmt"); //解冻金额(String型)
                if (trdAmt != null && !trdAmt.equals("")){
                    trdAmtDou = TypeTool.getDouble("trdAmt"); //解冻金额(Double型)
                }

                sqlUnFreeze = "INSERT INTO huanxun_unFreeze_bf(ID,HUANXUNFREEZEID,MERBILLNO,MERDATE,PROJECTNO,FREEZEID,BIZTYPE,MERFEE,IPSACCTNO,TRDAMT,FLAG,CREATETIME)"
                            + "VALUES(<Uid>,<huanxunFreezeId>,<merBillNo>,<merDate>,<projectNo>,<freezeId>,<bizType>,<merFeeDou>,<ipsAcctNo>,<trdAmtDou>,<flag>,<createTime>)";
                dMapUnFreeze.setData("Uid",Uid); //解冻表主键
                dMapUnFreeze.setData("huanxunFreezeId",huanxunFreezeId); //冻结表主键
                dMapUnFreeze.setData("merBillNo",merBillNo); //商户订单号
                dMapUnFreeze.setData("merDate",merDate); //解冻日期
                dMapUnFreeze.setData("projectNo",projectNo); //项目ID号
                dMapUnFreeze.setData("freezeId",freezeId); //原IPS冻结订单号
                dMapUnFreeze.setData("bizType",bizType); //业务类型(1-流标、2-撤标、3-解冻保证金、4-解冻红包)
                dMapUnFreeze.setData("merFeeDou",merFeeDou); //手续费
                dMapUnFreeze.setData("ipsAcctNo",ipsAcctNo); //解冻账号
                dMapUnFreeze.setData("trdAmtDou",trdAmtDou); //解冻金额
                dMapUnFreeze.setData("flag",flag); //状态(0-解冻成功、1-解冻失败、2-待解冻)
                dMapUnFreeze.setData("createTime",createTime); //解冻时间
                daoResultUnFreeze = DaoTool.update(sqlUnFreeze,dMapUnFreeze.getData(),conn);
                if (daoResultUnFreeze.getCode() < 0){
                    conn.rollback();
                    result = "1"; //失败
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            conn.rollback();
            result = "1";
        }finally {
            conn.commit();
            conn.close();
        }
        return result;
    }
}
