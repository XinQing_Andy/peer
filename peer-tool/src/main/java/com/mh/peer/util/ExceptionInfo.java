package com.mh.peer.util;

/**
 * Created by zhangerxin on 2016-8-10.
 * 异常文件路径
 */
public class ExceptionInfo {

    private String regProjectPath = "/usr/exception/regproject/";  //项目登记路径
    private String investFreezePath = "/usr/exception/freeze/"; //冻结路径(投资)
    private String unInvestFreezePath = "/usr/exception/unfreeze/"; //解冻路径(投资)
    private String investTransferPath = "/usr/exception/transfer/"; //转账路径(投资)
    private String attornFreezePath = "/usr/exception/freeze/"; //冻结路径(债权)
    private String attornTransferPath = "/usr/exception/transfer/"; //转账路径(债权)
    private String reChargePath = "/usr/exception/recharge/"; //充值路径
    private String repayFreezePath = "/usr/exception/freeze/"; //冻结路径(还款)
    private String repayTransferPath = "/usr/exception/transfer/"; //转账路径(还款)

    public String getRegProjectPath() {
        return regProjectPath;
    }
    public void setRegProjectPath(String regProjectPath) {
        this.regProjectPath = regProjectPath;
    }

    public String getInvestFreezePath() {
        return investFreezePath;
    }
    public void setInvestFreezePath(String investFreezePath) {
        this.investFreezePath = investFreezePath;
    }

    public String getUnInvestFreezePath() {
        return unInvestFreezePath;
    }
    public void setUnInvestFreezePath(String unInvestFreezePath) {
        this.unInvestFreezePath = unInvestFreezePath;
    }

    public String getInvestTransferPath() {
        return investTransferPath;
    }
    public void setInvestTransferPath(String investTransferPath) {
        this.investTransferPath = investTransferPath;
    }

    public String getAttornFreezePath() {
        return attornFreezePath;
    }
    public void setAttornFreezePath(String attornFreezePath) {
        this.attornFreezePath = attornFreezePath;
    }

    public String getAttornTransferPath() {
        return attornTransferPath;
    }
    public void setAttornTransferPath(String attornTransferPath) {
        this.attornTransferPath = attornTransferPath;
    }

    public String getReChargePath() {
        return reChargePath;
    }
    public void setReChargePath(String reChargePath) {
        this.reChargePath = reChargePath;
    }

    public String getRepayFreezePath() {
        return repayFreezePath;
    }
    public void setRepayFreezePath(String repayFreezePath) {
        this.repayFreezePath = repayFreezePath;
    }

    public String getRepayTransferPath() {
        return repayTransferPath;
    }
    public void setRepayTransferPath(String repayTransferPath) {
        this.repayTransferPath = repayTransferPath;
    }
}
