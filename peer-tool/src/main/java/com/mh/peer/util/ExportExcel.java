package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Date;

/**
 *  Created by zhangerxin on 2016-9-13.
 *  Excel导出
 */
public class ExportExcel {

    /**
     * 导出
     * @param headers
     * @param crr
     * @return
     */
    public HSSFWorkbook exportExcel(String[] headers,Object[][] crr) {
        // 声明一个工作薄
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 生成一个表格s
        HSSFSheet sheet = workbook.createSheet();
        // 生成一个样式
        HSSFCellStyle style = workbook.createCellStyle();

        // 设置这些样式
        style.setFillForegroundColor(HSSFColor.WHITE.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        // 生成一个字体
        HSSFFont font = workbook.createFont();
        //font.setColor(HSSFColor.VIOLET.index);
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        // 把字体应用到当前的样式
        style.setFont(font);

        // 生成并设置另一个样式
        HSSFCellStyle style2 = workbook.createCellStyle();
        style2.setFillForegroundColor(HSSFColor.WHITE.index);
        style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style2.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style2.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        // 生成另一个字体
        HSSFFont font2 = workbook.createFont();
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);

        // 把字体应用到当前的样式
        style2.setFont(font2);

        // 声明一个画图的顶级管理器
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();

        // 定义注释的大小和位置,详见文档
        HSSFComment comment = patriarch.createComment(new HSSFClientAnchor(0,
                0, 0, 0, (short) 4, 2, (short) 6, 5));

        HSSFRow row = sheet.createRow(0);

        for (short i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellStyle(style);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }

        Object value = null;
        for(short i=0;i<crr.length;i++){
            row = sheet.createRow(i+1);
            for(short j=0;j<headers.length;j++){
                HSSFCell cell = row.createCell(j);
                cell.setCellStyle(style2);
                // 判断值的类型后进行强制类型转换
                HSSFRichTextString textValue = null;
                value = crr[i][j];

                if (value instanceof Integer) {
                    int intValue = (Integer) value;
                    cell.setCellValue(intValue);
                } else if (value instanceof Float) {
                    float fValue = (Float) value;
                    textValue = new HSSFRichTextString(String.valueOf(fValue));
                    cell.setCellValue(textValue);
                } else if (value instanceof Double) {
                    double dValue = (Double) value;
                    textValue = new HSSFRichTextString(String.valueOf(dValue));
                    cell.setCellValue(textValue);
                } else if (value instanceof Long) {
                    long longValue = (Long) value;
                    cell.setCellValue(longValue);
                } else {
                    cell.setCellValue(value + "");
                }
            }
        }
        return workbook;
    }


    /**
     * 根据条件获取导出信息
     * @param request
     * @param response
     */
    public void exportByInfo(HttpServletRequest request,HttpServletResponse response){
        String title = request.getParameter("ExportTitle"); //表头信息(题目;字段名,题目;字段名)
        String tableName = request.getParameter("tableName"); //表名
        String where = request.getParameter("where"); //查询条件
        String sql = ""; //查询语句
        String arr[] = null; //用于存放题目名称
        String brr[] = null; //用于存放字段名称
        String crr[] = null; //用于存放表头信息拆分
        String drr[] = null;
        Object err[][] = null; //用于存放需导出信息

        try{

            if (title != null && !title.equals("")){ //表头信息(题目;字段名,题目;字段名)
                crr = title.split(","); //表头信息
                if (crr != null && crr.length > 0){
                    arr = new String[crr.length]; //用于存放题目名称
                    brr = new String[crr.length]; //用于存放字段名称
                    for (int i=0;i<crr.length;i++){
                        drr = crr[i].split(";");
                        if (drr != null && drr.length > 0){
                            arr[i] = drr[0]; //题目名称
                            brr[i] = drr[1]; //字段名称
                        }
                    }
                }
            }

            sql = "SELECT * FROM "+tableName+" "; //查询语句
            if (where != null && !where.equals("")){
                sql = sql + where;
            }
            DMap dMap = DaoTool.select(sql);
            if (dMap != null && dMap.getCount() > 0){
                err = new Object[dMap.getCount()][arr.length]; //用于存放需导出信息
                for (int i=0;i<dMap.getCount();i++){
                    for (int j=0;j<brr.length;j++){
                        System.out.println("brr["+j+"]======"+brr[j]);
                        err[i][j] = dMap.getValue(brr[j],i); //导出信息内容
                    }
                }
            }

            /**********导出部分Start**********/
            ExportExcel exportExcel = new ExportExcel();
            HSSFWorkbook wb = exportExcel.exportExcel(arr,err);
            Date date=new Date();
            response.setContentType("application/vnd.ms-excel; charset=utf-8");
            response.setHeader("Content-Disposition","attachment;filename="+date.getTime()+".xls");
            response.setCharacterEncoding("utf-8");
            OutputStream ouputStream = response.getOutputStream();
            wb.write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
            /**********导出部分End**********/
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}