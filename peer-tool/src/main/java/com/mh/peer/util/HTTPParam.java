package com.mh.peer.util;

/**
 * Created by WangMeng on 2016/7/30.
 */
public class HTTPParam {
    //key值
    private String key;
    //value值
    private String value;

    public HTTPParam() {

    }

    public HTTPParam(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
