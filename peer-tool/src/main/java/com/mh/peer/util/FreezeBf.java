package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;

import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-10-10.
 */
public class FreezeBf {

    /**
     * 冻结前备份
     * @param mapFreeze
     * @return
     */
    public static String freezeResult(Map<String,String> mapFreeze){
        DConnection conn = DaoTool.getConnection();
        String result = "0"; //结果(0-成功、1-失败)
        String id = ""; //主键
        String merBillNo = ""; //商户订单号
        String merDate = ""; //冻结日期
        String projectNo = ""; //项目ID号
        String bizType = ""; //业务类型(1-投标、2-债权转让、3-还款、4-分红、5-代偿、6-代偿还款、7-风险准备金、8-结算担保收益、9-红包、10-融资方保证金、11-投资人保证金、12-担保人保证金)
        String regType = ""; //登记方式(1-手动、2-自动)
        String contractNo = ""; //合同号
        String authNo = ""; //授权号
        String trdAmt = ""; //冻结金额(String型)
        Double trdAmtDou = 0.0d; //冻结金额(Double型)
        String merFee = "0"; //平台手续费(Stirng型)
        Double merFeeDou = 0.0d; //平台手续费(Double型)
        String freezeMerType = "1"; //冻结方类型(1-用户、2-商户)
        String ipsAcctNo = ""; //冻结账号
        String otherIpsAcctNo = ""; //它方账号
        String flag = "2"; //状态(0-冻结成功、1-冻结失败、2-待冻结)
        String memberId = ""; //会员id
        String createTime = ""; //创建时间
        String sqlFreeze = "";
        DaoResult daoResultFreeze = new DaoResult(); //冻结部分
        DMap dMapFreeze = new DMap();

        try{

            id = UUID.randomUUID().toString().replaceAll("-",""); //主键
            createTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //创建时间

            if (mapFreeze != null){
                merBillNo = mapFreeze.get("merBillNo"); //商户订单号
                merDate = mapFreeze.get("merDate"); //冻结日期
                projectNo = mapFreeze.get("projectNo"); //项目ID号
                bizType = mapFreeze.get("bizType"); //业务类型(1-投标、2-债权转让、3-还款、4-分红、5-代偿、6-代偿还款、7-风险准备金、8-结算担保收益、9-红包、10-融资方保证金、11-投资人保证金、12-担保人保证金)
                regType = mapFreeze.get("regType"); //登记方式(1-手动、2-自动)
                contractNo = mapFreeze.get("contractNo"); //合同号
                authNo = mapFreeze.get("authNo"); //授权号
                trdAmt = mapFreeze.get("trdAmt"); //冻结金额(String型)
                if (trdAmt != null && !trdAmt.equals("")){
                    trdAmtDou = TypeTool.getDouble(trdAmt); //冻结金额(Double型)
                }
                merFee = mapFreeze.get("merFee"); //平台手续费(String型)
                if (merFee != null && !merFee.equals("")){
                    merFeeDou = TypeTool.getDouble(merFee); //平台手续费(Double型)
                }
                freezeMerType = mapFreeze.get("freezeMerType"); //冻结方类型(1-用户、2-商户)
                ipsAcctNo = mapFreeze.get("ipsAcctNo"); //冻结账号
                otherIpsAcctNo = mapFreeze.get("otherIpsAcctNo"); //它方账号
                memberId = mapFreeze.get("memberId"); //会员id

                sqlFreeze = "INSERT INTO huanxun_freeze_bf(ID,MERBILLNO,MERDATE,PROJECTNO,BIZTYPE,REGTYPE,CONTRACTNO,AUTHNO,TRDAMT,MERFEE,FREEZEMERTYPE,IPSACCTNO,OTHERIPSACCTNO,MEMBERID,CREATETIME)"
                          + "VALUES(<id>,<merBillNo>,<merDate>,<projectNo>,<bizType>,<regType>,<contractNo>,<authNo>,<trdAmt>,<merFee>,<freezeMerType>,<ipsAcctNo>,<otherIpsAcctNo>,<memberId>,<createTime>)";
                dMapFreeze.setData("id",id); //主键
                dMapFreeze.setData("merBillNo",merBillNo); //商户订单号
                dMapFreeze.setData("merDate",merDate); //冻结日期
                dMapFreeze.setData("projectNo",projectNo); //项目ID号
                dMapFreeze.setData("bizType",bizType); //业务类型
                dMapFreeze.setData("regType",regType); //登记方式(1-手动、2-自动)
                dMapFreeze.setData("contractNo",contractNo); //合同号
                dMapFreeze.setData("authNo",authNo); //授权号
                dMapFreeze.setData("trdAmt",trdAmtDou); //冻结金额
                dMapFreeze.setData("merFee",merFeeDou); //平台手续费
                dMapFreeze.setData("freezeMerType",freezeMerType); //冻结方类型(1-用户、2-商户)
                dMapFreeze.setData("ipsAcctNo",ipsAcctNo); //冻结账号
                dMapFreeze.setData("otherIpsAcctNo",otherIpsAcctNo); //它方账号
                dMapFreeze.setData("memberId",memberId); //会员id
                dMapFreeze.setData("createTime",createTime); //创建时间
                daoResultFreeze = DaoTool.update(sqlFreeze,dMapFreeze.getData(),conn);
                if (daoResultFreeze.getCode() < 0){
                    result = "1";
                    conn.rollback();
                }
            }
        }catch (Exception e){
            result = "1";
            e.printStackTrace();
            conn.rollback();
        }finally {
            conn.commit();
            conn.close();
        }
        return result;
    }
}
