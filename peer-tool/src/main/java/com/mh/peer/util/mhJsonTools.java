package com.mh.peer.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Administrator on 2016/6/12.
 */
public class mhJsonTools{


    /**
     * 如果参数json格式 {"key":"value","key":"value"} 注：严格按这种格式
     *
     * @param paramter 取值可以赢 key  如：HashMap map=new HashMap() map.get("key");
     * @return HashMap
     */
    public static HashMap mhHashMap(String paramter) {
        ObjectMapper mapper = new ObjectMapper();
        HashMap map = null;
        try {
            map = mapper.readValue(paramter, HashMap.class);//readValue到一个原始数据类型.
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }
}
