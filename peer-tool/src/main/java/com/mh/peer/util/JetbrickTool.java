package com.mh.peer.util;

import jetbrick.template.JetEngine;
import jetbrick.template.JetTemplate;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.util.Map;

/**
 * Created by WangMeng on 2015/12/9.
 */
@Component
public class JetbrickTool {
    /**
     * 得到模板的HTML页面
     *
     * @param jetxName
     * @return
     */
    public StringWriter getJetbrickTemp(String jetxName, Map context) {
        // 1. 创建一个默认的 JetEngine
        JetEngine engine = JetEngine.create();
        // 2. 获取一个模板对象 (从默认的 classpath 下面)
        JetTemplate template = engine.getTemplate(jetxName);
        // 3. 渲染模板到自定义的 Writer
        StringWriter writer = new StringWriter();
        // 4. 渲染模板
        template.render(context, writer);
        return writer;
    }
}
