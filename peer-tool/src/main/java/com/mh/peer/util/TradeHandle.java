package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;

/**
 * Created by zhangerxin on 2016-6-16.
 */
public class TradeHandle {

    /**
     * 获取流水号
     *
     * @return
     */
    public int getSerialNumber() {
        String sql = "SELECT NUMBER FROM serialNumber_wh"; //查询流水号
        String number = "0"; //流水号(String型)
        int numberin = 0; //流水号(int型)
        DMap dMap = DaoTool.select(sql);
        if (dMap != null) {
            number = dMap.getValue("NUMBER", 0); //流水号(String型)
            if (number != null && !number.equals("")) {
                numberin = TypeTool.getInt(number);
                numberin = numberin + 1; //下一个流水号
            }
        }
        return numberin;
    }


    /**
     * 根据会员id获取投资收益
     * @param memberId
     * @return
     */
    public static String getInvestAmount(String memberId){
        String investAmount = "0"; //投资收益
        String sql = "SELECT IFNULL(SUM(INVESTAMOUNT),0)INVESTAMOUNT FROM view_product_buy_min WHERE BUYER = '"+memberId+"'";
        DMap dMap = DaoTool.select(sql);
        if (dMap != null && dMap.getCount() > 0){
            investAmount = dMap.getValue("INVESTAMOUNT",0); //投资收益
        }
        return investAmount;
    }

    /**
     * 获取提现流水号
     * @return
     */
    public static String getPostalSerialNumber(){
        DConnection conn = DaoTool.getConnection();
        String serialNumber = "0"; //流水号(String型)
        int serialNumberDou = 0; //流水号(int型)
        String sqlSearch = "SELECT NUMBER FROM huanxun_postal_serialNumber WHERE ID = 'bd8ca06491ce11e68c9f00163e00076c'";
        String sqlUpdate = "";
        DMap dMapSearch = new DMap();
        DMap dMapUpdate = new DMap();
        DaoResult daoResult = new DaoResult();

        try{

            dMapSearch = DaoTool.select(sqlSearch); //提现流水号查询
            if (dMapSearch != null && dMapSearch.getCount() > 0){
                serialNumber = dMapSearch.getValue("NUMBER",0); //流水号(String型)
                if (serialNumber != null && !serialNumber.equals("")){
                    serialNumberDou = TypeTool.getInt(serialNumber);
                    serialNumberDou = serialNumberDou + 1;
                    serialNumber = TypeTool.getString(serialNumberDou);

                    sqlUpdate = "UPDATE huanxun_postal_serialNumber SET NUMBER = <serialNumber> WHERE ID = 'bd8ca06491ce11e68c9f00163e00076c'";
                    dMapUpdate.setData("serialNumber",serialNumber); //流水号
                    daoResult = DaoTool.update(sqlUpdate,dMapUpdate.getData(),conn);
                    if (daoResult.getCode() < 0){
                        conn.rollback();
                    }
                }
            }
        }catch(Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }
        return serialNumber;
    }
}
