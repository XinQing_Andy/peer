package com.mh.peer.util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by zhangerxin on 2016-6-30.
 * 3des加密
 */
@SuppressWarnings({ "restriction" })
public class Pdes {

    private static final String handleKey = "zN2umWsMgFBY8gLkkdpJT4mH"; //密钥
    private static final String iv = "kXQ5Uyfz"; //向量


    /**
     * 3DES加密
     * @param encryptString
     * @return
     */
    public static String encrypt3DES(String encryptString)  {
        try{
            IvParameterSpec zeroIv = new IvParameterSpec(iv.getBytes("UTF-8"));
            SecretKeySpec key = new SecretKeySpec(handleKey.getBytes("UTF-8"), "DESede");
            Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
            byte[] encryptedData = cipher.doFinal(encryptString.getBytes("UTF-8"));
            String encryptStr = new BASE64Encoder().encode(encryptedData).replaceAll("\n",""); //加密后字段
            return encryptStr;
        }catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }


    /**
     * 3des解密
     * @param decryptString
     * @return
     */
    public static String decrypt3DES(String decryptString) {
        try{
            byte[] byteMi =new BASE64Decoder().decodeBuffer(decryptString);
            IvParameterSpec zeroIv = new IvParameterSpec(iv.getBytes("UTF-8"));
            SecretKeySpec key = new SecretKeySpec(handleKey.getBytes("UTF-8"), "DESede");
            Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);
            byte decryptedData[] = cipher.doFinal(byteMi);
            String decryptStr = new String(decryptedData,"UTF-8"); //解密后字段
            return new String(decryptedData,"UTF-8");
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
