package com.mh.peer.util;

/**
 * Created by zhangerxin on 2016-6-28.
 */
public class PublicAddress {

    //private final String emailAddress = "http://192.168.1.103:8087/"; //邮箱地址
    //private final String serverAddress = "http://192.168.1.103:8087/"; //服务器地址
    //private final String checkUpdateAndriod = "E:/mhwork/peer/peer/peer-web/target/peer-web/checkUpdate/andriod/"; //安卓部分路径(绝对)
    //private final String checkUpdateIos = "E:/mhwork/peer/peer/peer-web/target/peer-web/checkUpdate/ios/"; //ios部分路径(绝对)
    //private final String checkUpdateAndriodXd = "http://192.168.1.103:8087/checkUpdate/andriod/"; //安卓相对路径

    private final String emailAddress = "http://120.76.137.147/"; //邮箱地址
    private final String serverAddress = "http://120.76.137.147/"; //服务器地址
    private final String checkUpdateAndriod = "/usr/tomcat7/webapps/peer-web/checkUpdate/andriod/"; //安卓部分路径(绝对)
    private final String checkUpdateIos = "/usr/tomcat7/webapps/peer-web/checkUpdate/ios/"; //ios部分路径(绝对)
    private final String checkUpdateAndriodXd = "http://120.76.137.147/checkUpdate/andriod/"; //安卓相对路径

    //返回邮箱地址
    public String getEmailAddress(){
        return emailAddress;
    }

    //返回服务器地址
    public String getServerAddress() {
        return serverAddress;
    }

    public String getCheckUpdateAndriod() {
        return checkUpdateAndriod;
    }

    public String getCheckUpdateIos() {
        return checkUpdateIos;
    }

    public String getCheckUpdateAndriodXd() {
        return checkUpdateAndriodXd;
    }
}
