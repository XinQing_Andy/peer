package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-10-13.
 * 充值备份
 */
public class ReChargeBf {

    /**
     * 充值前备份
     * @param map
     * @return
     */
    public static String reChargeHandle(Map<String,String> map){
        DConnection conn = DaoTool.getConnection();
        String result = "0"; //结果
        String Uid = ""; //主键
        String memberId = ""; //会员id
        String merBillNo = ""; //商户订单号
        String merDate = ""; //充值日期
        String depositType = ""; //充值类型(1-普通充值、2-还款充值)
        String channelType = ""; //充值渠道(1-个人网银、2-企业网银)
        String bankCode = ""; //充值银行号
        String userType = ""; //用户类型(1-个人、2-企业)
        String ipsAcctNo = ""; //IPS存管账户号
        String trdAmt = ""; //充值金额(String型)
        Double trdAmtDou = 0.0d; //充值金额(Double型)
        String ipsFeeType = ""; //IPS手续费承担方(1-平台商户、2-平台用户)
        String merFee = ""; //平台手续费(String型)
        Double merFeeDou = 0.0d; //平台手续费(Double型)
        String merFeeType = ""; //平台手续费收取方式(1-内扣、2-外扣)
        String taker = ""; //发起方(1-商户发起、2-用户发起)
        String flag = "1"; //状态(0-已读、1-未读)
        String createTime = ""; //创建时间
        String sqlRecharge = ""; //充值
        DaoResult daoResultRecharge = new DaoResult(); //充值部分
        DMap dMapRecharge = new DMap();

        try{

            Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
            createTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //创建时间

            if (map != null){
                memberId = map.get("memberId"); //会员id
                merBillNo = map.get("merBillNo"); //商户订单号
                merDate = map.get("merDate"); //充值日期
                depositType = map.get("depositType"); //充值类型(1-普通充值、2-还款充值)
                channelType = map.get("channelType"); //充值渠道(1-个人网银、2-企业网银)
                bankCode = map.get("bankCode"); //充值银行号
                userType = map.get("userType"); //用户类型(1-个人、2-企业)
                ipsAcctNo = map.get("ipsAcctNo"); //IPS存管账户号
                trdAmt = map.get("trdAmt"); //充值金额(String型)
                if (trdAmt != null && !trdAmt.equals("")){
                    trdAmtDou = TypeTool.getDouble(trdAmt); //充值金额(Double型)
                }
                ipsFeeType = map.get("ipsFeeType"); //IPS手续费承担方(1-平台商户、2-平台用户)
                merFee = map.get("merFee"); //平台手续费(String型)
                if (merFee != null && !merFee.equals("")){
                    merFeeDou = TypeTool.getDouble(merFee); //平台手续费(Double型)
                }
                merFeeType = map.get("merFeeType"); //平台手续费收取方式(1-内扣、2-外扣)
                taker = map.get("taker"); //发起方(1-商户发起、2-用户发起)
                sqlRecharge = "INSERT INTO huanxun_recharge_bf(ID,MEMERID,MERBILLNO,MERDATE,DEPOSITTYPE,CHANNELTYPE,BANKCODE,USERTYPE,IPSACCTNO,TRDAMT,IPSFEETYPE,MERFEE,MERFEETYPE,TAKER,FLAG,CREATETIME)"
                            + "VALUES(<Uid>,<memberId>,<merBillNo>,<merDate>,<depositType>,<channelType>,<bankCode>,<userType>,<ipsAcctNo>,<trdAmtDou>,<ipsFeeType>,<merFeeDou>,<merFeeType>,<taker>,<flag>,<createTime>)";
                dMapRecharge.setData("Uid",Uid); //主键
                dMapRecharge.setData("memberId",memberId); //会员id
                dMapRecharge.setData("merBillNo",merBillNo); //商户订单号
                dMapRecharge.setData("merDate",merDate); //充值日期
                dMapRecharge.setData("depositType",depositType); //充值类型(1-普通充值、2-还款充值)
                dMapRecharge.setData("channelType",channelType); //充值渠道(1-个人网银、2-企业网银)
                dMapRecharge.setData("bankCode",bankCode); //充值银行号
                dMapRecharge.setData("userType",userType); //用户类型(1-个人、2-企业)
                dMapRecharge.setData("ipsAcctNo",ipsAcctNo); //IPS存管账户号
                dMapRecharge.setData("trdAmtDou",trdAmtDou); //充值金额(Double型)
                dMapRecharge.setData("ipsFeeType",ipsFeeType); //IPS手续费承担方(1-平台商户、2-平台用户)
                dMapRecharge.setData("merFeeDou",merFeeDou); //平台手续费(Double型)
                dMapRecharge.setData("merFeeType",merFeeType); //平台手续费收取方式(1-内扣、2-外扣)
                dMapRecharge.setData("taker",taker); //发起方(1-商户发起、2-用户发起)
                dMapRecharge.setData("flag",flag); //状态(0-已读、1-未读)
                dMapRecharge.setData("createTime",createTime); //创建日期
                daoResultRecharge = DaoTool.update(sqlRecharge,dMapRecharge.getData(),conn);
                if (daoResultRecharge.getCode() < 0){
                    result = "1";
                    conn.rollback();
                }
            }
        }catch(Exception e){
            result = "1";
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }
        return result;
    }
}
