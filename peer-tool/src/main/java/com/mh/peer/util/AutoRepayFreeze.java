package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-8-6.
 * 自动还款
 */
public class AutoRepayFreeze {

    /**
     * 自动还款部分冻结
     * @param operaTime
     * @return
     */
    public String freezeRepayhx(String operaTime){
        System.out.println("开始冻结操作");
        String productId = ""; //产品id
        String operationType = "trade.freeze"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        String merBillNo = "181006"; //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //登记日期
        String projectNo = ""; //项目ID号
        String bizType = "3"; //业务类型(1-投标、2-债权转让、3-还款)
        String regType = "2"; //登记方式(1-手动、2-自动)
        String contractNo = ""; //合同号
        String authNo = ""; //授权号
        String trdAmt = ""; //冻结金额(String型)
        Double trdAmtDou = 0.0d; //冻结金额(Double型)
        String merFee = "0"; //平台手续费
        String freezeMertype = "1"; //冻结方类型(1-用户、2-商户)
        String ipsAcctNo = ""; //冻结账号
        String otherIpsAcctNo = ""; //它方账号
        String WebUrl = "http://jianbing88.com"; //页面返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/repayhxResult?operaTime="+operaTime; //后台通知地址
        String md5Zs = ""; //md5证书
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm"; //请求地址
        String result = ""; //获取接口调用后返回信息
        String resultCode = ""; //响应吗
        String resultMsg = ""; //响应信息
        String freezeResult = "0"; //冻结成功
        String repayId = ""; //还款id
        String UidHuanXunRepay = ""; //冻结还款部分主键

        try{

            /********** 获取md5证书Start **********/
            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs();
            /*********** 获取md5证书End ***********/

            DMap dMap = this.getRepayInfoList(operaTime); //获取需还款信息
            System.out.println("符合规则的还款数量为======"+dMap.getCount());

            if (dMap != null && dMap.getCount() > 0){
                for (int i=0;i<dMap.getCount();i++){
                    merBillNo = "181006"; //商户订单号
                    merBillNo = merBillNo + UUID.randomUUID().toString().replace("-","");
                    projectNo = dMap.getValue("PROJECTNO",i); //项目ID号
                    contractNo = dMap.getValue("OUTAUTOSIGNBILLNO",i); //自动签约订单号
                    authNo = dMap.getValue("OUTAUTOSIGNAUTHNO",i); //自动签约授权号
                    trdAmtDou = TypeTool.getDouble(dMap.getValue("PRINCIPAL", i)) + TypeTool.getDouble(dMap.getValue("INTEREST",i)); //冻结金额
                    ipsAcctNo = dMap.getValue("OUTIPSACCTNO",i); //冻结账号
                    otherIpsAcctNo = dMap.getValue("INIPSACCTNO",i); //它方账号
                    repayId = dMap.getValue("REPAYID",i); //还款id
                    UidHuanXunRepay = UUID.randomUUID().toString().replaceAll("-",""); //环迅还款主键
                    s2SUrl = s2SUrl + "?repayId="+repayId+"&UidHuanXunRepay="+UidHuanXunRepay; //后台返回地址

                    request = "{\"projectNo\":\""+projectNo+"\",\"merBillNo\":\""+merBillNo+"\",\"bizType\":\""+bizType+"\",\"regType\":\""+regType+"\",\"contractNo\":\""+contractNo+"\",\"authNo\":\""+authNo+"\",\"trdAmt\":\""+trdAmtDou+"\",\"merFee\":\""+merFee+"\",\"freezeMerType\":\""+freezeMertype+"\",\"otherIpsAcctNo\":\""+otherIpsAcctNo+"\",\"ipsAcctNo\":\""+ipsAcctNo+"\",\"merDate\":\""+merDate+"\",\"webUrl\":\""+WebUrl+"\",\"s2SUrl\":\""+s2SUrl+"\"}";
                    requestJm = Pdes.encrypt3DES(request); //3des加密
                    sign = operationType + merchantID + requestJm + md5Zs; //签名
                    signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密
                    System.out.println("还款报文是："+request);

                    List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
                    HTTPParam httpParam1 = new HTTPParam("operationType",operationType); //操作类型
                    HTTPParam httpParam2 = new HTTPParam("merchantID",merchantID); //商户存管交易账号
                    HTTPParam httpParam3 = new HTTPParam("request",requestJm); //请求信息
                    HTTPParam httpParam4 = new HTTPParam("sign",signJm); //签名

                    listHTTParam.add(httpParam1);
                    listHTTParam.add(httpParam2);
                    listHTTParam.add(httpParam3);
                    listHTTParam.add(httpParam4);

                    result = HttpRequest.sendPost(url,listHTTParam); //获取接口调用后返回信息
                    System.out.println("自动还款冻结部分返回信息为：");
                    System.out.println(result);
                }
            }
        }catch (Exception e){
            freezeResult = "1";
            e.printStackTrace();
        }
        return freezeResult;
    }


    /**
     * 获取需还款信息
     * @param operaTime
     * @return
     */
    public DMap getRepayInfoList(String operaTime){
        String sql = "SELECT * FROM view_product_autoRepay WHERE 1 = 1 AND RECEIVEPRICE = 0 AND SRECEIVETIME IS NULL";
        if (operaTime != null && !operaTime.equals("")){
            sql = sql + " AND DATE(YRECEIVETIME) = DATE('"+operaTime+"')";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
