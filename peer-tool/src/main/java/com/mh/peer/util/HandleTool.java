package com.mh.peer.util;

/**
 * Created by zhangerxin on 2016-4-14.
 * 去除Map类型的[]
 */
public class HandleTool {

    /**
     * 去除根据id查询出现的[]
     *
     * @param str
     * @return
     */
    public String getParamById(String str) {
        String param = "";
        if (str != null && !str.equals("")) {
            if (!str.equals("[]")) {
                param = str.substring(1, str.length() - 1);
            }
        }
        return param;
    }
}
