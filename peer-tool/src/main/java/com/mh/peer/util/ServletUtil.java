package com.mh.peer.util;

import com.salon.frame.data.DMap;
import com.salon.frame.util.DConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;
import org.springframework.ui.context.Theme;
import org.springframework.util.ClassUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ThemeResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;

/**
 * Created by WangMeng on 2015-05-10.
 */
@Component
public class ServletUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServletUtil.class);

    /**
     * 得到HttpServletRequest
     *
     * @return
     */
    public HttpServletRequest getRequest() {
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (sra == null)
            return null;

        return sra.getRequest();
    }

    /**
     * 得到会话
     *
     * @return
     */
    public HttpSession getSession() {
        return getRequest().getSession();
    }

    /**
     * 得到会话的对象
     *
     * @param key
     * @return
     */
    public Object getSessionValue(String key) {
        return getSession().getAttribute(key);
    }

    /**
     * 设置会话对象
     *
     * @param key
     * @param obj
     */
    public void setSessionValue(String key, Object obj) {
        getSession().setAttribute(key, obj);
    }

    /**
     * 得到ServletContext
     *
     * @return
     */
    public ServletContext getServletContext() {
        return getSession().getServletContext();
    }

    /**
     * 得到ApplicationContext
     *
     * @return
     */
    public ApplicationContext getApplicationContext() {
        return WebApplicationContextUtils.getWebApplicationContext(getServletContext());
    }

    /**
     * 得到会话
     *
     * @param create
     * @return
     */
    public HttpSession getSession(boolean create) {
        return getRequest().getSession(create);
    }

    /**
     * 得到会话ID
     *
     * @return
     */
    public String getSessionId() {
        return getSession().getId();
    }

    /**
     * 得到本地化
     *
     * @return
     */
    public Locale getLocale() {
        return RequestContextUtils.getLocale(getRequest());
    }

    /**
     * 得到主题
     *
     * @return
     */
    public Theme getTheme() {
        return RequestContextUtils.getTheme(getRequest());
    }

    public ApplicationEventPublisher getApplicationEventPublisher() {
        return getApplicationContext();
    }

    public LocaleResolver getLocaleResolver() {
        return RequestContextUtils.getLocaleResolver(getRequest());
    }

    public ThemeResolver getThemeResolver() {
        return RequestContextUtils.getThemeResolver(getRequest());
    }

    public ResourceLoader getResourceLoader() {
        return getApplicationContext();
    }

    public ResourcePatternResolver getResourcePatternResolver() {
        return getApplicationContext();
    }

    public MessageSource getMessageSource() {
        return getApplicationContext();
    }

    public ConversionService getConversionService() {
        return getBeanFromApplicationContext(ConversionService.class);
    }

    public DataSource getDataSource() {
        return getBeanFromApplicationContext(DataSource.class);
    }

    public Collection<String> getActiveProfiles() {
        return Arrays.asList(getApplicationContext().getEnvironment().getActiveProfiles());
    }

    public ClassLoader getBeanClassLoader() {
        return ClassUtils.getDefaultClassLoader();
    }

    /**
     * 得到Spring加载对象
     *
     * @param requiredType
     * @param <T>
     * @return
     */
    private <T> T getBeanFromApplicationContext(Class<T> requiredType) {
        try {
            return getApplicationContext().getBean(requiredType);
        } catch (NoUniqueBeanDefinitionException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        } catch (NoSuchBeanDefinitionException e) {
            LOGGER.warn(e.getMessage());
            return null;
        }
    }

    /**
     * 获取资源文件
     *
     * @param fileName
     * @return
     */
    private DMap getResourcesFile(String fileName) {
        return DConfig.loadFile("%ROOT%/WEB-INF/classes/" + fileName);
    }

    /**
     * 获得资源文件属性值
     *
     * @param fileName
     * @param groupName
     * @param key
     * @return
     */
    public String getPropertiesFileVale(String fileName, String groupName, String key) {
        DMap parm = getResourcesFile(fileName);
        return parm.getValue(groupName, key);
    }

    /**
     * 获得资源文件值数组
     *
     * @param fileName
     * @param groupName
     * @param key
     * @return
     */
    public String[] getPropertiesFileArray(String fileName, String groupName, String key) {
        DMap parm = getResourcesFile(fileName);
        return parm.getStringList(groupName, key);
    }

    /**
     * 是否登陆
     *
     * @return
     */
    public boolean isLogin() {
        boolean falg = true;
        HttpSession session = getSession();
        if (session.getAttribute("user") == null) {
            falg = false;
        }
        return falg;
    }
}
