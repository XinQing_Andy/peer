package com.mh.peer.util;

import com.mh.peer.exception.HuanXunTransferRepayException;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

/**
 * Created by zhangerxin on 2016-8-7.
 * 自动转账
 */
public class AutoTransfer {

    private static final Logger LOGGER = LoggerFactory.getLogger(AutoTransfer.class);

    /**
     * 自动还款转账部分
     * @param operateTime
     * @return
     */
    public void transferhx(String operateTime){

        String freezeRepayId = ""; //冻结还款主键
        String productId = ""; //产品id
        String yrepaymentTime = ""; //应还款日期
        String repayId = ""; //还款id
        String freezeId = ""; //IPS原冻结订单号
        String projectNo = ""; //项目ID号
        String outIpsAcctNo = ""; //转出方IPS存管账户

        try{

            DMap dMapFreezeRepay = this.getFreezeRepayList(operateTime);
            System.out.println("徐转章数量："+dMapFreezeRepay.getCount());
            if (dMapFreezeRepay != null && dMapFreezeRepay.getCount() > 0){
                for (int i=0;i<dMapFreezeRepay.getCount();i++){
                    freezeRepayId = dMapFreezeRepay.getValue("ID",i); //冻结还款主键
                    productId = dMapFreezeRepay.getValue("PRODUCTID",i); //产品id
                    yrepaymentTime = dMapFreezeRepay.getValue("YREPAYMENTTIME",i); //应还款日期
                    repayId = dMapFreezeRepay.getValue("REPAYID",i); //还款id
                    freezeId = dMapFreezeRepay.getValue("IPSBILLNO",i); //IPS原冻结订单号
                    projectNo = dMapFreezeRepay.getValue("PROJECTNO",i); //项目ID号
                    outIpsAcctNo = dMapFreezeRepay.getValue("IPSACCTNO",i); //转出方IPS账户
                    System.out.println("开始自动转账");
                    this.repayhxTransfer(freezeRepayId,productId,yrepaymentTime,repayId,freezeId,projectNo,outIpsAcctNo);
                    System.out.println("自动转账结束");
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 获取冻结信息
     * @param operaTime
     * @return
     */
    public DMap getFreezeRepayList(String operaTime){
        String sql = "SELECT ID,PRODUCTID,IPSBILLNO,YREPAYMENTTIME,PROJECTNO,REPAYID,IPSACCTNO"
                   +" FROM view_huanxun_freeze_repay WHERE DATE(CREATETIME) = DATE('"+operaTime+"') AND RESULTCODE = '000000' AND FLAG = '0'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 根据冻结信息进行还款转账
     * @param freezeRepayId
     * @param productId
     * @param yrepaymentTime
     * @param repayId
     * @param freezeId
     * @param projectNo
     * @param outIpsAcctNo
     */
    public void repayhxTransfer(String freezeRepayId,String productId,String yrepaymentTime,String repayId,
                                    String freezeId,String projectNo,String outIpsAcctNo) {
        String operationType = "trade.transfer"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String requesthx = ""; //请求信息
        String requesthxJm = ""; //请求信息(加密)
        String transferType = "3"; //转账类型(3-还款)
        String s2SUrl = "http://120.76.137.147/huanxun/transferRepayResult"; //后台地址
        String batchNo = ""; //商户转账批次编号
        String merDate = ""; //转账日期
        String isAutoRepayment = "2"; //是否自动还款(1-是、2-否)
        String transferMode = "2"; //1-逐笔转账、2-批量转账
        String md5Zs = ""; //md5证书
        String transferAccDetail = ""; //转账明细集合
        String merBillNo = ""; //商户订单号
        String outMerFee = "0"; //转出方平台手续费
        String inIpsAcctNo = ""; //转入方IPS存管账户
        String inMerFee = "0"; //转入方平台手续费
        String trdAmt = ""; //转账金额
        String transferId = ""; //转账表主键
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm"; //环迅接口调用地址
        String resultCode = ""; //响应状态
        String resultMsg = ""; //响应信息
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息
        String responsehx = ""; //返回信息
        String principal = ""; //应收本金(String型)
        Double principalDou = 0.0d; //应收本金(Double型)
        String interest = ""; //应收利息(String型)
        Double interestDou = 0.0d; //应收利息(Double型)
        Double receivePriceDou = 0.0d; //需还款金额
        String buyer = ""; //投资人id

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs(); //md5证书

            transferId = UUID.randomUUID().toString().replaceAll("-",""); //转账表主键
            batchNo = "181006" + UUID.randomUUID().toString().replaceAll("-",""); //商户转账批次编号
            merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //转账日期

            //转账细节信息
            DMap dMapTransferAcctDetail = this.getRepayTransferAcctDetail(productId,yrepaymentTime);
            if (dMapTransferAcctDetail != null && dMapTransferAcctDetail.getCount() > 0){
                for (int i=0;i<dMapTransferAcctDetail.getCount();i++){
                    merBillNo = "181006" + UUID.randomUUID().toString().replaceAll("-",""); //商户订单号
                    inIpsAcctNo = dMapTransferAcctDetail.getValue("BUYERIPSACCTNO",i); //转入方IPS存管账户

                    principal = dMapTransferAcctDetail.getValue("PRINCIPAL",i); //应收本金
                    if (principal != null && !principal.equals("")){
                        principalDou = TypeTool.getDouble(principal);
                    }
                    interest = dMapTransferAcctDetail.getValue("INTEREST",i); //利息
                    if (interest != null && !interest.equals("")){
                        interestDou = TypeTool.getDouble(interest);
                    }
                    receivePriceDou = principalDou + interestDou; //需还款金额
                    trdAmt = TypeTool.getString(receivePriceDou); //转账金额
                    buyer = dMapTransferAcctDetail.getValue("BUYER",i); //投资人
                    System.out.println("投资人id======"+buyer);
                    inMerFee = MerFeeHandle.getRepayInFee(buyer,interest); //转入方服务费(String型)

                    if (transferAccDetail == null || transferAccDetail.equals("")){
                        transferAccDetail = "{"
                                          + "\"merBillNo\":\"" + merBillNo + "\","
                                          + "\"freezeId\":\"" + freezeId + "\","
                                          + "\"outIpsAcctNo\":\"" + outIpsAcctNo + "\","
                                          + "\"outMerFee\":\"" + outMerFee + "\","
                                          + "\"inIpsAcctNo\":\"" + inIpsAcctNo + "\","
                                          + "\"inMerFee\":\"" + inMerFee + "\","
                                          + "\"trdAmt\":\"" + trdAmt + "\""
                                          + "}";
                    }else{
                        transferAccDetail += ",{"
                                           + "\"merBillNo\":\"" + merBillNo + "\","
                                           + "\"freezeId\":\"" + freezeId + "\","
                                           + "\"outIpsAcctNo\":\"" + outIpsAcctNo + "\","
                                           + "\"outMerFee\":\"" + outMerFee + "\","
                                           + "\"inIpsAcctNo\":\"" + inIpsAcctNo + "\","
                                           + "\"inMerFee\":\"" + inMerFee + "\","
                                           + "\"trdAmt\":\"" + trdAmt + "\""
                                           + "}";
                    }
                }

                //后台通知地址
                s2SUrl = s2SUrl + "?transferId="+transferId+"&yrepaymentTime="+yrepaymentTime+"&repayId="+repayId;

                requesthx = "{"
                        + "\"batchNo\":\"" + batchNo + "\","
                        + "\"merDate\":\"" + merDate + "\","
                        + "\"projectNo\":\"" + projectNo + "\","
                        + "\"transferType\":\"" + transferType + "\","
                        + "\"isAutoRepayment\":\"" + isAutoRepayment + "\","
                        + "\"transferMode\":\"" + transferMode + "\","
                        + "\"s2SUrl\":\"" + s2SUrl + "\","
                        + "\"transferAccDetail\":"
                        + "["
                        + transferAccDetail
                        + "]"
                        + "}";

                requesthxJm = Pdes.encrypt3DES(requesthx); //3des加密
                sign = operationType + merchantID + requesthxJm + md5Zs; //签名
                signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

                List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
                HTTPParam httpParam1 = new HTTPParam("operationType", operationType); //操作类型
                HTTPParam httpParam2 = new HTTPParam("merchantID", merchantID); //商户存管交易账号
                HTTPParam httpParam3 = new HTTPParam("request", requesthxJm); //请求信息
                HTTPParam httpParam4 = new HTTPParam("sign", signJm); //签名
                listHTTParam.add(httpParam1);
                listHTTParam.add(httpParam2);
                listHTTParam.add(httpParam3);
                listHTTParam.add(httpParam4);

                String result = HttpRequest.sendPost(url, listHTTParam); //获取接口调用后返回信息

                Map<String, String> map = JsonHelper.getObjectToMap(result); //将Json数据转换为Map格式
                if (map != null) {
                    resultCode = map.get("resultCode"); //响应状态(000000-成功、999999-失败)
                    resultMsg = map.get("resultMsg"); //响应信息
                    responsehx = map.get("response"); //返回信息

                    mapException.put("resultCode",resultCode); //响应状态(000000-成功、999999-失败)
                    mapException.put("resultMsg",resultMsg); //响应信息
                    mapException.put("batchNo",batchNo); //商户转账批次号
                    mapException.put("projectNo",projectNo); //项目ID号
                    mapException.put("transferType",transferType); //转账类型(3-还款)
                    mapException.put("response",responsehx); //返回信息
                    mapException.put("freezeRepayId",freezeRepayId); //环迅冻结表主键

                    if (resultCode != null && !resultCode.equals("")){
                        if (!resultCode.equals("000000")){ //成功
                            HuanXunTransferRepayException.handle(mapException);
                        }
                    }
                }
            }
        }catch(Exception e){
            HuanXunTransferRepayException.handle(mapException);
            e.printStackTrace();
        }
    }


    /**
     * 获取转账详细信息
     * @param productId
     * @param yrepaymentTime
     * @return
     */
    public DMap getRepayTransferAcctDetail(String productId,String yrepaymentTime){
        String sql = "SELECT BUYERIPSACCTNO,PRINCIPAL,INTEREST,BUYER FROM view_product_receive WHERE"
                   + " PRODUCTID = '"+productId+"' AND DATE(YRECEIVETIME) = DATE('"+yrepaymentTime+"')";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

}
