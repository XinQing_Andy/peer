package com.mh.peer.util;

/**
 * Created by zhangerxin 2016/4/27.
 * 加密部分
 */
public class Encrypt {

    public String handleMd5(String str) {
        String jmStr = ""; //加密后字符串
        if (str != null && !str.equals("")) {
            jmStr = MD5Util.MD5(str);
        }

        return jmStr;
    }
}
