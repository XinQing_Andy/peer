package com.mh.peer.service;

import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import javax.servlet.ServletContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by zhangerxin on 2016-5-16.
 */
public class MonitorService implements Runnable {

    private Thread thread;
    private ServletContext context; //上下文
    private static int repayTimes; //还款次数
    private long period; //下次执行时间

    public MonitorService(ServletContext context) {
        this.context = context;
    }

    /**
     * 开始
     */
    public void start() {
        if (thread != null)
            return;
        thread = new Thread(this);
        thread.setName("peer MonitorSession");
        thread.start();
        System.out.println("线程开始!");
    }


    /**
     * 结束
     */
    public void stop() {
        thread = null;
    }

    @Override
    public void run() {
        System.out.println("线程开始运行！");
        this.work();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public void work() {
        String listenerTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //当前时间(String型)
        Date listenerTimeDate = null; //当前时间(Date型)

        /********** 转天凌晨时间Start **********/
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0); //小时
        calendar.set(Calendar.MINUTE, 0); //分钟
        calendar.set(Calendar.SECOND, 0); //秒
        calendar.set(Calendar.MILLISECOND, 0); //毫秒
        period = (calendar.getTimeInMillis() - System.currentTimeMillis()); //剩余时间
        System.out.println("据凌晨间隔时间为：" + period);
        /*********** 转天凌晨时间End ***********/

        if (listenerTime != null && !listenerTime.equals("")) { //当前时间
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                listenerTimeDate = sdf.parse(listenerTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        /********** 时间任务Start **********/
        TimerTask task = new SynchroTimerTask();
        Timer timer = new Timer();
        timer.schedule(task, listenerTimeDate, period);
        /*********** 时间任务End ***********/
    }
}
