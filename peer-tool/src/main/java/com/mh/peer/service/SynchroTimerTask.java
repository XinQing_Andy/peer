package com.mh.peer.service;

import com.mh.peer.util.AutoRepayFreeze;
import com.mh.peer.util.AutoTransfer;
import com.mh.peer.util.DateHandle;
import com.mh.peer.util.HandleTool;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import java.util.TimerTask;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-5-16.
 */
public class SynchroTimerTask extends TimerTask {

    @Override
    public void run() {
        System.out.println("任务开始");
        String startTime = ""; //开始监听时间
        String currentTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //当前时间
        System.out.println("当前时间为："+currentTime);
        String operateTime = ""; //操作时间
        long days = 0; //相差时间差
        String sqlListener = "SELECT LISTENERTIME FROM sys_listener"; //获取监听时间语句
        DateHandle dateHandle = new DateHandle(); //时间格式处理

        DMap dmapListener = DaoTool.select(sqlListener); //查询监听信息
        if (dmapListener != null && dmapListener.getCount() > 0) {
            startTime = dmapListener.getValue("LISTENERTIME",0); //开始监听时间
            days = Math.abs(dateHandle.getDays(startTime, currentTime)); //获取距离上次监听所相差的天数
            System.out.println("上一次调用的时间为："+startTime);
            System.out.println("相差天数为："+days);
            operateTime = dateHandle.getTimeAfterStart(startTime, 1); //操作日期

            /********** 自动还款冻结操作Start **********/
            if (days >= 1) { //大于一天
                for (int i = 1; i <= days; i++) {
                    this.work(operateTime);
                    System.out.println("第"+i+"次调用....");
                    operateTime = dateHandle.getTimeAfterStart(operateTime, 1); //操作日期
                }
            } else { //小于一天
                System.out.println("只调用一次");
                this.work(currentTime); //进行自动还款操作
            }
            /*********** 自动还款冻结操作End ***********/

        }
        this.updateListener(currentTime); //更新监听器部分
        System.out.println("任务结束");
    }


    /**
     * 进行自动还款操作
     *
     * @param operateTime
     */
    public void work(String operateTime) {
        AutoRepayFreeze autoRepayFreeze = new AutoRepayFreeze();
        String result = autoRepayFreeze.freezeRepayhx(operateTime); //根据当前时间对钱进行冻结
        if (result != null && !result.equals("")){
            if (result.equals("0")){ //成功
                System.out.println("冻结成功，将进入自动转账！");
                AutoTransfer autoTransfer = new AutoTransfer();
                autoTransfer.transferhx(operateTime);
            }else{
                System.out.println("自动还款失败！");
            }
        }
    }


    /**
     * 更新监听器时间
     */
    public void updateListener(String currentTime) {
        System.out.println("更新监听器！");
        String sqlListenerQuery = "SELECT * FROM sys_listener"; //监听器部分查询
        DMap dmapListenerQuery = DaoTool.select(sqlListenerQuery);
        DConnection conn = DaoTool.getConnection();
        DaoResult resultListener = new DaoResult(); //监听器部分
        DaoResult resultListenerDetails = new DaoResult(); //监听器详细信息部分
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //监听器详细信息部分主键
        String sqlListener = "UPDATE sys_listener SET LISTENERTIME = <currentTime> WHERE ID = 'd836313c1f3111e69b6d00163e0063ab'";
        String sqlListenerDetails = "INSERT INTO sys_listener_details(ID,LISTENERID,OPERATETIME)VALUES(<id>,<listenerId>,<operateTime>)";

        try{

            DMap dmapListenerUpdate = new DMap(); //更新监听器操作
            dmapListenerUpdate.setData("currentTime", currentTime);
            resultListener = DaoTool.update(sqlListener, dmapListenerUpdate.getData(), conn);
            if (resultListener.getCode() < 0){
                conn.rollback();
            }else{
                DMap dMapListenerDetails = new DMap(); //监听器详细信息部分
                dMapListenerDetails.setData("id",Uid); //主键
                dMapListenerDetails.setData("listenerId","d836313c1f3111e69b6d00163e0063ab"); //监听器主键
                dMapListenerDetails.setData("operateTime",currentTime); //操作日期
                resultListenerDetails = DaoTool.update(sqlListenerDetails,dMapListenerDetails.getData(),conn);
                if (resultListenerDetails.getCode() < 0){
                    conn.rollback();
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            conn.rollback();
        }finally{
            conn.commit();
            conn.close();
            System.out.println("监听器更新结束");
        }
    }
}
