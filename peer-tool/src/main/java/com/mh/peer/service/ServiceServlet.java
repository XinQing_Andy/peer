package com.mh.peer.service;

import javax.servlet.http.HttpServlet;

/**
 * Created by zhangerxin on 2016-5-16.
 */
public class ServiceServlet extends HttpServlet {

    /**
     * 监听器
     */
    private MonitorService monitorService;

    /**
     * 初始化
     */
    public void init() {
        System.out.println("进入监听器");
        monitorService = new MonitorService(getServletContext());
        monitorService.start();
    }
}
