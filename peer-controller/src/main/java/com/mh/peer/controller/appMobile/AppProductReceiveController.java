package com.mh.peer.controller.appMobile;

import com.mh.peer.model.message.AppProductReceiveMessageBean;
import com.mh.peer.model.message.AppProductReceiveTjMessageBean;
import com.mh.peer.service.product.ProductReceiveService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhangerxin on 2016-7-11.
 */
@Controller
@RequestMapping("/appProductReceive")
public class AppProductReceiveController {

    @Autowired
    private ProductReceiveService productReceiveService;

    /**
     * 回款信息
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppReceiveInfo", method = RequestMethod.POST)
    @ResponseBody
    public void getAppReceiveInfo(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String receiveFlag = request.getParameter("receiveFlag"); //回款状态(0-待收、1-已收)
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String receiveStr = ""; //回款情况拼接
        String receiveDetailStr = ""; //回款情况详情拼接

        //回收情况统计
        AppProductReceiveTjMessageBean appProductReceiveTjMessageBean = productReceiveService.getAppReceiveInfo(memberId,receiveFlag);
        //回收情况列表
        List<AppProductReceiveMessageBean> listAppProductReceiveMes = productReceiveService.getAppReceiveList(memberId,receiveFlag,pageIndex,pageSize);
        if (receiveFlag != null && !receiveFlag.equals("")){
            if (receiveFlag.equals("0")){ //待收
                receiveStr = receiveStr + "<tr>"
                           + "<td colspan=\"2\" style=\"height:120px;padding-top:50px;\">"
                           + "<span>待收总额(元)</span>"
                           + "<span>"+appProductReceiveTjMessageBean.getDsPrice()+"元</span>"
                           + "</td>"
                           + "</tr>"
                           + "<tr>"
                           + "<td>"
                           + "<span>待收投资(笔)</span>"
                           + "<span>"+appProductReceiveTjMessageBean.getDsInvestCount()+"笔</span>"
                           + "</td>"
                           + "<td>"
                           + "<span>待收期数(期)</span>"
                           + "<span>"+appProductReceiveTjMessageBean.getDsPeriod()+"期</span>"
                           + "</td>"
                           + "</tr>";

                if (listAppProductReceiveMes != null && listAppProductReceiveMes.size()>0){
                    for (AppProductReceiveMessageBean appProductReceiveMessageBean : listAppProductReceiveMes){
                        receiveDetailStr = receiveDetailStr + "<tr onclick=\"doSearchDetail('"+appProductReceiveMessageBean.getId()+"')\">"
                                         + "<td>"
                                         + "名称<span>"+appProductReceiveMessageBean.getTitle()+"</span>"
                                         + "</td>"
                                         + "<td>"
                                         + "<div style=\"border-right:1px solid #EBEBEB;border-left:1px solid #EBEBEB;line-height:30px;\">"
                                         + "<ul>"
                                         + "<li class=\"schemes_d\">待收总额</li>"
                                         + "<li class=\"schemes_x\">"+appProductReceiveMessageBean.getYreceivePrice()+"元</li>"
                                         + "</ul>"
                                         + "</div>"
                                         + "</td>"
                                         + "<td>"
                                         + "待收日期 <span>"+appProductReceiveMessageBean.getYreceiveTime()+"</span>"
                                         + "</td>"
                                         + "<td id=\"schemes_details\">"
                                         + "<span class=\"icon-app-05\"></span>"
                                         + "</td>"
                                         + "</tr>";
                    }
                }
            }else if (receiveFlag.equals("1")){
                receiveStr = receiveStr + "<tr>"
                        + "<td colspan=\"2\" style=\"height:120px;padding-top:50px;\">"
                        + "<span>已收总额(元)</span>"
                        + "<span>"+appProductReceiveTjMessageBean.getYsPrice()+"元</span>"
                        + "</td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td>"
                        + "<span>已收投资(笔)</span>"
                        + "<span>"+appProductReceiveTjMessageBean.getYsInvestCount()+"笔</span>"
                        + "</td>"
                        + "<td>"
                        + "<span>已收期数(期)</span>"
                        + "<span>"+appProductReceiveTjMessageBean.getYsPeriod()+"期</span>"
                        + "</td>"
                        + "</tr>";

                if (listAppProductReceiveMes != null && listAppProductReceiveMes.size()>0){
                    for (AppProductReceiveMessageBean appProductReceiveMessageBean : listAppProductReceiveMes){
                        receiveDetailStr = receiveDetailStr + "<tr onclick=\"doSearchDetail('"+appProductReceiveMessageBean.getId()+"')\">"
                                + "<td>"
                                + "名称<span>"+appProductReceiveMessageBean.getTitle()+"</span>"
                                + "</td>"
                                + "<td>"
                                + "<div style=\"border-right:1px solid #EBEBEB;border-left:1px solid #EBEBEB;line-height:30px;\">"
                                + "<ul>"
                                + "<li class=\"schemes_d\">实收金额</li>"
                                + "<li class=\"schemes_x\">"+appProductReceiveMessageBean.getReceivePrice()+"元</li>"
                                + "</ul>"
                                + "</div>"
                                + "</td>"
                                + "<td>"
                                + "实收日期 <span>"+appProductReceiveMessageBean.getSreceiveTime()+"</span>"
                                + "</td>"
                                + "<td id=\"schemes_details\">"
                                + "<span class=\"icon-app-05\"></span>"
                                + "</td>"
                                + "</tr>";
                    }
                }
            }
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("receiveStr",receiveStr); //回款情况拼接
        in.put("receiveDetailStr",receiveDetailStr); //回款详情部分拼接
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 回款列表
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppReceiveList", method = RequestMethod.POST)
    @ResponseBody
    public void getAppReceiveList(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String receiveFlag = request.getParameter("receiveFlag"); //回款状态(0-待收、1-已收)
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String receiveDetailStr = ""; //回款情况详情拼接

        //回收情况列表
        List<AppProductReceiveMessageBean> listAppProductReceiveMes = productReceiveService.getAppReceiveList(memberId,receiveFlag,pageIndex,pageSize);
        if (receiveFlag != null && !receiveFlag.equals("")){
            if (receiveFlag.equals("0")){ //待收
                if (listAppProductReceiveMes != null && listAppProductReceiveMes.size()>0){
                    for (AppProductReceiveMessageBean appProductReceiveMessageBean : listAppProductReceiveMes){
                        receiveDetailStr = receiveDetailStr + "<tr onclick=\"doSearchDetail('"+appProductReceiveMessageBean.getId()+"')\">"
                                + "<td>"
                                + "名称<span>"+appProductReceiveMessageBean.getTitle()+"</span>"
                                + "</td>"
                                + "<td>"
                                + "<div style=\"border-right:1px solid #EBEBEB;border-left:1px solid #EBEBEB;line-height:30px;\">"
                                + "<ul>"
                                + "<li class=\"schemes_d\">待收总额</li>"
                                + "<li class=\"schemes_x\">"+appProductReceiveMessageBean.getYreceivePrice()+"元</li>"
                                + "</ul>"
                                + "</div>"
                                + "</td>"
                                + "<td>"
                                + "待收日期 <span>"+appProductReceiveMessageBean.getYreceiveTime()+"</span>"
                                + "</td>"
                                + "<td id=\"schemes_details\">"
                                + "<span class=\"icon-app-05\"></span>"
                                + "</td>"
                                + "</tr>";
                    }
                }
            }else if (receiveFlag.equals("1")){ //已收
                if (listAppProductReceiveMes != null && listAppProductReceiveMes.size()>0){
                    for (AppProductReceiveMessageBean appProductReceiveMessageBean : listAppProductReceiveMes){
                        receiveDetailStr = receiveDetailStr + "<tr onclick=\"doSearchDetail('"+appProductReceiveMessageBean.getId()+"')\">"
                                + "<td>"
                                + "名称<span>"+appProductReceiveMessageBean.getTitle()+"</span>"
                                + "</td>"
                                + "<td>"
                                + "<div style=\"border-right:1px solid #EBEBEB;border-left:1px solid #EBEBEB;line-height:30px;\">"
                                + "<ul>"
                                + "<li class=\"schemes_d\">实收总额</li>"
                                + "<li class=\"schemes_x\">"+appProductReceiveMessageBean.getReceivePrice()+"元</li>"
                                + "</ul>"
                                + "</div>"
                                + "</td>"
                                + "<td>"
                                + "实收日期 <span>"+appProductReceiveMessageBean.getYreceiveTime()+"</span>"
                                + "</td>"
                                + "<td id=\"schemes_details\">"
                                + "<span class=\"icon-app-05\"></span>"
                                + "</td>"
                                + "</tr>";
                    }
                }
            }
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("receiveDetailStr",receiveDetailStr); //回款详情部分拼接
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 根据id获取更多详细信息
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppReceiveDetailMore", method = RequestMethod.POST)
    @ResponseBody
    public void getAppReceiveDetailInfo(HttpServletRequest request,HttpServletResponse response){
        String receiveId = request.getParameter("receiveId"); //回款id
        String receiveFlag = request.getParameter("receiveFlag"); //回款状态(0-待收、1-已收)
        String receiveMoreStr = ""; //更多详细信息

        try{

            //回款信息
            AppProductReceiveMessageBean appProductReceiveMessageBean = productReceiveService.getAppReceiveInfoById(receiveId);
            if (receiveFlag != null && !receiveFlag.equals("")){
                if (receiveFlag.equals("0")){ //待收
                    if (appProductReceiveMessageBean != null){
                        receiveMoreStr = receiveMoreStr + "<ul>"
                                       + "<li>"
                                       + "<span>"
                                       + "<img src=\"../img/hkjh_03.png\" style=\"width:30px;height:30px;\"/>"
                                       + "</span>"
                                       + "<span class=\"schem_title\">&nbsp;&nbsp;"+appProductReceiveMessageBean.getTitle()+"</span>"
                                       + "</li>"
                                       + "</ul>"
                                       + "<ul>"
                                       + "<table class=\"schems_table\">"
                                       + "<tr>"
                                       + "<td>"
                                       + "<img src=\"../img/hkjh_06.png\" style=\"width:25px;height:25px;\" />"
                                       + "</td>"
                                       + "<td>编号：<span>"+appProductReceiveMessageBean.getCode()+"</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<img src=\"../img/hkjh_08.png\" style=\"width:25px;height:25px;\" />"
                                       + "</td>"
                                       + "<td>待收日期：<span>"+appProductReceiveMessageBean.getYreceiveTime()+"</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<img src=\"../img/hkjh_10.png\" style=\"width:25px;height:25px;\" />"
                                       + "</td>"
                                       + "<td>待收本金<b style=\"color:#C7C7C7;\">(元)</b>：<span>"+appProductReceiveMessageBean.getPrincipal()+"元</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<img src=\"../img/hkjh_12.png\" style=\"width:25px;height:25px;\" />"
                                       + "</td>"
                                       + "<td>待收利息<b style=\"color:#C7C7C7;\">(元)</b>：<span>"+appProductReceiveMessageBean.getInterest()+"元</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<img src=\"../img/hkjh_14.png\" style=\"width:25px;height:25px;\" />"
                                       + "</td>"
                                       + "<td>待收总额<b style=\"color:#C7C7C7;\">(元)</b>：<span>"+appProductReceiveMessageBean.getYreceivePrice()+"元</span></td>"
                                       + "</tr>"
                                       + "</table>"
                                       + "</ul>";
                    }
                }else if (receiveFlag.equals("1")){ //已收
                    if (appProductReceiveMessageBean != null){
                        receiveMoreStr = receiveMoreStr + "<ul>"
                                       + "<li>"
                                       + "<span>"
                                       + "<img src=\"../img/hkjh_03.png\" style=\"width:30px;height:30px;\"/>"
                                       + "</span>"
                                       + "<span class=\"schem_title\">&nbsp;&nbsp;"+appProductReceiveMessageBean.getTitle()+"</span>"
                                       + "</li>"
                                       + "</ul>"
                                       + "<ul>"
                                       + "<table class=\"schems_table\">"
                                       + "<tr>"
                                       + "<td>"
                                       + "<img src=\"../img/hkjh_06.png\" style=\"width:25px;height:25px;\" />"
                                       + "</td>"
                                       + "<td>编号：<span>"+appProductReceiveMessageBean.getCode()+"</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<img src=\"../img/hkjh_08.png\" style=\"width:25px;height:25px;\" />"
                                       + "</td>"
                                       + "<td>实收日期：<span>"+appProductReceiveMessageBean.getSreceiveTime()+"</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<img src=\"../img/hkjh_14.png\" style=\"width:25px;height:25px;\" />"
                                       + "</td>"
                                       + "<td>实收金额<b style=\"color:#C7C7C7;\">(元)</b>：<span>"+appProductReceiveMessageBean.getReceivePrice()+"元</span></td>"
                                       + "</tr>"
                                       + "</table>"
                                       + "</ul>";
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("receiveMoreStr",receiveMoreStr); //回款详情部分拼接
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
