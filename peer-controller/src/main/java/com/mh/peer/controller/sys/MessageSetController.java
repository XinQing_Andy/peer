package com.mh.peer.controller.sys;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.MessageSetMessageBean;
import com.mh.peer.service.sys.MessageSetService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;

/**
 * Created by cuibin on 2016/5/30.
 */
@Controller
@RequestMapping("/messageSet")
public class MessageSetController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private MessageSetService messageSetService;

    /**
     * 打开短信设置
     * @param messageSetMessageBean
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openMessageSet", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openMessageSet(@ModelAttribute MessageSetMessageBean messageSetMessageBean,ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        MessageSetMessageBean messageSetMessageBean1 = messageSetService.queryOnlyMessage();
        modelMap.put("messageSetMessageBean",messageSetMessageBean1);
        try {
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/sys/system_message_set.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }

    /**
     * 更新短信设置
     * @param messageSetMessageBean
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/updateMessageSet", method = RequestMethod.POST)
    @ResponseBody
    public MessageSetMessageBean updateMessageSet(@ModelAttribute MessageSetMessageBean messageSetMessageBean,ModelMap modelMap){

        return messageSetService.updateMessageSet(messageSetMessageBean);
    }
}
