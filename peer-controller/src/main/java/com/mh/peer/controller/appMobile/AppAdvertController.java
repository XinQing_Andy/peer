package com.mh.peer.controller.appMobile;

import com.mh.peer.model.message.AppAdvertMessageBean;
import com.mh.peer.model.message.AppSysFileMessageBean;
import com.mh.peer.service.platform.AdvertService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.PublicAddress;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhangerxin on 2016-7-8.
 */
@Controller
@RequestMapping("/appAdvert")
public class AppAdvertController {

    @Autowired
    private AdvertService advertService;
    @Autowired
    private SysFileService sysFileService;

    /**
     * 获取商城列表(App端)
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAdvert", method = RequestMethod.POST)
    @ResponseBody
    public void getAdvert(HttpServletRequest request,HttpServletResponse response){
        String bannerZero = "";
        String bannerOne = ""; //第一章背景图
        String bannerTwo = ""; //第二章背景图
        String bannerThree = ""; //第三章背景图
        String bannerFour = "";
        String advertPicStr = ""; //广告图片地址
        String serverAddress = ""; //地址
        String advertId = ""; //广告id
        String picUrl = ""; //图片地址
        int num = 0; //轮播图数量

        PublicAddress publicAddress = new PublicAddress();
        serverAddress = publicAddress.getServerAddress(); //服务器地址

        /************************* 轮播图部分Start *************************/
        List<AppAdvertMessageBean> listAppAdvertBannerMes = advertService.getAdvert("2","1","3","SORT","ASC");
        if (listAppAdvertBannerMes != null && listAppAdvertBannerMes.size() > 0){
            for (AppAdvertMessageBean appAdvertMessageBean : listAppAdvertBannerMes){
                advertId = appAdvertMessageBean.getId(); //广告id
                AppSysFileMessageBean appSysFileMessageBean = sysFileService.getAppSysFile(advertId,"CREATETIME","DESC","1","1");
                picUrl = ""; //图片地址
                if (appSysFileMessageBean != null){
                    picUrl = serverAddress + appSysFileMessageBean.getImgUrl(); //图片地址
                }
                if (num == 0){ //第一张轮播图
                    bannerOne = "<a href=\"JavaScript:void(0)\" onclick=\"doSearchAdvertDetail('"+appAdvertMessageBean.getId()+"')\">"
                              + "<img src=\""+picUrl+"\" class=\"img_shop\">"
                              + "</a>";
                }
                if (num == 1){
                    bannerTwo = "<a href=\"JavaScript:void(0)\" onclick=\"doSearchAdvertDetail('"+appAdvertMessageBean.getId()+"')\">"
                              + "<img src=\""+picUrl+"\" class=\"img_shop\">"
                              + "</a>";
                }
                if (num == 2){
                    bannerThree = "<a href=\"JavaScript:void(0)\" onclick=\"doSearchAdvertDetail('"+appAdvertMessageBean.getId()+"')\">"
                                + "<img src=\""+picUrl+"\" class=\"img_shop\">"
                                + "</a>";
                }
                num++;
            }
            if (num == 3){ //3张图
                bannerZero = bannerThree;
                bannerFour = bannerOne;
            }
        }
        /************************** 轮播图部分End **************************/


        /************************* 广告图片地址Start *************************/
        num = 0; //用于统计图片数量
        List<AppAdvertMessageBean> listAdvertMes = advertService.getAdvert("1","1","3","SORT","ASC");
        if (listAdvertMes != null && listAdvertMes.size() > 0){
            for (AppAdvertMessageBean appAdvertMessageBean : listAdvertMes){
                advertId = appAdvertMessageBean.getId(); //广告id
                AppSysFileMessageBean appSysFileMessageBean = sysFileService.getAppSysFile(advertId,"CREATETIME","DESC","1","1");
                picUrl = ""; //图片地址
                if (appSysFileMessageBean != null){
                    picUrl = serverAddress + appSysFileMessageBean.getImgUrl(); //图片地址
                }
                if (num%2 == 0){
                    advertPicStr = advertPicStr + "<tr>";
                }
                advertPicStr = advertPicStr + "<td onclick=\"doSearchAdvertDetail('"+appAdvertMessageBean.getId()+"')\">"
                             + "<dl>"
                             + "<dt>"
                             + "<img src=\""+picUrl+"\"/>"
                             + "</dt>"
                             + "<dd>"
                             + "<p class=\"mall_tm\">"+appAdvertMessageBean.getTitle()+"</p>"
                             + "<p class=\"mall_date\">"+appAdvertMessageBean.getCreateTime()+"</p>"
                             + "<p class=\"mall_wei\">"+appAdvertMessageBean.getContent()+"</p>"
                             + "</dd>"
                             + "</dl>"
                             + "</td>";
                if (num%2 == 1){
                    advertPicStr = advertPicStr + "</tr>";
                }
                num++;
            }
            if (num%2 == 0){
                advertPicStr = advertPicStr + "</tr>";
            }
        }
        /************************** 广告图片地址End **************************/

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("bannerZero",bannerZero);
        in.put("bannerOne",bannerOne); //第一张轮播图部分拼接
        in.put("bannerTwo",bannerTwo); //第二张轮播图部分拼接
        in.put("bannerThree",bannerThree); //第三章轮播图部分拼接
        in.put("bannerFour",bannerFour);
        in.put("advertPicStr", advertPicStr); //广告图片地址
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取广告详情
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAdvertDetail", method = RequestMethod.POST)
    @ResponseBody
    public void getAdvertDetail(HttpServletRequest request,HttpServletResponse response){
        String advertId = request.getParameter("advertId"); //广告id
        String serverAddress = ""; //地址
        PublicAddress publicAddress = new PublicAddress();
        serverAddress = publicAddress.getServerAddress(); //服务器地址
        String picAddress = ""; //图片地址
        String advertDetailStr = ""; //广告详情拼接

        AppAdvertMessageBean appAdvertMessageBean = advertService.getAdvertDetail(advertId);
        AppSysFileMessageBean appSysFileMessageBean = sysFileService.getAppSysFile(advertId,"CREATETIME","DESC","1","1");
        if (appSysFileMessageBean != null){
            picAddress = serverAddress + appSysFileMessageBean.getImgUrl(); //图片地址
        }

        if (appAdvertMessageBean != null){
            advertDetailStr = advertDetailStr + "<div class=\"store_img\">"
                            + "<img src=\""+picAddress+"\" style=\"height:200px\"/>"
                            + "</div>"
                            + "<div class=\"store_show\">"
                            + "<ul>"
                            + "<li class=\"stroe_title\">"+appAdvertMessageBean.getTitle()+"</li>"
                            + "<li class=\"stroe_date\">"+appAdvertMessageBean.getCreateTime()+"</li>"
                            + "<li class=\"stroe_txt\">"+appAdvertMessageBean.getContent()+"</li>"
                            + "</ul>"
                            + "</div>";
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("advertDetailStr",advertDetailStr); //广告详细信息拼接
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
