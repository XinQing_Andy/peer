package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.front.SecuritySettingsService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.sys.SysSecurityQuestionService;
import com.mh.peer.service.sys.SystemEmailService;
import com.mh.peer.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Cuibin on 2016/5/2.
 */
@Controller
@RequestMapping("/security")
public class SecuritySettingsController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private SecuritySettingsService securitySettingsService;
    @Autowired
    private SystemEmailService systemEmailService;
    @Autowired
    private SysSecurityQuestionService sysSecurityQuestionService;
    @Autowired
    private AllMemberService allMemberService;

    /**
     * 跳转安全设置
     */
    @RequestMapping(value = "/goSecuritySettings", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goSecuritySettings(@ModelAttribute ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        //查询用户
        MemberInfoMessageBean mimb = new MemberInfoMessageBean();
        mimb.setId(memberInfo.getId());
        MemberInfoMessageBean mimb2 = allMemberService.queryOnlyMember(mimb);
        //查询紧急联系人
        MemberLinkmanMessageBean memberLinkmanMessageBean = securitySettingsService.queryOnlyContact(memberInfo.getId());
        String isMemberLinkman = memberLinkmanMessageBean.getId() == null ? "1" : "0";
        //查询是否设置安全保护问题
        String isQuestion = securitySettingsService.queryIsQuestion(memberInfo.getId()) == true ? "0" : "1";
        /******************** 安全等级 start ********************/
        int amount = 0;
        MemberInfo sessionMemberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if(sessionMemberInfo.getRealFlg().equals("0")){
            amount++;
        }
        if(sessionMemberInfo.getTraderpassword() != null){
            amount++;
        }
        if(sessionMemberInfo.getTelephoneFlg().equals("0")){
            amount++;
        }
        if(sessionMemberInfo.getMailFlg().equals("0")){
            amount++;
        }
        if(securitySettingsService.queryOnlyContact(sessionMemberInfo.getId()).getId() != null){
            amount++;
        }
        if(securitySettingsService.queryIsQuestion(sessionMemberInfo.getId())){
            amount++;
        }
        modelMap.addAttribute("amount", amount);
        /******************** 安全等级 end ********************/
        modelMap.put("memberInfoMessageBean",mimb2);
        modelMap.put("isMemberLinkman",isMemberLinkman);
        modelMap.put("isQuestion",isQuestion);
        modelMap.put("logintime",memberInfo.getLogintime());
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 跳转到修改密码
     */
    @RequestMapping(value = "/goPassWordUpdate", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goPassWordUpdate(@ModelAttribute ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_update.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 更新密码
     */
    @RequestMapping(value = "/updatePassWord", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean updatePassWord(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        MemberInfoMessageBean mimb = null;
        if (memberInfo == null || memberInfo.equals("")) {
            mimb = new MemberInfoMessageBean();
            mimb.setInfo("会话已过期请重新登陆");
            mimb.setResult("error");
            return mimb;
        }
        mimb = securitySettingsService.checkOldPassword(memberInfoMessageBean.getOldPassWord());
        if (mimb.getResult().equals("success")) {
            return securitySettingsService.updatePassWord(memberInfoMessageBean.getPassword());
        }
        return mimb;
    }

    /**
     * ======================================================================================
     * 跳转到设置紧急联系人
     */
    @RequestMapping(value = "/goContact", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goContact(@ModelAttribute ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_contact.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 保存紧急联系人
     */
    @RequestMapping(value = "/saveContact", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean saveContact(@ModelAttribute MemberLinkmanMessageBean memberLinkmanMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        MemberLinkmanMessageBean mlmb = securitySettingsService.saveContact(memberLinkmanMessageBean);
        if (mlmb.getResult().equals("error")) {
            bean.setInfo(mlmb.getInfo());
            bean.setResult(mlmb.getResult());
            return bean;
        }
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_contactsuccess.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }
/**
 * ===========================================================
 */
    /**
     * 跳转邮箱设置
     */
    @RequestMapping(value = "/goEmailSet", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goEmailSet(@ModelAttribute ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_mail.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 跳转到去邮箱验证
     */
    @RequestMapping(value = "/goEmailCheck", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goEmailCheck(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        JetbrickBean jetbrickBean = new JetbrickBean();
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            jetbrickBean.setResult("error");
            jetbrickBean.setInfo("请重新登陆");
            return jetbrickBean;
        }
        MemberInfoMessageBean memberInfoMessageBean1 = securitySettingsService.checkEmailIsSuccess(memberInfoMessageBean.getEmail());
        if (memberInfoMessageBean1.getResult().equals("success")) {
            jetbrickBean.setResult("error");
            jetbrickBean.setInfo("此邮箱已被认证");
            return jetbrickBean;
        }
        String userId = memberInfo.getId(); //用户id

        String result = "0"; //0-发送成功，1-发送失败

        String email = memberInfoMessageBean.getEmail(); //用户填写的邮箱
        String mailFlg = "1"; //是否被验证
        MemberInfoMessageBean resultM = securitySettingsService.saveEmailMessage(mailFlg, memberInfoMessageBean.getEmail());//保存验证标识和邮箱

        if (resultM.getResult().equals("error")) {
            jetbrickBean.setResult("error");
            jetbrickBean.setInfo("发送邮件失败,请稍后重试");
            return jetbrickBean;
        }

        SysEmailSettingMessageBean sysesm = systemEmailService.querySystemEmail(); //邮箱通道
        String title = "";
        String body = "";

        PublicAddress publicAddress = new PublicAddress();//获取服务器地址
        String address = publicAddress.getEmailAddress();

        try {
            title = "煎饼金融邮箱认证";
            body = "亲爱的用户,您正在进行邮箱认证 <br/><a href='" + address + "security/finishEmailCheck?id=" + userId + "' style='color:blue'>请点击此链接完成认证</a>" +
                    "<br/>此邮件由系统发出,如有疑问可直接联系我们";
            SendEmail sendEmail = new SendEmail();
            sendEmail.send(sysesm.getSmtp(), sysesm.getEmail(), sysesm.getPassword(), email, title, body);
        } catch (Exception e) {
            e.printStackTrace();
            result = "1";
        }

        if (result.equals("1")) {
            jetbrickBean.setResult("error");
            jetbrickBean.setInfo("发送邮件失败,请稍后重试");
            return jetbrickBean;
        }
        modelMap.put("email", email);
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_mail_approve_go.jetx", modelMap);
            jetbrickBean.setHtmlText(stringWriter.toString());
            jetbrickBean.setInfo("成功");
            jetbrickBean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            jetbrickBean.setInfo("失败");
            jetbrickBean.setResult("error");
            jetbrickBean.setHtmlText("");
        }

        return jetbrickBean;

    }

    /**
     * 完成邮箱验证
     */
    @RequestMapping(value = "/finishEmailCheck", method = RequestMethod.GET)
    public String finishEmailCheck(HttpServletRequest request, ModelMap modelMap) {
        String id = request.getParameter("id");
        String message = "1";
        if (id == null || id.equals("")) {
            return "front/safeSet/safe_set_mailError";
        }
        MemberInfoMessageBean myMimb = securitySettingsService.checkEmailMessage(message, id);
        MemberInfoMessageBean myMimb2 = null;
        if (myMimb.getResult().equals("success")) {
            myMimb2 = securitySettingsService.updateEmailMessage(id);
            if (myMimb2.getResult().equals("success")) {
                return "front/safeSet/safe_set_mailSuccess";
            }
        }

        return "front/safeSet/safe_set_mailError";
    }

    /**
     * 跳转到邮箱验证成功页面
     */
    @RequestMapping(value = "/goEmailSetSuccess", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goEmailSetSuccess(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();

        MemberInfoMessageBean memberInfoMessageBean1 = securitySettingsService.checkEmailIsSuccess(memberInfoMessageBean.getEmail());
        if (memberInfoMessageBean1.getResult().equals("error")) {
            bean.setInfo("请先去邮箱验证");
            bean.setResult("error");
            return bean;
        }
        modelMap.put("email", memberInfoMessageBean.getEmail());
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_mail_approve_success.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }
    /**
     * ===========================================================
     */
    /**
     * 保护问题 发送验证码
     */
    @RequestMapping(value = "/setQuestionGetCode", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean setQuestionGetCode(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) throws IOException {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");

        MemberInfoMessageBean result = new MemberInfoMessageBean();
        if (memberInfo == null) {
            result.setInfo("会话已过期 请重新登录");
            result.setResult("error");
            return result;
        }

        HttpSession session = servletUtil.getSession();
        String setQuestionCodeMessage = (String) session.getAttribute("setQuestionCodeMessage"); //获取验证码的时间

        if (setQuestionCodeMessage == null || setQuestionCodeMessage.equals("")) {
            Date a = new Date();
            setQuestionCodeMessage = a.getTime() + "";
            session.setAttribute("setQuestionCodeMessage", setQuestionCodeMessage);
        } else {
            long time = Long.parseLong(setQuestionCodeMessage);
            Date b = new Date();
            long now = b.getTime();
            if ((now - time) / 1000 < 60) {
                result.setInfo("提交过于频繁 请稍后重试");
                result.setResult("error");
                return result;
            }
            session.setAttribute("setQuestionCodeMessage", now + "");
        }

        String telephone = ""; //获取手机号
        memberInfoMessageBean.setId(memberInfo.getId()); //会员id
        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
        if(memberInfoMessageBean != null){
            telephone = memberInfoMessageBean.getTelephone();
        }

        SendMobileMessage sendMobileMessage = new SendMobileMessage();
        PublicsTool publicsTool = new PublicsTool();
        String random = publicsTool.getRandom();
        session.setAttribute("setQuestionCode", random);
        String content = "【煎饼金融P2P】尊敬的用户您好,验证码为:" + random + ",请妥善保管,验证码提供给他人将导致信息泄露";
        String message = sendMobileMessage.send(telephone, content);
        //System.out.println(content);
        if (message.equals("0")) {
            result.setResult("success");
            result.setInfo("验证码已发送");
        } else {
            result.setResult("error");
        }
        return result;
    }


    /**
     * 跳转到保护问题
     */
    @RequestMapping(value = "/goProtect", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goProtect(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        String telephoneFlg = ""; //是否有手机号
        memberInfoMessageBean.setId(memberInfo.getId()); //会员id
        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
        if (memberInfoMessageBean != null) {
            telephoneFlg = memberInfoMessageBean.getTelephoneFlg();
        }

        //模板解析方法
        try {
            StringWriter stringWriter = null;
            //模板渲染
            if (telephoneFlg.equals("1")) {
                stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/please_set_phone.jetx", modelMap);
            } else {
                stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_protect.jetx", modelMap);
            }
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 跳转到填写保护问题
     */
    @RequestMapping(value = "/goSetProtect", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goSetProtect(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        //验证验证码
        String code = memberInfoMessageBean.getCode(); //用户输入验证码
        String setQuestionCode = (String) servletUtil.getSession().getAttribute("setQuestionCode"); //服务器保存验证码

        if(!code.equals(setQuestionCode)){
            bean.setInfo("验证码错误");
            bean.setResult("error");
            return bean;
        }
        //查询所有密保问题
        SysSecurityQuestionMessageBean sysSecurityQuestionMessageBean = new SysSecurityQuestionMessageBean();
        sysSecurityQuestionMessageBean.setPageIndex(null);
        sysSecurityQuestionMessageBean.setPageSize(null);
        sysSecurityQuestionMessageBean.setQuestion(null);
        List<SysSecurityQuestionMessageBean> allquestionList = sysSecurityQuestionService.querySecurityQuestionByPage(sysSecurityQuestionMessageBean);

        int count = allquestionList.size();
        for (int i = 0; i < count; i++) {
            String temp = allquestionList.get(i).getActive_flg();
            if(temp.equals("1"))
                allquestionList.remove(i);
        }
        int newCount = allquestionList.size();
        int index = 0;

        if(newCount % 3 != 0){
            index = newCount / 3 + 1;
        }else{
            index = newCount / 3;
        }
        List questionListOne = new ArrayList();
        List questionListTwo = new ArrayList();
        List questionListThree = new ArrayList();
        int i;
        int j;
        for (i = 0; i < index; i++) {
            questionListOne.add(allquestionList.get(i));
        }
        for (j = i; j < index * 2; j++) {
            questionListTwo.add(allquestionList.get(j));
        }
        for (int k = j; k < allquestionList.size(); k++) {
            questionListThree.add(allquestionList.get(k));
        }
        modelMap.put("questionListOne", questionListOne);
        modelMap.put("questionListTwo", questionListTwo);
        modelMap.put("questionListThree", questionListThree);
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_protect_select.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 跳转到确认问题
     */
    @RequestMapping(value = "/goConfirmProtect", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goConfirmProtect(@ModelAttribute MemberSetupQuestionsMessageBean msqm, ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        //查询问题
        SysSecurityQuestionMessageBean sqm1 = securitySettingsService.queryOnlyquestion(Integer.parseInt(msqm.getQuestionone()));
        SysSecurityQuestionMessageBean sqm2 = securitySettingsService.queryOnlyquestion(Integer.parseInt(msqm.getQuestiontwo()));
        SysSecurityQuestionMessageBean sqm3 = securitySettingsService.queryOnlyquestion(Integer.parseInt(msqm.getQuestionthree()));

        modelMap.put("questionone", sqm1.getQuestion());
        modelMap.put("questiontwo", sqm2.getQuestion());
        modelMap.put("questionthree", sqm3.getQuestion());

        modelMap.put("questiononeId", sqm1.getId());
        modelMap.put("questiontwoId", sqm2.getId());
        modelMap.put("questionthreeId", sqm3.getId());

        modelMap.put("answerone", msqm.getAnswerone());
        modelMap.put("answertwo", msqm.getAnswertwo());
        modelMap.put("answerthree", msqm.getAnswerthree());

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_protect_affirm.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 保存问题
     */
    @RequestMapping(value = "/saveQuestion", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean saveQuestion(@ModelAttribute MemberSetupQuestionsMessageBean msqm, ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        //保存问题
        MemberSetupQuestionsMessageBean myMsmb = securitySettingsService.saveQuestion(msqm);
        if (myMsmb.getResult().equals("error")) {
            bean.setInfo(myMsmb.getInfo());
            bean.setResult("error");
            return bean;
        }
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/safe_set_protect_success.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }
    /**
     * ===============================================================================================================
     */
    /**
     * 跳转交易密码设置
     */
    @RequestMapping(value = "/goSetTransactionPwd", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goSetTransactionPwd(ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        //是否设置手机号
        MemberInfoMessageBean memberInfoMes = new MemberInfoMessageBean();
        String telephoneFlg = null;
        memberInfoMes.setId(memberInfo.getId()); //会员id
        memberInfoMes = allMemberService.queryOnlyMember(memberInfoMes);
        if (memberInfoMes != null) {
            telephoneFlg = memberInfoMes.getTelephoneFlg();
        }
        //模板解析方法
        try {
            StringWriter stringWriter = null;
            //模板渲染
            if (telephoneFlg.equals("1")) {
                stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/please_set_phone.jetx", modelMap);
            } else {
                stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/set_transaction_pwd.jetx", modelMap);
            }

            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 保存交易密码
     */
    @RequestMapping(value = "/saveTransactionPwd", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean saveTransactionPwd(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        String code = memberInfoMessageBean.getCode(); //用户验证码
        String seesionCode = (String) servletUtil.getSession().getAttribute("transactionCheckCode");//服务器验证码
        if (!code.equals(seesionCode)) {
            bean.setInfo("验证码有误");
            bean.setResult("error");
            return bean;
        }
        //保存交易密码
        MemberInfoMessageBean mimb = securitySettingsService.saveTransactionPwd(memberInfoMessageBean);
        if (mimb.getResult().equals("error")) {
            bean.setInfo(mimb.getInfo());
            bean.setResult("error");
            return bean;
        }
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/set_transaction_pwd_success.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 发送验证码
     */
    @RequestMapping(value = "/sendMessageCheck", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean sendMessageCheck(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) throws IOException {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        MemberInfoMessageBean result = new MemberInfoMessageBean();
        HttpSession session = servletUtil.getSession();
        String traCheckCodeMessage = (String) session.getAttribute("traCheckCodeMessage");

        if (traCheckCodeMessage == null || traCheckCodeMessage.equals("")) {
            Date a = new Date();
            traCheckCodeMessage = a.getTime() + "";
            session.setAttribute("traCheckCodeMessage", traCheckCodeMessage);
        } else {
            long time = Long.parseLong(traCheckCodeMessage);
            Date b = new Date();
            long now = b.getTime();
            if ((now - time) / 1000 < 60) {
                result.setInfo("提交过于频繁 请稍后重试");
                result.setResult("error");
                return result;
            }
            session.setAttribute("traCheckCodeMessage", now + "");
        }
        String telephone = ""; //获取手机号
        memberInfoMessageBean.setId(memberInfo.getId()); //会员id
        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
        if(memberInfoMessageBean != null){
            telephone = memberInfoMessageBean.getTelephone();
        }
        SendMobileMessage sendMobileMessage = new SendMobileMessage();
        PublicsTool publicsTool = new PublicsTool();
        String random = publicsTool.getRandom();
        session.setAttribute("transactionCheckCode", random);
        String content = "【煎饼金融P2P】尊敬的用户您好,验证码为:" + random + ",请妥善保管,验证码提供给他人将导致信息泄露";
        String message = sendMobileMessage.send(telephone, content);
        //System.out.println(random);
        if (message.equals("0")) {
            result.setResult("success");
            result.setInfo("验证码已发送，有效期为10分钟，请及时查收。");
        } else {
            result.setResult("error");
        }
        return result;
    }


    /**
     * 跳转到环迅注册
     */
    @RequestMapping(value = "/openRealName", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openRealName(ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        String merBillNo = "181006"; //商户号
        JetbrickBean bean = new JetbrickBean();
        HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }else{
            memberId = memberInfo.getId(); //会员id
            MemberInfoMessageBean memberInfoMessageBean = allMemberService.queryMemberInfoById(memberId);

            merBillNo = merBillNo + UUID.randomUUID().toString().replaceAll("-",""); //商户订单号

            if (memberInfoMessageBean != null){
                huanXunRegistMessageBean.setMerBillNo(merBillNo); //商户订单号
                huanXunRegistMessageBean.setUserType("1"); //用户类型(1-个人、2-企业)
                huanXunRegistMessageBean.setUserName(memberInfoMessageBean.getUsername()); //用户名
                huanXunRegistMessageBean.setMobileNo(memberInfoMessageBean.getTelephone()); //手机号
                huanXunRegistMessageBean.setIdentNo(memberInfoMessageBean.getIdcard()); //身份证号
                huanXunRegistMessageBean.setBizType("1"); //业务类型(1-P2P、2-众筹)
                huanXunRegistMessageBean.setRealName(memberInfoMessageBean.getRealname()); //真实姓名
                huanXunRegistMessageBean.setEnterName(""); //企业名称
                huanXunRegistMessageBean.setOrgCode(""); //营业执照编码
                huanXunRegistMessageBean.setIsAssureCom(""); //是否为担保企业
                huanXunRegistMessageBean.setWebUrl("http://jianbing88.com/front/regResult?memberId="+memberId); //页面返回地址
                huanXunRegistMessageBean.setS2SUrl("http://jianbing88.com/huanxun/registerhxResult?memberId="+memberId+""); //后台通知地址
            }
            modelMap.put("huanXunRegistMessageBean",huanXunRegistMessageBean); //环迅信息
        }

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/real_name.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 跳转到修改手机认证
     */
    @RequestMapping(value = "/openReplaceTelephone", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openReplaceTelephone(ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        String telephone = memberInfo.getTelephone();
        if (!telephone.equals("") && telephone != null && telephone.length() == 11) {
            telephone = telephone.substring(0, 3) + "****" + telephone.substring(7, 11);
        }
        modelMap.put("telephone", telephone);

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/phone_update.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 跳转到短信修改手机号
     */
    @RequestMapping(value = "/openReplaceTelephoneNote", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openReplaceTelephoneNote(ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        //查询交易密码
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        memberInfoMessageBean.setId(memberInfo.getId());
        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
        String traderpassword = memberInfoMessageBean.getTraderpassword();//交易密码

        if (!traderpassword.equals("") && traderpassword != null) {
            String telephone = memberInfo.getTelephone(); //页面显示的手机号码
            if (!telephone.equals("") && telephone != null && telephone.length() == 11) {
                telephone = telephone.substring(0, 3) + "****" + telephone.substring(7, 11);
            }
            modelMap.put("telephone", telephone);
        }


        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = null;
            if (!traderpassword.equals("") && traderpassword != null) {
                stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/phone_update_note.jetx", modelMap);
            } else {
                stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/please_set_traderpassword.jetx", modelMap);
            }
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 修改手机认证:获取手机验证码
     */
    @RequestMapping(value = "/replaceGetCode", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean replaceGetCode(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) throws IOException {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        MemberInfoMessageBean result = new MemberInfoMessageBean();
        HttpSession session = servletUtil.getSession();
        //提交标记
        String replaceCheckCodeMessage = (String) session.getAttribute("replaceCheckCodeMessage");

        if (replaceCheckCodeMessage == null || replaceCheckCodeMessage.equals("")) {
            Date a = new Date();
            replaceCheckCodeMessage = a.getTime() + "";
            session.setAttribute("replaceCheckCodeMessage", replaceCheckCodeMessage);
        } else {
            long time = Long.parseLong(replaceCheckCodeMessage);
            Date b = new Date();
            long now = b.getTime();
            if ((now - time) / 1000 < 60) {
                result.setInfo("提交过于频繁 请稍后重试");
                result.setResult("error");
                return result;
            }
            session.setAttribute("replaceCheckCodeMessage", now + "");
        }
        String telephone = ""; //获取手机号
        memberInfoMessageBean.setId(memberInfo.getId()); //会员id
        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
        if(memberInfoMessageBean != null){
            telephone = memberInfoMessageBean.getTelephone();
        }

        SendMobileMessage sendMobileMessage = new SendMobileMessage();
        PublicsTool publicsTool = new PublicsTool();
        String random = publicsTool.getRandom();
        session.setAttribute("replaceCheckCode", random);
        String content = "【煎饼金融P2P】尊敬的用户您好,验证码为:" + random + ",请妥善保管,验证码提供给他人将导致信息泄露";
        //System.out.println("=====================" + random);
        String message = sendMobileMessage.send(telephone, content);
        if (message.equals("0")) {
            result.setResult("success");
            result.setInfo("验证码已发送");
        } else {
            result.setResult("error");
        }
        return result;
    }

    /**
     * 验证 交易密码&验证码
     */
    @RequestMapping(value = "/openNewTraderpasswordSet", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openNewTraderpasswordSet(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) throws IOException {
        JetbrickBean bean = new JetbrickBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        String traderpassword = memberInfoMessageBean.getTraderpassword();//交易密码 用户输入
        String code = memberInfoMessageBean.getCode(); //验证码
        String replaceCheckCode = (String) servletUtil.getSession().getAttribute("replaceCheckCode");

        String myTraderpassword = ""; //获取交易密码 数据库
        memberInfoMessageBean.setId(memberInfo.getId()); //会员id
        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
        if (memberInfoMessageBean != null) {
            myTraderpassword = memberInfoMessageBean.getTraderpassword();
        }
        if (!traderpassword.equals(myTraderpassword)) {
            bean.setInfo("交易密码错误");
            bean.setResult("error");
            return bean;
        }
        if (!code.equals(replaceCheckCode)) {
            bean.setInfo("验证码错误");
            bean.setResult("error");
            return bean;
        }
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/phone_update_set.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 设置手机号
     */
    @RequestMapping(value = "/openPhoneSet", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openPhoneSet(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) throws IOException {
        JetbrickBean bean = new JetbrickBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/phone_update_set.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 设置新手机号:获取手机验证码
     */
    @RequestMapping(value = "/setPhoneGetCode", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean setPhoneGetCode(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) throws IOException {
        MemberInfoMessageBean result = new MemberInfoMessageBean();

        HttpSession session = servletUtil.getSession();
        //提交标记
        String newPhoneCheckCodeMessage = (String) session.getAttribute("newPhoneCheckCodeMessage");

        if (newPhoneCheckCodeMessage == null || newPhoneCheckCodeMessage.equals("")) {
            Date a = new Date();
            newPhoneCheckCodeMessage = a.getTime() + "";
            session.setAttribute("newPhoneCheckCodeMessage", newPhoneCheckCodeMessage);
        } else {
            long time = Long.parseLong(newPhoneCheckCodeMessage);
            Date b = new Date();
            long now = b.getTime();
            if ((now - time) / 1000 < 60) {
                result.setInfo("提交过于频繁 请稍后重试");
                result.setResult("error");
                return result;
            }
            session.setAttribute("newPhoneCheckCodeMessage", now + "");
        }

        String telephone = memberInfoMessageBean.getTelephone(); //新手机号
        SendMobileMessage sendMobileMessage = new SendMobileMessage();
        PublicsTool publicsTool = new PublicsTool();
        String random = publicsTool.getRandom();
        session.setAttribute("newPhoneCheckCode", random);
        session.setAttribute("newPhoneTelephone", telephone);
        String content = "【煎饼金融P2P】尊敬的用户您好,验证码为:" + random + ",请妥善保管,验证码提供给他人将导致信息泄露";
        //System.out.println("=====================" + random);
        String message = sendMobileMessage.send(telephone, content);
        if (message.equals("0")) {
            result.setResult("success");
            result.setInfo("验证码已发送");
        } else {
            result.setResult("error");
        }
        return result;
    }


    /**
     * 保存新手机号
     */
    @RequestMapping(value = "/setNewPhone", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean setNewPhone(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        //System.out.println("12312312312312312312");
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        String code = memberInfoMessageBean.getCode(); //用户验证码
        String seesionCode = (String) servletUtil.getSession().getAttribute("newPhoneCheckCode");//服务器验证码
        if (!code.equals(seesionCode)) {
            bean.setInfo("验证码有误");
            bean.setResult("error");
            return bean;
        }

        String newPhoneTelephone = (String) servletUtil.getSession().getAttribute("newPhoneTelephone");//服务器手机号
        String telephone = memberInfoMessageBean.getTelephone(); //用户输入手机号
        if(!newPhoneTelephone.equals(telephone)){
            bean.setInfo("请重新获取验证码");
            bean.setResult("error");
            return bean;
        }


        //保存新手机号
        memberInfoMessageBean.setId(memberInfo.getId());
        MemberInfoMessageBean mimb = securitySettingsService.updateTelephone(memberInfoMessageBean);
        if (mimb.getResult().equals("error")) {
            bean.setInfo(mimb.getInfo());
            bean.setResult("error");
            return bean;
        } else {
            memberInfo.setTelephone(memberInfoMessageBean.getTelephone());
            servletUtil.getSession().setAttribute("sessionMemberInfo", memberInfo);
        }
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/phone_update_success.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 跳转到 修改手机认证 密保问题
     */
    @RequestMapping(value = "/goQuestionUpdatePhone", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goQuestionUpdatePhone(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        boolean b = securitySettingsService.queryIsQuestion(memberInfo.getId()); //是否设置密保问题
        if(b){
            String[] arr = securitySettingsService.getOneQuestion(memberInfo.getId()); //问题和答案
            servletUtil.getSession().setAttribute("UpdatePhoneAnswer", arr[1]);
            modelMap.put("nickname", memberInfo.getNickname());
            modelMap.put("question", arr[0]);
        }

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = null;
            if(b){
                stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/phone_update_question.jetx", modelMap);
            }else{
                stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/please_set_question.jetx", modelMap);
            }

            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 验证密保
     */
    @RequestMapping(value = "/checkQuestion", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean checkQuestion(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        String traderpassword = memberInfoMessageBean.getTraderpassword(); // 交易密码
        String idcard = memberInfoMessageBean.getIdcard(); // 身份证
        String code = memberInfoMessageBean.getCode(); // 密保答案

        String answer = (String)servletUtil.getSession().getAttribute("UpdatePhoneAnswer");
        if(!code.equals(answer)){
            bean.setInfo("密保答案不正确");
            bean.setResult("error");
            return bean;
        }
        if(!idcard.equals(memberInfo.getIdcard())){
            bean.setInfo("身份证号码不正确");
            bean.setResult("error");
            return bean;
        }
        if(!traderpassword.equals(memberInfo.getTraderpassword())){
            bean.setInfo("交易密码不正确");
            bean.setResult("error");
            return bean;
        }

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/safeSet/phone_update_set.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * ===============================================================================================================
     */

    /**
     * =======================================忘记密码========================================================================
     */
    /**
     * 跳转到忘记密码手机号验证
     */

    @RequestMapping(value = "/openforgetPw", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openforgetPw(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/forgetPw/forget_pw_check.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    @RequestMapping(value = "/openforgetPw2", method = RequestMethod.GET)
    public String openforgetPw2() {
        return "front/forgetPw/forget_pw_check";
    }

    /**
     * 获取验证码
     */
    @RequestMapping(value = "/forgetPwGetCode", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean forgetPwGetCode(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean) {
        MemberInfoMessageBean result = new MemberInfoMessageBean();

        HttpSession session = servletUtil.getSession();
        //提交标记
        String forgetPwCheckCodeMessage = (String) session.getAttribute("forgetPwCheckCodeMessage");

        if (forgetPwCheckCodeMessage == null || forgetPwCheckCodeMessage.equals("")) {
            Date a = new Date();
            forgetPwCheckCodeMessage = a.getTime() + "";
            session.setAttribute("forgetPwCheckCodeMessage", forgetPwCheckCodeMessage);
        } else {
            long time = Long.parseLong(forgetPwCheckCodeMessage);
            Date b = new Date();
            long now = b.getTime();
            if ((now - time) / 1000 < 60) {
                result.setInfo("提交过于频繁 请稍后重试");
                result.setResult("error");
                return result;
            }
            session.setAttribute("forgetPwCheckCodeMessage", now + "");
        }

        String telephone = memberInfoMessageBean.getTelephone();
        String id = securitySettingsService.checkIsTelephone(telephone);//验证是否有此手机号
        if (id == null || id.equals("")) {
            result.setInfo("查无此手机号");
            result.setResult("error");
            return result;
        }
        SendMobileMessage sendMobileMessage = new SendMobileMessage();
        PublicsTool publicsTool = new PublicsTool();
        String random = publicsTool.getRandom();

        session.setAttribute("forgetPwId", id); //用户id
        session.setAttribute("forgetPwCheckCode", random);
        session.setAttribute("forgetPwTelephone", telephone);

        String content = "【煎饼金融P2P】尊敬的用户您好,验证码为:" + random + ",请妥善保管,验证码提供给他人将导致信息泄露";
        //System.out.println("=====================" + random);
        String message = sendMobileMessage.send(telephone, content);
        if (message.equals("0")) {
            result.setResult("success");
            result.setInfo("验证码已发送");
        } else {
            result.setResult("error");
            result.setInfo("验证码发送失败 请稍后重试");
        }

        return result;
    }

    /**
     * 验证验证码
     */
    @RequestMapping(value = "/forgetPwCheckCode", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean forgetPwCheckCode(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();

        String telephone = memberInfoMessageBean.getTelephone(); //用户手机号
        String forgetPwTelephone = (String) servletUtil.getSession().getAttribute("forgetPwTelephone"); //服务器手机号

        if (forgetPwTelephone == null || forgetPwTelephone.equals("")) {
            bean.setInfo("验证码已过期");
            bean.setResult("error");
            return bean;
        }
        if (!telephone.equals(forgetPwTelephone)) {
            bean.setInfo("修改手机号需重新获取验证码");
            bean.setResult("error");
            return bean;
        }

        String sessionCode = (String) servletUtil.getSession().getAttribute("forgetPwCheckCode");//服务器验证码
        String code = memberInfoMessageBean.getCode(); //用户验证码
        if (sessionCode == null || sessionCode.equals("")) {
            bean.setInfo("验证码已过期");
            bean.setResult("error");
            return bean;
        }
        if (!code.equals(sessionCode)) {
            bean.setInfo("验证码有误");
            bean.setResult("error");
            return bean;
        }
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/forgetPw/forget_pw_reset.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 更新密码
     */
    @RequestMapping(value = "/reSetPw", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean reSetPw(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        HttpSession session = servletUtil.getSession();
        String id = (String) session.getAttribute("forgetPwId");
        if (id == null || id.equals("")) {
            bean.setInfo("操作超时 请重新发送验证码");
            bean.setResult("error");
            return bean;
        }
        memberInfoMessageBean.setId(id);
        memberInfoMessageBean = securitySettingsService.reSetPw(memberInfoMessageBean);
        if (memberInfoMessageBean.getResult().equals("error")) {
            bean.setInfo("修改失败 请重试");
            bean.setResult("error");
            return bean;
        }

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/forgetPw/forget_pw_success.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;

    }


    /**
     * =======================================忘记密码========================================================================
     */
}
