package com.mh.peer.controller.appMobile;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.AppProductAttornDetailMessageBean;
import com.mh.peer.model.message.AppProductAttornMessageBean;
import com.mh.peer.model.message.AttornProductMessageBean;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.product.ProductAttornService;
import com.mh.peer.util.HuanXunInfo;
import com.mh.peer.util.MD5Util;
import com.mh.peer.util.Pdes;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-7-14.
 */
@Controller
@RequestMapping("/appAttorn")
public class AppProductAttornController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppProductAttornController.class);
    @Autowired
    private ProductAttornService productAttornService;
    @Autowired
    private HuanXunRegistService huanXunRegistService;

    /**
     * 获取债权转让列表
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAttornList", method = RequestMethod.POST)
    @ResponseBody
    public void getAppAttornList(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String typeId = request.getParameter("typeId"); //产品类型id
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String orderColumn = request.getParameter("orderColumn"); //排序字段
        String orderType = request.getParameter("orderType"); //排序类型
        String shFlag = ""; //审核状态(0-审核通过、1-审核不通过、2-待审核)
        String typeName = ""; //产品类型名称
        String attornFlag = ""; //转让状态
        String attornStr = ""; //债权转让部分拼接

        try{

            //债权转让集合
            List<AppProductAttornMessageBean> listAppProductAttornMes = productAttornService.getAppAttornList(memberId,typeId,pageIndex,pageSize,orderColumn,orderType);
            if (listAppProductAttornMes != null && listAppProductAttornMes.size() > 0){
                for (AppProductAttornMessageBean appProductAttornMessageBean : listAppProductAttornMes){
                    shFlag = appProductAttornMessageBean.getShFlag(); //审核状态(0-审核通过、1-审核不通过、2-待审核)
                    if (shFlag != null && !shFlag.equals("")){
                        if (shFlag.equals("0")){
                            attornFlag = "已转让";
                        }else if (shFlag.equals("2")){
                            attornFlag = "转让中";
                        }
                    }
                    typeId = appProductAttornMessageBean.getTypeId(); //产品类型id
                    if (typeId != null && !typeId.equals("")){
                       if (typeId.equals("26")){ //车贷宝
                           typeName = "车";
                       }else if (typeId.equals("27")){
                           typeName = "房";
                       }else if (typeId.equals("28")){
                           typeName = "融";
                       }else if (typeId.equals("29")){
                           typeName = "票";
                       }
                    }
                    attornStr = attornStr + "<div class=\"finac_info\" onclick=\"doAttornDetails('"+appProductAttornMessageBean.getId()+"')\">"
                              + "<div class=\"finac_con\">"
                              + "<div class=\"fi_title\">"
                              + "<p class=\"fi_che\">"+typeName+"</p>"
                              + "<ul class=\"fi_ul\">"
                              + "<li class=\"fi_yang\">"+appProductAttornMessageBean.getTitle()+"</li>"
                              + "</ul>"
                              + "</div>"
                              + "<table class=\"finac_table_sj\">"
                              + "<tr>"
                              + "<td>"
                              + "<span class=\"ft_zhi\">"+appProductAttornMessageBean.getExpectedRate()+"</span>%"
                              + "</td>"
                              + "<td>剩余期限：<span class=\"ft_zhi\">"+appProductAttornMessageBean.getSurplusDays()+"</span>天</td>"
                              + "</tr>"
                              + "<tr>"
                              + "<td>预期收益率</td>"
                              + "<td>转让价格(元)：<span class=\"ft_zhi\">"+appProductAttornMessageBean.getAttronPrice()+"</span></td>"
                              + "</tr>"
                              + "</table>"
                              + "<div class=\"financ_btn_yqw\">"
                              + "<input type=\"button\" value=\""+attornFlag+"\" class=\"btn_yqw\" />"
                              + "</div>"
                              + "</div>"
                              + "</div>";
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("attornStr",attornStr); //债权转让列表
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取债权转让标详情
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppAttornDetail", method = RequestMethod.POST)
    @ResponseBody
    public void getAppAttornDetail(HttpServletRequest request,HttpServletResponse response){
        String attornId = request.getParameter("attornId"); //债权转让id
        String shFlag = ""; //审核状态(0-审核通过、1-审核未通过、2-未审核)
        String repayment = ""; //还款方式
        String repaymentName = ""; //还款方式名称
        AppProductAttornDetailMessageBean appProductAttornDetailMessageBean = productAttornService.getAppAttornDetail(attornId);
        if (appProductAttornDetailMessageBean != null){
            repayment = appProductAttornDetailMessageBean.getRepayment();
            if (repayment != null && !repayment.equals("")){
                if (repayment.equals("0")){
                    repaymentName = "按月还款、等额本息";
                }else if (repayment.equals("1")){
                    repaymentName = "按月付息、到期还本";
                }else if (repayment.equals("2")){
                    repaymentName = "一次性还款";
                }
            }
            shFlag = appProductAttornDetailMessageBean.getShFlag(); //审核状态
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("attornId",attornId); //债权转让id
        in.put("title",appProductAttornDetailMessageBean.getTitle()); //题目
        in.put("loanRate",appProductAttornDetailMessageBean.getLoanRate()); //年利率
        in.put("attornPrice",appProductAttornDetailMessageBean.getAttronPrice()); //转让价格
        in.put("surplusDays",appProductAttornDetailMessageBean.getSurplusDays()); //剩余期限
        in.put("surplusPrincipal",appProductAttornDetailMessageBean.getSurplusPrincipal()); //剩余本金
        in.put("receivedPrice",appProductAttornDetailMessageBean.getReceivedPrice()); //待收本息
        in.put("nextReceiveTime",appProductAttornDetailMessageBean.getNextReceiveTime()); //下期还款日
        in.put("code",appProductAttornDetailMessageBean.getCode()); //产品编码
        in.put("loanFee",appProductAttornDetailMessageBean.getLoanFee()); //借款金额
        in.put("fullTime",appProductAttornDetailMessageBean.getFullTime()); //发标日期
        in.put("loanLength",appProductAttornDetailMessageBean.getLoanLength()); //借款时长
        in.put("repaymentName",repaymentName); //还款方式
        in.put("expectedRate",appProductAttornDetailMessageBean.getExpectedRate()); //预期收益率
        in.put("productId",appProductAttornDetailMessageBean.getProductId()); //产品id
        in.put("shFlag",shFlag); //审核状态(0-审核通过、1-审核不通过、2-未审核)
        in.put("projectNo",appProductAttornDetailMessageBean.getProjectNo()); //项目ID号
        in.put("otherIpsAcctNo",appProductAttornDetailMessageBean.getOtherIpsAcctNo()); //它方账号
        in.put("applyMemberId",appProductAttornDetailMessageBean.getApplyMemberId()); //借款人id
        in.put("buyer",appProductAttornDetailMessageBean.getBuyer()); //投资人id
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 债权部分冻结
     * @param request
     * @param response
     */
    @RequestMapping(value = "/appfreeze", method = RequestMethod.POST)
    @ResponseBody
    public void freezeAttornhx(HttpServletRequest request,HttpServletResponse response) {
        MemberInfo memberInfo = new MemberInfo();
        String memberId = request.getParameter("memberId"); //会员id
        String buyer = request.getParameter("buyer"); //投资人
        String attornId = request.getParameter("attornId"); //债权id
        String projectNo = request.getParameter("projectNo"); //项目ID号
        String trdAmt = request.getParameter("trdAmt"); //冻结金额
        String otherIpsAcctNo = request.getParameter("otherIpsAcctNo"); //它方账号
        String applyMemberId = request.getParameter("applyMemberId"); //借款申请人
        String ipsAcctNo = ""; //冻结账号
        String Uid = ""; //主键
        String operationType = "trade.freeze"; //操作类型(trade.freeze)
        String merchantID = "1810060028"; //商户存管交易账号
        String merBillNo = "181006" + UUID.randomUUID().toString().replaceAll("-", ""); //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //冻结日期
        String bizType = "2"; //业务类型(债权转让)
        String regType = "1"; //登记方式(1-手动、2-自动)
        String contractNo = ""; //合同号
        String authNo = ""; //授权号
        String merFee = "0"; //平台手续费
        String freezeMerType = "1"; //冻结方类型(1-用户、2-商户)
        String webUrl = "http://jianbing88.com/appAttorn/freezeWebResult"; //页面返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/attornhxResult"; //后台通知地址
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String requesthx = ""; //请求信息
        String requesthxJm = ""; //请求信息(加密)
        String md5Zs = ""; //md5证书
        String resultInfo = ""; //返回信息
        List<HuanXunRegistMessageBean> listHuanXunRegistMes = new ArrayList<HuanXunRegistMessageBean>(); //注册环迅会员信息
        HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean();

        try {

            if (memberId == null || memberId.equals("")) {
                resultInfo = "登录信息过期，请重新登录！";
            }else{
                LOGGER.info("投资人======"+buyer);
                if (memberId.equals(buyer)){ //投资人
                    resultInfo = "该债权由您发起，不可以对自己发起的债权进行投资！";
                }else{
                    if (memberId.equals(applyMemberId)){ //借款人
                        resultInfo = "您是该产品借款人，不允许对自己的产品进行承接！";
                    }else{
                        LOGGER.info("借款人======"+buyer);

                        huanXunRegistMessageBean.setMemberId(memberId);
                        listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
                        if (listHuanXunRegistMes != null && listHuanXunRegistMes.size() > 0){
                            ipsAcctNo = listHuanXunRegistMes.get(0).getIpsAcctNo(); //IPS冻结账号
                        }

                        Uid = UUID.randomUUID().toString().replaceAll("-", ""); //主键
                        s2SUrl = s2SUrl + "?id=" + Uid+"&attornId="+attornId; //后台通知地址

                        HuanXunInfo huanXunInfo = new HuanXunInfo();
                        md5Zs = huanXunInfo.getMd5Zs(); //md5证书

                        requesthx = "{\"projectNo\":\"" + projectNo + "\",\"merBillNo\":\"" + merBillNo + "\",\"bizType\":\"" + bizType + "\"," +
                                    "\"regType\":\"" + regType + "\",\"contractNo\":\"" + contractNo + "\",\"authNo\":\"" + authNo + "\",\"trdAmt\":\"" + trdAmt + "\"," +
                                    "\"merFee\":\"0\",\"freezeMerType\":\"" + freezeMerType + "\",\"otherIpsAcctNo\":\"" + otherIpsAcctNo + "\"," +
                                    "\"ipsAcctNo\":\"" + ipsAcctNo + "\",\"merDate\":\"" + merDate + "\",\"webUrl\":\"" + webUrl + "\",\"s2SUrl\":\"" + s2SUrl + "\"}";

                        requesthxJm = Pdes.encrypt3DES(requesthx); //3des加密
                        sign = operationType + merchantID + requesthxJm + md5Zs; //签名
                        signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密
                    }
                }
            }

            HashMap<String,Object> in = null;
            in = new HashMap<String,Object>();
            in.put("sign",signJm); //签名
            in.put("request",requesthxJm); //请求信息
            in.put("resultInfo",resultInfo); //错误信息
            output(response, JSONObject.fromObject(in).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 提现返回页面(投资冻结)
     * @param request
     * @param response
     */
    @RequestMapping(value = "/freezeWebResult", method = RequestMethod.POST)
    @ResponseBody
    public void postalWebResult(HttpServletRequest request,HttpServletResponse response){
        try{
            response.sendRedirect("freezeWait"); //返回冻结等待页面
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 环迅等待界面(投资冻结)
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/freezeWait", method = RequestMethod.GET)
    public String postalWait(HttpServletRequest request,HttpServletResponse response){
        return "appMobile/attornFreeze";
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
