package com.mh.peer.controller.huanxun;

import com.mh.peer.exception.HuanXunRegProjectException;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.*;
import com.mh.peer.service.huanxun.HuanXunRegProjectService;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.product.ProductSerialNumberService;
import com.mh.peer.service.product.ProductService;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.apache.commons.lang.math.DoubleRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.net.www.http.HttpClient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * Created by zhangerxin on 2016-7-26.
 * 环迅项目登记
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunRegProjectController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private ProductService productService;
    @Autowired
    private HuanXunRegistService huanXunRegistService;
    @Autowired
    private HuanXunRegProjectService huanXunRegProjectService;
    @Autowired
    private ProductSerialNumberService productSerialNumberService;

    /**
     * 项目登记
     * @param huanXunRegProjectMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/regProjecthx", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunRegProjectMessageBean regProjecthx(@ModelAttribute HuanXunRegProjectMessageBean huanXunRegProjectMes, ModelMap modelMap) {
        String projectId = huanXunRegProjectMes.getId(); //产品id
        String operationType = "project.regProject"; //操作类型(project.regProject)
        String merchantID = "1810060028"; //商户存管交易账号
        String md5Zs = ""; //md5证书
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String request = ""; //请求信息
        String requestJm = ""; //签名(加密)
        String merBillNo = "181006"; //商户订单号(前6位以商户号起始)
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //登记日期
        String projectNo = ""; //项目ID号
        String projectName = ""; //项目名称
        String projectType = "1"; //项目类型(1-P2P、2-众筹)
        String projectAmt = ""; //项目金额
        String rateType = "1"; //利率类型(1-固定、2-浮动)
        String rateVal = ""; //利率值
        String rateMinVal = ""; //最小利率值
        String rateMaxVal = ""; //最大利率值
        String cycVal = ""; //周期值(天)(String型)
        int cycValIn = 0; //周期值(天)(int型)
        String projectUse = ""; //项目用途
        String finaAccType = "1"; //融资方账户类型(1-个人、2-企业)
        String finaCertNo = ""; //融资方证件号
        String finaName = ""; //融资方姓名
        String finaIpsAcctNo = ""; //融资方IPS账号
        String isExcess = "0"; //是否超额(0-否、1-是)
        String s2SUrl = "http://120.76.137.147/huanxun/regProjecthxResult";
        String bidFee = ""; //最小投标金额(String型)
        Double bidFeeDou = 0d; //最小投标金额(Double型)
        String splitCount = ""; //最大拆分份数(String型)
        int splitCountIn = 0; //最大拆分份数(Int型)
        String loanYear = ""; //贷款期限(年)(String型)
        String loanMonth = ""; //贷款期限(月)(String型)
        String loadDay = ""; //贷款期限(日)(String型)
        String applyMember = ""; //融资人id
        String glId = ""; //关联id
        HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean(); //环迅部分人员注册
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm"; //请求地址
        String data = ""; //请求数据
        String resultCode = ""; //响应吗(000000-成功、999999-失败)
        String resultMsg = ""; //响应信息

        try{

            glId = UUID.randomUUID().toString().replaceAll("-",""); //关联id

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs(); //md5证书

            if (projectId == null || projectId.equals("")){
                huanXunRegProjectMes.setCode("-1");
            }

            /************************* 项目信息Start *************************/
            ProductHuanXunMessageBean productHuanXunMessageBean = new ProductHuanXunMessageBean();
            productHuanXunMessageBean.setId(projectId); //产品id
            List<ProductHuanXunMessageBean> listProductHuanXunMes = productService.queryProductDjInfo(productHuanXunMessageBean);
            if (listProductHuanXunMes != null && listProductHuanXunMes.size() > 0){
                projectNo = productSerialNumberService.getNextNumber(); //项目ID号
                projectName = listProductHuanXunMes.get(0).getTitle(); //项目名称
                bidFee = listProductHuanXunMes.get(0).getBidFee(); //最小投标金额(String型)
                splitCount = listProductHuanXunMes.get(0).getSplitCount(); //最大拆分份数(String型)
                if (bidFee != null && !bidFee.equals("") && splitCount != null && !splitCount.equals("")){
                    bidFeeDou = TypeTool.getDouble(bidFee); //最小投标金额(Double型)
                    splitCountIn = TypeTool.getInt(splitCount); //最大拆分份数(Int型)
                    projectAmt = TypeTool.getString(bidFeeDou * splitCountIn); //项目金额
                }
                rateVal = listProductHuanXunMes.get(0).getLoanRate(); //利率值
                loanYear = listProductHuanXunMes.get(0).getLoanYear(); //贷款期限(年)
                loanMonth = listProductHuanXunMes.get(0).getLoanMonth(); //贷款期限(月)
                loadDay = listProductHuanXunMes.get(0).getLoadDay(); //贷款期限(日)
                if (loanYear != null && !loanYear.equals("")){
                    cycValIn = cycValIn + TypeTool.getInt(loanYear) * 365;
                }
                if (loanMonth != null && !loanMonth.equals("")){
                    cycValIn = cycValIn + TypeTool.getInt(loanMonth) * 30;
                }
                if (loadDay != null && !loadDay.equals("")){
                    cycValIn = cycValIn + TypeTool.getInt(loadDay);
                }
                cycVal = TypeTool.getString(cycValIn); //周期值(天)(String型)
                finaCertNo = listProductHuanXunMes.get(0).getIdCard(); //融资方证件号
                finaName = listProductHuanXunMes.get(0).getRealName(); //融资方姓名

                /************** 融资人IPS信息Start ***************/
                applyMember = listProductHuanXunMes.get(0).getApplyMember(); //融资人id
                if (applyMember != null && !applyMember.equals("")){
                    huanXunRegistMessageBean.setMemberId(applyMember);
                    List<HuanXunRegistMessageBean> listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
                    if (listHuanXunRegistMes != null && listHuanXunRegistMes.size() > 0){
                        finaIpsAcctNo = listHuanXunRegistMes.get(0).getIpsAcctNo(); //融资人IPS账号
                    }
                }
                /*************** 融资人IPS信息End ****************/

            }
            /************************** 项目信息End **************************/


            s2SUrl = s2SUrl + "?projectId="+projectId+"&memberId="+applyMember+"&glId="+glId; //后台地址

            merBillNo = merBillNo + UUID.randomUUID().toString().replaceAll("-","").toLowerCase(); //商户订单号(前6位以商户号起始)

            request = "{\"merBillNo\":\""+merBillNo+"\",\"merDate\":\""+merDate+"\",\"projectNo\":\""+projectNo+"\","
                    + "\"projectName\":\""+projectName+"\",\"projectType\":\""+projectType+"\",\"projectAmt\":\""+projectAmt+"\","
                    + "\"rateType\":\""+rateType+"\",\"rateVal\":\""+rateVal+"\",\"rateMinVal\":\""+rateMinVal+"\","
                    + "\"rateMaxVal\":\""+rateMaxVal+"\",\"cycVal\":\""+cycVal+"\",\"projectUse\":\""+projectUse+"\","
                    + "\"finaAccType\":\""+finaAccType+"\",\"finaCertNo\":\""+finaCertNo+"\",\"finaName\":\""+finaName+"\","
                    + "\"finaIpsAcctNo\":\""+finaIpsAcctNo+"\",\"isExcess\":\""+isExcess+"\",\"s2SUrl\":\""+s2SUrl+"\"}";

            requestJm = Pdes.encrypt3DES(request); //3des加密
            sign = operationType + merchantID + requestJm + md5Zs; //签名
            signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

            System.out.println("开始项目登记请求");
            List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
            HTTPParam httpParam1 = new HTTPParam("operationType",operationType); //操作类型
            HTTPParam httpParam2 = new HTTPParam("merchantID",merchantID); //商户存管交易账号
            HTTPParam httpParam3 = new HTTPParam("request",requestJm); //请求信息
            HTTPParam httpParam4 = new HTTPParam("sign",signJm); //签名
            listHTTParam.add(httpParam1);
            listHTTParam.add(httpParam2);
            listHTTParam.add(httpParam3);
            listHTTParam.add(httpParam4);

            String result = HttpRequest.sendPost(url,listHTTParam); //获取接口调用后返回信息
            Map<String,String> map = JsonHelper.getObjectToMap(result); //将Json数据转换为Map格式
            if (map != null){
                resultCode = map.get("resultCode"); //响应状态
                resultMsg = map.get("resultMsg"); //响应信息
            }

            if (resultCode != null && !resultCode.equals("")){
                if (resultCode.equals("000000")){ //成功
                    huanXunRegProjectMes.setCode("0"); //程序返回状态
                    huanXunRegProjectMes.setInfo("审核成功！"); //程序返回信息
                }else{
                    huanXunRegProjectMes.setCode("-1");
                    huanXunRegProjectMes.setInfo("审核失败！"+resultMsg);
                }
            }else{
                huanXunRegProjectMes.setCode("-1");
                huanXunRegProjectMes.setInfo("审核失败！"+resultMsg);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return huanXunRegProjectMes;
    }


    /**
     * 项目登记后返回结果
     * @param request
     * @param modelMap
     */
    @RequestMapping(value = "/regProjecthxResult", method = RequestMethod.POST)
    @ResponseBody
    public void regProjecthxResult(HttpServletRequest request, HttpServletResponse responseInfo, ModelMap modelMap) {
        String projectId = request.getParameter("projectId"); //项目id
        String glId = request.getParameter("glId"); //关联id
        String memberId = request.getParameter("memberId"); // 会员id
        String resultCode = request.getParameter("resultCode"); //响应吗(000000-成功、999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //响应信息
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String response = request.getParameter("response"); //响应信息
        String responseJm = ""; //响应信息(解密)
        String merBillNo = ""; //商户订单号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS登记时间
        String projectNo = ""; //项目ID号
        String status = ""; //登记状态(0-失败、1-成功)
        Map<String,String> map = new HashMap<String,String>();
        String UidMyMessage = ""; //我的消息主键
        String UidTradeDetail = ""; //交易记录主键
        String titleProject = ""; //产品题目
        String bidFee = ""; //最小投标金额
        String splitCount = ""; //最大拆分份数
        Double ipsTrdAmtDou = 0d; //实际金额
        String titleMyMessage = ""; //我的消息题目
        String content = ""; //我的消息内容
        String balance = ""; //余额(String型)
        Double balanceDou = 0.0d; //余额(Double型)
        String result = "0"; //环迅项目登记结果
        Map<String,Object> mapException = new HashMap<String,Object>();

        try{

            output(responseInfo,"ipsCheckOk"); //接收成功

            if (response != null && !response.equals("")){
                responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                if (responseJm != null && !responseJm.equals("")){
                    map = JsonHelper.getObjectToMap(responseJm);
                    if (map != null){
                        merBillNo = map.get("merBillNo"); //商户订单号
                        ipsBillNo = map.get("ipsBillNo"); //IPS订单号
                        ipsDoTime = map.get("ipsDoTime"); //IPS登记时间
                        projectNo = map.get("projectNo"); //项目ID号
                        status = map.get("status"); //登记状态(0-失败、1-成功)
                    }
                }
            }

            ProductHuanXunMessageBean productHuanXunMessageBean = new ProductHuanXunMessageBean(); //项目登记信息
            productHuanXunMessageBean.setId(projectId); //主键
            List<ProductHuanXunMessageBean> listProductHuanXunMes = productService.queryProductDjInfo(productHuanXunMessageBean); //项目登记前信息查询

            if (listProductHuanXunMes != null && listProductHuanXunMes.size() > 0){
                titleProject = listProductHuanXunMes.get(0).getTitle(); //产品部分题目
                bidFee = listProductHuanXunMes.get(0).getBidFee(); //最小投标金额
                splitCount = listProductHuanXunMes.get(0).getSplitCount(); //最大拆分份数
                if (bidFee != null && !bidFee.equals("") && splitCount != null && !splitCount.equals("")){
                    ipsTrdAmtDou = TypeTool.getDouble(bidFee) * TypeTool.getInt(splitCount); //实际金额
                }
                balance = listProductHuanXunMes.get(0).getBalance(); //余额(String型)
                if (balance != null && !balance.equals("")){
                    balanceDou = TypeTool.getDouble(balance); //余额(Double型)
                }
            }

            /******************** 环迅项目登记Start ********************/
            HuanxunRegproject huanxunRegProject = new HuanxunRegproject();
            huanxunRegProject.setId(projectId); //主键
            huanxunRegProject.setGlid(glId); //关联id
            huanxunRegProject.setMemberid(memberId); //会员id
            huanxunRegProject.setResultcode(resultCode); //响应吗(000000-成功、999999-失败)
            huanxunRegProject.setResultmsg(resultMsg); //响应信息
            huanxunRegProject.setMerbillno(merBillNo); //商户订单号
            huanxunRegProject.setMerchantid(merchantID); //商户存管交易账号
            huanxunRegProject.setIpsbillno(ipsBillNo); //IPS订单号
            huanxunRegProject.setIpsdotime(ipsDoTime); //IPS登记时间
            huanxunRegProject.setProjectno(projectNo); //项目ID号
            huanxunRegProject.setStatus(status); //登记状态(0-失败、1-成功)
            huanxunRegProject.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd")); //创建时间
            /********************* 环迅项目登记End *********************/

            MemberMessage memberMessage = new MemberMessage(); //我的消息部分
            TradeHandle tradeHandle = new TradeHandle(); //流水号工具类
            TradeDetail tradeDetail = new TradeDetail(); //交易记录

            if(resultCode != null && !resultCode.equals("")){
                if (resultCode.equals("000000")){
                    /*************** 我的消息部分(Start) ***************/
                    UidMyMessage = UUID.randomUUID().toString().replaceAll("-",""); //主键
                    titleMyMessage = "申请借款标成功";
                    content = "<li>项目名称：<span>"+titleProject+"</span></li>"
                            + "<li>项目ID号：<span>"+projectNo+"</span></li>"
                            + "<li>商户订单号：<span>"+merBillNo+"</span></li>"
                            + "<li>IPS订单号：<span>"+ipsBillNo+"</span></li>"
                            + "<li>IPS登记日期：<span>"+ipsDoTime+"</span></li>";

                    memberMessage.setId(UidMyMessage); //主键
                    memberMessage.setGlid(projectId); //关联id
                    memberMessage.setType("0"); //类型(0-系统消息)
                    memberMessage.setMemberid(memberId); //会员id
                    memberMessage.setReadflag("1"); //读取状态(0-已读、1-未读)
                    memberMessage.setTitle(titleMyMessage); //题目
                    memberMessage.setContent(content); //内容
                    memberMessage.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送日期
                    memberMessage.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建日期
                    memberMessage.setDelflg("1"); //删除标志(0-已删除、1-未删除)
                    /**************** 我的消息部分(End) ****************/


                    /********** 交易记录Start **********/
                    int serialNumber = tradeHandle.getSerialNumber(); //流水号
                    UidTradeDetail = UUID.randomUUID().toString().replaceAll("-",""); //交易记录主键
                    tradeDetail.setId(UidTradeDetail); //主键
                    tradeDetail.setGlid(projectId); //关联id
                    tradeDetail.setSerialnum(serialNumber); //流水号
                    tradeDetail.setType("4"); //类型(0-充值、1-提现、2-投资、3-回款、4-借款、5-还款)
                    tradeDetail.setSprice(ipsTrdAmtDou); //实际金额
                    tradeDetail.setProcedurefee(0d); //手续费
                    tradeDetail.setFlag("0"); //状态(0-成功、1-失败)
                    tradeDetail.setMemberid(memberId); //会员id
                    tradeDetail.setSource("0"); //来源(0-电脑端、1-手机端)
                    tradeDetail.setTime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //交易时间
                    tradeDetail.setBz("环迅借款标项目登记"); //备注
                    tradeDetail.setBalance(balanceDou); //余额
                    tradeDetail.setCreateuser(memberId); //创建人
                    tradeDetail.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                    /*********** 交易记录End ***********/
                }
            }

            mapException.put("productId",projectId); //产品id
            mapException.put("resultCode",resultCode); //响应码
            mapException.put("resultMsg",resultMsg); //响应信息描述
            mapException.put("merchantID",merchantID); //商户存管交易账号
            mapException.put("response",response); //响应信息详情

            huanXunRegProjectService.regProjecthxResult(mapException,huanxunRegProject,memberMessage,tradeDetail); //环迅项目登记部分
        }catch(Exception e){
            HuanXunRegProjectException.handle(mapException); //输出异常文件
            e.printStackTrace();
        }
    }



    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
