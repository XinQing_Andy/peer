package com.mh.peer.controller.platform;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.message.QuestionContentMessageBean;
import com.mh.peer.service.platform.QuestionService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.StringWriter;

/**
 * Created by Cuibin on 2016/4/19.
 */
@Controller
@RequestMapping("/question")
public class QuestionController {
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private QuestionService questionService;
    /**
     * 查询所有
     */
    @RequestMapping(value = "/queryQuestionByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryQuestionByPage(@ModelAttribute Paging paging, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = paging.getPageIndex(); //当前数量
        String pageSize = paging.getPageSize(); //每页显示数量
        String currentPage = paging.getCurrentPage(); //当前页
        String title = paging.getTitle(); //题目
        //System.out.println("=================="+paging.getPageIndex());
        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
        }

        if(title != null && !title.equals("")){ //题目
            modelMap.put("title",title);
        }

        paging.setPageIndex(pageIndex); //当前页
        paging.setPageSize(pageSize); //每页显示数量
        paging = questionService.queryQuestionByPage(paging);
        String pageStr = this.getShowPage(currentPage,paging.getCount()); //显示所属分页

        modelMap.put("QuestionList", paging.getList()); //数据集合
        modelMap.put("totalCount", paging.getCount()); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/platform/problem.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }
    /**
     * 删除常见提问
     */
    @RequestMapping(value = "/deleteQuestion", method = RequestMethod.POST)
    @ResponseBody
    public QuestionContentMessageBean deleteQuestion(@ModelAttribute QuestionContentMessageBean qcmb) {
        return questionService.deleteQuestion(qcmb);
    }
    /**
     * 常见提问 开启添加页面
     */
    @RequestMapping(value = "/openAddQuestion", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openAddQuestion(@ModelAttribute QuestionContentMessageBean qcmb, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(qcmb.getMenuUrl(),modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }
    /**
     * 常见提问 添加
     */
    @RequestMapping(value = "/addQuestion", method = RequestMethod.POST)
    @ResponseBody
    public QuestionContentMessageBean addQuestion(@ModelAttribute QuestionContentMessageBean qcmb) {
        return questionService.addQuestion(qcmb);
    }
    /**
     * 常见提问 查询一个
     */
    @RequestMapping(value = "/queryOnlyQuestion", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryOnlyQuestion(@ModelAttribute QuestionContentMessageBean qcmb, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        QuestionContentMessageBean q = questionService.queryOnlyQuestion(qcmb);
        modelMap.put("question", q);
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(qcmb.getMenuUrl(),modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }
    /**
     * 常见提问 修改
     */
    @RequestMapping(value = "/updateQuestion", method = RequestMethod.POST)
    @ResponseBody
    public QuestionContentMessageBean updateQuestion(@ModelAttribute QuestionContentMessageBean qcmb) {
        return questionService.updateQuestion(qcmb);
    }
    /**
     * 常见提问 是否启用
     */
    @RequestMapping(value = "/updateStart", method = RequestMethod.POST)
    @ResponseBody
    public QuestionContentMessageBean updateStart(@ModelAttribute QuestionContentMessageBean qcmb) {
        return questionService.updateStart(qcmb);
    }

    /**
     * 获取需显示数字页码
     */
    public String getShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";
        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 < 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = 10 -(totalPage - currentin + 1);
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGoquPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }

}
