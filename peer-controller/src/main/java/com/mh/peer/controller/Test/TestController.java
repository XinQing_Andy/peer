package com.mh.peer.controller.Test;

import com.mh.peer.util.AutoRepayFreeze;
import com.mh.peer.util.AutoTransfer;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * Created by zhangerxin on 2016-9-5.
 * 测试类
 */
@Controller
@RequestMapping("/Test")
public class TestController {

    @RequestMapping(value = "/transferTest", method = RequestMethod.GET)
    public String getFontList(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap){
        return "test/transferTest";
    }

    @RequestMapping(value = "/repayTest", method = RequestMethod.GET)
    public String repayTest(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap){
        return "test/repayTest";
    }

    @RequestMapping(value = "/repayPriceTest", method = RequestMethod.POST)
    public void repayPriceTest(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap){
        String operateTime = request.getParameter("operateTime"); //操作时间
        String result = "0";

        try{


            /*************** 冻结部分Start ***************/
            AutoRepayFreeze autoRepayFreeze = new AutoRepayFreeze();
            String resultFreeze = autoRepayFreeze.freezeRepayhx(operateTime);
            /**************** 冻结部分End ****************/

        }catch(Exception e){
            e.printStackTrace();
            result = "1";
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("result",result);
        output(response, JSONObject.fromObject(in).toString());
    }

    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
