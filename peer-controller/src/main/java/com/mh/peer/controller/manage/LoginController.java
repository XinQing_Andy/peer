package com.mh.peer.controller.manage;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.message.LoginMessageBean;
import com.mh.peer.service.manager.LoginService;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpSession;

/**
 * Created by WangMeng on 2016/1/13.
 */
@Controller
@RequestMapping("/manage")
public class LoginController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private LoginService loginService;

    /**
     * 跳转至登录页面
     * @return
     */
    @RequestMapping(value = "/LoginPage", method = RequestMethod.GET)
    public String login(){
        return "login";
    }

    /**
     * 登录后跳转
     * @return
     */
//    @RequestMapping(value = "/mainPage", method = RequestMethod.GET)
//    public String main(){
//        if(!servletUtil.isLogin())
//            return "login";
//        return "main";
//    }

    /**
     * 登录操作
     * @param user
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public LoginMessageBean login(@ModelAttribute LoginMessageBean user){
        // 拿到会话
        HttpSession session = servletUtil.getSession();
        // 设置session
        user.setMessageId(session.getId() + session.getCreationTime());

        // 设置客户端IP
        user.setIp(servletUtil.getRequest().getRemoteAddr());
        LOGGER.info("接受信息==" + user);
        // 调用登录方法
        LoginBusinessBean loginBusinessBean = loginService.login(user);
        LOGGER.info("业务消息==" + loginBusinessBean);
        user.setInfo(loginBusinessBean.getInfo());
        user.setResult(loginBusinessBean.getResult());
        session.setAttribute("loginBusinessBean", loginBusinessBean);
        return user;
    }

    /**
     * 退出登录
     * @return
     */
    @RequestMapping(value = "/loginOut", method = RequestMethod.GET)
    public String loginout(){
        //拿到会话
        HttpSession session = servletUtil.getSession();
        //会话销毁返回登录页面
        session.invalidate();
        return "login";
    }
}
