package com.mh.peer.controller.manage;

import com.mh.peer.model.message.*;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.product.ProductAttornService;
import com.mh.peer.service.product.ProductBuyService;
import com.mh.peer.service.product.ProductRepayService;
import com.mh.peer.service.product.ProductService;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

/**
 * Created by cuibin on 2016/5/31.
 */
@Controller
@RequestMapping("/manage")
public class HomeController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductBuyService productBuyService;
    @Autowired
    private ProductAttornService productAttornService;
    @Autowired
    private ProductRepayService productRepayService;


    @RequestMapping(value = "/mainPage", method = RequestMethod.GET)
    public String mainPage(ModelMap modelMap){
        if(!servletUtil.isLogin())
            return "login";

        MainMessageBean mainMessageBean = new MainMessageBean();
        /**************** 查询会员总数 ****************/
        int memberCount = allMemberService.queryCountMember();
        mainMessageBean.setMemberCount(memberCount+"");
        /**************** 查询会员总数 ****************/

        /**************** 成功借款标数量 ****************/
        ProductMessageBean productMessageBean = new ProductMessageBean();
        productMessageBean.setShflag("0");//0 审核成功
        productMessageBean.setFullScale("0");//0 已经满标
        int porductCount = productService.getCountProductByPage(productMessageBean);
        mainMessageBean.setPorductCount(porductCount+"");
        /**************** 成功借款标数量 ****************/

        /**************** 成功借款总额 ****************/
        ProductMessageBean productMessageBean1 = productService.getTradePriceSum(productMessageBean);
        mainMessageBean.setFeeSum(productMessageBean1.getLoadFeeSum());
        /**************** 成功借款总额 ****************/

        /**************** 首页echar ****************/
        String[] yesterdayReg = allMemberService.queryYesterdayReg();
        String[] lastDayMoney = productBuyService.queryLastDayMoney();
        String[] lastDayInvest = productBuyService.queryLastDayInvest();
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        sb.append("\"yesterdayReg\":"+"\""+ Arrays.toString(yesterdayReg) + "\",");
        sb.append("\"lastDayMoney\":"+"\""+ Arrays.toString(lastDayMoney) + "\",");
        sb.append("\"lastDayInvest\":"+"\""+ Arrays.toString(lastDayInvest) + "\"");
        sb.append("}");
        modelMap.put("jStr", sb);
        /**************** 首页echar ****************/

        /**************** 待审核借款标数量 ****************/
        ProductMessageBean productMessageBean2 = new ProductMessageBean();
        productMessageBean2.setShflag("2");
        mainMessageBean.setAwaitBorrowSum(productService.getCountProductByPage(productMessageBean2)+"");
        /**************** 待审核借款标数量 ****************/

        /**************** 将要到期借款标数量 ****************/
        mainMessageBean.setDueDateSum(productService.getCountDueDate()+"");
        /**************** 将要到期借款标数量 ****************/

        /**************** 满表借款标数量 ****************/
        ProductMessageBean productMessageBean3 = new ProductMessageBean();
        productMessageBean3.setFullScale("0");//满标
        mainMessageBean.setFullScaleSum(productService.getCountProductByPage(productMessageBean3)+"");
        /**************** 满表借款标数量 ****************/

        /**************** 当前时间 ****************/
        String time = StringTool.getString(DaoTool.getTime(), "yyyy年MM月dd日 hh:mm:ss");
        modelMap.put("time",time);
        /**************** 当前时间 ****************/

        /**************** 转让申请数量 ****************/
        ProductAttornRecordMessageBean productAttornRecordMessageBean = new ProductAttornRecordMessageBean();
        productAttornRecordMessageBean.setShFlag("2");
        mainMessageBean.setTransferSum(productAttornService.getCountByType(productAttornRecordMessageBean));
        /**************** 转让申请数量 ****************/

        /**************** 待放款借款标 ****************/
        ProductMessageBean productMessageBean4 = new ProductMessageBean();
        productMessageBean4.setShflag("0");
        productMessageBean4.setFullScale("0");
        mainMessageBean.setWaitLoan(productService.getCountProductByPage(productMessageBean4) + "");
        /**************** 待放款借款标 ****************/

        /**************** 到期还款金额 ****************/
        mainMessageBean.setExpireRefund(productRepayService.getRepayedPrice(null, "0"));
        /**************** 到期还款金额 ****************/

        /**************** 待还总额 ****************/
        mainMessageBean.setWaitTotal(productRepayService.getRepayedPrice(null, "1"));
        /**************** 待还总额 ****************/

        modelMap.put("mainMessageBean", mainMessageBean);
        return "main";
    }


    /**
     * 查询过去一天注册会员数量 和 昨天新增投资数量
     */
    @RequestMapping(value = "/queryYesterdayReg", method = RequestMethod.POST)
    @ResponseBody
    public MainGraphMessageBean queryYesterdayReg() {
        MainGraphMessageBean mainGraphMessageBean = new MainGraphMessageBean();
        mainGraphMessageBean.setYesterdayReg(allMemberService.queryYesterdayReg());
        mainGraphMessageBean.setLastDayMoney(productBuyService.queryLastDayMoney());
        return mainGraphMessageBean;
    }

    /**
     * 查询过去七天注册会员数量 和 过去七天新增投资数量
     */
    @RequestMapping(value = "/queryLastWeekReg", method = RequestMethod.POST)
    @ResponseBody
    public MainGraphMessageBean queryLastWeekReg() {
        MainGraphMessageBean mainGraphMessageBean = new MainGraphMessageBean();
        mainGraphMessageBean.setLastWeekReg(allMemberService.queryLastWeekReg());
        mainGraphMessageBean.setLastWeekMoney(productBuyService.queryLastWeekMoney());
        mainGraphMessageBean.setShowDate(allMemberService.queryDate(7));
        return mainGraphMessageBean;
    }

    /**
     * 查询过去30天注册会员数量 和 过去30天新增投资数量
     */
    @RequestMapping(value = "/queryLastMonthReg", method = RequestMethod.POST)
    @ResponseBody
    public MainGraphMessageBean queryLastMonthReg() {
        MainGraphMessageBean mainGraphMessageBean = new MainGraphMessageBean();
        mainGraphMessageBean.setLastMonthReg(allMemberService.queryLastMonthReg());
        mainGraphMessageBean.setLastMonthMoney(productBuyService.queryLastMonthMoney());
        mainGraphMessageBean.setShowDate(allMemberService.queryDateMonth());
        return mainGraphMessageBean;
    }


    /**
     * 查询昨天投资金额 和 充值金额
     */
    @RequestMapping(value = "/queryLastDayInvest", method = RequestMethod.POST)
    @ResponseBody
    public MainGraphMessageBean queryLastDayInvest() {
        MainGraphMessageBean mainGraphMessageBean = new MainGraphMessageBean();
        mainGraphMessageBean.setLastDayInvest(productBuyService.queryLastDayInvest());
        return mainGraphMessageBean;
    }


    /**
     * 查询最近七天投资金额 和 充值金额
     */
    @RequestMapping(value = "/queryLastWeekInvest", method = RequestMethod.POST)
    @ResponseBody
    public MainGraphMessageBean queryLastWeekInvest() {
        MainGraphMessageBean mainGraphMessageBean = new MainGraphMessageBean();
        mainGraphMessageBean.setLastWeekInvest(productBuyService.queryLastWeekInvest());
        mainGraphMessageBean.setShowDate(allMemberService.queryDate(7));
        return mainGraphMessageBean;
    }

    /**
     * 查询最近30天投资金额 和 充值金额
     */
    @RequestMapping(value = "/queryLastMonthInvest", method = RequestMethod.POST)
    @ResponseBody
    public MainGraphMessageBean queryLastMonthInvest() {
        MainGraphMessageBean mainGraphMessageBean = new MainGraphMessageBean();
        mainGraphMessageBean.setLastMonthInvest(productBuyService.queryLastMonthInvest());
        mainGraphMessageBean.setShowDate(allMemberService.queryDateMonth());
        return mainGraphMessageBean;
    }



}
