package com.mh.peer.controller.manage;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.MenuBeanMessage;
import com.mh.peer.service.manager.MainService;
import com.mh.peer.util.JetbrickTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by WangMeng on 2016/1/13.
 */
@Controller
@RequestMapping("/manage")
public class MainController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
    @Autowired
    private MainService mainService;
    @Autowired
    private JetbrickTool jetbrickTool;

    /**
     * 获取一级菜单
     * @param menuBeanMessage
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/menu", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean getTopMenu(@ModelAttribute MenuBeanMessage menuBeanMessage,ModelMap modelMap){
        JetbrickBean jetbrickBean = new JetbrickBean();
        //调用service(一级菜单)
        List<MenuBeanMessage> menuBeanMessages = mainService.getMenu(menuBeanMessage);
        //赋值到共享对象
        modelMap.put("menulist",menuBeanMessages);

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(menuBeanMessage.getMenuUrl(), modelMap);
            jetbrickBean.setHtmlText(stringWriter.toString());
            jetbrickBean.setInfo("成功");
            jetbrickBean.setResult("success");
        }catch (Exception e){
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            jetbrickBean.setInfo("失败");
            jetbrickBean.setResult("error");
            jetbrickBean.setHtmlText("");
        }
        return jetbrickBean;
    }

    /**
     * 获取二级菜单
     * @param menuBeanMessage
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/levelmenu", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean getMenu(@ModelAttribute MenuBeanMessage menuBeanMessage,ModelMap modelMap){
        JetbrickBean jetbrickBean = new JetbrickBean();
        //调用service
        List<MenuBeanMessage> menuBeanMessages = mainService.getLevelMenu(menuBeanMessage);
        //赋值到共享对象
        modelMap.put("levelmenulist",menuBeanMessages);
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(menuBeanMessage.getMenuUrl(), modelMap);
            jetbrickBean.setHtmlText(stringWriter.toString());
            jetbrickBean.setInfo("成功");
            jetbrickBean.setResult("success");
        }catch (Exception e){
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            jetbrickBean.setInfo("失败");
            jetbrickBean.setResult("error");
            jetbrickBean.setHtmlText("");
        }
        return jetbrickBean;
    }

    /**
     * 获取三级菜单
     * @param menuBeanMessage
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/thirdmenu", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean getThirdMenu(@ModelAttribute MenuBeanMessage menuBeanMessage,ModelMap modelMap){
        JetbrickBean jetbrickBean = new JetbrickBean();
        //调用service
        List<MenuBeanMessage> menuBeanMessages = mainService.getLevelMenu(menuBeanMessage);
        //赋值到共享对象
        modelMap.put("thirdmenulist",menuBeanMessages);
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(menuBeanMessage.getMenuUrl(), modelMap);
            jetbrickBean.setHtmlText(stringWriter.toString());
            jetbrickBean.setInfo("成功");
            jetbrickBean.setResult("success");
        }catch (Exception e){
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            jetbrickBean.setInfo("失败");
            jetbrickBean.setResult("error");
            jetbrickBean.setHtmlText("");
        }
        return jetbrickBean;
    }


    /**
     * 获取产品信息
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryProductList", method = RequestMethod.GET)
    public String queryProductList(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap){
        System.out.println("1234567890");
        return "product/product_huanxun";
    }
}
