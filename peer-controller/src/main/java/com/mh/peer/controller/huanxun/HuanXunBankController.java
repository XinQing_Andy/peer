package com.mh.peer.controller.huanxun;

import com.mh.peer.model.message.BankWhMessageBean;
import com.mh.peer.model.message.HuanXunBankMessageBean;
import com.mh.peer.model.message.HuanXunRechargeMessageBean;
import com.mh.peer.util.HuanXunInfo;
import com.mh.peer.util.MD5Util;
import com.mh.peer.util.Pdes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zhangerxin on 2016-7-24.
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunBankController {

    @RequestMapping(value = "/queryBankhx", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunBankMessageBean queryBankhx(@ModelAttribute HuanXunBankMessageBean huanXunBankMes, ModelMap modelMap) {
        String operationType = "query.bankQuery"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String request = "{}"; //请求信息
        String requestJm = ""; //请求信息(加密)
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String md5Zs = ""; //md5证书
        HuanXunBankMessageBean huanXunBankMessageBean = new HuanXunBankMessageBean();

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs(); //md5证书

            requestJm = Pdes.encrypt3DES(request); //3des加密

            sign = operationType + merchantID + requestJm + md5Zs; //签名

            signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

            System.out.println("request======"+request);
            System.out.println("requestJm======"+requestJm);
            System.out.println("sign======"+sign);
            System.out.println("signJm======"+signJm);

            huanXunBankMessageBean.setSign(signJm);
            huanXunBankMessageBean.setRequest(requestJm);
        }catch(Exception e){
            e.printStackTrace();
        }
        return huanXunBankMessageBean;
    }
}
