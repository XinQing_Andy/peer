package com.mh.peer.controller.huanxun;

import com.mh.peer.exception.HuanXunFreezeInvestException;
import com.mh.peer.exception.HuanXunUnFreezeInvestException;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.service.huanxun.HuanXunFreezeService;
import com.mh.peer.service.huanxun.HuanXunInvestService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.JsonHelper;
import com.mh.peer.util.Pdes;
import com.mh.peer.util.ServletUtil;
import com.mh.peer.util.TradeHandle;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-7-28.
 * 环迅投标(投标部分)
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunInvestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HuanXunFreezeController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private HuanXunInvestService huanXunInvestService;
    @Autowired
    private HuanXunFreezeService huanXunFreezeService;


    /**
     * 项目投标结果
     *
     * @param request
     * @param modelMap
     */
    @RequestMapping(value = "/investhxResult", method = RequestMethod.POST)
    @ResponseBody
    public void investhxResult(HttpServletRequest request, HttpServletResponse responsehx, ModelMap modelMap) {
        System.out.println("开始进入后台");
        MemberInfo memberInfo = new MemberInfo(); //会员信息
        String buyId = request.getParameter("id"); //产品投资id
        String productId = request.getParameter("productId"); //产品id
        String memberId = request.getParameter("memberId"); //会员id
        String freezeType = request.getParameter("freezeType"); //冻结类型
        String resultCode = request.getParameter("resultCode"); //响应吗(000000-成功、000001~999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //响应信息
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String response = request.getParameter("response"); //响应信息
        String responseJm = ""; //响应信息解密
        String merBillNo = request.getParameter("merBillNo"); //商户订单号
        String projectNo = request.getParameter("projectNo"); //项目ID号
        String ipsTrdAmt = request.getParameter("ipsTrdAmt"); //IPS冻结金额(String型)
        Double ipsTrdAmtDou = 0.0d; //IPS冻结金额(Double型)
        String ipsAcctNo = request.getParameter("ipsAcctNo"); //冻结账号
        String otherIpsAcctNo = request.getParameter("otherIpsAcctNo"); //它方账号
        String ipsBillNo = request.getParameter("ipsBillNo"); //IPS订单号
        String ipsDoTime = request.getParameter("ipsDoTime"); //IPS处理时间
        String trdStatus = request.getParameter("trdStatus"); //冻结状态(0-失败、1-成功)
        String UidTradeDetail = ""; //交易记录主键
        String UidMyMessage = ""; //我的消息主键
        String balance = ""; //余额(String型)
        Double balanceDou = 0.0d; //余额(Double型)
        String title = ""; //我的消息题目
        String content = ""; //内容
        Map<String, String> map = new HashMap<String, String>();
        Map<String,Object> mapException = new HashMap<String,Object>();

        try {

            System.out.println("resultCode======"+resultCode+",resultMsg======"+resultMsg);
            output(responsehx,"ipsCheckOk"); //接收成功

            UidTradeDetail = UUID.randomUUID().toString().replaceAll("-", ""); //交易记录主键

            MemberInfoMessageBean memberInfoMessageBean = allMemberService.queryMemberInfoById(memberId);
            if (memberInfoMessageBean != null) {
                balance = memberInfoMessageBean.getBalance(); //余额(String型)
                if (balance != null && !balance.equals("")) {
                    balanceDou = TypeTool.getDouble(balance); //余额(Double型)
                }
            }

            if (response != null && !response.equals("")) {
                responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                if (responseJm != null && !responseJm.equals("")) {
                    map = JsonHelper.getObjectToMap(responseJm);
                    if (map != null) {
                        merBillNo = map.get("merBillNo"); //商户订单号
                        projectNo = map.get("projectNo"); //项目ID号

                        ipsTrdAmt = map.get("ipsTrdAmt"); //IPS冻结金额
                        if (ipsTrdAmt != null && !ipsTrdAmt.equals("")) {
                            ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt);
                            balanceDou = balanceDou - ipsTrdAmtDou; //冻结后余额
                        }

                        ipsAcctNo = map.get("ipsAcctNo"); //冻结账号
                        otherIpsAcctNo = map.get("otherIpsAcctNo"); //它方账号
                        ipsBillNo = map.get("ipsBillNo"); //IPS订单号
                        ipsDoTime = map.get("ipsDoTime"); //IPS处理时间
                        trdStatus = map.get("trdStatus"); //冻结状态(0-失败、1-成功)
                    }
                }
            }

            /*************** 冻结信息Start ***************/
            HuanxunFreeze huanxunFreeze = new HuanxunFreeze();
            huanxunFreeze.setId(buyId); //产品投资id
            huanxunFreeze.setProductid(productId); //产品id
            huanxunFreeze.setMemberid(memberId); //会员id
            huanxunFreeze.setFreezetype(freezeType); //冻结类型
            huanxunFreeze.setFlag("0"); //状态(0-冻结、1-解冻)
            huanxunFreeze.setResultcode(resultCode); //响应吗(000000-成功、000001~999999-失败)
            huanxunFreeze.setResultmsg(resultMsg); //响应信息
            huanxunFreeze.setMerchantid(merchantID); //商户存管交易账号
            huanxunFreeze.setSign(sign); //签名
            huanxunFreeze.setMerbillno(merBillNo); //商户订单号
            huanxunFreeze.setProjectno(projectNo); //项目ID号
            huanxunFreeze.setIpstrdamt(ipsTrdAmtDou); //IPS冻结金额
            huanxunFreeze.setIpsacctno(ipsAcctNo); //冻结账号
            huanxunFreeze.setOtheripsacctno(otherIpsAcctNo); //它方账号
            huanxunFreeze.setIpsbillno(ipsBillNo); //IPS订单号
            huanxunFreeze.setIpsdotime(ipsDoTime); //IPS处理时间
            huanxunFreeze.setTrdstatus(trdStatus); //冻结状态(0-失败、1-成功)
            huanxunFreeze.setDelflg("1"); //是否删除(0-删除、1-未删除)
            huanxunFreeze.setCreatetime(ipsDoTime); //创建时间
            /**************** 冻结信息End ****************/

            /*************** 产品投资Start ***************/
            ProductBuyInfo productBuyInfo = new ProductBuyInfo();
            productBuyInfo.setId(buyId); //产品投资id
            productBuyInfo.setProductid(productId); //产品id
            productBuyInfo.setBuyer(memberId); //产品投资人
            productBuyInfo.setPrice(ipsTrdAmtDou); //产品投资金额
            productBuyInfo.setBuytime(ipsDoTime); //购买时间
            productBuyInfo.setType("0"); //类型 0-持有 1-转让中 2-已转让
            productBuyInfo.setCreatetime(ipsDoTime); //创建时间
            /**************** 产品投资End ****************/

            /*************** 交易记录Start ***************/
            TradeHandle tradeHandle = new TradeHandle(); //流水号工具类
            int serialNumber = tradeHandle.getSerialNumber(); //流水号
            TradeDetail tradeDetail = new TradeDetail();
            tradeDetail.setId(UidTradeDetail); //交易记录主键
            tradeDetail.setGlid(buyId); //关联id
            tradeDetail.setSerialnum(serialNumber); //交易流水号
            tradeDetail.setType("2"); //交易类型(0-充值、1-提现、2-投资、3-回款、4-借款、5-还款)
            tradeDetail.setSprice(ipsTrdAmtDou); //实际金额
            tradeDetail.setProcedurefee(0d); //手续费
            tradeDetail.setFlag("0"); //状态(0-成功、1-失败)
            tradeDetail.setMemberid(memberId); //会员id
            tradeDetail.setSource("0"); //来源(0-电脑端、1-手机端)
            tradeDetail.setTime(ipsDoTime); //交易时间
            tradeDetail.setBz("环迅项目投标"); //备注
            tradeDetail.setBalance(balanceDou); //余额
            tradeDetail.setCreateuser(memberId); //创建人id
            tradeDetail.setCreatetime(ipsDoTime); //创建时间
            /**************** 交易记录End ****************/

            /*************** 我的消息Start ***************/
            UidMyMessage = UUID.randomUUID().toString().replaceAll("-", ""); //我的消息主键
            title = "环迅投资金额冻结";
            content = "<li>您有" + ipsTrdAmtDou + "元金额因投标被冻结，等待满标审核通过后自动转账给借款申请人</li>"
                    + "<li>冻结账号：" + ipsAcctNo + "</li>"
                    + "<li>项目编号：" + projectNo + "</li>"
                    + "<li>IPS订单号：" + ipsBillNo + "</li>"
                    + "<li>IPS处理时间：" + ipsDoTime + "</li>";

            MemberMessage memberMessage = new MemberMessage(); //我的消息部分
            memberMessage.setId(UidMyMessage); //主键
            memberMessage.setGlid(buyId); //关联id
            memberMessage.setType("0"); //类型(0-系统消息)
            memberMessage.setMemberid(memberId); //会员id
            memberMessage.setReadflag("1"); //读取状态(0-已读、1-未读)
            memberMessage.setTitle(title); //题目
            memberMessage.setContent(content); //内容
            memberMessage.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送日期
            memberMessage.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建日期
            memberMessage.setDelflg("1"); //删除标志(0-已删除、1-未删除)
            /**************** 我的消息End ****************/

            mapException.put("productId",productId); //产品id
            mapException.put("memberId",memberId); //会员id
            mapException.put("resultCode",resultCode); //响应吗(000000-成功、000001~999999-失败)
            mapException.put("resultMsg",resultMsg); //响应信息描述
            mapException.put("response",response); //响应信息
            mapException.put("merchantID",merchantID); //商户存管交易账号
            mapException.put("merBillNo",merBillNo); //商户订单号
            mapException.put("projectNo",projectNo); //项目ID号
            mapException.put("ipsTrdAmt",ipsTrdAmt); //冻结金额
            mapException.put("ipsAcctNo",ipsAcctNo); //冻结账号
            mapException.put("otherIpsAcctNo",otherIpsAcctNo); //它方账号
            mapException.put("ipsBillNo",ipsBillNo); //IPS订单号
            mapException.put("ipsDoTime",ipsDoTime); //IPS处理时间
            mapException.put("trdStatus",trdStatus); //冻结状态

            huanXunInvestService.investhxResult(mapException,huanxunFreeze, productBuyInfo, tradeDetail, memberMessage);
        } catch (Exception e) {
            HuanXunFreezeInvestException.handle(mapException); //投资冻结资金部分处理异常
            e.printStackTrace();
        }
    }


    /**
     * 解冻金额 (环迅返回方法) by cuibin，修改by zhangerxin
     *
     * @param request
     */
    @RequestMapping(value = "/unfreezeResult", method = RequestMethod.POST)
    @ResponseBody
    public void unfreezeResult(HttpServletRequest request,HttpServletResponse response) {
        String resultCode = request.getParameter("resultCode"); //返回码
        String resultMsg = request.getParameter("resultMsg"); //返回信息描述
        String merchantID = request.getParameter("merchantID"); //商户存管交易账户
        String responsehx = request.getParameter("response"); //响应信息
        String responseJm = ""; //响应信息(解密)
        String id = request.getParameter("id"); //冻结表id
        String flag = ""; //状态(0-冻结、1-解冻、2-转账)
        String merBillNo = ""; //商户订单号
        String projectNo = ""; //项目ID号
        String freezeId = ""; //原 IPS冻结订单号
        String merFee = ""; //解冻账号
        String ipsAcctNo = ""; //平台手续费
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String trdStatus = ""; //解冻状态
        String trdAmt = ""; //原冻结金额
        String memberId = ""; //用户id
        String balance = ""; //原余额(String型)
        String newBalanceStr = ""; //新余额
        Map<String, String> map = new HashMap<String, String>(); //解密后内容
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息

       try{

           output(response,"ipsCheckOk"); //接收成功

           if (resultCode != null && !resultCode.equals("")){ //响应码
               if (resultCode.equals("000000")){
                   /*************** 响应信息Start ***************/
                   if (responsehx != null && !responsehx.equals("")){
                       responseJm = Pdes.decrypt3DES(responsehx); //响应信息(解密)
                       if (responseJm != null && !responseJm.equals("")) {
                           map = JsonHelper.getObjectToMap(responseJm);
                           if (map != null) {
                               merBillNo = map.get("merBillNo"); //商户订单号
                               projectNo = map.get("projectNo"); //项目ID号
                               freezeId = map.get("freezeId"); //原 IPS 冻结 订单号
                               merFee = map.get("merFee"); //解冻账号
                               ipsAcctNo = map.get("ipsAcctNo"); //平台手续费
                               ipsBillNo = map.get("ipsBillNo"); //IPS订单号
                               ipsDoTime = map.get("ipsDoTime"); //IPS 处理时 间
                               trdStatus = map.get("trdStatus"); //解冻状态
                           }
                       }
                   }
                   /**************** 响应信息End ****************/

                   //查询冻结记录
                   HuanXunFreezeMessageBean huanXunFreezeMessageBean = huanXunFreezeService.queryOnlyfreeze(id);

                   if (huanXunFreezeMessageBean != null) {
                       flag = huanXunFreezeMessageBean.getFlag(); //状态(0-冻结、1-解冻、2-转账)
                       if (flag != null && !flag.equals("")){
                           if (!flag.equals("1")){ //未解冻
                               if (huanXunFreezeMessageBean.getResultCode() != null && !huanXunFreezeMessageBean.getResultCode().equals("")){
                                   if (huanXunFreezeMessageBean.getResultCode().equals("000000")){ //成功
                                       trdAmt = huanXunFreezeMessageBean.getTrdAmt(); //原冻结金额
                                       memberId = huanXunFreezeMessageBean.getMemberId(); //用户id
                                   }
                               }
                           }
                       }

                       mapException.put("id",id); //冻结表主键
                       mapException.put("resultCode",resultCode); //响应码
                       mapException.put("resultMsg",resultMsg); //响应信息描述
                       mapException.put("merchantID",merchantID); //商户存管交易账户
                       mapException.put("response",responsehx); //响应信息

                       //查询余额
                       balance = allMemberService.queryOnlyMemberBalance(memberId);
                       MemberInfo memberInfo = new MemberInfo(); //会员
                       if (balance != null && !balance.equals("")) {
                           double newBalance = TypeTool.getDouble(balance) + TypeTool.getDouble(trdAmt); //新余额
                           memberInfo.setId(memberId); //会员id
                           memberInfo.setBalance(newBalance); //余额
                       }

                       HuanxunFreeze huanxunFreeze = new HuanxunFreeze(); //环迅冻结
                       huanxunFreeze.setId(id); //主键
                       huanxunFreeze.setFlag("1"); //状态(0-冻结、1-解冻、2-转账)
                       huanxunFreeze.setMerbillno(merBillNo); //商户订单号
                       huanXunInvestService.unfreezeResult(mapException,memberInfo,huanxunFreeze);
                   }
               }
           }
       }catch (Exception e){
           HuanXunUnFreezeInvestException.handle(mapException);
           e.printStackTrace();
       }
    }




    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
