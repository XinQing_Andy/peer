package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.ProductInvestMessageBean;
import com.mh.peer.model.message.ProductOverRepayMessageBean;
import com.mh.peer.model.message.ProductRepayMessageBean;
import com.mh.peer.model.message.RepayDetailMessageBean;
import com.mh.peer.service.product.ProductRepayService;
import com.mh.peer.util.FrontPage;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by zhangerxin on 2016-5-27.
 * 还款部分
 */
@Controller
@RequestMapping("/repayRecord")
public class RepayRecordController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ProductRepayService productRepayService;


    /**
     * 获取还款部分
     * @param productRepayMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryRepayByPage", method = RequestMethod.POST)
    @ResponseBody
    public ProductRepayMessageBean queryRepayByPage(@ModelAttribute ProductRepayMessageBean productRepayMes, ModelMap modelMap){

        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productRepayMes.getPageIndex(); //当前页
        String pageSize = productRepayMes.getPageSize(); //每页显示数量
        String orderColumn = productRepayMes.getOrderColumn(); //排序字段
        String orderType = productRepayMes.getOrderType(); //排序类型
        String repayStr = ""; //还款列表部分拼接
        String srepaymentTime = ""; //实际还款日期
        String repayFlag = ""; //还款状态
        String pageStr = ""; //分页部分拼接

        if(pageIndex == null || pageIndex.equals("")){ //当前页
            pageIndex = "1";
            productRepayMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            productRepayMes.setPageSize(pageSize);
        }

        /****************************** 产品还款部分Start ******************************/
        //产品还款集合
        List<ProductRepayMessageBean> listProductRepayMes = productRepayService.getRepayListByPage(productRepayMes);
        //产品还款数量
        int totalCount = productRepayService.getRepayCount(productRepayMes);
        if(listProductRepayMes != null){

            /*************** 还款循环Start ***************/
            for(ProductRepayMessageBean productRepayMessageBean : listProductRepayMes){
                srepaymentTime = productRepayMessageBean.getSrepaymentTime(); //实际还款日期
                if(srepaymentTime != null && !srepaymentTime.equals("")){
                    repayFlag = "已还款";
                }else{
                    repayFlag = "未还款";
                }

                repayStr = repayStr + "<tr>"
                         + "<td>"+productRepayMessageBean.getYrepaymentTime()+"</td>"
                         + "<td>"+productRepayMessageBean.getPrincipal()+"元</td>"
                         + "<td>"+productRepayMessageBean.getInterest()+"元</td>"
                         + "<td>"+productRepayMessageBean.getYprice()+"元</td>"
                         + "<td>"+repayFlag+"<td>"
                         + "</tr>";
            }
            /**************** 还款循环End ****************/
        }
        /******************************* 产品还款部分End *******************************/

        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageRepay","goRepayPage");
        productRepayMes.setHtmlText(repayStr); //列表内容
        productRepayMes.setPageStr(pageStr); //分页部分拼接

        //获取还款本息
        ProductRepayMessageBean productRepayMessageBean = productRepayService.getRepayPriceSum(productRepayMes);
        productRepayMes.setPriceSum(productRepayMessageBean.getPriceSum());
        return productRepayMes;
    }


    /**
     * 还款详情
     * @param productRepayMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryRepayDetail", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryRepayDetail(@ModelAttribute ProductRepayMessageBean productRepayMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productRepayMes.getPageIndex(); //当前数量
        String pageSize = productRepayMes.getPageSize(); //每页显示数量
        String currentPage = productRepayMes.getCurrentPage(); //当前页
        int pageCount = 0; //总页数
        String pageStr = ""; //分页部分

        if (pageIndex == null || pageIndex.equals("")) { //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "10";
        }

        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        if (memberInfo != null){
            memberId = memberInfo.getId(); //会员id
            productRepayMes.setPageIndex(pageIndex); //当前页
            productRepayMes.setPageSize(pageSize); //每页显示数量
            productRepayMes.setApplyMember(memberId); //申请人
            List<ProductRepayMessageBean> repayList = productRepayService.getRepayList(productRepayMes);
            //产品还款数量
            int totalCount = productRepayService.getRepayCount(productRepayMes);
            FrontPage frontPage = new FrontPage();
            pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageRepayDetail","goRepayDetailPage"); //分页部分拼接
            modelMap.put("repayList", repayList); //产品集合
            modelMap.put("totalCount", totalCount); //产品数量
            modelMap.put("currentPage", currentPage); //当前页
            modelMap.put("pageCount",pageCount); //总数量
            modelMap.put("pageStr",pageStr); //分页
        }

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productRepayMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 获取总页数
     * @param pageSize
     * @param totalCount
     * @return
     */
    public int getPageCount(String pageSize, int totalCount) {
        int totalPage = 0; //总页数
        int pageSizein = 0; //每页显示数量(整型)

        if (pageSize != null && !pageSize.equals("")){
            pageSizein = TypeTool.getInt(pageSize);
        }

        if (totalCount % pageSizein == 0) {
            totalPage = totalCount / pageSizein;
        } else {
            totalPage = totalCount / pageSizein + 1;
        }

        return totalPage;
    }
}
