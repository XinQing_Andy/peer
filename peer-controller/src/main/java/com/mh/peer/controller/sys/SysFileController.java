package com.mh.peer.controller.sys;

import com.mh.peer.model.business.SysFileBusinessBean;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.SysFileMessageBean;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zhangerxin on 2016-5-3.
 */
@Controller
@RequestMapping("/sysFile")
public class SysFileController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private SysFileService sysFileService;

    /**
     * 删除附件
     * @param sysFileMes
     * @return
     */
    @RequestMapping(value = "/delSysFile", method = RequestMethod.POST)
    @ResponseBody
    public SysFileBusinessBean onDelRole(@ModelAttribute SysFileMessageBean sysFileMes){

        String fileId = sysFileMes.getId(); //附件id
        String jdPath = sysFileMes.getImgurljd(); //绝对地址
        SysFile sysFile = new SysFile();
        sysFile.setId(fileId);
        sysFile.setImgurljd(jdPath);
        SysFileBusinessBean sysFileBus = sysFileService.deleteSysFile(sysFile);
        sysFileMes.setResult(sysFileBus.getResult());
        sysFileMes.setInfo(sysFileBus.getInfo());
        return sysFileBus;
    }
}
