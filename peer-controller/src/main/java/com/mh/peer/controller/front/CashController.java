package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.bank.BankService;
import com.mh.peer.service.front.CashService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.FrontPage;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpSession;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Cuibin on 2016/6/20.
 */
@Controller
@RequestMapping("/cash")
public class CashController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CashController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private BankService bankService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private CashService cashService;

    /**
     * 跳转到提现记录
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openCash", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openCash(ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        memberId = memberInfo.getId(); //会员id
        MemberBanknoMessageBean memberBanknoMes = new MemberBanknoMessageBean(); //会员银行卡信息
        memberBanknoMes.setMemberid(memberId); //会员id
        List<MemberBanknoMessageBean> listMemberBanknoMes = bankService.getMemberBankInfoBySome(memberBanknoMes);
        if (listMemberBanknoMes != null && !listMemberBanknoMes.equals("")){
            memberBanknoMes.setBankName(listMemberBanknoMes.get(0).getBankName()); //银行名称
            memberBanknoMes.setRealName(listMemberBanknoMes.get(0).getRealName()); //真实姓名
            memberBanknoMes.setBalance(listMemberBanknoMes.get(0).getBalance()); //余额
        }
        modelMap.addAttribute("memberBanknoMes",memberBanknoMes); //会员银行卡信息

        try {

            StringWriter stringWriter = null;
            //模板渲染
            stringWriter = jetbrickTool.getJetbrickTemp("/front/cash/cash.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 查询提现记录
     */
    @RequestMapping(value = "/queryPostalRecordByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryPostalRecordByPage(@ModelAttribute HuanXunPostalMessageBean huanXunPostalMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = huanXunPostalMes.getPageIndex(); //起始数
        String pageSize = huanXunPostalMes.getPageSize(); //每页显示数量
        String pageStr = ""; //分页部分拼接
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            bean.setInfo("会话已过期 请重新登录");
            bean.setResult("error");
            return bean;
        }

        if (pageIndex == null || pageIndex.equals("")) { //起始数
            pageIndex = "1";
            huanXunPostalMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "5";
            huanXunPostalMes.setPageSize(pageSize);
        }

        /**************** 查询记录 *****************/
        huanXunPostalMes.setMemberid(memberInfo.getId()); //会员id
        //提现记录集合
        List<HuanXunPostalMessageBean> listHuanXunPostalMes = cashService.queryPostalRecordByPage(huanXunPostalMes);
        int totalCount = cashService.queryPostalRecordByPageCount(huanXunPostalMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pagePostal","goPostalPage"); //提现记录部分分页
        /**************** 查询记录 *****************/

        modelMap.put("listHuanXunPostalMes", listHuanXunPostalMes); //提现记录集合
        modelMap.put("totalCount", totalCount); //提现记录数量
        modelMap.put("pageStr", pageStr); //分页部分

        try {

            StringWriter stringWriter = null;
            stringWriter = jetbrickTool.getJetbrickTemp(huanXunPostalMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 根据条件查询提现记录
     * @param huanXunPostalMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryPostalRecordBySome", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunPostalMessageBean queryPostalRecordBySome(@ModelAttribute HuanXunPostalMessageBean huanXunPostalMes, ModelMap modelMap) {
        String pageIndex = huanXunPostalMes.getPageIndex(); //起始数
        String pageSize = huanXunPostalMes.getPageSize(); //每页显示数量
        String str = ""; //提现列表部分拼接
        String pageStr = ""; //分页部分拼接
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            huanXunPostalMes.setInfo("会话已过期 请重新登录");
            huanXunPostalMes.setResult("error");
            return huanXunPostalMes;
        }

        if (pageIndex == null || pageIndex.equals("")) { //起始数
            pageIndex = "1";
            huanXunPostalMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "5";
            huanXunPostalMes.setPageSize(pageSize);
        }

        /**************** 查询记录 *****************/
        huanXunPostalMes.setMemberid(memberInfo.getId()); //会员id
        //提现记录集合
        List<HuanXunPostalMessageBean> listHuanXunPostalMes = cashService.queryPostalRecordByPage(huanXunPostalMes);
        int totalCount = cashService.queryPostalRecordByPageCount(huanXunPostalMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pagePostal","goPostalPage"); //提现记录部分分页
        /**************** 查询记录 *****************/

        if(listHuanXunPostalMes != null && listHuanXunPostalMes.size() > 0){
            for (HuanXunPostalMessageBean huanXunPostalMessageBean : listHuanXunPostalMes){
                str = str + "<tr>"
                          + "<td style=\"width:10%;\">"+huanXunPostalMessageBean.getSumFee()+"元</td>"
                          + "<td style=\"width:10%;\">"+huanXunPostalMessageBean.getMerfee()+"元</td>"
                          + "<td style=\"width:10%;\">"+huanXunPostalMessageBean.getIpsfee()+"元</td>"
                          + "<td style=\"width:10%;\">"+huanXunPostalMessageBean.getEndFee()+"元</td>"
                          + "<td style=\"width:10%;\">"+huanXunPostalMessageBean.getIpsdotime()+"</td>"
                          + "</tr>";
            }
        }

        huanXunPostalMes.setStr(str); //列表拼接
        huanXunPostalMes.setPageStr(pageStr); //分页部分拼接
        huanXunPostalMes.setResult("success");
        return huanXunPostalMes;
    }
}
