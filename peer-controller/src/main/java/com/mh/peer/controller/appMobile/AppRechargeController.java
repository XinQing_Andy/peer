package com.mh.peer.controller.appMobile;

import com.mh.peer.exception.HuanXunReChargeException;
import com.mh.peer.model.entity.HuanxunRecharge;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.service.huanxun.HuanXunRechargeService;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-9-20.
 */
@Controller
@RequestMapping("/appRecharge")
public class AppRechargeController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private HuanXunRegistService huanXunRegistService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private HuanXunRechargeService huanXunRechargeService;

    /**
     * 普通充值部分处理
     * @param request
     * @param response
     */
    @RequestMapping(value = "/normalRechargeHandle", method = RequestMethod.POST)
    @ResponseBody
    public void rechargeHandle(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String operationType = "trade.deposit"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String depositType = "1"; //充值类型 1、普通充值
        String merBillNo = "181006"; //商户订单号
        String merDate = ""; //充值日期
        String ipsAcctNo = ""; //IPS托管账户号
        String userType = "1"; //用户类型1、个人2、企业
        String trdAmt = request.getParameter("trdAmt"); //充值金额
        String ipsFeeType = "1"; //ips手续费承担方:1、平台商户2、平台用户
        String merFee = "0"; //平台收取用户的手续费
        String merFeeType = "1"; //平台手续费收取方式：1、内扣，2、外扣
        String s2SUrl = "http://120.76.137.147/appRecharge/normalResult?memberId="+memberId;
        String bankCard = ""; //银行卡号
        String resultCode = "0"; //结果编码(0-成功、1-失败)
        String resultMsg = ""; //错误信息
        String md5Zs = ""; //md5证书
        String glId = ""; //关联id
        HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean(); //环迅注册信息

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs(); //md5证书

            merBillNo = merBillNo + UUID.randomUUID().toString().replaceAll("-",""); //商户存管交易账号
            glId = UUID.randomUUID().toString().replaceAll("-",""); //关联id
            merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //充值日期
            s2SUrl = s2SUrl + "&glId="+glId; //后台通知地址

            if (memberId == null || memberId.equals("")){
                resultCode = "1";
                resultMsg = "您还未登录，请登录后重新进行充值操作！";
            }else{
                huanXunRegistMessageBean.setMemberId(memberId); //环迅注册信息
                List<HuanXunRegistMessageBean> listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
                if (listHuanXunRegistMes != null && listHuanXunRegistMes.size() > 0){
                    ipsAcctNo = listHuanXunRegistMes.get(0).getIpsAcctNo(); //IPS存管账户
                }else{
                    resultCode = "1";
                    resultMsg = "您还未注册环迅，请注册后重新进行充值操作！";
                }
            }

            HashMap<String,Object> in = null;
            in = new HashMap<String,Object>();
            in.put("merBillNo",merBillNo); //商户订单号
            in.put("merchantID",merchantID); //商户存管交易账号
            in.put("merDate",merDate); //充值日期
            in.put("depositType",depositType); //充值类型 1、普通充值
            in.put("ipsAcctNo",ipsAcctNo); //IPS托管账户号
            in.put("userType",userType); //用户类型  1、个人  2、企业
            in.put("trdAmt",trdAmt); //充值金额
            in.put("ipsFeeType",ipsFeeType); //ips手续费承担方:1、平台商户2、平台用户
            in.put("merFee",merFee); //平台收取用户的手续费
            in.put("merFeeType",merFeeType); //平台手续费收取方式：1、内扣，2、外扣
            in.put("s2SUrl",s2SUrl); //后台通知地址
            output(response, JSONObject.fromObject(in).toString());
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 普通充值结果
     * @param request
     * @param response
     * @param modelMap
     */
    @RequestMapping(value = "/normalResult", method = RequestMethod.POST)
    @ResponseBody
    public void normalResult(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
        String memberId = request.getParameter("memberId"); //会员id
        String glId = request.getParameter("glId"); //关联id
        String source = "1"; //来源(0-电脑端、1-手机端)
        String balance = ""; //余额(String型)
        Double balanceDou = 0.0d; //余额(Double型)
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String resultCode = request.getParameter("resultCode"); //响应码(000000-成功、999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //响应信息
        String sign = request.getParameter("sign"); //签名(未返回)
        String responsehx = request.getParameter("response"); //返回参数
        String responsehxJm = ""; //返回参数(解密)
        String merBillNo = ""; //商户订单号
        String depositType = ""; //充值类型：1、普通充值2、还款充值
        String channelType = ""; //充值渠道：1、个人网银2、企业网银
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String ipsTrdAmt = ""; //IPS实际到账金额(String型)
        Double ipsTrdAmtDou = 0.0d; //IPS实际到账金额(Double型)
        String ipsFee = ""; //IPS实际到账金额(String型)
        Double ipsFeeDou = 0.0d; //IPS手续费金额(Double型)
        String merFee = ""; //平台服务费(String型)
        Double merFeeDou = 0.0d; //平台手续费(Double型)
        String bankCode = ""; //银行编号
        String trdStatus = ""; //充值状态(0-失败、1-成功、2-处理中)
        String Uidrechargehx = ""; //充值部分主键
        String UidMyMessage = ""; //我的消息主键
        String UidTradeDetail = ""; //交易记录主键
        String title = ""; //我的消息部分题目
        String content = ""; //我的消息部分内容
        Map<String,Object> map = new HashMap<String,Object>();
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息

        try{

            if (responsehx != null && !responsehx.equals("")){ //返回信息
                responsehxJm = Pdes.decrypt3DES(responsehx); //响应信息(解密)
                if (responsehxJm != null && !responsehxJm.equals("")){
                    map = JsonHelper.getObjectToMap2(responsehxJm);
                    if (map != null){
                        if (map.get("merBillNo") != null && !map.get("merBillNo").equals("")){
                            merBillNo = (String) map.get("merBillNo"); //商户订单号
                        }
                        if (map.get("depositType") != null){
                            depositType = (String) map.get("depositType"); //充值类型(1-普通充值、2-还款充值)
                        }
                        if (map.get("channelType") != null && !map.get("channelType").equals("") && !map.get("channelType").equals("null")){
                            channelType = (String) map.get("channelType"); //渠道种类(1-个人网银、2-企业网银)
                        }
                        if (map.get("ipsBillNo") != null && !map.get("ipsBillNo").equals("")){
                            ipsBillNo = (String) map.get("ipsBillNo"); //充值的IPS订单号
                        }
                        if (map.get("ipsDoTime") != null && !map.get("ipsDoTime").equals("")){
                            ipsDoTime = (String) map.get("ipsDoTime"); //IPS处理时间
                        }
                        if (map.get("ipsTrdAmt") != null && !map.get("ipsTrdAmt").equals("")){
                            ipsTrdAmt = (String) map.get("ipsTrdAmt"); //用户IPS实际到账金额(String型)
                            if (ipsTrdAmt != null && !ipsTrdAmt.equals("")){
                                ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //用户IPS实际到账金额(Double型)
                            }
                        }
                        if (map.get("ipsFee") != null && !map.get("ipsFee").equals("")){
                            ipsFee = (String) map.get("ipsFee"); //IPS手续费金额(String型)
                            if (ipsFee != null && !ipsFee.equals("")){
                                ipsFeeDou = TypeTool.getDouble(ipsFee); //IPS手续费金额(Double型)
                            }
                        }
                        if (map.get("merFee") != null && !map.get("merFee").equals("")){
                            merFee = (String) map.get("merFee"); //平台手续费(String型)
                            if (merFee != null && !merFee.equals("")){
                                merFeeDou = TypeTool.getDouble(merFee); //平台手续费(Double型)
                            }
                        }
                        if (map.get("trdStatus") != null && !map.get("trdStatus").equals("")){
                            trdStatus = (String) map.get("trdStatus"); //充值状态
                        }

                        /*************** 环迅接口充值记录部分Start ***************/
                        HuanxunRecharge huanxunRecharge = new HuanxunRecharge();
                        Uidrechargehx = UUID.randomUUID().toString().replaceAll("-",""); //主键(环迅充值)
                        huanxunRecharge.setId(Uidrechargehx);
                        huanxunRecharge.setGlid(glId); //关联id
                        huanxunRecharge.setMemberid(memberId); //充值用户id
                        huanxunRecharge.setSource("0"); //来源(0-电脑端、1-手机端)
                        huanxunRecharge.setResultcode(resultCode); //返回编号(000000-成功、999999-失败)
                        huanxunRecharge.setResultmsg(resultMsg); //响应信息描述
                        huanxunRecharge.setMerchantid(merchantID); //商户存管交易账号
                        huanxunRecharge.setSign(sign); //签名
                        huanxunRecharge.setMerbillno(merBillNo); //商户订单号
                        huanxunRecharge.setDeposittype(depositType); //充值类型(1-普通充值、2-还款充值)
                        huanxunRecharge.setChanneltype(channelType); //渠道种类(1-个人网银、2-企业网银)
                        huanxunRecharge.setBankcode(bankCode); //银行编号
                        huanxunRecharge.setIpsbillno(ipsBillNo); //充值的IPS订单号
                        huanxunRecharge.setIpsdotime(ipsDoTime); //IPS处理时间
                        huanxunRecharge.setIpstrdamt(ipsTrdAmtDou); //用户IPS实际到账金额(Double型)
                        huanxunRecharge.setIpsfee(ipsFeeDou); //IPS手续费金额(Double型)
                        huanxunRecharge.setMerfee(merFeeDou); //平台手续费(Double型)
                        huanxunRecharge.setTrdstatus(trdStatus); //充值状态
                        huanxunRecharge.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                        /**************** 环迅接口充值记录部分End ****************/

                        MemberMessage memberMessage = new MemberMessage(); //我的消息部分
                        TradeHandle tradeHandle = new TradeHandle(); //流水号工具类
                        TradeDetail tradeDetail = new TradeDetail(); //交易记录

                        if(resultCode != null && !resultCode.equals("")) { //响应吗
                            if (resultCode.equals("000000")) { //成功
                                /********** 我的消息部分(Start) **********/
                                UidMyMessage = UUID.randomUUID().toString().replaceAll("-","");
                                title = "环迅充值成功";
                                content = "<li>商户订单号：<span>"+merBillNo+"</span></li>"
                                        + "<li>充值银行编号：<span>"+bankCode+"</span></li>"
                                        + "<li>充值银行编号：<span>"+bankCode+"</span></li>"
                                        + "<li>IPS订单号：<span>"+ipsBillNo+"</span></li>"
                                        + "<li>IPS处理时间：<span>"+ipsDoTime+"</span></li>"
                                        + "<li>实际到账金额：<span>"+ipsTrdAmtDou+"</span></li>";

                                memberMessage.setId(UidMyMessage); //主键
                                memberMessage.setGlid(Uidrechargehx); //关联id
                                memberMessage.setType("0"); //类型(0-系统消息)
                                memberMessage.setMemberid(memberId); //会员id
                                memberMessage.setReadflag("1"); //读取状态(0-已读、1-未读)
                                memberMessage.setTitle(title); //题目
                                memberMessage.setContent(content); //内容
                                memberMessage.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送日期
                                memberMessage.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建日期
                                memberMessage.setDelflg("1"); //删除标志(0-已删除、1-未删除)
                                /*********** 我的消息部分(End) ***********/

                                /********** 交易记录Start **********/
                                int serialNumber = tradeHandle.getSerialNumber(); //流水号
                                UidTradeDetail = UUID.randomUUID().toString().replaceAll("-",""); //交易记录主键
                                tradeDetail.setId(UidTradeDetail); //主键
                                tradeDetail.setGlid(Uidrechargehx); //关联id
                                tradeDetail.setSerialnum(serialNumber); //流水号
                                tradeDetail.setType("0"); //类型(0-充值)
                                tradeDetail.setSprice(ipsTrdAmtDou); //实际金额
                                tradeDetail.setProcedurefee(ipsFeeDou); //手续费
                                tradeDetail.setFlag("0"); //状态(0-成功、1-失败)
                                tradeDetail.setMemberid(memberId); //会员id
                                tradeDetail.setSource(source); //来源(0-电脑端、1-手机端)
                                tradeDetail.setTime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //交易时间
                                tradeDetail.setBz("环迅支付充值"); //备注
                                tradeDetail.setBalance(balanceDou); //余额
                                tradeDetail.setCreateuser(memberId); //创建人
                                tradeDetail.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                /*********** 交易记录End ***********/
                            }
                        }

                        mapException.put("memberId",memberId); //会员id
                        mapException.put("glId",glId); //关联id
                        mapException.put("source",source); //来源(0-电脑端、1-手机端)
                        mapException.put("resultCode",resultCode); //返回编号(000000-成功、999999-失败)
                        mapException.put("resultMsg",resultMsg); //返回信息
                        mapException.put("merchantID",merchantID); //商户存管交易账号
                        mapException.put("sign",sign); //签名
                        mapException.put("response",response); //响应信息

                        huanXunRechargeService.rechargehxResult(huanxunRecharge,tradeDetail,memberMessage); //环迅充值
                        System.out.println("环迅充值执行结束!");
                    }
                }
            }

        }catch(Exception e){
            HuanXunReChargeException.handle(mapException);
            e.printStackTrace();
        }
    }

    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
