package com.mh.peer.controller.dataStatistic;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.ProductType;
import com.mh.peer.model.message.ProductSaleMessageBean;
import com.mh.peer.model.message.ProductTypeMessage;
import com.mh.peer.service.dataStatistic.ProductSaleService;
import com.mh.peer.service.product.ProductTypeService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-9-18.
 * 借款标销售情况分析表
 */
@Controller
@RequestMapping("/productSale")
public class ProductSaleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductSaleController.class);
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private ProductSaleService productSaleService;
    @Autowired
    private ProductTypeService productTypeService;

    /**
     * 借款标销售情况分析
     */
    @RequestMapping(value = "/queryProductSaleTj", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryProductSaleTj(@ModelAttribute ProductSaleMessageBean productSaleMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        ProductTypeMessage productTypeMes = new ProductTypeMessage(); //产品类型
        List<ProductTypeMessage> listProductTypeMes = productTypeService.queryAllProductType(productTypeMes); //产品类型集合
        List<ProductSaleMessageBean> listProductSaleMes = new ArrayList<ProductSaleMessageBean>(); //借款标销售情况分析集合
        String productTypeId = ""; //产品类型id
        String grantCount = "0"; //已放款数量
        String loanPrice = "0"; //已结款总额
        String loanCount = "0"; //借款标数量
        String investMemberCount = "0"; //已投资会员数
        if (listProductTypeMes != null && listProductTypeMes.size() > 0){
            for (ProductTypeMessage productTypeMessage : listProductTypeMes){
                ProductSaleMessageBean productSaleMessageBean = new ProductSaleMessageBean(); //销售情况分析
                productTypeId = productTypeMessage.getId(); //产品类型id
                productSaleMessageBean.setTypeId(productTypeId);
                productSaleMessageBean.setTypeName(productTypeMessage.getTypeName()); //产品名称

                grantCount = productSaleService.getGrantPriceCount(productTypeId); //已放款数量
                productSaleMessageBean.setGrantCount(grantCount);

                loanPrice = productSaleService.getLoanPrice(productTypeId); //已借款总额
                productSaleMessageBean.setLoanPrice(loanPrice);

                loanCount = productSaleService.getLoanCount(productTypeId); //借款标数量
                productSaleMessageBean.setLoanCount(loanCount);

                investMemberCount = productSaleService.getInvestMemberCount(productTypeId);//已投资会员数
                productSaleMessageBean.setInvestMemberCount(investMemberCount);

                listProductSaleMes.add(productSaleMessageBean);
            }
        }

        modelMap.put("listProductSaleMes",listProductSaleMes);

        try {

            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productSaleMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }
}
