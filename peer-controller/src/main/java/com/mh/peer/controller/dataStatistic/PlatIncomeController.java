package com.mh.peer.controller.dataStatistic;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.PlatIncomeMessageBean;
import com.mh.peer.service.dataStatistic.PlatIncomeService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.TypeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by zhangerxin on 2016-9-18.
 * 平台收入统计表
 */
@Controller
@RequestMapping("/platIncome")
public class PlatIncomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductSaleController.class);
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private PlatIncomeService platIncomeService;


    /**
     * 平台收入管理统计查询
     * @param platIncomeMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryPlatIncomeTj", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryPlatIncomeTj(@ModelAttribute PlatIncomeMessageBean platIncomeMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String startTime  = platIncomeMes.getStartTime(); //开始时间
        String endTime = platIncomeMes.getEndTime(); //截止时间
        String currentTime = ""; //当前日期
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String orderType = platIncomeMes.getOrderType(); //排序类型
        String orderColumn = platIncomeMes.getOrderColumn(); //排序字段
        String pageIndex = platIncomeMes.getPageIndex(); //起始数
        String pageSize = platIncomeMes.getPageSize(); //每页显示数量
        String currentPage = platIncomeMes.getCurrentPage(); //当前页
        int pageCount = 0; //总页数
        currentTime = df.format(new Date()).toString();
        if (startTime == null || startTime.equals("") || endTime == null || endTime.equals("")){
            startTime = currentTime;
            endTime = currentTime;
            platIncomeMes.setStartTime(startTime);
            platIncomeMes.setEndTime(endTime);
            modelMap.put("startTime",startTime);
            modelMap.put("endTime",endTime);
        }
        if (orderType == null || orderType.equals("")){ //排序类型
            orderType = "DESC";
            platIncomeMes.setOrderType(orderType);
            modelMap.put("orderType",orderType); //排序类型
        }
        if (orderColumn == null || orderColumn.equals("")){ //排序字段
            orderColumn = "DATATIME";
            platIncomeMes.setOrderColumn(orderColumn);
            modelMap.put("orderColumn",orderColumn); //排序字段
        }
        if (pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
            currentPage = "1";
            platIncomeMes.setPageIndex(pageIndex);
            modelMap.put("pageIndex",pageIndex);
            modelMap.put("currentPage",currentPage);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            platIncomeMes.setPageSize(pageSize);
            modelMap.put("pageSize",pageSize);
        }
        //平台收入统计集合
        List<PlatIncomeMessageBean> listPlatIncomeMes = platIncomeService.getPlatIncomeList(platIncomeMes);
        //平台收入统计数量
        int totalCount = platIncomeService.getPlatIncomeCount(platIncomeMes);
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量

        modelMap.put("listPlatIncomeMes",listPlatIncomeMes); //平台收入统计集合
        modelMap.put("totalCount",totalCount); //平台收入统计数量
        modelMap.put("pageCount",pageCount); //总页数

        try {

            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(platIncomeMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 根据条件查询平台收入
     * @param platIncomeMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryPlatIncomeTjBySome", method = RequestMethod.POST)
    @ResponseBody
    public PlatIncomeMessageBean queryPlatIncomeTjBySome(@ModelAttribute PlatIncomeMessageBean platIncomeMes, ModelMap modelMap) {
        String startTime  = platIncomeMes.getStartTime(); //开始时间
        String endTime = platIncomeMes.getEndTime(); //截止时间
        String currentTime = ""; //当前日期
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String orderType = platIncomeMes.getOrderType(); //排序类型
        String orderColumn = platIncomeMes.getOrderColumn(); //排序字段
        String currentPage = platIncomeMes.getCurrentPage(); //当前页
        String pageIndex = platIncomeMes.getPageIndex(); //当前数
        String pageSize = platIncomeMes.getPageSize(); //每页显示数量
        currentTime = df.format(new Date()).toString();
        String platIncomeStr = ""; //平台收入拼接
        int num = 1; //序号
        int pageCount = 0; //总页数

        if (startTime == null || startTime.equals("") || endTime == null || endTime.equals("")){
            startTime = currentTime;
            endTime = currentTime;
        }
        if (orderType == null || orderType.equals("")){ //排序类型
            orderType = "DESC";
            platIncomeMes.setOrderType(orderType);
        }
        if (orderColumn == null || orderColumn.equals("")){ //排序字段
            orderColumn = "DATATIME";
            platIncomeMes.setOrderColumn(orderColumn);
        }
        if (pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
        }
        //平台收入统计集合
        List<PlatIncomeMessageBean> listPlatIncomeMes = platIncomeService.getPlatIncomeList(platIncomeMes);
        //平台收入统计数量
        int totalCount = platIncomeService.getPlatIncomeCount(platIncomeMes);
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量

        if (listPlatIncomeMes != null && listPlatIncomeMes.size() > 0){
            for (PlatIncomeMessageBean platIncomeMessageBean : listPlatIncomeMes){
                platIncomeStr = platIncomeStr + "<tr>"
                              + "<td>"+num+"</td>"
                              + "<td>"+platIncomeMessageBean.getPlatIncome()+"</td>"
                              + "<td>"+platIncomeMessageBean.getUnInvestChargeFee()+"</td>"
                              + "<td>"+platIncomeMessageBean.getReChargeMerFee()+"</td>"
                              + "<td>"+platIncomeMessageBean.getPostalMerFee()+"</td>"
                              + "<td>"+platIncomeMessageBean.getAttornMerFee()+"</td>"
                              + "<td>"+platIncomeMessageBean.getDataTime()+"</td>"
                              + "</tr>";
            }
        }

        platIncomeMes.setPlatStatisticStr(platIncomeStr); //列表拼接
        platIncomeMes.setTotalCount(TypeTool.getString(totalCount)); //数量
        platIncomeMes.setCurrentPage(currentPage); //当前页
        platIncomeMes.setPageCount(TypeTool.getString(pageCount)); //总页数

        return platIncomeMes;
    }


    /**
     * 获取总页数
     * @param pageSize
     * @param totalCount
     * @return
     */
    public int getPageCount(String pageSize, int totalCount) {
        int totalPage = 0; //总页数
        int pageSizein = 0; //每页显示数量(整型)

        if (pageSize != null && !pageSize.equals("")){
            pageSizein = TypeTool.getInt(pageSize);
        }

        if (totalCount % pageSizein == 0) {
            totalPage = totalCount / pageSizein;
        } else {
            totalPage = totalCount / pageSizein + 1;
        }

        return totalPage;
    }
}
