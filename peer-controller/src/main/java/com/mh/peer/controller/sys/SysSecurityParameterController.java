package com.mh.peer.controller.sys;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.business.SysSecurityParameterBusinessBean;
import com.mh.peer.model.entity.SysSecurityParameter;
import com.mh.peer.model.message.MenuBeanMessage;
import com.mh.peer.model.message.SysSecurityParameterMessageBean;
import com.mh.peer.model.message.SysUserMessageBean;
import com.mh.peer.service.sys.SysSecurityParameterService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.StringWriter;
import java.util.List;

/**
 * 安全问题参数
 * create by zhangerxin
 */
@Controller
@RequestMapping("/sysSecurityParameter")
public class SysSecurityParameterController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private SysSecurityParameterService sysSecurityParameterService;

    /**
     * 获取安全参数信息
     * @param sysSecurityParameterMes
     * @return
     */
    @RequestMapping(value = "/querySecurityParameter", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean querySecurityParameter(@ModelAttribute SysSecurityParameterMessageBean sysSecurityParameterMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        SysSecurityParameterMessageBean sysSecurityParameter = sysSecurityParameterService.querySecurityParameter(sysSecurityParameterMes);
        //将集合传递给前台渲染
        modelMap.put("sysSecurityParameter", sysSecurityParameter); //安全参数集合

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(sysSecurityParameterMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 设置安全参数
     * @param securityParameterMes
     * @return
     */
    @RequestMapping(value = "/setSecurityParameter", method = RequestMethod.POST)
    @ResponseBody
    public SysSecurityParameterMessageBean setSecurityParameter(@ModelAttribute SysSecurityParameterMessageBean securityParameterMes){
       /* HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        String userName= bean.getSysUser().getUserName();
        user.setCreateUserId(userName);*/
        String id = securityParameterMes.getId(); //主键
        SysSecurityParameterBusinessBean sysSecurityParameterBus = new SysSecurityParameterBusinessBean();
        if(id != null && !id.equals("")){ //修改
            sysSecurityParameterBus = sysSecurityParameterService.updateSysSecurityParameter(securityParameterMes);
        }else{ //新增
            sysSecurityParameterBus = sysSecurityParameterService.addSysSecurityParameter(securityParameterMes);
        }

        securityParameterMes.setResult(sysSecurityParameterBus.getResult());
        securityParameterMes.setInfo(sysSecurityParameterBus.getInfo());
        return securityParameterMes;
    }
}
