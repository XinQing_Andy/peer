package com.mh.peer.controller.huanxun;

import com.mh.peer.exception.HuanXunFreezeAttornException;
import com.mh.peer.exception.HuanXunUnFreezeAttornException;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.service.huanxun.HuanXunAttornService;
import com.mh.peer.service.huanxun.HuanXunFreezeService;
import com.mh.peer.service.huanxun.HuanXunInvestService;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.JsonHelper;
import com.mh.peer.util.Pdes;
import com.mh.peer.util.ServletUtil;
import com.mh.peer.util.TradeHandle;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import com.sun.deploy.net.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-8-1.
 * 转债权
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunAttornController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HuanXunAttornController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private HuanXunInvestService huanXunInvestService;
    @Autowired
    private HuanXunRegistService huanXunRegistService;
    @Autowired
    private HuanXunAttornService huanXunAttornService;
    @Autowired
    private HuanXunFreezeService huanXunFreezeService;



    /**
     * 债权返回结果
     * @param request
     * @param modelMap
     */
    @RequestMapping(value = "/attornhxResult", method = RequestMethod.POST)
    @ResponseBody
    public void attornhxResult(HttpServletRequest request,HttpServletResponse responsehx, ModelMap modelMap) {
        String resultCode = request.getParameter("resultCode"); //响应吗(000000-成功、999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //响应信息描述
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String response = request.getParameter("response"); //响应
        String responseJm = ""; //响应(解密)
        String merBillNo = ""; //商户订单号
        String projectNo = ""; //项目ID号
        String ipsTrdAmt = ""; //IPS冻结金额(String型)
        Double ipsTrdAmtDou = 0.0d; //IPS冻结金额(Double型)
        String ipsAcctNo = ""; //冻结的IPS存管账号
        String otherIpsAcctNo = ""; //它方账号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String trdStatus = ""; //冻结状态(0-失败、1-成功)
        String attornId = request.getParameter("attornId"); //债权转让主键
        String id = request.getParameter("id"); //主键
        String UidTradeDetail = ""; //主键(交易明细)
        String UidMyMessage = ""; //主键(我的消息)
        String memberId = ""; //会员id
        String balance = ""; //余额(String型)
        Double balanceDou = 0.0d; //余额(Double型)
        Map<String,String> map = new HashMap<String,String>();
        int serialnum = 0; //流水号
        String content = ""; //内容
        TradeDetail tradeDetail = new TradeDetail(); //交易记录
        MemberMessage memberMessage = new MemberMessage(); //我的消息
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息

        try{

            output(responsehx,"ipsCheckOk"); //返回成功

            mapException.put("attornId",attornId); //债权id
            mapException.put("resultCode",resultCode); //响应吗
            mapException.put("resultMsg",resultMsg); //响应信息描述
            mapException.put("response",response); //响应信息

            /*************** 获取环迅服务器信息Start ***************/
            if (response != null && !response.equals("")){
                responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                if (responseJm != null && !responseJm.equals("")){
                    map = JsonHelper.getObjectToMap(responseJm);
                    if (map != null){
                        merBillNo = map.get("merBillNo"); //商户订单号
                        projectNo = map.get("projectNo"); //项目ID号
                        ipsTrdAmt = map.get("ipsTrdAmt"); //IPS冻结金额(String型)
                        if (ipsTrdAmt != null && !ipsTrdAmt.equals("")){
                            ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt);
                        }
                        ipsAcctNo = map.get("ipsAcctNo"); //冻结的IPS存管账号
                        otherIpsAcctNo = map.get("otherIpsAcctNo"); //它方账号
                        ipsBillNo = map.get("ipsBillNo"); //IPS订单号
                        ipsDoTime = map.get("ipsDoTime"); //IPS处理时间
                        trdStatus = map.get("trdStatus"); //冻结状态(0-失败、1-成功)
                    }
                }
            }
            /**************** 获取环迅服务器信息End ****************/

            HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean(); //环迅注册用户
            huanXunRegistMessageBean.setIpsAcctNo(ipsAcctNo); //IPS虚拟账户
            List<HuanXunRegistMessageBean> listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
            if (listHuanXunRegistMes != null && listHuanXunRegistMes.size() > 0){
                memberId = listHuanXunRegistMes.get(0).getMemberId(); //会员id
            }
            MemberInfoMessageBean memberInfoMessageBean = allMemberService.queryMemberInfoById(memberId);
            if (memberInfoMessageBean != null){
                balance = memberInfoMessageBean.getBalance(); //余额(String型)
                if (balance != null && !balance.equals("")){
                    balanceDou = TypeTool.getDouble(balance); //余额(Double型)
                }
            }

            /******************** 债权转让资金冻结Start ********************/
            HuanxunFreezeAttorn huanxunFreezeAttorn = new HuanxunFreezeAttorn();
            huanxunFreezeAttorn.setId(id); //主键
            huanxunFreezeAttorn.setAttornid(attornId); //债权转让id
            huanxunFreezeAttorn.setMemberid(memberId); //会员id
            huanxunFreezeAttorn.setFreezetype("2"); //冻结类型(1-投标、2-债权转让、3-还款、4-分红、5-代偿、6-代偿还款、7-风险准备金、8-结算担保收益、9-红包)
            huanxunFreezeAttorn.setFlag("0"); //状态(0-冻结、1-解冻、2-转账)
            huanxunFreezeAttorn.setResultcode(resultCode); //响应吗(000000-成功、999999-失败)
            huanxunFreezeAttorn.setResultmsg(resultMsg); //响应信息
            huanxunFreezeAttorn.setMerchantid(merchantID); //商户存管交易账号
            huanxunFreezeAttorn.setSign(sign); //签名
            huanxunFreezeAttorn.setMerbillno(merBillNo); //商户订单号
            huanxunFreezeAttorn.setProjectno(projectNo); //项目ID号
            huanxunFreezeAttorn.setIpstrdamt(ipsTrdAmtDou); //IPS冻结金额
            huanxunFreezeAttorn.setOtheripsacctno(otherIpsAcctNo); //它方账号
            huanxunFreezeAttorn.setIpsacctno(ipsAcctNo); //冻结的IPS存管账号
            huanxunFreezeAttorn.setIpsbillno(ipsBillNo); //IPS订单号
            huanxunFreezeAttorn.setIpsdotime(ipsDoTime); //IPS处理时间
            huanxunFreezeAttorn.setTrdstatus(trdStatus); //冻结状态(0-失败、1-成功)
            huanxunFreezeAttorn.setDelflg("1"); //是否删除(0-删除、1-未删除)
            huanxunFreezeAttorn.setCreatetime(ipsDoTime); //创建时间
            /********************* 债权转让资金冻结End *********************/

            if(resultCode != null && !resultCode.equals("")){
                if (resultCode.equals("000000")){
                    /******************** 交易明细Start ********************/
                    UidTradeDetail = UUID.randomUUID().toString().replaceAll("-",""); //交易记录主键
                    TradeHandle tradeHandle = new TradeHandle(); //流水号工具类
                    int serialNumber = tradeHandle.getSerialNumber(); //流水号
                    tradeDetail.setId(UidTradeDetail); //主键
                    tradeDetail.setGlid(attornId); //关联id
                    tradeDetail.setSerialnum(serialNumber); //流水号
                    tradeDetail.setType("7"); //交易类型(债权)
                    tradeDetail.setSprice(ipsTrdAmtDou); //实际金额
                    tradeDetail.setProcedurefee(0d); //手续费
                    tradeDetail.setFlag("0"); //状态(0-成功、1-失败)
                    tradeDetail.setMemberid(memberId); //会员id
                    tradeDetail.setSource("0"); //来源(0-电脑端、1-手机端)
                    tradeDetail.setTime(ipsDoTime); //交易时间
                    tradeDetail.setBz("环迅债权转让"); //备注
                    balanceDou = balanceDou - ipsTrdAmtDou; //余额
                    tradeDetail.setBalance(balanceDou); //余额
                    tradeDetail.setCreateuser(memberId); //会员id
                    tradeDetail.setCreatetime(ipsDoTime); //创建时间
                    /********************* 交易明细End *********************/

                    /******************** 我的消息Start ********************/
                    UidMyMessage = UUID.randomUUID().toString().replaceAll("-",""); //我的消息主键
                    memberMessage.setId(UidMyMessage); //主键(我的消息)
                    memberMessage.setGlid(attornId); //关联id
                    memberMessage.setType("0"); //消息类型(0-系统消息)
                    memberMessage.setMemberid(memberId); //会员id
                    memberMessage.setReadflag("1"); //读取状态(0-已读、1-未读)
                    memberMessage.setTitle("债权转让承接成功！"); //题目
                    content = "<li>您有" + ipsTrdAmtDou + "元金额因债权承接被冻结，等待满标审核通过后自动转账给债权发起人</li>"
                            + "<li>项目ID号："+projectNo+"</li>"
                            + "<li>IPS冻结金额:"+ipsTrdAmtDou+"</li>"
                            + "<li>冻结的IPS存管账号:"+ipsAcctNo+"</li>"
                            + "<li>它方账号:"+otherIpsAcctNo+"</li>"
                            + "<li>IPS订单号:"+ipsBillNo+"</li>"
                            + "<li>IPS处理时间:"+ipsDoTime+"</li>";
                    memberMessage.setContent(content); //内容
                    memberMessage.setSendtime(ipsDoTime); //发送时间
                    memberMessage.setCreatetime(ipsDoTime); //创建时间
                    memberMessage.setDelflg("1"); //删除状态(0-已删除、2-未删除)
                    /********************* 我的消息End *********************/
                }
            }

            HuanXunFreezeAttornException.handle(mapException);
            huanXunAttornService.attornhxResult( mapException,huanxunFreezeAttorn,tradeDetail,memberMessage); //环迅债权转让

        }catch(Exception e){
            HuanXunFreezeAttornException.handle(mapException);
            e.printStackTrace();
        }
    }


    /**
     * 债权解冻返回结果
     * @param request
     * @param responsehx
     */
    @RequestMapping(value = "/unfreezeAttornResult", method = RequestMethod.POST)
    @ResponseBody
    public void unfreezeAttornResult(HttpServletRequest request,HttpServletResponse responsehx) {
        String resultCode = request.getParameter("resultCode"); //响应码(000000：成功、000001-999999：失败)
        String resultMsg = request.getParameter("resultMsg"); //响应信息描述
        String response = request.getParameter("response"); //响应信息
        String responseJm = ""; //响应信息(解密)
        String id = request.getParameter("id"); //冻结表主键
        String attornId = ""; //债权表主键
        String buyId = ""; //产品购买id
        String merBillNo = ""; //商户订单号
        String projectNo = ""; //项目ID号
        String freezeId = ""; //原IPS冻结订单号
        String merFee = ""; //解冻账号
        String ipsAcctNo = ""; //平台手续费
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String trdStatus = ""; //解冻状态
        String trdAmt = ""; //园东街金额
        String memberId = ""; //会员id
        String balance = ""; //会员余额(String型)
        Double balanceDou = 0.0d; //会员余额(Double型)
        Map<String,String> map = new HashMap<String,String>();
        int count = 0; //债权冻结数量
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息

        try{

            output(responsehx,"ipsCheckOk"); //接收成功

            count = huanXunFreezeService.queryFreezeAttornCount(id); //债权解冻数量

            if (count == 0){
                if (response != null && !response.equals("")){
                    responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                    if (responseJm != null && !responseJm.equals("")) {
                        map = JsonHelper.getObjectToMap(responseJm);
                        if (map != null) {
                            merBillNo = map.get("merBillNo"); //商户订单号
                            projectNo = map.get("projectNo"); //项目ID号
                            freezeId = map.get("freezeId"); //原IPS冻结订单号
                            merFee = map.get("merFee"); //平台手续费
                            ipsAcctNo = map.get("ipsAcctNo"); //解冻账号
                            ipsBillNo = map.get("ipsBillNo"); //IPS订单号
                            ipsDoTime = map.get("ipsDoTime"); //IPS处理时间
                            trdStatus = map.get("trdStatus"); //解冻状态
                        }
                    }
                }

                //查询债权转让冻结记录
                HuanXunFreezeMessageBean huanXunFreezeMessageBean = huanXunFreezeService.queryFreezeAttorn(id);
                if (huanXunFreezeMessageBean != null) {
                    trdAmt = huanXunFreezeMessageBean.getTrdAmt(); //原冻结金额
                    memberId = huanXunFreezeMessageBean.getMemberId(); //用户id
                    attornId = huanXunFreezeMessageBean.getAttornId(); //债权表主键
                    buyId = huanXunFreezeMessageBean.getBuyId(); //产品购买id

                    if (memberId != null && !memberId.equals("")){
                        balance = allMemberService.queryOnlyMemberBalance(memberId); //余额
                        if (balance != null && !balance.equals("")) {
                            balanceDou = TypeTool.getDouble(balance) + TypeTool.getDouble(trdAmt); //余额
                        }
                    }
                }

                /********** 会员信息Start **********/
                MemberInfo memberInfo = new MemberInfo();
                memberInfo.setBalance(balanceDou); //余额
                memberInfo.setId(memberId); //会员id
                /*********** 会员信息End ***********/

                /********** 冻结信息Start **********/
                HuanxunFreeze huanxunFreeze = new HuanxunFreeze();
                huanxunFreeze.setId(id); //冻结表主键
                huanxunFreeze.setFlag("1"); //类型
                huanxunFreeze.setMerbillno(merBillNo); //商户订单号
                /*********** 冻结信息End ***********/

                /********** 债权信息Start **********/
                ProductAttornRecord productAttornRecord = new ProductAttornRecord();
                productAttornRecord.setId(attornId); //债权主键
                productAttornRecord.setShflag("1"); //审核状态(0-审核通过、1-审核未通过、2-待审核)
                /*********** 债权信息End ***********/

                /********** 产品购买信息Start **********/
                ProductBuyInfo productBuyInfo = new ProductBuyInfo();
                productBuyInfo.setId(buyId); //产品购买id
                productBuyInfo.setType("0"); //类型(0-持有、1-转让中、2-已转让)
                /*********** 产品购买信息End ***********/

                /********** 异常信息部分Start **********/
                mapException.put("id",id); //冻结表主键
                mapException.put("resultCode",resultCode); //响应码
                mapException.put("resultMsg",resultMsg); //响应信息描述
                mapException.put("response",response); //响应信息
                /*********** 异常信息部分End ***********/

                huanXunAttornService.unfreezeAttornResult(mapException,memberInfo,huanxunFreeze,productAttornRecord,productBuyInfo);
            }
        }catch (Exception e){
            HuanXunUnFreezeAttornException.handle(mapException);
            e.printStackTrace();
        }
    }



    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
