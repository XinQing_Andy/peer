package com.mh.peer.controller.huanxun;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.huanxun.HuanXunFreezeService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.product.*;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by zhangerxin on 2016-7-28.
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunFreezeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HuanXunFreezeController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private HuanXunFreezeService huanXunFreezeService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductBuyService productBuyService;
    @Autowired
    private ProductAttornService productAttornService;
    @Autowired
    private ProductReceiveService productReceiveService;
    @Autowired
    private ProductRepayService productRepayService;

    /**
     * 冻结金额(投资)
     *
     * @param huanXunFreezeMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/freezehx", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunFreezeMessageBean freezehx(@ModelAttribute HuanXunFreezeMessageBean huanXunFreezeMes, ModelMap modelMap, HttpServletRequest requestcz) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String Uid = ""; //主键
        String operationType = huanXunFreezeMes.getOperationType(); //操作类型(trade.freeze)
        String merchantID = huanXunFreezeMes.getMerchantID(); //商户存管交易账号
        String merBillNo = huanXunFreezeMes.getMerBillNo(); //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //冻结日期
        String projectNo = huanXunFreezeMes.getProjectNo(); //项目ID号
        String bizType = huanXunFreezeMes.getBizType(); //业务类型
        String regType = huanXunFreezeMes.getRegType(); //登记方式(1-手动、2-自动)
        String contractNo = huanXunFreezeMes.getContractNo(); //合同号
        String authNo = huanXunFreezeMes.getAuthNo(); //授权号
        String trdAmt = huanXunFreezeMes.getTrdAmt(); //冻结金额
        String merFee = huanXunFreezeMes.getMerFee(); //平台手续费
        String freezeMerType = huanXunFreezeMes.getFreezeMerType(); //冻结方类型(1-用户、2-商户)
        String ipsAcctNo = huanXunFreezeMes.getIpsAcctNo(); //冻结账号
        String otherIpsAcctNo = huanXunFreezeMes.getOtherIpsAcctNo(); //它方账号
        String webUrl = huanXunFreezeMes.getWebUrl(); //页面返回地址
        String s2SUrl = huanXunFreezeMes.getS2SUrl(); //后台通知地址
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        String md5Zs = ""; //md5证书
        String captcha = huanXunFreezeMes.getCaptcha(); //验证码
        ProductMessageBean productMessageBean = new ProductMessageBean(); //产品部分
        String memberId = ""; //会员id
        String productId = huanXunFreezeMes.getProductId(); //产品id
        String applyMemberId = ""; //申请人
        String bidFee = ""; //最低投标金额(String型)
        Double bidFeeDou = 0.0d; //最低投标金额(Double型)
        Double buyPriceDou = 0.0d; //购买金额(Double型)
        String loadFee = ""; //贷款金额(String型)
        Double loadFeeDou = 0.0d; //贷款金额(Double型)
        String newShare = ""; //是否为新手专享(0-是、1-否)
        int totalCount = 0; //用于判断是否投资过
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean(); //会员部分
        ProductInvestMessageBean productInvestMes = new ProductInvestMessageBean(); //项目投资
        HttpSession session = requestcz.getSession();

        try {

            if (memberInfo == null) { //该会员未登录
                LOGGER.info("会话已过期请重新登陆");
                huanXunFreezeMes.setInfo("会话已过期请重新登陆");
                huanXunFreezeMes.setResult("error");
                return huanXunFreezeMes;
            }

            memberId = memberInfo.getId(); //会员id
            productMessageBean.setId(productId); //产品id
            productMessageBean = productService.queryProductById(productMessageBean); //根据id查询产品
            if (productMessageBean != null){
                applyMemberId = productMessageBean.getApplyMember(); //申请人id
                bidFee = productMessageBean.getBidfee(); //最低投标金额(String型)
                if (bidFee != null && !bidFee.equals("")){
                    bidFeeDou = TypeTool.getDouble(bidFee); //最低投标金额(Double型)
                }
                loadFee = productMessageBean.getLoadFee(); //贷款金额(String型)
                if (loadFee != null && !loadFee.equals("")){
                    loadFeeDou = TypeTool.getDouble(loadFee); //贷款金额(Double型)
                }
                newShare = productMessageBean.getType(); //是否为新手专享(0-是、1-否)
            }

            productInvestMes.setBuyer(memberId); //购买人
            if (newShare != null && !newShare.equals("")){
                if (newShare.equals("0")){
                    totalCount = productBuyService.getCountBySome(productInvestMes); //用于判断是否投资过
                    if (totalCount > 0){
                        LOGGER.info("该产品为新手专享，您不符合该条件！");
                        huanXunFreezeMes.setInfo("该产品为新手专享，您不符合该条件！");
                        huanXunFreezeMes.setResult("error");
                        return huanXunFreezeMes;
                    }
                }
            }

            if(memberId.equals(applyMemberId)){
                LOGGER.info("该产品由您发起，不允许对自己发起的产品进行投资！");
                huanXunFreezeMes.setInfo("该产品由您发起，不允许对自己发起的产品进行投资！");
                huanXunFreezeMes.setResult("error");
                return huanXunFreezeMes;
            }

            if (trdAmt != null && !trdAmt.equals("")){ //冻结金额
                buyPriceDou = TypeTool.getDouble(trdAmt);
            }

            productInvestMes.setProductId(productId); //产品id
            int count = productBuyService.getCountBySome(productInvestMes); //购买数量(用于判断当前登录人是否已购买)
            if(count > 0){ //已购买
                LOGGER.info("您已投资该产品，不能重复投资！");
                huanXunFreezeMes.setResult("error");
                huanXunFreezeMes.setInfo("您已投资该产品，不能重复投资！");
                return huanXunFreezeMes;
            }

            if (bidFeeDou > buyPriceDou) {
                huanXunFreezeMes.setResult("error");
                LOGGER.info("该产品最低投标金额为" + bidFeeDou + "元!");
                huanXunFreezeMes.setInfo("该产品最低投标金额为" + bidFeeDou + "元!");
                return huanXunFreezeMes;
            }

            if (loadFeeDou - buyPriceDou < bidFeeDou && loadFeeDou - buyPriceDou > 0){
                huanXunFreezeMes.setResult("error");
                LOGGER.info("剩余金额不足以再进行下次投标，请填写合法的金额!");
                huanXunFreezeMes.setInfo("剩余金额不足以再进行下次投标，请填写合法的金额!");
                return huanXunFreezeMes;
            }

            if (!captcha.equals(session.getAttribute("certCode"))) {
                LOGGER.info("验证码错误!");
                huanXunFreezeMes.setResult("error");
                huanXunFreezeMes.setInfo("验证码错误!");
                return huanXunFreezeMes;
            }

            Uid = UUID.randomUUID().toString().replaceAll("-", ""); //主键
            s2SUrl = s2SUrl + "&id=" + Uid; //后台通知地址

            merBillNo = merBillNo + UUID.randomUUID().toString().replaceAll("-", "");

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs();

            request = "{\"projectNo\":\"" + projectNo + "\",\"merBillNo\":\"" + merBillNo + "\",\"bizType\":\"" + bizType + "\"," +
                    "\"regType\":\"" + regType + "\",\"contractNo\":\"" + contractNo + "\",\"authNo\":\"" + authNo + "\",\"trdAmt\":\"" + trdAmt + "\"," +
                    "\"merFee\":\"0\",\"freezeMerType\":\"" + freezeMerType + "\",\"otherIpsAcctNo\":\"" + otherIpsAcctNo + "\"," +
                    "\"ipsAcctNo\":\"" + ipsAcctNo + "\",\"merDate\":\"" + merDate + "\",\"webUrl\":\"" + webUrl + "\",\"s2SUrl\":\"" + s2SUrl + "\"}";

            LOGGER.info("request======"+request);
            requestJm = Pdes.encrypt3DES(request); //3des加密
            sign = operationType + merchantID + requestJm + md5Zs; //签名
            signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

            huanXunFreezeMes.setSign(signJm);
            huanXunFreezeMes.setRequest(requestJm);
            huanXunFreezeMes.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return huanXunFreezeMes;
    }


    /**
     * 冻结金额(债权部分)
     *
     * @param huanXunFreezeMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/freezeAttornhx", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunFreezeMessageBean freezeAttornhx(@ModelAttribute HuanXunFreezeMessageBean huanXunFreezeMes, ModelMap modelMap, HttpServletRequest requestcz) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        String Uid = ""; //主键
        String operationType = huanXunFreezeMes.getOperationType(); //操作类型(trade.freeze)
        String merchantID = huanXunFreezeMes.getMerchantID(); //商户存管交易账号
        String merBillNo = huanXunFreezeMes.getMerBillNo(); //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //冻结日期
        String projectNo = huanXunFreezeMes.getProjectNo(); //项目ID号
        String bizType = huanXunFreezeMes.getBizType(); //业务类型
        String regType = huanXunFreezeMes.getRegType(); //登记方式(1-手动、2-自动)
        String contractNo = huanXunFreezeMes.getContractNo(); //合同号
        String authNo = huanXunFreezeMes.getAuthNo(); //授权号
        String trdAmt = huanXunFreezeMes.getTrdAmt(); //冻结金额
        String merFee = huanXunFreezeMes.getMerFee(); //平台手续费
        String freezeMerType = huanXunFreezeMes.getFreezeMerType(); //冻结方类型(1-用户、2-商户)
        String ipsAcctNo = huanXunFreezeMes.getIpsAcctNo(); //冻结账号
        String otherIpsAcctNo = huanXunFreezeMes.getOtherIpsAcctNo(); //它方账号
        String webUrl = huanXunFreezeMes.getWebUrl(); //页面返回地址
        String s2SUrl = huanXunFreezeMes.getS2SUrl(); //后台通知地址
        String attornId = huanXunFreezeMes.getAttornId(); //债权id
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        String md5Zs = ""; //md5证书
        String buyer = ""; //投资人
        String applyMemberId = ""; //借款申请人
        String captcha = ""; //验证码

        try {

            if (memberInfo == null) {
                huanXunFreezeMes.setResult("error");
                huanXunFreezeMes.setInfo("抱歉，请您先登录在进行操作！");
                return huanXunFreezeMes;
            }
            memberId = memberInfo.getId(); //会员id
            LOGGER.info("会员id======"+memberId);

            AttornProductMessageBean attornProductMes = new AttornProductMessageBean();
            attornProductMes.setId(attornId); //债权id
            attornProductMes = productAttornService.getAttornProductDetail(attornProductMes);
            if (attornProductMes != null){
                buyer = attornProductMes.getBuyer(); //投资人
            }
            LOGGER.info("投资人======"+buyer);

            if (memberId.equals(buyer)){ //投资人
                huanXunFreezeMes.setResult("error");
                huanXunFreezeMes.setInfo("该债权由您发起，不可以对自己发起的债权进行投资！");
                return huanXunFreezeMes;
            }

            if (memberId.equals(applyMemberId)){ //借款人
                huanXunFreezeMes.setResult("error");
                huanXunFreezeMes.setInfo("您是该产品借款人，不允许对自己的产品进行承接！");
                return huanXunFreezeMes;
            }
            LOGGER.info("借款人======"+buyer);

            HttpSession session = requestcz.getSession();
            if (captcha != null && !captcha.equals("")) { //验证码
                if (!captcha.equals((String) session.getAttribute("certCode"))) { //验证码不符
                    huanXunFreezeMes.setResult("error");
                    huanXunFreezeMes.setInfo("验证码错误！");
                    return huanXunFreezeMes;
                }
            }

            Uid = UUID.randomUUID().toString().replaceAll("-", ""); //主键
            s2SUrl = s2SUrl + "&id=" + Uid; //后台通知地址

            merBillNo = merBillNo + UUID.randomUUID().toString().replaceAll("-", "");

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs();

            request = "{\"projectNo\":\"" + projectNo + "\",\"merBillNo\":\"" + merBillNo + "\",\"bizType\":\"" + bizType + "\"," +
                    "\"regType\":\"" + regType + "\",\"contractNo\":\"" + contractNo + "\",\"authNo\":\"" + authNo + "\",\"trdAmt\":\"" + trdAmt + "\"," +
                    "\"merFee\":\"0\",\"freezeMerType\":\"" + freezeMerType + "\",\"otherIpsAcctNo\":\"" + otherIpsAcctNo + "\"," +
                    "\"ipsAcctNo\":\"" + ipsAcctNo + "\",\"merDate\":\"" + merDate + "\",\"webUrl\":\"" + webUrl + "\",\"s2SUrl\":\"" + s2SUrl + "\"}";

            System.out.println("request======"+request);
            requestJm = Pdes.encrypt3DES(request); //3des加密
            sign = operationType + merchantID + requestJm + md5Zs; //签名
            signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

            huanXunFreezeMes.setSign(signJm);
            huanXunFreezeMes.setRequest(requestJm);
            huanXunFreezeMes.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return huanXunFreezeMes;
    }


    /**
     * 解冻金额 by Cuibin，修改 by zhangerxin
     */
    @RequestMapping(value = "/unfreeze", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunFreezeMessageBean unfreeze(@ModelAttribute ProductMessageBean productMessageBean, ModelMap modelMap) throws Exception {
        HuanXunFreezeMessageBean huanXunFreezeMes = new HuanXunFreezeMessageBean();
        String productId = productMessageBean.getId(); //产品id
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //解冻日期
        String s2SUrl = ""; //后台地址
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm"; //请求地址
        String operationType = "trade.unFreeze";
        String merchantID = "1810060028";
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        String MD5ZS = ""; //md5证书
        String merBillNo = ""; //商户订单号
        String projectNo = "";//项目id
        String freezeId = ""; //原 IPS 冻结 订单号
        String ipsAcctNo = ""; //解冻账号
        String ipsAcctNoPj = ""; //解冻账号拼接
        String trdAmt = ""; //解冻金额
        String merFee = "0"; //手续费
        String id = ""; //冻结表id
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String bizType = "2"; //业务类型(1-流标、2-撤标、3-解冻保障金、4-解冻红包)
        String str = ""; //显示信息
        String resultCode = ""; //响应码
        String result = ""; //接口返回标志
        String resultBf = "0"; //解冻备份执行结果(0-成功、1-失败)
        String info = ""; //响应信息

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            MD5ZS = huanXunInfo.getMd5Zs(); //md5证书

            List<HuanXunFreezeMessageBean> listHuanXunFreezeMes = huanXunFreezeService.queryAllfreeze(productId); //产品集合
            if (listHuanXunFreezeMes != null && listHuanXunFreezeMes.size() > 0){
                for (HuanXunFreezeMessageBean huanXunFreezeMessageBean : listHuanXunFreezeMes) {
                    merBillNo = "181006" + UUID.randomUUID().toString().replaceAll("-", ""); //商户订单号
                    projectNo = huanXunFreezeMessageBean.getProjectNo();//项目id
                    freezeId = huanXunFreezeMessageBean.getIpsBillNo(); //原IPS冻结订单号
                    ipsAcctNo = huanXunFreezeMessageBean.getIpsAcctNo(); //解冻账号
                    trdAmt = huanXunFreezeMessageBean.getTrdAmt(); //解冻金额
                    id = huanXunFreezeMessageBean.getId(); //冻结主键
                    s2SUrl = "http://120.76.137.147/huanxun/unfreezeResult?id="+id;

                    request = "{\"merBillNo\":\""+merBillNo+"\",\"freezeId\":\""+freezeId+"\",\"bizType\":\""+bizType+"\"," +
                              "\"merDate\":\""+merDate+"\",\"projectNo\":\""+projectNo+"\",\"merFee\":\""+merFee+"\"," +
                              "\"ipsAcctNo\":\""+ipsAcctNo+"\",\"trdAmt\":\""+trdAmt+"\",\"s2SUrl\":\""+s2SUrl+"\"}";

                    System.out.println("request======"+request);
                    if (request != null && !request.equals("")){
                        requestJm = Pdes.encrypt3DES(request); //3des加密
                        sign = operationType + merchantID + requestJm + MD5ZS; //签名
                        signJm = MD5Util.MD5(sign).toLowerCase(); //签名(加密)
                        List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
                        HTTPParam httpParam1 = new HTTPParam("operationType", operationType); //操作类型
                        HTTPParam httpParam2 = new HTTPParam("merchantID", merchantID); //商户存管交易账号
                        HTTPParam httpParam3 = new HTTPParam("request", requestJm); //请求信息
                        HTTPParam httpParam4 = new HTTPParam("sign", signJm); //签名
                        listHTTParam.add(httpParam1);
                        listHTTParam.add(httpParam2);
                        listHTTParam.add(httpParam3);
                        listHTTParam.add(httpParam4);

                        /*************** 解冻前备份Start ***************/
                        Map<String,String> mapUnFreeze = new HashMap<String,String>(); //解冻信息
                        mapUnFreeze.put("huanxunFreezeId",id); //冻结表主键
                        mapUnFreeze.put("merBillNo",merBillNo); //商户订单号
                        mapUnFreeze.put("merDate",merDate); //解冻日期
                        mapUnFreeze.put("projectNo",projectNo); //项目ID号
                        mapUnFreeze.put("freezeId",freezeId); //原IPS冻结订单号
                        mapUnFreeze.put("bizType",bizType); //业务类型(1-流标、2-撤标、3-解冻保证金、4-解冻红包)
                        mapUnFreeze.put("merFee",merFee); //手续费
                        mapUnFreeze.put("ipsAcctNo",ipsAcctNo); //解冻账号
                        mapUnFreeze.put("trdAmt",trdAmt); //解冻金额
                        resultBf = UnFreezeBf.unFreezeResult(mapUnFreeze);
                        /**************** 解冻前备份End ****************/

                        if(resultBf != null && !resultBf.equals("")){ //解冻前备份
                            if (resultBf.equals("0")){
                                System.out.println("解冻订单号为"+freezeId+"的订单解冻成功！");
                                result = HttpRequest.sendPost(url,listHTTParam); //获取接口调用后返回信息
                                Map<String, String> map = JsonHelper.getObjectToMap(result); //将Json数据转换为Map格式
                                if (map != null) {
                                    resultCode = map.get("resultCode"); //响应状态
                                    if (resultCode != null && !resultCode.equals("")){
                                        if (!resultCode.equals("000000")){
                                            if (ipsAcctNoPj.equals("")){
                                                ipsAcctNoPj = ipsAcctNo; //解冻账号
                                            }else{
                                                ipsAcctNoPj = ipsAcctNoPj + "," + ipsAcctNo; //解冻账号拼接
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }

            if (!ipsAcctNoPj.equals("")){ //解冻失败的账号
                info = "解冻失败，失败账号为"+ipsAcctNoPj+"";
                huanXunFreezeMes.setResult("error");
                huanXunFreezeMes.setInfo(info);
            }else{
                info = "解冻成功";
                boolean b = huanXunFreezeService.updateProductFlag(productId); //更新产品审核状态
                huanXunFreezeMes.setResult("success");
                huanXunFreezeMes.setInfo(info);
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return huanXunFreezeMes;
    }


    /**
     * 解冻债权转让金额  by Cuibin  修改by zhangerxin
     */
    @RequestMapping(value = "/unfreezeAttorn", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunFreezeMessageBean unfreezeAttorn(@ModelAttribute HuanXunFreezeMessageBean huanXunFreezeMessageBean){
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //登记日期
        String s2SUrl = ""; //后台地址
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm"; //请求地址
        String operationType = "trade.unFreeze";
        String merchantID = "1810060028";
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String merBillNo = ""; //商户订单号
        String projectNo = "";//项目id
        String freezeId = ""; //原 IPS 冻结 订单号
        String ipsAcctNo = ""; //解冻账号
        String trdAmt = ""; //解冻金额
        String merFee = "0"; //平台手续费
        String bizType = "2"; //业务类型(1-流标、2-撤标、3-解冻保证金、4-解冻红包)
        String id = ""; //冻结表id
        String attornid = huanXunFreezeMessageBean.getId(); //债权id
        String resultCode = ""; //响应码
        String resultMsg = ""; //响应信息描述
        String result = ""; //环迅解冻接口返回标志
        String resultBf = ""; //环迅解冻备份返回标志

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            String MD5ZS = huanXunInfo.getMd5Zs(); //md5证书

            huanXunFreezeMessageBean = huanXunFreezeService.queryOnlyFreezeAttorn(attornid); //债权信息

            if (huanXunFreezeMessageBean != null) {
                projectNo = huanXunFreezeMessageBean.getProjectNo(); //项目ID号
                freezeId = huanXunFreezeMessageBean.getIpsBillNo(); //原IPS冻结订单号
                ipsAcctNo = huanXunFreezeMessageBean.getIpsAcctNo(); //解冻账号
                merBillNo = "181006" + UUID.randomUUID().toString().replaceAll("-",""); //商户订单号
                trdAmt = huanXunFreezeMessageBean.getTrdAmt(); //解冻金额
                id = huanXunFreezeMessageBean.getId(); //冻结表主键
            }

            s2SUrl = "http://120.76.137.147/huanxun/unfreezeAttornResult?id="+id; //后台地址
            //request = "{\"merBillNo\":\""+merBillNo+"\",\"merDate\":\""+merDate+"\",\"projectNo\":\""+projectNo+"\",\"freezeId\":\""+freezeId+"\",\"bizType\":\""+bizType+"\",\"merFee\":\""+merFee+"\",\"ipsAcctNo\":\""+ipsAcctNo+"\",\"trdAmt\":\""+trdAmt+"\",\"s2SUrl\":\""+s2SUrl+"\"}";

            request = "{\"merBillNo\":\""+merBillNo+"\",\"freezeId\":\""+freezeId+"\",\"bizType\":\""+bizType+"\"," +
                      "\"merDate\":\""+merDate+"\",\"projectNo\":\""+projectNo+"\",\"merFee\":\""+merFee+"\"," +
                      "\"ipsAcctNo\":\""+ipsAcctNo+"\",\"trdAmt\":\""+trdAmt+"\",\"s2SUrl\":\""+s2SUrl+"\"}";

            System.out.println("request======"+request);
            if (request != null && !request.equals("")){
                requestJm = Pdes.encrypt3DES(request); //请求信息(加密)
            }
            System.out.println("requestJm======"+requestJm);

            sign = operationType + merchantID + requestJm + MD5ZS; //签名

            if (sign != null && !sign.equals("")){
                signJm = MD5Util.MD5(sign).toLowerCase(); //签名(加密)
            }

            List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
            HTTPParam httpParam1 = new HTTPParam("operationType", operationType); //操作类型
            HTTPParam httpParam2 = new HTTPParam("merchantID", merchantID); //商户存管交易账号
            HTTPParam httpParam3 = new HTTPParam("request", requestJm); //请求信息
            HTTPParam httpParam4 = new HTTPParam("sign", signJm); //签名
            listHTTParam.add(httpParam1);
            listHTTParam.add(httpParam2);
            listHTTParam.add(httpParam3);
            listHTTParam.add(httpParam4);

            /*************** 解冻前备份Start ***************/
            Map<String,String> mapUnFreeze = new HashMap<String,String>(); //解冻信息
            mapUnFreeze.put("huanxunFreezeId",id); //冻结表主键
            mapUnFreeze.put("merBillNo",merBillNo); //商户订单号
            mapUnFreeze.put("merDate",merDate); //解冻日期
            mapUnFreeze.put("projectNo",projectNo); //项目ID号
            mapUnFreeze.put("freezeId",freezeId); //原IPS冻结订单号
            mapUnFreeze.put("bizType",bizType); //业务类型(1-流标、2-撤标、3-解冻保证金、4-解冻红包)
            mapUnFreeze.put("merFee",merFee); //手续费
            mapUnFreeze.put("ipsAcctNo",ipsAcctNo); //解冻账号
            mapUnFreeze.put("trdAmt",trdAmt); //解冻金额
            resultBf = UnFreezeBf.unFreezeResult(mapUnFreeze);
            /**************** 解冻前备份End ****************/

            if(resultBf != null && !resultBf.equals("")){
                if (resultBf.equals("0")){
                    result = HttpRequest.sendPost(url,listHTTParam); //获取接口调用后返回信息
                    Map<String, String> map = JsonHelper.getObjectToMap(result); //将Json数据转换为Map格式
                    if (map != null) {
                        resultCode = map.get("resultCode"); //响应状态
                        resultMsg = map.get("resultMsg"); //响应信息
                    }
                }
            }

            if (resultCode != null && !resultCode.equals("")){
                if (resultCode.equals("000000")){ //成功
                    huanXunFreezeMessageBean.setResult("success"); //程序返回状态
                    huanXunFreezeMessageBean.setInfo("操作成功！"); //程序返回信息
                }else{
                    huanXunFreezeMessageBean.setResult("error");
                    huanXunFreezeMessageBean.setInfo("操作失败！"+resultMsg);
                }
            }else{
                huanXunFreezeMessageBean.setResult("error");
                huanXunFreezeMessageBean.setInfo("操作失败！"+resultMsg);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return huanXunFreezeMessageBean;
    }


    /**
     * 自动还款部分冻结(根据操作时间)
     * @param operatime 操作时间
     * @return
     */
    public String freezeRepayhx(String operatime) {
        String productId = ""; //产品id
        String operationType = "trade.freeze"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        String merBillNo = "181006"; //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //登记日期
        String projectNo = ""; //项目ID号
        String bizType = "3"; //业务类型(1-投标、2-债权转让、3-还款)
        String regType = "2"; //登记方式(1-手动、2-自动)
        String contractNo = ""; //合同号
        String authNo = ""; //授权号
        String trdAmt = ""; //冻结金额(String型)
        Double trdAmtDou = 0.0d; //冻结金额(Double型)
        String merFee = "0"; //平台手续费
        String freezeMertype = "1"; //冻结方类型(1-用户、2-商户)
        String ipsAcctNo = ""; //冻结账号
        String otherIpsAcctNo = ""; //它方账号
        String WebUrl = ""; //页面返回地址
        String s2SUrl = ""; //后台通知地址
        String requestfs = ""; //请求信息
        String requestfsJm = ""; //请求信息(加密)
        String md5Zs = ""; //md5证书
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm"; //请求地址
        String result = ""; //获取接口调用后返回信息
        String resultCode = ""; //响应吗
        String resultMsg = ""; //响应信息

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs();

            ProductAutoRepayMessageBean productAutoRepayMessageBean = new ProductAutoRepayMessageBean();
            productAutoRepayMessageBean.setYreceiveTime(operatime); //应还款时间
            List<ProductAutoRepayMessageBean> listProductAutoRepayMes = productReceiveService.getReceiveInfoList(productAutoRepayMessageBean);
            System.out.println("还款日期为："+operatime+",该日期还款数量为："+listProductAutoRepayMes.size());

            if (listProductAutoRepayMes != null && listProductAutoRepayMes.size()>0){
                for (ProductAutoRepayMessageBean productAutoRepayMes : listProductAutoRepayMes){
                    merBillNo = "181006"; //商户订单号
                    merBillNo = merBillNo + UUID.randomUUID().toString().replace("-","");
                    projectNo = productAutoRepayMes.getProjectNo(); //项目id号
                    contractNo = productAutoRepayMes.getOutAutoSignBillNo(); //自动签约订单号
                    authNo = productAutoRepayMes.getOutAutoSignAuthNo(); //自动签约授权号
                    trdAmtDou = TypeTool.getDouble(productAutoRepayMes.getPrincipal()) + TypeTool.getDouble(productAutoRepayMes.getInterest()); //冻结金额
                    ipsAcctNo = productAutoRepayMes.getOutIpsAcctNo(); //冻结账号
                    otherIpsAcctNo = productAutoRepayMes.getInIpsAcctNo(); //它方账号
                    requestfs = "{"
                              + "\"merBillNo\":\""+merBillNo+"\","
                              + "\"merDate\":\""+merDate+"\","
                              + "\"projectNo\":\""+projectNo+"\","
                              + "\"bizType\":\""+bizType+"\","
                              + "\"regType\":\""+regType+"\","
                              + "\"contractNo\":\""+contractNo+"\","
                              + "\"authNo\":\""+authNo+"\","
                              + "\"trdAmt\":\""+trdAmtDou+"\","
                              + "\"merFee\":\""+merFee+"\","
                              + "\"freezeMertype\":\""+freezeMertype+"\","
                              + "\"ipsAcctNo\":\""+ipsAcctNo+"\","
                              + "\"otherIpsAcctNo\":\""+otherIpsAcctNo+"\","
                              + "\"WebUrl\":\""+WebUrl+"\","
                              + "\"s2SUrl\":\""+s2SUrl+"\""
                              + "}";

                    requestJm = Pdes.encrypt3DES(request); //3des加密
                    sign = operationType + merchantID + requestJm + md5Zs; //签名
                    signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

                    List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
                    HTTPParam httpParam1 = new HTTPParam("operationType",operationType); //操作类型
                    HTTPParam httpParam2 = new HTTPParam("merchantID",merchantID); //商户存管交易账号
                    HTTPParam httpParam3 = new HTTPParam("request",requestJm); //请求信息
                    HTTPParam httpParam4 = new HTTPParam("sign",signJm); //签名

                    listHTTParam.add(httpParam1);
                    listHTTParam.add(httpParam2);
                    listHTTParam.add(httpParam3);
                    listHTTParam.add(httpParam4);

                    result = HttpRequest.sendPost(url,listHTTParam); //获取接口调用后返回信息

                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 还款部分冻结(zhangerxin)
     * @param request
     * @param response
     */
    @RequestMapping(value = "/freezeRepay", method = RequestMethod.POST)
    @ResponseBody
    public void freezeRepay(HttpServletRequest request,HttpServletResponse response){
        System.out.println("********** 还款冻结Start **********");
        String repayId = request.getParameter("repayId"); //还款id
        String operationType = "trade.freeze"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String requesthx = ""; //请求信息
        String requesthxJm = ""; //请求信息(加密)
        String merBillNo = ""; //商户订单号
        String merDate = ""; //冻结日期
        String projectNo = ""; //项目ID号
        String bizType = "3"; //业务类型
        String regType = "1"; //登记方式(1-手动、2-自动)
        String contractNo = ""; //合同号
        String authNo = ""; //授权号
        String trdAmt = ""; //冻结金额
        String merFee = "0"; //平台手续费
        String freezeMerType = "1"; //冻结方类型(1-用户、2-商户)
        String ipsAcctNo = ""; //冻结账号
        String otherIpsAcctNo = ""; //它方账号
        String UidHuanXunRepay = UUID.randomUUID().toString().replaceAll("-",""); // 还款部分主键
        String webUrl = "http://jianbing88.com/front/registerhxWebResult"; //页面返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/repayPricehxResult"; //后台通知地址
        String md5Zs = ""; //md5证书
        String resultCode = "0"; //执行结果(0-成功、1-失败、2-未登录)
        String resultMsg = ""; //执行信息
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        Map<String,String> mapFreeze = new HashMap<String,String>();

        try{

            if (memberInfo == null){
                resultCode = "2";
                resultMsg = "您还未登录，请登陆后重新进行操作！";
            }else{
                memberId = memberInfo.getId(); //会员id
                merBillNo = "181006" + UUID.randomUUID().toString().replaceAll("-",""); //商户订单号
                merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //冻结日期

                webUrl = webUrl + "?memberId="+memberId+"&UidHuanXunRepay="+UidHuanXunRepay; //页面返回地址
                s2SUrl = s2SUrl + "?repayId="+repayId+"&UidHuanXunRepay="+UidHuanXunRepay; //后台通知地址

                HuanXunFreezeMessageBean huanXunFreezeMessageBean = productRepayService.getRepayInfoById(repayId); //还款冻结信息
                if (huanXunFreezeMessageBean != null){
                    projectNo = huanXunFreezeMessageBean.getProjectNo(); //项目ID号
                    trdAmt = huanXunFreezeMessageBean.getTrdAmt(); //冻结金额
                    ipsAcctNo = huanXunFreezeMessageBean.getIpsAcctNo(); //冻结账号
                    requesthx = "{"
                            + "\"merBillNo\":\""+merBillNo+"\","
                            + "\"merDate\":\""+merDate+"\","
                            + "\"projectNo\":\""+projectNo+"\","
                            + "\"bizType\":\""+bizType+"\","
                            + "\"regType\":\""+regType+"\","
                            + "\"contractNo\":\""+contractNo+"\","
                            + "\"authNo\":\""+authNo+"\","
                            + "\"trdAmt\":\""+trdAmt+"\","
                            + "\"merFee\":\""+merFee+"\","
                            + "\"freezeMerType\":\""+freezeMerType+"\","
                            + "\"ipsAcctNo\":\""+ipsAcctNo+"\","
                            + "\"otherIpsAcctNo\":\""+otherIpsAcctNo+"\","
                            + "\"webUrl\":\""+webUrl+"\","
                            + "\"s2SUrl\":\""+s2SUrl+"\""
                            + "}";

                    HuanXunInfo huanXunInfo = new HuanXunInfo();
                    md5Zs = huanXunInfo.getMd5Zs();

                    requesthxJm = Pdes.encrypt3DES(requesthx); //3des加密
                    sign = operationType + merchantID + requesthxJm + md5Zs; //签名
                    signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

                    mapFreeze.put("merBillNo",merBillNo); //商户订单号
                    mapFreeze.put("merDate",merDate); //冻结日期
                    mapFreeze.put("projectNo",projectNo); //项目ID号
                    mapFreeze.put("bizType",bizType); //业务类型
                    mapFreeze.put("regType",regType); //登记方式(1-手动、2-自动)
                    mapFreeze.put("contractNo",contractNo); //合同号
                    mapFreeze.put("authNo",authNo); //授权号
                    mapFreeze.put("trdAmt",trdAmt); //冻结金额
                    mapFreeze.put("merFee",merFee); //平台手续费
                    mapFreeze.put("freezeMerType",freezeMerType); //冻结方类型(1-用户、2-商户)
                    mapFreeze.put("ipsAcctNo",ipsAcctNo); //冻结账号
                    mapFreeze.put("otherIpsAcctNo",otherIpsAcctNo); //它方账号
                    mapFreeze.put("memberId",memberId); //会员id
                    resultCode = FreezeBf.freezeResult(mapFreeze);
                    System.out.println("resultCode======"+resultCode);
                }
            }
        }catch(Exception e){
            resultCode = "1";
            resultMsg = e.getMessage();
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("resultCode",resultCode);
        in.put("resultMsg",resultMsg);
        in.put("request",requesthxJm); //请求加密信息
        in.put("sign",signJm); //签名加密信息
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
