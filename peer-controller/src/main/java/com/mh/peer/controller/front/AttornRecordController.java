package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.ProductMyAttornBusinessBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.front.ReceiveService;
import com.mh.peer.service.huanxun.HuanXunFreezeService;
import com.mh.peer.service.huanxun.HuanXunRegProjectService;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.product.ProductAttornService;
import com.mh.peer.service.product.ProductBuyService;
import com.mh.peer.service.product.ProductTypeService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.*;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhangerxin on 2016-5-23.
 * 债权转让
 */
@Controller
@RequestMapping("/attornRecord")
public class AttornRecordController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ProductBuyService productBuyService;
    @Autowired
    private ProductAttornService productAttornService;
    @Autowired
    private ProductTypeService productTypeService;
    @Autowired
    private SysFileService sysFileService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private HuanXunFreezeService huanXunFreezeService;
    @Autowired
    private HuanXunRegistService huanXunRegistService;
    @Autowired
    private HuanXunRegProjectService huanXunRegProjectService;
    @Autowired
    private ReceiveService receiveService;


    /**
     * 获取可转让的债权
     * @param productInvestMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttornFq", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAttornFq(@ModelAttribute ProductInvestMessageBean productInvestMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        //从session中获取会员登录信息
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String pageIndex = productInvestMes.getPageIndex(); //当前页
        String pageSize = productInvestMes.getPageSize(); //每页显示数量
        String orderColumn = productInvestMes.getOrderColumn(); //排序字段
        String orderType = productInvestMes.getOrderType(); //排序类型
        String type = productInvestMes.getType(); //类型 0-持有 1-转让中 2-已转让
        String buyerId = ""; //购买人id
        String repayment = ""; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        String repaymentName = ""; //还款方式名称
        String notAttornStr = ""; //可转让的债权拼接
        String pageStr = ""; //分页部分内容

        if (memberInfo != null) {
            buyerId = memberInfo.getId(); //购买人id
            productInvestMes.setBuyer(buyerId);
        }
        if (pageIndex == null || pageIndex.equals("")) { //当前页
            pageIndex = "1";
            productInvestMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "3";
            productInvestMes.setPageSize(pageSize);
        }

        //产品投资集合
        List<ProductInvestMessageBean> listProductInvestMes = productAttornService.getAttornList(productInvestMes);
        //可转让债权数量
        int totalCount = productAttornService.getAttornListCount(productInvestMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageNotAttorn","goNotAttornPage"); //分页部分拼接

        if (listProductInvestMes != null && listProductInvestMes.size() > 0) {
            /*************** 可转让的债权循环Start ***************/
            for (ProductInvestMessageBean productInvestMessageBean : listProductInvestMes) {
                //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
                repayment = productInvestMessageBean.getRepayment();
                if (repayment != null && !repayment.equals("")) {
                    if (repayment.equals("0")) {
                        repaymentName = "按月还款、等额本息";
                    } else if (repayment.equals("1")) {
                        repaymentName = "按月付息、到期还本";
                    } else if (repayment.equals("2")) {
                        repaymentName = "一次性还款";
                    }
                }

                notAttornStr = notAttornStr + "<tr class=\"t_txt\">"
                             + "<td>"+productInvestMessageBean.getTitle()+"</td>"
                             + "<td>"+productInvestMessageBean.getCode()+"</td>"
                             + "<td>"+productInvestMessageBean.getLoanrate()+"%</td>"
                             + "<td>"+repaymentName+"</td>"
                             + "<td>"+productInvestMessageBean.getWaitPrice()+"元</td>"
                             + "<td>"+productInvestMessageBean.getEndTime()+"</td>"
                             + "<td>"+productInvestMessageBean.getReceiveFlag()+"</td>"
                             + "<td class=\"caozuo\">"
                             + "<a href=\"JavaScript:void(0)\" onclick=\"doQueryInvestProductDetails('"+productInvestMessageBean.getId()+"','"+productInvestMessageBean.getProductId()+"','attorn')\">详情</a>&nbsp;";

                if (productInvestMessageBean.getType().equals("0")) {  //审核状态(0-审核通过、1-审核不通过、2-待审核)
                    notAttornStr = notAttornStr + "<a href=\"JavaScript:void(0)\" onclick=\"dabetattornOper('"+productInvestMessageBean.getId()+"')\">债权转让</a>";
                }
            }
            /**************** 可转让的债权循环End ****************/
        }

        modelMap.put("notAttornStr",notAttornStr); //可发起的债权
        modelMap.put("pageStr",pageStr); //分页部分

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productInvestMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 查询所有可转让的债权
     *
     * @param productInvestMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryNotAttornByPage", method = RequestMethod.POST)
    @ResponseBody
    public ProductInvestMessageBean queryInvestByPage(@ModelAttribute ProductInvestMessageBean productInvestMes, ModelMap modelMap) {
        //从session中获取会员登录信息
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String pageIndex = productInvestMes.getPageIndex(); //当前页
        String pageSize = productInvestMes.getPageSize(); //每页显示数量
        String orderColumn = productInvestMes.getOrderColumn(); //排序字段
        String orderType = productInvestMes.getOrderType(); //排序类型
        String type = productInvestMes.getType(); //类型 0-持有 1-转让中 2-已转让
        String buyerId = ""; //购买人id
        String repayment = ""; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        String repaymentName = ""; //还款方式名称
        String notAttornStr = ""; //可转让的债权拼接
        String pageStr = ""; //分页部分内容

        if (memberInfo != null) {
            buyerId = memberInfo.getId(); //购买人id
            productInvestMes.setBuyer(buyerId);
        }
        if (pageIndex == null || pageIndex.equals("")) { //当前页
            pageIndex = "1";
            productInvestMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "3";
            productInvestMes.setPageSize(pageSize);
        }

        //产品投资集合
        List<ProductInvestMessageBean> listProductInvestMes = productAttornService.getAttornList(productInvestMes);
        //可转让债权数量
        int totalCount = productAttornService.getAttornListCount(productInvestMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageNotAttorn","goNotAttornPage"); //分页部分拼接

        if (listProductInvestMes != null && listProductInvestMes.size() > 0) {
            /*************** 可转让的债权循环Start ***************/
            for (ProductInvestMessageBean productInvestMessageBean : listProductInvestMes) {
                //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
                repayment = productInvestMessageBean.getRepayment();
                if (repayment != null && !repayment.equals("")) {
                    if (repayment.equals("0")) {
                        repaymentName = "按月还款、等额本息";
                    } else if (repayment.equals("1")) {
                        repaymentName = "按月付息、到期还本";
                    } else if (repayment.equals("2")) {
                        repaymentName = "一次性还款";
                    }
                }

                notAttornStr = notAttornStr + "<tr class=\"t_txt\">"
                             + "<td>"+productInvestMessageBean.getTitle()+"</td>"
                             + "<td>"+productInvestMessageBean.getCode()+"</td>"
                             + "<td>"+productInvestMessageBean.getLoanrate()+"%</td>"
                             + "<td>"+repaymentName+"</td>"
                             + "<td>"+productInvestMessageBean.getWaitPrice()+"元</td>"
                             + "<td>"+productInvestMessageBean.getEndTime()+"</td>"
                             + "<td>"+productInvestMessageBean.getReceiveFlag()+"</td>"
                             + "<td class=\"caozuo\">"
                             + "<a href=\"JavaScript:void(0)\" onclick=\"doQueryProductDetails('" + productInvestMessageBean.getId() + "','"+productInvestMessageBean.getProductId()+"')\">详情</a>&nbsp;";

                if (productInvestMessageBean.getType().equals("0")) {  //审核状态(0-审核通过、1-审核不通过、2-待审核)
                    notAttornStr = notAttornStr + "<a href=\"JavaScript:void(0)\" onclick=\"dabetattornOper('"+productInvestMessageBean.getId()+"')\">债权转让</a>";
                }

            }
            /**************** 可转让的债权循环End ****************/
        }

        productInvestMes.setStr(notAttornStr); //查询列表
        productInvestMes.setStr2(pageStr); //分页部分
        return productInvestMes;
    }


    /**
     * 转让中债权  by cuibin(重做 by zhangerxin)
     * @param productAttornRecordMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryTransferAttornByPage", method = RequestMethod.POST)
    @ResponseBody
    public ProductAttornRecordMessageBean queryTransferAttornByPage(@ModelAttribute ProductAttornRecordMessageBean productAttornRecordMes, ModelMap modelMap) {
        //从session中获取会员登录信息
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String pageIndex = productAttornRecordMes.getPageIndex(); //当前页
        String pageSize = productAttornRecordMes.getPageSize(); //每页显示数量
        String orderColumn = productAttornRecordMes.getOrderColumn(); //排序字段
        String orderType = productAttornRecordMes.getOrderType(); //排序类型
        String memberId = ""; //会员id
        String str = ""; //列表内容拼接
        String pageStr = ""; //分页部分拼接

        if (memberInfo != null) {
            memberId = memberInfo.getId(); //投资人id
            productAttornRecordMes.setBuyer(memberId);
        }
        if (pageIndex == null || pageIndex.equals("")) { //当前页
            pageIndex = "1";
            productAttornRecordMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "3";
            productAttornRecordMes.setPageSize(pageSize);
        }

        //转让中的债权集合
        List<ProductAttornRecordMessageBean> listProductInvestMes = productAttornService.queryMyAttornByPage(productAttornRecordMes);
        //转让中的债权数量
        int totalCount = productAttornService.queryMyAttornCount(productAttornRecordMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageTransferAttorn","goTransferAttorn"); //分页部分拼接

        if (listProductInvestMes != null && listProductInvestMes.size() > 0) {
            for (ProductAttornRecordMessageBean productAttornRecordMessageBean : listProductInvestMes) {
                str = str + "<tr class=\"t_txt\">"
                        + "<td>"+productAttornRecordMessageBean.getTitle()+"</td>"
                        + "<td>"+productAttornRecordMessageBean.getCode()+"</td>"
                        + "<td>"+productAttornRecordMessageBean.getLoanrate()+"%</td>"
                        + "<td>"+productAttornRecordMessageBean.getWaitPrice()+"元</td>"
                        + "<td>"+productAttornRecordMessageBean.getPrice()+"元</td>"
                        + "<td>"+productAttornRecordMessageBean.getCreateTime()+"</td>"
                        + "<td class=\"caozuo\">"
                        + "<a href=\"JavaScript:void(0)\" onclick=\"doQueryProductDetails('"+productAttornRecordMessageBean.getId()+"','"+productAttornRecordMessageBean.getProductbuyId()+"','"+productAttornRecordMessageBean.getProductid()+"')\">详情</a>"
                        + "</td>"
                        + "</tr>";
            }
        }
        productAttornRecordMes.setStr(str); //列表部分拼接
        productAttornRecordMes.setPageStr(pageStr); //分页部分拼接
        return productAttornRecordMes;
    }


    /**
     * 已购买债权部分列表(重做 by zhangerxin)
     */
    @RequestMapping(value = "/queryInvestedAttornByPage", method = RequestMethod.POST)
    @ResponseBody
    public ProductAttornRecordMessageBean queryInvestedAttornByPage(@ModelAttribute ProductAttornRecordMessageBean productAttornRecordMes,ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        String pageIndex = productAttornRecordMes.getPageIndex(); //起始数
        String pageSize = productAttornRecordMes.getPageSize(); //每页显示数量
        String orderColumn = productAttornRecordMes.getOrderColumn(); //排序字段
        String orderType = productAttornRecordMes.getOrderType(); //排序类型
        String str = ""; //列表部分拼接
        String pageStr = ""; //分页部分拼接

        if (memberInfo != null){
            memberId = memberInfo.getId(); //会员id
            productAttornRecordMes.setUndertakeMember(memberId);
        }
        if (pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
            productAttornRecordMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            productAttornRecordMes.setPageSize(pageSize);
        }

        //已购买的债权集合
        List<ProductAttornRecordMessageBean> listAttornRecordMes = productAttornService.queryMyAttornByPage(productAttornRecordMes);
        //已购买的债权数量
        int totalCount = productAttornService.queryMyAttornCount(productAttornRecordMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageInvestedAttorn","goInvestedAttornPage");

        if (listAttornRecordMes != null && listAttornRecordMes.size() > 0){
            for (ProductAttornRecordMessageBean productAttornRecordMessageBean : listAttornRecordMes){
                str = str + "<tr class=\"t_txt\">"
                          + "<td>"+productAttornRecordMessageBean.getTitle()+"</td>"
                          + "<td>"+productAttornRecordMessageBean.getCode()+"</td>"
                          + "<td>"+productAttornRecordMessageBean.getLoanrate()+"%</td>"
                          + "<td>"+productAttornRecordMessageBean.getWaitPrice()+"元</td>"
                          + "<td>"+productAttornRecordMessageBean.getPrice()+"元</td>"
                          + "<td>"+productAttornRecordMessageBean.getUndertakeTime()+"</td>"
                          + "<td>"
                          + "</td>"
                          + "</tr>";
            }
        }

        productAttornRecordMes.setStr(str); //列表部分拼接
        productAttornRecordMes.setPageStr(pageStr); //分页部分拼接
        return productAttornRecordMes;
    }


    /**
     * 已转让债权(重做by zhangerxin)
     * @param productAttornRecordMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAlreadyTransferAttorn", method = RequestMethod.POST)
    @ResponseBody
    public ProductAttornRecordMessageBean queryAlreadyTransferAttorn(@ModelAttribute ProductAttornRecordMessageBean productAttornRecordMes, ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        String pageIndex = productAttornRecordMes.getPageIndex(); //当前页
        String pageSize = productAttornRecordMes.getPageSize(); //每页显示数量
        String orderColumn = productAttornRecordMes.getOrderColumn(); //排序字段
        String orderType = productAttornRecordMes.getOrderType(); //排序类型
        String str = ""; //内容拼接
        String pageStr = ""; //分页部分拼接

        if (memberInfo != null) {
            memberId = memberInfo.getId();
            productAttornRecordMes.setBuyer(memberId); //购买人id
        }
        if (pageIndex == null || pageIndex.equals("")) { //当前页
            pageIndex = "1";
            productAttornRecordMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "3";
            productAttornRecordMes.setPageSize(pageSize);
        }
        if (orderColumn == null || orderColumn.equals("")){ //排序字段
            orderColumn = "UNDERTAKETIME";
            productAttornRecordMes.setOrderColumn(orderColumn);
        }
        if (orderType == null || orderType.equals("")){ //排序类型
            orderType = "ASC";
            productAttornRecordMes.setOrderType(orderType);
        }

        //已转让债权集合
        List<ProductAttornRecordMessageBean> listProductAttornRecordMes = productAttornService.queryMyAttornByPage(productAttornRecordMes);
        //已转让债权数量
        int totalCount = productAttornService.queryMyAttornCount(productAttornRecordMes);
        FrontPage frontPage = new FrontPage();
        //分页部分拼接
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageAlreadyTransferAttorn","goAlreadyTransferAttornPage");

        if (listProductAttornRecordMes != null && listProductAttornRecordMes.size() > 0) {
            for (ProductAttornRecordMessageBean productAttornRecordMessageBean : listProductAttornRecordMes) {
                System.out.println(productAttornRecordMessageBean.getWaitprice());
                str = str + "<tr class=\"t_txt\">"
                          + "<td>"+productAttornRecordMessageBean.getTitle()+"</td>"
                          + "<td>"+productAttornRecordMessageBean.getCode()+"</td>"
                          + "<td>"+productAttornRecordMessageBean.getLoanrate()+"%</td>"
                          + "<td>"+productAttornRecordMessageBean.getWaitPrice()+"元</td>"
                          + "<td>"+productAttornRecordMessageBean.getPrice()+"元</td>"
                          + "<td>"+productAttornRecordMessageBean.getUndertakeTime()+"</td>"
                          + "<td>"
                          + "<a href=\"JavaScript:void(0)\" onclick=\"doQueryProductDetails('"+productAttornRecordMessageBean.getId()+"','"+productAttornRecordMessageBean.getProductid()+"')\">详情</a>"
                          + "</td>"
                          + "</tr>";
            }
        }
        productAttornRecordMes.setStr(str); //列表部分
        productAttornRecordMes.setPageStr(pageStr); //分页部分
        return productAttornRecordMes;
    }



    /**
     * 获取债权详细内容
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/doQueryAttornProductDetails", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean doQueryAttornProductDetails(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap){
        String attornId = request.getParameter("id"); //债权id
        String investId = request.getParameter("investId"); //购买id
        String productId = request.getParameter("productId"); //产品id
        String contractType = request.getParameter("contractType"); //合同类型(1-投资、2-债权)
        String yreceivePrice = ""; //已回款金额
        String wreceivePrice = ""; //未还款金额
        Double receiveProgress = 0.0d; //投资进度
        String shFlag = ""; //审核状态(0-审核通过、1-审核未通过、2-未审核)
        String investFlag = ""; //投资状态
        String pageStr = ""; //分页部分拼接
        String pageIndex = request.getParameter("pageIndex"); //起始数
        String pageSize = request.getParameter("pageSize"); //每页显示数量

        /******************** 产品信息部分Start ********************/
        ProductInfoMessageBean productInfoMessageBean = productBuyService.queryProductDetails(productId);
        modelMap.put("productInfoMessageBean",productInfoMessageBean);
        /********************* 产品信息部分End *********************/

        /******************** 已收回款信息Start ********************/
        ProductReceiveInfoMessageBean productReceiveInfoYsMessageBean = productBuyService.queryYReceiveInfo(investId);
        modelMap.put("productReceiveInfoYsMessageBean",productReceiveInfoYsMessageBean);
        /********************* 已收回款信息End *********************/

        /******************** 未收回款信息Start ********************/
        ProductReceiveInfoMessageBean productReceiveInfoWsMessageBean = productBuyService.queryWReceiveInfo(investId);
        modelMap.put("productReceiveInfoWsMessageBean",productReceiveInfoWsMessageBean);
        /********************* 未收回款信息End *********************/

        yreceivePrice = productReceiveInfoYsMessageBean.getYreceivementPrice(); //已回款金额部分
        wreceivePrice = productReceiveInfoWsMessageBean.getYreceivementPrice(); //未还款金额部分
        if ((TypeTool.getDouble(yreceivePrice) + TypeTool.getDouble(wreceivePrice)) != 0.0d){
            receiveProgress = TypeTool.getDouble(yreceivePrice)/(TypeTool.getDouble(yreceivePrice) + TypeTool.getDouble(wreceivePrice)) * 100;
        }

        modelMap.put("receiveProgress",receiveProgress);

        if (pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "5";
        }

        //还款信息集合
        List<ProductReceiveInfoMessageBean> listProductReceiveInfoMes = receiveService.getReceiveInfoList(investId,"YRECEIVETIME","ASC",pageIndex,pageSize);
        int totalCount = receiveService.getReceiveInfoCount(investId);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo2(pageIndex, pageSize, totalCount, "pageReceive", "goReceivePage", investId); //分页部分拼接
        modelMap.put("listProductReceiveInfoMes",listProductReceiveInfoMes); //还款信息集合
        modelMap.put("pageStr",pageStr); //分页部分拼接

        shFlag = productInfoMessageBean.getShFlag(); //审核状态(0-审核通过、1-审核未通过、2-未审核)
        if (shFlag != null && !shFlag.equals("")){
            if (shFlag.equals("2")){ //待审核
                investFlag = "投标中";
            }else if (shFlag.equals("0")){ //审核通过
                if (wreceivePrice != null && !wreceivePrice.equals("")){ //未回款金额
                    if (wreceivePrice.equals("0")){
                        investFlag = "已结清";
                    }else{
                        investFlag = "回收中";
                    }
                }
            }
        }

        modelMap.put("investFlag",investFlag); //投资状态
        modelMap.put("attornId",attornId); //债权id
        modelMap.put("investId",investId); //投资id
        modelMap.put("productId",productId); //产品id
        modelMap.put("contractType",contractType); //合同类型(1-投资、2-债权)

        JetbrickBean bean = new JetbrickBean();

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/attorn/attornProduct_details.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 查询所有待转让的债权
     *
     * @param productInvestMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/infoNotAttornByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean infoInvestByPage(@ModelAttribute ProductInvestMessageBean productInvestMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String pageIndex = productInvestMes.getPageIndex(); //当前页
        String pageSize = productInvestMes.getPageSize(); //每页显示数量
        String buyerId = ""; //购买人id
        String isSy = productInvestMes.getIsSy();
        if (memberInfo != null) {
            buyerId = memberInfo.getId(); //购买人id
            productInvestMes.setBuyer(buyerId);
        }
        if (pageIndex != null && !pageIndex.equals("")) { //当前页
            pageIndex = "1";
            productInvestMes.setPageIndex(pageIndex);
        }
        if (pageSize != null && !pageSize.equals("")) { //每页显示数量
            pageSize = "10";
            productInvestMes.setPageSize(pageSize);
        }
        //产品投资集合
        final List<ProductInvestMessageBean> attornList1 = productAttornService.getAttornList(productInvestMes);
        List<ProductInvestMessageBean> attornList = attornList1;
        ProductInvestMessageBean productInvestMessageBean = attornList.get(0);
        modelMap.addAttribute("productInvestMessageBean", productInvestMessageBean); //可投资的内容拼接
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productInvestMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }





    /**
     * 根据分页查询债权
     *
     * @param productAttornRecordMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttornWhereByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAttornWhereByPage(@ModelAttribute ProductAttornRecordMessageBean productAttornRecordMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productAttornRecordMes.getPageIndex(); //当前页
        String pageSize = productAttornRecordMes.getPageSize(); //每次显示数量
        if (pageIndex == null || pageIndex.equals("")){
            pageIndex = "1";
            productAttornRecordMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){
            pageSize = "3";
            productAttornRecordMes.setPageSize(pageSize);
        }
        //债权产品集合
        List<ProductAttornRecordMessageBean> listProductAttornRecordMes = productAttornService.getFrontAttornList("ENDTIME", "DESC", productAttornRecordMes);
        //债权产品数量
        int totalCount = productAttornService.getFrontAttornCount(productAttornRecordMes);
        String attornStr = ""; //债权转让拼接
        FrontPage frontPage = new FrontPage();
        attornStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageAttorn","goAttornPage");
        modelMap.addAttribute("listProductAttornRecordMes", listProductAttornRecordMes); //债权部分集合
        modelMap.addAttribute("attornStr",attornStr); //债权部分拼接
        /********************* 产品部分End *********************/

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productAttornRecordMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }



    /**
     * 首页债权查询上面按钮(刚进入页面)
     *
     * @param productAttornRecordMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttornByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAttornByPage(@ModelAttribute ProductAttornRecordMessageBean productAttornRecordMes, ModelMap modelMap) {

        JetbrickBean bean = new JetbrickBean();
        String productTypeIdPj = ""; //产品类型id拼接
        String selectMutli = productAttornRecordMes.getSelectMutli(); //是否多选(0-多选、1-单选)
        String typeId = productAttornRecordMes.getType(); //产品类型
        String typeName = productAttornRecordMes.getTypeName(); //产品名称
        String attornStr = ""; //债权转让拼接
        modelMap.addAttribute("typeName", typeName); //菜单类型；
        /********** 产品类型集合Start **********/

        /******************** 产品类型部分Start ********************/
        ProductTypeMessage productTypeMessage = new ProductTypeMessage();
        List<ProductTypeMessage> listProductTypeMes = productTypeService.queryAllProductType(productTypeMessage);
        if (listProductTypeMes != null && listProductTypeMes.size() > 0) {
            /********** 产品类型循环Start **********/
            for (ProductTypeMessage productTypeMes : listProductTypeMes) {
                productTypeIdPj = productTypeIdPj + "," + productTypeMes.getId(); //产品类型主键
            }
            if (!productTypeIdPj.equals("")) {
                productTypeIdPj = productTypeIdPj.substring(1);
            }
            /*********** 产品类型循环End ***********/
        }
        modelMap.addAttribute("listProductTypeMes", listProductTypeMes); //产品类型集合
        modelMap.addAttribute("productTypeIdPj", productTypeIdPj); //产品类型拼接
        modelMap.addAttribute("selectMutli", selectMutli); //是否多选(0-多选、1-单选)
        /********************* 产品类型部分End *********************/

        /******************** 产品部分Start ********************/
        String pageIndex = productAttornRecordMes.getPageIndex(); //当前数量
        String pageSize = productAttornRecordMes.getPageSize(); //每页显示数量
        if (pageIndex == null || pageIndex.equals("")) { //当前数
            pageIndex = "1";
            productAttornRecordMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "3";
            productAttornRecordMes.setPageSize(pageSize);
        }

        //债权转让集合
        List<ProductAttornRecordMessageBean> listProductAttornRecordMes = productAttornService.getFrontAttornList("CREATETIME", "DESC", productAttornRecordMes);
        //债权转让数量
        int totalCount = productAttornService.getFrontAttornCount(productAttornRecordMes);
        FrontPage frontPage = new FrontPage();
        attornStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageAttorn","goAttornPage");
        modelMap.addAttribute("listProductAttornRecordMes", listProductAttornRecordMes); //债权转让
        modelMap.addAttribute("attornStr",attornStr); //债权转让拼接
        /********************* 产品部分End *********************/

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productAttornRecordMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 债权转让前台详情
     *
     * @param attornProductMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttornInfo", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAttornInfoByPage(@ModelAttribute AttornProductMessageBean attornProductMes, ModelMap modelMap) {

        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取session信息
        MyAccountMessageBean myAccountMessageBean = new MyAccountMessageBean();
        String memberId = ""; //会员id
        String buyer = ""; //购买人id
        String balance = ""; //余额
        String attornId = ""; //债权id
        String ipsAcctNo = ""; //冻结账号(承接人账号)
        String otherIpsAcctNo = ""; //它方账号(发起人账号)
        String projectNo = ""; //项目id号
        String projectId = ""; //项目id
        String trdAmt = ""; //冻结金额
        String webUrl = "http://jianbing88.com/front/registerhxWebResult"; //页面返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/attornhxResult"; //后台返回地址

        if(memberInfo != null){
            memberId = memberInfo.getId(); //会员id
            myAccountMessageBean.setId(memberId);
            myAccountMessageBean = allMemberService.getMemberInfo(myAccountMessageBean);
            if(myAccountMessageBean != null){
                balance = myAccountMessageBean.getBalance();
                modelMap.addAttribute("balance",balance); //余额
            }
        }

        /************************* 产品投资部分Start *************************/
        //债权信息
        attornProductMes = productAttornService.getAttornProductDetail(attornProductMes);
        modelMap.addAttribute("attornProductMes", attornProductMes); //产品投资集合
        if (attornProductMes != null) {
            buyer = attornProductMes.getBuyer(); //购买人
            attornId = attornProductMes.getId(); //债权id
            projectId = attornProductMes.getProductId(); //项目id
            trdAmt = attornProductMes.getPrice(); //转让价格(冻结金额)
            MemberInfoMessageBean memberInfoMessageBean = attornProductMes.getMemberInfoMessageBean();
            modelMap.addAttribute("memberInfoMessageBean", memberInfoMessageBean); //会员信息
            modelMap.addAttribute("trdAmt",trdAmt); //转让价格
        }
        /************************** 产品投资部分End **************************/

        //附件集合
        List<SysFileMessageBean> sysFileMessageBeanList = sysFileService.getAllFile(attornProductMes.getProductId());
        modelMap.addAttribute("sysFileMessageBeanList", sysFileMessageBeanList); //附件信息

        /******************** 购买人信息部分Start ********************/
        HuanXunRegistMessageBean huanXunRegistMesOne = new HuanXunRegistMessageBean();
        huanXunRegistMesOne.setMemberId(buyer);
        List<HuanXunRegistMessageBean> listHuanXunRegistMesOne = huanXunRegistService.getRegisterhxBySome(huanXunRegistMesOne);
        if (listHuanXunRegistMesOne != null && listHuanXunRegistMesOne.size() > 0){
            otherIpsAcctNo = listHuanXunRegistMesOne.get(0).getIpsAcctNo(); //它方人账号
            modelMap.addAttribute("otherIpsAcctNo",otherIpsAcctNo);
        }
        /********************* 购买人信息部分End *********************/

        /******************** 承接人信息部分Start ********************/
        HuanXunRegistMessageBean huanXunRegistMesTwo = new HuanXunRegistMessageBean();
        huanXunRegistMesTwo.setMemberId(memberId);
        List<HuanXunRegistMessageBean> listHuanXunRegistMesTwo = huanXunRegistService.getRegisterhxBySome(huanXunRegistMesTwo);
        if (listHuanXunRegistMesTwo != null && listHuanXunRegistMesTwo.size() > 0){
            ipsAcctNo = listHuanXunRegistMesTwo.get(0).getIpsAcctNo(); //购买人账号
            modelMap.addAttribute("ipsAcctNo",ipsAcctNo);
        }
        /********************* 承接人信息部分End *********************/

        ProductHuanXunMessageBean productHuanXunMessageBean = new ProductHuanXunMessageBean();
        productHuanXunMessageBean.setId(projectId); //项目id
        List<ProductHuanXunMessageBean> listHuanXunRegProjectMes = huanXunRegProjectService.getRegProjectInfoList(productHuanXunMessageBean);
        if (listHuanXunRegProjectMes != null && listHuanXunRegProjectMes.size() > 0){
            projectNo = listHuanXunRegProjectMes.get(0).getProjectNo(); //项目ID号
            modelMap.addAttribute("projectNo",projectNo);
        }

        webUrl = webUrl + "?memberId="+memberId; //页面返回地址
        s2SUrl = s2SUrl + "?attornId="+attornId; //后台返回地址
        modelMap.addAttribute("webUrl",webUrl);
        modelMap.addAttribute("s2SUrl",s2SUrl);
        JetbrickBean bean = new JetbrickBean();

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(attornProductMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 债权转让(承接)
     *
     * @param attornProductMes
     */
    @RequestMapping(value = "/addAttorn", method = RequestMethod.POST)
    @ResponseBody
    public AttornProductMessageBean onUpdateProduct(@ModelAttribute AttornProductMessageBean attornProductMes, HttpServletRequest request) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String traderpassword = attornProductMes.getTraderpassword(); //交易密码
        String memberId = ""; //会员id
        String buyer = attornProductMes.getBuyer(); //投资人
        String captcha = attornProductMes.getCaptcha(); //验证码
        String applyMemberId = attornProductMes.getApplyMemberId(); //产品申请人

        if (memberInfo == null) {
            attornProductMes.setResult("error");
            attornProductMes.setInfo("抱歉，请您先登录在进行操作！");
        } else {
            memberId = memberInfo.getId(); //会员id
            if(buyer != null && !buyer.equals("")){ //投资人
                if(!buyer.equals(memberId)){
                    if(!applyMemberId.equals(memberId)){ //产品申请人
                        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
                        memberInfoMessageBean.setId(memberInfo.getId());
                        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);

                        /****************************** 交易密码验证Start ******************************/
                        if (traderpassword != null && !traderpassword.equals("")) { //交易密码
                            if (traderpassword.equals(memberInfoMessageBean.getTraderpassword())) { //交易密码相同
                                /********** 验证码部分Start **********/
                                HttpSession session = request.getSession();
                                if (captcha != null && !captcha.equals("")) { //验证码
                                    if (!captcha.equals((String) session.getAttribute("certCode"))) { //验证码不符
                                        attornProductMes.setResult("error");
                                        attornProductMes.setInfo("验证码错误！");
                                    } else {
                                        attornProductMes = productAttornService.saveProductAttorn(attornProductMes, memberInfo);
                                    }
                                }
                                /*********** 验证码部分End ***********/
                            } else {
                                attornProductMes.setResult("error");
                                attornProductMes.setInfo("交易密码错误！");
                            }
                        } else {
                            attornProductMes.setResult("error");
                            attornProductMes.setInfo("请您填写交易密码！");
                        }
                        /******************************* 交易密码验证End *******************************/

                    }else{
                        attornProductMes.setResult("error");
                        attornProductMes.setInfo("您是该产品借款人，不允许对自己的产品进行承接！");
                    }
                }else{
                    attornProductMes.setResult("error");
                    attornProductMes.setInfo("该债权由您发起，不可以对自己发起的债权进行投资！");
                }
            }
        }
        return attornProductMes;
    }


    /**
     * 我的投资中的债权转让
     * @param productMyAttornMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/confirmAttorn", method = RequestMethod.POST)
    @ResponseBody
    public ProductMyAttornBusinessBean confirmAttornProduct(ProductMyAttornMessageBean productMyAttornMes,ModelMap modelMap) {
        ProductMyAttornBusinessBean productMyAttornBusinessBean = new ProductMyAttornBusinessBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean(); //会员

        if(memberInfo == null){
            productMyAttornBusinessBean.setInfo("您还未登录，请先进行登录操作！");
            productMyAttornBusinessBean.setCode("error");
            return productMyAttornBusinessBean;
        }else{
            memberInfoMessageBean.setId(memberInfo.getId()); //会员id
            memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
            if(memberInfoMessageBean != null){
                productMyAttornBusinessBean = productAttornService.confirmAttornProduct(productMyAttornMes); //发起债权转让
            }
        }
        return productMyAttornBusinessBean;
    }


    /**
     * 债权转让详情页
     *
     * @param productMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttronDetails", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAttronDetailsByPage(@ModelAttribute ProductMessageBean productMes, ModelMap modelMap) {

        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取session信息
        String type = productMes.getType();
        String id = productMes.getId();
        ProductInvestMessageBean productInvestMessageBean = new ProductInvestMessageBean();
        productInvestMessageBean.setBuyer(productMes.getId());
        List<ProductInvestMessageBean> productInvestMessageBeans = productBuyService.getProductInvestList(productInvestMessageBean);
        modelMap.addAttribute("productInvestMessageBean", productInvestMessageBeans.get(0));

        JetbrickBean bean = new JetbrickBean();

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 债权转让操作页
     *
     * @param productMyInvestMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttronOper", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAttronOperByPage(@ModelAttribute ProductMyInvestMessageBean productMyInvestMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        //从session中获取session信息
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        ProductMyInvestMessageBean productMyInvestMessageBean = productBuyService.getInvestInfoById(productMyInvestMes); //投资信息
        modelMap.addAttribute("productMyInvestMessageBean",productMyInvestMessageBean); //我的投资项目信息

        //模板解析方法
        try {

            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productMyInvestMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     *  获取预期收益率
     * @return
     */
    @RequestMapping(value = "/getExpectedRate", method = RequestMethod.POST)
    @ResponseBody
    public void getExpectedRate(HttpServletRequest request,HttpServletResponse response) {
        String surplusPricipal = request.getParameter("surplusPricipal"); //剩余本金(String型)
        String loanRate = request.getParameter("loanRate"); //贷款利率(String型)
        String nextReceiveTime = request.getParameter("nextReceiveTime"); //下次回款日期(String型)
        String attornPrice = request.getParameter("attornPrice"); //转让价格(String型)
        Double expectedRate = 0.0d; //预期收益率
        ProductAttorn productAttorn = new ProductAttorn();

        try{
            expectedRate = productAttorn.getExpectedRate(surplusPricipal,loanRate,attornPrice,nextReceiveTime); //预期收益率
            expectedRate = expectedRate * 100;
            expectedRate = PublicsTool.round(expectedRate,2);
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("expectedRate",expectedRate); //预期收益率
        output(response, JSONObject.fromObject(in).toString());
    }




    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
