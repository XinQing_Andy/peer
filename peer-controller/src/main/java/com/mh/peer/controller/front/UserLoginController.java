package com.mh.peer.controller.front;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.message.LoginMessageBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.service.front.UserLoginService;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpSession;

/**
 * Created by cuibin on 2016/4/22.
 */
@Controller
@RequestMapping("/user")
public class UserLoginController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserLoginController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private UserLoginService userLoginService;

    /**
     * 用户登录
     */
    @RequestMapping(value = "/userLogin", method = RequestMethod.POST)
    @ResponseBody
    public LoginMessageBean userLogin(@ModelAttribute LoginMessageBean user) {
        // 拿到会话
        HttpSession session = servletUtil.getSession();
        // 设置session
        user.setMessageId(session.getId() + session.getCreationTime());
        // 设置客户端IP
        user.setIp(servletUtil.getRequest().getRemoteAddr());
        LOGGER.info("接受信息==" + user);
        // 调用登录方法
        LoginBusinessBean loginBusinessBean = userLoginService.userLogin(user);
        LOGGER.info("业务消息==" + loginBusinessBean);
        user.setInfo(loginBusinessBean.getInfo());
        user.setResult(loginBusinessBean.getResult());
        session.setAttribute("sessionMemberInfo", loginBusinessBean.getMemberInfo());
        return user;
    }

    /**
     * 退出登录
     */
    @RequestMapping(value = "/loginOut", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean loginOut(){
        //拿到会话
        HttpSession session = servletUtil.getSession();
        //会话销毁返回登录页面
        session.invalidate();
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        memberInfoMessageBean.setResult("success");
        return memberInfoMessageBean;
    }
}
