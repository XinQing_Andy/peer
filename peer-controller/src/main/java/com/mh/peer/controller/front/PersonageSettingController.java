package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.model.message.SysCityMessagebean;
import com.mh.peer.model.message.SysFileMessageBean;
import com.mh.peer.service.front.PersonageSettingService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.UUID;

/**
 * Created by Cuibin on 2016/5/1.
 */
@Controller
@RequestMapping("/personal")
public class PersonageSettingController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonageSettingController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private PersonageSettingService personageSettingService;

    /**
     * 跳转到个人基本资料
     */
    @RequestMapping(value = "/goPersonal", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean goPersonal(@ModelAttribute ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean bean = new JetbrickBean();
        if (memberInfo == null) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }
        String provinceId = ""; //省id
        String cityId = ""; //城市id
        String strCity = ""; //城市下拉选项
        String selected = ""; //是否被选中

        //查询一个用户
        MemberInfoMessageBean memberInfoMessageBean = personageSettingService.queryOnlyMemberInfo();
        String telephone = memberInfoMessageBean.getTelephone();//手机号
        String email = memberInfoMessageBean.getEmail(); //邮箱

        //查询所有省
        List listProvince = personageSettingService.queryAllProvince();
        //查询头像信息
        SysFileMessageBean img = personageSettingService.queryOnlyPortrait(memberInfo.getId());

        if (memberInfoMessageBean != null) {
            provinceId = memberInfoMessageBean.getProvince(); //省id
            cityId = memberInfoMessageBean.getCity(); //城市id
            List<SysCityMessagebean> listCity = personageSettingService.queryCity(provinceId); //城市集合
            if (listCity != null && listCity.size() > 0) {
                for (SysCityMessagebean sysCityMes : listCity) {
                    if (cityId.equals(sysCityMes.getId())) {
                        selected = "selected";
                    } else {
                        selected = "";
                    }
                    strCity = strCity + "<option value=\"" + sysCityMes.getId() + "\" " + selected + ">" + sysCityMes.getCityname() + "</option>";
                }
                memberInfoMessageBean.setCityPj(strCity);
            }
        }
        modelMap.put("memberInfoMessageBean", memberInfoMessageBean); //数据集合
        modelMap.put("provinceList", listProvince); //省集合
        modelMap.put("img", img); //头像信息

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/personal/personal.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("渲染成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("渲染失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 查询城市(下拉菜单级联)
     */
    @RequestMapping(value = "/queryCity", method = RequestMethod.POST)
    @ResponseBody
    public SysCityMessagebean queryCity(@ModelAttribute SysCityMessagebean sysCityMessagebean) {
        String provinceId = sysCityMessagebean.getProvinceid(); //省id

        List<SysCityMessagebean> list = personageSettingService.queryCity(provinceId);
        String str = ""; //城市部分拼接
        if (list.size() > 0) {
            for (SysCityMessagebean sysCityMes : list) {
                str = str + "<option value=\"" + sysCityMes.getId() + "\">" + sysCityMes.getCityname() + "</option>";
            }

            sysCityMessagebean.setCityPj(str);
        }
        return sysCityMessagebean;
    }

    /**
     * 更新个人基本资料
     */
    @RequestMapping(value = "/updatePersona", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean updatePersona(@ModelAttribute MemberInfo memberInfo) {
        return personageSettingService.updatePersona(memberInfo);
    }

    /**
     * 更新头像
     */
    @RequestMapping(value = "/updatePortrait", method = RequestMethod.POST)
    @ResponseBody
    public void updatePortrait(@RequestParam MultipartFile[] portrait, HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String id = memberInfo.getId();
        String ctxPath = request.getRealPath("/") + "image/member"; //绝对路径
        String ctxPath2 = "/image/member"; //相对路径
        String originalFilename = ""; //原文件名称
        String uuid = "";
        String newFileName = ""; //新文件名称
        String suffix = ""; //后缀
        response.setContentType("text/plain; charset=UTF-8");

        try {

            ctxPath = ctxPath.replaceAll("\\\\", "/"); //绝对路径
            ctxPath2 = ctxPath2.replaceAll("\\\\", "/"); //相对路径

            for (MultipartFile myfile : portrait) {
                if (myfile.isEmpty()) {
                    out.print("请选择要上传的文件！");
                    out.flush();
                } else {
                    originalFilename = myfile.getOriginalFilename(); //原文件名称
                    suffix = originalFilename.indexOf(".") != -1 ? originalFilename.substring(originalFilename.lastIndexOf("."), originalFilename.length()) : null;
                    suffix = suffix.toUpperCase(); //转换为大写
                    if (suffix != null && !suffix.equals("")) {
                        if (suffix.equals(".JPG") || suffix.equals(".PNG") || suffix.equals(".GIF") || suffix.equals(".JPEG")) {
                            uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
                            newFileName = uuid + (suffix != null ? suffix : ""); //构成新文件名。
                            try {
                                FileUtils.copyInputStreamToFile(myfile.getInputStream(), new File(ctxPath, newFileName));
                                String path = ctxPath2 + "/" + newFileName;
                                ctxPath = ctxPath + "/" + newFileName;
                                SysFileMessageBean img = personageSettingService.queryOnlyPortrait(memberInfo.getId());
                                if(img.getId() != null && !img.getId().equals("")){
                                    personageSettingService.deletePortrait(img.getId());
                                    File f = new File(img.getImgurljd());
                                    //System.out.println(img.getImgurljd());
                                    f.delete();
                                }
                                personageSettingService.updatePortrait(path, ctxPath, id);

                            } catch (Exception e) {
                                out.print("文件上传失败，请稍后重试!");
                                e.printStackTrace();
                            }
                        } else {
                            out.print("" + originalFilename + "不符合图片规则，请上传图片格式文件!");
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            out.print("文件上传失败，请稍后重试!");
            e.printStackTrace();
        }
    }

}
