package com.mh.peer.controller.Export;

import com.mh.peer.util.ExportExcel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Created by zhangerxin on 2016-9-13.
 */
@Controller
@RequestMapping("/export")
public class ExportController {


    /**
     * 导出Excel
     * @param request
     * @param response
     */
    @RequestMapping(value = "/exportExcel", method = RequestMethod.POST)
    @ResponseBody
    public void exportExcel(HttpServletRequest request,HttpServletResponse response) {
        ExportExcel exportExcel = new ExportExcel();
        exportExcel.exportByInfo(request,response);
    }
}
