package com.mh.peer.controller.front;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.model.message.SysEmailSettingMessageBean;
import com.mh.peer.service.front.RegistService;
import com.mh.peer.service.sys.MessageSetService;
import com.mh.peer.service.sys.SystemEmailService;
import com.mh.peer.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.UUID;


/**
 * Created by Cuibin on 2016/4/23.
 */
@Controller
@RequestMapping("/regist")
public class RegistController {

    String spwd = null;//全局密码
//    String sUname = null; //全局用户名
//    String jmSPwd = null; //加密全局密码
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private RegistService registService;
    @Autowired
    private SystemEmailService systemEmailService;
    @Autowired
    private MessageSetService messageSetService;

    /**
     * 注册
     */
    @RequestMapping(value = "/regist", method = RequestMethod.GET)
    public String regist(HttpServletRequest request, ModelMap modelMap) throws IOException {
        String nickname = new String(request.getParameter("nickname").getBytes("iso8859-1"), "gb2312");
        nickname = URLDecoder.decode(nickname, "gb2312");
        String username = new String(request.getParameter("username").getBytes("iso8859-1"), "utf-8");
        String password = new String(request.getParameter("password").getBytes("iso8859-1"), "utf-8");

//        if (username == null || username.equals("") || password == null || password.equals("")) {
//            return "front/reg/regist";
//        }
//        if (spwd == null || spwd.equals("") || sUname == null || sUname.equals("") || jmSPwd == null || jmSPwd.equals("")) {
//            return "front/reg/regist";
//        }
//        if (!jmSPwd.equals(password) || !sUname.equals(username)) {
//            return "front/reg/regist";
//        }
        MemberInfoMessageBean memberInfoMessageBean = registService.checkUsername(username);
        if (memberInfoMessageBean == null) {
            return "front/reg/regist";
        }
        if (memberInfoMessageBean.getResult().equals("error") || memberInfoMessageBean.getResult().equals("")
                || memberInfoMessageBean.getResult() == null) {
            return "front/reg/regist";
        }

        modelMap.put("nickname", nickname);
        modelMap.put("username", username);
        modelMap.put("password", spwd);
        MemberInfo memberInfo = new MemberInfo();
        String uid = UUID.randomUUID().toString().replaceAll("\\-", "");
        memberInfo.setId(uid);
        memberInfo.setUsername(username);
        memberInfo.setNickname(nickname);
        memberInfo.setPassword(password);
        memberInfo.setEmail(username);
        memberInfo.setLogintype("0");
        memberInfo.setTelephoneFlg("1"); //是否手机认证
        memberInfo.setMailFlg("0"); //是否邮箱注册
        MemberInfoMessageBean result = registService.regist(memberInfo);
        if (result.getResult().equals("success")) {
            return "front/reg/regist_success";
        }
        return "front/reg/regist";
    }

    /**
     * 跳转到注册页面
     */
    @RequestMapping(value = "/openRegist", method = RequestMethod.GET)
    public String openRegist() {
        return "front/reg/regist";
    }


    /**
     * 跳转到邮箱验证页面
     */
    @RequestMapping(value = "/openEmailCheck", method = RequestMethod.POST)
    public String openEmailCheck(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws IOException {
        String nickname = new String(request.getParameter("nickname").getBytes("utf-8"), "utf-8");
        String username = new String(request.getParameter("username").getBytes("iso8859-1"), "utf-8");
        String password = new String(request.getParameter("password").getBytes("iso8859-1"), "utf-8");
        modelMap.put("nickname", nickname);
        modelMap.put("username", username);
        modelMap.put("password", password);
        return "front/reg/regist_yz_mail";
    }

    /**
     * 发送验证
     */
    @RequestMapping(value = "/sendEmailCheck", method = RequestMethod.POST)
    public void sendEmailCheck(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String nickname = new String(request.getParameter("nickname").getBytes("utf-8"), "utf-8");
        nickname = URLEncoder.encode(nickname, "gb2312");
        String username = new String(request.getParameter("username").getBytes("iso8859-1"), "utf-8");
        String password = new String(request.getParameter("password").getBytes("iso8859-1"), "utf-8");
        spwd = password;
        //sUname = username;

        String passwordJm = ""; //密码(加密)
        String result = "0"; //0-发送成功，1-发送失败、2-验证码不符
        String certCode = request.getParameter("certCode"); //验证码
        SysEmailSettingMessageBean sysesm = systemEmailService.querySystemEmail();
        String title = "";
        String body = "";

        PublicAddress publicAddress = new PublicAddress();//获取服务器地址
        String address = publicAddress.getEmailAddress();
        try {

            HttpSession session = request.getSession();
            if (certCode != null && !certCode.equals("")) {
                if (certCode.equals(session.getAttribute("certCode"))) { //验证码正确
                    /********** 密码加密Start **********/
                    if (password != null && !password.equals("")) {
                        Encrypt encrypt = new Encrypt();
                        passwordJm = encrypt.handleMd5(password);
                    }
                    //jmSPwd = passwordJm;
                    /*********** 密码加密End 120.76.137.147***********/

                    title = "煎饼金融账号激活";
                    body = "亲爱的用户,您正在注册煎饼金融 <br/><a href='" + address + "regist/regist?username=" + username + "&password=" + passwordJm + "&nickname=" + nickname + "' style='color:blue'>请点击此链接完成注册</a>  感谢您的支持" +
                            "<br/>此邮件由系统发出,如有疑问可直接联系我们";
                    SendEmail sendEmail = new SendEmail();
                    sendEmail.send(sysesm.getSmtp(), sysesm.getEmail(), sysesm.getPassword(), username, title, body);
                } else {
                    result = "2";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = "1";
        }

        PrintWriter out = response.getWriter();
        out.print(result);

    }


    /**
     * 验证用户名是否存在
     */
    @RequestMapping(value = "/checkUsername", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean checkUsername(@ModelAttribute MemberInfoMessageBean mimb) {
        return registService.checkUsername(mimb.getUsername());
    }

    /**
     * 生成验证码
     *
     * @param request
     * @param response
     * @param modelMap
     */
    @RequestMapping(value = "/makeCertPic", method = RequestMethod.GET)
    public void yanzm(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws IOException {
        HttpSession session = request.getSession();
        MakeCertPic makecertpic = new MakeCertPic();
        String str = makecertpic.getCertPic(75, 30, response.getOutputStream());
        session.setAttribute("certCode", str);
    }

    /**
     * 跳转到手机号认证页面
     */
    @RequestMapping(value = "/openPhoneCheck", method = RequestMethod.POST)
    public String openPhoneCheck(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws IOException {
        String nickname = new String(request.getParameter("nickname").getBytes("utf-8"), "utf-8");
        String username = new String(request.getParameter("username").getBytes("iso8859-1"), "utf-8");
        String password = new String(request.getParameter("password").getBytes("iso8859-1"), "utf-8");

        modelMap.put("nickname", nickname);
        modelMap.put("username", username);
        modelMap.put("password", password);
        modelMap.put("message", messageSetService.loadSwitch());
        return "front/reg/regist_yz_phone";
    }

    /**
     * 跳转到注册协议
     */
    @RequestMapping(value = "/openRegistProtocol", method = RequestMethod.GET)
    public String openRegistProtocol() throws IOException {
        return "front/reg/regist_protocol";
    }

    /**
     * 跳转到隐私条款
     */
    @RequestMapping(value = "/registPrivacyPolicy", method = RequestMethod.GET)
    public String registPrivacyPolicy() throws IOException {
        return "front/reg/regist_privacy_policy";
    }

    /**
     * 获取手机验证码
     */
    @RequestMapping(value = "/sendMessageCheck", method = RequestMethod.POST)
    @ResponseBody
    public void sendMessageCheck(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) throws IOException {
        String username = memberInfoMessageBean.getUsername();
        SendMobileMessage sendMobileMessage = new SendMobileMessage();
        PublicsTool publicsTool = new PublicsTool();
        String random = publicsTool.getRandom();
        HttpSession session = servletUtil.getSession();
        session.setAttribute("checkCode", random);

        String content = "【煎饼金融P2P】尊敬的用户您好,验证码为:" + random + ",请妥善保管,验证码提供给他人将导致信息泄露";
        sendMobileMessage.send(username, content);
        //System.out.println(content);
    }

    /**
     * 手机注册
     */
    @RequestMapping(value = "/phoneRegist", method = RequestMethod.POST)
    public String phoneRegist(HttpServletRequest request, ModelMap modelMap) throws IOException {
        request.setCharacterEncoding("UTF-8");
        String nickname = request.getParameter("nickname");
        String username = new String(request.getParameter("username").getBytes("iso8859-1"), "utf-8");
        String password = new String(request.getParameter("password").getBytes("iso8859-1"), "utf-8");

        MemberInfoMessageBean memberInfoMessageBean = registService.checkUsername(username);
        if (memberInfoMessageBean.getResult().equals("error")) {
            return "front/reg/regist";
        }
        modelMap.put("password", password);
        modelMap.put("nickname", nickname);
        modelMap.put("username", username);
        if (password != null && !password.equals("")) {
            Encrypt encrypt = new Encrypt();
            password = encrypt.handleMd5(password);
        }
        MemberInfo memberInfo = new MemberInfo();
        String uid = UUID.randomUUID().toString().replaceAll("\\-", "");
        memberInfo.setId(uid);
        memberInfo.setUsername(username);
        memberInfo.setNickname(nickname);
        memberInfo.setPassword(password);
        memberInfo.setTelephone(username);
        memberInfo.setTelephoneFlg("0"); //是否手机认证
        memberInfo.setMailFlg("1"); //是否邮箱注册
        memberInfo.setLogintype("0");
        MemberInfoMessageBean result = registService.regist(memberInfo);
        if (result.getResult().equals("success")) {
            return "front/reg/regist_success";
        }
        return "front/reg/regist";
    }

    /**
     * 验证短信验证码
     */
    @RequestMapping(value = "/checkCode", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean checkCode(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean, ModelMap modelMap) throws IOException {
        String code = memberInfoMessageBean.getCode();
        HttpSession session = servletUtil.getSession();
        String sessionCheckCode = (String) session.getAttribute("checkCode");
        MemberInfoMessageBean result = new MemberInfoMessageBean();
        if (code.equals(sessionCheckCode)) {
            result.setResult("success");
            result.setInfo("验证成功");
        } else {
            result.setResult("error");
            result.setInfo("验证失败");
        }
        return result;

    }
}
