package com.mh.peer.controller.sys;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.message.SysEmailSettingMessageBean;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.mh.peer.service.sys.SystemEmailService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.StringWriter;

/**
 * Created by cuibin on 2016/4/25.
 */
@Controller
@RequestMapping("/email")
public class SystemEmailController {
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private SystemEmailService systemEmailService;
    /**
     *查询邮件设置
     */
    @RequestMapping(value = "/querySystemEmail", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean querySystemEmail(@ModelAttribute SysEmailSettingMessageBean sesm, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        modelMap.put("SysEmailSettingMessageBean", systemEmailService.querySystemEmail());
        try {
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(sesm.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }

    /**
     *  添加 邮件设置
     */
    @RequestMapping(value = "/addSystemEmail", method = RequestMethod.POST)
    @ResponseBody
    public SysEmailSettingMessageBean addSystemEmail(@ModelAttribute SysEmailSettingMessageBean sesm){
        SysEmailSettingMessageBean my = systemEmailService.querySystemEmail();
        if(my.getResult().equals("error")){
            return systemEmailService.addSystemEmail(sesm);
        }
        return systemEmailService.updateSystemEmail(sesm);
    }
    /**
     *  更新邮件设置
     */
    @RequestMapping(value = "/updateSystemEmail", method = RequestMethod.POST)
    @ResponseBody
    public SysEmailSettingMessageBean updateSystemEmail(@ModelAttribute SysEmailSettingMessageBean sesm){
        return systemEmailService.updateSystemEmail(sesm);
    }
}
