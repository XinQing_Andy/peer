package com.mh.peer.controller.huanxun;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by ${zhangerxin} on 2016-7-25.
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunLoginController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private HuanXunRegistService huanXunRegistService;

    /**
     * 环迅用户登录
     * @param huanXunRegistMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunRegistMessageBean login(@ModelAttribute HuanXunRegistMessageBean huanXunRegistMes, ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        String userName = ""; //用户名
        String ipsAcctNo = ""; //IPS存管交易账号
        if (memberInfo == null){
            huanXunRegistMes.setCode("-1");
            huanXunRegistMes.setInfo("请重新登录！");
        }
        memberId = memberInfo.getId(); //会员id
        huanXunRegistMes.setMemberId(memberId); //会员id
        //查询会员注册信息
        List<HuanXunRegistMessageBean> listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMes);
        if (listHuanXunRegistMes == null || listHuanXunRegistMes.size() == 0){
            huanXunRegistMes.setCode("-1"); //执行状态
            huanXunRegistMes.setInfo("您还未注册环迅，请前去注册！");
            return huanXunRegistMes;
        }
        userName = listHuanXunRegistMes.get(0).getUserName(); //用户名
        ipsAcctNo = listHuanXunRegistMes.get(0).getIpsAcctNo(); //IPS存管账号
        huanXunRegistMes.setCode("0");
        huanXunRegistMes.setUserName(userName);
        huanXunRegistMes.setIpsAcctNo(ipsAcctNo);
        return huanXunRegistMes;
    }
}
