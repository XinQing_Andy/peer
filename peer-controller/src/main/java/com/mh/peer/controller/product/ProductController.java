package com.mh.peer.controller.product;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.business.ProductBusinessBean;
import com.mh.peer.model.business.SysSecurityQuestionBusinessBean;
import com.mh.peer.model.entity.ProductContent;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.*;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.product.ProductBuyService;
import com.mh.peer.service.product.ProductService;
import com.mh.peer.service.product.ProductTypeService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.PublicAddress;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONObject;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by zhangerxin on 2016-4-10.
 */
@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductBuyService productBuyService;
    @Autowired
    private SysFileService sysFileService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private ProductTypeService productTypeService;
    @Autowired
    private HuanXunRegistService huanXunRegistService;

    /**
     * 根据分页获取产品信息
     *
     * @param productMese
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryProductByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryProductByPage(@ModelAttribute ProductMessageBean productMese, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productMese.getPageIndex(); //当前数量
        String pageSize = productMese.getPageSize(); //每页显示数量
        String currentPage = productMese.getCurrentPage(); //当前页
        int pageCount = 0; //总页数
        String title = productMese.getTitle(); //题目

        if (pageIndex == null || pageIndex.equals("")) { //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "10";
        }
        if (title != null && !title.equals("")) { //题目
            modelMap.addAttribute("title", title);
        }

        productMese.setPageIndex(pageIndex); //当前页
        productMese.setPageSize(pageSize); //每页显示数量
        List<ProductMessageBean> productList = productService.queryProductByPage(productMese); //产品集合(根据分页查询)
        int totalCount = productService.getCountProductByPage(productMese); //产品数量
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量

        modelMap.put("productList", productList); //产品集合
        modelMap.put("totalCount", totalCount); //产品数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageCount",pageCount); //总数量

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productMese.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 打开新增产品页面
     *
     * @param productMese
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openProduct", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openProduct(@ModelAttribute ProductMessageBean productMese, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        ProductTypeMessage productTypeMessage = new ProductTypeMessage();
        List<ProductTypeMessage> productTypeMessageBeans = productTypeService.queryAllProductType(productTypeMessage); //会员集合
        modelMap.put("productTypeMessageBeans", productTypeMessageBeans);

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productMese.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 新增产品
     *
     * @param productMes
     * @return
     */
    @RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
    @ResponseBody
    public ProductMessageBean onAddUser(@ModelAttribute ProductMessageBean productMes) {
        String applyMemberId = productMes.getApplyMember(); //申请人id
        if (applyMemberId == null || applyMemberId.equals("")) {
            productMes.setResult("error");
            productMes.setInfo("请选择申请人！");
            return productMes;
        } else {
            HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean(); //环迅注册
            huanXunRegistMessageBean.setMemberId(applyMemberId); //申请人id
            List<HuanXunRegistMessageBean> listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
            if (listHuanXunRegistMes == null || listHuanXunRegistMes.size() == 0) {
                productMes.setResult("error");
                productMes.setInfo("该借款人还未在环迅注册，不能进行借款！");
                return productMes;
            }

            HttpSession session = servletUtil.getSession();
            LoginBusinessBean bean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
            int userId = bean.getSysUser().getId(); //创建用户id
            productMes.setCreateuser(TypeTool.getString(userId)); //创建用户id
            productMes.setUpdateuser(TypeTool.getString(userId)); //更新人id
            ProductBusinessBean productBus = productService.saveProduct(productMes); //添加产品

            productMes.setResult(productBus.getResult());
            productMes.setInfo(productBus.getInfo());
        }

        return productMes;
    }


    /**
     * 上传附件(产品新增部分)
     *
     * @param upProductAdd
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/addProductFile", method = RequestMethod.POST)
    public void addProductFile(@RequestParam MultipartFile[] upProductAdd, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String ctxPath = request.getRealPath("/") + "image/product"; //绝对路径(不包含文件)
        String ctxPath2 = "/image/product"; //相对路径(不包含文件)
        String xdPath = ""; //相对路径(包含文件)
        String jdPath = ""; //绝对路径(包含文件)
        String pathPj = ""; //路径拼接(相对路径,绝对路径;相对路径,绝对路径)
        String originalFilename = ""; //原文件名称
        String uuid = ""; //附件名称
        String newFileName = ""; //新文件名称
        String suffix = ""; //后缀
        response.setContentType("text/plain; charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {

            ctxPath = ctxPath.replaceAll("\\\\", "/"); //绝对路径
            ctxPath2 = ctxPath2.replaceAll("\\\\", "/"); //相对路径

            /************************* 附件循环上传Start *************************/
            for (MultipartFile myfile : upProductAdd) {
                if (myfile.isEmpty()) {
                    out.print("请选择要上传的文件！");
                    out.flush();
                } else {
                    originalFilename = myfile.getOriginalFilename(); //原文件名称
                    suffix = originalFilename.indexOf(".") != -1 ? originalFilename.substring(originalFilename.lastIndexOf("."), originalFilename.length()) : null;
                    suffix = suffix.toUpperCase(); //转换为大写

                    /*************** 判断后缀Start ***************/
                    if (suffix != null && !suffix.equals("")) {
                        if (suffix.equals(".JPG") || suffix.equals(".PNG") || suffix.equals(".GIF") || suffix.equals(".JPEG")) {
                            uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
                            newFileName = uuid + (suffix != null ? suffix : ""); //构成新文件名。
                            jdPath = ctxPath + "/" + newFileName; //相对路径(包含文件名)
                            xdPath = ctxPath2 + "/" + newFileName; //绝对路径(包含文件名)
                            pathPj = xdPath + "," + jdPath; //最终路径拼接
                            FileUtils.copyInputStreamToFile(myfile.getInputStream(), new File(ctxPath, newFileName));
                        } else {
                            out.print("[请选择符合规则的图片进行上传！]");
                            break;
                        }
                    }
                    /**************** 判断后缀End ****************/
                }
            }
            /************************** 附件循环上传End **************************/

            if (pathPj.length() > 0) {
                out.print("[" + pathPj + "]");
            }
        } catch (Exception e) {
            out.print("[上传失败！]");
            e.printStackTrace();
        }
    }


    /**
     * 根据id查询产品信息
     *
     * @param productMessageBean
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryProductById", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryProductById(@ModelAttribute ProductMessageBean productMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        ProductMessageBean productMes = productService.queryProductById(productMessageBean); //产品集合(根据id查询)
        String productId = productMes.getId(); //产品id
        List<SysFileMessageBean> sysFileMessageBeanList = new ArrayList<SysFileMessageBean>(); //附件集合
        sysFileMessageBeanList = sysFileService.getAllFile(productId); //附件集合
        ProductTypeMessage productTypeMessage = new ProductTypeMessage(); //产品类型
        List<ProductTypeMessage> productTypeMessageBeans = productTypeService.queryAllProductType(productTypeMessage); //产品类型集合
        modelMap.put("productMes", productMes); //产品集合
        modelMap.put("productTypeMessageBeans", productTypeMessageBeans); //产品类型
        modelMap.put("sysFileMessageBeanList", sysFileMessageBeanList); //附件集合

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productMessageBean.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 修改产品信息
     *
     * @param productMes
     */
    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
    @ResponseBody
    public ProductMessageBean onUpdateProduct(@ModelAttribute ProductMessageBean productMes) {
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        int userId = bean.getSysUser().getId(); //创建用户id
        productMes.setUpdateuser(TypeTool.getString(userId)); //更新人
        ProductBusinessBean productBus = productService.updateProduct(productMes); //修改产品
        productMes.setResult(productBus.getResult());
        productMes.setInfo(productBus.getInfo());
        return productMes;
    }


    /**
     * 设置产品状态
     *
     * @param productMess
     * @return
     */
    @RequestMapping(value = "/setProductFlag")
    @ResponseBody
    public ProductMessageBean setProductFlag(@ModelAttribute ProductMessageBean productMess) {

        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        int userId = bean.getSysUser().getId(); //用户id
        String type = productMess.getType(); //用于判断是否在环迅登记过
        String flag = productMess.getFlag(); //状态(0-上架、1-下架)
        productMess.setUpdateuser(TypeTool.getString(userId)); //更新人
        productMess.setUpdatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //最后更新时间

        if (type != null && !type.equals("")){
            if (type.equals("1")){
                if (flag != null && !flag.equals("")){
                    if (flag.equals("0")){ //上架
                        productMess.setResult("error");
                        productMess.setInfo("您还未进行环迅登记，不能进行上架，请先去进行环迅登记！");
                        return productMess;
                    }
                }
            }
        }

        //更新产品状态
        ProductBusinessBean productBus = productService.settingProduct(productMess);

        //设置返回安全参数消息
        productMess.setResult(productBus.getResult()); //状态(成功或失败)
        productMess.setInfo(productBus.getInfo()); //错误信息
        return productMess;
    }


    /**
     * 设置产品审核状态
     *
     * @param productMess
     * @return
     */
    @RequestMapping(value = "/setProductShFlag")
    @ResponseBody
    public ProductMessageBean setProductShFlag(@ModelAttribute ProductMessageBean productMess) throws Exception {

        /*************** 获取登录信息Start ********************/
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean) session.getAttribute("loginBusinessBean");
        int userId = bean.getSysUser().getId();
        /**************** 获取登录信息End *********************/

        productMess.setUpdateuser(TypeTool.getString(userId)); //更新人
        productMess.setUpdatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //最后更新时间

        //设置审核状态
        ProductBusinessBean productBus = productService.setProductShFlag(productMess);
        //设置返回安全参数消息
        productMess.setResult(productBus.getResult()); //状态(成功或失败)
        productMess.setInfo(productBus.getInfo()); //错误信息
        return productMess;
    }


    /**
     * 获取所有会员
     *
     * @param memberInfoMes
     * @return
     */
    @RequestMapping(value = "/queryAllMember", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean querySetRoleList(@ModelAttribute MemberInfoMessageBean memberInfoMes) {
        List<MemberInfoMessageBean> memberInfoMessageBeans = new ArrayList<MemberInfoMessageBean>();
        memberInfoMessageBeans = allMemberService.queryMemberBysome(memberInfoMes); //会员集合
        int totalCount = allMemberService.queryCountMember(); //会员数量
        String pageIndex = memberInfoMes.getPageIndex(); //当前数
        String pageSize = memberInfoMes.getPageSize(); //每页显示数量
        String currentPage = memberInfoMes.getCurrentPage(); //当前页
        String memberStr = ""; //会员拼接
        int num = 1; //循环数量

        if (pageIndex == null || pageIndex.equals("")) { //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "10";
        }

        memberInfoMes.setPageIndex(pageIndex); //当前数
        memberInfoMes.setPageSize(pageSize); //每页显示数量
        String pageMemberStr = this.getShowPageMember(currentPage, totalCount); //显示所属分页

        if (memberInfoMessageBeans != null && memberInfoMessageBeans.size() > 0) {
            for (MemberInfoMessageBean memberInfoMessageBean : memberInfoMessageBeans) {
                memberStr = memberStr + "<tr>"
                        + "<td><input type=\"radio\" name=\"chooseMember\" id=\"chooseMember\" onclick=\"doCheckProductMember('" + memberInfoMessageBean.getId() + "','" + memberInfoMessageBean.getNickname() + "')\"/></td>"
                        + "<td>" + num + "</td>"
                        + "<td>" + memberInfoMessageBean.getUsername() + "</td>"
                        + "<td>" + memberInfoMessageBean.getNickname() + "</td>"
                        + "<td>" + memberInfoMessageBean.getRealname() + "</td>"
                        + "<td>" + memberInfoMessageBean.getIdcard() + "</td>"
                        + "</tr>";
                num++;
            }
        }

        memberInfoMes.setHtmlText(memberStr); //会员集合拼接
        memberInfoMes.setTotalCount(TypeTool.getString(totalCount)); //会员数量
        memberInfoMes.setCurrentPage(currentPage); //当前页
        memberInfoMes.setPageMemberStr(pageMemberStr); //分页部分拼接

        return memberInfoMes;
    }

    /**
     * 跳转到投资记录
     */
    @RequestMapping(value = "/openInvestHistory")
    @ResponseBody
    public JetbrickBean openInvestHistory(@ModelAttribute ProductBuyMessageBean productBuyMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productBuyMessageBean.getPageIndex(); //当前数量
        String pageSize = productBuyMessageBean.getPageSize(); //每页显示数量
        String currentPage = productBuyMessageBean.getCurrentPage(); //当前页
        String totalCount = ""; //投资数量(String型)
        int totalCountin = 0; //投资数量(int型)
        int pageCount = 0; //总页数
        String serverAddress = ""; //服务器地址

        if (pageIndex == null || pageIndex.equals("")) { //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "10";
        }
        productBuyMessageBean.setPageIndex(pageIndex); //当前页
        productBuyMessageBean.setPageSize(pageSize); //每页显示数量

        //投资集合
        List<ProductBuyMessageBean> listProductBuyMes = productService.queryInvestHistory(productBuyMessageBean);
        //投资数量
        totalCount = productService.queryInvestHistoryCount(productBuyMessageBean);
        if (totalCount != null && !totalCount.equals("")){
            totalCountin = TypeTool.getInt(totalCount);
        }
        pageCount = this.getPageCount(pageSize,totalCountin); //获取总页数

        PublicAddress publicAddress = new PublicAddress();
        serverAddress = publicAddress.getServerAddress(); //服务器地址

        modelMap.put("productBuyList", listProductBuyMes); //产品集合
        modelMap.put("totalCount", totalCount); //产品数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageCount",pageCount); //总页数
        modelMap.put("productId", productBuyMessageBean.getProductId());
        modelMap.put("serverAddress",serverAddress); //服务器地址

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("product/invest_history.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 按分页查询投资记录
     */
    @RequestMapping(value = "/queryInvestHistory")
    @ResponseBody
    public ProductBuyMessageBean queryInvestHistory(@ModelAttribute ProductBuyMessageBean productBuyMessageBean, ModelMap modelMap) {
        ProductBuyMessageBean result = new ProductBuyMessageBean();
        String pageIndex = productBuyMessageBean.getPageIndex(); //当前数量
        String pageSize = productBuyMessageBean.getPageSize(); //每页显示数量
        String currentPage = productBuyMessageBean.getCurrentPage(); //当前页

        if (pageIndex == null || pageIndex.equals("")) { //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "10";
        }
        productBuyMessageBean.setPageIndex(pageIndex); //当前页
        productBuyMessageBean.setPageSize(pageSize); //每页显示数量
        List<ProductBuyMessageBean> list = productService.queryInvestHistory(productBuyMessageBean);
        String totalCount = productService.queryInvestHistoryCount(productBuyMessageBean);
        String pageStr = this.getShowPageInvestHistory(currentPage, Integer.parseInt(totalCount)); //显示所属分页

        String str = "";
        int i = 0;
        str = str + "<table class=\"table1\" style=\"border-collapse: collapse;\">" +
                "<tr class=\"table_first\">" +
                "<td>序号</td>" +
                "<td>名称</td>" +
                "<td>编码 </td>" +
                "<td>投资金额</td>" +
                "<td>投资时间</td>" +
                "<td>年利率</td>" +
                "</tr>";

        for (ProductBuyMessageBean temp : list) {
            str = str + "<tr>" +
                    "<td>" + ++i + "</td>" +
                    "<td>" + temp.getBuyerName() + "</td>" +
                    "<td>" + temp.getCode() + "</td>" +
                    "<td>" + temp.getPrice() + "</td>" +
                    "<td>" + temp.getBuytime() + "</td>" +
                    "<td>" + temp.getLoanrate() + "%</td>" +
                    "</tr>";
        }
        str = str + "</table>";

        result.setStr(str);//拼接字符串
        result.setProductId(productBuyMessageBean.getProductId());//产品id
        result.setPageStr(pageStr);//显示所属分页
        result.setCurrentPage(currentPage);//当前页
        result.setTotalCount(totalCount);//产品数

        return result;
    }


    /**
     * 设置推荐项目状态
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/setNewShare")
    public void setNewShare(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap) {
        String newShare = request.getParameter("newShare"); //是否设为新手专享(0-是、1-否)
        String productId = request.getParameter("productId"); //产品id
        String id = ""; //循环产品id
        String result = "0"; //状态(0-成功、1-失败)

        try{

            List<Map<String,String>> listMap = productService.getNewShareList("0"); //获取已推荐的项目
            if (listMap != null && listMap.size() > 0){
                for (Map<String,String> map : listMap){
                    id = map.get("productId"); //产品id
                    result = productService.setNewShare(id,"1"); //将已推荐的项目更新成未推荐
                    if (result.equals("1")){
                        break;
                    }
                }
            }

            if (result.equals("0")){
                result = productService.setNewShare(productId,newShare); //设置共享状态
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("result",result); //广告详细信息拼接
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 获取需显示数字页码(会员)
     *
     * @param currentPage
     * @param totalCount
     * @return
     */
    public String getShowPageMember(String currentPage, int totalCount) {
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if (currentPage != null && !currentPage.equals("")) { //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if (totalCount % 10 == 0) {
            totalPage = totalCount / 10;
        } else {
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if (totalPage <= 10) {
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        } else if (totalPage > 10) {
            if (currentin - 4 <= 1) {
                showStartPage = 1;
                showEndPage = 10;
            } else if (currentin - 4 > 1) {
                if (currentin + 5 > totalPage) {
                    showStartPage = currentin - (10 - (totalPage - currentin + 1));
                    showEndPage = totalPage;
                } else {
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for (int i = showStartPage; i <= showEndPage; i++) {
            if (currentin == i) {
                style = "style=\"color:#C63;font-weight:bold;\"";
            } else {
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doProductMemberGoPage('" + i + "')\" " + style + ">" + i + "</a></td>";
        }
        return str;
    }


    /**
     * 获取需显示数字页码(产品)
     *
     * @param currentPage
     * @param totalCount
     * @return
     */
    public String getShowPage(String currentPage, int totalCount) {
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if (currentPage != null && !currentPage.equals("")) { //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if (totalCount % 10 == 0) {
            totalPage = totalCount / 10;
        } else {
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if (totalPage <= 10) {
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        } else if (totalPage > 10) {
            if (currentin - 4 <= 1) {
                showStartPage = 1;
                showEndPage = 10;
            } else if (currentin - 4 > 1) {
                if (currentin + 5 > totalPage) {
                    showStartPage = currentin - (10 - (totalPage - currentin + 1));
                    showEndPage = totalPage;
                } else {
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for (int i = showStartPage; i <= showEndPage; i++) {
            if (currentin == i) {
                style = "style=\"color:#C63;font-weight:bold;\"";
            } else {
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doProductGoPage('" + i + "')\" " + style + ">" + i + "</a></td>";
        }
        return str;
    }


    public String getShowPageInvestHistory(String currentPage, int totalCount) {
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if (currentPage != null && !currentPage.equals("")) { //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if (totalCount % 10 == 0) {
            totalPage = totalCount / 10;
        } else {
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if (totalPage <= 10) {
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        } else if (totalPage > 10) {
            if (currentin - 4 <= 1) {
                showStartPage = 1;
                showEndPage = 10;
            } else if (currentin - 4 > 1) {
                if (currentin + 5 > totalPage) {
                    showStartPage = currentin - (10 - (totalPage - currentin + 1));
                    showEndPage = totalPage;
                } else {
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for (int i = showStartPage; i <= showEndPage; i++) {
            if (currentin == i) {
                style = "style=\"color:#C63;font-weight:bold;\"";
            } else {
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGoPageInvestHistory('" + i + "')\" " + style + ">" + i + "</a></td>";
        }
        return str;
    }


    /**
     * 获取总页数
     * @param pageSize
     * @param totalCount
     * @return
     */
    public int getPageCount(String pageSize, int totalCount) {
        int totalPage = 0; //总页数
        int pageSizein = 0; //每页显示数量(整型)

        if (pageSize != null && !pageSize.equals("")){
            pageSizein = TypeTool.getInt(pageSize);
        }

        if (totalCount % pageSizein == 0) {
            totalPage = totalCount / pageSizein;
        } else {
            totalPage = totalCount / pageSizein + 1;
        }

        if (totalPage == 0)totalPage=1;
        return totalPage;
    }
}
