package com.mh.peer.controller.weixin;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.LoginMessageBean;
import com.mh.peer.model.message.MyAccountMessageBean;
import com.mh.peer.service.front.UserLoginService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by Cuibin on 2016/7/8.
 */
@Controller
@RequestMapping("/weixin")
public class WeixinLoginController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeixinLoginController.class);
    @Autowired
    private UserLoginService userLoginService;
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private AllMemberService allMemberService;

    /**
     * 用户登录
     */
    @RequestMapping(value = "/userLogin", method = RequestMethod.POST)
    @ResponseBody
    public LoginMessageBean userLogin(@ModelAttribute LoginMessageBean user) {
        // 拿到会话
        HttpSession session = servletUtil.getSession();
        // 设置session
        user.setMessageId(session.getId() + session.getCreationTime());
        // 设置客户端IP
        user.setIp(servletUtil.getRequest().getRemoteAddr());
        LOGGER.info("接受信息==" + user);
        // 调用登录方法
        LoginBusinessBean loginBusinessBean = userLoginService.userLogin(user);
        LOGGER.info("业务消息==" + loginBusinessBean);
        user.setInfo(loginBusinessBean.getInfo());
        user.setResult(loginBusinessBean.getResult());
        session.setAttribute("sessionMemberInfo", loginBusinessBean.getMemberInfo());
        return user;
    }

    /**
     * 跳转到登录页面
     */
    @RequestMapping(value = "/goLogin", method = RequestMethod.GET)
    public String goLogin() {

        return "weixin/weixin_login";
    }
    /**
     * 跳转到帐户信息
     */
    @RequestMapping(value = "/goAsset", method = RequestMethod.GET)
    public String goAsset(ModelMap modelMap){
        MemberInfo memberInfo = (MemberInfo)servletUtil.getSession().getAttribute("sessionMemberInfo");
        if(memberInfo == null){
            return "weixin/weixin_login";
        }
        String memberId = memberInfo.getId();
        MyAccountMessageBean myAccountMes = new MyAccountMessageBean();
        myAccountMes.setId(memberId); //会员id
        MyAccountMessageBean myAccountMessageBean = allMemberService.getMemberInfo(myAccountMes);
        modelMap.put("myAccountMessageBean",myAccountMessageBean);
        return "weixin/weixin_asset_details";
    }
}
