package com.mh.peer.controller.news;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.NewsContentBusinessBean;
import com.mh.peer.model.business.NewsTypeBusinessBean;
import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.message.NewsContentMessageBean;
import com.mh.peer.model.message.NewsTypeMessageBean;
import com.mh.peer.model.message.ProductInvestMessageBean;
import com.mh.peer.model.message.SysFileMessageBean;
import com.mh.peer.service.news.NewsService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.TypeTool;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.UUID;

/**
 * Created by Cuibin on 2016/4/7.
 * 新闻资讯管理controller
 */
@Controller
@RequestMapping("/manage")
public class NewsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private NewsService newsService;
    @Autowired
    private SysFileService sysFileService;

    /**
     * 查询所有新闻内容
     */
    @RequestMapping(value = "/queryNewsByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryNewsByPage(@ModelAttribute NewsContentMessageBean newsContentMes, ModelMap modelMap){

        JetbrickBean bean = new JetbrickBean();
        String pageIndex = newsContentMes.getPageIndex(); //当前数量
        String pageSize = newsContentMes.getPageSize(); //每页显示数量
        String currentPage = newsContentMes.getCurrentPage(); //当前页
        String firstType = newsContentMes.getFirstType(); //第一级别
        String secondType = newsContentMes.getKindId(); //第二级别
        String title = newsContentMes.getTitle(); //题目
        int pageCount = 0; //总页数

        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
            newsContentMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
            newsContentMes.setPageSize(pageSize);
        }

        NewsTypeMessageBean newsTypeMessageBean = new NewsTypeMessageBean();
        //查询所有类别
        List<NewsTypeMessageBean> listNewsType = newsService.queryNewsType(newsTypeMessageBean);
        modelMap.put("listNewsType", listNewsType); //新闻类别

        //新闻集合
        List<NewsContentMessageBean> listNewsContentMes = newsService.queryNewsByP(newsContentMes);
        //新闻数量
        int totalCount = newsService.queryNewsCount(newsContentMes);
        pageCount = this.getPageCount(pageSize,totalCount); //总页数

        modelMap.put("listNewsContentMes", listNewsContentMes); //新闻集合
        modelMap.put("totalCount",totalCount); //新闻数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageCount",pageCount); //总页数

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(newsContentMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 根据条件查询新闻
     * @param newsContentMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryNewsBySome", method = RequestMethod.POST)
    @ResponseBody
    public NewsContentMessageBean queryNewsBySome(@ModelAttribute NewsContentMessageBean newsContentMes, ModelMap modelMap){
        String pageIndex = newsContentMes.getPageIndex(); //当前数量
        String pageSize = newsContentMes.getPageSize(); //每页显示数量
        String currentPage = newsContentMes.getCurrentPage(); //当前页
        String firstType = newsContentMes.getFirstType(); //第一级别
        String secondType = newsContentMes.getKindId(); //第二级别
        String title = newsContentMes.getTitle(); //题目
        String newsStr = ""; //新闻部分拼接
        int pageCount = 0; //总页数
        int num = 1;

        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
            newsContentMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
            newsContentMes.setPageSize(pageSize);
        }

        //新闻集合
        List<NewsContentMessageBean> listNewsContentMes = newsService.queryNewsByP(newsContentMes);
        //新闻数量
        int totalCount = newsService.queryNewsCount(newsContentMes);
        pageCount = this.getPageCount(pageSize,totalCount); //总页数
        if (listNewsContentMes != null && listNewsContentMes.size() > 0){
            for (NewsContentMessageBean newsContentMessageBean : listNewsContentMes){
                newsStr = newsStr + "<tr>"
                        + "<td>"+num+"</td>"
                        + "<td>"+newsContentMessageBean.getTitle()+"</td>"
                        + "<td>"+newsContentMessageBean.getCreateDate()+"</td>"
                        + "<td>"+newsContentMessageBean.getShowDate()+"</td>"
                        + "<td>"+newsContentMessageBean.getbName()+"</td>"
                        + "<td class=\"caozuo\">"
                        + "<a href=\"JavaScript:void(0)\" onclick=\"showNews('"+newsContentMessageBean.getId()+"')\">"
                        + "编辑"
                        + "</a>&nbsp;&nbsp;"
                        + "<a href=\"JavaScript:void(0)\" onclick=\"deleteNews('"+newsContentMessageBean.getId()+"')\">"
                        + "删除"
                        + "</a>"
                        + "</td>"
                        + "</tr>";
            }
        }
        newsContentMes.setInfo(newsStr);
        newsContentMes.setCurrentPage(currentPage);
        newsContentMes.setCount(TypeTool.getString(pageCount)); //总页数
        return newsContentMes;
    }

    /**
     * 打开新闻添加页面
     * @param newsContentMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openAddNews", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openAddNews(@ModelAttribute NewsContentMessageBean newsContentMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        NewsTypeMessageBean newsTypeMessageBean = new NewsTypeMessageBean();

        //查询所有类别
        List<NewsTypeMessageBean> listNewsType = newsService.queryNewsType(newsTypeMessageBean);
        modelMap.put("listNewsType", listNewsType); //新闻类别

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(newsContentMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 类别管理(查询所有类别)
     */
    @RequestMapping(value = "/queryType", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryType(@ModelAttribute NewsTypeMessageBean newsType, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        //List<NewsTypeMessageBean> listNewsTypeMes = newsService.queryType(); //新闻类别集合
        List<NewsTypeMessageBean> listNewsTypeFirstMes = newsService.queryNewsType(newsType); //查询新闻类别
        String newsId = ""; //新闻主键
        int firstNum = 1; //第一类别排序
        int secondNum = 1; //第二类别排序
        String newsTypeStr = ""; //新闻类型拼接
        if (listNewsTypeFirstMes != null && listNewsTypeFirstMes.size() > 0){
            for (NewsTypeMessageBean newsFirstTypeMessageBean : listNewsTypeFirstMes){
                newsId = newsFirstTypeMessageBean.getId(); //新闻主键
                secondNum = 1;
                newsTypeStr = newsTypeStr + "<tr class=\"table_title\">"
                            + "<td>"+firstNum+"</td>"
                            + "<td>"+newsFirstTypeMessageBean.getName()+"</td>"
                            + "<td>"+newsFirstTypeMessageBean.getDescription()+"</td>"
                            + "<td class=\"bianji\">"
                            + "<a href=\"JavaScript:void(0)\" id=\"platform_bianji\" onclick=\"showEdit('"+newsFirstTypeMessageBean.getName()+"','"+newsFirstTypeMessageBean.getDescription()+"','"+newsFirstTypeMessageBean.getId()+"','"+newsFirstTypeMessageBean.getFid()+"')\">编辑</a>&nbsp;&nbsp;"
                            + "<a href=\"JavaScript:void(0)\" id=\"platform_add\" onclick=\"showAdd('"+newsFirstTypeMessageBean.getId()+"','"+newsFirstTypeMessageBean.getId()+"')\">新增</a>"
                            + "</td>"
                            + "</tr>";
                NewsTypeMessageBean NewsSecondTypeMes = new NewsTypeMessageBean();
                NewsSecondTypeMes.setFid(newsId);
                List<NewsTypeMessageBean> listNewsTypeSecondMes = newsService.queryNewsType(NewsSecondTypeMes); //查询新闻类别
                if (listNewsTypeSecondMes != null && listNewsTypeSecondMes.size() > 0){
                    for (NewsTypeMessageBean newsSecondTypeMessageBean : listNewsTypeSecondMes){
                        newsTypeStr = newsTypeStr + "<tr class=\"table_txt\">"
                                + "<td>"+firstNum+"."+secondNum+"</td>"
                                + "<td>"+newsSecondTypeMessageBean.getName()+"</td>"
                                + "<td>"+newsSecondTypeMessageBean.getDescription()+"</td>"
                                + "<td class=\"bianji\">"
                                + "<a href=\"JavaScript:void(0)\" onclick=\"showEdit2('"+newsSecondTypeMessageBean.getName()+"','"+newsFirstTypeMessageBean.getName()+"','"+newsSecondTypeMessageBean.getId()+"','"+newsSecondTypeMessageBean.getFid()+"')\">编辑</a>&nbsp;&nbsp;"
                                + "<a href=\"JavaScript:void(0)\" onclick=\"deleteTwoNewsType('"+newsSecondTypeMessageBean.getId()+"')\">删除</a>"
                                + "</td>"
                                + "</tr>";
                        secondNum++;
                    }
                }
                firstNum++;
            }
        }
        modelMap.put("newsTypeStr", newsTypeStr);

        try {

            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(newsType.getMenuUrl() == null ? newsType.getJetxName() : newsType.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }






    /**
     * 类别管理-编辑后保存
     */
    @RequestMapping(value = "/saveType", method = RequestMethod.POST)
    @ResponseBody
    public NewsTypeMessageBean saveType(@ModelAttribute NewsTypeMessageBean newsTypeM){
        NewsTypeBusinessBean newsTypeB = newsService.saveType(newsTypeM);
        newsTypeM.setResult(newsTypeB.getResult());
        newsTypeM.setInfo(newsTypeB.getInfo());
        return newsTypeM;
    }


    /**
     * 内容管理 删除新闻
     */
    @RequestMapping(value = "/deleteNews", method = RequestMethod.POST)
    @ResponseBody
    public NewsContentMessageBean deleteNews(@ModelAttribute NewsContentMessageBean newsContentMes){
        NewsContentBusinessBean newsConB = newsService.deleteNews(newsContentMes);
        newsContentMes.setInfo(newsConB.getInfo());
        newsContentMes.setResult(newsConB.getResult());
        return newsContentMes;
    }


    /**
     *内容管理 添加新闻
     */
    @RequestMapping(value="/saveNews", method = RequestMethod.POST)
    @ResponseBody
    public NewsContentBusinessBean saveNews(NewsContentMessageBean newsContentMes, ModelMap modelMap) {
        NewsContentBusinessBean newsContentBusinessBean = newsService.saveNews(newsContentMes);
        String result = newsContentBusinessBean.getResult();
        System.out.println("result======"+result);
        return newsContentBusinessBean;
    }


    /**
     * 进入修改界面
     */
    @RequestMapping(value = "/queryNewsById", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryNewsById(@ModelAttribute NewsContentMessageBean newsContentMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String id = newsContentMes.getId(); //主键
        String secondTypeId = ""; //二级新闻类型(String型)
        String firstTypeId = ""; //一级新闻类型(String型)
        NewsContentMessageBean newsContentMessageBean = newsService.queryNewsById(newsContentMes); //新闻内容部分
        NewsTypeMessageBean newsTypeMessageBean = new NewsTypeMessageBean(); //新闻类别
        List<NewsTypeMessageBean> listNewsTypes = newsService.queryNewsType(newsTypeMessageBean); //新闻类别
        if (newsContentMessageBean != null){
            firstTypeId = newsContentMessageBean.getFirstType(); //一级新闻类别
            secondTypeId = newsContentMessageBean.getKindId(); //二级新闻类别
            modelMap.put("firstTypeId",firstTypeId);
            modelMap.put("secondTypeId",secondTypeId);
            modelMap.put("newsContentMessageBean",newsContentMessageBean);
        }

        modelMap.put("listNewsTypes",listNewsTypes); //新闻集合

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(newsContentMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     *  修改新闻
     */
    @RequestMapping(value="/updateNews", method = RequestMethod.POST)
    @ResponseBody
    public NewsContentBusinessBean updateNews(@ModelAttribute NewsContentMessageBean newsContentMes, ModelMap modelMap){
        NewsContentBusinessBean newsContentBusinessBean = newsService.updateNews(newsContentMes);
        return newsContentBusinessBean;
    }


    /**
     *更新是否显示
     */
    @RequestMapping(value="/updateShow", method = RequestMethod.POST)
    @ResponseBody
    public NewsContentMessageBean updateShow(NewsContentMessageBean newsConM){
        return newsService.updateShow(newsConM);
    }
    /**
     *更新是否推荐
     */
    @RequestMapping(value="/updateRecommend", method = RequestMethod.POST)
    @ResponseBody
    public NewsContentMessageBean updateRecommend(NewsContentMessageBean newsConM){
        return newsService.updateRecommend(newsConM);
    }


    /**
     * 根据一级类别查询二级类别拼接
     * @param newsTypeMes
     * @return
     */
    @RequestMapping(value="/getSecondTypeByFirstType", method = RequestMethod.POST)
    @ResponseBody
    public String getSecondTypeByFirstType(NewsTypeMessageBean newsTypeMes){
        List<NewsTypeMessageBean> listNewsTypeMes = newsService.queryNewsType(newsTypeMes);
        String secondTypeId = newsTypeMes.getId(); //二级类别id
        String seconfTypeIdNew = ""; //二级类别id
        String secondTypeStr = ""; //二级类别拼接
        String selected = ""; //用于判断是否被选中
        if (listNewsTypeMes != null && listNewsTypeMes.size() > 0){
            secondTypeStr = "<option value=\"\">请选择</option>";
            for (NewsTypeMessageBean newsTypeMessageBean : listNewsTypeMes){
                seconfTypeIdNew = newsTypeMessageBean.getId(); //新二级类别id
                if (seconfTypeIdNew != null && !seconfTypeIdNew.equals("")){
                    if (seconfTypeIdNew.equals(secondTypeId)){
                        selected = "selected"; //被选中
                    }else{
                        selected = "";
                    }
                    secondTypeStr = secondTypeStr + "<option value=\""+seconfTypeIdNew+"\">"
                                  + newsTypeMessageBean.getName()
                                  + "</option>";
                }
            }
        }
        return secondTypeStr;
    }

    /**
     * 添加二级类别
     */
    @RequestMapping(value = "/addTwoType", method = RequestMethod.POST)
    @ResponseBody
    public NewsTypeMessageBean addTwoType(@ModelAttribute NewsTypeMessageBean newsTypeM){
        return newsService.addTwoType(newsTypeM);
    }
    /**
     * 删除二级类别
     */
    @RequestMapping(value = "/deleteTwoNewsType", method = RequestMethod.POST)
    @ResponseBody
    public NewsTypeMessageBean deleteTwoNewsType(@ModelAttribute NewsTypeMessageBean newsTypeM){
        return newsService.deleteTwoNewsType(newsTypeM);
    }
    /**
     * 获取需显示数字页码
     */
    public String getShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";
        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGoNewsPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }


    /**
     * 获取总页数
     * @param pageSize
     * @param totalCount
     * @return
     */
    public int getPageCount(String pageSize, int totalCount) {
        int totalPage = 0; //总页数
        int pageSizein = 0; //每页显示数量(整型)

        if (pageSize != null && !pageSize.equals("")){
            pageSizein = TypeTool.getInt(pageSize);
        }

        if (totalCount % pageSizein == 0) {
            totalPage = totalCount / pageSizein;
        } else {
            totalPage = totalCount / pageSizein + 1;
        }

        return totalPage;
    }
}
