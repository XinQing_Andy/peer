package com.mh.peer.controller.appMobile;

import com.mh.peer.model.message.*;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.product.ProductBuyService;
import com.mh.peer.service.product.ProductService;
import com.mh.peer.util.HuanXunInfo;
import com.mh.peer.util.MD5Util;
import com.mh.peer.util.Pdes;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-7-12.
 */
@Controller
@RequestMapping("/appProductBuy")
public class AppProductBuyController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppProductBuyController.class);
    @Autowired
    private ProductBuyService productBuyService;
    @Autowired
    private ProductService productService;
    @Autowired
    private HuanXunRegistService huanXunRegistService;

    /**
     * 获取投资项目
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getInvestProject", method = RequestMethod.POST)
    @ResponseBody
    public void getInvestProject(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String typeId = request.getParameter("typeId"); //产品类型id
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String investProjectStr = ""; //投资项目拼接
        String typeName = ""; //产品类型名称

        try{

            if (pageIndex == null || pageIndex.equals("")){ //当前数
                pageIndex = "1";
            }
            if (pageSize == null || pageSize.equals("")){ //每次显示数量
                pageSize = "5";
            }

            List<AppProductBuyMessageBean> listAppProductBuyMes = productBuyService.getInvestProject(memberId,typeId,pageIndex,pageSize);
            if (listAppProductBuyMes != null && listAppProductBuyMes.size() > 0){
                for (AppProductBuyMessageBean appProductBuyMessageBean : listAppProductBuyMes){
                    typeId = appProductBuyMessageBean.getTypeId(); //产品类型id
                    if (typeId != null && !typeId.equals("")){
                        if (typeId.equals("26")){ //车贷宝
                            typeName = "车";
                        }else if (typeId.equals("27")){ //房贷宝
                            typeName = "房";
                        }else if (typeId.equals("28")){ //融贷宝
                            typeName = "融";
                        }else if (typeId.equals("29")){ //票贷宝
                            typeName = "票";
                        }
                    }

                    investProjectStr = investProjectStr + "<div class=\"invested_car\" onclick=\"doProductDetail('"+appProductBuyMessageBean.getProductId()+"');\">"
                                     + "<ul>"
                                     + "<li>"
                                     + "<span class=\"invested_che\">"+typeName+"</span>"
                                     + "<span class=\"invested_title\">&nbsp;"+appProductBuyMessageBean.getTitle()+"</span>"
                                     + "<span class=\"icon-app-05 btn_invested\"></span>"
                                     + "</li>"
                                     + "</ul>"
                                     + "</div>"
                                     + "<table class=\"invested_pro_table\">"
                                     + "<tr onclick=\"doProductDetail('"+appProductBuyMessageBean.getProductId()+"');\">"
                                     + "<td style=\"width: 40%;\">"
                                     + "<span class=\"invested_shuju\">"+appProductBuyMessageBean.getLoanRate()+"</span>"
                                     + "<span style=\"color: #FF2021;\">%</span>"
                                     + "</td>"
                                     + "<td>到期时间：<span class=\"invested_date\">"+appProductBuyMessageBean.getEndTime()+"</span></td>"
                                     + "</tr>"
                                     + "<tr>"
                                     + "<td>年利率</td>"
                                     + "<td>投资金额(元)：<span class=\"invested_money\">"+appProductBuyMessageBean.getPrice()+"元</span></td>"
                                     + "</tr>"
                                     + "</table>";
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("investProjectStr",investProjectStr); //投资项目
        output(response, JSONObject.fromObject(in).toString());
    }



    /**
     * 投资部分资金冻结
     * @param request
     * @param response
     */
    @RequestMapping(value = "/appFreeze", method = RequestMethod.POST)
    @ResponseBody
    public void appFreeze(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String projectNo = request.getParameter("projectNo"); //项目ID号
        String trdAmt = request.getParameter("trdAmt"); //冻结金额(String型)
        Double trdAmtDou = 0.0d; //冻结金额(Double型)
        String productId = request.getParameter("productId"); //产品id
        String ipsAcctNo = ""; //冻结账号
        String Uid = ""; //主键
        String operationType = "trade.freeze"; //操作类型(trade.freeze)
        String merchantID = "1810060028"; //商户存管交易账号
        String merBillNo = "181006" + UUID.randomUUID().toString().replaceAll("-", ""); //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //冻结日期
        String bizType = "1"; //业务类型(1-投标、2-债权转让、3-还款)
        String regType = "1"; //登记方式(1-手动、2-自动)
        String contractNo = ""; //合同号
        String authNo = ""; //授权号
        String merFee = "0"; //平台手续费
        String freezeMerType = "1"; //冻结方类型(1-用户、2-商户)
        String otherIpsAcctNo = ""; //它方账号
        String webUrl = "http://jianbing88.com/appProductBuy/freezeWebResult"; //页面返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/investhxResult"; //后台通知地址
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String requesthx = ""; //请求信息
        String requesthxJm = ""; //请求信息(加密)
        String md5Zs = ""; //md5证书
        String applyMemberId = ""; //申请人
        String bidFee = ""; //最低投标金额(String型)
        Double bidFeeDou = 0.0d; //最低投标金额(Double型)
        String splitCount = ""; //最大拆分份数(String型)
        int splitCountin = 0; //最大拆分份数(int型)
        Double loadFee = 0.0d; //贷款金额(Double型)
        AppProductDetailMessageBean appProductDetailMes = new AppProductDetailMessageBean(); //产品详情信息
        ProductInvestMessageBean productInvestMes = new ProductInvestMessageBean(); //项目投资
        List<HuanXunRegistMessageBean> listHuanXunRegistMes = new ArrayList<HuanXunRegistMessageBean>(); //注册环迅会员信息
        HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean();
        String resultInfo = ""; //错误信息

        try {


            if (memberId == null || memberId.equals("")) { //该会员未登录
                resultInfo = "会话已过期，请重新登录！";
            }else{
                appProductDetailMes = productService.getInvestedProductInfo(productId); //根据id查询产品
                if (appProductDetailMes != null){
                    applyMemberId = appProductDetailMes.getApplyMember(); //申请人id
                    bidFee = appProductDetailMes.getBidFee(); //最低投标金额(String型)
                    if (bidFee != null && !bidFee.equals("")){
                        bidFeeDou = TypeTool.getDouble(bidFee); //最低投标金额(Double型)
                    }
                    splitCount = appProductDetailMes.getSplitCount(); //最大拆分份数(String型)
                    if (splitCount != null && !splitCount.equals("")){
                        splitCountin = TypeTool.getInt(splitCount); //最大拆分份数(int型)
                    }
                    loadFee = bidFeeDou * splitCountin; //贷款金额
                }

                if(memberId.equals(applyMemberId)){
                    resultInfo = "该产品由您发起，不允许对自己发起的产品进行投资！";
                }else{
                    if (trdAmt != null && !trdAmt.equals("")){ //冻结金额
                        trdAmtDou = TypeTool.getDouble(trdAmt);
                    }
                    productInvestMes.setProductId(productId); //产品id
                    productInvestMes.setBuyer(memberId); //购买人
                    int count = productBuyService.getCountBySome(productInvestMes); //购买数量(用于判断当前登录人是否已购买)
                    if(count > 0){ //已购买
                        resultInfo = "您已投资该产品，不能重复投资！";
                    }else{
                        if (bidFeeDou > trdAmtDou) {
                            resultInfo = "该产品最低投标金额为"+bidFeeDou+"元";
                        }else {
                            if (loadFee - trdAmtDou < bidFeeDou && loadFee - trdAmtDou > 0){
                                resultInfo = "剩余金额不足以再进行下次投标，请填写合法的金额!";
                            }else{
                                huanXunRegistMessageBean.setMemberId(memberId);
                                listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
                                if (listHuanXunRegistMes != null && listHuanXunRegistMes.size() > 0){
                                    ipsAcctNo = listHuanXunRegistMes.get(0).getIpsAcctNo(); //IPS冻结账号
                                }
                                Uid = UUID.randomUUID().toString().replaceAll("-", ""); //主键
                                s2SUrl = s2SUrl + "?id=" + Uid+"&memberId="+memberId+"&productId="+productId+"&freezetype=1"; //后台通知地址
                                HuanXunInfo huanXunInfo = new HuanXunInfo();
                                md5Zs = huanXunInfo.getMd5Zs(); //md5证书

                                requesthx = "{\"projectNo\":\"" + projectNo + "\",\"merBillNo\":\"" + merBillNo + "\",\"bizType\":\"" + bizType + "\"," +
                                        "\"regType\":\"" + regType + "\",\"contractNo\":\"" + contractNo + "\",\"authNo\":\"" + authNo + "\",\"trdAmt\":\"" + trdAmt + "\"," +
                                        "\"merFee\":\"0\",\"freezeMerType\":\"" + freezeMerType + "\",\"otherIpsAcctNo\":\"" + otherIpsAcctNo + "\"," +
                                        "\"ipsAcctNo\":\"" + ipsAcctNo + "\",\"merDate\":\"" + merDate + "\",\"webUrl\":\"" + webUrl + "\",\"s2SUrl\":\"" + s2SUrl + "\"}";

                                System.out.println("requesthx======"+requesthx);
                                requesthxJm = Pdes.encrypt3DES( requesthx); //3des加密
                                sign = operationType + merchantID + requesthxJm + md5Zs; //签名
                                signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密
                            }
                        }
                    }
                }
            }

            HashMap<String,Object> in = null;
            in = new HashMap<String,Object>();
            in.put("sign",signJm); //签名
            in.put("request",requesthxJm); //请求信息
            in.put("resultInfo",resultInfo); //错误信息
            output(response, JSONObject.fromObject(in).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 提现返回页面(投资冻结)
     * @param request
     * @param response
     */
    @RequestMapping(value = "/freezeWebResult", method = RequestMethod.POST)
    @ResponseBody
    public void postalWebResult(HttpServletRequest request,HttpServletResponse response){
        try{
            response.sendRedirect("freezeWait"); //返回冻结等待页面
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 环迅等待界面(投资冻结)
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/freezeWait", method = RequestMethod.GET)
    public String postalWait(HttpServletRequest request,HttpServletResponse response){
        return "appMobile/investFreeze";
    }

    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
