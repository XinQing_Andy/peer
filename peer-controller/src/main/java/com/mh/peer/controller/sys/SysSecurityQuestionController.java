package com.mh.peer.controller.sys;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.business.SysSecurityParameterBusinessBean;
import com.mh.peer.model.business.SysSecurityQuestionBusinessBean;
import com.mh.peer.model.message.SysSecurityParameterMessageBean;
import com.mh.peer.model.message.SysSecurityQuestionMessageBean;
import com.mh.peer.model.message.SysUserMessageBean;
import com.mh.peer.service.sys.SysSecurityQuestionService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-1.
 */
@Controller
@RequestMapping("/sysSecurityQuestion")
public class SysSecurityQuestionController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private SysSecurityQuestionService sysSecurityQuestionService;


    /**
     * 根据分页获取安全问题信息
     * @param sysSecurityQuestionMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/querySecurityQuestionByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean querySecurityQuestionByPage(@ModelAttribute SysSecurityQuestionMessageBean sysSecurityQuestionMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = sysSecurityQuestionMes.getPageIndex(); //当前数
        String pageSize = sysSecurityQuestionMes.getPageSize(); //每页显示数量
        String currentPage = sysSecurityQuestionMes.getCurrentPage(); //当前页
        String question = sysSecurityQuestionMes.getQuestion(); //问题

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
        }
        if(question != null && !question.equals("")){ //问题
            modelMap.addAttribute("question",question);
        }


        sysSecurityQuestionMes.setPageIndex(pageIndex); //当前页
        sysSecurityQuestionMes.setPageSize(pageSize); //每页显示数量
        //安全问题集合(根据分页查询)
        List<SysSecurityQuestionMessageBean> sysSecurityQuestionList = sysSecurityQuestionService.querySecurityQuestionByPage(sysSecurityQuestionMes);
        int totalCount = sysSecurityQuestionService.getCountSecurityQuestion(sysSecurityQuestionMes); //安全问题数量
        String pageStr = this.getShowPage(currentPage,totalCount); //显示所属分页

        //将list传递给前台渲染
        modelMap.put("sysSecurityQuestionList", sysSecurityQuestionList); //安全问题集合
        modelMap.put("totalCount", totalCount); //产品数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(sysSecurityQuestionMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 新增安全问题
     * @param sysSecurityQuestionMess
     * @return
     */
    @RequestMapping(value = "/addSysSecurityQuestion")
    @ResponseBody
    public SysSecurityQuestionMessageBean addSysSecurityQuestion(@ModelAttribute SysSecurityQuestionMessageBean sysSecurityQuestionMess){

        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        String userName= bean.getSysUser().getUserName();
        sysSecurityQuestionMess.setCreateUser(userName); //创建人
        sysSecurityQuestionMess.setUpdateUser(userName); //更新人
        sysSecurityQuestionMess.setCreateTime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss")); //创建时间
        sysSecurityQuestionMess.setUpdateTime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss")); //最后更新时间
        sysSecurityQuestionMess.setFormat("文本"); //问题格式
        sysSecurityQuestionMess.setActive_flg("0"); //是否启用(0-启用、1-暂停)

        //新增安全问题
        SysSecurityQuestionBusinessBean sysSecurityQuestionBus = sysSecurityQuestionService.addSysSecurityQuestion(sysSecurityQuestionMess);
        sysSecurityQuestionMess.setResult(sysSecurityQuestionBus.getResult());
        sysSecurityQuestionMess.setInfo(sysSecurityQuestionBus.getInfo());

        return sysSecurityQuestionMess;
    }


    /**
     * 修改安全问题
     * @param sysSecurityQuestionMess
     * @return
     */
   /* @RequestMapping(value = "/updateSysSecurityQuestion")
    @ResponseBody
    public SysSecurityQuestionMessageBean updateSysSecurityQuestion(@ModelAttribute SysSecurityQuestionMessageBean sysSecurityQuestionMess){
        //System.out.println("sysSecurityQuestionMess======"+sysSecurityQuestionMess); //安全问题信息类
        //修改安全问题
        SysSecurityQuestionBusinessBean sysSecurityQuestionBus = sysSecurityQuestionService.updateSysSecurityQuestion(sysSecurityQuestionMess);

        //设置返回安全参数消息
        sysSecurityQuestionMess.setResult(sysSecurityQuestionBus.getResult()); //状态(成功或失败)
        sysSecurityQuestionMess.setInfo(sysSecurityQuestionBus.getInfo()); //错误信息
        return sysSecurityQuestionMess;
    }*/


    /**
     * 设置安全问题状态(0-正常、暂停)
     * @param sysSecurityQuestionMess
     * @return
     */
    @RequestMapping(value = "/setSysSecurityQuestion")
    @ResponseBody
    public SysSecurityQuestionMessageBean setSysSecurityQuestion(@ModelAttribute SysSecurityQuestionMessageBean sysSecurityQuestionMess){

        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        String userName= bean.getSysUser().getUserName();
        sysSecurityQuestionMess.setUpdateUser(userName); //更新人
        sysSecurityQuestionMess.setUpdateTime(StringTool.getString(DaoTool.getTime(), "yyyyMMddHHmmss")); //最后更新时间

        //修改安全问题
        SysSecurityQuestionBusinessBean sysSecurityQuestionBus = sysSecurityQuestionService.updateSysSecurityQuestion(sysSecurityQuestionMess);

        //设置返回安全参数消息
        sysSecurityQuestionMess.setResult(sysSecurityQuestionBus.getResult()); //状态(成功或失败)
        sysSecurityQuestionMess.setInfo(sysSecurityQuestionBus.getInfo()); //错误信息
        return sysSecurityQuestionMess;
    }


    /**
     * 获取需显示数字页码
     * @param currentPage
     * @param totalCount
     * @return
     */
    public String getShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 < 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = 10 -(totalPage - currentin + 1);
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doSysProblemGoPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }
}
