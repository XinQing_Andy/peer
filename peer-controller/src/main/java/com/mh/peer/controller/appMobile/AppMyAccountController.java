package com.mh.peer.controller.appMobile;

import com.mh.peer.model.message.AppMyAccountMessageBean;
import com.mh.peer.service.member.AllMemberService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * Created by zhangerxin on 2016-7-14.
 */
@Controller
@RequestMapping("/appMyAccount")
public class AppMyAccountController {

    @Autowired
    private AllMemberService allMemberService;

    /**
     * 我的账户资产统计部分
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getPriceTj", method = RequestMethod.POST)
    @ResponseBody
    public void getPriceTj(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String balance = "0"; //余额
        String receivedPrice = "0"; //待收总额
        String accountPrice = "0"; //账户总资产

        try{

            AppMyAccountMessageBean appMyAccountMessageBean = allMemberService.getPriceTj(memberId);
            if (appMyAccountMessageBean != null){
                balance = appMyAccountMessageBean.getBalance(); //余额
                receivedPrice = appMyAccountMessageBean.getReceivedPrice(); //待收总额
                accountPrice = appMyAccountMessageBean.getAccountPrice(); //账户总资产
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("balance",balance); //余额
        in.put("receivedPrice",receivedPrice); //待收总额
        in.put("accountPrice",accountPrice); ///账户总资产
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
