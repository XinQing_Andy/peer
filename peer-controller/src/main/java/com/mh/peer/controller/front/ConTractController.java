package com.mh.peer.controller.front;

import com.mh.peer.model.message.ConTractMessageBean;
import com.mh.peer.service.front.ConTractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by zhangerxin on 2016-8-31.
 * 合同部分
 */
@Controller
@RequestMapping("/conTract")
public class ConTractController {

    @Autowired
    private ConTractService conTractService;

    /**
     * 打开产品合同部分
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openProductContract", method = RequestMethod.GET)
    public String openProductContract(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap){
        String investId = request.getParameter("investId"); //投资id
        String productId = request.getParameter("productId"); //产品id
        ConTractMessageBean conTractProductInfoMessageBean = conTractService.getProductInfo(productId); //获取产品信息
        modelMap.put("conTractProductInfoMessageBean",conTractProductInfoMessageBean); //获取产品信息
        List<ConTractMessageBean> listConTractProductBuyInfoMessageBean = conTractService.getProductBuyInfoList(productId);
        modelMap.put("listConTractProductBuyInfoMessageBean",listConTractProductBuyInfoMessageBean); //获取产品投资信息
        return "front/contract/productContract";
    }


    /**
     * 打开债权合同书
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openAttornContract", method = RequestMethod.GET)
    public String openAttornContract(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap){
        String attornId = request.getParameter("attornId"); //债权id
        String investId = request.getParameter("investId"); //投资id
        String productId = request.getParameter("productId"); //产品id
        ConTractMessageBean conTractAttornMessageBean = conTractService.getAttornInfo(attornId); //获取债权部分信息
        modelMap.put("conTractAttornMessageBean",conTractAttornMessageBean); //获取债权信息
        return "front/contract/AttornContract";
    }
}
