package com.mh.peer.controller.appMobile;

import com.mh.peer.model.message.*;
import com.mh.peer.service.front.UserLoginService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.MD5Util;
import com.mh.peer.util.PublicAddress;
import com.mh.peer.util.PublicsTool;
import com.mh.peer.util.SendMobileMessage;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-6-20.
 */
@Controller
@RequestMapping("/appLogin")
public class AppMemberController {

    @Autowired
    private UserLoginService userLoginService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private SysFileService sysFileService;

    /**
     * 登录操作(app端)
     * @param request
     * @param response
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public void login(HttpServletRequest request,HttpServletResponse response){
        String userName = request.getParameter("userName"); //用户名
        String password = request.getParameter("password"); //密码
        String passwordJm = ""; //密码(加密)
        AppMemberMessageBean appMemberMessageBean = new AppMemberMessageBean();
        String resultCode = ""; //返回标志
        String memberId = ""; //会员id

        try{

            passwordJm = MD5Util.MD5(password); //密码(加密)
            appMemberMessageBean.setMemberUserName(userName); //用户名
            appMemberMessageBean.setPassword(passwordJm); //密码
            appMemberMessageBean = userLoginService.login(appMemberMessageBean);
            if(appMemberMessageBean != null){
                resultCode = appMemberMessageBean.getResultCode(); //返回标志
                memberId = appMemberMessageBean.getId(); //会员id
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("memberId",memberId); //会员id
        in.put("resultCode", resultCode); //返回标志
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 发送验证码(注册时)
     * @param request
     * @param response
     */
    @RequestMapping(value = "/sendYzm", method = RequestMethod.POST)
    @ResponseBody
    public void sendYzm(HttpServletRequest request,HttpServletResponse response){
        PublicsTool publicsTool = new PublicsTool();
        String mobileNumber = request.getParameter("mobileNumber"); //手机号
        String random = publicsTool.getRandom(); //验证码
        String content = "【煎饼金融P2P】验证码:" + random + "，请妥善保管，验证码提供给他人将导致信息泄露";
        SendMobileMessage sendMobileMessage = new SendMobileMessage(); //手机发送短信
        AppMemberMessageBean appMemberMessageBean = new AppMemberMessageBean();
        String memberCount = "0"; //会员数量
        String sendResult = "0"; //发送结果

        try{

            appMemberMessageBean.setMemberUserName(mobileNumber); //用户名
            appMemberMessageBean.setDelFlg("1"); //是否删除(0-是、1-否)
            memberCount = userLoginService.getMemberCount(appMemberMessageBean); //会员数量
            if (memberCount.equals("0")){ //可以注册
                sendResult = sendMobileMessage.send(mobileNumber,content); //发送结果
                System.out.println("验证码为："+random);
            }

        }catch(Exception e){
            sendResult = "1";
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("memberCount",memberCount); //会员数量
        in.put("random",random); //验证码
        in.put("sendResult", sendResult); //返回标志
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 发送验证码(忘记密码时)
     * @param request
     * @param response
     */
    @RequestMapping(value = "/sendYzmForget", method = RequestMethod.POST)
    @ResponseBody
    public void sendYzmForget(HttpServletRequest request,HttpServletResponse response){
        PublicsTool publicsTool = new PublicsTool();
        String mobileNumber = request.getParameter("mobileNumber"); //手机号
        String random = publicsTool.getRandom(); //验证码
        String content = "【煎饼金融P2P】验证码:" + random + "，请妥善保管，验证码提供给他人将导致信息泄露";
        String id = ""; //主键
        SendMobileMessage sendMobileMessage = new SendMobileMessage(); //手机发送短信
        AppMemberMessageBean appMemberMessageBean = new AppMemberMessageBean();
        String sendResult = "0"; //发送结果

        try{

            appMemberMessageBean.setMemberUserName(mobileNumber); //用户名
            appMemberMessageBean.setDelFlg("1"); //是否删除(0-是、1-否)
            appMemberMessageBean = userLoginService.getMemberInfo(appMemberMessageBean);
            if (appMemberMessageBean != null){
                id = appMemberMessageBean.getId(); //主键
            }
            if (id != null && !id.equals("")){ //信息正确
                sendResult = sendMobileMessage.send(mobileNumber,content); //发送结果
                System.out.println("验证码为："+random);
            }

        }catch(Exception e){
            sendResult = "1";
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("id",id); //会员id
        in.put("random",random); //验证码
        in.put("sendResult", sendResult); //返回标志
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 修改密码
     * @param request
     * @param response
     */
    @RequestMapping(value = "/undatePassword", method = RequestMethod.POST)
    @ResponseBody
    public void undatePassword(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String password = request.getParameter("password"); //密码
        String passwordJm = ""; //密码(加密)
        String result = "0"; //修改结果(0-成功、1-失败)

        try{

            if (password != null && !password.equals("")){
                passwordJm = MD5Util.MD5(password); //密码(加密)
                AppMemberMessageBean appMemberMessageBean = userLoginService.updatePassword(memberId,passwordJm);
                if (appMemberMessageBean.getResultCode().equals("success")){
                    result = "0";
                }else{
                    result = "1";
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            result = "1";
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("result",result); //修改结果
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 手机注册
     * @param request
     * @param response
     */
    @RequestMapping(value = "/regist", method = RequestMethod.POST)
    @ResponseBody
    public void regist(HttpServletRequest request,HttpServletResponse response){
        String mobileNumber = request.getParameter("mobileNumber"); //手机号
        String password = request.getParameter("password"); //密码
        String passwordJm = ""; //密码(加密)
        String loginType = "1"; //登录类型(0-电脑端、1-手机端)
        String Uid = UUID.randomUUID().toString().replaceAll("-",""); //主键
        String result = "0"; //注册状态(0-成功、1-失败)
        String memberCount = "0"; //会员数量
        AppMemberMessageBean appMemberMessageBean = new AppMemberMessageBean();

        try{

            /*************** 会员用户名检验Start ***************/
            appMemberMessageBean.setMemberUserName(mobileNumber); //用户名
            appMemberMessageBean.setDelFlg("1"); //是否删除(0-是、1-否)
            memberCount = userLoginService.getMemberCount(appMemberMessageBean);
            /**************** 会员用户名检验End ****************/

            if(memberCount.equals("0")){ //会员数量
                passwordJm = MD5Util.MD5(password); //密码(加密)
                appMemberMessageBean.setId(Uid); //主键
                appMemberMessageBean.setTelephone(mobileNumber); //手机
                appMemberMessageBean.setPassword(passwordJm); //密码
                appMemberMessageBean.setLoginType("1"); //登录类型(0-电脑端、1-手机端)
                appMemberMessageBean = userLoginService.regist(appMemberMessageBean); //注册
                if(appMemberMessageBean.getResultCode().equals("error")){
                    result = "1"; //注册失败
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("Uid",Uid); //主键
        in.put("memberCount",memberCount); //会员数量
        in.put("result", result); //注册状态
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取会员信息
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppMemberInfo", method = RequestMethod.POST)
    @ResponseBody
    public void getAppMemberInfo(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String address = ""; //服务器地址
        String picUrl = ""; //图片拼接地址
        String headPicStr = ""; //图片部分拼接
        PublicAddress publicAddress = new PublicAddress();
        address = publicAddress.getServerAddress(); //服务器地址
        String memberInfoStr = ""; //会员信息拼接
        AppSysFileMessageBean appSysFileMessageBean = sysFileService.getAppSysFile(memberId,"CREATETIME","DESC","1","1");
        AppMemberInfoMessageBean appMemberInfoMessageBean = allMemberService.getAppMemberInfo(memberId); //会员详细信息

        if (appSysFileMessageBean != null){
            picUrl = address + appSysFileMessageBean.getImgUrl(); //图片地址
            headPicStr = "<img src=\""+picUrl+"\" style=\"width:130px;height:130px;border-radius:50%;\"/>";
        }else{
            headPicStr = "<img src=\"img/wd_03.png\" style=\"width:130px;height:130px;border-radius:50%;\"/>";
        }

        if (appMemberInfoMessageBean != null){
            memberInfoStr = memberInfoStr + "<div class=\"person_sj\">"
                          + "<ul>"
                          + "<li class=\"sj_jf\">"
                          + appMemberInfoMessageBean.getPoints()
                          + "<span>当前积分</span>"
                          + "</li>"
                          + "<li class=\"sj_lj\">"
                          + appMemberInfoMessageBean.getInterest()
                          + "<span>累计收益(元)</span>"
                          + "</li>"
                          + "<li class=\"sj_ky\">"
                          + appMemberInfoMessageBean.getBalance()
                          + "<span>可用金额(元)</span>"
                          + "</li>"
                          + "</ul>"
                          + "</div>"
                          + "<div class=\"person_choose\">"
                          + "<table class=\"person_table\" style=\"border-collapse:collapse;\">"
                          + "<tr>"
                          + "<td id=\"schemes\" onclick=\"doReceivePlan()\">"
                          + "<span class=\"icon-app-12 person_icon\"></span>"
                          + "<span class=\"pt_tit\">回款计划</span>"
                          + "<span class=\"pt_money\">待收金额(元)</span>"
                          + "<span class=\"pt_money\">"+appMemberInfoMessageBean.getReceivedPrice()+"元</span>"
                          + "</td>"
                          + "<td id=\"invested_project\" onclick=\"doInvestProject()\">"
                          + "<span class=\"icon-app-13 person_icon\"></span>"
                          + "<span class=\"pt_tit\">投资项目</span>"
                          + "<span class=\"pt_money\">投资"+appMemberInfoMessageBean.getInvestCount()+"笔</span>"
                          + "<span class=\"pt_money\" style=\"opacity:0;\">0</span>"
                          + "</td>"
                          + "<td style=\"border-right:none;\" id=\"record\" onclick=\"doTradeRecord()\">"
                          + "<span class=\"icon-app-14 person_icon\"></span>"
                          + "<span class=\"pt_tit\">交易记录</span>"
                          + "<span class=\"pt_money\">查看所有</span>"
                          + "<span class=\"pt_money\" style=\"opacity:0;\">0</span>"
                          + "</td>"
                          + "</tr>"
                          + "<tr>"
                          + "<td id=\"refund\" onclick=\"repayPlan();\">"
                          + "<span class=\"icon-app-15 person_icon\"></span>"
                          + "<span class=\"pt_tit\">还款计划</span>"
                          + "<span class=\"pt_money\">还款金额(元)</span>"
                          + "<span class=\"pt_money\">"+appMemberInfoMessageBean.getRepayedPrice()+"元</span>"
                          + "</td>"
                          + "<td id=\"financing\" onclick=\"productAttorn();\">"
                          + "<span class=\"icon-app-16 person_icon\"></span>"
                          + "<span class=\"pt_tit\">债权转让</span>"
                          + "<span class=\"pt_money\">投资"+appMemberInfoMessageBean.getAttornCount()+"笔</span>"
                          + "<span class=\"pt_money\" style=\"opacity:0;\">0</span>"
                          + "</td>"
                          + "<td style=\"border-right:none;\" id=\"asset\" onclick=\"doSearchAsset()\">"
                          + "<span class=\"icon-app-17 person_icon\"></span>"
                          + "<span class=\"pt_tit\">资产统计</span>"
                          + "<span class=\"pt_money\">查看所有</span>"
                          + "<span class=\"pt_money\" style=\"opacity:0;\">0</span>"
                          + "</td>"
                          + "</tr>"
                          + "</table>"
                          + "</div>";
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("headPicStr",headPicStr); //头像地址拼接
        in.put("memberInfoStr",memberInfoStr); //个人中心部分拼接
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取会员投资榜
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getMemberByInvest", method = RequestMethod.POST)
    @ResponseBody
    public void getMemberByInvest(HttpServletRequest request,HttpServletResponse response){
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String orderColumn = request.getParameter("orderColumn"); //排序字段
        String orderType = request.getParameter("orderType"); //排序类型
        String investStr = ""; //投资实力榜拼接
        int num = 1;
        String classStr = ""; //样式

        try{

            List<AppMemberInvestMessageBean> listAppMemberInvestMes = allMemberService.getMemberByInvest(pageIndex,pageSize,orderColumn,orderType);
            if (listAppMemberInvestMes != null && listAppMemberInvestMes.size()>0){
                for (AppMemberInvestMessageBean appMemberInvestMessageBean : listAppMemberInvestMes){
                    if (num == 1){
                        classStr = "i_t_one";
                    }else if (num == 2){
                        classStr = "i_t_two";
                    }else if (num == 3){
                        classStr = "i_t_three";
                    }else{
                        classStr = "i_t_four";
                    }

                    investStr = investStr + "<tr>"
                              + "<td class=\""+classStr+"\">"
                              + "<span class=\"icon-app-39 icon_king\"></span>"
                              + "<b>"+num+"</b>"
                              + "</td>"
                              + "<td class=\"table_name\">"+appMemberInvestMessageBean.getNickName()+"</td>"
                              + "<td class=\"table_money\">"+appMemberInvestMessageBean.getInvestPrice()+"元</td>"
                              + "</tr>";
                    num++;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("investStr",investStr); //投资风云榜
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取个人信息资料
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppPersonInfo", method = RequestMethod.POST)
    @ResponseBody
    public void getAppPersonInfo(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String nickName = ""; //用户昵称
        String realFlg = ""; //是否实名化认证(0-是、1-否)
        String realFlgName = ""; //实名化认证名称
        String telephoneFlg = ""; //是否手机认证
        String telephoneFlgName = ""; //是否手机认证名称
        String personInfoStr = ""; //个人信息资料拼接

        try{

            AppPersonInfoMessageBean appPersonInfoMessageBean = allMemberService.getAppPersonInfo(memberId);
            if (appPersonInfoMessageBean != null){
                nickName = appPersonInfoMessageBean.getNickName(); //昵称
                if (nickName == null && nickName.equals("")){
                    nickName = "不填写默认手机号为用户昵称";
                }
                realFlg = appPersonInfoMessageBean.getRealFlg(); //是否实名认证(0-是、1-否)

            }
        }catch(Exception e){

        }
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
