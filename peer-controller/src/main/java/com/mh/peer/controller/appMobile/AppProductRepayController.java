package com.mh.peer.controller.appMobile;

import com.mh.peer.model.message.AppProductRepayMessageBean;
import com.mh.peer.model.message.AppProductRepayTjMessageBean;
import com.mh.peer.service.product.ProductRepayService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhangerxin on 2016-7-13.
 * 还款计划
 */
@Controller
@RequestMapping("/appProductRepay")
public class AppProductRepayController {

    @Autowired
    private ProductRepayService productRepayService;

    /**
     * 获取还款信息
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppRepayInfo", method = RequestMethod.POST)
    @ResponseBody
    public void getAppRepayInfo(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String repayFlag = request.getParameter("repayFlag"); //还款状态(0-待还、1-已还)
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String repayInfoTj = ""; //还款信息统计
        String repayInfoList = ""; //还款信息列表
        AppProductRepayTjMessageBean appProductRepayTjMessageBean = productRepayService.getAppRepayInfo(memberId,repayFlag);
        List<AppProductRepayMessageBean> listAppProductRepayMes = productRepayService.getAppRepayList(memberId,repayFlag,pageIndex,pageSize);

        try{

            if (repayFlag != null && !repayFlag.equals("")){
                if (repayFlag.equals("0")){ //待还
                    if (appProductRepayTjMessageBean != null){
                        repayInfoTj = repayInfoTj + "<tr>"
                                    + "<td>"
                                    + "<span>待还产品(笔)</span>"
                                    + "<span>"+appProductRepayTjMessageBean.getDhRepayCount()+"笔</span>"
                                    + "</td>"
                                    + "<td style=\"width:130px;height:130px;border-radius:50%;background-color:#FF2021;\">"
                                    + "<span style=\"color:#FFFFFF;\">待还总额(元)</span>"
                                    + "<span style=\"color:#FFFFFF;\">"+appProductRepayTjMessageBean.getDhPrice()+"元</span>"
                                    + "</td>"
                                    + "<td>"
                                    + "<span>待还期数(期)</span>"
                                    + "<span>"+appProductRepayTjMessageBean.getDhPeriod()+"期</span>"
                                    + "</td>"
                                    + "</tr>";
                    }

                    if (listAppProductRepayMes != null && listAppProductRepayMes.size() > 0){
                        for (AppProductRepayMessageBean appProductRepayMes : listAppProductRepayMes){
                            repayInfoList = repayInfoList + "<tr onclick=\"repayPlanDetail('"+appProductRepayMes.getId()+"')\">"
                                          + "<td>名称<span>"+appProductRepayMes.getTitle()+"</span></td>"
                                          + "<td>"
                                          + "<div style=\"border-right:1px solid #EBEBEB;border-left:1px solid #EBEBEB;line-height:30px;\">"
                                          + "<ul>"
                                          + "<li class=\"schemes_d\">待还总额</li>"
                                          + "<li class=\"schemes_x\">"+appProductRepayMes.getRepayPrice()+"元</li>"
                                          + "</ul>"
                                          + "</div>"
                                          + "</td>"
                                          + "<td>"
                                          + "待还日期<span>"+appProductRepayMes.getYrepaymentTime()+"</span>"
                                          + "</td>"
                                          + "<td id=\"refund_details\">"
                                          + "<span class=\"icon-app-05\"></span>"
                                          + "</td>"
                                          + "</tr>";

                        }
                    }
                }else{ //已还
                    if (appProductRepayTjMessageBean != null){
                        repayInfoTj = repayInfoTj + "<tr>"
                                    + "<td>"
                                    + "<span>已还产品(笔)</span>"
                                    + "<span>"+appProductRepayTjMessageBean.getYhRepayCount()+"笔</span>"
                                    + "</td>"
                                    + "<td style=\"width:130px;height:130px;border-radius:50%;background-color:#FF2021;\">"
                                    + "<span style=\"color:#FFFFFF;\">已还总额(元)</span>"
                                    + "<span style=\"color:#FFFFFF;\">"+appProductRepayTjMessageBean.getYhPrice()+"元</span>"
                                    + "</td>"
                                    + "<td>"
                                    + "<span>已还期数(期)</span>"
                                    + "<span>"+appProductRepayTjMessageBean.getYhPeriod()+"期</span>"
                                    + "</td>"
                                    + "</tr>";
                    }

                    if (listAppProductRepayMes != null && listAppProductRepayMes.size() > 0){
                        for (AppProductRepayMessageBean appProductRepayMes : listAppProductRepayMes){
                            repayInfoList = repayInfoList + "<tr onclick=\"repayPlanDetail('"+appProductRepayMes.getId()+"')\">"
                                    + "<td>名称<span>"+appProductRepayMes.getTitle()+"</span></td>"
                                    + "<td>"
                                    + "<div style=\"border-right:1px solid #EBEBEB;border-left:1px solid #EBEBEB;line-height:30px;\">"
                                    + "<ul>"
                                    + "<li class=\"schemes_d\">实还总额</li>"
                                    + "<li class=\"schemes_x\">"+appProductRepayMes.getRepaymentPrice()+"元</li>"
                                    + "</ul>"
                                    + "</div>"
                                    + "</td>"
                                    + "<td>"
                                    + "实还日期<span>"+appProductRepayMes.getRepaymentTime()+"</span>"
                                    + "</td>"
                                    + "<td id=\"refund_details\">"
                                    + "<span class=\"icon-app-05\"></span>"
                                    + "</td>"
                                    + "</tr>";

                        }
                    }
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("repayInfoTj",repayInfoTj); //还款信息统计
        in.put("repayInfoList",repayInfoList); //还款信息列表
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 还款信息列表
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppRepayList", method = RequestMethod.POST)
    @ResponseBody
    public void getAppRepayList(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String repayFlag = request.getParameter("repayFlag"); //还款状态(0-待还、1-已还)
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String repayInfoList = ""; //还款信息列表
        List<AppProductRepayMessageBean> listAppProductRepayMes = productRepayService.getAppRepayList(memberId,repayFlag,pageIndex,pageSize);

        try{

            if (repayFlag != null && !repayFlag.equals("")){
                if (repayFlag.equals("0")){ //待还

                    if (listAppProductRepayMes != null && listAppProductRepayMes.size() > 0){
                        for (AppProductRepayMessageBean appProductRepayMes : listAppProductRepayMes){
                            repayInfoList = repayInfoList + "<tr onclick=\"repayPlanDetail('"+appProductRepayMes.getId()+"')\">"
                                         + "<td>名称<span>"+appProductRepayMes.getTitle()+"</span></td>"
                                         + "<td>"
                                         + "<div style=\"border-right:1px solid #EBEBEB;border-left:1px solid #EBEBEB;line-height:30px;\">"
                                         + "<ul>"
                                         + "<li class=\"schemes_d\">待还总额</li>"
                                         + "<li class=\"schemes_x\">"+appProductRepayMes.getRepayPrice()+"</li>"
                                         + "</ul>"
                                         + "</div>"
                                         + "</td>"
                                         + "<td>"
                                         + "待还日期<span>"+appProductRepayMes.getYrepaymentTime()+"</span>"
                                         + "</td>"
                                         + "<td id=\"refund_details\">"
                                         + "<span class=\"icon-app-05\"></span>"
                                         + "</td>"
                                         + "</tr>";

                        }
                    }
                }else{ //已还

                    if (listAppProductRepayMes != null && listAppProductRepayMes.size() > 0){
                        for (AppProductRepayMessageBean appProductRepayMes : listAppProductRepayMes){
                            repayInfoList = repayInfoList + "<tr onclick=\"repayPlanDetail('"+appProductRepayMes.getId()+"')\">"
                                         + "<td>名称<span>"+appProductRepayMes.getTitle()+"</span></td>"
                                         + "<td>"
                                         + "<div style=\"border-right:1px solid #EBEBEB;border-left:1px solid #EBEBEB;line-height:30px;\">"
                                         + "<ul>"
                                         + "<li class=\"schemes_d\">实还总额</li>"
                                         + "<li class=\"schemes_x\">"+appProductRepayMes.getRepaymentPrice()+"元</li>"
                                         + "</ul>"
                                         + "</div>"
                                         + "</td>"
                                         + "<td>"
                                         + "实还日期<span>"+appProductRepayMes.getRepaymentTime()+"</span>"
                                         + "</td>"
                                         + "<td id=\"refund_details\">"
                                         + "<span class=\"icon-app-05\"></span>"
                                         + "</td>"
                                         + "</tr>";

                        }
                    }
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("repayInfoList",repayInfoList); //还款信息列表
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取还款详细信息
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppRepayDetailInfo", method = RequestMethod.POST)
    @ResponseBody
    public void getAppRepayDetailInfo(HttpServletRequest request,HttpServletResponse response){
        String productRepayId = request.getParameter("productRepayId"); //还款id
        String repayFlag = request.getParameter("repayFlag"); //还款状态(0-待还、1-已还)
        String repayDetailStr = ""; //还款详细信息

        try{

            AppProductRepayMessageBean appProductRepayMessageBean = productRepayService.getAppRepayDetails(productRepayId);
            if (appProductRepayMessageBean != null){
                if (repayFlag != null && !repayFlag.equals("")){
                    if (repayFlag.equals("0")){ //待还
                        repayDetailStr = "<ul>"
                                       + "<li>"
                                       + "<span><img src=\"../img/hkjh_03.png\" style=\"width:30px;height:30px;\"/></span>"
                                       + "<span class=\"schem_title\">&nbsp;&nbsp;"+appProductRepayMessageBean.getTitle()+"</span>"
                                       + "</li>"
                                       + "</ul>"
                                       + "<ul>"
                                       + "<table class=\"schems_table\">"
                                       + "<tr>"
                                       + "<td><img src=\"../img/hkjh_06.png\" style=\"width:25px;height:25px;\" /></td>"
                                       + "<td>编号：<span>"+appProductRepayMessageBean.getCode()+"</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td><img src=\"../img/hkjh_08.png\" style=\"width:25px;height:25px;\" /></td>"
                                       + "<td>待还日期：<span>"+appProductRepayMessageBean.getYrepaymentTime()+"</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td><img src=\"../img/hkjh_10.png\" style=\"width:25px;height:25px;\" /></td>"
                                       + "<td>待还本金<b style=\"color:#C7C7C7;\">(元)</b>：<span>"+appProductRepayMessageBean.getPrincipal()+"元</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td><img src=\"../img/hkjh_12.png\" style=\"width:25px;height:25px;\" /></td>"
                                       + "<td>待收利息<b style=\"color: #C7C7C7;\">(元)</b>：<span>"+appProductRepayMessageBean.getInterest()+"元</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td><img src=\"../img/hkjh_14.png\" style=\"width:25px;height:25px;\" /></td>"
                                       + "<td>待收总额<b style=\"color: #C7C7C7;\">(元)</b>：<span>"+appProductRepayMessageBean.getRepayPrice()+"元</span></td>"
                                       + "</tr>"
                                       + "</table>"
                                       + "</ul>";

                    }else if (repayFlag.equals("1")){ //已还

                        repayDetailStr = "<ul>"
                                       + "<li>"
                                       + "<span><img src=\"../img/hkjh_03.png\" style=\"width:30px;height:30px;\"/></span>"
                                       + "<span class=\"schem_title\">&nbsp;&nbsp;"+appProductRepayMessageBean.getTitle()+"</span>"
                                       + "</li>"
                                       + "</ul>"
                                       + "<ul>"
                                       + "<table class=\"schems_table\">"
                                       + "<tr>"
                                       + "<td><img src=\"../img/hkjh_06.png\" style=\"width:25px;height:25px;\" /></td>"
                                       + "<td>编号：<span>"+appProductRepayMessageBean.getCode()+"</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td><img src=\"../img/hkjh_08.png\" style=\"width:25px;height:25px;\" /></td>"
                                       + "<td>实还日期：<span>"+appProductRepayMessageBean.getRepaymentTime()+"</span></td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td><img src=\"../img/hkjh_14.png\" style=\"width:25px;height:25px;\" /></td>"
                                       + "<td>实收总额<b style=\"color: #C7C7C7;\">(元)</b>：<span>"+appProductRepayMessageBean.getRepaymentPrice()+"元</span></td>"
                                       + "</tr>"
                                       + "</table>"
                                       + "</ul>";
                    }
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("repayDetailStr",repayDetailStr); //还款详细信息
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
