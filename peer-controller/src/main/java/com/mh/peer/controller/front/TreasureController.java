package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.NewsContentMessageBean;
import com.mh.peer.model.message.NewsTypeMessageBean;
import com.mh.peer.model.message.WeathInformationMessageBean;
import com.mh.peer.service.news.NewsService;
import com.mh.peer.service.news.NewsTypeService;
import com.mh.peer.util.FrontPage;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpSession;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Cuibin on 2016/5/13.
 */

@Controller
@RequestMapping("/treasure")
public class TreasureController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TreasureController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private NewsService newsService;
    @Autowired
    private NewsTypeService newsTypeService;


    /**
     * 搜索财富资讯(重写by Zhangerxin)
     * @param weathInformationMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryWeathByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryWeathByPage(@ModelAttribute WeathInformationMessageBean weathInformationMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String orderType = weathInformationMes.getOrderType(); //排序类型
        String orderColumn = weathInformationMes.getOrderColumn(); //排序字段
        String pageIndex = weathInformationMes.getPageIndex(); //起始数
        String pageSize = weathInformationMes.getPageSize(); //每页显示数量
        String firstTypeId = weathInformationMes.getFirstTypeId(); //一级类别id
        String newsFirstTypeId = ""; //一级类别id(列表部分)
        String newsTypeStr = ""; //新闻类别Str
        String pageStr = ""; //分页部分
        NewsTypeMessageBean newsFirstTypeMes = new NewsTypeMessageBean(); //新闻类别

        if (orderColumn == null || orderColumn.equals("")){
            orderColumn = "show_date";
            orderType = "DESC";
            weathInformationMes.setOrderColumn(orderColumn);
            weathInformationMes.setOrderType(orderType);
        }
        if (pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
            weathInformationMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            weathInformationMes.setPageSize(pageSize);
        }
        if (firstTypeId == null || firstTypeId.equals("")){ //一级类别id
            firstTypeId = "0";
        }

        //财富资讯信息集合
        List<WeathInformationMessageBean> listWeathInformationMes = newsService.queryWeathByPage(weathInformationMes);
        //财富资讯信息数量
        int totalCount = newsService.queryWeathCount(weathInformationMes);
        FrontPage frontPage = new FrontPage();
        //分页部分拼接
        pageStr = frontPage.getPageInfo3(pageIndex, pageSize, totalCount, "pageWeathInformation", "goWeathInformationPage", firstTypeId,"");

        //一级类别集合
        List<NewsTypeMessageBean> listFirstNewsTypeMes = newsTypeService.getAllNewsFirstType(newsFirstTypeMes);

        if (listFirstNewsTypeMes != null && listFirstNewsTypeMes.size() > 0){
            for (NewsTypeMessageBean newsFirstTypeMessageBean : listFirstNewsTypeMes){
                newsFirstTypeId = newsFirstTypeMessageBean.getId(); //一级类别id
                NewsTypeMessageBean newsSecondTypeMes = new NewsTypeMessageBean();
                newsSecondTypeMes.setFid(newsFirstTypeId);
                newsTypeStr = newsTypeStr + "<li>"
                            + "<h4>"
                            + "<a href=\"JavaScript:void(0)\" onclick=\"queryWealthBySome('"+newsFirstTypeId+"','')\">"
                            + "<i class=\"ico-home-10\"></i>"
                            + ""+newsFirstTypeMessageBean.getName()+""
                            + "</a>"
                            + "<span class=\"ico-home-14\"></span>"
                            + "<span class=\"ico-home-23\" style=\"display:none;\"></span>"
                            + "</h4>"
                            + "<ul class=\"p_zh_li_none\">";

                //二级类别集合
                List<NewsTypeMessageBean> listSecondNewsTypeMes = newsTypeService.getAllNewsSecondType(newsSecondTypeMes);
                if (listSecondNewsTypeMes != null && listSecondNewsTypeMes.size() > 0){
                    for (NewsTypeMessageBean newsSecondTypeMessageBean : listSecondNewsTypeMes){
                        newsTypeStr = newsTypeStr + "<li>"
                                    + "<a href=\"JavaScript:void(0)\" onclick=\"queryWealthBySome('"+newsFirstTypeId+"','"+newsSecondTypeMessageBean.getId()+"')\">"
                                    + newsSecondTypeMessageBean.getName()
                                    + "</a>"
                                    + "</li>";
                    }
                }

                newsTypeStr = newsTypeStr + "</ul>";
            }
        }

        modelMap.put("listWeathInformationMes",listWeathInformationMes); //财富资讯集合
        modelMap.put("totalCount",totalCount); //财富资讯数量
        modelMap.put("newsTypeStr",newsTypeStr); //类型拼接
        modelMap.put("pageStr",pageStr); //分页部分拼接

        try {

            StringWriter stringWriter = null;
            //模板渲染
            stringWriter = jetbrickTool.getJetbrickTemp(weathInformationMes.getMeneUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            LOGGER.error("模板解析失败！");
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
            e.printStackTrace();
        }
        return bean;
    }



    /**
     * 按条件搜索财富资讯(重写by Zhangerxin)
     * @param weathInformationMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryWeathBySome", method = RequestMethod.POST)
    @ResponseBody
    public WeathInformationMessageBean queryWeathBySome(@ModelAttribute WeathInformationMessageBean weathInformationMes, ModelMap modelMap) {
        String orderType = weathInformationMes.getOrderType(); //排序类型
        String orderColumn = weathInformationMes.getOrderColumn(); //排序字段
        String pageIndex = weathInformationMes.getPageIndex(); //起始数
        String pageSize = weathInformationMes.getPageSize(); //每页显示数量
        String firstTypeId = weathInformationMes.getFirstTypeId(); //一级类别id
        String secondTypeId = weathInformationMes.getSecondTypeId(); //二级类别id
        String weathInformationStr = ""; //财富部分拼接
        String pageStr = ""; //分页部分

        if (orderColumn == null || orderColumn.equals("")){
            orderColumn = "show_date";
            orderType = "DESC";
            weathInformationMes.setOrderColumn(orderColumn);
            weathInformationMes.setOrderType(orderType);
        }
        if (pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
            weathInformationMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            weathInformationMes.setPageSize(pageSize);
        }

        //财富资讯信息集合
        List<WeathInformationMessageBean> listWeathInformationMes = newsService.queryWeathByPage(weathInformationMes);
        if (listWeathInformationMes != null && listWeathInformationMes.size() > 0){
            for (WeathInformationMessageBean weathInformationMessageBean : listWeathInformationMes){
                weathInformationStr = weathInformationStr + "<li>"
                                    + "<a href=\"JavaScript:void(0)\" onclick=\"queryWealthDetails('"+weathInformationMessageBean.getId()+"')\">"
                                    + weathInformationMessageBean.getTitle()
                                    + "</a>"
                                    + "<span class=\"date_rich\">"+weathInformationMessageBean.getShowDate()+"</span>"
                                    + "</li>";
            }
        }

        //财富资讯信息数量
        int totalCount = newsService.queryWeathCount(weathInformationMes);
        FrontPage frontPage = new FrontPage();
        //分页部分拼接
        pageStr = frontPage.getPageInfo3(pageIndex, pageSize, totalCount, "pageWeathInformation", "goWeathInformationPage", firstTypeId, secondTypeId);
        weathInformationMes.setWeathInformationStr(weathInformationStr); //财富资讯部分拼接
        weathInformationMes.setPageStr(pageStr);

        return weathInformationMes;
    }


    /**
     * 财富资讯详细信息
     * @param weathInformationMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryWealthDetails", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryWealthDetails(@ModelAttribute WeathInformationMessageBean weathInformationMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        WeathInformationMessageBean weathInformationMessageBean =  newsService.queryWeathDetails(weathInformationMes); //财富资讯详细信息
        modelMap.put("weathInformationMessageBean",weathInformationMessageBean);

        try {

            StringWriter stringWriter = null;
            //模板渲染
            stringWriter = jetbrickTool.getJetbrickTemp(weathInformationMes.getMeneUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            LOGGER.error("模板解析失败！");
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
            e.printStackTrace();
        }
        return bean;
    }
}
