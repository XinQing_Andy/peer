package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.MyAccountMessageBean;
import com.mh.peer.model.message.TradeDetailMessageBean;
import com.mh.peer.service.front.TradeDetailService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.FrontPage;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpSession;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

/**
 * Created by cuibin on 2016/6/20.
 */

@Controller
@RequestMapping("/tradeDetail")
public class TradeDetailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TradeDetailController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private TradeDetailService tradeDetailService;
    @Autowired
    private AllMemberService allMemberService;


    /**
     * 查询所有资金明细
     * @param tradeDetailMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryTradeDetail", method = RequestMethod.POST)
    @ResponseBody
    public TradeDetailMessageBean queryTradeDetail(@ModelAttribute TradeDetailMessageBean tradeDetailMes, ModelMap modelMap) {
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        String pageIndex = tradeDetailMes.getPageIndex(); //起始量
        String pageSize = tradeDetailMes.getPageSize(); //每页显示数量
        String type = ""; //交易类型
        String typeName = ""; //交易类型名称
        String str = ""; //列表部分拼接
        String pageStr = ""; //分页部分拼接

        if (pageIndex == null || pageIndex.equals("")) { //起始数
            pageIndex = "1";
            tradeDetailMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "4"; //每页显示数量
            tradeDetailMes.setPageSize(pageSize); //每页显示数量
        }

        if(memberInfo != null){
            memberId = memberInfo.getId();
            tradeDetailMes.setMemberid(memberId); //会员id
        }

        //交易记录集合
        List<TradeDetailMessageBean> listTradeDetailMes = tradeDetailService.queryTradeDetail(tradeDetailMes);
        //交易记录数
        int totalCount = tradeDetailService.queryTradeDetailCount(tradeDetailMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageTrade","goTradePage"); //提现记录部分分页
        if (listTradeDetailMes != null && listTradeDetailMes.size() > 0){
            for (TradeDetailMessageBean tradeDetailMessageBean : listTradeDetailMes){
                type = tradeDetailMessageBean.getType(); //交易类型
                if (type != null && !type.equals("")){
                    if (type.equals("0")){
                        typeName = "充值";
                    }else if(type.equals("1")){
                        typeName = "提现";
                    }else if (type.equals("2")){
                        typeName = "投资";
                    }else if (type.equals("3")){
                        typeName = "回款";
                    }else if (type.equals("4")){
                        typeName = "借款";
                    }else if (type.equals("5")){
                        typeName = "还款";
                    }else if (type.equals("7")){
                        typeName = "债权";
                    }
                }
                str = str + "<tr style=\"border:none\">"
                          + "<td>"+tradeDetailMessageBean.getTime()+"</td>"
                          + "<td>"+typeName+"</td>"
                          + "<td>"+tradeDetailMessageBean.getSerialnum()+"</td>"
                          + "<td>"+tradeDetailMessageBean.getSprice()+"元</td>"
                          + "<td>"+tradeDetailMessageBean.getBalance()+"元</td>"
                          + "</tr>";
            }
        }

        tradeDetailMes.setStr(str); //列表部分拼接
        tradeDetailMes.setPageStr(pageStr); //分页部分拼接

        return tradeDetailMes;
    }


    /**
     * 跳转至资金明细
     * @param tradeDetailMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openTradeDetail", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openTradeDetail(@ModelAttribute TradeDetailMessageBean tradeDetailMes,ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String memberId = ""; //会员id
        String pageIndex = tradeDetailMes.getPageIndex(); //起始数量
        String pageSize = tradeDetailMes.getPageSize(); //每页显示数量
        String pageStr = ""; //分页拼接
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if (memberInfo == null || memberInfo.equals("")) {
            bean.setInfo("会话已过期请重新登陆");
            bean.setResult("error");
            return bean;
        }

        memberId = memberInfo.getId(); //会员id
        if (pageIndex == null || pageIndex.equals("")) {  //起始数
            pageIndex = "1";
            tradeDetailMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "4";
            tradeDetailMes.setPageSize(pageSize);
        }
        if (memberId != null && !memberId.equals("")){
            tradeDetailMes.setMemberid(memberId);
        }

        //交易记录集合
        List<TradeDetailMessageBean> listTradeDetailMes = tradeDetailService.queryTradeDetail(tradeDetailMes);
        //查询总记录数
        int totalCount = tradeDetailService.queryTradeDetailCount(tradeDetailMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageTrade","goTradePage"); //提现记录部分分页

        modelMap.put("totalCount", totalCount); //交易记录数
        modelMap.put("listTradeDetailMes", listTradeDetailMes); //交易记录集合
        modelMap.put("pageStr",pageStr);

        try {

            StringWriter stringWriter = null;
            //模板渲染
            stringWriter = jetbrickTool.getJetbrickTemp(tradeDetailMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }

}
