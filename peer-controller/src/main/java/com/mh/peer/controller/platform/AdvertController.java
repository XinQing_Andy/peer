package com.mh.peer.controller.platform;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.AdvertContentBusinessBean;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.business.ProductBusinessBean;
import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.entity.AdvertType;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.AdvertContentMessageBean;
import com.mh.peer.service.platform.AdvertService;
import com.mh.peer.service.platform.AdvertTypeService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-4-11.
 */
@Controller
@RequestMapping("/advert")
public class AdvertController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private AdvertService advertService;
    @Autowired
    private AdvertTypeService advertTypeService;
    @Autowired
    private SysFileService sysFileService;

    /**
     * 根据分页获取广告信息
     * @param advertContentMese
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAdvertByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAdvertByPage(@ModelAttribute AdvertContentMessageBean advertContentMese, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = advertContentMese.getPageIndex(); //当前数量
        String pageSize = advertContentMese.getPageSize(); //每页显示数量
        String currentPage = advertContentMese.getCurrentPage(); //当前页
        int pageCount = 0; //总页数
        String title = advertContentMese.getTitle(); //题目
        String typeId = advertContentMese.getTypeId(); //类型id

        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
        }

        if(title != null && !title.equals("")){ //题目
            modelMap.put("title",title);
        }
        if(typeId != null && !typeId.equals("")){ //类型id
            modelMap.put("typeId",typeId);
        }

        advertContentMese.setPageIndex(pageIndex); //当前页
        advertContentMese.setPageSize(pageSize); //每页显示数量
        List<AdvertContentMessageBean> advertList = advertService.queryAdvertByPage(advertContentMese); //广告集合(根据分页查询)
        int totalCount = advertService.getCountAdvertByPage(advertContentMese); //广告数量
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量

        modelMap.put("advertList", advertList); //广告集合
        modelMap.put("totalCount", totalCount); //广告数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageCount",pageCount); //总数量

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(advertContentMese.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 打开新增广告页面
     * @param advertContentMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openAdvertContent", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openProduct(@ModelAttribute AdvertContentMessageBean advertContentMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        List<AdvertType> listAdvertType = new ArrayList<AdvertType>(); //广告类别集合
        listAdvertType = advertTypeService.getAllAdvertType(); //广告类别集合
        modelMap.addAttribute("listAdvertType",listAdvertType);

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(advertContentMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 上传附件(广告新增部分)
     * @param advertFile
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value="/UploadAdvert", method = RequestMethod.POST)
    public void UploadAdvert(@RequestParam MultipartFile[] advertFile,HttpServletRequest request,HttpServletResponse response) throws IOException{

        String ctxPath = request.getRealPath("/")+"image/advert"; //绝对路径
        String ctxPath2 = "/image/advert"; //相对路径
        String originalFilename = ""; //原文件名称
        String Uid = ""; //广告表主键
        String UidFj = ""; //附件表主键
        String uuid = ""; //新附件名称(不带后缀)
        String newFileName = ""; //新文件名称
        String suffix = ""; //后缀
        response.setContentType("text/plain; charset=UTF-8");
        PrintWriter out = response.getWriter();
        AdvertContent advertContent = new AdvertContent(); //广告
        SysFile sysFile = new SysFile(); //附件部分

        /********** 用户信息Start **********/
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        String userName = bean.getSysUser().getUserName();
        /*********** 用户信息End ***********/

        //接收前台传过来的值
        String typeid = request.getParameter("typeid"); //广告类别id
        String title = new String(request.getParameter("title").getBytes("iso8859-1"),"utf-8"); //题目
        String autor = new String(request.getParameter("autor").getBytes("iso8859-1"),"utf-8"); //作者
        String content = new String(request.getParameter("content").getBytes("iso8859-1"),"utf-8"); //内容
        String sort = request.getParameter("sort"); //排序
        String fjType = ""; //附件类别

        Uid = UUID.randomUUID().toString().replaceAll("\\-", ""); //广告表主键
        advertContent.setId(Uid); //主键
        advertContent.setTypeid(typeid); //类型id
        advertContent.setTitle(title); //题目
        advertContent.setAutor(autor); //作者
        advertContent.setContent(content); //内容
        advertContent.setFlag("0"); //状态(0-启用、1-暂停)
        advertContent.setSort(StringTool.getInt(sort)); //排序
        advertContent.setCreateuser(userName); //创建人
        advertContent.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

        AdvertContentBusinessBean advertBus = advertService.saveAdvert(advertContent); //保存广告
        if(advertBus.getResult().equals("success")) {
            ctxPath = ctxPath.replaceAll("\\\\","/"); //绝对路径
            ctxPath2 = ctxPath2.replaceAll("\\\\","/"); //相对路径

            if(typeid != null && !typeid.equals("")){
                if(typeid.equals("1")){ //广告内容
                    fjType = "2";
                }else if(typeid.equals("2")){ //广告首页轮播图
                    fjType = "3";
                }else{ //本金保障
                    fjType = typeid;
                }
            }

            for(MultipartFile myfile : advertFile){
                if(myfile.isEmpty()){ //附件为空
                    out.print("success");
                    out.flush();
                }else{
                    originalFilename = myfile.getOriginalFilename(); //原文件名称
                    suffix = originalFilename.indexOf(".") != -1 ? originalFilename.substring(originalFilename.lastIndexOf("."), originalFilename.length()) : null;
                    suffix = suffix.toUpperCase(); //转换为大写
                    if(suffix != null && !suffix.equals("")){
                        if(suffix.equals(".JPG") || suffix.equals(".PNG") || suffix.equals(".GIF") || suffix.equals(".JPEG")){
                            uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
                            newFileName = uuid + (suffix != null ? suffix : ""); //构成新文件名。
                            try {
                                FileUtils.copyInputStreamToFile(myfile.getInputStream(), new File(ctxPath, newFileName));
                                UidFj = UUID.randomUUID().toString().replaceAll("\\-", "");
                                sysFile.setId(UidFj); //主键
                                sysFile.setGlid(Uid); //关联id
                                sysFile.setImgurljd(ctxPath+"/"+newFileName); //附件地址(绝对地址)
                                sysFile.setImgurl(ctxPath2+"/"+newFileName); //附件地址
                                sysFile.setType(fjType); //类型(产品)
                                sysFile.setCreateuser(userName); //创建用户
                                sysFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                sysFileService.saveSysFile(sysFile); //保存附件
                                out.print("success");
                                out.flush();
                            }catch (Exception e){
                                out.print("error");
                                out.flush();
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

//    /**
//     * 上传附件(广告新增部分)
//     * @param advertFile
//     * @param request
//     * @param response
//     * @return
//     * @throws IOException
//     */
    @RequestMapping(value="/UploadContentAdvert", method = RequestMethod.POST)
    public void UploadContentAdvert(@RequestParam MultipartFile[] yjeditorUploadFile,HttpServletRequest request,HttpServletResponse response) throws IOException{
        String ctxPath = request.getRealPath("/")+"imageyjeditor/advert"; //绝对路径
        String ctxPath2 = "/imageyjeditor/advert"; //相对路径
        String originalFilename = ""; //原文件名称
        String Uid = ""; //广告表主键
        String UidFj = ""; //附件表主键
        String uuid = ""; //新附件名称(不带后缀)
        String newFileName = ""; //新文件名称
        String suffix = ""; //后缀
        response.setContentType("text/plain; charset=UTF-8");
        PrintWriter out = response.getWriter();
        SysFile sysFile = new SysFile(); //附件部分

        /********** 用户信息Start **********/
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        String userName = bean.getSysUser().getUserName();
        /*********** 用户信息End ***********/

        //接收前台传过来的值
        String fjType = ""; //附件类别

        Uid = UUID.randomUUID().toString().replaceAll("\\-", ""); //广告表主键
            ctxPath = ctxPath.replaceAll("\\\\","/"); //绝对路径
            ctxPath2 = ctxPath2.replaceAll("\\\\","/"); //相对路径
            for(MultipartFile myfile : yjeditorUploadFile){
                if(myfile.isEmpty()){ //附件为空
                    out.print("success");
                    out.flush();
                }else{
                    originalFilename = myfile.getOriginalFilename(); //原文件名称
                    suffix = originalFilename.indexOf(".") != -1 ? originalFilename.substring(originalFilename.lastIndexOf("."), originalFilename.length()) : null;
                    suffix = suffix.toUpperCase(); //转换为大写
                    if(suffix != null && !suffix.equals("")){
                        if(suffix.equals(".JPG") || suffix.equals(".PNG") || suffix.equals(".GIF") || suffix.equals(".JPEG")){
                            uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
                            newFileName = uuid + (suffix != null ? suffix : ""); //构成新文件名。
                            try {
                                FileUtils.copyInputStreamToFile(myfile.getInputStream(), new File(ctxPath, newFileName));

                                UidFj = UUID.randomUUID().toString().replaceAll("\\-", "");
                                sysFile.setId(UidFj); //主键
                                sysFile.setGlid(Uid); //关联id
                                sysFile.setImgurljd(ctxPath+"/"+newFileName); //附件地址(绝对地址)
                                sysFile.setImgurl(ctxPath2+"/"+newFileName); //附件地址
                                sysFile.setType(fjType); //类型(产品)
                                sysFile.setCreateuser(userName); //创建用户
                                sysFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                sysFileService.saveSysFile(sysFile); //保存附件
                                out.print("/imageyjeditor/advert/"+newFileName);
                                out.flush();
                            }catch (Exception e){
                                out.print("error");
                                out.flush();
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
    }



    /**
     * 根据id获取广告信息
     * @param advertContentMessageBean
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAdvertById", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAdvertById(@ModelAttribute AdvertContentMessageBean advertContentMessageBean, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        AdvertContentMessageBean advertContentMes = advertService.queryProductById(advertContentMessageBean); //广告集合(根据id查询)
        modelMap.put("advertContentMes", advertContentMes); //广告集合

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(advertContentMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 修改广告
     * @param advertFile
     * @param request
     * @param response
     */
    @RequestMapping(value="/UploadAdvertEdit", method = RequestMethod.POST)
    public void UploadAdvertEdit(@RequestParam MultipartFile[] advertFile,HttpServletRequest request,HttpServletResponse response) throws IOException {
        String ctxPath = request.getRealPath("/")+"image/advert"; //绝对路径
        String ctxPath2 = "/image/advert"; //相对路径
        String originalFilename = ""; //原文件名称
        String UidFj = ""; //附件表主键
        String uuid = ""; //新附件名称(不带后缀)
        String newFileName = ""; //新文件名称
        String suffix = ""; //后缀
        response.setContentType("text/plain; charset=UTF-8");
        PrintWriter out = response.getWriter();
        AdvertContent advertContent = new AdvertContent(); //广告
        SysFile sysFile = new SysFile(); //附件部分
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        String userName = bean.getSysUser().getUserName();

        //接收前台传过来的值
        String id = request.getParameter("id"); //主键
        String fileId = request.getParameter("fileId"); //附件id
        String imgUrl = request.getParameter("imgUrl"); //附件地址(绝对)
        String typeid = request.getParameter("typeId"); //广告类别id
        String title = new String(request.getParameter("title").getBytes("iso8859-1"),"utf-8"); //题目
        String autor = new String(request.getParameter("autor").getBytes("iso8859-1"),"utf-8"); //作者
        String content = new String(request.getParameter("content").getBytes("iso8859-1"),"utf-8"); //内容
        String sort = request.getParameter("sort"); //排序
        String fjType = ""; //附件类型

        //给实体类赋值
        advertContent.setId(id); //主键
        advertContent.setTypeid(typeid); //广告类别id
        advertContent.setTitle(title); //题目
        advertContent.setContent(content); //内容
        advertContent.setAutor(autor); //作者
        advertContent.setSort(StringTool.getInt(sort)); //排序

        //修改广告
        AdvertContentBusinessBean advertBus = advertService.updateAdvert(advertContent);

        if(typeid != null && !typeid.equals("")){
            if(typeid.equals("1")){ //广告内容
                fjType = "2";
            }else if(typeid.equals("2")){ //广告首页轮播图
                fjType = "3";
            }else{ //本金保障
                fjType = typeid;
            }
        }

        if(advertBus.getResult().equals("success")) {
            ctxPath = ctxPath.replaceAll("\\\\", "/"); //绝对路径
            ctxPath2 = ctxPath2.replaceAll("\\\\", "/"); //相对路径

            for (MultipartFile myfile : advertFile) {
                if (myfile.isEmpty()) {
                    out.print("success");
                    out.flush();
                } else {
                    originalFilename = myfile.getOriginalFilename(); //原文件名称
                    suffix = originalFilename.indexOf(".") != -1 ? originalFilename.substring(originalFilename.lastIndexOf("."), originalFilename.length()) : null;
                    suffix = suffix.toUpperCase(); //转换为大写
                    if (suffix != null && !suffix.equals("")) {
                        if (suffix.equals(".JPG") || suffix.equals(".PNG") || suffix.equals(".GIF") || suffix.equals(".JPEG")) {
                            uuid = UUID.randomUUID().toString().replaceAll("\\-", ""); //附件名称
                            newFileName = uuid + (suffix != null ? suffix : ""); //构成新文件名。

                            try {

                                /********** 删除附件Start **********/
                                if(fileId != null && !fileId.equals("")){
                                    sysFile.setId(fileId); //附件id
                                    sysFileService.deleteSysFile(sysFile);
                                    File file = new File(imgUrl);
                                    file.delete();
                                }
                                /*********** 删除附件End ***********/

                                FileUtils.copyInputStreamToFile(myfile.getInputStream(), new File(ctxPath, newFileName));

                                UidFj = UUID.randomUUID().toString().replaceAll("\\-", "");
                                sysFile.setId(UidFj); //主键
                                sysFile.setGlid(id); //关联id
                                sysFile.setImgurljd(ctxPath + "/" + newFileName); //附件地址(绝对地址)
                                sysFile.setImgurl(ctxPath2 + "/" + newFileName); //附件地址
                                sysFile.setType(fjType); //类型(产品)
                                sysFile.setCreateuser(userName); //创建用户
                                sysFile.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                sysFileService.saveSysFile(sysFile); //保存附件
                                out.print("success");
                                out.flush();
                            } catch (Exception e) {
                                out.print("error");
                                out.flush();
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * 设置产品状态
     * @param advertContentMess
     * @return
     */
    @RequestMapping(value = "/setAdvertFlag")
    @ResponseBody
    public AdvertContentMessageBean setProductFlag(@ModelAttribute AdvertContentMessageBean advertContentMess){

        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        String userName = bean.getSysUser().getUserName();
        AdvertContent advertContent = new AdvertContent(); //广告
        advertContent.setId(advertContentMess.getId()); //主键
        advertContent.setFlag(advertContentMess.getFlag()); //状态

        //修改安全问题
        AdvertContentBusinessBean advertContentBus = advertService.updateAdvert(advertContent);

        //设置返回安全参数消息
        advertContentMess.setResult(advertContentBus.getResult()); //状态(成功或失败)
        advertContentMess.setInfo(advertContentBus.getInfo()); //错误信息
        return advertContentMess;
    }

    /**
     * 获取总页数
     * @param pageSize
     * @param totalCount
     * @return
     */
    public int getPageCount(String pageSize, int totalCount) {
        int totalPage = 0; //总页数
        int pageSizein = 0; //每页显示数量(整型)

        if (pageSize != null && !pageSize.equals("")){
            pageSizein = TypeTool.getInt(pageSize);
        }

        if (totalCount % pageSizein == 0) {
            totalPage = totalCount / pageSizein;
        } else {
            totalPage = totalCount / pageSizein + 1;
        }

        return totalPage;
    }
}
