package com.mh.peer.controller.huanxun;

import com.mh.peer.model.entity.HuanxunAutosign;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.service.huanxun.HuanXunMotionSignService;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Cuibin on 2016/8/4.  环迅自动签约
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunMotionSignController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private HuanXunMotionSignService huanXunMotionSignService;

    /**
     * 自动签约后台返回地址
     *
     * @param request
     */
    @RequestMapping(value = "/motionSignResult", method = RequestMethod.POST)
    @ResponseBody
    public void motionSignResult(HttpServletRequest request) {
        System.out.println("======================自动签约后台返回地址===========================");
        String response = request.getParameter("response");
        String resultMsg = request.getParameter("resultMsg");
        String merchantID = request.getParameter("merchantID");
        String sign = request.getParameter("sign");
        String resultCode = request.getParameter("resultCode");
        String memberId = request.getParameter("memberId");
        String uuid = UUID.randomUUID().toString().replaceAll("\\-", ""); //主键
        System.out.println("memberId==========="+memberId);
        System.out.println("resultCode==========="+resultCode);

        /************ response ************/
        String merBillNo = ""; //商户订单号
        String merDate = ""; //日期
        String userType = ""; //用户类型
        String ipsAcctNo = ""; //IPS存管账户号
        String signedType = ""; //签约类型(1-自动投标,2-自动还款)
        String validity = ""; //有效期
        String cycMinVal = ""; //标的周期最大值
        String cycMaxVal = ""; //标的周期最小值
        String bidMin = "0.0"; //投标限额最小值
        String bidMax = "0.0"; //投标限额最大值
        String rateMin = "0.0"; //利率最小值
        String rateMax = "0.0"; //利率最大值
        String ipsBillNo = ""; //IPS订单号
        String ipsAuthNo = ""; //IPS授权号
        String ipsDoTime = "";//IPS处理时间
        String status = "";//签约结果
        /************ response ************/

        String responseJm = ""; //响应信息(解密)
        Map<String, Object> map = new HashMap<String, Object>();

        try{

            responseJm = Pdes.decrypt3DES(response); //响应信息(解密)

            if (responseJm != null && !responseJm.equals("")) {
                System.out.println("responseJm==========="+responseJm);
                map = JsonHelper.getObjectToMap2(responseJm);
                if (map != null) {
                    merBillNo = (String) map.get("merBillNo");
                    merDate = (String) map.get("merDate");
                    userType = (String) map.get("userType");
                    ipsAcctNo = (String) map.get("ipsAcctNo");
                    signedType = (String) map.get("signedType");
                    validity = (String) map.get("validity");
                    ipsBillNo = (String) map.get("ipsBillNo");
                    ipsAuthNo = (String) map.get("ipsAuthNo");
                    ipsDoTime = (String) map.get("ipsDoTime");
                    status = (String) map.get("status");
                }
            }

            System.out.println("flag====="+huanXunMotionSignService.queryIsResult(memberId,signedType));
            if (huanXunMotionSignService.queryIsResult(memberId,signedType)) { //判断是否可以签约
                HuanxunAutosign huanxunAutosign = new HuanxunAutosign();
                huanxunAutosign.setId(uuid); //主键
                huanxunAutosign.setResultcode(resultCode);
                huanxunAutosign.setResultmsg(resultMsg);
                huanxunAutosign.setSign(sign); //签名
                huanxunAutosign.setMerchantid(merchantID); //商户存管交易账号
                huanxunAutosign.setMerbillno(merBillNo);
                huanxunAutosign.setUsertype(userType);
                huanxunAutosign.setIpsacctno(ipsAcctNo);
                huanxunAutosign.setSignedtype(signedType);
                huanxunAutosign.setValidity(validity);
                huanxunAutosign.setCycmaxval(cycMaxVal);
                huanxunAutosign.setCycminval(cycMinVal);
                huanxunAutosign.setBidmin(Double.parseDouble(bidMin));
                huanxunAutosign.setBidmax(Double.parseDouble(bidMax));
                huanxunAutosign.setRatemin(Double.parseDouble(rateMin));
                huanxunAutosign.setRatemax(Double.parseDouble(rateMax));
                huanxunAutosign.setIpsbillno(ipsBillNo);
                huanxunAutosign.setIpsauthno(ipsAuthNo);
                huanxunAutosign.setIpsdotime(ipsDoTime);
                huanxunAutosign.setStatus(status);
                huanxunAutosign.setMemberid(memberId);
                huanxunAutosign.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd")); //创建时间

                boolean b = huanXunMotionSignService.saveMotionSignResult(huanxunAutosign);
                System.out.println("saveMotionSignResult===================" + b);
            }else{
                System.out.println("==================数据已存在");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }



    /**
     * 自动签约
     *
     * @param response
     */
    @RequestMapping(value = "/motionSign", method = RequestMethod.POST)
    @ResponseBody
    public void motionSign(HttpServletRequest req, HttpServletResponse response) {
        String memberId = req.getParameter("memberId"); //会员id
        HuanXunRegistMessageBean huanXunRegistMes = huanXunMotionSignService.queryHuanXunReg(memberId);
        String merBillNo = "181006" + UUID.randomUUID().toString().replaceAll("-", "").toLowerCase(); //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //登记日期
        String ipsAcctNo = huanXunRegistMes.getIpsAcctNo(); //IPS存管账户号
        String validity = "50000"; //有效期(单位天)
        String webUrl = "http://jianbing88.com/front/motionSignhxWebResult?memberId="+memberId; //前台返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/motionSignResult?memberId="+memberId ; //后台通知地址

        String operationType = "user.autoSign"; //操作类型
        String merchantID = "1810060028";
        String request = "{\"merBillNo\":\"" + merBillNo + "\",\"merDate\":\"" + merDate + "\",\"userType\":\"1\",\"ipsAcctNo\":\"" + ipsAcctNo + "\","
                       + "\"signedType\":\"1\",\"validity\":\"" + validity + "\",\"cycMinVal\":\"\",\"cycMaxVal\":\"\",\"bidMin\":\"\",\"bidMax\":\"\","
                       + "\"rateMin\":\"\",\"rateMax\":\"\",\"webUrl\":\"" + webUrl + "\",\"s2SUrl\":\"" + s2SUrl + "\"}";

        request = Pdes.encrypt3DES(request);
        HuanXunInfo huanXunInfo = new HuanXunInfo();
        String MD5ZS = huanXunInfo.getMd5Zs();
        String sign = operationType + merchantID + request + MD5ZS;
        sign = MD5Util.MD5(sign).toLowerCase();

        HashMap<String, Object> in = null;
        in = new HashMap<String, Object>();
        in.put("operationType", operationType);
        in.put("merchantID", merchantID);
        in.put("request", request);
        in.put("sign", sign);
        output(response, JSONObject.fromObject(in).toString());
    }

    /**
     * 输出结果到response中
     *
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8"));
            response.getOutputStream().flush();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
