package com.mh.peer.controller.huanxun;

import com.mh.peer.exception.HuanXunReChargeException;
import com.mh.peer.model.entity.HuanxunRecharge;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.HuanXunRechargeMessageBean;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.model.message.MemberBanknoMessageBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.service.bank.BankService;
import com.mh.peer.service.huanxun.HuanXunRechargeService;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-7-24.
 * 充值
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunRechargeController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private HuanXunRegistService huanXunRegistService;
    @Autowired
    private BankService bankService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private HuanXunRechargeService huanXunRechargeService;

    /**
     * 环迅部分充值
     * @param huanXunRechargeMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/rechargehx", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunRechargeMessageBean rechargehx(@ModelAttribute HuanXunRechargeMessageBean huanXunRechargeMes, ModelMap modelMap) {
        HuanXunRechargeMessageBean huanXunRechargeMessageBean = new HuanXunRechargeMessageBean();
        String operationType = huanXunRechargeMes.getOperationType(); //操作类型
        String merchantID = huanXunRechargeMes.getMerchantID(); //商户存管交易账号
        String md5Zs = ""; //md5证书
        String merBillNo = "181006"; //商户订单号(前6位以商户号起始)
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //充值日期
        String depositType = huanXunRechargeMes.getDepositType(); //充值类型(1-普通充值、2-还款充值)
        String channelType = "1"; //充值渠道(1-个人网银、2-企业网银)
        String bankCode = ""; //充值银行号
        String userType = "1"; //用户类型(1-个人、2-企业)
        String ipsAcctNo = ""; //IPS存管账号
        String trdAmt = huanXunRechargeMes.getTrdAmt(); //充值金额
        String ipsFeeType = "1"; //IPS手续费承担方(1-平台商户、2-平台用户)
        String merFee = "0"; //平台收取用户的手续费
        String merFeeType = "1"; //平台手续费收取方式(1-内扣、2-外扣)
        String taker = "2"; //发起方(1-商户发起、2-用户发起)
        String webUrl = "http://jianbing88.com/front/registerhxWebResult"; //页面返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/rechargehxResult"; //后台通知地址
        String sign = ""; //签名
        String signJm = ""; //签名加密
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        String glId = ""; //关联id
        String result = "0"; //备份结果(0-成功、1-失败)
        Map<String,String> mapRechargeBf = new HashMap<String,String>();

        try{

            if (depositType != null && !depositType.equals("")){ //充值类型
                if (depositType.equals("1")){ //普通充值
                    ipsFeeType = "1"; //IPS手续费承担方(平台商户)
                }else if (depositType.equals("2")){ //还款充值
                    ipsFeeType = "2"; //IPS手续费承担方(平台用户)
                }
            }

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs(); //md5证书

            glId = UUID.randomUUID().toString().replaceAll("-",""); //关联id

            if (memberInfo == null){
                huanXunRechargeMessageBean.setCode("-1"); //执行状态
                huanXunRechargeMessageBean.setInfo("请重新登录！");
                return huanXunRechargeMessageBean;
            }
            memberId = memberInfo.getId(); //会员id
            if (memberId != null && !memberId.equals("")){
                webUrl = webUrl + "?memberId="+memberId; //页面返回地址
                s2SUrl = s2SUrl + "?memberId="+memberId+"&glId="+glId+"&source=0"; //后台通知地址

                HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean();
                huanXunRegistMessageBean.setMemberId(memberId); //会员id
                //查询会员注册信息
                List<HuanXunRegistMessageBean> listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
                if (listHuanXunRegistMes == null || listHuanXunRegistMes.size() == 0){
                    huanXunRechargeMessageBean.setCode("-1"); //执行状态
                    huanXunRechargeMessageBean.setInfo("您还未注册环迅，请前去注册！");
                    return huanXunRechargeMessageBean;
                }
                ipsAcctNo = listHuanXunRegistMes.get(0).getIpsAcctNo(); //IPS存管账号

                MemberBanknoMessageBean memberBanknoMessageBean = new MemberBanknoMessageBean();
                memberBanknoMessageBean.setMemberid(memberId); //会员id
                List<MemberBanknoMessageBean> listMemberBanknoMes = bankService.getMemberBankInfoBySome(memberBanknoMessageBean);
                if (listMemberBanknoMes == null || listMemberBanknoMes.size() == 0){
                    huanXunRechargeMessageBean.setCode("-1"); //执行状态
                    huanXunRechargeMessageBean.setInfo("您还未设置银行卡，请前去设置银行卡！");
                    return huanXunRechargeMessageBean;
                }
                bankCode = listMemberBanknoMes.get(0).getBankNo(); //银行卡号

                merBillNo = merBillNo + UUID.randomUUID().toString().replaceAll("-","");

                request = "{\"merBillNo\":\""+merBillNo+"\",\"merDate\":\""+merDate+"\",\"depositType\":\""+depositType+"\","
                        + "\"channelType\":\""+channelType+"\",\"bankCode\":\""+bankCode+"\",\"userType\":\""+userType+"\","
                        + "\"ipsAcctNo\":\""+ipsAcctNo+"\",\"trdAmt\":\""+trdAmt+"\",\"ipsFeeType\":\""+ipsFeeType+"\","
                        + "\"merFee\":\""+merFee+"\",\"merFeeType\":\""+merFeeType+"\",\"taker\":\""+taker+"\","
                        + "\"webUrl\":\""+webUrl+"\",\"s2SUrl\":\""+s2SUrl+"\"}";

                requestJm = Pdes.encrypt3DES(request); //3des加密
                sign = operationType + merchantID + requestJm + md5Zs; //签名
                signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

                huanXunRechargeMes.setSign(signJm); //签名
                huanXunRechargeMes.setRequest(requestJm); //请求信息

                /*************** 充值备份Start ***************/
                mapRechargeBf.put("memberId",memberId); //会员id
                mapRechargeBf.put("merBillNo",merBillNo); //商户订单号
                mapRechargeBf.put("merDate",merDate); //充值日期
                mapRechargeBf.put("depositType",depositType); //充值类型(1-普通充值、2-还款充值)
                mapRechargeBf.put("channelType",channelType); //充值渠道(1-个人网银、2-企业网银)
                mapRechargeBf.put("bankCode",bankCode); //充值银行号
                mapRechargeBf.put("userType",userType); //用户类型
                mapRechargeBf.put("ipsAcctNo",ipsAcctNo); //IPS存管账户号
                mapRechargeBf.put("trdAmt",trdAmt); //充值金额
                mapRechargeBf.put("ipsFeeType",ipsFeeType); //IPS手续费承担方(1-平台商户、2-平台用户)
                mapRechargeBf.put("merFee",merFee); //平台手续费(String型)
                mapRechargeBf.put("merFeeType",merFeeType); //平台手续费收取方式(1-内扣、2-外扣)
                mapRechargeBf.put("taker",taker); //发起方(1-商户发起、2-用户发起)
                result = ReChargeBf.reChargeHandle(mapRechargeBf); //充值备份结果(0-成功、1-失败)
                /**************** 充值备份End ****************/

                huanXunRechargeMes.setCode(result); //充值结果(0-成功、1-失败)
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return huanXunRechargeMes;
    }


    /**
     * 环迅充值结果
     * @param request
     * @param responsehx
     * @param modelMap
     */
    @RequestMapping(value = "/rechargehxResult", method = RequestMethod.POST)
    @ResponseBody
    public void rechargehxResult(HttpServletRequest request, HttpServletResponse responsehx, ModelMap modelMap) {
        String memberId = request.getParameter("memberId"); //会员id
        String glId = request.getParameter("glId"); //关联id
        String source = request.getParameter("source"); //来源(0-电脑端、1-手机端)
        String resultCode = request.getParameter("resultCode"); //返回编号(000000-成功、999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //返回信息
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String response = request.getParameter("response"); //响应信息
        String responseJm = ""; //响应信息(解密)
        String merBillNo = ""; //商户订单号
        String depositType = ""; //充值类型(1-普通充值、2-还款充值)
        String channelType = ""; //渠道种类(1-个人网银、2-企业网银)
        String bankCode = ""; //充值银行
        String ipsBillNo = ""; //充值的IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String ipsTrdAmt = ""; //用户IPS实际到账金额(String型)
        Double ipsTrdAmtDou = 0.0d; //用户IPS实际到账金额(Double型)
        String ipsFee = ""; //IPS手续费金额(String型)
        Double ipsFeeDou = 0.0d; //IPS手续费金额(Double型)
        String merFee = ""; //平台手续费(String型)
        Double merFeeDou = 0.0d; //平台手续费(Double型)
        String trdStatus = ""; //充值状态(0-失败、1-成功、2-失败)
        Map<String,Object> map = new HashMap<String,Object>();
        String Uidrechargehx = ""; //主键(环迅充值)
        String UidMyMessage = ""; //主键(我的消息)
        String title = ""; //交易题目
        String content = ""; //交易内容
        String UidTradeDetail = ""; //主键(交易记录)
        String serialnum = ""; //流水号
        String balance = ""; //余额(String型)
        Double balanceDou = 0.0d; //余额(Double型)
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息

        try{

            output(responsehx,"ipsCheckOk"); //接收成功

            MemberInfoMessageBean memberInfoMessageBean = allMemberService.queryMemberInfoById(memberId);
            if (memberInfoMessageBean != null){
                balance = memberInfoMessageBean.getBalance(); //余额(String型)
                if (balance != null && !balance.equals("")){
                    balanceDou = TypeTool.getDouble(balance); //余额(Double型)
                }
            }

            if (response != null && !response.equals("")){
                responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                if (responseJm != null && !responseJm.equals("")){
                    map = JsonHelper.getObjectToMap2(responseJm);
                    if (map != null){
                        if (map.get("merBillNo") != null && !map.get("merBillNo").equals("")){
                            merBillNo = (String) map.get("merBillNo"); //商户订单号
                        }
                        if (map.get("depositType") != null){
                            depositType = (String) map.get("depositType"); //充值类型(1-普通充值、2-还款充值)
                        }
                        if (map.get("channelType") != null && !map.get("channelType").equals("") && !map.get("channelType").equals("null")){
                            channelType = (String) map.get("channelType"); //渠道种类(1-个人网银、2-企业网银)
                        }
                        if (map.get("bankCode") != null && !map.get("bankCode").equals("") && !map.get("bankCode").equals("null")){
                            bankCode = (String) map.get("bankCode"); //充值银行
                        }
                        if (map.get("ipsBillNo") != null && !map.get("ipsBillNo").equals("")){
                            ipsBillNo = (String) map.get("ipsBillNo"); //充值的IPS订单号
                        }
                        if (map.get("ipsDoTime") != null && !map.get("ipsDoTime").equals("")){
                            ipsDoTime = (String) map.get("ipsDoTime"); //IPS处理时间
                        }
                        if (map.get("ipsTrdAmt") != null && !map.get("ipsTrdAmt").equals("")){
                            ipsTrdAmt = (String) map.get("ipsTrdAmt"); //用户IPS实际到账金额(String型)
                            if (ipsTrdAmt != null && !ipsTrdAmt.equals("")){
                                ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //用户IPS实际到账金额(Double型)
                            }
                        }
                        if (map.get("ipsFee") != null && !map.get("ipsFee").equals("")){
                            ipsFee = (String) map.get("ipsFee"); //IPS手续费金额(String型)
                            if (ipsFee != null && !ipsFee.equals("")){
                                ipsFeeDou = TypeTool.getDouble(ipsFee); //IPS手续费金额(Double型)
                            }
                        }
                        if (map.get("merFee") != null && !map.get("merFee").equals("")){
                            merFee = (String) map.get("merFee"); //平台手续费(String型)
                            if (merFee != null && !merFee.equals("")){
                                merFeeDou = TypeTool.getDouble(merFee); //平台手续费(Double型)
                            }
                        }
                        if (map.get("trdStatus") != null && !map.get("trdStatus").equals("")){
                            trdStatus = (String) map.get("trdStatus"); //充值状态
                        }
                    }
                }
            }

            /*************** 环迅接口充值记录部分Start ***************/
            HuanxunRecharge huanxunRecharge = new HuanxunRecharge();
            Uidrechargehx = UUID.randomUUID().toString().replaceAll("-",""); //主键(环迅充值)
            huanxunRecharge.setId(Uidrechargehx);
            huanxunRecharge.setGlid(glId); //关联id
            huanxunRecharge.setMemberid(memberId); //充值用户id
            huanxunRecharge.setSource("0"); //来源(0-电脑端、1-手机端)
            huanxunRecharge.setResultcode(resultCode); //返回编号(000000-成功、999999-失败)
            huanxunRecharge.setResultmsg(resultMsg); //响应信息描述
            huanxunRecharge.setMerchantid(merchantID); //商户存管交易账号
            huanxunRecharge.setSign(sign); //签名
            huanxunRecharge.setMerbillno(merBillNo); //商户订单号
            huanxunRecharge.setDeposittype(depositType); //充值类型(1-普通充值、2-还款充值)
            huanxunRecharge.setChanneltype(channelType); //渠道种类(1-个人网银、2-企业网银)
            huanxunRecharge.setBankcode(bankCode); //充值银行
            huanxunRecharge.setIpsbillno(ipsBillNo); //充值的IPS订单号
            huanxunRecharge.setIpsdotime(ipsDoTime); //IPS处理时间
            huanxunRecharge.setIpstrdamt(ipsTrdAmtDou); //用户IPS实际到账金额(Double型)
            huanxunRecharge.setIpsfee(ipsFeeDou); //IPS手续费金额(Double型)
            huanxunRecharge.setMerfee(merFeeDou); //平台手续费(Double型)
            huanxunRecharge.setTrdstatus(trdStatus); //充值状态
            huanxunRecharge.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
            /**************** 环迅接口充值记录部分End ****************/

            MemberMessage memberMessage = new MemberMessage(); //我的消息部分
            TradeHandle tradeHandle = new TradeHandle(); //流水号工具类
            TradeDetail tradeDetail = new TradeDetail(); //交易记录

            if(resultCode != null && !resultCode.equals("")){ //响应吗
                if (resultCode.equals("000000")){ //成功
                    if (ipsDoTime != null && !ipsDoTime.equals("")){
                        ipsDoTime = ipsDoTime.substring(0,16);
                    }
                    /********** 我的消息部分(Start) **********/
                    UidMyMessage = UUID.randomUUID().toString().replaceAll("-","");
                    title = "环迅充值成功";
                    content = "<li>商户订单号：<span>"+merBillNo+"</span></li>"
                            + "<li>充值银行编号：<span>"+bankCode+"</span></li>"
                            + "<li>IPS订单号：<span>"+ipsBillNo+"</span></li>"
                            + "<li>IPS处理时间：<span>"+ipsDoTime+"</span></li>"
                            + "<li>实际到账金额：<span>"+ipsTrdAmtDou+"元</span></li>";

                    memberMessage.setId(UidMyMessage); //主键
                    memberMessage.setGlid(Uidrechargehx); //关联id
                    memberMessage.setType("0"); //类型(0-系统消息)
                    memberMessage.setMemberid(memberId); //会员id
                    memberMessage.setReadflag("1"); //读取状态(0-已读、1-未读)
                    memberMessage.setTitle(title); //题目
                    memberMessage.setContent(content); //内容
                    memberMessage.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送日期
                    memberMessage.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建日期
                    memberMessage.setDelflg("1"); //删除标志(0-已删除、1-未删除)
                    /*********** 我的消息部分(End) ***********/

                    /********** 交易记录Start **********/
                    int serialNumber = tradeHandle.getSerialNumber(); //流水号
                    UidTradeDetail = UUID.randomUUID().toString().replaceAll("-",""); //交易记录主键
                    tradeDetail.setId(UidTradeDetail); //主键
                    tradeDetail.setGlid(Uidrechargehx); //关联id
                    tradeDetail.setSerialnum(serialNumber); //流水号
                    tradeDetail.setType("0"); //类型(0-充值)
                    tradeDetail.setSprice(ipsTrdAmtDou); //实际金额
                    tradeDetail.setProcedurefee(ipsFeeDou); //手续费
                    tradeDetail.setFlag("0"); //状态(0-成功、1-失败)
                    tradeDetail.setMemberid(memberId); //会员id
                    tradeDetail.setSource(source); //来源(0-电脑端、1-手机端)
                    tradeDetail.setTime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //交易时间
                    tradeDetail.setBz("环迅支付充值"); //备注
                    tradeDetail.setBalance(balanceDou); //余额
                    tradeDetail.setCreateuser(memberId); //创建人
                    tradeDetail.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                    /*********** 交易记录End ***********/
                }
            }

            mapException.put("memberId",memberId); //会员id
            mapException.put("glId",glId); //关联id
            mapException.put("source",source); //来源(0-电脑端、1-手机端)
            mapException.put("resultCode",resultCode); //返回编号(000000-成功、999999-失败)
            mapException.put("resultMsg",resultMsg); //返回信息
            mapException.put("merchantID",merchantID); //商户存管交易账号
            mapException.put("sign",sign); //签名
            mapException.put("response",response); //响应信息

            huanXunRechargeService.rechargehxResult(huanxunRecharge,tradeDetail,memberMessage); //环迅充值
            System.out.println("环迅充值执行结束!");

        }catch(Exception e){
            HuanXunReChargeException.handle(mapException);
            e.printStackTrace();
        }
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
