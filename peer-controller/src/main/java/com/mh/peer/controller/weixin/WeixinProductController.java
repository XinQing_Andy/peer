package com.mh.peer.controller.weixin;

import com.mh.peer.model.message.WeixinProductAttornDetail;
import com.mh.peer.model.message.WeixinProductBuyMes;
import com.mh.peer.model.message.WeixinProductMemberMes;
import com.mh.peer.model.message.WeixinProductMessageBean;
import com.mh.peer.service.product.ProductService;
import com.mh.peer.util.JetbrickTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Cuibin on 2016/7/5.
 */
@Controller
@RequestMapping("/weixin")
public class WeixinProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private JetbrickTool jetbrickTool;

    /**
     * 获取推荐产品
     */
    @RequestMapping(value = "/productList", method = RequestMethod.GET)
    public String productList(@ModelAttribute WeixinProductMessageBean weixinProductMes, ModelMap modelMap) {
        try {
            List<WeixinProductMessageBean> list = productService.getWeixinProductList(weixinProductMes);
            String count = productService.getWeixinProductCount(weixinProductMes); //获取总数
            modelMap.addAttribute("productList", list);
            modelMap.addAttribute("count", count);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "weixin/weixin_product";
    }

    /**
     * 按照条件获取推荐产品
     */
    @RequestMapping(value = "/queryProductByType", method = RequestMethod.POST)
    @ResponseBody
    public WeixinProductMessageBean queryProductByType(@ModelAttribute WeixinProductMessageBean weixinProductMes, ModelMap modelMap) {

        List<WeixinProductMessageBean> list = productService.getWeixinProductList(weixinProductMes);//按条件获取产品集合
        String count = productService.getWeixinProductCount(weixinProductMes); //获取总数

        String repayment = ""; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        String repaymentName = ""; //还款方式名称
        String typeName = "";
        String str = "";
        for (WeixinProductMessageBean temp : list) {
            repayment = temp.getRepayment(); //还款方式
            if (repayment != null && !repayment.equals("")) {
                if (repayment.equals("0")) { //按月还款、等额本息
                    repaymentName = "按月还款、等额本息";
                } else if (repayment.equals("1")) {
                    repaymentName = "按月付息、到期还本";
                } else if (repayment.equals("2")) {
                    repaymentName = "一次性还款";
                }
            }

            String typeId = temp.getTypeId(); //产品类型id
            if (typeId != null && !typeId.equals("")) {
                if (typeId.equals("26")) { //产品类型(车贷宝)
                    typeName = "车";
                } else if (typeId.equals("27")) {
                    typeName = "房";
                } else if (typeId.equals("28")) {
                    typeName = "融";
                } else if (typeId.equals("29")) {
                    typeName = "票";
                }
            }

            str = str + "<a onclick=\"openProductParticulars1('" + temp.getId() + "')\">" +
                    "<div class=\"finac_info\" style=\"margin-top:18px\">" +
                    "<div class=\"finac_con\">" +
                    "<div class=\"fi_title\">" +
                    "<p class=\"fi_che\">" +
                    typeName +
                    "</p>" +
                    "<ul class=\"fi_ul\">" +
                    "<li class=\"fi_yang\">" +
                    temp.getTitle() +
                    "</li>" +
                    "<li class=\"fi_hk\">" +
                    "还款方式:" +
                    repaymentName +
                    "</li>" +
                    "</ul>" +
                    "</div>" +
                    "<table class=\"finac_table_sj\">" +
                    "<tr>" +
                    "<td>" +
                    "<span class=\"ft_zhi\">" + temp.getLoanRate() + "</span>%" +
                    "</td>" +
                    "<td>" +
                    "借款期限：<span class=\"ft_zhi\">" + temp.getLoanLength() + "</span>" +
                    "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>" +
                    "年利率" +
                    "</td>" +
                    "<td>" +
                    "借款金额(元)：<span class=\"ft_zhi\">" + temp.getLoadFee() + "</span>" +
                    "</td>" +
                    "</tr>" +
                    "</table>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</a>";
        }

        WeixinProductMessageBean result = new WeixinProductMessageBean();
        result.setCount(count);
        result.setResult("success");
        result.setHtmlText(str);
        return result;
    }

    /**
     * 查询债权转让列表
     */
    @RequestMapping(value = "/queryProductAttornByType", method = RequestMethod.POST)
    @ResponseBody
    public WeixinProductAttornDetail queryProductAttornByType(@ModelAttribute WeixinProductAttornDetail weixinProductAttornDetail, ModelMap modelMap) {

        List<WeixinProductAttornDetail> list = productService.queryProductAttornByType(weixinProductAttornDetail);//按条件获取产品集合
        String count = productService.queryProductAttornByTypeCount(weixinProductAttornDetail); //获取总数

        String typeName = "";
        String str2 = "";
        for (WeixinProductAttornDetail temp : list) {

            String typeId = temp.getTypeId(); //产品类型id
            if (typeId != null && !typeId.equals("")) {
                if (typeId.equals("26")) { //产品类型(车贷宝)
                    typeName = "车";
                } else if (typeId.equals("27")) {
                    typeName = "房";
                } else if (typeId.equals("28")) {
                    typeName = "融";
                } else if (typeId.equals("29")) {
                    typeName = "票";
                }
            }

            str2 = str2 + "<a onclick=\"queryAttornDetails('" + temp.getId() + "')\">" +
                    "<div class=\"finac_info\" style=\"margin-top:18px\">" +
                    "<div class=\"finac_con\">" +
                    "<div class=\"fi_title\">" +
                    "<p class=\"fi_che\">" +
                    typeName +
                    "</p>" +
                    "<ul class=\"fi_ul\">" +
                    "<li class=\"fi_yang\">" +
                    temp.getTitle() +
                    "</li>" +
                    "</ul>" +
                    "</div>" +
                    "<table class=\"finac_table_sj\" style=\"border-collapse:separate;border-spacing:0px;\">" +
                    "<tr>" +
                    "<td>" +
                    "<span class=\"ft_zhi\">" + temp.getExpectedrate() + "</span>%" +
                    "</td>" +
                    "<td>" +
                    "剩余期限：<span class=\"ft_zhi\">" + temp.getSurplusdays() + "天</span>" +
                    "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>" +
                    "预期收益率" +
                    "</td>" +
                    "<td>" +
                    "转让金额(元)：<span class=\"ft_zhi\">" + temp.getAttornprice() + "</span>" +
                    "</td>" +
                    "</tr>" +
                    "</table>" +
                    "</div>" +
                    "</div>" +
                    "</a>";
        }

        WeixinProductAttornDetail result = new WeixinProductAttornDetail();
        result.setCount(count);
        result.setResult("success");
        result.setHtmlText(str2);

        return result;
    }

    /**
     * 查询债权转让详情
     */
    @RequestMapping(value = "/queryAttornDetails", method = RequestMethod.GET)
    public String queryAttornDetails(HttpServletRequest request, ModelMap modelMap) {
        String id = request.getParameter("id");
        modelMap.put("weixinProductAttornDetail", productService.queryAttornDetails(id));

        return "weixin/weixin_attorn_details";
    }

    /**
     * 查看产品详情
     */
    @RequestMapping(value = "/productDetails1", method = RequestMethod.GET)
    public String productDetails1(HttpServletRequest request, ModelMap modelMap) {
        String id = request.getParameter("id");
        WeixinProductMessageBean weixinProductMessageBean = new WeixinProductMessageBean();
        weixinProductMessageBean.setId(id);
        weixinProductMessageBean = productService.productDetails(weixinProductMessageBean);

        modelMap.put("weixinProductMessageBean", weixinProductMessageBean);

        return "weixin/weixin_product_details";
    }

    /**
     * 查看借款信息
     */
    @RequestMapping(value = "/openProductBorrow", method = RequestMethod.GET)
    public String openProductBorrow(HttpServletRequest request, ModelMap modelMap) {
        String id = request.getParameter("id");
        WeixinProductMemberMes weixinProductMemberMes = new WeixinProductMemberMes();
        weixinProductMemberMes.setId(id);
        weixinProductMemberMes = productService.openProductBorrow(weixinProductMemberMes);

        modelMap.put("weixinProductMemberMes", weixinProductMemberMes);

        return "weixin/weixin_borrow_info";
    }


    /**
     * 查看投资记录
     */
    @RequestMapping(value = "/openLoanDetails", method = RequestMethod.GET)
    public String openLoanDetails(HttpServletRequest request, ModelMap modelMap) {
        String productid = request.getParameter("productid");
        WeixinProductBuyMes weixinProductBuyMes = new WeixinProductBuyMes();
        weixinProductBuyMes.setProductid(productid);
        List<WeixinProductBuyMes> list = productService.openLoanDetails(weixinProductBuyMes);
        //已投资金额
        double priceDou = 0;
        for (WeixinProductBuyMes temp : list) {
            String price = temp.getPrice();
            priceDou += Double.parseDouble(price);
        }

        //借款金额
        WeixinProductMemberMes weixinProductMemberMes = new WeixinProductMemberMes();
        weixinProductMemberMes.setId(productid);
        weixinProductMemberMes = productService.openProductBorrow(weixinProductMemberMes);
        double loadfee = Double.parseDouble(weixinProductMemberMes.getLoadfee());

        //剩余投标金额
        double residue = loadfee - priceDou < 0 ? 0 : loadfee - priceDou;

        modelMap.put("weixinProductBuyMesList", list);
        modelMap.put("residue", residue + "");
        modelMap.put("priceDou", priceDou + "");

        return "weixin/weixin_loan_details";
    }

    /**
     * 查看债权转让 投资记录
     */
    @RequestMapping(value = "/openAttornLoanDetails", method = RequestMethod.GET)
    public String openAttornLoanDetails(HttpServletRequest request, ModelMap modelMap) {
        String productid = request.getParameter("productid");
        WeixinProductBuyMes weixinProductBuyMes = new WeixinProductBuyMes();
        weixinProductBuyMes.setProductid(productid);
        List<WeixinProductBuyMes> list = productService.openLoanDetails(weixinProductBuyMes);
        //已投资金额
        double priceDou = 0;
        for (WeixinProductBuyMes temp : list) {
            String price = temp.getPrice();
            priceDou += Double.parseDouble(price);
        }

        //借款金额
        WeixinProductMemberMes weixinProductMemberMes = new WeixinProductMemberMes();
        weixinProductMemberMes.setId(productid);
        weixinProductMemberMes = productService.openProductBorrow(weixinProductMemberMes);
        double loadfee = Double.parseDouble(weixinProductMemberMes.getLoadfee());

        //剩余投标金额
        double residue = loadfee - priceDou < 0 ? 0 : loadfee - priceDou;

        modelMap.put("weixinProductBuyMesList", list);
        modelMap.put("residue", residue + "");
        modelMap.put("priceDou", priceDou + "");

        return "weixin/weixin_attorn_loan_details";
    }


}
