package com.mh.peer.controller.huanxun;

import com.mh.peer.model.entity.HuanxunRegist;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-7-22.
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunRegistController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private HuanXunRegistService huanXunRegistService;

    /**
     * 环迅部分注册
     * @param huanXunRegistMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/registerhx", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunRegistMessageBean registerhx(@ModelAttribute HuanXunRegistMessageBean huanXunRegistMes, ModelMap modelMap) {
        String merBillNo = huanXunRegistMes.getMerBillNo(); //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //开户日期
        String userType = huanXunRegistMes.getUserType(); //用户类型(1-个人、2-企业)
        String userName = huanXunRegistMes.getUserName(); //用户名
        String mobileNo = huanXunRegistMes.getMobileNo(); //手机号
        String identNo = huanXunRegistMes.getIdentNo(); //身份证号
        String bizType = huanXunRegistMes.getBizType(); //业务类型(1-P2P、2-众筹)
        String realName = huanXunRegistMes.getRealName(); //真实姓名
        String enterName = huanXunRegistMes.getEnterName(); //企业名称
        String orgCode = huanXunRegistMes.getOrgCode(); //营业执照编码
        String isAssureCom = huanXunRegistMes.getIsAssureCom(); //是否为担保企业
        String webUrl = huanXunRegistMes.getWebUrl(); //页面返回地址
        String s2SUrl = huanXunRegistMes.getS2SUrl(); //后台通知地址
        String operationType = "user.register"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String sign = ""; //签名
        String signJm = ""; //签名加密
        String request = ""; //请求信息
        String requestJm = ""; //请求信息加密
        String md5Zs = ""; //md5证书

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs();

            request = "{\"merBillNo\":\""+merBillNo+"\",\"merDate\":\""+merDate+"\"," +
                      "\"userType\":\""+userType+"\",\"userName\":\""+userName+"\",\"mobileNo\":\""+mobileNo+"\"," +
                      "\"identNo\":\""+identNo+"\",\"bizType\":\""+bizType+"\",\"realName\":\""+realName+"\"," +
                      "\"enterName\":\""+enterName+"\",\"orgCode\":\""+orgCode+"\",\"isAssureCom\":\""+isAssureCom+"\"," +
                      "\"webUrl\":\""+webUrl+"\",\"s2SUrl\":\""+s2SUrl+"\"}";

            requestJm = Pdes.encrypt3DES(request); //3des加密

            sign = operationType + merchantID + requestJm + md5Zs; //签名

            signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

            System.out.println("开始注册请求");
            System.out.println("request==="+request);
            System.out.println("requestJm==="+requestJm);
            System.out.println("sign==="+sign);
            System.out.println("signJm==="+signJm);

            huanXunRegistMes.setOperationType(operationType); //操作类型
            huanXunRegistMes.setMerchantID(merchantID); //商户存管交易账号
            huanXunRegistMes.setSign(signJm); //签名
            huanXunRegistMes.setRequest(requestJm); //请求信息
        }catch(Exception e){
            e.printStackTrace();
        }
        return huanXunRegistMes;
    }


    /**
     * 环迅注册结果
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/registerhxResult", method = RequestMethod.POST)
    @ResponseBody
    public void registerhxResult(HttpServletRequest request, ModelMap modelMap) {

        String memberId = request.getParameter("memberId"); //会员id
        String resultCode = request.getParameter("resultCode"); //响应吗
        String resultMsg = request.getParameter("resultMsg"); //响应信息
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String response = request.getParameter("response").replaceAll("\r\n",""); //响应信息(以3des加密形式)
        String responseJm = ""; //响应信息(解密)
        String merBillNo = ""; //商户订单号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS注册日期
        String ipsAcctNo = ""; //IPS虚拟账号
        String status = ""; //注册状态(0-失败、1-成功、2-待审核)
        String UidHx = ""; //主键(环迅注册)
        String UidMyMessage = ""; //主键(我的消息)
        String title = ""; //我的消息题目
        String content = ""; //我的消息内容
        Map<String,String> map = new HashMap<String,String>();

        /********** 服务器打印信息Start **********/
        //System.out.println("会员id memberId======"+memberId);
        //System.out.println("开始返回信息");
        //System.out.println("resultCode======"+resultCode);
        //System.out.println("resultMsg======"+resultMsg);
        //System.out.println("merchantID======"+merchantID);
        //System.out.println("sign======"+sign);
        //System.out.println("response======"+response);
        /*********** 服务器打印信息End ***********/

        if (response != null && !response.equals("")){
            responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
            if (responseJm != null && !responseJm.equals("")){
                map = JsonHelper.getObjectToMap(responseJm);
                if (map != null){
                    merBillNo = map.get("merBillNo");
                    ipsBillNo = map.get("ipsBillNo");
                    ipsDoTime = map.get("ipsDoTime");
                    ipsAcctNo = map.get("ipsAcctNo");
                    status = map.get("status");
                }
            }
        }

        /********** 环迅注册部分(Start) **********/
        HuanxunRegist huanxunRegist = new HuanxunRegist();
        UidHx = UUID.randomUUID().toString().replaceAll("-","");
        huanxunRegist.setId(UidHx); //主键
        huanxunRegist.setMemberid(memberId); //会员id
        huanxunRegist.setResultcode(resultCode); //响应吗
        huanxunRegist.setResultmsg(resultMsg); //响应信息
        huanxunRegist.setMerchantid(merchantID); //商户存管交易账号
        huanxunRegist.setSign(sign); //签名
        huanxunRegist.setResponse(response); //响应信息
        huanxunRegist.setMerbillno(merBillNo); //商户订单号
        huanxunRegist.setIpsbillno(ipsBillNo); //IPS订单号
        huanxunRegist.setIpsdotime(ipsDoTime); //IPS注册日期
        huanxunRegist.setIpsacctno(ipsAcctNo); //IPS虚拟账号(成功时返回)
        huanxunRegist.setStatus(status); //注册状态(0-失败、1-成功、2-待审核)
        huanxunRegist.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
        /*********** 环迅注册部分(End) ***********/

        MemberMessage memberMessage = new MemberMessage();
        if(resultCode != null && !resultCode.equals("")){ //响应吗
            if (resultCode.equals("000000")){ //成功
                /********** 我的消息部分(Start) **********/
                UidMyMessage = UUID.randomUUID().toString().replaceAll("-","");
                title = "环迅注册成功";
                content = "<li>商户订单号：<span>"+merBillNo+"</span></li>"
                        + "<li>IPS订单号：<span>"+ipsBillNo+"</span></li>"
                        + "<li>IPS注册日期：<span>"+ipsDoTime+"</span></li>"
                        + "<li>IPS虚拟账号：<span>"+ipsAcctNo+"</span></li>";

                memberMessage.setId(UidMyMessage); //主键
                memberMessage.setGlid(UidHx); //关联id
                memberMessage.setType("0"); //类型(0-系统消息)
                memberMessage.setMemberid(memberId); //会员id
                memberMessage.setReadflag("1"); //读取状态(0-已读、1-未读)
                memberMessage.setTitle(title); //题目
                memberMessage.setContent(content); //内容
                memberMessage.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送日期
                memberMessage.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建日期
                memberMessage.setDelflg("1"); //删除标志(0-已删除、1-未删除)
                /*********** 我的消息部分(End) ***********/
            }
        }

        huanXunRegistService.registerhxResult(huanxunRegist,memberMessage); //环迅注册
        //System.out.println("执行完毕！");
    }



}
