package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.business.ProductBusinessBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.bank.BankService;
import com.mh.peer.service.front.PersonageSettingService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.product.ProductService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.PublicsTool;
import com.mh.peer.util.SendMobileMessage;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

/**
 * Created by Cuibin on 2016/5/17.
 */
@Controller
@RequestMapping("/bankSet")
public class BankSetController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private PersonageSettingService personageSettingService;
    @Autowired
    private BankService bankService;
    @Autowired
    private AllMemberService allMemberService;

    /**
     * 跳转到设置银行卡
     */
    @RequestMapping(value = "/openBankSet", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openBankSet(ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            bean.setInfo("会话过期 请重新登陆");
            bean.setResult("error");
            return bean;
        }
        //查询当前银行卡
        MemberBanknoMessageBean memberBanknoMessageBean = bankService.queryUserBank(memberInfo.getId());
        String cardno = memberBanknoMessageBean.getCardno();
        //当前银行卡号
        String newCardno = "************";
        //尾号
        String tailNum = null;
        if (cardno != null && !cardno.equals("") && cardno.length() >= 16) {
            newCardno = cardno.substring(0, 4) + "******" + cardno.substring(cardno.length() - 3, cardno.length());
            tailNum = cardno.substring(cardno.length() - 3, cardno.length());
        }
        //银行名称
        String bankid = memberBanknoMessageBean.getBankid();
        BankWhMessageBean bankWhMessageBean = new BankWhMessageBean();
        bankWhMessageBean.setId(bankid);
        BankWhMessageBean newbankWh = bankService.queryOnlyBank(bankWhMessageBean);
        //是否已经设置银行卡 0否
        if (cardno == null || cardno.equals("")) {
            modelMap.put("isSetBank", "0");
        }
        //真实姓名
        String realname = "";
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        memberInfoMessageBean.setId(memberInfo.getId()); //会员id
        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
        if (memberInfoMessageBean != null) {
            realname = memberInfoMessageBean.getRealname();
        }
        modelMap.put("bankname", newbankWh.getBankname());
        modelMap.put("cardno", newCardno);
        modelMap.put("tailNum", tailNum);
        modelMap.put("realname", realname);
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/bankSet/bank_set.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 跳转到添加银行卡
     */
    @RequestMapping(value = "/openBankAdd", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openBankAdd(ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        //查询所有省
        List listProvince = personageSettingService.queryAllProvince();
        //查询所有银行
        BankWhMessageBean bankWhMessageBean = new BankWhMessageBean();
        List<BankWhMessageBean> list = bankService.queryAllBank(bankWhMessageBean);
        //查询用户memberBankno
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        MemberInfoMessageBean mimb2 = null;
        if (memberInfo != null) {
            MemberInfoMessageBean mimb = new MemberInfoMessageBean();
            mimb.setId(memberInfo.getId()); //会员主键
            mimb2 = allMemberService.queryOnlyMember(mimb);
        }

        modelMap.put("nickname", mimb2.getNickname());
        modelMap.put("listProvince", listProvince);
        modelMap.put("AllBankList", list);

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/bankSet/bank_set_add.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 查询城市(下拉菜单级联)
     */
    @RequestMapping(value = "/openBankAddQueryCity", method = RequestMethod.POST)
    @ResponseBody
    public SysCityMessagebean openBankAddQueryCity(@ModelAttribute SysCityMessagebean sysCityMessagebean) {
        String provinceId = sysCityMessagebean.getProvinceid(); //省id
        List<SysCityMessagebean> list = personageSettingService.queryCity(provinceId);
        String str = ""; //城市部分拼接
        if (list.size() > 0) {
            for (SysCityMessagebean sysCityMes : list) {
                str = str + "<option value=\"" + sysCityMes.getId() + "\">" + sysCityMes.getCityname() + "</option>";
            }

            sysCityMessagebean.setCityPj(str);
        }
        return sysCityMessagebean;
    }

    /**
     * 保存银行卡信息
     */
    @RequestMapping(value = "/saveFrontBank", method = RequestMethod.POST)
    @ResponseBody
    public MemberBanknoMessageBean saveFrontBank(@ModelAttribute MemberBanknoMessageBean memberBanknoMessageBean) {
        return bankService.saveFrontBank(memberBanknoMessageBean);
    }

    /**
     * 跳转到更新页面
     */
    @RequestMapping(value = "/openUpdatebank", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openUpdatebank(ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            bean.setInfo("会话过期 请重新登陆");
            bean.setResult("error");
            return bean;
        }

        //查询所有省
        List listProvince = personageSettingService.queryAllProvince();

        //查询绑定银行卡信息
        MemberBanknoMessageBean memberBanknoMes = new MemberBanknoMessageBean();
        memberBanknoMes.setMemberid(memberInfo.getId());
        List<MemberBanknoMessageBean> mlist = bankService.getMemberBankInfoBySome(memberBanknoMes);
        //城市字符串拼接
        String provinceId = mlist.get(0).getProvince();
        String cityId = mlist.get(0).getCity(); //城市id
        //System.out.println("===================="+cityId);
        String bankId = mlist.get(0).getBankid();
        String selected = "";
        String strCity = "";
        List<SysCityMessagebean> listCity = personageSettingService.queryCity(provinceId); //城市集合
        if (listCity != null && listCity.size() > 0) {
            for (SysCityMessagebean sysCityMes : listCity) {
                if (cityId.equals(sysCityMes.getId())) {
                    selected = "selected";
                } else {
                    selected = "";
                }
                strCity = strCity + "<option value=\"" + sysCityMes.getId() + "\" " + selected + ">" + sysCityMes.getCityname() + "</option>";
            }
        }
        //查询所有银行
        BankWhMessageBean bankWhMessageBean = new BankWhMessageBean();
        List<BankWhMessageBean> list = bankService.queryAllBank(bankWhMessageBean);

        modelMap.put("listProvince", listProvince);
        modelMap.put("AllBankList", list);
        modelMap.put("nickname", memberInfo.getNickname());
        modelMap.put("strCity", strCity);
        modelMap.put("provinceId", provinceId);
        modelMap.put("bankId", bankId);
        modelMap.put("bankaddress", mlist.get(0).getBankaddress());

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/bankSet/bank_set_update.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 更新银行
     */
    @RequestMapping(value = "/frontUpdateBank", method = RequestMethod.POST)
    @ResponseBody
    public MemberBanknoMessageBean frontUpdateBank(@ModelAttribute MemberBanknoMessageBean memberBanknoMessageBean) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        MemberBanknoMessageBean result = new MemberBanknoMessageBean();
        if (memberInfo == null) {
            result.setInfo("会话过期 请重新登陆");
            result.setResult("error");
            return result;
        }
        //查询当前银行卡
//        MemberBanknoMessageBean myMemberBanknoMes = bankService.queryUserBank(memberInfo.getId());
//        String cardno = "";
//        if (myMemberBanknoMes != null) {
//            cardno = myMemberBanknoMes.getCardno();
//        }

        //验证当前银行卡号
//        String nowCardno = memberBanknoMessageBean.getNowCardno();
//        if (!nowCardno.equals(cardno)) {
//            result.setInfo("当前银行卡号不正确");
//            result.setResult("error");
//            return result;
//        }

        //获取交易密码
//        String myTraderpassword = "";
//        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
//        memberInfoMessageBean.setId(memberInfo.getId()); //会员id
//        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
//        if (memberInfoMessageBean != null) {
//            myTraderpassword = memberInfoMessageBean.getTraderpassword();
//        }
//        if (myTraderpassword == null || myTraderpassword.equals("")) {
//            result.setInfo("请先去设置交易密码");
//            result.setResult("error");
//            return result;
//        }
//        //System.out.println("==================="+myTraderpassword);
//        String traderpassword = memberBanknoMessageBean.getTradepassword();
//        if (!traderpassword.equals(myTraderpassword)) {
//            result.setInfo("交易密码错误");
//            result.setResult("error");
//            return result;
//        }
//        //验证 验证码
//        String code = memberBanknoMessageBean.getCode();
//        String updateBankCode = (String) servletUtil.getSession().getAttribute("updateBankCode");
//        if (!code.equals(updateBankCode)) {
//            result.setInfo("验证码不正确");
//            result.setResult("error");
//            return result;
//        }
        return bankService.frontUpdatebank(memberBanknoMessageBean);
    }

    /**
     * 更新银行 发送验证码
     */
    @RequestMapping(value = "/updateBankGetCode", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean updateBankGetCode(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean) throws IOException {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        JetbrickBean jetbrickBean = new JetbrickBean();
        HttpSession session = servletUtil.getSession();
        String updateBankCodeMessage = (String) session.getAttribute("updateBankCodeMessage");

        if (updateBankCodeMessage == null || updateBankCodeMessage.equals("")) {
            Date a = new Date();
            updateBankCodeMessage = a.getTime() + "";
            session.setAttribute("updateBankCodeMessage", updateBankCodeMessage);
        } else {
            long time = Long.parseLong(updateBankCodeMessage);
            Date b = new Date();
            long now = b.getTime();
            if ((now - time) / 1000 < 60) {
                jetbrickBean.setInfo("提交过于频繁 请稍后重试");
                jetbrickBean.setResult("error");
                return jetbrickBean;
            }
            session.setAttribute("updateBankCodeMessage", now + "");
        }
        String telephone = ""; //获取手机号
        memberInfoMessageBean.setId(memberInfo.getId()); //会员id
        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
        if (memberInfoMessageBean != null) {
            telephone = memberInfoMessageBean.getTelephone();
        }
        SendMobileMessage sendMobileMessage = new SendMobileMessage();
        PublicsTool publicsTool = new PublicsTool();
        String random = publicsTool.getRandom();
        session.setAttribute("updateBankCode", random);
        String content = "【煎饼金融P2P】尊敬的用户您好,验证码为:" + random + ",请妥善保管,验证码提供给他人将导致信息泄露";
        String message = sendMobileMessage.send(telephone, content);
        //String message = "0";
        //System.out.println("============" + random);
        if (message.equals("0")) {
            jetbrickBean.setResult("success");
            jetbrickBean.setInfo("验证码已发送");
        } else {
            jetbrickBean.setResult("error");
        }
        return jetbrickBean;
    }


}
