package com.mh.peer.controller.appMobile;

import com.mh.peer.model.message.AppNewsContentMessageBean;
import com.mh.peer.model.message.AppSysFileMessageBean;
import com.mh.peer.service.news.NewsService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.PublicAddress;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhangerxin on 2016-7-15.
 */
@Controller
@RequestMapping("/appNews")
public class AppNewsController {

    @Autowired
    private NewsService newsService;
    @Autowired
    private SysFileService sysFileService;


    /**
     * 获取新闻列表
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppNewsList", method = RequestMethod.POST)
    @ResponseBody
    public void getAppNewsList(HttpServletRequest request,HttpServletResponse response) {
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String orderColumn = request.getParameter("orderColumn"); //排序字段
        String orderType = request.getParameter("orderType"); //排序类型
        String newsId = ""; //新闻id
        String newsPic = ""; //图片地址
        String address = ""; //服务器地址
        String newsListStr = ""; //新闻列表

        try{

            PublicAddress publicAddress = new PublicAddress();
            address = publicAddress.getServerAddress();

            List<AppNewsContentMessageBean> listAppNewsContent = newsService.getAppNewsList(orderColumn,orderType,pageIndex,pageSize);
            if (listAppNewsContent != null && listAppNewsContent.size() > 0){
                for (AppNewsContentMessageBean appNewsContentMessageBean : listAppNewsContent){
                    newsId = appNewsContentMessageBean.getId(); //新闻id
                    AppSysFileMessageBean appSysFileMessageBean = sysFileService.getAppSysFile(newsId, "CREATETIME", "DESC", "1", "1");
                    if (appSysFileMessageBean != null){
                        newsPic = appSysFileMessageBean.getImgUrl();
                        if (newsPic != null && !newsPic.equals("")){
                            newsPic = address + newsPic;
                        }
                    }
                    newsListStr = newsListStr + "<li onclick=\"doSearchNewsDetail('"+newsId+"')\">"
                                + "<table class=\"message_table\">"
                                + "<tr>"
                                + "<td colspan=\"3\" class=\"mess_title\">"+appNewsContentMessageBean.getTitle()+"</td>"
                                + "</tr>"
                                + "<tr>"
                                + "<td>"
                                + "<img src=\""+newsPic+"\" style=\"width:70px;height:50px;\" />"
                                + "</td>"
                                + "<td class=\"mess_txt\">"
                                + "<span>"+appNewsContentMessageBean.getContent()+"</span>"
                                + "<span style=\"line-height:35px;\">"+appNewsContentMessageBean.getShowDate()+"</span>"
                                + "</td>"
                                + "<td>"
                                + "<span class=\"icon-app-05\"></span>"
                                + "</td>"
                                + "</tr>"
                                + "</table>"
                                + "</li>";
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("newsListStr",newsListStr); //新闻列表
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取新闻详细信息
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAppNewsDetail", method = RequestMethod.POST)
    @ResponseBody
    public void getAppNewsDetail(HttpServletRequest request,HttpServletResponse response){
        String newsId = request.getParameter("newsId"); //新闻id
        String newsTitle = ""; //新闻题目
        String showDate = ""; //显示时间
        String newsPic = ""; //新闻背景图片地址
        String newsContent = ""; //新闻内容
        String address = ""; //服务器地址

        PublicAddress publicAddress = new PublicAddress();
        address = publicAddress.getServerAddress();

        AppSysFileMessageBean appSysFileMessageBean = sysFileService.getAppSysFile(newsId, "CREATETIME", "DESC", "1", "1");
        if (appSysFileMessageBean != null){
            newsPic = appSysFileMessageBean.getImgUrl();
            if (newsPic != null && !newsPic.equals("")){
                newsPic = address + newsPic;
            }
        }

        AppNewsContentMessageBean appNewsContentMessageBean = newsService.getAppNewsDetails(newsId);
        if (appNewsContentMessageBean != null){
            newsTitle = appNewsContentMessageBean.getTitle(); //题目
            showDate = appNewsContentMessageBean.getShowDate(); //显示时间
            newsContent = appNewsContentMessageBean.getContent(); //内容
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("newsTitle",newsTitle); //题目
        in.put("showDate",showDate); //显示时间
        in.put("newsContent",newsContent); //内容
        in.put("newsPic",newsPic); //新闻背景图片地址
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
