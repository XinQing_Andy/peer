package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.front.RechargeService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.FrontPage;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Cuibin on 2016/6/17.
 */

@Controller
@RequestMapping("/recharge")
public class RechargeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RechargeController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private RechargeService rechargeService;
    @Autowired
    private AllMemberService allMemberService;

    /**
     * 跳转到充值页
     * @param chargeRecordMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openRecharge", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openRecharge(ChargeRecordMessageBean chargeRecordMes,ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        String chargeType = chargeRecordMes.getChargeType(); //充值类型(1-普通充值、2-还款充值)
        String balance = "0"; //余额
        if (memberInfo == null) {
            bean.setInfo("会话已过期 请重新登录");
            bean.setResult("error");
            return bean;
        }

        /*************** 会员信息Start ***************/
        memberId = memberInfo.getId(); //会员id
        balance = allMemberService.queryOnlyMemberBalance(memberId);
        modelMap.put("balance",balance); //余额
        /**************** 会员信息End ****************/
        modelMap.put("chargeType",chargeType); //充值类型(1-普通充值、2-还款充值)

        try {

            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(chargeRecordMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 跳转到充值记录
     */
    @RequestMapping(value = "/openRechargeHistory", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openRechargeHistory(@ModelAttribute ViewHuanxunRechargeMes viewHuanxunRechargeMes,ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        HttpSession session = servletUtil.getSession();
        String pageIndex = viewHuanxunRechargeMes.getPageIndex(); //起始数量
        String pageSize = viewHuanxunRechargeMes.getPageSize(); //每页显示数量
        String orderColumn = viewHuanxunRechargeMes.getOrderColumn(); //排序字段
        String orderType = viewHuanxunRechargeMes.getOrderType(); //排序类型
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        String pageStr = ""; //分页部分拼接

        if (memberInfo == null) {
            bean.setInfo("会话已过期 请重新登录");
            bean.setResult("error");
            return bean;
        }

        if(pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
            viewHuanxunRechargeMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "5";
            viewHuanxunRechargeMes.setPageSize(pageSize);
        }
        if (orderColumn == null || orderColumn.equals("")){ //排序字段
            orderColumn = "CREATETIME";
            viewHuanxunRechargeMes.setOrderColumn(orderColumn);
        }
        if (orderType == null || orderType.equals("")){ //排序类型
            orderType = "DESC";
            viewHuanxunRechargeMes.setOrderType(orderType);
        }

        /**************** 充值记录Start *****************/
        memberId = memberInfo.getId(); //会员id
        viewHuanxunRechargeMes.setMemberid(memberId);
        //充值记录集合
        List<ViewHuanxunRechargeMes> listHuanxunRechargeMes = rechargeService.queryViewRechargeHistory(viewHuanxunRechargeMes);
        //充值记录数量
        int totalCount = rechargeService.queryViewRechargeHistoryCount(viewHuanxunRechargeMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageRecharge","goRechargePage"); //充值记录部分分页
        /***************** 充值记录End ******************/

        modelMap.addAttribute("listHuanxunRechargeMes",listHuanxunRechargeMes); //充值记录集合
        modelMap.addAttribute("totalCount",totalCount); //充值记录数量
        modelMap.addAttribute("pageStr",pageStr); //分页部分拼接

        try {

            StringWriter stringWriter = null;
            stringWriter = jetbrickTool.getJetbrickTemp(viewHuanxunRechargeMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }

    /**
     * 根据条件查询充值记录
     * @param viewHuanxunRechargeMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/searchRechargeBySome", method = RequestMethod.POST)
    @ResponseBody
    public ViewHuanxunRechargeMes searchRechargeBySome(@ModelAttribute ViewHuanxunRechargeMes viewHuanxunRechargeMes,ModelMap modelMap) {
        String startTime = viewHuanxunRechargeMes.getStartTime(); //充值起日期
        String endTime = viewHuanxunRechargeMes.getEndTime(); //充值止日期
        String pageIndex = viewHuanxunRechargeMes.getPageIndex(); //起始数量
        String pageSize = viewHuanxunRechargeMes.getPageSize(); //每页显示数量
        String orderColumn = viewHuanxunRechargeMes.getOrderColumn(); //排序字段
        String orderType = viewHuanxunRechargeMes.getOrderType(); //排序类型
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        String memberId = ""; //会员id
        String str = ""; //列表部分拼接
        String pageStr = ""; //分页部分拼接
        String resultMsg = "";

        try{

            if (memberInfo == null) {
                viewHuanxunRechargeMes.setInfo("会话已过期 请重新登录");
                viewHuanxunRechargeMes.setResult("error");
                return viewHuanxunRechargeMes;
            }

            if(pageIndex == null || pageIndex.equals("")){ //起始数
                pageIndex = "1";
                viewHuanxunRechargeMes.setPageIndex(pageIndex);
            }
            if(pageSize == null || pageSize.equals("")){ //每页显示数量
                pageSize = "5";
                viewHuanxunRechargeMes.setPageSize(pageSize);
            }
            if (orderColumn == null || orderColumn.equals("")){ //排序字段
                orderColumn = "CREATETIME";
                viewHuanxunRechargeMes.setOrderColumn(orderColumn);
            }
            if (orderType == null || orderType.equals("")){ //排序类型
                orderType = "DESC";
                viewHuanxunRechargeMes.setOrderType(orderType);
            }

            /**************** 充值记录Start *****************/
            memberId = memberInfo.getId(); //会员id
            viewHuanxunRechargeMes.setMemberid(memberId);
            //充值记录集合
            List<ViewHuanxunRechargeMes> listHuanxunRechargeMes = rechargeService.queryViewRechargeHistory(viewHuanxunRechargeMes);
            //充值记录数量
            int totalCount = rechargeService.queryViewRechargeHistoryCount(viewHuanxunRechargeMes);
            FrontPage frontPage = new FrontPage();
            pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageRecharge","goRechargePage"); //充值记录部分分页
            /***************** 充值记录End ******************/

            if(listHuanxunRechargeMes != null && listHuanxunRechargeMes.size() > 0){
                for (ViewHuanxunRechargeMes viewHuanxunRechargeMessageBean : listHuanxunRechargeMes){
                    str = str + "<li>"
                        + "<ul>"
                        + "<li>"+viewHuanxunRechargeMessageBean.getIpsbillno()+"</li>"
                        + "<li>"+viewHuanxunRechargeMessageBean.getIpstrdamt()+"元</li>"
                        + "<li>"+viewHuanxunRechargeMessageBean.getIpsdotime()+"</li>"
                        + "<li>环迅支付</li>";
                    if (viewHuanxunRechargeMessageBean.getResultcode() != null && !viewHuanxunRechargeMessageBean.getResultcode().equals("")){
                        if (viewHuanxunRechargeMessageBean.getResultcode().equals("000000")){
                            resultMsg = "充值成功";
                        }else{
                            resultMsg = "充值失败";
                        }
                    }

                    str = str + "<li>"+resultMsg+"</li>"
                        + "</ul>"
                        + "</li>";
                }
            }

            viewHuanxunRechargeMes.setResult("success"); //状态
            viewHuanxunRechargeMes.setStr(str); //列表拼接
            viewHuanxunRechargeMes.setPageStr(pageStr); //分页部分
        }catch(Exception e){
            e.printStackTrace();
        }

        return viewHuanxunRechargeMes;
    }


}
