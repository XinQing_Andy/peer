package com.mh.peer.controller.appMobile;

import com.mh.peer.model.message.AppTradeMessageBean;
import com.mh.peer.service.front.TradeDetailService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhangerxin on 2016-7-12.
 */
@Controller
@RequestMapping("/appTrade")
public class AppTradeController {

    @Autowired
    private TradeDetailService tradeDetailService;

    /**
     * 获取交易记录
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getTradeRecordList", method = RequestMethod.POST)
    @ResponseBody
    public void getTradeRecordList(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String tradeTime = request.getParameter("tradeTime"); //交易时间
        String tradeType = request.getParameter("tradeType"); //交易类型
        String tradeTypeName = ""; //交易类型名称
        String tradeRecordStr = ""; //交易记录

        try{

            List<AppTradeMessageBean> listAppTradeMes = tradeDetailService.getTradeRecordList(memberId,pageIndex,pageSize,tradeType,tradeTime);
            if (listAppTradeMes != null && listAppTradeMes.size() > 0){
                for (AppTradeMessageBean appTradeMessageBean : listAppTradeMes){
                    tradeType = appTradeMessageBean.getType(); //交易类型
                    if (tradeType != null && !tradeType.equals("")){
                        if (tradeType.equals("0")){
                            tradeTypeName = "充值";
                        }else if (tradeType.equals("1")){
                            tradeTypeName = "提现";
                        }
                    }
                    tradeRecordStr = tradeRecordStr + "<tr>"
                                   + "<td class=\"rec_txt\">"+tradeTypeName+"</td>"
                                   + "<td class=\"rec_sj\">"+appTradeMessageBean.getTradePrice()+"元</td>"
                                   + "<td class=\"rec_txt\">"+appTradeMessageBean.getTime()+"</td>"
                                   + "</tr>";
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("tradeRecordStr",tradeRecordStr); //交易记录
        output(response, JSONObject.fromObject(in).toString());
    }



    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
