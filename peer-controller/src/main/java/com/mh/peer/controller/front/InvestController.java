package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.ProductInvestBusinessBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.front.PersonageSettingService;
import com.mh.peer.service.front.ReceiveService;
import com.mh.peer.service.huanxun.HuanXunRegProjectService;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.product.*;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.FrontPage;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.PublicsTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-5-4.
 * 投资部分
 */
@Controller
@RequestMapping("/invert")
public class InvestController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ProductService productService;
    @Autowired
    private SysFileService sysFileService;
    @Autowired
    private ProductTypeService productTypeService;
    @Autowired
    private ProductBuyService productBuyService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private ProductReceiveService productReceiveService;
    @Autowired
    private ProductRepayService productRepayService;
    @Autowired
    private PersonageSettingService personageSettingService;
    @Autowired
    private HuanXunRegistService huanXunRegistService;
    @Autowired
    private HuanXunRegProjectService huanXunRegProjectService;
    @Autowired
    private ReceiveService receiveService;


    /**
     * 根据分页查询投资(前台刚进入页面)
     * @param productMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryInvestByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryInvestByPage(@ModelAttribute ProductMessageBean productMes, ModelMap modelMap){

        JetbrickBean bean = new JetbrickBean();
        String productTypeIdPj = ""; //产品类型id拼接
        String selectMutli = productMes.getSelectMutli(); //是否多选(0-多选、1-单选)
        String shflag = productMes.getShflag(); //审核状态(0-审核通过、1-审核未通过、2-未审核)
        String typeName = productMes.getTypeName(); //类型名称
        String loadFeeSum = ""; //交易总额
        String interestSum = ""; //利息总额
        String pageStr = ""; //分页部分
        modelMap.addAttribute("typeName",typeName); //菜单类型；

        /************************* 产品类型部分Start *************************/
        ProductTypeMessage productTypeMessage = new ProductTypeMessage();
        //产品类型集合
        List<ProductTypeMessage> listProductTypeMes = productTypeService.queryAllProductType(productTypeMessage);
        if(listProductTypeMes != null && listProductTypeMes.size() > 0){
            /********** 产品类型循环Start **********/
            for(ProductTypeMessage productTypeMes : listProductTypeMes){
                productTypeIdPj = productTypeIdPj + "," + productTypeMes.getId(); //产品类型主键
            }
            if(!productTypeIdPj.equals("")){
                productTypeIdPj = productTypeIdPj.substring(1);
            }
            /*********** 产品类型循环End ***********/
        }
        modelMap.addAttribute("listProductTypeMes",listProductTypeMes); //产品类型集合
        modelMap.addAttribute("productTypeIdPj",productTypeIdPj); //产品类型拼接
        modelMap.addAttribute("selectMutli",selectMutli); //是否多选(0-多选、1-单选)
        /************************** 产品类型部分End **************************/

        /************************* 产品总额部分Start *************************/
        ProductMessageBean productMessageBean = new ProductMessageBean();
        productMessageBean = productService.getTradePriceSum(productMessageBean);
        if(productMessageBean != null){
            loadFeeSum = productMessageBean.getLoadFeeSum(); //交易总额
            interestSum = productMessageBean.getInterestSum(); //利息总额
            modelMap.addAttribute("loadFeeSum",loadFeeSum);
            modelMap.addAttribute("interestSum",interestSum);
        }
        /************************** 产品总额部分End **************************/

        /************************* 产品部分Start *************************/
        String pageIndex = productMes.getPageIndex(); //当前数量
        String pageSize = productMes.getPageSize(); //每页显示数量
        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            productMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "3";
            productMes.setPageSize(pageSize);
        }

        //产品集合
        List<ProductMessageBean> listProductMes = productService.getFrontList("UPDATETIME","DESC",productMes);
        //产品数量
        int totalCount = productService.queryProductCount(productMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageInvest","goInvestPage"); //分页部分拼接
        modelMap.addAttribute("listProductMes",listProductMes); //产品列表
        modelMap.addAttribute("pageStr",pageStr); //分页部分拼接
        /************************** 产品部分End **************************/

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 根据条件查询需投资产品
     * @param productMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryInvestBySome", method = RequestMethod.POST)
    @ResponseBody
    public ProductMessageBean queryInvestBySome(@ModelAttribute ProductMessageBean productMes, ModelMap modelMap){

        String productStr = ""; //产品列表部分拼接
        String repaymentName = ""; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        String fullScale = ""; //是否满标(0-是、1-否)
        String str = ""; //用于判断是立即投标还是已抢完
        String pageStr = ""; //分页部分

        /************************* 产品部分Start *************************/
        String pageIndex = productMes.getPageIndex(); //当前数量
        String pageSize = productMes.getPageSize(); //每页显示数量
        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            productMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "3";
            productMes.setPageSize(pageSize);
        }

        //产品集合
        List<ProductMessageBean> listProductMes = productService.getFrontList("UPDATETIME","DESC",productMes);
        //产品数量
        int totalCount = productService.queryProductCount(productMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageInvest","goInvestPage"); //分页部分拼接
        if(listProductMes != null && listProductMes.size() > 0){
            for(ProductMessageBean productMessageBean : listProductMes){

                /***** 还款方式Start *****/
                if(productMessageBean.getRepayment().equals("0")){
                    repaymentName = "按月还款、等额本息";
                }else if(productMessageBean.getRepayment().equals("1")){
                    repaymentName = "按月付息、到期还本";
                }else if(productMessageBean.getRepayment().equals("2")){
                    repaymentName = "一次性还款";
                }
                /****** 还款方式End ******/

                fullScale = productMessageBean.getFullScale(); //是否满标(0-是、1-否)
                productStr = productStr + "<tr>"
                        + "<td>"
                        + "<a href=\"#\">"+productMessageBean.getTitle()+"</a>"
                        + "</td>"
                        + "<td>"+productMessageBean.getLoadFee()+"元</td>"
                        + "<td>"+productMessageBean.getLoanrate()+"%</td>"
                        + "<td>"+productMessageBean.getLoadLength()+"</td>"
                        + "<td>"+repaymentName+"</td>"
                        + "<td>"
                        + "<span class=\"jindutiao\">"
                        + "<span class=\"fill\" style=\"width:"+productMessageBean.getBidProgress()+"%\"></span>"
                        + "</span>"
                        + "<span class=\"j_txt\">"+productMessageBean.getBidProgress()+"%</span>"
                        + "</td>"
                        + "<td>";

                if(fullScale != null && !fullScale.equals("")){
                    if(fullScale.equals("0")){ //满标
                        str = "<input type=\"button\" value=\"已抢完\" class=\"liji2\" onclick=\"queryProductInvestById('"+productMessageBean.getId()+"')\"/>";
                    }else if(fullScale.equals("1")){ //未满标
                        str = "<input type=\"button\" value=\"立即抢标\" class=\"liji\" onclick=\"queryProductInvestById('"+productMessageBean.getId()+"')\"/>";
                    }
                }else{
                    str = "";
                }

                productStr = productStr + str
                                        + "<span class=\"t_table_date\">发标日期："+productMessageBean.getUpdatetime()+"</span>"
                                        + "</td>"
                                        + "</tr>";

            }
        }
        /************************** 产品部分End **************************/

        productMes.setInfo(productStr); //产品信息
        productMes.setPageStr(pageStr); //分页部分拼接
        return productMes;
    }


    /**
     * 查询产品投资记录(By zhangerxin)
     * @param productInvestMes
     * @return
     */
    @RequestMapping(value = "/queryInvestRecord", method = RequestMethod.POST)
    @ResponseBody
    public ProductInvestMessageBean queryInvestRecord(@ModelAttribute ProductInvestMessageBean productInvestMes){

        String pageIndex = productInvestMes.getPageIndex(); //当前数量
        String pageSize = productInvestMes.getPageSize(); //每页显示数量
        String productInvestStr = ""; //投资部分拼接
        String pageStr = ""; //分页部分拼接

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            productInvestMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            productInvestMes.setPageSize(pageSize);
        }

        /******************** 产品投资集合Start ********************/
        List<ProductInvestMessageBean> listProductInvest = new ArrayList<ProductInvestMessageBean>();
        listProductInvest = productBuyService.getProductInvestList(productInvestMes);
        if(listProductInvest != null && listProductInvest.size() > 0){
            for(ProductInvestMessageBean productInvestMessageBean : listProductInvest){
                productInvestStr = productInvestStr + "<tr>"
                                 + "<td>"+productInvestMessageBean.getBuyerName()+"</td>"
                                 + "<td>"+productInvestMessageBean.getPrice()+"元</td>"
                                 + "<td>"+productInvestMessageBean.getBuyTime()+"</td>"
                                 + "</tr>";
            }
            productInvestMes.setStr(productInvestStr); //产品投资部分拼接
        }
        /********************* 产品投资集合End *********************/

        //产品投资数量
        int totalCount = productBuyService.getProductInvestCount(productInvestMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageInvestRecord","goInvestRecordPage"); //投资部分分页
        productInvestMes.setStr2(pageStr); //投资记录分页部分

        /******************** 产品投资金额部分Start ********************/
        ProductMessageBean productMessageBean = new ProductMessageBean(); //产品部分
        productMessageBean.setId(productInvestMes.getProductId()); //产品主键
        productMessageBean = productService.getTradePriceSum(productMessageBean);
        productInvestMes.setyInvestPrice(productMessageBean.getBiddingFee()); //已投标金额
        productInvestMes.setwInvestPrice(productMessageBean.getWbiddingFee()); //未投标金额
        /********************* 产品投资金额部分End *********************/
        return productInvestMes;
    }


    /**
     * 我要投标部分页面跳转
     * @param productMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryInvestProductInfo", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryInvestProductInfoByPage(@ModelAttribute ProductMessageBean productMes,ModelMap modelMap){
        //从session中获取session信息
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        String productId = productMes.getId(); //产品id
        String balance = "0"; //余额
        String memberId = ""; //会员id
        String ipsAcctNo = ""; //冻结方账号
        String projectNo = ""; //项目ID号
        String webUrl = "http://jianbing88.com/front/registerhxWebResult"; //页面返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/investhxResult"; //后台通知地址

        /******************** 登录会员部分Start ********************/
        if(memberInfo != null){
            memberId = memberInfo.getId(); //会员id
            HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean(); //环迅注册部分
            huanXunRegistMessageBean.setMemberId(memberId);
            //环迅注册信息
            List<HuanXunRegistMessageBean> listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
            if (listHuanXunRegistMes != null && listHuanXunRegistMes.size() > 0){
                ipsAcctNo = listHuanXunRegistMes.get(0).getIpsAcctNo(); //冻结方账号
                balance = listHuanXunRegistMes.get(0).getBalance(); //余额
            }
            webUrl = webUrl + "?memberId="+memberId; //页面返回地址
            s2SUrl = s2SUrl + "?productId="+productId+"&memberId="+memberId+"&freezeType=1"; //后台通知地址
            modelMap.addAttribute("webUrl",webUrl);
            modelMap.addAttribute("s2SUrl",s2SUrl);
        }
        modelMap.addAttribute("ipsAcctNo",ipsAcctNo); //冻结方账号
        modelMap.addAttribute("memberId",memberId); //会员id
        modelMap.addAttribute("balance",balance); //余额
        /********************* 登录会员部分End *********************/

        /******************** 项目登记部分Start ********************/
        if(productId != null && !productId.equals("")){
            ProductHuanXunMessageBean productHuanXunMessageBean = new ProductHuanXunMessageBean();
            productHuanXunMessageBean.setId(productId); //产品id
            List<ProductHuanXunMessageBean> listProductHuanXunMes = huanXunRegProjectService.getRegProjectInfoList(productHuanXunMessageBean);
            if (listProductHuanXunMes != null && listProductHuanXunMes.size() > 0){
                projectNo = listProductHuanXunMes.get(0).getProjectNo(); //项目id号
                modelMap.addAttribute("projectNo",projectNo);
            }
        }
        /********************* 项目登记部分End *********************/

        /************************* 产品部分Start *************************/
        InvestProductMessageBean investProductMes = productService.queryInvestProductById(productMes); //投资产品
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean(); //会员部分
        if(investProductMes != null){
            memberInfoMessageBean = investProductMes.getMemberInfoMessageBean(); //人员信息
            modelMap.addAttribute("memberInfoMes",memberInfoMessageBean); //会员信息
        }
        modelMap.addAttribute("investProductMes",investProductMes); //投资产品信息
        List<SysFileMessageBean> sysFileMessageBeanList = sysFileService.getAllFile(productId);
        modelMap.addAttribute("sysFileMessageBeanList",sysFileMessageBeanList); //产品附件信息
        /************************** 产品部分End **************************/

        JetbrickBean bean = new JetbrickBean();

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 立即投标(新增购买产品)
     * @param productInvestMes
     */
    @RequestMapping(value = "/addInvest", method = RequestMethod.POST)
    @ResponseBody
    public ProductInvestMessageBean onUpdateProduct(@ModelAttribute ProductInvestMessageBean productInvestMes,HttpServletRequest request){
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        String id = UUID.randomUUID().toString().replaceAll("\\-", ""); //主键
        String productId = productInvestMes.getProductId(); //产品id
        String traderpassword = productInvestMes.getTraderpassword(); //交易密码
        String captcha = productInvestMes.getCaptcha(); //验证码
        String createTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //创建时间
        String fullScale = "1"; //判断是否满标(0-是、1-否)
        String applyMemberId = ""; //申请人id
        String bidFee = ""; //最低投标金额(String型)
        Double bidFeeDou = 0.0d; //最低投标金额(Double型)
        String balance = ""; //余额(String型)
        Double balanceDou = 0.0d; //余额(Double型)
        String buyPrice = ""; //购买金额(String型)
        Double buyPriceDou = 0.0d; //购买金额(Double型)
        MemberInfo memberInfoInvest = new MemberInfo();
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean(); //会员部分
        ProductMessageBean productMessageBean = new ProductMessageBean(); //产品部分

        if (memberInfo == null) { //该会员未登录
            productInvestMes.setInfo("会话已过期请重新登陆");
            productInvestMes.setResult("error");
        }else{ //该会员已登录
            memberId = memberInfo.getId(); //会员id
            productMessageBean.setId(productId); //产品id
            productMessageBean = productService.queryProductById(productMessageBean); //根据id查询产品
            buyPrice = productInvestMes.getPrice(); //购买金额(String型)
            if(buyPrice != null && !buyPrice.equals("")){
                buyPriceDou = TypeTool.getDouble(buyPrice); //购买金额(Double型)
            }
            if(productMessageBean != null){
                fullScale = productMessageBean.getFullScale(); //是否满标(0-是、1-否)
                applyMemberId = productMessageBean.getApplyMember(); //申请人id
                bidFee = productMessageBean.getBidfee(); //最低投标金额
                if(bidFee != null && !bidFee.equals("")){
                    bidFeeDou = TypeTool.getDouble(bidFee);
                }
                if(memberId.equals(applyMemberId)){
                    productInvestMes.setInfo("该产品由您发起，不允许对自己发起的产品进行投资！");
                    productInvestMes.setResult("error");
                }else{
                    if(fullScale != null && !fullScale.equals("")){
                        if(fullScale.equals("0")){ //满标
                            productInvestMes.setResult("error");
                            productInvestMes.setInfo("该产品已满标，不能进行投资!");
                        }else{
                            memberId = memberInfo.getId(); //会员id
                            memberInfoMessageBean.setId(memberId);
                            productInvestMes.setBuyer(memberId); //购买人
                            int count = productBuyService.getCountBySome(productInvestMes); //购买数量(用于判断当前登录人是否已购买)
                            if(count > 0){ //已购买
                                productInvestMes.setResult("error");
                                productInvestMes.setInfo("您已投资该产品，不能重复投资！");
                            }else{
                                memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean); //获取会员信息
                                if(memberInfoMessageBean != null){
                                    balance = memberInfoMessageBean.getBalance(); //余额(String型)
                                    if(balance != null && !balance.equals("")) {
                                        balanceDou = TypeTool.getDouble(balance); //余额(Double型)
                                        if(balanceDou < bidFeeDou){ //余额不足
                                            productInvestMes.setResult("error");
                                            productInvestMes.setInfo("余额不足，请先进行充值操作!");
                                        }else {
                                            if (bidFeeDou > buyPriceDou) {
                                                productInvestMes.setResult("error");
                                                productInvestMes.setInfo("该产品最低投标金额为" + bidFeeDou + "元!");
                                            } else {
                                                if (traderpassword != null && !traderpassword.equals("")) { //交易密码
                                                    if (traderpassword.equals(memberInfoMessageBean.getTraderpassword())) { //交易密码相同
                                                        /********** 验证码部分Start **********/
                                                        HttpSession session = request.getSession();
                                                        if (captcha != null && !captcha.equals("")) { //验证码
                                                            if (!captcha.equals(session.getAttribute("certCode"))) { //验证码不符
                                                                productInvestMes.setResult("error");
                                                                productInvestMes.setInfo("验证码错误！");
                                                            } else {

                                                                /********** 新增投资Start **********/
                                                                productInvestMes.setId(id); //主键
                                                                productInvestMes.setBuyTime(createTime); //购买时间
                                                                productInvestMes.setBuyer(memberId); //会员id
                                                                productInvestMes.setType("0"); //类型 0-持有 1-转让中 2-已转让
                                                                productInvestMes.setCreateTime(createTime); //创建时间
                                                                ProductInvestBusinessBean productInvestBus = productBuyService.saveProductInvest(productInvestMes);
                                                                productInvestMes.setResult(productInvestBus.getResult());
                                                                productInvestMes.setInfo(productInvestBus.getInfo());
                                                                /*********** 新增投资End ***********/

                                                                /********** 更新账户余额Start **********/
                                                                balanceDou = balanceDou - buyPriceDou; //余额
                                                                memberInfoMessageBean.setBalance(TypeTool.getString(balanceDou));
                                                                memberInfoMessageBean = allMemberService.updateMemberInfo(memberInfoMessageBean);
                                                                /*********** 更新账户余额End ***********/
                                                            }
                                                        }
                                                        /*********** 验证码部分End ***********/
                                                    } else {
                                                        productInvestMes.setResult("error");
                                                        productInvestMes.setInfo("交易密码错误！");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return productInvestMes;
    }


    /**
     * 打开我的投资(默认查询回收中的项目)
     * @param productInvestMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openMyInvest", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openMyInvest(@ModelAttribute ProductInvestMessageBean productInvestMes,ModelMap modelMap){
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        String pageStr = ""; //分页部分拼接
        String pageIndex = productInvestMes.getPageIndex(); //当前数
        String pageSize = productInvestMes.getPageSize(); //每次显示数量

        if (memberInfo != null){
            memberId = memberInfo.getId();
        }
        if (pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            productInvestMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "5";
            productInvestMes.setPageSize(pageSize);
        }

        /************************* 产品类型Start *************************/
        String typeIdStr = ""; //类型名称
        ProductTypeMessage productTypeMessage = new ProductTypeMessage(); //产品类型
        List<ProductTypeMessage> listProductTypes = productTypeService.queryAllProductType(productTypeMessage); //产品类型
        modelMap.put("listProductTypes",listProductTypes);
        if(listProductTypes != null && listProductTypes.size() > 0){
            for(ProductTypeMessage productTypeMes : listProductTypes){
                if(typeIdStr.equals("")){
                    typeIdStr = productTypeMes.getId(); //类型id
                }else{
                    typeIdStr = typeIdStr + "," + productTypeMes.getId();
                }
            }
        }
        modelMap.put("typeIdStr",typeIdStr); //类型id拼接
        /************************** 产品类型End **************************/

        /************************* 投标中的项目Start *************************/
        productInvestMes.setInvestFlag("0"); //投资状态(0-回收中、1-投标中、2-已结清)
        productInvestMes.setBuyer(memberId); //投资人id
        //回收项目列表
        List<ProductInvestMessageBean> listProductInvestMes = productBuyService.getProductInvestList(productInvestMes);
        //回收项目数量
        int totalCount = productBuyService.getCountBySome(productInvestMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageInvestHs","goInvestHsPage"); //投资部分分页
        modelMap.put("listProductInvestMes",listProductInvestMes); //投资项目列表
        modelMap.put("pageStr",pageStr); //分页拼接部分
        /************************** 投标中的项目End **************************/

        JetbrickBean bean = new JetbrickBean();

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productInvestMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 获取回收中的项目
     * @param productInvestMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/getRecoverProduct", method = RequestMethod.POST)
    @ResponseBody
    public ProductInvestMessageBean getRecoverProduct(@ModelAttribute ProductInvestMessageBean productInvestMes,ModelMap modelMap){

        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        String pageIndex = productInvestMes.getPageIndex(); //当前页
        String pageSize = productInvestMes.getPageSize(); //每页显示数量
        String pageStr = ""; //分页部分拼接
        String str = ""; //投资过程的产品拼接

        if (pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            productInvestMes.setPageIndex(pageIndex);
            modelMap.put("pageIndex",pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "5";
            productInvestMes.setPageSize(pageSize);
            modelMap.put("pageSize",pageSize);
        }

        if(memberInfo != null){
            memberId = memberInfo.getId();
            productInvestMes.setInvestFlag("0"); //投资状态(0-回收中、1-投标中、2-已结清)
            productInvestMes.setBuyer(memberId); //投资人

            //投资的项目集合
            List<ProductInvestMessageBean> listProductBuys = productBuyService.getProductInvestList(productInvestMes);
            //回收项目数量
            int totalCount = productBuyService.getCountBySome(productInvestMes);
            FrontPage frontPage = new FrontPage();
            pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageInvestHs","goInvestHsPage"); //投资部分分页

            if(listProductBuys != null && listProductBuys.size() > 0){
                for(ProductInvestMessageBean productInvestMessageBean : listProductBuys){
                    str = str + "<tr class=\"t_txt\">"
                            + "<td>"+productInvestMessageBean.getTitle()+"</td>"
                            + "<td>"+productInvestMessageBean.getCode()+"</td>"
                            + "<td>"+productInvestMessageBean.getBuyTime()+"</td>"
                            + "<td>"+productInvestMessageBean.getLoadLength()+"</td>"
                            + "<td>"+productInvestMessageBean.getLoanrate()+"%</td>"
                            + "<td>"+productInvestMessageBean.getLoadFee()+"</td>"
                            + "<td>"+productInvestMessageBean.getShouldPrice()+"</td>"
                            + "<td>"+productInvestMessageBean.getYsPrice()+"</td>"
                            + "<td>"+productInvestMessageBean.getWaitPrice()+"</td>"
                            + "<td>"+productInvestMessageBean.getNextReceiveTime()+"</td>"
                            + "<td class=\"caozuo\">"
                            + "<a href=\"JavaScript:void(0)\" onclick=\"doQueryInvestProductDetails('"+productInvestMessageBean.getId()+"','"+productInvestMessageBean.getProductId()+"','')\">"
                            + "详情"
                            + "</a>"
                            + "</td>"
                            + "</tr>";
                }
            }

            productInvestMes.setStr(str); //列表部分拼接
            productInvestMes.setStr2(pageStr); //分页部分
        }

        return productInvestMes;
    }


    /**
     * 获取投标中的项目
     * @param productInvestMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/getProcessProduct", method = RequestMethod.POST)
    @ResponseBody
    public ProductInvestMessageBean getProcessProduct(@ModelAttribute ProductInvestMessageBean productInvestMes,ModelMap modelMap){
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        String str = ""; //投资过程的产品拼接
        String repayment = ""; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        String repaymentName = ""; //还款方式名称
        String pageIndex = productInvestMes.getPageIndex(); //当前数
        String pageSize = productInvestMes.getPageSize(); //每页显示数量
        String pageStr = ""; //分页部分拼接

        if(memberInfo != null){
            memberId = memberInfo.getId(); //会员id
            productInvestMes.setBuyer(memberId); //投资人id
            productInvestMes.setInvestFlag("1"); //投资状态(0-回收中、1-投标中、2-已结清)
            if (pageIndex == null || pageIndex.equals("")){ //当前数
                pageIndex = "1";
                modelMap.put("pageIndex",pageIndex);
            }
            if (pageSize == null || pageSize.equals("")){ //每次显示数量
                pageSize = "5";
                modelMap.put("pageSize",pageSize);
            }
            //投资的项目集合
            List<ProductInvestMessageBean> listProductBuys = productBuyService.getProductInvestList(productInvestMes);
            //投资中的项目数量
            int totalCount = productBuyService.getCountBySome(productInvestMes);

            FrontPage frontPage = new FrontPage();
            pageStr = frontPage.getPageInfo(pageIndex, pageSize, totalCount, "pageInvestTb", "goInvestTbPage"); //投资部分分页
            System.out.println("pageStr======"+pageStr);

            if(listProductBuys != null && listProductBuys.size() > 0){
                for(ProductInvestMessageBean productInvestMessageBean : listProductBuys){
                    repayment = productInvestMessageBean.getRepayment();
                    if(repayment != null && !repayment.equals("")){
                        if(repayment.equals("0")){ //按月还款
                            repaymentName = "按月还款、等额本息";
                        }else if(repayment.equals("1")){
                            repaymentName = "按月付息、到期还本";
                        }else if(repayment.equals("2")){
                            repaymentName = "一次性还款";
                        }
                    }
                    str = str + "<tr class=\"t_txt\">"
                            + "<td>"+productInvestMessageBean.getTitle()+"</td>"
                            + "<td>"+productInvestMessageBean.getCode()+"</td>"
                            + "<td>"+productInvestMessageBean.getBuyTime()+"</td>"
                            + "<td>"+productInvestMessageBean.getLoadLength()+"</td>"
                            + "<td>"+productInvestMessageBean.getLoanrate()+"%</td>"
                            + "<td>"+productInvestMessageBean.getPrice()+"元</td>"
                            + "<td>"+repaymentName+"</td>"
                            + "<td>"
                            + "<span class=\"jdt_num\">"+productInvestMessageBean.getInvestProgress()+"%</span>"
                            + "</td>"
                            + "</tr>";
                }
            }
            productInvestMes.setStr(str); //列表部分拼接
            productInvestMes.setStr2(pageStr); //分页部分拼接
        }

        return productInvestMes;
    }


    /**
     * 获取结清的项目
     * @param productInvestMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/getSquareProduct", method = RequestMethod.POST)
    @ResponseBody
    public ProductInvestMessageBean getSquareProduct(@ModelAttribute ProductInvestMessageBean productInvestMes,ModelMap modelMap){
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        String str = ""; //投资过程的产品拼接
        String squareDate = ""; //结清日期
        String pageStr = ""; //分页部分拼接
        String pageIndex = productInvestMes.getPageIndex(); //当前数
        String pageSize = productInvestMes.getPageSize(); //每页显示数量

        if (pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            productInvestMes.setPageIndex(pageIndex);
            modelMap.put("pageIndex",pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "5";
            productInvestMes.setPageSize(pageSize);
            modelMap.put("pageSize",pageSize);
        }

        if(memberInfo != null){
            memberId = memberInfo.getId(); //投资人id
            productInvestMes.setInvestFlag("2"); //投资状态(0-回收中、1-投标中、2-已结清)
            //投资的项目集合
            List<ProductInvestMessageBean> listProductBuys = productBuyService.getProductInvestList(productInvestMes);
            //投资的项目数量
            int totalCount = productBuyService.getProductInvestCount(productInvestMes);

            FrontPage frontPage = new FrontPage();
            pageStr = frontPage.getPageInfo(pageIndex, pageSize, totalCount, "pageInvestJq", "goInvestJqPage"); //投资部分分页

            if(listProductBuys != null && listProductBuys.size() > 0){
                for(ProductInvestMessageBean productInvestMessageBean : listProductBuys){
                    squareDate = productInvestMessageBean.getSquareDate(); //结清日期
                    if (squareDate != null && !squareDate.equals("")){
                        squareDate = squareDate.substring(0,10); //结清日期
                    }
                    str = str + "<tr class=\"t_txt\">"
                            + "<td>"+productInvestMessageBean.getTitle()+"</td>"
                            + "<td>"+productInvestMessageBean.getCode()+"</td>"
                            + "<td>"+productInvestMessageBean.getBuyTime()+"</td>"
                            + "<td>"+productInvestMessageBean.getLoadLength()+"</td>"
                            + "<td>"+productInvestMessageBean.getLoanrate()+"%</td>"
                            + "<td>"+productInvestMessageBean.getPrice()+"元</td>"
                            + "<td>"+squareDate+"</td>"
                            + "<td>"+productInvestMessageBean.getYsPrice()+"元</td>"
                            + "<td class=\"caozuo\">"
                            + "<a href=\"JavaScript:void(0)\" onclick=\"doQueryInvestProductDetails('"+productInvestMessageBean.getId()+"','"+productInvestMessageBean.getProductId()+"','')\">"
                            + "详情"
                            + "</a>"
                            + "</td>"
                            + "</tr>";
                }
            }

            productInvestMes.setStr(str); //列表拼接
            productInvestMes.setStr2(pageStr); //分页部分拼接
        }

        return productInvestMes;
    }





    /**
     * 待回收统计(重做by zhangerxin)
     * @param productReceiveMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/receiveTjInfo", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean receiveTjInfo(@ModelAttribute ProductReceiveMessageBean productReceiveMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();

        /******************** 判断session信息是否存在Start ********************/
        HttpSession session = servletUtil.getSession();
        String memberId = ""; //会员id
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            bean.setResult("error");
            bean.setInfo("会话已过期,请重新登录");
            return bean;
        }
        memberId = memberInfo.getId(); //会员id
        /********************* 判断session信息是否存在End *********************/

        String pageIndex = productReceiveMes.getPageIndex(); //起始数
        String pageSize = productReceiveMes.getPageSize(); //每页显示数量
        String dReceivePriceSum = "0"; //待回收金额总和
        String dReceivePricipalSum = "0"; //待回收本金总和
        String dReceiveInterest = "0"; //待回收利息总和
        String dReceiveCount = "0"; //待回收期数
        int investCount = 0; //待回收投资数量

        if (pageIndex == null || pageIndex.equals("")) {
            pageIndex = "1"; //当前数量
            productReceiveMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "10"; //每页显示数量
            productReceiveMes.setPageSize(pageSize);
        }

        productReceiveMes.setBuyer(memberId); //投资人
        productReceiveMes.setIsRecycle("0"); //0-待回收、1-已回收
        ProductReceiveMessageBean productReceiveMessageBean = productBuyService.getReceivePriceSum(productReceiveMes); //待回收金额总和
        dReceivePriceSum = productReceiveMessageBean.getDsPriceTotal(); //待回收总额合计
        dReceivePricipalSum = productReceiveMessageBean.getDsPrincipalTotal(); //待回收本金合计
        dReceiveInterest = productReceiveMessageBean.getDsInterestTotal(); //待回收利息合计
        dReceiveCount = productBuyService.getReceiveCount(productReceiveMes); //待回收期数总和

        ProductInvestMessageBean productInvestMessageBean = new ProductInvestMessageBean();
        productInvestMessageBean.setBuyer(memberId); //投资人id
        productInvestMessageBean.setInvestFlag("0"); //投资状态(投资状态(0-回收中、1-投标中、2-已结清))
        investCount = productBuyService.getProductInvestCount(productInvestMessageBean); //待收投资数量

        modelMap.put("dReceivePriceSum",dReceivePriceSum); //待回收总额合计
        modelMap.put("dReceivePricipalSum",dReceivePricipalSum); //待回收本金合计
        modelMap.put("dReceiveInterest",dReceiveInterest); //待回收利息合计
        modelMap.put("dReceiveCount",dReceiveCount); //待回收期数总和
        modelMap.put("investCount",investCount); //待收投资数量

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productReceiveMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }



    /**
     * 获取投资产品详细信息(重做 by zhangerxin)
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryInvestProductDetails", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryInvestProductDetails(HttpServletRequest request,ModelMap modelMap) {
        String investId = request.getParameter("investId"); //投资id
        String productId = request.getParameter("productId"); //产品id
        String contractType = request.getParameter("contractType"); //合同类型(1-投资、2-债权)
        String yreceivePrice = ""; //已回款金额
        String wreceivePrice = ""; //未还款金额
        Double receiveProgress = 0.0d; //投资进度
        String shFlag = ""; //审核状态(0-审核通过、1-审核未通过、2-未审核)
        String investFlag = ""; //投资状态
        String pageIndex = request.getParameter("pageIndex"); //起始数
        String pageSize = request.getParameter("pageSize"); //每页显示数量
        String pageStr = ""; //分页拼接

        /******************** 产品信息Start ********************/
        ProductInfoMessageBean productInfoMessageBean = productBuyService.queryProductDetails(productId);
        modelMap.put("productInfoMessageBean",productInfoMessageBean);
        /********************* 产品信息End *********************/

        /******************** 已回款信息Start ********************/
        ProductReceiveInfoMessageBean productReceiveInfoYsMessageBean = productBuyService.queryYReceiveInfo(investId);
        modelMap.put("productReceiveInfoYsMessageBean",productReceiveInfoYsMessageBean);
        /********************* 已回款信息End *********************/

        /******************** 未回款信息Start ********************/
        ProductReceiveInfoMessageBean productReceiveInfoWsMessageBean = productBuyService.queryWReceiveInfo(investId);
        modelMap.put("productReceiveInfoWsMessageBean",productReceiveInfoWsMessageBean);
        /********************* 未回款信息End *********************/

        yreceivePrice = productReceiveInfoYsMessageBean.getYreceivementPrice(); //已回款金额部分
        wreceivePrice = productReceiveInfoWsMessageBean.getYreceivementPrice(); //未还款金额部分
        receiveProgress = TypeTool.getDouble(yreceivePrice)/(TypeTool.getDouble(yreceivePrice) + TypeTool.getDouble(wreceivePrice)) * 100;
        receiveProgress = PublicsTool.round(receiveProgress,2); //收款进度
        modelMap.put("receiveProgress",receiveProgress);

        if (pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "5";
        }

        //还款信息集合
        List<ProductReceiveInfoMessageBean> listProductReceiveInfoMes = receiveService.getReceiveInfoList(investId,"YRECEIVETIME","ASC",pageIndex,pageSize);
        //还款信息数量
        int totalCount = receiveService.getReceiveInfoCount(investId);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo2(pageIndex, pageSize, totalCount, "pageReceive", "goReceivePage", investId); //还款信息部分分页
        modelMap.put("pageStr",pageStr); //还款部分分页
        modelMap.put("listProductReceiveInfoMes",listProductReceiveInfoMes); //还款信息集合

        shFlag = productInfoMessageBean.getShFlag(); //审核状态(0-审核通过、1-审核未通过、2-未审核)
        if (shFlag != null && !shFlag.equals("")){
            if (shFlag.equals("2")){ //待审核
                investFlag = "投标中";
            }else if (shFlag.equals("0")){ //审核通过
                if (wreceivePrice != null && !wreceivePrice.equals("")){ //未回款金额
                    if (wreceivePrice.equals("0")){
                        investFlag = "已结清";
                    }else{
                        investFlag = "回收中";
                    }
                }
            }
        }
        modelMap.put("investFlag",investFlag); //投资状态
        modelMap.put("investId",investId); //投资id
        modelMap.put("productId",productId); //产品id
        modelMap.put("contractType",contractType); //合同类型(1-投资、2-债权)

        JetbrickBean bean = new JetbrickBean();
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/invest/investProduct_details.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 根据条件获取投资产品详细信息中的回收列表(重做 by zhangerxin)
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/doQueryInvestProductDetailsByPage", method = RequestMethod.POST)
    @ResponseBody
    public void doQueryInvestProductDetailsByPage(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap) {
        String investId = request.getParameter("investId"); //投资id
        String pageIndex = request.getParameter("pageIndex"); //起始数
        String pageSize = request.getParameter("pageSize"); //每页显示数量
        String str = ""; //列表部分
        String pageStr = ""; //分页拼接

        if (pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "5";
        }

        //还款信息集合
        List<ProductReceiveInfoMessageBean> listProductReceiveInfoMes = receiveService.getReceiveInfoList(investId,"YRECEIVETIME","ASC",pageIndex,pageSize);
        //还款信息数量
        int totalCount = receiveService.getReceiveInfoCount(investId);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo2(pageIndex, pageSize, totalCount, "pageReceive", "goReceivePage", investId); //还款信息部分分页

        if (listProductReceiveInfoMes != null && listProductReceiveInfoMes.size() > 0){
            for (ProductReceiveInfoMessageBean productReceiveInfoMessageBean : listProductReceiveInfoMes){
                str = str + "<tr class=\"bj_tr\" >"
                          + "<td>"+productReceiveInfoMessageBean.getyReceiveTime()+"</td>"
                          + "<td>"+productReceiveInfoMessageBean.getSreceivementTime()+"</td>"
                          + "<td>￥"+productReceiveInfoMessageBean.getYreceivementPrice()+"元</td>"
                          + "<td>￥"+productReceiveInfoMessageBean.getReceivementPrice()+"元</td>"
                          + "<td>"+productReceiveInfoMessageBean.getReceiveFlag()+"</td>"
                          + "</tr>";
            }
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("str", str); //列表部分拼接
        in.put("pageStr", pageStr); //分页部分拼接
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 待回收明细(重做 by zhangerxin)
     * @param productReceiveMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/querydReceiveList", method = RequestMethod.POST)
    @ResponseBody
    public ProductReceiveMessageBean querydReceiveList(@ModelAttribute ProductReceiveMessageBean productReceiveMes,ModelMap modelMap) {
        /******************** 判断session信息是否存在Start ********************/
        HttpSession session = servletUtil.getSession();
        String memberId = ""; //会员id
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            productReceiveMes.setResult("error");
            productReceiveMes.setInfo("会话已过期,请重新登录");
            return productReceiveMes;
        }
        memberId = memberInfo.getId(); //会员id
        productReceiveMes.setBuyer(memberId);
        /********************* 判断session信息是否存在End *********************/

        String str = ""; //列表部分拼接
        String pageStr = ""; //分页部分拼接
        String pageIndex = productReceiveMes.getPageIndex(); //起始数
        String pageSize = productReceiveMes.getPageSize(); //每页显示数量
        String orderColumn = productReceiveMes.getOrderColumn(); //排序字段
        String orderType = productReceiveMes.getOrderType(); //排序类型

        if(pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
            productReceiveMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            productReceiveMes.setPageSize(pageSize);
        }
        if (orderColumn == null || orderColumn.equals("")){ //排序字段
            orderColumn = "YRECEIVETIME";
            productReceiveMes.setOrderColumn(orderColumn);
        }
        if (orderType == null || orderType.equals("")){ //排序类型
            orderType = "ASC";
            productReceiveMes.setOrderType(orderType);
        }

        //回款信息集合
        List<ProductReceiveMessageBean> listProductReceiveMes = receiveService.getProductReceiveList(productReceiveMes);
        //回款信息数量
        int totalCount = receiveService.getProductReceiveListCount(productReceiveMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex, pageSize, totalCount, "pageyReceive", "goyReceivePage"); //待回收信息分页

        if (listProductReceiveMes != null && listProductReceiveMes.size() > 0){
            for (ProductReceiveMessageBean productReceiveMessageBean : listProductReceiveMes){
                str = str + "<tr class=\"t_txt\">"
                          + "<td>"+productReceiveMessageBean.getTitle()+"</td>"
                          + "<td>"+productReceiveMessageBean.getYreceivetime()+"</td>"
                          + "<td>"+productReceiveMessageBean.getSumPrice()+"元</td>"
                          + "<td>"+productReceiveMessageBean.getPrincipal()+"元</td>"
                          + "<td>"+productReceiveMessageBean.getInterest()+"元</td>"
                          + "<td>"+productReceiveMessageBean.getOverduedays()+"天</td>"
                          + "</tr>";
            }
        }

        productReceiveMes.setStr(str); //列表部分拼接
        productReceiveMes.setStr2(pageStr); //分页部分拼接
        return productReceiveMes;
    }


    /**
     * 查询已回收明细
     * @param productReceiveMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryyReceiveList", method = RequestMethod.POST)
    @ResponseBody
    public ProductReceiveMessageBean queryyReceiveList(@ModelAttribute ProductReceiveMessageBean productReceiveMes,ModelMap modelMap) {
        /******************** 判断session信息是否存在Start ********************/
        HttpSession session = servletUtil.getSession();
        String memberId = ""; //会员id
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            productReceiveMes.setResult("error");
            productReceiveMes.setInfo("会话已过期,请重新登录");
            return productReceiveMes;
        }
        memberId = memberInfo.getId(); //会员id
        productReceiveMes.setBuyer(memberId);
        /********************* 判断session信息是否存在End *********************/

        String str = ""; //列表部分拼接
        String pageStr = ""; //分页部分拼接
        String pageIndex = productReceiveMes.getPageIndex(); //起始数
        String pageSize = productReceiveMes.getPageSize(); //每页显示数量
        String orderColumn = productReceiveMes.getOrderColumn(); //排序字段
        String orderType = productReceiveMes.getOrderType(); //排序类型

        if(pageIndex == null || pageIndex.equals("")){ //起始数
            pageIndex = "1";
            productReceiveMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            productReceiveMes.setPageSize(pageSize);
        }
        if (orderColumn == null || orderColumn.equals("")){ //排序字段
            orderColumn = "YRECEIVETIME";
            productReceiveMes.setOrderColumn(orderColumn);
        }
        if (orderType == null || orderType.equals("")){ //排序类型
            orderType = "ASC";
            productReceiveMes.setOrderType(orderType);
        }

        //回款信息集合
        List<ProductReceiveMessageBean> listProductReceiveMes = receiveService.getProductReceiveList(productReceiveMes);
        //回款信息数量
        int totalCount = receiveService.getProductReceiveListCount(productReceiveMes);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo(pageIndex, pageSize, totalCount, "pageyReceive", "goyReceivePage"); //已回收信息分页

        if (listProductReceiveMes != null && listProductReceiveMes.size() > 0){
            for (ProductReceiveMessageBean productReceiveMessageBean : listProductReceiveMes){
                str = str + "<tr class=\"t_txt\">"
                          + "<td>"+productReceiveMessageBean.getSreceivetime()+"</td>"
                          + "<td>"+productReceiveMessageBean.getTitle()+"</td>"
                          + "<td>"+productReceiveMessageBean.getCode()+"</td>"
                          + "<td>"+productReceiveMessageBean.getReceiveprice()+"元</td>"
                          + "<td>"+productReceiveMessageBean.getManagePrice()+"元</td>"
                          + "</tr>";
            }
        }

        productReceiveMes.setStr(str); //列表部分拼接
        productReceiveMes.setStr2(pageStr); //分页部分拼接
        return productReceiveMes;
    }


    /**
     * 跳转到项目详情(待回收统计) by cuibin
     */
    @RequestMapping(value = "/queryOnlyProjectDetails", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryOnlyProjectDetails(@ModelAttribute ProductMessageBean productMessageBean,ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            bean.setResult("error");
            bean.setInfo("会话已过期,请重新登录");
            return bean;
        }

        /************************* 项目基本信息Start *************************/
        //产品部分
        ProductMessageBean productMes = productService.queryProductById(productMessageBean);
        //会员部分
        MemberInfoMessageBean memberInfoMes = productMes.getMemberInfoMessageBean();
        modelMap.put("productMessageBean",productMes);
        modelMap.put("memberInfoMes",memberInfoMes);
        /************************* 项目基本信息End *************************/

        /************************* 期数列表Start *************************/
        String pageSize = productMessageBean.getPageSize();
        String pageIndex = productMessageBean.getPageIndex();
        //String currentPage = productMessageBean.getCurrentPage(); //当前页
        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            //currentPage = "1"; //当前页
            productMessageBean.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "5"; //每页显示数量
            productMessageBean.setPageSize(pageSize);
        }

        List<ProductReceiveMessageBean> list = productReceiveService.phaseDetails(productMessageBean,memberInfo.getId());
        //收款进度
        String jd = list.get(0).getReceiveflag();
        modelMap.put("prmbList",list);
        modelMap.put("jd",jd);

        /************************* 期数列表End *************************/

        /************************* 已回收详情 笔 Start *************************/
        ProductReceiveMessageBean productReceiveMessageBean = new ProductReceiveMessageBean();
        ProductReceiveMessageBean productReceiveMessageBean1 = productReceiveService.alreadyReceive(productReceiveMessageBean,memberInfo.getId());
        modelMap.put("productReceiveMessageBean",productReceiveMessageBean1);
        /************************* 已回收详情 End *************************/

        /************************* 未回收详情 笔 Start *************************/
        ProductReceiveMessageBean productReceiveMes = new ProductReceiveMessageBean();
        ProductReceiveMessageBean productReceiveMes1 = productReceiveService.notReceive(productReceiveMes,memberInfo.getId());
        modelMap.put("productReceiveMes",productReceiveMes1);
        /************************* 未回收详情 End *************************/

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/invest/invest_details.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 打开投资统计
     */
    @RequestMapping(value = "/openinvestTj", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openinvestTj(@ModelAttribute ProductMessageBean productMessageBean,ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = (MemberInfo) session.getAttribute("sessionMemberInfo");
        if (memberInfo == null) {
            bean.setInfo("会话已过期 请重新登陆");
            bean.setResult("error");
            return bean;
        }
        InvestTotalMessageBean investTotalMessageBean = new InvestTotalMessageBean();

        /************************* 投标中本金 *************************/
        String inaBid = productReceiveService.inaBid(memberInfo.getId());
        /************************* 投标中本金 *************************/

        /************************* 待回收本金 *************************/
        String toBeRecycled = productReceiveService.toBeRecycled(memberInfo.getId());
        /************************* 待回收本金 *************************/

        /************************* 已回收的本金 *************************/
        String retired = productReceiveService.retired(memberInfo.getId());
        /************************* 已回收的本金 *************************/

        /************************* 投资总额 *************************/
        //String investTotal = productReceiveService.investTotal(memberInfo.getId());
        String investTotal = (Double.parseDouble(toBeRecycled) + Double.parseDouble(retired)) + "";
        /************************* 投资总额 *************************/

        /************************* 计算百分比 *************************/
        double retiredDou = Double.parseDouble(retired); //已回收的本金
        double inaBidDou = Double.parseDouble(inaBid);//投标中本金
        double toBeRecycledDou = Double.parseDouble(toBeRecycled);//待回收本金
        double sum = retiredDou + inaBidDou + toBeRecycledDou;
        if (sum != 0) {
            PublicsTool publicsTool = new PublicsTool();
            investTotalMessageBean.setRetiredPercent((publicsTool.round(retiredDou / sum, 2) * 100) + "");
            investTotalMessageBean.setInaBidPercent((publicsTool.round(inaBidDou / sum, 2) * 100) + "");
            investTotalMessageBean.setToBeRecycledPercent((publicsTool.round(toBeRecycledDou / sum, 2) * 100) + "");
        } else {
            investTotalMessageBean.setRetiredPercent("0");
            investTotalMessageBean.setInaBidPercent("0");
            investTotalMessageBean.setToBeRecycledPercent("0");
        }

        /************************* 计算百分比 *************************/

        /************************* 待回收金额 *************************/
        String[] arr = productReceiveService.moneyRecycled(memberInfo.getId());
        /************************* 待回收金额 *************************/

        /************************* 平均受益率 *************************/
        String avgBenefit = productReceiveService.avgBenefit(memberInfo.getId());
        /************************* 平均受益率 *************************/

        investTotalMessageBean.setInvestTotal(investTotal);
        investTotalMessageBean.setInaBid(inaBid);
        investTotalMessageBean.setToBeRecycled(toBeRecycled);
        investTotalMessageBean.setRetired(retired);
        investTotalMessageBean.setPrincipalRecycled(arr[0]);//待回收本金
        investTotalMessageBean.setInterestRecycled(arr[1]);//待回收本金
        investTotalMessageBean.setMoneyRecycled(arr[2]);//待回收总额
        investTotalMessageBean.setAvgBenefit(avgBenefit);

        modelMap.put("investTotalMessageBean", investTotalMessageBean);

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/invest/invest_tj.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }

    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

}
