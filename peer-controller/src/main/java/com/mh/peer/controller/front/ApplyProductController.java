package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.ProductApplyDetailsMessageBean;
import com.mh.peer.model.message.ProductApplyMessageBean;
import com.mh.peer.model.message.ProductInfoMessageBean;
import com.mh.peer.model.message.ProductReceiveInfoMessageBean;
import com.mh.peer.service.front.ApplyProductService;
import com.mh.peer.service.product.ProductBuyService;
import com.mh.peer.util.*;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhangerxin on 2016-9-7.
 * 申请项目
 */
@Controller
@RequestMapping("/applyProduct")
public class ApplyProductController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ApplyProductService applyProductService;
    @Autowired
    private ProductBuyService productBuyService;

    /**
     * 已申请项目集合
     * @param productApplyMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/getApplyProduct", method = RequestMethod.POST)
    @ResponseBody
    public ProductApplyMessageBean getApplyProduct(@ModelAttribute ProductApplyMessageBean productApplyMes,ModelMap modelMap){
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        String pageIndex = productApplyMes.getPageIndex(); //当前数
        String pageSize = productApplyMes.getPageSize(); //每页显示数量
        List<ProductApplyMessageBean> listProductApplyMes = new ArrayList<ProductApplyMessageBean>();
        int totalCount = 0; //已申请项目数量
        String applyStr = ""; //已申请部分拼接
        String pageStr = ""; //分页部分拼接

        if (pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            productApplyMes.setPageIndex(pageIndex);
            modelMap.put("pageIndex",pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "5";
            productApplyMes.setPageSize(pageSize);
            modelMap.put("pageSize",pageSize);
        }

        if(memberInfo != null){
            memberId = memberInfo.getId(); //会员id
            productApplyMes.setApplyMemberId(memberId); //申请人
            listProductApplyMes = applyProductService.getApplyProductList(productApplyMes); //已申请项目集合
            totalCount = applyProductService.getApplyProductListCount(productApplyMes); //已申请项目数量
            FrontPage frontPage = new FrontPage();
            pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageApply","goApplyPage"); //已申请部分分页
            if (listProductApplyMes != null && listProductApplyMes.size() > 0){
                for (ProductApplyMessageBean productApplyMessageBean : listProductApplyMes){
                    applyStr = applyStr + "<tr class=\"t_txt\">"
                             + "<td>"+productApplyMessageBean.getTitle()+"</td>"
                             + "<td>"+productApplyMessageBean.getCode()+"</td>"
                             + "<td>"+productApplyMessageBean.getApplyTime()+"</td>"
                             + "<td>"+productApplyMessageBean.getLoanLength()+"</td>"
                             + "<td>"+productApplyMessageBean.getLoanRate()+"%</td>"
                             + "<td>"+productApplyMessageBean.getLoanPrice()+"元</td>"
                             + "<td class=\"caozuo\">"
                             + "<a href=\"JavaScript:void(0)\" onclick=\"doQueryApplyProductDetails('"+productApplyMessageBean.getProductId()+"')\">"
                             + "详情"
                             + "</a>"
                             + "</td>"
                             + "</tr>";
                }
            }

            productApplyMes.setListStr(applyStr); //列表部分拼接
            productApplyMes.setPageStr(pageStr); //分页部分拼接
        }
        return productApplyMes;
    }


    /**
     * 查询已申请项目详情
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryApplyProductDetails", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryApplyProductDetails(HttpServletRequest request,ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String productId = request.getParameter("productId"); //产品id
        String memberId = ""; //会员id
        String yPrice = ""; //应还款金额(已还金额部分，String型)
        Double yPriceDou = 0.0d; //应还款金额(已还款金额部分，Double型)
        String dPrice = ""; //应还款金额(待还金额部分，String型)
        Double dPriceDou = 0.0d; //应还款金额(待还款金额部分，Double型)
        Double repayProgress = 0.0d; //还款进度
        String repayFlag = ""; //还款状态(0-待还款、1-还款中、2-已还清)
        String repayFlagName = ""; //还款状态名称
        String shFlag = ""; //审核状态(0-审核通过、1-审核不通过、2-待审核)
        String serverAddress = ""; //服务地址
        String pageIndex = request.getParameter("pageIndex"); //起始数
        String pageSize = request.getParameter("pageSize"); //每页显示数量
        String pageStr = ""; //分页部分拼接

        /******************** 产品信息Start ********************/
        ProductInfoMessageBean productInfoMessageBean = productBuyService.queryProductDetails(productId);
        if (productInfoMessageBean != null){
            shFlag = productInfoMessageBean.getShFlag();
        }
        modelMap.put("productInfoMessageBean",productInfoMessageBean);
        /********************* 产品信息End *********************/

        if (memberInfo != null){
            memberId = memberInfo.getId();

            /******************** 已还款情况Start ********************/
            ProductApplyDetailsMessageBean yProductApplyDetailsMessageBean = applyProductService.getyRepay(productId,memberId); //已还款情况
            if (yProductApplyDetailsMessageBean != null){
                yPrice = yProductApplyDetailsMessageBean.getYprice(); //应还款金额
                if (yPrice != null && !yPrice.equals("")){
                    yPriceDou = TypeTool.getDouble(yPrice);
                }
            }
            modelMap.put("yProductApplyDetailsMessageBean",yProductApplyDetailsMessageBean);
            /********************* 已还款情况End *********************/

            /******************** 待还款情况Start ********************/
            ProductApplyDetailsMessageBean dProductApplyDetailsMessageBean = applyProductService.getdRepay(productId, memberId); //待还款情况
            if (dProductApplyDetailsMessageBean != null){
                dPrice = dProductApplyDetailsMessageBean.getYprice(); //应还款金额
                if (dPrice != null && !dPrice.equals("")){
                    dPriceDou = TypeTool.getDouble(dPrice);
                }
            }
            modelMap.put("dProductApplyDetailsMessageBean",dProductApplyDetailsMessageBean);
            /********************* 待还款情况End *********************/

            repayProgress = PublicsTool.round((yPriceDou/(yPriceDou+dPriceDou)*100),2); //还款进度
            modelMap.put("repayProgress",repayProgress);

            if (shFlag != null && !shFlag.equals("")){
                if (shFlag.equals("0")){ //审核通过
                    if (dPriceDou == 0){
                        repayFlag = "2"; //已还清
                        repayFlagName = "已还清";
                    }else{
                        repayFlag = "1"; //还款中
                        repayFlagName = "还款中";
                    }
                }else{
                    repayFlag = "0"; //待还款
                    repayFlagName = "待还款";
                }
            }

            modelMap.put("repayFlag",repayFlag); //还款状态
            modelMap.put("repayFlagName",repayFlagName); //还款状态名称

            if (pageIndex == null || pageIndex.equals("")){ //起始数
                pageIndex = "1";
            }
            if (pageSize == null || pageSize.equals("")){ //每页显示数量
                pageSize = "5";
            }
            //还款列表集合
            List<ProductApplyDetailsMessageBean> listProductApplyDetailsMes = applyProductService.getRepayList(productId,"YREPAYMENTTIME","ASC",pageIndex,pageSize); //还款集合
            //还款列表数量
            int totalCount = applyProductService.getRepayListCount(productId);
            FrontPage frontPage = new FrontPage();
            pageStr = frontPage.getPageInfo2(pageIndex, pageSize, totalCount, "pageRepay", "goRepayPage", productId); //还款信息部分分页
            modelMap.put("listProductApplyDetailsMes",listProductApplyDetailsMes); //还款集合
            modelMap.put("pageStr",pageStr); //分页部分拼接

            PublicAddress publicAddress = new PublicAddress();
            serverAddress = publicAddress.getServerAddress(); //服务器地址
            modelMap.put("serverAddress",serverAddress);
        }

        JetbrickBean bean = new JetbrickBean();
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/product/product_details.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 根据条件查询已申请项目详情中的还款列表
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/doQueryApplyProductDetailsBySome", method = RequestMethod.POST)
    @ResponseBody
    public void doQueryApplyProductDetailsBySome(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap) {
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String productId = request.getParameter("productId"); //产品id
        String memberId = ""; //会员id
        String pageIndex = request.getParameter("pageIndex"); //起始数
        String pageSize = request.getParameter("pageSize"); //每页显示数量
        String str = ""; //列表部分拼接
        String pageStr = ""; //分页部分拼接

        if (memberInfo != null){
            memberId = memberInfo.getId();
            if (pageIndex == null || pageIndex.equals("")){ //起始数
                pageIndex = "1";
            }
            if (pageSize == null || pageSize.equals("")){ //每页显示数量
                pageSize = "5";
            }
            //还款列表集合
            List<ProductApplyDetailsMessageBean> listProductApplyDetailsMes = applyProductService.getRepayList(productId,"YREPAYMENTTIME","ASC",pageIndex,pageSize); //还款集合
            //还款列表数量
            int totalCount = applyProductService.getRepayListCount(productId);
            FrontPage frontPage = new FrontPage();
            pageStr = frontPage.getPageInfo2(pageIndex, pageSize, totalCount, "pageRepay", "goRepayPage", productId); //还款信息部分分页
            if (listProductApplyDetailsMes != null && listProductApplyDetailsMes.size() > 0){
                for (ProductApplyDetailsMessageBean productApplyDetailsMessageBean : listProductApplyDetailsMes){
                    str = str + "<tr class=\"bj_tr\">"
                              + "<td>"+productApplyDetailsMessageBean.getdRepayTime()+"</td>"
                              + "<td>"+productApplyDetailsMessageBean.getdRepayTime()+"</td>"
                              + "<td>￥"+productApplyDetailsMessageBean.getYprice()+"元</td>"
                              + "<td>￥"+productApplyDetailsMessageBean.getyRepayPrice()+"元</td>"
                              + "</tr>";
                }
            }
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("str",str); //列表部分拼接
        in.put("pageStr",pageStr); //分页部分拼接
        output(response, JSONObject.fromObject(in).toString());
    }

    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
