package com.mh.peer.controller.bank;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.BankWhMessageBean;
import com.mh.peer.service.bank.BankService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Cuibin on 2016/5/16.  queryAllBank
 */
@Controller
@RequestMapping("/bank")
public class BankController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BankController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private BankService bankService;


    /**
     * 查询所有银行
     */
    @RequestMapping(value = "/queryAllBank", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAllBank(@ModelAttribute BankWhMessageBean bankWhMessageBean, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = bankWhMessageBean.getPageIndex(); //当前数量
        String pageSize = bankWhMessageBean.getPageSize(); //每页显示数量
        String currentPage = bankWhMessageBean.getCurrentPage(); //当前页
        String keyword = bankWhMessageBean.getKeyword(); //关键字

        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
        }
        if(pageSize != null || !pageSize.equals("")){
            modelMap.put("keyword", keyword);
        }

        bankWhMessageBean.setPageIndex(pageIndex); //当前页
        bankWhMessageBean.setPageSize(pageSize); //每页显示数量
        List<BankWhMessageBean> list = bankService.queryAllBank(bankWhMessageBean);
        int count = bankService.queryAllBankCount(bankWhMessageBean);
        String pageStr = this.getShowPage(currentPage, count); //显示所属分页

        modelMap.put("bankList", list);

        modelMap.put("totalCount", count); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/bank/bank.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 保存银行
     */
    @RequestMapping(value = "/saveBank", method = RequestMethod.POST)
    @ResponseBody
    public BankWhMessageBean saveBank(@ModelAttribute BankWhMessageBean bankWhMessageBean){
        return bankService.saveBank(bankWhMessageBean);
    }
    /**
     * 查询一个银行
     */
    @RequestMapping(value = "/queryOnlyBank", method = RequestMethod.POST)
    @ResponseBody
    public BankWhMessageBean queryOnlyBank(@ModelAttribute BankWhMessageBean bankWhMessageBean){
        return bankService.queryOnlyBank(bankWhMessageBean);
    }
    /**
     * 更新银行
     */
    @RequestMapping(value = "/updateBank", method = RequestMethod.POST)
    @ResponseBody
    public BankWhMessageBean updateBank(@ModelAttribute BankWhMessageBean bankWhMessageBean){
        return bankService.updateBank(bankWhMessageBean);
    }
    /**
     * 删除银行
     */
    @RequestMapping(value = "/deleteBank", method = RequestMethod.POST)
    @ResponseBody
    public BankWhMessageBean deleteBank(@ModelAttribute BankWhMessageBean bankWhMessageBean){
        return bankService.deleteBank(bankWhMessageBean);
    }

    /**
     * 获取需显示数字页码
     */
    public String getShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";
        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGoBankPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }



}
