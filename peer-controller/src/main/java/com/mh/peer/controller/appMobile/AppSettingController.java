package com.mh.peer.controller.appMobile;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.AppPersonInfoMessageBean;
import com.mh.peer.model.message.MemberBanknoMessageBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.model.message.MemberLinkmanMessageBean;
import com.mh.peer.service.bank.BankService;
import com.mh.peer.service.front.SecuritySettingsService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.DownloadFile;
import com.mh.peer.util.Encrypt;
import com.mh.peer.util.PublicAddress;
import com.mh.peer.util.SearchFiles;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Cuibin on 2016/7/22.
 */
@Controller
@RequestMapping("/appSetting")
public class AppSettingController {

    @Autowired
    SecuritySettingsService securitySettingsService;
    @Autowired
    BankService bankService;
    @Autowired
    private AllMemberService allMemberService;


    /**
     * 检查是否需要更新，获取版本
     * @param request
     * @param response
     */
    @RequestMapping(value = "/checkUpdate", method = RequestMethod.POST)
    @ResponseBody
    public void checkUpdate(HttpServletRequest request,HttpServletResponse response){
        String updateBz = request.getParameter("updateBz"); //更新标志(0-Android,1-Ios)
        String path = ""; //检测更新路径
        String version = ""; //版本号
        if (updateBz != null && !updateBz.equals("")){
            PublicAddress publicAddress = new PublicAddress();
            if (updateBz.equals("0")){ //Andriod
                path = publicAddress.getCheckUpdateAndriod();
                version = SearchFiles.queryFiles(path,"0").get(0); //版本文件
                if (version != null && !version.equals("")){
                    version = version.substring(version.lastIndexOf("\\")+1);
                    version = version.replaceAll(".apk", "");
                }
            }else if (updateBz.equals("1")){ //Ios
                path = publicAddress.getCheckUpdateIos();
            }

        }
        HashMap<String, Object> in = null;
        in = new HashMap<String, Object>();
        in.put("version", version);
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 下载更新
     * @param request
     * @param response
     */
    @RequestMapping(value = "/downUpdate", method = RequestMethod.GET)
    @ResponseBody
    public void downUpdate(HttpServletRequest request,HttpServletResponse response) throws IOException {
        String updateBz = request.getParameter("updateBz"); //版本控制(0-android，1-ios)
        String path = ""; //检测更新路径(绝对)
        String fileName = ""; //文件名称

        if (updateBz != null && !updateBz.equals("")){
            PublicAddress publicAddress = new PublicAddress();
            if (updateBz.equals("0")){ //android
                path = publicAddress.getCheckUpdateAndriod(); //安卓路径(绝对)
                System.out.println("path======"+path);
                fileName = SearchFiles.queryFiles(path,"0").get(0); //android文件名称
                System.out.println("fileName======"+fileName);
                if (fileName != null && !fileName.equals("")){
                    fileName = fileName.substring(fileName.lastIndexOf("\\")+1);
                }
            }else if (updateBz.equals("1")){ //ios
                path = publicAddress.getCheckUpdateIos(); //ios路径
                fileName = SearchFiles.queryFiles(path,"0").get(0); //ios文件名称
            }
        }
        DownloadFile downloadFile = new DownloadFile();
        downloadFile.download("/"+fileName,"煎饼金融.apk",response);
    }


    /**
     * 跳转到个人信息资料
     */
    @RequestMapping(value = "/openPersonal", method = RequestMethod.POST)
    @ResponseBody
    public void openPersonal(HttpServletRequest request, HttpServletResponse response) {
        String memberId = request.getParameter("memberId"); //会员id
        String nickname = ""; //昵称
        String realname = ""; //真实姓名
        String telephone = ""; //电话

        AppPersonInfoMessageBean appPersonInfoMessageBean = allMemberService.getAppPersonInfo(memberId); //会员信息

        if (appPersonInfoMessageBean != null) {
            nickname = appPersonInfoMessageBean.getNickName(); //昵称
            realname = appPersonInfoMessageBean.getRealName(); //真实姓名
            telephone = appPersonInfoMessageBean.getTelePhone(); //手机号

            if (realname != null && !realname.equals("")){
                realname = realname.substring(0, 1) + "**";
            }

            if (telephone != null && !telephone.equals("")){
                telephone = telephone.substring(0, 3) + "****" + telephone.substring(7, 11);
            }
        }

        HashMap<String, Object> in = null;
        in = new HashMap<String, Object>();
        in.put("realname", realname); //真实姓名
        in.put("telephone", telephone); //手机号
        in.put("nickname", nickname); //昵称
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 跳转到用户姓名认证
     */
    @RequestMapping(value = "/openRealname", method = RequestMethod.POST)
    @ResponseBody
    public void openRealname(HttpServletRequest request, HttpServletResponse response) {
        String memberId = request.getParameter("memberId"); //会员id
        String realname = ""; //真实姓名
        String idcard = ""; //身份证号
        AppPersonInfoMessageBean appPersonInfoMessageBean = allMemberService.getAppPersonInfo(memberId);

        if (appPersonInfoMessageBean != null){
            realname = appPersonInfoMessageBean.getRealName(); //真实姓名
            idcard = appPersonInfoMessageBean.getIdCard(); //身份证号
            if (realname != null && !realname.equals("")){
                realname = realname.substring(0, 1) + "**";
            }
            if (idcard != null && !idcard.equals("")){
                idcard = idcard.substring(0, 6) + "****" + idcard.substring(idcard.length() - 4, idcard.length());
            }
        }

        HashMap<String, Object> in = null;
        in = new HashMap<String, Object>();
        in.put("realname", realname); //真实姓名
        in.put("idcard", idcard); //身份证号
        output(response, JSONObject.fromObject(in).toString());
    }

    /**
     * 跳转到紧急联系人
     */
    @RequestMapping(value = "/openEmergencyPerson", method = RequestMethod.POST)
    @ResponseBody
    public void openEmergencyPerson(HttpServletRequest request, HttpServletResponse response) {

        String memberId = request.getParameter("memberId"); //会员id
        String xm = ""; //姓名
        String telephone = ""; //电话
        String relationship = ""; //关系
        String relationshipName = ""; //关系名称
        MemberLinkmanMessageBean memberLinkmanMes = securitySettingsService.queryOnlyContact(memberId);

        if (memberLinkmanMes != null){
            xm = memberLinkmanMes.getXm(); //联系人姓名
            telephone = memberLinkmanMes.getTelephone();//手机号
            relationship = memberLinkmanMes.getRelationship();//与联系人关系

           /* if (relationship != null && !relationship.equals("")){
                if (relationship.equals("0")){
                    relationshipName = "父母";
                }else if (relationship.equals("1")){
                    relationshipName = "配偶";
                }else if (relationship.equals("2")){
                    relationshipName = "子女";
                }else if (relationship.equals("3")){
                    relationshipName = "兄弟姐妹";
                }else if (relationship.equals("4")){
                    relationshipName = "朋友";
                }else if (relationship.equals("5")){
                    relationshipName = "同事";
                }else if (relationship.equals("6")){
                    relationshipName = "其他";
                }
            }*/
        }

        HashMap<String, Object> in = null;
        in = new HashMap<String, Object>();
        in.put("xm", xm);
        in.put("telephone", telephone);
        in.put("relationship", relationship);
        output(response, JSONObject.fromObject(in).toString());
    }



    /**
     * 保存紧急联系人
     */
    @RequestMapping(value = "/saveEmergencyPerson", method = RequestMethod.POST)
    @ResponseBody
    public void saveEmergencyPerson(HttpServletRequest request, HttpServletResponse response) {

        String memberId = request.getParameter("memberId"); //会员id
        String xm = request.getParameter("xm"); //联系人姓名
        String telephone = request.getParameter("telephone"); //手机号
        String relationship = request.getParameter("relationship"); //与联系人关系

        MemberLinkmanMessageBean memberLinkmanMes = new MemberLinkmanMessageBean();
        memberLinkmanMes.setXm(xm);
        memberLinkmanMes.setTelephone(telephone);
        memberLinkmanMes.setRelationship(relationship);
        memberLinkmanMes.setMemberid(memberId);
        memberLinkmanMes = securitySettingsService.appSaveContact(memberLinkmanMes);

        HashMap<String, Object> in = null;
        in = new HashMap<String, Object>();
        in.put("info", memberLinkmanMes.getInfo());
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 跳转到添加银行卡
     */
    @RequestMapping(value = "/openAddBank", method = RequestMethod.POST)
    @ResponseBody
    public void openAddBank(HttpServletRequest request, HttpServletResponse response) {

        String memberId = request.getParameter("memberId"); //会员id
        MemberBanknoMessageBean memberBanknoMes = new MemberBanknoMessageBean();
        memberBanknoMes.setMemberid(memberId);
        List<MemberBanknoMessageBean> list = bankService.getMemberBankInfoBySome(memberBanknoMes);

        MemberInfoMessageBean memberInfoMes = new MemberInfoMessageBean();
        memberInfoMes.setId(memberId);
        memberInfoMes = allMemberService.queryOnlyMember(memberInfoMes);

        String bankName = list.get(0).getBankName(); //银行名称
        String realname = memberInfoMes.getRealname(); //真实姓名
        if (!"".equals(realname) && null != realname) {
            realname = realname.substring(0, 1) + "**";
        }

        HashMap<String, Object> in = null;
        in = new HashMap<String, Object>();
        in.put("bankName", bankName);
        in.put("realname", realname);
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 修改登陆密码
     */
    @RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
    @ResponseBody
    public void updatePwd(HttpServletRequest request, HttpServletResponse response) {
        String memberId = request.getParameter("memberId"); //会员id
        String oldPwd = request.getParameter("oldPwd"); //原始密码
        String newPwd = request.getParameter("newPwd"); //新密码
        String password = "";

        MemberInfoMessageBean memberInfoMes = new MemberInfoMessageBean();
        memberInfoMes.setId(memberId);
        memberInfoMes = allMemberService.queryOnlyMember(memberInfoMes);

        password = memberInfoMes.getPassword();
        MemberInfoMessageBean memberInfoMes2 = new MemberInfoMessageBean();
        Encrypt encrypt = new Encrypt();
        oldPwd = encrypt.handleMd5(oldPwd);
        if(oldPwd.equals(password)){
            memberInfoMes2.setId(memberId);
            memberInfoMes2.setPassword(newPwd);
            memberInfoMes2 = allMemberService.updateMemberPassWord(memberInfoMes2);
        }else{
            memberInfoMes2.setInfo("当前密码不正确");
        }

        HashMap<String, Object> in = null;
        in = new HashMap<String, Object>();
        in.put("info", memberInfoMes2.getInfo());
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 输出结果到response中
     *
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8"));
            response.getOutputStream().flush();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
