package com.mh.peer.controller.huanxun;

import com.mh.peer.model.entity.HuanxunPostal;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.HuanXunPostalMessageBean;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.service.huanxun.HuanXunPostalService;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-7-25.
 * 环迅提现部分
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunPostalController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private HuanXunRegistService huanXunRegistService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private HuanXunPostalService huanXunPostalService;

    /**
     * 环迅提现部分
     * @param huanXunPostalMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/postalhx", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunPostalMessageBean postalhx(@ModelAttribute HuanXunPostalMessageBean huanXunPostalMes, ModelMap modelMap) {
        String operationType = huanXunPostalMes.getOperationType(); //操作类型
        String merchantID = huanXunPostalMes.getMerchantID(); //商户存管交易账号
        String merBillNo = "181006"; //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //提现日期
        String userType = "1"; //用户类型(1-个人、2-企业)
        String trdAmt = huanXunPostalMes.getTrdAmt(); //提现金额(Stirng型)
        Double trdAmtDou = 0.0d; //提现金额(Double型)
        String merFee = "0"; //平台手续费(String型)
        Double merFeeDou = 0.0d; //平台手续费(Double型)
        String merFeeType = "1"; //平台手续费类型(1-内扣、2-外扣)
        String ipsFeeType = "2"; //IPS手续费承担方(1-平台承担、2-用户承担)
        String ipsAcctNo = ""; //IPS存管账户号
        String balance = "0"; //余额(String型)
        Double balanceDou = 0.0d; //余额(Double型)
        String webUrl = "http://jianbing88.com/front/registerhxWebResult"; //浏览器返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/postalhxResult"; //后台通知地址
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        String md5Zs = ""; //md5证书
        String memberId = ""; //会员id
        String glId = ""; //关联id
        Double minusFee = 0.0d; //扣除金额
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        HuanXunPostalMessageBean huanXunPostalMessageBean = new HuanXunPostalMessageBean(); //环迅提现
        Map<String,String> mapPostal = new HashMap<String,String>();
        String result = "0"; //提现结果

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs();

            glId = UUID.randomUUID().toString().replaceAll("-",""); //关联id

            if (memberInfo == null){
                huanXunPostalMessageBean.setCode("-1"); //执行状态
                huanXunPostalMessageBean.setInfo("请重新登录!"); //执行信息
                return huanXunPostalMessageBean;
            }
            memberId = memberInfo.getId(); //会员信息

            /*************** 判断提现金额是否大于服务费Start ***************/
            merFee = MerFeeHandle.getPostalFee(memberId,trdAmt); //获取服务费
            if (merFee != null && !merFee.equals("")){
                merFeeDou = TypeTool.getDouble(merFee); //平台手续费(Double型)
            }
            if (trdAmt != null && !trdAmt.equals("")){ //提现金额(String型)
                trdAmtDou = TypeTool.getDouble(trdAmt); //提现金额(Double型)
            }
            minusFee = merFeeDou + 2; //扣除金额
            if (trdAmtDou <= minusFee){
                huanXunPostalMessageBean.setCode("-1"); //执行状态
                huanXunPostalMessageBean.setInfo("提现金额必须大于"+minusFee+"元!"); //执行信息
                return huanXunPostalMessageBean;
            }
            /**************** 判断提现金额是否大于服务费End ****************/

            if (memberId != null && !memberId.equals("")){ //会员id
                webUrl = webUrl + "?memberId="+memberId; //浏览器返回地址
                s2SUrl = s2SUrl + "?memberId="+memberId+"&glId="+glId+"&source=0"; //后台通知地址

                /*************** 判断提现金额是否小于余额Start ***************/
                balance = allMemberService.queryOnlyMemberBalance(memberId); //余额(String型)
                if (balance != null && !balance.equals("")){
                    balanceDou = TypeTool.getDouble(balance); //余额(Double型)
                }
                if (balanceDou < trdAmtDou){
                    huanXunPostalMessageBean.setCode("-1"); //执行状态
                    huanXunPostalMessageBean.setInfo("余额不足，当前余额为"+balanceDou+"元！"); //执行信息
                    return huanXunPostalMessageBean;
                }
                /**************** 判断提现金额是否小于余额End ****************/

                HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean();
                huanXunRegistMessageBean.setMemberId(memberId); //会员id
                //查询会员注册信息
                List<HuanXunRegistMessageBean> listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
                if (listHuanXunRegistMes == null || listHuanXunRegistMes.size() == 0){
                    huanXunPostalMessageBean.setCode("-1"); //执行状态
                    huanXunPostalMessageBean.setInfo("您还未注册环迅，请前去注册！");
                    return huanXunPostalMessageBean;
                }
                ipsAcctNo = listHuanXunRegistMes.get(0).getIpsAcctNo(); //IPS存管账号
                merBillNo = merBillNo + TradeHandle.getPostalSerialNumber(); //商户订单号

                request = "{\"merBillNo\":\""+merBillNo+"\",\"merDate\":\""+merDate+"\",\"userType\":\""+userType+"\","
                        + "\"trdAmt\":\""+trdAmt+"\",\"merFee\":\""+merFee+"\",\"merFeeType\":\""+merFeeType+"\","
                        + "\"ipsFeeType\":\""+ipsFeeType+"\",\"ipsAcctNo\":\""+ipsAcctNo+"\",\"webUrl\":\""+webUrl+"\","
                        + "\"s2SUrl\":\""+s2SUrl+"\"}";

                requestJm = Pdes.encrypt3DES(request); //3des加密
                sign = operationType + merchantID + requestJm + md5Zs; //签名
                signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

                huanXunPostalMes.setRequest(requestJm); //请求信息
                huanXunPostalMes.setSign(signJm); //签名

                /********** 提现备份Start **********/
                mapPostal.put("memberId",memberId); //会员id
                mapPostal.put("merBillNo",merBillNo); //商户订单号
                mapPostal.put("merDate",merDate); //提现日期
                mapPostal.put("userType",userType); //用户类型(1-个人、2-企业)
                mapPostal.put("trdAmt",trdAmt); //提现金额
                mapPostal.put("merFee",merFee); //平台手续费
                mapPostal.put("merFeeType",merFeeType); //平台手续费类型(1-内扣、2-外扣)
                mapPostal.put("ipsFeeType",ipsFeeType); //IPS手续费承担方(1-平台承担、2-用户承担)
                mapPostal.put("ipsAcctNo",ipsAcctNo); //IPS存管账户号
                result = PostalBf.postalHandle(mapPostal); //提现备份结果
                /*********** 提现备份End ***********/

                huanXunPostalMes.setCode(result); //提现结果(0-成功、1-失败)
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return huanXunPostalMes;
    }


    /**
     * 提现结果
     * @param request
     * @param modelMap
     */
    @RequestMapping(value = "/postalhxResult", method = RequestMethod.POST)
    @ResponseBody
    public void postalhxResult(HttpServletRequest request, ModelMap modelMap) {
        String memberId = request.getParameter("memberId"); //会员id
        String glId = request.getParameter("glId"); //关联id
        String source = request.getParameter("source"); //来源(0-电脑端、2-手机端)
        String resultCode = request.getParameter("resultCode"); //响应状态(000000-成功、999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //响应信息描述
        String memberid = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String response = request.getParameter("response"); //响应信息
        String responseJm = ""; //响应信息(解密)
        String merBillNo = ""; //商户订单号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String merFee = ""; //平台手续费(String型)
        Double merFeeDou = 0d; //平台手续费(Double型)
        String ipsFee = ""; //IPS手续费(String型)
        Double ipsFeeDou = 0d; //IPS手续费(Double型)
        String ipsAcctNo = ""; //IPS存管账户号
        String ipsTrdAmt = ""; //用户到账金额(String型)
        Double ipsTrdAmtDou = 0d; //用户到账金额(Double型)
        String trdStatus = ""; //提现状态(0-失败、1-成功、2-处理中、3-退票)
        String balance = ""; //余额(String型)
        Double balanceDou = 0d; //余额(Double型)
        Map<String,String> map = new HashMap<String,String>();
        String Uidpostalhx = ""; //提现主键
        String UidMyMessage = ""; //我的消息部分主键
        String UidTradeDetail = ""; //交易记录主键
        String title = ""; //题目
        String content = ""; //内容

        try{

            MemberInfoMessageBean memberInfoMessageBean = allMemberService.queryMemberInfoById(memberId);
            if (memberInfoMessageBean != null){
                balance = memberInfoMessageBean.getBalance(); //余额(String型)
                if (balance != null && !balance.equals("")){
                    balanceDou = TypeTool.getDouble(balance); //余额(Double型)
                }
            }

            if (response != null && !response.equals("")){
                responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                if (responseJm != null && !responseJm.equals("")){
                    map = JsonHelper.getObjectToMap(responseJm);
                    if (map != null){
                        merBillNo = map.get("merBillNo"); //商户订单号
                        ipsBillNo = map.get("ipsBillNo"); //IPS订单号
                        ipsDoTime = map.get("ipsDoTime"); //IPS处理时间
                        merFee = map.get("merFee"); //平台手续费(String型)
                        if (merFee != null && !merFee.equals("")){
                            merFeeDou = TypeTool.getDouble(merFee); //平台手续费(Double型)
                        }
                        ipsFee = map.get("ipsFee"); //IPS手续费(String型)
                        if (ipsFee != null && !ipsFee.equals("")){
                            ipsFeeDou = TypeTool.getDouble(ipsFee); //IPS手续费(Double型)
                        }
                        ipsAcctNo = map.get("ipsAcctNo"); //IPS存管账户号
                        ipsTrdAmt = map.get("ipsTrdAmt"); //用户到账金额(String型)
                        if (ipsTrdAmt != null && !ipsTrdAmt.equals("")){
                            ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //用户到账金额(Double型)
                        }
                        trdStatus = map.get("trdStatus"); //提现状态(0-失败、1-成功、2-处理中、3-退票)
                    }
                }
            }

            /********** 环迅支付提现记录部分Start **********/
            HuanxunPostal huanxunPostal = new HuanxunPostal();
            Uidpostalhx = UUID.randomUUID().toString().replaceAll("-",""); //提现部分主键
            huanxunPostal.setId(Uidpostalhx); //主键
            huanxunPostal.setGlid(glId); //关联id
            huanxunPostal.setMemberid(memberId); //会员id
            huanxunPostal.setSource(source); //来源(0-电脑端、1-手机端)
            huanxunPostal.setResultcode(resultCode); //响应状态(000000-成功、999999-失败)
            huanxunPostal.setResultmsg(resultMsg); //响应信息描述
            huanxunPostal.setMerchantid(memberid); //商户存管交易账号
            huanxunPostal.setSign(sign); //签名
            huanxunPostal.setMerbillno(merBillNo); //商户订单号
            huanxunPostal.setIpsbillno(ipsBillNo); //IPS订单号
            huanxunPostal.setIpsdotime(ipsDoTime); //IPS处理时间
            huanxunPostal.setMerfee(merFeeDou); //平台手续费
            huanxunPostal.setIpsfee(ipsFeeDou); //IPS手续费
            huanxunPostal.setIpsacctno(ipsAcctNo); //IPS存管账户号
            huanxunPostal.setIpstrdamt(ipsTrdAmtDou); //用户到账金额
            huanxunPostal.setTrdstatus(trdStatus); //提现状态
            huanxunPostal.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
            /*********** 环迅支付提现记录部分End ***********/

            MemberMessage memberMessage = new MemberMessage(); //我的消息部分
            TradeHandle tradeHandle = new TradeHandle(); //流水号工具类
            TradeDetail tradeDetail = new TradeDetail(); //交易记录

            if(resultCode != null && !resultCode.equals("")) { //响应吗
                if (resultCode.equals("000000")) { //成功
                    /********** 我的消息部分(Start) **********/
                    UidMyMessage = UUID.randomUUID().toString().replaceAll("-",""); //主键
                    ipsDoTime = ipsDoTime.substring(0,16); //IPS处理时间

                    title = "环迅提现成功";
                    content = "<li>商户订单号：<span>"+merBillNo+"</span></li>"
                            + "<li>IPS订单号：<span>"+ipsBillNo+"</span></li>"
                            + "<li>IPS处理时间：<span>"+ipsDoTime+"</span></li>"
                            + "<li>平台手续费：<span>"+merFeeDou+"元</span></li>"
                            + "<li>IPS手续费：<span>"+ipsFeeDou+"元</span></li>"
                            + "<li>IPS存管账户号：<span>"+ipsAcctNo+"</span></li>"
                            + "<li>用户到账金额：<span>"+ipsTrdAmtDou+"元</span></li>";

                    memberMessage.setId(UidMyMessage); //主键
                    memberMessage.setGlid(Uidpostalhx); //关联id
                    memberMessage.setType("0"); //类型(0-系统消息)
                    memberMessage.setMemberid(memberId); //会员id
                    memberMessage.setReadflag("1"); //读取状态(0-已读、1-未读)
                    memberMessage.setTitle(title); //题目
                    memberMessage.setContent(content); //内容
                    memberMessage.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送日期
                    memberMessage.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建日期
                    memberMessage.setDelflg("1"); //删除标志(0-已删除、1-未删除)
                    /*********** 我的消息部分(End) ***********/

                    /********** 交易记录Start **********/
                    int serialNumber = tradeHandle.getSerialNumber(); //流水号
                    UidTradeDetail = UUID.randomUUID().toString().replaceAll("-",""); //交易记录主键
                    tradeDetail.setId(UidTradeDetail); //主键
                    tradeDetail.setGlid(Uidpostalhx); //关联id
                    tradeDetail.setSerialnum(serialNumber); //流水号
                    tradeDetail.setType("1"); //类型(0-充值)
                    tradeDetail.setSprice(ipsTrdAmtDou); //实际金额
                    tradeDetail.setProcedurefee(ipsFeeDou); //手续费
                    tradeDetail.setFlag("0"); //状态(0-成功、1-失败)
                    tradeDetail.setMemberid(memberId); //会员id
                    tradeDetail.setSource(source); //来源(0-电脑端、1-手机端)
                    tradeDetail.setTime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //交易时间
                    tradeDetail.setBz("环迅支付提现"); //备注
                    tradeDetail.setBalance(balanceDou); //余额
                    tradeDetail.setCreateuser(memberId); //创建人
                    tradeDetail.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                    /*********** 交易记录End ***********/
                }
            }

            huanXunPostalService.postalhxResult(huanxunPostal,memberMessage,tradeDetail);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
