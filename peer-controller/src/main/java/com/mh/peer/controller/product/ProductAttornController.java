package com.mh.peer.controller.product;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.ProductAttornBusiness;
import com.mh.peer.model.message.*;
import com.mh.peer.service.front.ProductAttornDetailsService;
import com.mh.peer.service.front.SecuritySettingsService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.product.ProductAttornService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.PublicAddress;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-6-23.
 */
@Controller
@RequestMapping("/productAttorn")
public class ProductAttornController {

    @Autowired
    private ProductAttornService productAttornService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private SecuritySettingsService securitySettingsService;
    @Autowired
    private ProductAttornDetailsService productAttornDetailsService;
    @Autowired
    private SysFileService sysFileService;
    @Autowired
    private JetbrickTool jetbrickTool;


    /**
     * 根据分页查询待审核的债权转让标(默认查询待审核的)
     * @param productAttornMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryProductAttornByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryProductAttornByPage(@ModelAttribute ProductAttornMessage productAttornMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productAttornMes.getPageIndex(); //当前数
        String pageSize = productAttornMes.getPageSize(); //每次显示数量
        String currentPage = productAttornMes.getCurrentPage(); //当前页
        String shFlag = productAttornMes.getShFlag(); //审核状态(0-审核通过、1-审核不通过、2-待审核)
        int pageCount = 0; //总数量

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
            productAttornMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){ //每次显示数量
            pageSize = "10";
            productAttornMes.setPageSize(pageSize);
        }
        if(shFlag == null || shFlag.equals("")){ //审核状态(0-审核通过、1-审核不通过、2-待审核)
            shFlag = "2";
            productAttornMes.setShFlag(shFlag);
        }

        //债权转让集合
        List<ProductAttornMessage> listProductAttornMes = productAttornService.queryProductAttornByPage(productAttornMes);
        //债权转让数量
        int totalCount = productAttornService.queryProductAttornCount(productAttornMes);
        //获取总数量
        pageCount = this.getPageCount(pageSize,totalCount);

        modelMap.put("listProductAttornMes",listProductAttornMes); //待审核债权集合
        modelMap.put("totalCount", totalCount); //产品数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageCount",pageCount); //总数量

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productAttornMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
     }


    /**
     * 根据分页查询待审核的债权转让标(默认查询成功的)
     * @param productAttornMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryProductAttornSuccessByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryProductAttornSuccessByPage(@ModelAttribute ProductAttornMessage productAttornMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productAttornMes.getPageIndex(); //当前数
        String pageSize = productAttornMes.getPageSize(); //每次显示数量
        String currentPage = productAttornMes.getCurrentPage(); //当前页
        int totalCount = 0; //债权数量
        int pageCount = 0; //总页数
        String shFlag = productAttornMes.getShFlag(); //审核状态(0-审核通过、1-审核不通过、2-待审核)
        String serverAddress = ""; //服务器地址

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            productAttornMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){ //每次显示数量
            pageSize = "10";
            productAttornMes.setPageSize(pageSize);
        }
        if (currentPage == null || currentPage.equals("")){ //当前页
            currentPage = "1";
        }
        if(shFlag == null || shFlag.equals("")){ //审核状态(0-审核通过、1-审核不通过、2-待审核)
            shFlag = "0";
            productAttornMes.setShFlag(shFlag);
        }

        //产品债权转让集合
        List<ProductAttornMessage> listProductAttornMes = productAttornService.queryProductAttornByPage(productAttornMes);
        //债权数量
        totalCount = productAttornService.queryProductAttornCount(productAttornMes);
        //获取总页数
        pageCount = this.getPageCount(pageSize,totalCount);

        PublicAddress publicAddress = new PublicAddress();
        serverAddress = publicAddress.getServerAddress(); //服务器地址

        modelMap.put("listProductAttornMes",listProductAttornMes); //债权集合
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("totalCount",totalCount); //总数量
        modelMap.put("pageCount",pageCount); //总页数
        modelMap.put("serverAddress",serverAddress); //服务器地址

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productAttornMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 查询待承接债权转让标
     * @param productAttornMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttornUnderTaked", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAttornUnderTaked(@ModelAttribute ProductAttornMessage productAttornMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productAttornMes.getPageIndex(); //当前数
        String pageSize = productAttornMes.getPageSize(); //每次显示数量
        String currentPage = productAttornMes.getCurrentPage(); //当前页
        String orderColumn = productAttornMes.getOrderColumn(); //排序字段
        String orderType = productAttornMes.getOrderType(); //排序类型
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        List<ProductAttornMessage> listProductAttornMes = new ArrayList<ProductAttornMessage>();
        int totalCount = 0; //数量
        int pageCount = 0; //总页数
        String serverAddress = ""; //服务器地址

        try{

            if (pageIndex == null || pageIndex.equals("")){ //当前页
                pageIndex = "1";
                currentPage = "1";
                productAttornMes.setPageIndex(pageIndex);
                modelMap.put("pageIndex",pageIndex);
            }
            if (pageSize == null || pageSize.equals("")){ //每页显示数量
                pageSize = "10";
                productAttornMes.setPageSize(pageSize);
                modelMap.put("pageSize",pageSize);
            }
            //待承接债权集合
            listProductAttornMes = productAttornService.queryProductAttornUnderTaked(productAttornMes);
            //待承接债权数量
            totalCount = productAttornService.queryProductAttornUnderTakedCount(productAttornMes);
            //获取总页数
            pageCount = this.getPageCount(pageSize,totalCount);

            PublicAddress publicAddress = new PublicAddress();
            serverAddress = publicAddress.getServerAddress(); //服务器地址

            modelMap.put("listProductAttornMes",listProductAttornMes); //债权集合
            modelMap.put("currentPage", currentPage); //当前页
            modelMap.put("totalCount",totalCount); //总数量
            modelMap.put("pageCount",pageCount); //总页数
            modelMap.put("serverAddress",serverAddress); //服务器地址

            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productAttornMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch(Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }

    /**
     * 根据条件查询待承接债权转让标
     * @param productAttornMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttornUnderTakedBySome", method = RequestMethod.POST)
    @ResponseBody
    public ProductAttornMessage queryAttornUnderTakedBySome(@ModelAttribute ProductAttornMessage productAttornMes, ModelMap modelMap){
        String pageIndex = productAttornMes.getPageIndex(); //当前数
        String pageSize = productAttornMes.getPageSize(); //每次显示数量
        String currentPage = productAttornMes.getCurrentPage(); //当前页
        String orderColumn = productAttornMes.getOrderColumn(); //排序字段
        String orderType = productAttornMes.getOrderType(); //排序类型
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        List<ProductAttornMessage> listProductAttornMes = new ArrayList<ProductAttornMessage>();
        int totalCount = 0; //数量
        int pageCount = 0; //总页数
        String attornStr = ""; //债权部分拼接
        int number = 1; //序号

        try{

            if (pageIndex == null || pageIndex.equals("")){ //当前页
                pageIndex = "1";
                currentPage = "1";
                productAttornMes.setPageIndex(pageIndex);
                modelMap.put("pageIndex",pageIndex);
            }
            if (pageSize == null || pageSize.equals("")){ //每页显示数量
                pageSize = "10";
                productAttornMes.setPageSize(pageSize);
                modelMap.put("pageSize",pageSize);
            }
            //待承接债权集合
            listProductAttornMes = productAttornService.queryProductAttornUnderTaked(productAttornMes);
            //待承接债权数量
            totalCount = productAttornService.queryProductAttornUnderTakedCount(productAttornMes);
            //获取总页数
            pageCount = this.getPageCount(pageSize,totalCount);

            if (listProductAttornMes != null && listProductAttornMes.size() > 0){
                for (ProductAttornMessage productAttornMessage : listProductAttornMes){
                    attornStr = attornStr + "<tr>"
                              + "<td>"+number+"</td>"
                              + "<td>"+productAttornMessage.getCode()+"</td>"
                              + "<td>"+productAttornMessage.getTitle()+"</td>"
                              + "<td>"+productAttornMessage.getBuyerName()+"</td>"
                              + "<td>"+productAttornMessage.getPrice()+"元</td>"
                              + "<td>"+productAttornMessage.getLoanRate()+"%</td>"
                              + "<td>"+productAttornMessage.getExpectedRate()+"%</td>"
                              + "<td>"+productAttornMessage.getLoanLength()+"</td>"
                              + "<td>"+productAttornMessage.getSurplusQs()+"期</td>"
                              + "<td>"+productAttornMessage.getSurplusDays()+"天</td>"
                              + "<td>"+productAttornMessage.getWaitingPrincipal()+"元</td>"
                              + "<td>"+productAttornMessage.getCreateTime()+"</td>"
                              + "</tr>";
                    number++;
                }
            }

            productAttornMes.setAttornStr(attornStr); //债权部分列表拼接
            productAttornMes.setTotalCount(TypeTool.getString(totalCount)); //总数量

        }catch(Exception e){
            e.printStackTrace();

        }
        return productAttornMes;
    }


    /**
     * 查询失败的债权转让标
     * @param productAttornMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttornFail", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAttornFail(@ModelAttribute ProductAttornMessage productAttornMes, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productAttornMes.getPageIndex(); //当前数
        String pageSize = productAttornMes.getPageSize(); //每次显示数量
        String currentPage = productAttornMes.getCurrentPage(); //当前页
        String orderColumn = productAttornMes.getOrderColumn(); //排序字段
        String orderType = productAttornMes.getOrderType(); //排序类型
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        String shFlag = productAttornMes.getShFlag(); //审核状态(0-审核通过、1-审核未通过、2-未审核)
        List<ProductAttornMessage> listProductAttornMes = new ArrayList<ProductAttornMessage>();
        int totalCount = 0; //数量
        int pageCount = 0; //总页数
        String serverAddress = ""; //服务器地址

        try{

            if (pageIndex == null || pageIndex.equals("")){ //当前页
                pageIndex = "1";
                currentPage = "1";
                productAttornMes.setPageIndex(pageIndex);
                modelMap.put("pageIndex",pageIndex);
            }
            if (pageSize == null || pageSize.equals("")){ //每页显示数量
                pageSize = "10";
                productAttornMes.setPageSize(pageSize);
                modelMap.put("pageSize",pageSize);
            }
            if (shFlag == null || shFlag.equals("")){ //审核状态
                shFlag = "1";
                productAttornMes.setShFlag(shFlag);
            }
            //待承接债权集合
            listProductAttornMes = productAttornService.queryProductAttornFail(productAttornMes);
            //待承接债权数量
            totalCount = productAttornService.queryProductAttornFailCount(productAttornMes);
            //获取总页数
            pageCount = this.getPageCount(pageSize,totalCount);

            PublicAddress publicAddress = new PublicAddress();
            serverAddress = publicAddress.getServerAddress(); //服务器地址

            modelMap.put("listProductAttornMes",listProductAttornMes); //债权集合
            modelMap.put("currentPage", currentPage); //当前页
            modelMap.put("totalCount",totalCount); //总数量
            modelMap.put("pageCount",pageCount); //总页数
            modelMap.put("serverAddress",serverAddress); //服务器地址

            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(productAttornMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch(Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 根据条件查询待承接债权转让标
     * @param productAttornMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAttornFailBySome", method = RequestMethod.POST)
    @ResponseBody
    public ProductAttornMessage queryAttornFailBySome(@ModelAttribute ProductAttornMessage productAttornMes, ModelMap modelMap){
        String pageIndex = productAttornMes.getPageIndex(); //当前数
        String pageSize = productAttornMes.getPageSize(); //每次显示数量
        String currentPage = productAttornMes.getCurrentPage(); //当前页
        String orderColumn = productAttornMes.getOrderColumn(); //排序字段
        String orderType = productAttornMes.getOrderType(); //排序类型
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        List<ProductAttornMessage> listProductAttornMes = new ArrayList<ProductAttornMessage>();
        int totalCount = 0; //数量
        int pageCount = 0; //总页数
        String attornStr = ""; //债权部分拼接
        int number = 1; //序号

        try{

            if (pageIndex == null || pageIndex.equals("")){ //当前页
                pageIndex = "1";
                currentPage = "1";
                productAttornMes.setPageIndex(pageIndex);
                modelMap.put("pageIndex",pageIndex);
            }
            if (pageSize == null || pageSize.equals("")){ //每页显示数量
                pageSize = "10";
                productAttornMes.setPageSize(pageSize);
                modelMap.put("pageSize",pageSize);
            }
            //待承接债权集合
            listProductAttornMes = productAttornService.queryProductAttornFail(productAttornMes);
            //待承接债权数量
            totalCount = productAttornService.queryProductAttornFailCount(productAttornMes);
            //获取总页数
            pageCount = this.getPageCount(pageSize,totalCount);

            if (listProductAttornMes != null && listProductAttornMes.size() > 0){
                for (ProductAttornMessage productAttornMessage : listProductAttornMes){
                    attornStr = attornStr + "<tr>"
                            + "<td>"+number+"</td>"
                            + "<td>"+productAttornMessage.getCode()+"</td>"
                            + "<td>"+productAttornMessage.getTitle()+"</td>"
                            + "<td>"+productAttornMessage.getBuyerName()+"</td>"
                            + "<td>"+productAttornMessage.getPrice()+"元</td>"
                            + "<td>"+productAttornMessage.getLoanRate()+"%</td>"
                            + "<td>"+productAttornMessage.getExpectedRate()+"%</td>"
                            + "<td>"+productAttornMessage.getLoanLength()+"</td>"
                            + "<td>"+productAttornMessage.getSurplusQs()+"期</td>"
                            + "<td>"+productAttornMessage.getSurplusDays()+"天</td>"
                            + "<td>"+productAttornMessage.getWaitingPrincipal()+"元</td>"
                            + "<td>"+productAttornMessage.getCreateTime()+"</td>"
                            + "</tr>";
                    number++;
                }
            }

            productAttornMes.setAttornStr(attornStr); //债权部分列表拼接
            productAttornMes.setTotalCount(TypeTool.getString(totalCount)); //总数量

        }catch(Exception e){
            e.printStackTrace();

        }
        return productAttornMes;
    }


    /**
     * 根据分页查询待审核的债权转让标(默认查询待审核的)
     * @param productAttornMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryProductAttornBySort", method = RequestMethod.POST)
    @ResponseBody
    public ProductAttornMessage queryProductAttornBySort(@ModelAttribute ProductAttornMessage productAttornMes,ModelMap modelMap){
        String pageIndex = productAttornMes.getPageIndex(); //当前数
        String pageSize = productAttornMes.getPageSize(); //每次显示数量
        String currentPage = productAttornMes.getCurrentPage(); //当前页
        String attornStr = ""; //债权转让列表拼接
        String underTakeTime = ""; //承接时间
        String shFlag = productAttornMes.getShFlag(); //审核状态(0-审核通过、1-审核未通过、2-未审核)
        int i = 1; //排序

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            productAttornMes.setPageIndex(pageIndex);
        }
        if(pageSize == null || pageSize.equals("")){ //每次显示数量
            pageSize = "10";
            productAttornMes.setPageSize(pageSize);
        }
        if (shFlag == null || shFlag.equals("")){ //审核状态
            shFlag = "2";
        }

        //产品债权转让集合
        List<ProductAttornMessage> listProductAttornMes = productAttornService.queryProductAttornByPage(productAttornMes);
        if(listProductAttornMes != null && listProductAttornMes.size() > 0){
            for(ProductAttornMessage productAttornMessage : listProductAttornMes){
                underTakeTime = productAttornMessage.getUnderTakeTime(); //承接时间
                if (underTakeTime != null && !underTakeTime.equals("")){
                    underTakeTime = underTakeTime.substring(0,16);
                }
                attornStr = attornStr + "<tr>"
                          + "<td>"+i+"</td>"
                          + "<td>"+productAttornMessage.getCode()+"</td>"
                          + "<td>"+productAttornMessage.getTitle()+"</td>"
                          + "<td>"+productAttornMessage.getBuyerName()+"</td>"
                          + "<td>"+productAttornMessage.getPrice()+"元</td>"
                          + "<td>"+productAttornMessage.getLoanRate()+"%</td>"
                          + "<td>"+productAttornMessage.getLoanLength()+"</td>"
                          + "<td>"+productAttornMessage.getSurplusQs()+"期</td>"
                          + "<td>"+productAttornMessage.getWaitingPrincipal()+"元</td>"
                          + "<td>"+underTakeTime+"</td>"
                          + "<td>"+productAttornMessage.getSurplusDays()+"天</td>"
                          + "<td>";

                if (shFlag.equals("2")){ //待审核
                    attornStr = attornStr + "<a href=\"JavaScript:void(0)\" onclick=\"openProductAttornShFlag('"+productAttornMessage.getId()+"')\">"
                              + "<span style=\"color:#06F\">审核</span>"
                              + "</a>";
                }else if (shFlag.equals("0")){
                    attornStr = attornStr + "<a href=\"JavaScript:void(0)\" onclick=\"openProductAttornDetails('"+productAttornMessage.getId()+"')\">"
                            + "<span style=\"color:#06F\">详情</span>"
                            + "</a>";
                }

                attornStr = attornStr + "</td>"
                          + "</tr>";
                i++;
            }
            productAttornMes.setAttornStr(attornStr); //债权拼接
            productAttornMes.setCurrentPage(currentPage); //当前页
        }

        return productAttornMes;
    }



    /**
     * 设置债权审核状态
     * @param productAttornMes
     * @return
     */
    @RequestMapping(value = "/setProductAttornShFlag")
    @ResponseBody
    public ProductAttornMessage setProductAttornShFlag(@ModelAttribute ProductAttornMessage productAttornMes) {
        //设置审核状态
        ProductAttornBusiness productAttornBus = productAttornService.setProductAttornShFlag(productAttornMes);
        //设置返回安全参数消息
        productAttornMes.setResult(productAttornBus.getResult()); //状态(成功或失败)
        productAttornMes.setInfo(productAttornBus.getInfo()); //错误信息
        return productAttornMes;
    }


    /**
     * 跳转到 成功的债权转让标 详情 by cuibin
     */
    @RequestMapping(value = "/openProductAttornDetails")
    @ResponseBody
    public JetbrickBean openProductAttornDetails(@ModelAttribute ProductAttornDetailsMes productAttornDetailsMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        //产品部分
        productAttornDetailsMessageBean = productAttornDetailsService.queryProductAttornDetails(productAttornDetailsMessageBean);
        String memberId = productAttornDetailsMessageBean.getBuyer();
        //头像部分
        SysFileMessageBean sysFileMessageBean = new SysFileMessageBean();
        sysFileMessageBean.setGlid(memberId); //关联id
        sysFileMessageBean.setPageIndex("1"); //当前数
        sysFileMessageBean.setPageSize("1"); //每次显示数量
        sysFileMessageBean.setOrderColumn("CREATETIME"); //创建时间
        sysFileMessageBean.setOrderType("DESC"); //排序类型
        SysFileMessageBean sysFile = sysFileService.getSysFileById(sysFileMessageBean);
        //会员信息
        MemberInfoMessageBean memberInfoMessageBean = new MemberInfoMessageBean();
        memberInfoMessageBean.setId(memberId);
        memberInfoMessageBean = allMemberService.queryOnlyMember(memberInfoMessageBean);
        productAttornDetailsMessageBean.setNickname(memberInfoMessageBean.getNickname());
        productAttornDetailsMessageBean.setPoints(memberInfoMessageBean.getPoints());
        //竞拍记录
        List<ProductAttornDetailsMes> list = productAttornDetailsService.queryAuctionHistory(productAttornDetailsMessageBean.getProductid());

        modelMap.put("productAttornDetailsMessageBean", productAttornDetailsMessageBean);
        modelMap.put("sysFile", sysFile);
        modelMap.put("productAttornDetailsMesList", list);


        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/product/product_attorn_details.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 获取总页数
     * @param pageSize
     * @param totalCount
     * @return
     */
    public int getPageCount(String pageSize, int totalCount) {
        int totalPage = 0; //总页数
        int pageSizein = 0; //每页显示数量(整型)

        if (pageSize != null && !pageSize.equals("")){
            pageSizein = TypeTool.getInt(pageSize);
        }

        if (totalCount % pageSizein == 0) {
            totalPage = totalCount / pageSizein;
        } else {
            totalPage = totalCount / pageSizein + 1;
        }

        return totalPage;
    }
}
