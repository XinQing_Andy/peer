package com.mh.peer.controller.product;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.ProductTypeMessage;
import com.mh.peer.service.product.ProductTypeService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.StringWriter;
import java.util.List;

/**
 * Created by Cuibin on 2016/4/29.
 */
@Controller
@RequestMapping("/productType")
public class ProductTypeController {
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ProductTypeService productTypeService;

    @RequestMapping(value = "/queryProductTypeByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryProductTypeByPage(@ModelAttribute ProductTypeMessage productTypeMessage, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = productTypeMessage.getPageIndex(); //当前数量
        String pageSize = productTypeMessage.getPageSize(); //每页显示数量
        String currentPage = productTypeMessage.getCurrentPage(); //当前页
        String title = productTypeMessage.getTitle(); //题目

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
        }
        if(title != null && !title.equals("")){ //题目
            modelMap.addAttribute("title",title);
        }

        productTypeMessage.setPageIndex(pageIndex); //当前页
        productTypeMessage.setPageSize(pageSize); //每页显示数量
        List<ProductTypeMessage> productTypeList = productTypeService.queryAllProductType(productTypeMessage);
        int totalCount = productTypeService.queryAllProductTypeCount(productTypeMessage);
        String pageStr = this.getShowPage(currentPage,totalCount); //显示所属分页

        modelMap.put("productTypeList", productTypeList); //产品集合
        modelMap.put("totalCount", totalCount); //产品数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("product/productType.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 删除产品类别
     */
    @RequestMapping(value = "/deleteProductType", method = RequestMethod.POST)
    @ResponseBody
    public ProductTypeMessage deleteProductType(@ModelAttribute ProductTypeMessage productTypeMessage){
        return productTypeService.deleteProductType(productTypeMessage);
    }
    /**
     * 添加产品类别
     */
    @RequestMapping(value = "/addProductType", method = RequestMethod.POST)
    @ResponseBody
    public ProductTypeMessage addProductType(@ModelAttribute ProductTypeMessage productTypeMessage){
        return productTypeService.addProductType(productTypeMessage);
    }
    /**
     * 更新产品类别
     */
    @RequestMapping(value = "/updateProductType", method = RequestMethod.POST)
    @ResponseBody
    public ProductTypeMessage updateProductType(@ModelAttribute ProductTypeMessage productTypeMessage){
        return productTypeService.updateProductType(productTypeMessage);
    }

    public String getShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";
        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"productdoGoPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }
}
