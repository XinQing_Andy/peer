package com.mh.peer.controller.fundSetting;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.message.*;
import com.mh.peer.service.fundSetting.FundSettingService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.StringWriter;
import java.util.List;

/**
 * Created by Cuibin on 2016/4/12.
 */
@Controller
@RequestMapping("/finance")
public class FundSettingController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FundSettingController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private FundSettingService fundSettingService;

    /**
     * 按会员等级费用维护 查询所有
     */
    @RequestMapping(value = "/openMemberFeeLevel", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openMemberFeeLevel(@ModelAttribute Paging paging, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = paging.getPageIndex(); //当前数量
        String pageSize = paging.getPageSize(); //每页显示数量
        String currentPage = paging.getCurrentPage(); //当前页
        String title = paging.getTitle(); //题目

        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
        }

        if(title != null && !title.equals("")){ //题目
            modelMap.put("title",title);
        }

        paging.setPageIndex(pageIndex); //当前页
        paging.setPageSize(pageSize); //每页显示数量
        paging = fundSettingService.queryAllMemberFeeLevel(paging);
        String pageStr = this.getShowPage(currentPage,paging.getCount()); //显示所属分页

        modelMap.put("MemberFeeLevelList", paging.getList()); //数据集合
        modelMap.put("totalCount", paging.getCount()); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页
        String jetx = paging.getMenuUrl() == null ? paging.getJetxName() : paging.getMenuUrl();


        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/system/system_spend.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 按会员等级费用维护  添加
     */
    @RequestMapping(value = "/addMemberFeeLevel", method = RequestMethod.POST)
    @ResponseBody
    public MemberFeeLevelMessageBean addMemberFeeLevel(@ModelAttribute MemberFeeLevelMessageBean MemberFeeM) {
        return fundSettingService.addMemberFeeLevel(MemberFeeM);
    }

    /**
     * 按会员等级费用维护 查询一个
     */
    @RequestMapping(value = "/queryOnlyMemberFeeLevel", method = RequestMethod.POST)
    @ResponseBody
    public MemberFeeLevelMessageBean queryOnlyMemberFeeLevel(@ModelAttribute MemberFeeLevelMessageBean MemberFeeM) {
        return fundSettingService.queryOnlyMemberFeeLevel(MemberFeeM);
    }

    /**
     * 更新按会员等级费用维护
     */
    @RequestMapping(value = "/updateMemberFeeLevel", method = RequestMethod.POST)
    @ResponseBody
    public MemberFeeLevelMessageBean updateMemberFeeLevel(@ModelAttribute MemberFeeLevelMessageBean MemberFeeM) {
        return fundSettingService.updateMemberFeeLevel(MemberFeeM);
    }

    /**
     * 按会员等级费用维护 删除
     */
    @RequestMapping(value = "/deleteMemberFeeLevel", method = RequestMethod.POST)
    @ResponseBody
    public MemberFeeLevelMessageBean deleteMemberFeeLevel(@ModelAttribute MemberFeeLevelMessageBean MemberFeeM) {
        return fundSettingService.deleteMemberFeeLevel(MemberFeeM);
    }

    /**
     * 查询所有大字典
     */
    @RequestMapping(value = "/queryAllDictionary", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAllDictionary(@ModelAttribute Paging paging, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = paging.getPageIndex(); //当前数量
        String pageSize = paging.getPageSize(); //每页显示数量
        String currentPage = paging.getCurrentPage(); //当前页
        String title = paging.getTitle(); //题目
        //System.out.println("=================="+paging.getPageIndex());
        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
        }

        if(title != null && !title.equals("")){ //题目
            modelMap.put("title",title);
        }

        paging.setPageIndex(pageIndex); //当前页
        paging.setPageSize(pageSize); //每页显示数量
        paging = fundSettingService.queryAllDictionary(paging);
        String pageStr = this.getShowPageda(currentPage, paging.getCount()); //显示所属分页

        modelMap.put("SysDictionaryList", paging.getList()); //数据集合
        modelMap.put("totalCount", paging.getCount()); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("system/system_dictionary.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 大字典添加
     */
    @RequestMapping(value = "/addDictionary", method = RequestMethod.POST)
    @ResponseBody
    public SysDictionaryMessageBean addDictionary(@ModelAttribute SysDictionaryMessageBean sysDM) {
        return fundSettingService.addDictionary(sysDM);
    }

    /**
     * 大字典删除
     */
    @RequestMapping(value = "/deleteDictionary", method = RequestMethod.POST)
    @ResponseBody
    public SysDictionaryMessageBean deleteDictionary(@ModelAttribute SysDictionaryMessageBean sysDM) {
        return fundSettingService.deleteDictionary(sysDM);
    }

    /**
     * 大字典查询一个
     */
    @RequestMapping(value = "/queryOnlyDictionary", method = RequestMethod.POST)
    @ResponseBody
    public SysDictionaryMessageBean queryOnlyDictionary(@ModelAttribute SysDictionaryMessageBean sysDM) {
        return fundSettingService.queryOnlyDictionary(sysDM);
    }

    /**
     * 修改大字典
     */
    @RequestMapping(value = "/updateDictionary", method = RequestMethod.POST)
    @ResponseBody
    public SysDictionaryMessageBean updateDictionary(@ModelAttribute SysDictionaryMessageBean sysDM) {
        return fundSettingService.updateDictionary(sysDM);
    }

    /**
     * 查询所有会员费用关联
     */
    @RequestMapping(value = "/queryAllMemberFeeRelation", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAllMemberFeeRelation(@ModelAttribute Paging paging, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = paging.getPageIndex(); //当前数量
        String pageSize = paging.getPageSize(); //每页显示数量
        String currentPage = paging.getCurrentPage(); //当前页
        String title = paging.getTitle(); //题目
        //System.out.println("=================="+paging.getPageIndex());
        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
        }

        if(title != null && !title.equals("")){ //题目
            modelMap.put("title",title);
        }

        paging.setPageIndex(pageIndex); //当前页
        paging.setPageSize(pageSize); //每页显示数量
        paging = fundSettingService.queryAllMemberFeeRelation(paging);
        List fywh = fundSettingService.queryAllMemberFeeLevel2();//按会员等级费用维护
        List djwh = fundSettingService.queryAllVipMaintain2();//vip等级维护

        String pageStr = this.getassShowPage(currentPage, paging.getCount()); //显示所属分页

        modelMap.put("queryAllMemberFeeRelationList", paging.getList()); //数据集合
        modelMap.put("totalCount", paging.getCount()); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页

        modelMap.put("fywh", fywh); //费用维护
        modelMap.put("djwh", djwh); //等级维护

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/system/system_association.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 添加会员费用关联表
     */
    @RequestMapping(value = "/addMemberFeeRelation", method = RequestMethod.POST)
    @ResponseBody
    public MemberFeeRelationMessageBean addMemberFeeRelation(@ModelAttribute MemberFeeRelationMessageBean mfrm) {
        return fundSettingService.addMemberFeeRelation(mfrm);
    }

    /**
     * 更新会员费用关联表
     */
    @RequestMapping(value = "/updateMemberFeeRelation", method = RequestMethod.POST)
    @ResponseBody
    public MemberFeeRelationMessageBean updateMemberFeeRelation(@ModelAttribute MemberFeeRelationMessageBean mfrm) {
        return fundSettingService.updateMemberFeeRelation(mfrm);
    }

    /**
     * 会员费用关联 查询一个
     */
    @RequestMapping(value = "/queryOnlyMemberFeeRelation", method = RequestMethod.POST)
    @ResponseBody
    public MemberFeeRelationMessageBean queryOnlyMemberFeeRelation(@ModelAttribute MemberFeeRelationMessageBean mfrm) {
        return fundSettingService.queryOnlyMemberFeeRelation(mfrm);
    }
    /**
     * 会员费用关联 删除
     */
    @RequestMapping(value = "/deleteMemberFeeRelation", method = RequestMethod.POST)
    @ResponseBody
    public MemberFeeRelationMessageBean deleteMemberFeeRelation(@ModelAttribute MemberFeeRelationMessageBean mfrm) {
        return fundSettingService.deleteMemberFeeRelation(mfrm);
    }
    /**
     * 查询所有固定费用过账
     */
    @RequestMapping(value = "/queryAllSysManageAmt", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAllSysManageAmt(@ModelAttribute Paging paging, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = paging.getPageIndex(); //当前数量
        String pageSize = paging.getPageSize(); //每页显示数量
        String currentPage = paging.getCurrentPage(); //当前页
        String title = paging.getTitle(); //题目
        //System.out.println("=================="+paging.getPageIndex());
        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
        }

        if(title != null && !title.equals("")){ //题目
            modelMap.put("title",title);
        }

        paging.setPageIndex(pageIndex); //当前页
        paging.setPageSize(pageSize); //每页显示数量
        paging = fundSettingService.queryAllSysManageAmt(paging);
        String pageStr = this.getpostShowPage(currentPage, paging.getCount()); //显示所属分页

        modelMap.put("SysManageAmtMessageBeanList", paging.getList()); //数据集合
        modelMap.put("totalCount", paging.getCount()); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("system/system_post.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 添加固定费用过账
     */
    @RequestMapping(value = "/addSysManageAmt", method = RequestMethod.POST)
    @ResponseBody
    public SysManageAmtMessageBean addSysManageAmt(@ModelAttribute SysManageAmtMessageBean smam) {
        return fundSettingService.addSysManageAmt(smam);
    }

    /**
     * 固定费用过账查询一个
     */
    @RequestMapping(value = "/queryOnlySysManageAmt", method = RequestMethod.POST)
    @ResponseBody
    public SysManageAmtMessageBean queryOnlySysManageAmt(@ModelAttribute SysManageAmtMessageBean smam) {
        return fundSettingService.queryOnlySysManageAmt(smam);
    }

    /**
     * 固定费用过账删除
     */
    @RequestMapping(value = "/deleteSysManageAmt", method = RequestMethod.POST)
    @ResponseBody
    public SysManageAmtMessageBean deleteSysManageAmt(@ModelAttribute SysManageAmtMessageBean smam) {
        return fundSettingService.deleteSysManageAmt(smam);
    }

    /**
     * 更新固定费用过账
     */
    @RequestMapping(value = "/updateSysManageAmt", method = RequestMethod.POST)
    @ResponseBody
    public SysManageAmtMessageBean updateSysManageAmt(@ModelAttribute SysManageAmtMessageBean smam) {
        return fundSettingService.updateSysManageAmt(smam);
    }

    /**
     * 查询所有vip等级维护
     */
    @RequestMapping(value = "/queryAllVipMaintain", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAllVipMaintain(@ModelAttribute Paging paging, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = paging.getPageIndex(); //当前数量
        String pageSize = paging.getPageSize(); //每页显示数量
        String currentPage = paging.getCurrentPage(); //当前页
        String title = paging.getTitle(); //题目
        //System.out.println("=================="+paging.getPageIndex());
        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
        }

        if(title != null && !title.equals("")){ //题目
            modelMap.put("title",title);
        }

        paging.setPageIndex(pageIndex); //当前页
        paging.setPageSize(pageSize); //每页显示数量
        paging = fundSettingService.queryAllVipMaintain(paging);
        String pageStr = this.getVipShowPage(currentPage, paging.getCount()); //显示所属分页

        modelMap.put("MemberLevelMessageBeanList", paging.getList()); //数据集合
        modelMap.put("totalCount", paging.getCount()); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("system/system_member.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 查询一个vip等级维护
     */
    @RequestMapping(value = "/queryOnlyVipMaintain", method = RequestMethod.POST)
    @ResponseBody
    public MemberLevelMessageBean queryOnlyVipMaintain(@ModelAttribute MemberLevelMessageBean mlM) {
        return fundSettingService.queryOnlyVipMaintain(mlM);
    }

    /**
     * 更新vip等级维护
     */
    @RequestMapping(value = "/updateVipMaintain", method = RequestMethod.POST)
    @ResponseBody
    public MemberLevelMessageBean updateVipMaintain(@ModelAttribute MemberLevelMessageBean mlM) {
        return fundSettingService.updateVipMaintain(mlM);
    }

    /**
     * 添加vip等级维护
     */
    @RequestMapping(value = "/addVipMaintain", method = RequestMethod.POST)
    @ResponseBody
    public MemberLevelMessageBean addVipMaintain(@ModelAttribute MemberLevelMessageBean mlM) {
        return fundSettingService.addVipMaintain(mlM);
    }

    /**
     * 固定费用过账 删除
     */
    @RequestMapping(value = "/deleteVipMaintain", method = RequestMethod.POST)
    @ResponseBody
    public MemberLevelMessageBean deleteVipMaintain(@ModelAttribute MemberLevelMessageBean mlM) {
        return fundSettingService.deleteVipMaintain(mlM);
    }

    /**
     * 查询所有取号原则
     */
    @RequestMapping(value = "/queryAllTakeNo", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAllTakeNo(@ModelAttribute Paging paging, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = paging.getPageIndex(); //当前数量
        String pageSize = paging.getPageSize(); //每页显示数量
        String currentPage = paging.getCurrentPage(); //当前页
        String title = paging.getTitle(); //题目
        //System.out.println("=================="+paging.getPageIndex());
        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10"; //每页显示数量
        }

        if(title != null && !title.equals("")){ //题目
            modelMap.put("title",title);
        }

        paging.setPageIndex(pageIndex); //当前页
        paging.setPageSize(pageSize); //每页显示数量
        paging = fundSettingService.queryAllTakeNo(paging);
        String pageStr = this.getnoShowPage(currentPage, paging.getCount()); //显示所属分页

        modelMap.put("TakeNoList", paging.getList()); //数据集合
        modelMap.put("totalCount", paging.getCount()); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("system/system_number.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 添加取号原则
     */
    @RequestMapping(value = "/addTakeNo", method = RequestMethod.POST)
    @ResponseBody
    public SysTakeNoMessageBean addTakeNo(@ModelAttribute SysTakeNoMessageBean stM) {
        return fundSettingService.addTakeNo(stM);
    }

    /**
     * 更新取号原则
     */
    @RequestMapping(value = "/updateTakeNo", method = RequestMethod.POST)
    @ResponseBody
    public SysTakeNoMessageBean updateTakeNo(@ModelAttribute SysTakeNoMessageBean stM) {
        return fundSettingService.updateTakeNo(stM);
    }

    /**
     * 查询一个取号原则
     */
    @RequestMapping(value = "/queryOnlyTakeNo", method = RequestMethod.POST)
    @ResponseBody
    public SysTakeNoMessageBean queryOnlyTakeNo(@ModelAttribute SysTakeNoMessageBean mlM) {
        return fundSettingService.queryOnlyTakeNo(mlM);
    }

    /**
     * 固定费用过账删除
     */
    @RequestMapping(value = "/deleteTakeNo", method = RequestMethod.POST)
    @ResponseBody
    public SysTakeNoMessageBean deleteTakeNo(@ModelAttribute SysTakeNoMessageBean mlM) {
        return fundSettingService.deleteTakeNo(mlM);
    }
    /**
     * 获取需显示数字页码 大字典
     */
    public String getShowPageda(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGodaPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }


    /**
     * 获取需显示数字页码
     */
    public String getShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGoPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }



    /**
     * 获取需显示数字页码 会员等级维护
     */
    public String getVipShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGovipPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }


    /**
     * 获取需显示数字页码 取号原则
     */
    public String getnoShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";
        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGonoPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }


    /**
     * 获取需显示数字页码 固定费用过账
     */
    public String getpostShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";
        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGopostPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }

    /**
     * 获取需显示数字页码 会员费用关联
     */
    public String getassShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";
        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGoassPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }
}
