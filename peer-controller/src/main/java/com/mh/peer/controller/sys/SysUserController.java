package com.mh.peer.controller.sys;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.business.SysRoleBusinessBean;
import com.mh.peer.model.business.UserBusinessBean;
import com.mh.peer.model.entity.SysDept;
import com.mh.peer.model.entity.SysRole;
import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.MenuBeanMessage;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.mh.peer.model.message.SysUserMessageBean;
import com.mh.peer.service.sys.SysRoleService;
import com.mh.peer.service.sys.SysUserService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpSession;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016/1/27.
 */
@Controller
@RequestMapping("/manage")
public class SysUserController {
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 查询用户
     * @return
     */
   /* @RequestMapping(value = "/queryUser", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryUser(@ModelAttribute MenuBeanMessage menuBeanMessage, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        List<SysUserMessageBean> userList = sysUserService.getUserInfo();
        //System.out.println("userList == " + userList);
        //将list传递给前台渲染
        modelMap.put("userList", userList);
        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(menuBeanMessage.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        //System.out.println("返回参数=="+bean);
        return bean;
    }*/

    /**
     * 根据分页获取用户信息
     * @param sysUserMese
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryUserByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryUserByPage(@ModelAttribute SysUserMessageBean sysUserMese, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = sysUserMese.getPageIndex(); //当前数量
        String pageSize = sysUserMese.getPageSize(); //每页显示数量
        String currentPage = sysUserMese.getCurrentPage(); //当前页
        int pageCount = 0; //总页数
        String sysKeyWord = sysUserMese.getSysKeyWord(); //关键字

        if(pageIndex == null || pageIndex.equals("")){
            pageIndex = "1";
            currentPage = "1";
        }
        if(pageSize == null || pageSize.equals("")){
            pageSize = "10";
        }
        if(sysKeyWord != null && !sysKeyWord.equals("")){ //关键字
            modelMap.addAttribute("sysKeyWord",sysKeyWord);
        }

        sysUserMese.setPageIndex(pageIndex); //当前页
        sysUserMese.setPageSize(pageSize); //每页显示数量

        List<SysUserMessageBean> userList = sysUserService.getUserInfoByPage(sysUserMese); //用户集合(根据分页查询)
        int totalCount = sysUserService.getCountUserInfoByPage(sysUserMese); //用户数量
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量
        String pageStr = this.getShowPage(currentPage,totalCount); //显示所属分页

        modelMap.put("userList", userList); //用户集合
        modelMap.put("totalCount", totalCount); //用户数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页
        modelMap.put("pageCount",pageCount); //总页数

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(sysUserMese.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 获取所有角色(添加部分)
     * @param roleMes
     * @return
     */
    @RequestMapping(value = "/querySetRoleList", method = RequestMethod.POST)
    @ResponseBody
    public SysRoleMessageBean querySetRoleList(@ModelAttribute SysRoleMessageBean roleMes){
        List<SysRoleMessageBean> sysRoleMess = new ArrayList<SysRoleMessageBean>(); //角色
        sysRoleMess = sysRoleService.getRoleInfoByPage(roleMes); //角色集合
        int totalCount = sysRoleService.getCountSysRoleByPage(roleMes); //角色数量
        String pageIndex = roleMes.getPageIndex(); //当前数
        String pageSize = roleMes.getPageSize(); //每页显示数量
        String currentPage = roleMes.getCurrentPage(); //当前页
        String roleStr = ""; //角色拼接

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
        }

        roleMes.setPageIndex(pageIndex); //当前数
        roleMes.setPageSize(pageSize); //每页显示数量
        String pageRoleStr = this.getShowRolePage(currentPage, totalCount); //显示所属分页

        if(sysRoleMess != null && sysRoleMess.size() > 0){
            for (SysRoleMessageBean sysRoleMessageBean : sysRoleMess){
                roleStr = roleStr + "<tr>"
                        + "<td><input type=\"radio\" name=\"chooseAdd\" id=\"chooseAdd\" onclick=\"doCheck('"+sysRoleMessageBean.getId()+"','"+sysRoleMessageBean.getRoleName()+"','add')\"/></td>"
                        + "<td>"+sysRoleMessageBean.getNum()+"</td>"
                        + "<td>"+sysRoleMessageBean.getRoleName()+"</td>"
                        + "<td>"+sysRoleMessageBean.getRoleDescription()+"</td>"
                        + "</tr>";
            }
        }

        roleMes.setHtmlText(roleStr); //角色集合拼接
        roleMes.setTotalCount(TypeTool.getString(totalCount)); //角色数量
        roleMes.setCurrentPage(currentPage); //当前页
        roleMes.setRoleStr(pageRoleStr); //显示所属分页

        return roleMes;
    }


    /**
     * 获取所有角色(编辑部分)
     * @param roleMes
     * @return
     */
    @RequestMapping(value = "/querySetEditRoleList", method = RequestMethod.POST)
    @ResponseBody
    public SysRoleMessageBean querySetEditRoleList(@ModelAttribute SysRoleMessageBean roleMes){
        List<SysRoleMessageBean> sysRoleMess = new ArrayList<SysRoleMessageBean>(); //角色
        sysRoleMess = sysRoleService.getRoleInfoByPage(roleMes); //角色集合
        int totalCount = sysRoleService.getCountSysRoleByPage(roleMes); //角色数量
        String pageIndex = roleMes.getPageIndex(); //当前数
        String pageSize = roleMes.getPageSize(); //每页显示数量
        String currentPage = roleMes.getCurrentPage(); //当前页
        String roleStr = ""; //角色拼接
        String roleId = roleMes.getId(); //角色id
        String radio = ""; //是否被选中

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
        }

        roleMes.setPageIndex(pageIndex); //当前数
        roleMes.setPageSize(pageSize); //每页显示数量
        String pageRoleStr = this.getShowRolePage(currentPage, totalCount); //显示所属分页

        if(sysRoleMess != null && sysRoleMess.size() > 0){
            for (SysRoleMessageBean sysRoleMessageBean : sysRoleMess){
                if(roleId.equals(sysRoleMessageBean.getId())){
                    radio = "check";
                }else{
                    radio = "";
                }
                roleStr = roleStr + "<tr>"
                        + "<td><input type=\"radio\" name=\"chooseAdd\" id=\"chooseAdd\" onclick=\"doCheck('"+sysRoleMessageBean.getId()+"','"+sysRoleMessageBean.getRoleName()+"','edit')\" "+radio+"/></td>"
                        + "<td>"+sysRoleMessageBean.getNum()+"</td>"
                        + "<td>"+sysRoleMessageBean.getRoleName()+"</td>"
                        + "<td>"+sysRoleMessageBean.getRoleDescription()+"</td>"
                        + "</tr>";
            }
        }

        roleMes.setHtmlText(roleStr);
        roleMes.setTotalCount(TypeTool.getString(totalCount)); //角色数量
        roleMes.setCurrentPage(currentPage); //当前页
        roleMes.setRoleStr(pageRoleStr); //显示所属分页
        return roleMes;
    }

    /**
     * 根据分页获取用户信息
     * @param sysUserMese
     * @param modelMap
     * @return
     */
//    @RequestMapping(value = "/queryUserByPage", method = RequestMethod.POST)
//    @ResponseBody
//    public JetbrickBean queryRoleByPage(@ModelAttribute SysUserMessageBean sysUserMese, ModelMap modelMap){
//        JetbrickBean bean = new JetbrickBean();
//        String pageIndex = sysUserMese.getPageIndex(); //当前页
//        String pageSize = sysUserMese.getPageSize(); //当前页
//        if(pageIndex == null || pageIndex.equals("")){
//            pageIndex = "1";
//        }
//        if(pageSize == null || pageSize.equals("")){
//            pageSize = "10";
//        }
//        sysUserMese.setPageIndex(pageIndex); //当前页
//        sysUserMese.setPageSize(pageSize); //每页显示数量
//        List<SysUserMessageBean> userList = sysUserService.getUserInfoByPage(sysUserMese); //用户集合(根据分页查询)
//        //将list传递给前台渲染
//        modelMap.put("userList", userList); //用户集合
//
//        //模板解析方法
//        try {
//            //模板渲染
//            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(sysUserMese.getMenuUrl(), modelMap);
//            bean.setHtmlText(stringWriter.toString());
//            bean.setInfo("成功");
//            bean.setResult("success");
//        }catch (Exception e){
//            e.printStackTrace();
//            bean.setInfo("失败");
//            bean.setResult("error");
//            bean.setHtmlText("");
//        }
//
//        return bean;
//    }

    /**
     * js下拉菜单获得所有启用的用户组
     */
    /*@RequestMapping(value = "/getDeptInfo", method = RequestMethod.POST)
    @ResponseBody
    public List<SysDept> getUserDept(){
        return sysUserService.getDeptRole();
    }*/

    /**
     * js下拉菜单获得所有启用的用户组
     */
   /* @RequestMapping(value = "/getRoleInfo", method = RequestMethod.POST)
    @ResponseBody
    public List<SysRole> getUserRole(){
        return sysUserService.getSysRole();
    }*/

    /**
     * 新增用户
     * @return
     */
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @ResponseBody
    public SysUserMessageBean onAddUser(@ModelAttribute SysUserMessageBean user){
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        String userName= bean.getSysUser().getUserName();
        user.setCreateUserId(userName);
        user.setPassWord("111111"); //密码
        user.setDelFlg("0"); //是否删除
        user.setIsOnline("0");
        if(sysUserService.checkUser(user) > 0){
            user.setResult("error");
            user.setInfo("该用户已存在，请核对后重新添加！");
            return user;
        }
        LoginBusinessBean loginBusinessBean = sysUserService.addUser(user); //增加用户
        user.setResult(loginBusinessBean.getResult());
        user.setInfo(loginBusinessBean.getInfo());
        return user;
    }

    /**
     * 删除用户
     * @param user
     * @return
     */
   /* @RequestMapping(value = "/delUser", method = RequestMethod.POST)
    @ResponseBody
    public SysUserMessageBean onDelUser(@ModelAttribute SysUserMessageBean user){
        //System.out.println("user == " + user);
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        String userName= bean.getSysUser().getUserName();
        user.setCreateUserId(userName);
        LoginBusinessBean loginBusinessBean = sysUserService.delUser(user);
        user.setResult(loginBusinessBean.getResult());
        user.setInfo(loginBusinessBean.getInfo());
        return user;
    }*/

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    @ResponseBody
    public SysUserMessageBean onUpUser(@ModelAttribute SysUserMessageBean user){
        HttpSession session = servletUtil.getSession();
        LoginBusinessBean bean = (LoginBusinessBean)session.getAttribute("loginBusinessBean");
        /*String userName= bean.getSysUser().getUserName();
        user.setCreateUserId(userName);*/
        if(sysUserService.checkUser(user) > 0){
            user.setResult("error");
            user.setInfo("该用户已存在，请核对后重新添加！");
            return user;
        }
        LoginBusinessBean loginBusinessBean = sysUserService.updateUser(user);
        user.setResult(loginBusinessBean.getResult());
        user.setInfo(loginBusinessBean.getInfo());
        return user;
    }

    /**
     * 根据id查询用户信息
     * @param sysUserMese
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryUserById", method = RequestMethod.POST)
    @ResponseBody
    public SysUserMessageBean queryUserById(@ModelAttribute SysUserMessageBean sysUserMese, ModelMap modelMap){
        SysUserMessageBean sysUserMes = sysUserService.queryUserById(sysUserMese); //用户集合(根据id查询)
        return sysUserMes;
    }

    /**
     * 设置用户状态(zhangerxin)
     * @param user
     * @return
     */
    @RequestMapping(value = "/settingSysFlag", method = RequestMethod.POST)
    @ResponseBody
    public SysUserMessageBean settingSysFlag(@ModelAttribute SysUserMessageBean user){
        LoginBusinessBean loginBusinessBean = sysUserService.settingSysFlag(user);
        user.setResult(loginBusinessBean.getResult());
        user.setInfo(loginBusinessBean.getInfo());
        return user;
    }

    /**
     * 重置密码(zhangerxin)
     * @param user
     * @return
     */
    @RequestMapping(value = "/resetPwd", method = RequestMethod.POST)
    @ResponseBody
    public SysUserMessageBean resetPwd(@ModelAttribute SysUserMessageBean user){
        LoginBusinessBean loginBusinessBean = sysUserService.resetPwd(user);
        user.setResult(loginBusinessBean.getResult());
        user.setInfo(loginBusinessBean.getInfo());
        return user;
    }


    /**
     * 获取总页数
     * @param pageSize
     * @param totalCount
     * @return
     */
    public int getPageCount(String pageSize, int totalCount) {
        int totalPage = 0; //总页数
        int pageSizein = 0; //每页显示数量(整型)

        if (pageSize != null && !pageSize.equals("")){
            pageSizein = TypeTool.getInt(pageSize);
        }

        if (totalCount % pageSizein == 0) {
            totalPage = totalCount / pageSizein;
        } else {
            totalPage = totalCount / pageSizein + 1;
        }

        return totalPage;
    }

    /**
     * 获取需显示数字页码(zhangerxin)
     * @param currentPage
     * @param totalCount
     * @return
     */
    public String getShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 < 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = 10 -(totalPage - currentin + 1);
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                      + "<a href=\"JavaScript:void(0)\" onclick=\"doSysGoPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }


    /**
     * 获取需显示数字页码(角色部分)
     * @param currentPage
     * @param totalCount
     * @return
     */
    public String getShowRolePage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 < 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = 10 -(totalPage - currentin + 1);
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doSysUserRoleGoPage('"+i+"','add')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }
}
