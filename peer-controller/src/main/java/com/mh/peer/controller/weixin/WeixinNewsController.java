package com.mh.peer.controller.weixin;

import com.mh.peer.model.message.WeixinNewsMes;
import com.mh.peer.service.news.NewsService;
import com.mh.peer.util.JetbrickTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Cuibin on 2016/7/8.
 */

@Controller
@RequestMapping("/weixin")
public class WeixinNewsController {

    @Autowired
    private NewsService newsService;
    @Autowired
    private JetbrickTool jetbrickTool;

    /**
     * 获取新闻列表
     */
    @RequestMapping(value = "/queryNewsList", method = RequestMethod.GET)
    public String queryNewsList(ModelMap modelMap) {
        WeixinNewsMes weixinNewsMes = new WeixinNewsMes();
        List<WeixinNewsMes> list = newsService.queryNewsList(weixinNewsMes);
        String count = newsService.queryNewsCount(weixinNewsMes);

        modelMap.put("weixinNewsMesList", list);
        modelMap.put("count", count);

        return "weixin/weixin_news";
    }

    /**
     * 查询一个新闻
     */
    @RequestMapping(value = "/queryOnlyNews", method = RequestMethod.GET)
    public String queryOnlyNews(HttpServletRequest request, ModelMap modelMap) {

        String id = request.getParameter("id");
        WeixinNewsMes weixinNewsMes = newsService.queryOnlyNews(id);
        modelMap.put("weixinNewsMes", weixinNewsMes);

        return "weixin/weixin_news_details";
    }


    /**
     * 查询更多新闻
     */
    @RequestMapping(value = "/queryMoreNews", method = RequestMethod.POST)
    @ResponseBody
    public WeixinNewsMes queryMoreNews(@ModelAttribute WeixinNewsMes weixinNewsMes) {
        List<WeixinNewsMes> list = newsService.queryNewsList(weixinNewsMes);
        String count = newsService.queryNewsCount(weixinNewsMes);

        String str = "";
        for (WeixinNewsMes temp : list) {
            String img = temp.getImg().equals("") || temp.getImg() == null ? "../wxImg/zwtp.png" : temp.getImg();
            str += "<a onclick=\"queryOnlyNews('" + temp.getId() + "')\">" +
                    "<li>" +
                    "<table class=\"message_table\">" +
                    "<tr>" +
                    "<td colspan=\"3\" class=\"mess_title\">" + temp.getTitle() + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>" +
                    "<img src=\"" + img + "\" style=\"width: 70px; height: 50px;\" />" +
                    "</td>" +
                    "<td class=\"mess_txt\" style=\"width: 100%\">" +
                    "<span>" + temp.getContent() + "</span>" +
                    "<span style=\"line-height: 35px;\">" + temp.getShowDate() + "</span>" +
                    "</td>" +
                    "<td>" +
                    "<span class=\"icon-app-05\"></span>" +
                    "</td>" +
                    "</tr>" +
                    "</table>" +
                    "</li>" +
                    "</a>" +
                    "<div style=\"background-color:#ECECEC;width:auto;height:1px\"></div>";
        }

        WeixinNewsMes result = new WeixinNewsMes();
        if (result != null && !result.equals("")) {
            result.setHtmlText(str);
            result.setCount(count);
            result.setResult("success");
        } else {
            result.setResult("error");
            result.setInfo("查询失败");
        }


        return result;
    }
}
