package com.mh.peer.controller.ckupload;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.business.ProductMyAttornBusinessBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.huanxun.HuanXunFreezeService;
import com.mh.peer.service.huanxun.HuanXunRegProjectService;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.product.ProductAttornService;
import com.mh.peer.service.product.ProductBuyService;
import com.mh.peer.service.product.ProductTypeService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.*;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONObject;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-5-23.
 * ckEditor图片上传
 */
@Controller
@RequestMapping("/ckupload")
public class CkEditorController {

    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ServletUtil servletUtil;

    /**
     * 图片上传
     * @param upload
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value="/imageUpload", method = RequestMethod.POST)
    @ResponseBody
    public void imageComPressUpload1(@RequestParam MultipartFile[] upload, HttpServletRequest request, HttpServletResponse response) throws IOException{
        String ctxPath = request.getRealPath("/") + "image/CkEditor"; //绝对路径
        String ctxPath2 = "/image/CkEditor";
        String uuid = ""; //新附件名称(不带后缀)
        String newFileName = ""; //新文件名称
        String suffix = ""; //后缀
        String serverAddress = ""; //服务器地址
        //设置响应给前台内容的数据格式
        response.setContentType("text/html; charset=UTF-8");
        //设置响应给前台内容的PrintWriter对象
        PrintWriter out = response.getWriter();
        //上传文件的原名(即上传前的文件名字)
        String originalFilename = null;
        if (upload.length == 0){
            out.print("[1图片上传失败]");
            out.flush();
        }

        // 返回路径
        String newFilePath = "";
        for(MultipartFile myfile : upload){
            if(myfile.isEmpty()){
                out.print("[1请选择文件后上传]");
                out.flush();
            }else{
                originalFilename = myfile.getOriginalFilename(); //文件名称
                suffix = originalFilename.indexOf(".") != -1 ? originalFilename.substring(originalFilename.lastIndexOf("."), originalFilename.length()) : null;
                suffix = suffix.toUpperCase(); //转换为大写
                if(suffix != null && !suffix.equals("")){
                    uuid = UUID.randomUUID().toString().replaceAll("\\-", ""); //图片主键
                    newFileName = uuid + (suffix != null ? suffix : ""); //构成新文件名。
                    try {

                        PublicAddress publicAddress = new PublicAddress();
                        serverAddress = publicAddress.getServerAddress();
                        newFilePath = serverAddress + ctxPath2 + "/" + newFileName;
                        FileUtils.copyInputStreamToFile(myfile.getInputStream(), new File(ctxPath, newFileName));
                    } catch (IOException e) {
                        System.out.println("文件[" + originalFilename + "]上传失败,堆栈轨迹如下");
                        e.printStackTrace();
                        out.print("[1图片上传失败，请重试！！]");
                        out.flush();
                    }
                }


            }
        }
        String s = "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(1,'"+newFilePath+"');</script>";
        out.print(s);
        out.flush();
    }


    /**
     * 创建文件
     * @param path
     * @return
     */
    private String getFilePath(String path){
        Calendar date=Calendar.getInstance();
        SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd");
        String name=format.format(date.getTime());
        // 创建年文件夹
        String yearPath = name.substring(0,4);
        String month = name.substring(4,6);
        String day = name.substring(6,8);
        File file = new File(path+"/"+yearPath+"/"+month+"/"+day);
        if (!file.exists())
            file.mkdirs();
        return file.getPath();
    }


    /**
     * 创建文件
     * @param path
     * @return
     */
    private String[] getFilePathAritcle(String path){
        Calendar date=Calendar.getInstance();
        SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd");
        String name=format.format(date.getTime());
        // 创建年文件夹
        String yearPath = name.substring(0,4);
        String month = name.substring(4,6);
        String day = name.substring(6,8);
        File file = new File(path+"/"+yearPath+"/"+month+"/"+day);
        String[] result = new String[2];
        result[0]=servletUtil.getPropertiesFileVale("System.properties", "TXTPATH", "UPLOADURL")+servletUtil.getPropertiesFileVale("System.properties", "IMAGEPATH", "REALPATH")+"/"+yearPath+"/"+month+"/"+day;
        result[1]=file.getPath();
        if (!file.exists())
            file.mkdirs();
        return result;
    }

    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot >-1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return filename;
    }
}
