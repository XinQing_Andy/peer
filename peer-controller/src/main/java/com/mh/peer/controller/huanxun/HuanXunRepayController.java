package com.mh.peer.controller.huanxun;

import com.mh.peer.exception.HuanXunFreezeRepayException;
import com.mh.peer.exception.HuanXunTransferRepayException;
import com.mh.peer.model.entity.HuanxunFreezeRepay;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.mh.peer.model.message.HuanXunRepayMessageBean;
import com.mh.peer.model.message.HuanXunTransferMessage;
import com.mh.peer.service.huanxun.HuanXunFreezeService;
import com.mh.peer.service.huanxun.HuanXunRechargeService;
import com.mh.peer.service.huanxun.HuanXunRepayService;
import com.mh.peer.service.huanxun.HuanXunTransferService;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created by zhangerxin on 2016-8-6.
 * 环迅还款部分
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunRepayController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HuanXunFreezeController.class);
    @Autowired
    private HuanXunRepayService huanXunRepayService;
    @Autowired
    private HuanXunFreezeService huanXunFreezeService;
    @Autowired
    private HuanXunTransferService huanXunTransferService;

    /**
     * 环迅自动还款冻结部分返回结果
     * @param request
     * @param response
     * @param modelMap
     */
    @RequestMapping(value = "/repayhxResult", method = RequestMethod.POST)
    @ResponseBody
    public void repayhxResult(HttpServletRequest request,HttpServletResponse response, ModelMap modelMap) {
        System.out.println("还款冻结后台部分：");
        String resultCode = request.getParameter("resultCode"); //响应吗(000000-成功、1-000001~999999)
        String resultMsg = request.getParameter("resultMsg"); //响应信息
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String responsehx = request.getParameter("response"); //响应信息
        String responsehxJm = ""; //响应信息(解密)
        String repayId = request.getParameter("repayId"); //还款id
        String repayer = ""; //还款人id
        String balance = ""; //余额(String型)
        Double balanceDou = 0.0d; //余额(Double型)
        String merBillNo = ""; //商户订单号
        String projectNo = ""; //项目id号
        String ipsTrdAmt = ""; //ips冻结金额(String型)
        Double ipsTrdAmtDou = 0.0d; //ips冻结金额(Double型)
        String ipsAcctNo = ""; //冻结账号
        String otherIpsAcctNo = ""; //它方账号
        String ipsBillNo = ""; //ips订单号
        String ipsDoTime = ""; //ips处理时间
        String trdStatus = ""; //冻结状态(0-失败、1-成功)
        HuanXunRepayMessageBean huanXunRepayMessageBean = new HuanXunRepayMessageBean();
        List<HuanXunRepayMessageBean> listHuanXunRepayMes = new ArrayList<HuanXunRepayMessageBean>(); //还款部分
        Map<String,String> map = new HashMap<String,String>();
        String UidHuanXunRepay = request.getParameter("UidHuanXunRepay"); //环迅还款主键
        String UidMyMessage = ""; //我的消息
        String title = ""; //题目
        String content = ""; //内容
        String result = "0"; //执行结果(0-成功、1-失败)
        int count = 0; //数量
        HuanxunFreezeRepay huanxunFreezeRepay = new HuanxunFreezeRepay(); //还款冻结部分
        MemberMessage memberMessage = new MemberMessage(); //我的消息
        MemberInfo memberInfo = new MemberInfo(); //会员信息
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息

        try{

            output(response, "ipsCheckOk"); //返回成功

            if (resultCode != null && !resultCode.equals("")){ //响应码
                UidMyMessage = UUID.randomUUID().toString().replaceAll("-",""); //我的消息主键

                count = huanXunRepayService.queryFreezeRepayCount(repayId);
                System.out.println("环迅还款冻结数量======"+count+",如果为0，则开始录入冻结信息");

                if (count == 0){
                    if (responsehx != null && !responsehx.equals("")){
                        responsehxJm = Pdes.decrypt3DES(responsehx); //响应信息解密
                        System.out.println("还款解密后信息为：");
                        System.out.println("responsehxJm======"+responsehxJm);

                        if (responsehxJm != null && !responsehxJm.equals("")){
                            map = JsonHelper.getObjectToMap(responsehxJm);
                            System.out.println("map======"+map);
                            if (map != null){
                                merBillNo = map.get("merBillNo"); //商户订单号
                                projectNo = map.get("projectNo"); //项目id号
                                ipsTrdAmt = map.get("ipsTrdAmt"); //IPS冻结金额(String型)
                                if (ipsTrdAmt != null && !ipsTrdAmt.equals("")){
                                    ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //IPS冻结金额(Double型)
                                }
                                ipsAcctNo = map.get("ipsAcctNo"); //冻结账号
                                otherIpsAcctNo = map.get("otherIpsAcctNo"); //它方账号
                                ipsBillNo = map.get("ipsBillNo"); //ips订单号
                                ipsDoTime = map.get("ipsDoTime"); //ips处理时间
                                trdStatus = map.get("trdStatus"); //冻结状态(0-失败、1-成功)
                            }
                        }
                    }

                    if (resultCode.equals("000000")){ //成功
                        huanXunRepayMessageBean.setRepayId(repayId); //还款id
                        listHuanXunRepayMes = huanXunRepayService.queryMemberInfo(huanXunRepayMessageBean); //还款信息
                        if (listHuanXunRepayMes != null && listHuanXunRepayMes.size() > 0){
                            repayer = listHuanXunRepayMes.get(0).getRepayer(); //还款人id
                            balance = listHuanXunRepayMes.get(0).getBalance(); //余额(String型)
                            if (balance != null && !balance.equals("")){
                                balanceDou = TypeTool.getDouble(balance); //余额(Double型)
                            }
                        }
                    }

                    /*************** 还款冻结部分Strat ***************/
                    huanxunFreezeRepay.setId(UidHuanXunRepay); //环迅还款主键
                    huanxunFreezeRepay.setRepayid(repayId); //还款id
                    huanxunFreezeRepay.setMemberid(repayer); //还款人id
                    huanxunFreezeRepay.setFreezetype("3"); //冻结类型(1-投标、2-债权转让、3-还款、4-分红、5-代偿、6-代偿还款、7-风险准备金、8-结算担保收益、9-红包、10-融资方保证金、11-投资人保证金、12-担保人保证金)
                    huanxunFreezeRepay.setFlag("0"); //状态(0-冻结、1-解冻、2-转账)
                    huanxunFreezeRepay.setResultcode(resultCode); //响应吗(000000-成功、000001~999999-失败)
                    huanxunFreezeRepay.setResultmsg(resultMsg); //响应信息
                    huanxunFreezeRepay.setMerchantid(merchantID); //商户存管交易账号
                    huanxunFreezeRepay.setSign(sign); //签名
                    huanxunFreezeRepay.setMerbillno(merBillNo); //商户订单号
                    huanxunFreezeRepay.setProjectno(projectNo); //项目id号
                    huanxunFreezeRepay.setIpstrdamt(ipsTrdAmtDou); //IPS冻结金额
                    huanxunFreezeRepay.setIpsacctno(ipsAcctNo); //ips订单号
                    huanxunFreezeRepay.setOtheripsacctno(otherIpsAcctNo); //它方账号
                    huanxunFreezeRepay.setIpsbillno(ipsBillNo); //IPS订单号
                    huanxunFreezeRepay.setIpsdotime(ipsDoTime); //IPS处理时间
                    huanxunFreezeRepay.setTrdstatus(trdStatus); //冻结状态(0-失败、1-成功)
                    huanxunFreezeRepay.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                    huanxunFreezeRepay.setDelflg("1"); //是否删除(0-已删除、1-未删除);
                    /**************** 还款冻结部分End ****************/

                    /*************** 我的消息Start ***************/
                    title = "环迅还款金额冻结";
                    content = "<li>您有" + ipsTrdAmtDou + "元金额因还款被冻结，该笔钱将用于还款</li>"
                            + "<li>项目ID号：" + projectNo + "</li>"
                            + "<li>IPS冻结金额：" + ipsTrdAmtDou + "</li>"
                            + "<li>IPS订单号：" + ipsAcctNo + "</li>"
                            + "<li>它方账号：" + otherIpsAcctNo + "</li>"
                            + "<li>IPS订单号：" + ipsBillNo + "</li>"
                            + "<li>IPS处理时间：" + ipsDoTime + "</li>";

                    memberMessage.setId(UidMyMessage); //主键
                    memberMessage.setGlid(UidHuanXunRepay); //关联id
                    memberMessage.setType("0"); //类型(0-系统消息)
                    memberMessage.setMemberid(repayer); //还款人id
                    memberMessage.setReadflag("1"); //读取状态(0-已读、1-未读)
                    memberMessage.setTitle(title); //题目
                    memberMessage.setContent(content); //内容
                    memberMessage.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送时间
                    memberMessage.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                    memberMessage.setDelflg("1"); //删除标志(0-已删除、1-未删除)
                    /**************** 我的消息End ****************/

                    /*************** 会员消息Start ***************/
                    balanceDou = balanceDou - ipsTrdAmtDou; //冻结后余额
                    memberInfo.setId(repayer); //会员id
                    memberInfo.setBalance(balanceDou); //余额
                    /**************** 会员消息End ****************/

                    /*************** 异常处理部分Start ***************/
                    mapException.put("resultCode",resultCode); //响应码
                    mapException.put("resultMsg",resultMsg); //返回信息
                    mapException.put("merBillNo",merBillNo); //商户订单号
                    mapException.put("projectNo",projectNo); //项目ID号
                    mapException.put("ipsTrdAmt",ipsTrdAmt); //IPS冻结金额
                    mapException.put("ipsAcctNo",ipsAcctNo); //IPS冻结账号
                    mapException.put("otherIpsAcctNo",otherIpsAcctNo); //它方账号
                    mapException.put("ipsBillNo",ipsBillNo); //IPS订单号
                    mapException.put("ipsDoTime",ipsDoTime); //IPS处理时间
                    /**************** 异常处理部分End ****************/

                    result = huanXunRepayService.repayhxResult(huanxunFreezeRepay,memberMessage,memberInfo,mapException); //冻结结果
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 还款冻结部分返回结果
     * @param request
     * @param response
     * @param modelMap
     */
    @RequestMapping(value = "/repayPricehxResult", method = RequestMethod.POST)
    @ResponseBody
    public void repayPricehxResult(HttpServletRequest request,HttpServletResponse response, ModelMap modelMap) {
        String resultCode = request.getParameter("resultCode"); //响应吗(000000-成功、1-000001~999999)
        String resultMsg = request.getParameter("resultMsg"); //响应信息
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String responsehx = request.getParameter("response"); //响应信息
        String responsehxJm = ""; //响应信息(解密)
        String merBillNo = ""; //商户订单号
        String projectNo = ""; //项目id号
        String ipsTrdAmt = ""; //ips冻结金额(String型)
        Double ipsTrdAmtDou = 0.0d; //ips冻结金额(Double型)
        String ipsAcctNo = ""; //冻结账号
        String otherIpsAcctNo = ""; //它方账号
        String ipsBillNo = ""; //ips订单号
        String ipsDoTime = ""; //ips处理时间
        String trdStatus = ""; //冻结状态(0-失败、1-成功)
        String UidMyMessage = ""; //我的消息主键
        String repayId = request.getParameter("repayId"); //环迅还款id
        String UidHuanXunRepay = request.getParameter("UidHuanXunRepay"); //还款冻结部分主键
        int count = 0; //还款冻结数量
        String repayer = ""; //还款人id
        String balance = ""; //余额(String型)
        Double balanceDou = 0.0d; //余额(Double型)
        String title = ""; //题目
        String content = ""; //内容
        String result = "0"; //冻结结果(0-成功、1-失败)
        Map<String,String> map = new HashMap<String,String>();
        HuanXunRepayMessageBean huanXunRepayMessageBean = new HuanXunRepayMessageBean(); //还款信息
        List<HuanXunRepayMessageBean> listHuanXunRepayMes = new ArrayList<HuanXunRepayMessageBean>(); //还款信息集合
        HuanxunFreezeRepay huanxunFreezeRepay = new HuanxunFreezeRepay(); //还款冻结部分
        MemberMessage memberMessage = new MemberMessage(); //我的消息
        MemberInfo memberInfo = new MemberInfo(); //会员信息
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息

        try{

            output(response, "ipsCheckOk"); //返回成功

            if (resultCode != null && !resultCode.equals("")) { //响应码
                UidMyMessage = UUID.randomUUID().toString().replaceAll("-",""); //我的消息主键
                count = huanXunRepayService.queryFreezeRepayCount(repayId); //冻结数量
                System.out.println("根据repayId获取冻结数量，如果为0，则开始录入冻结信息");

                if (count == 0){
                    if (responsehx != null && !responsehx.equals("")){
                        responsehxJm = Pdes.decrypt3DES(responsehx); //响应信息解密
                        System.out.println("还款解密信息为："+responsehxJm);
                        if (responsehxJm != null && !responsehxJm.equals("")){
                            map = JsonHelper.getObjectToMap(responsehxJm); //将Json转换为Map
                            if (map != null){
                                merBillNo = map.get("merBillNo"); //商户订单号
                                projectNo = map.get("projectNo"); //项目id号
                                ipsTrdAmt = map.get("ipsTrdAmt"); //IPS冻结金额(String型)
                                if (ipsTrdAmt != null && !ipsTrdAmt.equals("")){
                                    ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //IPS冻结金额(Double型)
                                }
                                ipsAcctNo = map.get("ipsAcctNo"); //冻结账号
                                otherIpsAcctNo = map.get("otherIpsAcctNo"); //它方账号
                                ipsBillNo = map.get("ipsBillNo"); //ips订单号
                                ipsDoTime = map.get("ipsDoTime"); //ips处理时间
                                trdStatus = map.get("trdStatus"); //冻结状态(0-失败、1-成功)
                            }
                        }
                    }

                    if (resultCode.equals("000000")){ //成功
                        huanXunRepayMessageBean.setRepayId(repayId); //还款id
                        listHuanXunRepayMes = huanXunRepayService.queryMemberInfo(huanXunRepayMessageBean); //还款信息
                        if (listHuanXunRepayMes != null && listHuanXunRepayMes.size() > 0){
                            repayer = listHuanXunRepayMes.get(0).getRepayer(); //还款人id
                            balance = listHuanXunRepayMes.get(0).getBalance(); //余额(String型)
                            if (balance != null && !balance.equals("")){
                                balanceDou = TypeTool.getDouble(balance); //余额(Double型)
                            }
                        }
                    }

                    /*************** 还款冻结部分Strat ***************/
                    huanxunFreezeRepay.setId(UidHuanXunRepay); //环迅还款主键
                    huanxunFreezeRepay.setRepayid(repayId); //还款id
                    huanxunFreezeRepay.setMemberid(repayer); //还款人id
                    huanxunFreezeRepay.setFreezetype("3"); //冻结类型(1-投标、2-债权转让、3-还款、4-分红、5-代偿、6-代偿还款、7-风险准备金、8-结算担保收益、9-红包、10-融资方保证金、11-投资人保证金、12-担保人保证金)
                    huanxunFreezeRepay.setFlag("0"); //状态(0-冻结、1-解冻、2-转账)
                    huanxunFreezeRepay.setResultcode(resultCode); //响应吗(000000-成功、000001~999999-失败)
                    huanxunFreezeRepay.setResultmsg(resultMsg); //响应信息
                    huanxunFreezeRepay.setMerchantid(merchantID); //商户存管交易账号
                    huanxunFreezeRepay.setSign(sign); //签名
                    huanxunFreezeRepay.setMerbillno(merBillNo); //商户订单号
                    huanxunFreezeRepay.setProjectno(projectNo); //项目id号
                    huanxunFreezeRepay.setIpstrdamt(ipsTrdAmtDou); //IPS冻结金额
                    huanxunFreezeRepay.setIpsacctno(ipsAcctNo); //ips订单号
                    huanxunFreezeRepay.setOtheripsacctno(otherIpsAcctNo); //它方账号
                    huanxunFreezeRepay.setIpsbillno(ipsBillNo); //IPS订单号
                    huanxunFreezeRepay.setIpsdotime(ipsDoTime); //IPS处理时间
                    huanxunFreezeRepay.setTrdstatus(trdStatus); //冻结状态(0-失败、1-成功)
                    huanxunFreezeRepay.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                    huanxunFreezeRepay.setDelflg("1"); //是否删除(0-已删除、1-未删除);
                    /**************** 还款冻结部分End ****************/

                    /*************** 我的消息Start ***************/
                    title = "环迅还款金额冻结";
                    content = "<li>您有" + ipsTrdAmtDou + "元金额因还款被冻结，该笔钱将用于还款</li>"
                            + "<li>项目ID号：" + projectNo + "</li>"
                            + "<li>IPS冻结金额：" + ipsTrdAmtDou + "</li>"
                            + "<li>IPS订单号：" + ipsAcctNo + "</li>"
                            + "<li>它方账号：" + otherIpsAcctNo + "</li>"
                            + "<li>IPS订单号：" + ipsBillNo + "</li>"
                            + "<li>IPS处理时间：" + ipsDoTime + "</li>";

                    memberMessage.setId(UidMyMessage); //主键
                    memberMessage.setGlid(UidHuanXunRepay); //关联id
                    memberMessage.setType("0"); //类型(0-系统消息)
                    memberMessage.setMemberid(repayer); //还款人id
                    memberMessage.setReadflag("1"); //读取状态(0-已读、1-未读)
                    memberMessage.setTitle(title); //题目
                    memberMessage.setContent(content); //内容
                    memberMessage.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送时间
                    memberMessage.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                    memberMessage.setDelflg("1"); //删除标志(0-已删除、1-未删除)
                    /**************** 我的消息End ****************/

                    /*************** 会员消息Start ***************/
                    System.out.println("冻结前余额："+balanceDou+",冻结人："+repayer);
                    balanceDou = balanceDou - ipsTrdAmtDou; //冻结后余额
                    System.out.println("冻结后余额："+balanceDou+",冻结人："+repayer);
                    memberInfo.setId(repayer); //会员id
                    memberInfo.setBalance(balanceDou); //余额
                    /**************** 会员消息End ****************/

                    /*************** 异常处理部分Start ***************/
                    mapException.put("resultCode",resultCode); //响应码
                    mapException.put("resultMsg",resultMsg); //返回信息
                    mapException.put("merBillNo",merBillNo); //商户订单号
                    mapException.put("projectNo",projectNo); //项目ID号
                    mapException.put("ipsTrdAmt",ipsTrdAmt); //IPS冻结金额
                    mapException.put("ipsAcctNo",ipsAcctNo); //IPS冻结账号
                    mapException.put("otherIpsAcctNo",otherIpsAcctNo); //它方账号
                    mapException.put("ipsBillNo",ipsBillNo); //IPS订单号
                    mapException.put("ipsDoTime",ipsDoTime); //IPS处理时间
                    /**************** 异常处理部分End ****************/

                    result = huanXunRepayService.repayhxResult(huanxunFreezeRepay,memberMessage,memberInfo,mapException); //冻结结果

                    System.out.println("********** 还款冻结End **********");
                    if (result != null && !result.equals("")){
                        if (!result.equals("0")){ //失败
                            HuanXunFreezeRepayException.handle(mapException);
                        }else{
                            this.transferAfterFreeze(UidHuanXunRepay,response);
                        }
                    }
                }
            }
        }catch(Exception e){
            HuanXunFreezeRepayException.handle(mapException);
            e.printStackTrace();
        }
    }


    /**
     * 还款部分返回web服务
     * @param request
     * @param response
     */
    @RequestMapping(value = "/repayWebResult", method = RequestMethod.POST)
    @ResponseBody
    public void repayWebResult(HttpServletRequest request,HttpServletResponse response) throws IOException {
        String memberId = request.getParameter("memberId"); //会员id
        String UidHuanXunRepay = request.getParameter("UidHuanXunRepay"); //还款冻结部分主键
        response.sendRedirect("repayWebUrl?memberId="+memberId+"&UidHuanXunRepay="+UidHuanXunRepay);
    }

    /**
     * 返回还款等待页面
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/repayWebUrl", method = RequestMethod.GET)
    public String repayWebUrl(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap){
        String memberId = request.getParameter("memberId"); //会员id
        String freezeRepayId = request.getParameter("UidHuanXunRepay"); //还款冻结主键
        modelMap.addAttribute("memberId",memberId);
        modelMap.addAttribute("freezeRepayId",freezeRepayId);
        return "front/repay/repayFreezewaiting";
    }


    /**
     * 还款冻结后转账
     * @param freezeRepayId
     * @param response
     */
    public void transferAfterFreeze(String freezeRepayId,HttpServletResponse response) {
        System.out.println("********** 还款转账Start **********");
        String operationType = "trade.transfer"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String requesthx = ""; //请求信息
        String requesthxJm = ""; //请求信息(加密)
        String repayId = ""; //还款id
        String productId = ""; //产品id
        String yrepaymentTime = ""; //应还款日期
        String projectNo = ""; //项目ID号
        String freezeId = ""; //IPS原冻结订单号
        String transferType = "3"; //转账类型(3-还款)
        String s2SUrl = "http://120.76.137.147/huanxun/transferRepayResult"; //后台地址
        HuanXunFreezeMessageBean huanXunFreezeMessageBean = new HuanXunFreezeMessageBean(); //环迅冻结部分
        List<HuanXunTransferMessage> listHuanXunTransferMes = new ArrayList<HuanXunTransferMessage>();
        String batchNo = ""; //商户转账批次编号
        String merDate = ""; //转账日期
        String isAutoRepayment = "2"; //是否自动还款(1-是、2-否)
        String transferMode = "2"; //1-逐笔转账、2-批量转账
        String md5Zs = ""; //md5证书
        String transferAccDetail = ""; //转账明细集合
        String merBillNo = ""; //商户订单号
        String outIpsAcctNo = ""; //转出方IPS存管账户
        String outMerFee = "0"; //转出方平台手续费
        String inIpsAcctNo = ""; //转入方IPS存管账户
        String inMerFee = "0"; //转入方平台手续费
        String trdAmt = ""; //转账金额
        String transferId = ""; //转账表主键
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm"; //环迅接口调用地址
        String resultCode = ""; //响应状态
        String resultMsg = ""; //响应信息
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息
        String responsehx = ""; //返回信息

        /*************** 转账前备份部分Start ***************/
        Map<String,String> mapTransferBf = new HashMap<String,String>();
        List<Map<String,String>> listMapTransferDetailBf = new ArrayList<Map<String,String>>();
        /**************** 转账前备份部分End ****************/

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs(); //md5证书

            transferId = UUID.randomUUID().toString().replaceAll("-",""); //转账表主键

            huanXunFreezeMessageBean = huanXunFreezeService.queryFreezeRepay(freezeRepayId); //环迅冻结信息
            if (huanXunFreezeMessageBean != null){
                productId = huanXunFreezeMessageBean.getProductId(); //产品id
                yrepaymentTime = huanXunFreezeMessageBean.getyRepaymentTime(); //应还款日期
                repayId = huanXunFreezeMessageBean.getRepayId(); //还款id
                freezeId = huanXunFreezeMessageBean.getIpsBillNo(); //IPS原冻结订单号
                projectNo = huanXunFreezeMessageBean.getProjectNo(); //项目ID号
                batchNo = "181006" + UUID.randomUUID().toString().replaceAll("-",""); //商户转账批次编号
                merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //转账日期
                outIpsAcctNo = huanXunFreezeMessageBean.getIpsAcctNo(); //转出方IPS存管账户

                /*************** 转账信息备份Start ***************/
                mapTransferBf.put("transferId",transferId); //转账id
                mapTransferBf.put("batchNo",batchNo); //商户转账批次编号
                mapTransferBf.put("merDate",merDate); //转账日期
                mapTransferBf.put("projectNo",projectNo); //项目ID号
                mapTransferBf.put("transferType",transferType); //转账类型
                mapTransferBf.put("isAutoRepayment",isAutoRepayment); //是否自动还款(1-是、2-否)
                mapTransferBf.put("transferMode",transferMode); //转账类型(3-还款)
                /**************** 转账信息备份End ****************/

                System.out.println("productId======"+productId+",yrepaymentTime======"+yrepaymentTime);
                listHuanXunTransferMes = huanXunTransferService.getRepayTransferAcctDetail(productId,yrepaymentTime); //转账信息集合
                System.out.println("转账信息集合数量======"+listHuanXunTransferMes.size());

                if (listHuanXunTransferMes != null && listHuanXunTransferMes.size() > 0){
                    for (HuanXunTransferMessage huanXunTransferMessage : listHuanXunTransferMes){
                        merBillNo = "181006" + UUID.randomUUID().toString().replaceAll("-",""); //商户订单号
                        inIpsAcctNo = huanXunTransferMessage.getInIpsAcctNo(); //转入方IPS存管账户
                        trdAmt = huanXunTransferMessage.getTrdAmt(); //转账金额
                        inMerFee = huanXunTransferMessage.getInMerFee(); //平台手续费
                        s2SUrl = "http://120.76.137.147/huanxun/transferRepayResult"; //后台地址

                        System.out.println("平台手续费为："+inMerFee+"，转账金额为："+trdAmt);
                        if (transferAccDetail == null || transferAccDetail.equals("")){
                            transferAccDetail = "{"
                                    + "\"merBillNo\":\"" + merBillNo + "\","
                                    + "\"freezeId\":\"" + freezeId + "\","
                                    + "\"outIpsAcctNo\":\"" + outIpsAcctNo + "\","
                                    + "\"outMerFee\":\"" + outMerFee + "\","
                                    + "\"inIpsAcctNo\":\"" + inIpsAcctNo + "\","
                                    + "\"inMerFee\":\"" + inMerFee + "\","
                                    + "\"trdAmt\":\"" + trdAmt + "\""
                                    + "}";
                        }else{
                            transferAccDetail += ",{"
                                    + "\"merBillNo\":\"" + merBillNo + "\","
                                    + "\"freezeId\":\"" + freezeId + "\","
                                    + "\"outIpsAcctNo\":\"" + outIpsAcctNo + "\","
                                    + "\"outMerFee\":\"" + outMerFee + "\","
                                    + "\"inIpsAcctNo\":\"" + inIpsAcctNo + "\","
                                    + "\"inMerFee\":\"" + inMerFee + "\","
                                    + "\"trdAmt\":\"" + trdAmt + "\""
                                    + "}";
                        }
                    }
                    s2SUrl = s2SUrl + "?transferId="+transferId+"&yrepaymentTime="+yrepaymentTime+"&repayId="+repayId; //后台通知地址

                    /*************** 转账详细信息Start ***************/
                    Map<String,String> mapTransferDetailBf = new HashMap<String,String>();
                    mapTransferDetailBf.put("merBillNo",merBillNo); //商户订单号
                    mapTransferDetailBf.put("freezeId",freezeId); //IPS原冻结订单号
                    mapTransferDetailBf.put("outIpsAcctNo",outIpsAcctNo); //转出方IPS存管账户号
                    mapTransferDetailBf.put("outMerFee",outMerFee); //转出方平台手续费
                    mapTransferDetailBf.put("inIpsAcctNo",inIpsAcctNo); //转入方IPS存管账户号
                    mapTransferDetailBf.put("inMerFee",inMerFee); //转入方平台手续费
                    mapTransferDetailBf.put("trdAmt",trdAmt); //转账金额
                    listMapTransferDetailBf.add(mapTransferDetailBf);
                    /**************** 转账详细信息End ****************/

                    String transferResult = TransferBf.transferResult(mapTransferBf,listMapTransferDetailBf); //转账后结果
                    if (transferResult != null && !transferResult.equals("")){
                        if (transferResult.equals("0")){ //成功
                            requesthx = "{"
                                    + "\"batchNo\":\"" + batchNo + "\","
                                    + "\"merDate\":\"" + merDate + "\","
                                    + "\"projectNo\":\"" + projectNo + "\","
                                    + "\"transferType\":\"" + transferType + "\","
                                    + "\"isAutoRepayment\":\"" + isAutoRepayment + "\","
                                    + "\"transferMode\":\"" + transferMode + "\","
                                    + "\"s2SUrl\":\"" + s2SUrl + "\","
                                    + "\"transferAccDetail\":"
                                    + "["
                                    + transferAccDetail
                                    + "]"
                                    + "}";

                            requesthxJm = Pdes.encrypt3DES(requesthx); //3des加密
                            sign = operationType + merchantID + requesthxJm + md5Zs; //签名
                            signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

                            List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
                            HTTPParam httpParam1 = new HTTPParam("operationType", operationType); //操作类型
                            HTTPParam httpParam2 = new HTTPParam("merchantID", merchantID); //商户存管交易账号
                            HTTPParam httpParam3 = new HTTPParam("request", requesthxJm); //请求信息
                            HTTPParam httpParam4 = new HTTPParam("sign", signJm); //签名
                            listHTTParam.add(httpParam1);
                            listHTTParam.add(httpParam2);
                            listHTTParam.add(httpParam3);
                            listHTTParam.add(httpParam4);

                            String result = HttpRequest.sendPost(url,listHTTParam); //获取接口调用后返回信息

                            /******************** 存放异常信息Start ********************/
                            Map<String,String> map = JsonHelper.getObjectToMap(result); //将Json数据转换为Map格式
                            if (map != null) {
                                resultCode = map.get("resultCode"); //响应状态(000000-成功、999999-失败)
                                resultMsg = map.get("resultMsg"); //响应信息
                                responsehx = map.get("response"); //返回信息

                                mapException.put("resultCode",resultCode); //响应状态(000000-成功、999999-失败)
                                mapException.put("resultMsg",resultMsg); //响应信息
                                mapException.put("batchNo",batchNo); //商户转账批次号
                                mapException.put("projectNo",projectNo); //项目ID号
                                mapException.put("transferType",transferType); //转账类型(3-还款)
                                mapException.put("response",responsehx); //返回信息
                                mapException.put("freezeRepayId",freezeRepayId); //环迅冻结表主键

                                if (resultCode != null && !resultCode.equals("")){
                                    if (!resultCode.equals("000000")){
                                        HuanXunTransferRepayException.handle(mapException);
                                    }
                                }
                            }
                            /********************* 存放异常信息End *********************/
                        }
                    }
                }
                System.out.println("********** 还款转账End **********");
            }
        }catch (Exception e){
            HuanXunTransferRepayException.handle(mapException);
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("resultCode",resultCode);
        in.put("resultMsg",resultMsg);
        output(response, JSONObject.fromObject(in).toString());
    }



    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}