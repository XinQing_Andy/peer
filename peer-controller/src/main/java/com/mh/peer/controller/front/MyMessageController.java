package com.mh.peer.controller.front;

import com.mh.peer.dao.front.MyMessageDao;
import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.MemberMessageMesBean;
import com.mh.peer.service.front.MyMessageService;
import com.mh.peer.util.FrontPage;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Cuibin on 2016/6/24.
 */

@Controller
@RequestMapping("/myMessage")
public class MyMessageController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private MyMessageService myMessageService;


    /**
     * 跳转到我的消息
     */
    @RequestMapping(value = "openMyMessage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openMyMessage(@ModelAttribute MemberMessageMesBean memberMessageMesBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo"); //从session中获取会员登录信息
        String memberId = ""; //会员id
        String pageIndex = memberMessageMesBean.getPageIndex(); //起始数
        String pageSize = memberMessageMesBean.getPageSize(); //每页显示数量
        String readflag = memberMessageMesBean.getReadflag(); //读取状态(0-已读、1-未读)
        String startTime = memberMessageMesBean.getStartTime(); //发送起时间
        String endTime = memberMessageMesBean.getEndTime(); //发送止时间
        String pageStr = ""; //分页部分拼接

        if (memberInfo != null){
            memberId = memberInfo.getId();
            memberMessageMesBean.setMemberid(memberId);
            if (pageIndex == null || pageIndex.equals("")){ //起始数
                pageIndex = "1";
                memberMessageMesBean.setPageIndex(pageIndex);
            }
            if (pageSize == null || pageSize.equals("")){ //每页显示数量
                pageSize = "3";
                memberMessageMesBean.setPageSize(pageSize);
            }
            //我的消息集合
            List<MemberMessageMesBean> listMemberMessageMes = myMessageService.queryMyMessageByPage(memberMessageMesBean);
            //我的消息数量
            int totalCount = myMessageService.queryMyMessageCount(memberMessageMesBean);
            FrontPage frontPage = new FrontPage();
            pageStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageMemberMessage","goMemberMessagePage"); //我的消息分页部分拼接
            modelMap.put("messageList", listMemberMessageMes); //我的消息集合
            modelMap.put("pageStr",pageStr); //分页部分拼接
            modelMap.put("readflag",readflag); //读取状态(0-已读、1-未读)
            modelMap.put("startTime",startTime); //发送起时间
            modelMap.put("endTime",endTime); //止时间
        }

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/myMessage/my_message.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 跳转到我的消息详情
     */
    @RequestMapping(value = "/openMyMessageDetails", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openMyMessageDetails(MemberMessageMesBean memberMessageMes,ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = memberMessageMes.getPageIndex(); //起始数(字符型)
        int pageIndexIn = 0; //起始数(整型)
        if (pageIndex != null && !pageIndex.equals("")){
            pageIndexIn = TypeTool.getInt(pageIndex);
        }
        MemberMessageMesBean memberMessageMesBean = myMessageService.getMyMessageById(memberMessageMes);
        int totalCount = myMessageService.queryMyMessageCount(memberMessageMes); //我的消息数量
        if (pageIndexIn < totalCount){
            pageIndexIn = pageIndexIn + 1;
        }else{
            pageIndexIn = 1;
        }
        modelMap.put("memberMessageMesBean",memberMessageMesBean); //我的消息
        modelMap.put("num",pageIndexIn);

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/myMessage/message_details.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 查询下一条记录
     * @param memberMessageMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openNextMyMessageDetails", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openNextMyMessageDetails(MemberMessageMesBean memberMessageMes,ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = memberMessageMes.getPageIndex(); //起始数(字符型)
        int pageIndexIn = 0; //起始数(整型)
        if (pageIndex != null && !pageIndex.equals("")){
            pageIndexIn = TypeTool.getInt(pageIndex);
        }

        MemberMessageMesBean memberMessageMesBean = myMessageService.getNextMyMessageById(memberMessageMes);
        int totalCount = myMessageService.queryMyMessageCount(memberMessageMes); //我的消息数量
        if (pageIndexIn < totalCount){
            pageIndexIn = pageIndexIn + 1;
        }else{
            pageIndexIn = 1;
        }
        modelMap.put("memberMessageMesBean",memberMessageMesBean); //我的消息
        modelMap.put("num",pageIndexIn);

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/front/myMessage/message_details.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 删除我的消息
     * @param memberMessageMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/delMyMessageDetails", method = RequestMethod.POST)
    @ResponseBody
    public MemberMessageMesBean delMyMessageDetails(MemberMessageMesBean memberMessageMes,ModelMap modelMap) {
        MemberMessageMesBean memberMessageMesBean = myMessageService.delMyMessageById(memberMessageMes);
        return memberMessageMesBean;
    }


    /**
     * 批量删除我的消息
     * @param memberMessageMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/delBatchMyMessage", method = RequestMethod.POST)
    @ResponseBody
    public String delBatchMyMessage(MemberMessageMesBean memberMessageMes,ModelMap modelMap) {
        String checkSelected = memberMessageMes.getCheckSelected();
        String id = ""; //我的消息主键
        String resultCode = "0"; //删除标志

        try{

            if (checkSelected != null && !checkSelected.equals("")){
                String[] arr = checkSelected.split(",");
                if (arr != null && arr.length > 0){
                    for (int i=0;i<arr.length;i++){
                        MemberMessageMesBean memberMessageMesBean = new MemberMessageMesBean();
                        id = arr[i];
                        memberMessageMesBean.setId(id);
                        memberMessageMesBean = myMessageService.delMyMessageById(memberMessageMesBean);
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            resultCode = "-1";
        }
        return resultCode;
    }



    /**
     * 批量标记我的消息
     * @param memberMessageMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/updateBatchReadFlg", method = RequestMethod.POST)
    @ResponseBody
    public String updateBatchReadFlg(MemberMessageMesBean memberMessageMes,ModelMap modelMap) {
        String checkSelected = memberMessageMes.getCheckSelected();
        String id = ""; //我的消息主键
        String resultCode = "0"; //删除标志

        try{

            if (checkSelected != null && !checkSelected.equals("")){
                String[] arr = checkSelected.split(",");
                if (arr != null && arr.length > 0){
                    for (int i=0;i<arr.length;i++){
                        MemberMessageMesBean memberMessageMesBean = new MemberMessageMesBean();
                        id = arr[i];
                        memberMessageMesBean.setId(id);
                        memberMessageMesBean = myMessageService.updateBatchReadFlg(memberMessageMesBean);
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            resultCode = "-1";
        }
        return resultCode;
    }
}
