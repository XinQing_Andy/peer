package com.mh.peer.controller.huanxun;

import com.mh.peer.exception.HuanXunTransferAttornException;
import com.mh.peer.exception.HuanXunTransferInvestException;
import com.mh.peer.exception.HuanXunTransferRepayException;
import com.mh.peer.model.entity.HuanxunTransfer;
import com.mh.peer.model.entity.HuanxunTransferDetail;
import com.mh.peer.model.message.*;
import com.mh.peer.service.huanxun.HuanXunAttornService;
import com.mh.peer.service.huanxun.HuanXunFreezeService;
import com.mh.peer.service.huanxun.HuanXunInvestService;
import com.mh.peer.service.huanxun.HuanXunTransferService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.*;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * Created by zhangerxin on 2016-7-29.
 * 转账
 */
@Controller
@RequestMapping("/huanxun")
public class HuanXunTransferController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HuanXunTransferController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private HuanXunInvestService huanXunInvestService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private HuanXunTransferService huanXunTransferService;
    @Autowired
    private HuanXunAttornService huanXunAttornService;
    @Autowired
    private HuanXunFreezeService huanXunFreezeService;


    /**
     * 投资转账(审核成功后执行)
     *
     * @param huanXunTransferMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/transferBuyhx", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunTransferMessageBean transferBuyhx(@ModelAttribute HuanXunTransferMessageBean huanXunTransferMes, ModelMap modelMap) {
        String transferId = ""; //转账主键
        String productId = huanXunTransferMes.getProductId(); //产品id
        String operationType = "trade.transfer"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String batchNo = "181006"; //商户转账批次号
        String merDate = ""; //转账日期
        String projectNo = ""; //项目ID号
        String transferType = "1"; //转账类型
        String isAutoRepayment = ""; //是否自动还款
        String transferMode = "2"; //转账方式(1-逐笔转账、2-批量转账)
        String s2SUrl = "http://120.76.137.147/huanxun/transferBuyResult"; //后台通知地址
        String transferAccDetail = ""; //转账明细集合
        String merBillNo = "181006"; //商户订单号
        String outIpsAcctNo = ""; //转出方IPS存管账户号
        String outMerFee = ""; //转出方平台手续费
        String inIpsAcctNo = ""; //转入方IPS存管账户号
        String inMerFee = ""; //转入方平台手续费
        String trdAmt = ""; //转账金额
        String freezeId = ""; //冻结id
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        String md5Zs = ""; //md5证书
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm";
        String resultCode = ""; //响应码(000000-成功、999999-失败)
        String resultMsg = ""; //响应信息
        List<Map<String,Object>> listMapException = new ArrayList<Map<String,Object>>(); //异常信息集合

        try {

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs(); //md5证书

            HuanXunFreezeMessageBean huanXunFreezeMes = new HuanXunFreezeMessageBean();
            huanXunFreezeMes.setProductId(productId);

            transferId = UUID.randomUUID().toString().replaceAll("-", ""); //转账主键
            batchNo = batchNo + UUID.randomUUID().toString().replaceAll("-", ""); //商户转账批次号
            merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //转账日期

            //冻结(投资冻结资金列表)
            List<HuanXunFreezeMessageBean> listHuanXunFreezeMes = huanXunInvestService.getHuanXunInvestBySome(huanXunFreezeMes);
            if (listHuanXunFreezeMes != null && listHuanXunFreezeMes.size() > 0) {

                /************************* 投资部分集合Start *************************/
                for (HuanXunFreezeMessageBean huanXunFreezeMessageBean : listHuanXunFreezeMes) {

                    projectNo = huanXunFreezeMessageBean.getProjectNo(); //项目ID号
                    merBillNo = "181006";
                    merBillNo = merBillNo + UUID.randomUUID().toString().replaceAll("-", ""); //商户订单号
                    freezeId = huanXunFreezeMessageBean.getIpsBillNo(); //冻结id号
                    outIpsAcctNo = huanXunFreezeMessageBean.getIpsAcctNo(); //转出方IPS存管账户号
                    outMerFee = "0"; //转出方平台手续费
                    inIpsAcctNo = huanXunFreezeMessageBean.getInIpsAcctNo(); //转入方IPS存管账户号
                    inMerFee = "0"; //转入方平台手续费
                    trdAmt = huanXunFreezeMessageBean.getTrdAmt(); //转账金额

                    if (transferAccDetail == null || transferAccDetail.equals("")) { //转账明细集合
                        transferAccDetail = "{"
                                          + "\"merBillNo\":\"" + merBillNo + "\","
                                          + "\"freezeId\":\"" + freezeId + "\","
                                          + "\"outIpsAcctNo\":\"" + outIpsAcctNo + "\","
                                          + "\"outMerFee\":\"" + outMerFee + "\","
                                          + "\"inIpsAcctNo\":\"" + inIpsAcctNo + "\","
                                          + "\"inMerFee\":\"" + inMerFee + "\","
                                          + "\"trdAmt\":\"" + trdAmt + "\""
                                          + "}";
                    } else {
                        transferAccDetail += ",{"
                                           + "\"merBillNo\":\"" + merBillNo + "\","
                                           + "\"freezeId\":\"" + freezeId + "\","
                                           + "\"outIpsAcctNo\":\"" + outIpsAcctNo + "\","
                                           + "\"outMerFee\":\"" + outMerFee + "\","
                                           + "\"inIpsAcctNo\":\"" + inIpsAcctNo + "\","
                                           + "\"inMerFee\":\"" + inMerFee + "\","
                                           + "\"trdAmt\":\"" + trdAmt + "\""
                                           + "}";
                    }
                }
                /************************** 投资部分集合End **************************/
            }

            s2SUrl = s2SUrl + "?transferId=" + transferId + "&productId=" + productId; //后台通知地址

            request = "{"
                    + "\"batchNo\":\"" + batchNo + "\","
                    + "\"merDate\":\"" + merDate + "\","
                    + "\"projectNo\":\"" + projectNo + "\","
                    + "\"transferType\":\"" + transferType + "\","
                    + "\"isAutoRepayment\":\"" + isAutoRepayment + "\","
                    + "\"transferMode\":\"" + transferMode + "\","
                    + "\"s2SUrl\":\"" + s2SUrl + "\","
                    + "\"transferAccDetail\":"
                    + "["
                    + transferAccDetail
                    + "]"
                    + "}";
            requestJm = Pdes.encrypt3DES(request); //3des加密
            sign = operationType + merchantID + requestJm + md5Zs; //签名
            signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

            huanXunTransferMes.setSign(signJm); //签名
            huanXunTransferMes.setRequest(requestJm); //请求信息

            List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
            HTTPParam httpParam1 = new HTTPParam("operationType", operationType); //操作类型
            HTTPParam httpParam2 = new HTTPParam("merchantID", merchantID); //商户存管交易账号
            HTTPParam httpParam3 = new HTTPParam("request", requestJm); //请求信息
            HTTPParam httpParam4 = new HTTPParam("sign", signJm); //签名
            listHTTParam.add(httpParam1);
            listHTTParam.add(httpParam2);
            listHTTParam.add(httpParam3);
            listHTTParam.add(httpParam4);

            String result = HttpRequest.sendPost(url, listHTTParam); //获取接口调用后返回信息

            Map<String, String> map = JsonHelper.getObjectToMap(result); //将Json数据转换为Map格式
            if (map != null) {
                resultCode = map.get("resultCode"); //响应状态(000000-成功、999999-失败)
                resultMsg = map.get("resultMsg"); //响应信息
            }

            if (resultCode != null && !resultCode.equals("")) {
                if (resultCode.equals("000000")) { //成功
                    huanXunTransferMes.setRequest("success"); //程序返回状态
                    huanXunTransferMes.setInfo("审核成功！"); //程序返回信息
                } else {
                    huanXunTransferMes.setRequest("success");
                    huanXunTransferMes.setInfo("审核失败！" + resultMsg);
                }
            } else {
                huanXunTransferMes.setRequest("success");
                huanXunTransferMes.setInfo("审核失败！" + resultMsg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return huanXunTransferMes;
    }


    /**
     * 投资转账后结果
     *
     * @param request
     * @param modelMap
     */
    @RequestMapping(value = "/transferBuyResult", method = RequestMethod.POST)
    @ResponseBody
    public void transferBuyResult(HttpServletRequest request,HttpServletResponse responsehx,ModelMap modelMap) {

        /******************** 服务器返回信息Start ********************/
        String resultCode = request.getParameter("resultCode"); //响应状态(000000-成功、999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //响应消息描述
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String response = request.getParameter("response"); //响应信息
        String responseJm = ""; //响应信息(解密)
        String batchNo = ""; //商户转账批次号
        String projectNo = ""; //项目ID号
        String transferType = ""; //转账类型
        String transferAccDetail = ""; //转账明细集合
        String merBillNo = ""; //商户订单号
        String outIpsAcctNo = ""; //转出方IPS存管账户号
        String inIpsAcctNo = ""; //转入方IPS存管账号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String ipsTrdAmt = ""; //IPS转账金额(String型)
        Double ipsTrdAmtDou = 0.0d; //IPS转账金额(Double型)
        String trdStatus = ""; //转账状态(0-失败、1-成功)
        /********************* 服务器返回信息End *********************/

        String transferId = request.getParameter("transferId"); //转账id
        String productId = request.getParameter("productId"); //产品id
        String freezeId = ""; //冻结id
        Map map = new HashMap(); //Json集合
        Map<String,String> mapTransferDetail = new HashMap<String, String>(); //转账详细信息
        String UidTransferDetail = ""; //主键(转账详情)

        List<HuanXunTransferMessage> listHuanXunTransferMes = new ArrayList<HuanXunTransferMessage>(); //转账集合(消息对象)
        HuanxunTransfer huanxunTransfer = new HuanxunTransfer(); //转账集合(实体对象)
        List<HuanxunTransferDetail> listHuanxunTransferDetail = new ArrayList<HuanxunTransferDetail>(); //转账详情集合(实体对象)
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息
        List<Map<String,Object>> listMaps = new ArrayList<Map<String,Object>>();

        try {

            output(responsehx,"ipsCheckOk"); //接收成功

            HuanXunTransferMessage huanXunTransferMessage = new HuanXunTransferMessage();
            huanXunTransferMessage.setId(transferId); //主键
            listHuanXunTransferMes = huanXunTransferService.getTransferInfoBySome(huanXunTransferMessage);
            if (listHuanXunTransferMes.size() == 0) { //转账笔数
                if (response != null && !response.equals("")) { //响应信息
                    responseJm = Pdes.decrypt3DES(response); //响应信息(解密)

                        if (responseJm != null && !responseJm.equals("")) {
                            map = JsonToMap.getMapFromJsonObjStr(responseJm);
                            if (map != null) {
                                batchNo = (String) map.get("batchNo"); //商户转账批次号
                                projectNo = (String) map.get("projectNo"); //项目ID号
                                transferType = (String) map.get("transferType"); //转账类型
                                huanxunTransfer.setId(transferId); //主键
                                huanxunTransfer.setResultcode(resultCode); //响应状态(000000-成功、999999-失败)
                                huanxunTransfer.setResultmsg(resultMsg); //响应消息描述
                                huanxunTransfer.setMerchantid(merchantID); //商户存管交易账号
                                huanxunTransfer.setBatchno(batchNo); //商户转账批次号
                                huanxunTransfer.setProjectno(projectNo); //项目ID号
                                huanxunTransfer.setTransfertype(transferType); //转账类型
                                huanxunTransfer.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

                                JSONArray jsonArray = (JSONArray) map.get("transferAccDetail"); //转账明细集合(集合形式)
                                transferAccDetail = jsonArray.toString(); //转账明细集合(转账明细集合)

                                if (transferAccDetail != null && !transferAccDetail.equals("")) {
                                    transferAccDetail = transferAccDetail.replaceAll("\\[", "");
                                    transferAccDetail = transferAccDetail.replaceAll("\\]", "");
                                    transferAccDetail = transferAccDetail.replaceAll("\\},\\{", "\\}、\\{");
                                    String[] arr = transferAccDetail.split("、"); //数组(用于存放转账明细)

                                    if (arr != null && arr.length > 0) {
                                        for (int i = 0; i < arr.length; i++) {
                                            mapTransferDetail = JsonHelper.getObjectToMap(arr[i]); //用于存放转账明细
                                            if (mapTransferDetail != null) {
                                                merBillNo = mapTransferDetail.get("merBillNo"); //商户订单号
                                                outIpsAcctNo = mapTransferDetail.get("outIpsAcctNo"); //转出方IPS存管账户号
                                                inIpsAcctNo = mapTransferDetail.get("inIpsAcctNo"); //转入方IPS存管账号号
                                                ipsBillNo = mapTransferDetail.get("ipsBillNo"); //IPS订单号
                                                ipsDoTime = mapTransferDetail.get("ipsDoTime"); //IPS处理时间
                                                ipsTrdAmt = mapTransferDetail.get("ipsTrdAmt"); //IPS转账金额(String型)
                                                if (ipsTrdAmt != null && !ipsTrdAmt.equals("")) {
                                                    ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //IPS转账金额(double型)
                                                }
                                                trdStatus = mapTransferDetail.get("trdStatus"); //转账状态(0-失败、1-成功)

                                                HuanxunTransferDetail huanxunTransferDetail = new HuanxunTransferDetail();
                                                UidTransferDetail = UUID.randomUUID().toString().replaceAll("-", ""); //主键(转账明细表)
                                                huanxunTransferDetail.setId(UidTransferDetail);
                                                huanxunTransferDetail.setTransferid(transferId); //转账id
                                                huanxunTransferDetail.setMerbillno(merBillNo); //商户订单号
                                                huanxunTransferDetail.setOutipsacctno(outIpsAcctNo); //转出方IPS存管账户号
                                                huanxunTransferDetail.setInipsacctno(inIpsAcctNo); //转入方IPS存管账号
                                                huanxunTransferDetail.setIpsbillno(ipsBillNo); //IPS订单号
                                                huanxunTransferDetail.setIpsdotime(ipsDoTime); //IPS处理时间
                                                huanxunTransferDetail.setIpstrdamt(ipsTrdAmtDou); //IPS转账金额
                                                huanxunTransferDetail.setTrdstatus(trdStatus); //转账状态(0-失败、1-成功)
                                                huanxunTransferDetail.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                                listHuanxunTransferDetail.add(huanxunTransferDetail);
                                            }

                                            Map<String,Object> mapDetails = new HashMap<String,Object>();
                                            mapDetails.put("merBillNo",merBillNo); //商户订单号
                                            mapDetails.put("outIpsAcctNo",outIpsAcctNo); //转出方IPS存管账户号
                                            mapDetails.put("inIpsAcctNo",inIpsAcctNo); //转入方IPS存管账号号
                                            mapDetails.put("ipsBillNo",ipsBillNo); //IPS订单号
                                            mapDetails.put("ipsDoTime",ipsDoTime); //IPS处理时间
                                            mapDetails.put("ipsTrdAmt",ipsTrdAmt); //IPS转账金额
                                            mapDetails.put("trdStatus",trdStatus); //转账状态(0-失败、1-成功)
                                            listMaps.add(mapDetails);
                                        }
                                    }
                                }
                            }
                        }
                }

                mapException.put("productId",productId); //产品id
                mapException.put("resultCode",resultCode); //响应状态
                mapException.put("resultMsg",resultMsg); //响应信息描述
                mapException.put("response",response); //响应信息
                mapException.put("listTransferDetail",listMaps); //转账详细信息

                huanXunTransferService.transferBuyResult(mapException,huanxunTransfer, listHuanxunTransferDetail, productId);
            }

        } catch (Exception e) {
            HuanXunTransferInvestException.handle(mapException);
            e.printStackTrace();
        }
    }


    /**
     * 债权转账
     *
     * @param huanXunTransferMes
     * @param modelMap
     */
    @RequestMapping(value = "/transferAttornhx", method = RequestMethod.POST)
    @ResponseBody
    public HuanXunTransferMessageBean transferAttornhx(HuanXunTransferMessageBean huanXunTransferMes, ModelMap modelMap) {
        String transferId = ""; //转账id
        String attornId = huanXunTransferMes.getAttornId(); //债权id
        String operationType = "trade.transfer"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String request = ""; //请求信息
        String requestJm = ""; //请求信息(加密)
        String batchNo = "181006"; //商户转账批次号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //转账日期
        String projectNo = ""; //项目ID号
        String transferType = "2"; //转账类型(1-放款、2-债权转让、3-还款)
        String isAutoRepayment = ""; //是否自动还款
        String transferMode = "1"; //转账方式(1-逐笔转账、2-批量转账)
        String s2SUrl = "http://120.76.137.147/huanxun/transferAttornResult"; ///后台通知地址
        String merBillNo = "181006"; //商户订单号
        String freezeId = ""; //IPS原冻结订单号
        String outIpsAcctNo = ""; //转出方IPS存管账户号
        String outMerFee = "0"; //转出方平台手续费
        String inIpsAcctNo = ""; //转入方IPS存管账户号
        String inMerFee = "0"; //转入方平台手续费
        String trdAmt = ""; //转账金额
        String md5Zs = ""; //md5证书
        HuanXunFreezeMessageBean huanXunFreezeMessageBean = new HuanXunFreezeMessageBean();
        List<HuanXunFreezeMessageBean> listHuanXunFreezeMes = new ArrayList<HuanXunFreezeMessageBean>();
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm"; //访问地址
        String resultCode = ""; //响应码(000000-成功、999999-失败)
        String resultMsg = ""; //响应信息

        try {

            transferId = UUID.randomUUID().toString().replaceAll("-", ""); //转账id

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs();

            batchNo = batchNo + UUID.randomUUID().toString().replaceAll("-", ""); //商户转账批次号
            merBillNo = merBillNo + UUID.randomUUID().toString().replaceAll("-", ""); //商户订单号
            huanXunFreezeMessageBean.setAttornId(attornId); //债权id
            listHuanXunFreezeMes = huanXunAttornService.getFreezeAttornInfo(huanXunFreezeMessageBean); //债权部分资金冻结集合
            if (listHuanXunFreezeMes != null && listHuanXunFreezeMes.size() > 0) {
                projectNo = listHuanXunFreezeMes.get(0).getProjectNo(); //项目ID号
                freezeId = listHuanXunFreezeMes.get(0).getIpsBillNo(); //IPS原冻结订单号
                outIpsAcctNo = listHuanXunFreezeMes.get(0).getIpsAcctNo(); //转出方IPS存管账户
                inIpsAcctNo = listHuanXunFreezeMes.get(0).getOtherIpsAcctNo(); //转入方IPS存管账户
                trdAmt = listHuanXunFreezeMes.get(0).getTrdAmt(); //冻结金额
            }


            s2SUrl = s2SUrl + "?attornId=" + attornId + "&transferId=" + transferId;

            request = "{"
                    + "\"batchNo\":\"" + batchNo + "\","
                    + "\"merDate\":\"" + merDate + "\","
                    + "\"projectNo\":\"" + projectNo + "\","
                    + "\"transferType\":\"" + transferType + "\","
                    + "\"isAutoRepayment\":\"" + isAutoRepayment + "\","
                    + "\"transferMode\":\"" + transferMode + "\","
                    + "\"s2SUrl\":\"" + s2SUrl + "\","
                    + "\"transferAccDetail\":"
                    + "["
                    + "{"
                    + "\"merBillNo\":\"" + merBillNo + "\","
                    + "\"freezeId\":\"" + freezeId + "\","
                    + "\"outIpsAcctNo\":\"" + outIpsAcctNo + "\","
                    + "\"outMerFee\":\"" + outMerFee + "\","
                    + "\"inIpsAcctNo\":\"" + inIpsAcctNo + "\","
                    + "\"inMerFee\":\"" + inMerFee + "\","
                    + "\"trdAmt\":\"" + trdAmt + "\""
                    + "}"
                    + "]"
                    + "}";

            requestJm = Pdes.encrypt3DES(request); //3des加密
            sign = operationType + merchantID + requestJm + md5Zs; //签名
            signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

            List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
            HTTPParam httpParam1 = new HTTPParam("operationType", operationType); //操作类型
            HTTPParam httpParam2 = new HTTPParam("merchantID", merchantID); //商户存管交易账号
            HTTPParam httpParam3 = new HTTPParam("request", requestJm); //请求信息
            HTTPParam httpParam4 = new HTTPParam("sign", signJm); //签名
            listHTTParam.add(httpParam1);
            listHTTParam.add(httpParam2);
            listHTTParam.add(httpParam3);
            listHTTParam.add(httpParam4);

            String result = HttpRequest.sendPost(url, listHTTParam); //获取接口调用后返回信息

            Map<String, String> map = JsonHelper.getObjectToMap(result); //将Json数据转换为Map格式
            if (map != null) {
                resultCode = map.get("resultCode"); //响应状态(000000-成功、999999-失败)
                resultMsg = map.get("resultMsg"); //响应信息
            }

            if (resultCode != null && !resultCode.equals("")) {
                if (resultCode.equals("000000")) { //成功
                    huanXunTransferMes.setResult("success"); //程序返回状态
                    huanXunTransferMes.setInfo("审核成功！"); //程序返回信息
                } else {
                    huanXunTransferMes.setResult("success");
                    huanXunTransferMes.setInfo("审核失败！" + resultMsg);
                }
            } else {
                huanXunTransferMes.setResult("success");
                huanXunTransferMes.setInfo("审核失败！" + resultMsg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return huanXunTransferMes;
    }


    /**
     * 债权审核成功后转账
     *
     * @param request
     * @param modelMap
     */
    @RequestMapping(value = "/transferAttornResult", method = RequestMethod.POST)
    @ResponseBody
    public void transferAttornResult(HttpServletRequest request, HttpServletResponse responsehx, ModelMap modelMap) {
        String resultCode = request.getParameter("resultCode"); //相应状态(000000-成功、999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //响应消息描述
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String response = request.getParameter("response"); //响应信息
        String responseJm = ""; //响应信息(解密)
        String batchNo = ""; //商户转账批次号
        String projectNo = ""; //项目ID号
        String transferType = ""; //转账类型
        String transferAccDetail = ""; //转账明细集合
        String merBillNo = ""; //商户订单号
        String outIpsAcctNo = ""; //转出方IPS存管账号
        String inIpsAcctNo = ""; //转入方IPS存管账号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String ipsTrdAmt = ""; //IPS转账金额(String型)
        Double ipsTrdAmtDou = 0.0d; //IPS转账金额(Double型)
        String trdStatus = ""; //转账状态(0-失败、1-成功)

        String transferId = request.getParameter("transferId"); //转账id
        String attornId = request.getParameter("attornId"); //债权id
        String freezeId = ""; //冻结id
        List<HuanXunTransferMessage> listHuanXunTransferMes = new ArrayList<HuanXunTransferMessage>(); //转账集合(消息对象)
        HuanxunTransfer huanxunTransfer = new HuanxunTransfer(); //转账集合(实体对象)
        Map map = new HashMap();
        Map<String, String> mapTransferDetail = new HashMap<String, String>(); //转账详细信息
        String UidTransferDetail = ""; //主键(转账详情)
        List<HuanxunTransferDetail> listHuanxunTransferDetail = new ArrayList<HuanxunTransferDetail>(); //转账详情集合(实体对象)
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息
        List<Map<String,Object>> listMaps = new ArrayList<Map<String,Object>>();

        try {

            output(responsehx,"ipsCheckOk"); //接收成功

            HuanXunTransferMessage huanXunTransferMessage = new HuanXunTransferMessage();
            huanXunTransferMessage.setId(transferId); //主键
            listHuanXunTransferMes = huanXunTransferService.getTransferInfoBySome(huanXunTransferMessage);
            if (listHuanXunTransferMes.size() == 0) { //转账笔数
                if (resultCode != null && !resultCode.equals("")) {
                    responseJm = Pdes.decrypt3DES(response); //响应信息(解密)
                    if (responseJm != null && !responseJm.equals("")) {
                        if (responseJm != null && !responseJm.equals("")) {
                            map = JsonToMap.getMapFromJsonObjStr(responseJm);
                            if (map != null) {
                                batchNo = (String) map.get("batchNo"); //商户转账批次号
                                projectNo = (String) map.get("projectNo"); //项目ID号
                                transferType = (String) map.get("transferType"); //转账类型
                                huanxunTransfer.setId(transferId); //主键
                                huanxunTransfer.setResultcode(resultCode); //响应状态(000000-成功、999999-失败)
                                huanxunTransfer.setResultmsg(resultMsg); //响应消息描述
                                huanxunTransfer.setMerchantid(merchantID); //商户存管交易账号
                                huanxunTransfer.setBatchno(batchNo); //商户转账批次号
                                huanxunTransfer.setProjectno(projectNo); //项目ID号
                                huanxunTransfer.setTransfertype(transferType); //转账类型
                                huanxunTransfer.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

                                JSONArray jsonArray = (JSONArray) map.get("transferAccDetail"); //转账明细集合(集合形式)
                                transferAccDetail = jsonArray.toString(); //转账明细集合(转账明细集合)
                                if (transferAccDetail != null && !transferAccDetail.equals("")) {
                                    transferAccDetail = transferAccDetail.replaceAll("\\[", "");
                                    transferAccDetail = transferAccDetail.replaceAll("\\]", "");
                                    transferAccDetail = transferAccDetail.replaceAll("\\},\\{", "\\}、\\{");
                                    String[] arr = transferAccDetail.split("、"); //数组(用于存放转账明细)
                                    if (arr != null && arr.length > 0) {
                                        for (int i = 0; i < arr.length; i++) {
                                            mapTransferDetail = JsonHelper.getObjectToMap(arr[i]); //用于存放转账明细
                                            if (mapTransferDetail != null) {
                                                merBillNo = mapTransferDetail.get("merBillNo"); //商户订单号
                                                outIpsAcctNo = mapTransferDetail.get("outIpsAcctNo"); //转出方IPS存管账户号
                                                inIpsAcctNo = mapTransferDetail.get("inIpsAcctNo"); //转入方IPS存管账号号
                                                ipsBillNo = mapTransferDetail.get("ipsBillNo"); //IPS订单号
                                                ipsDoTime = mapTransferDetail.get("ipsDoTime"); //IPS处理时间
                                                ipsTrdAmt = mapTransferDetail.get("ipsTrdAmt"); //IPS转账金额(String型)
                                                if (ipsTrdAmt != null && !ipsTrdAmt.equals("")) {
                                                    ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //IPS转账金额(double型)
                                                }
                                                trdStatus = mapTransferDetail.get("trdStatus"); //转账状态(0-失败、1-成功)
                                            }

                                            HuanxunTransferDetail huanxunTransferDetail = new HuanxunTransferDetail();
                                            UidTransferDetail = UUID.randomUUID().toString().replaceAll("-", ""); //主键(转账明细表)
                                            huanxunTransferDetail.setId(UidTransferDetail);
                                            huanxunTransferDetail.setTransferid(transferId); //转账id
                                            huanxunTransferDetail.setMerbillno(merBillNo); //商户订单号
                                            huanxunTransferDetail.setOutipsacctno(outIpsAcctNo); //转出方IPS存管账户号
                                            huanxunTransferDetail.setInipsacctno(inIpsAcctNo); //转入方IPS存管账号号
                                            huanxunTransferDetail.setIpsbillno(ipsBillNo); //IPS订单号
                                            huanxunTransferDetail.setIpsdotime(ipsDoTime); //IPS处理时间
                                            huanxunTransferDetail.setIpstrdamt(ipsTrdAmtDou); //IPS转账金额
                                            huanxunTransferDetail.setTrdstatus(trdStatus); //转账状态(0-失败、1-成功)
                                            huanxunTransferDetail.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                            listHuanxunTransferDetail.add(huanxunTransferDetail);

                                            Map<String,Object> mapDetails = new HashMap<String,Object>();
                                            mapDetails.put("merBillNo",merBillNo); //商户订单号
                                            mapDetails.put("outIpsAcctNo",outIpsAcctNo); //转出方IPS存管账户号
                                            mapDetails.put("inIpsAcctNo",inIpsAcctNo); //转入方IPS存管账号号
                                            mapDetails.put("ipsBillNo",ipsBillNo); //IPS订单号
                                            mapDetails.put("ipsDoTime",ipsDoTime); //IPS处理时间
                                            mapDetails.put("ipsTrdAmt",ipsTrdAmt); //IPS转账金额
                                            mapDetails.put("trdStatus",trdStatus); //转账状态(0-失败、1-成功)
                                            listMaps.add(mapDetails);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                mapException.put("attornId",attornId); //债权id
                mapException.put("resultCode",resultCode); //响应状态
                mapException.put("resultMsg",resultMsg); //响应信息描述
                mapException.put("response",response); //响应信息
                mapException.put("listTransferDetail",listMaps); //转账详细信息
                HuanXunTransferAttornException.handle(mapException);
                huanXunTransferService.transferAttornResult(mapException,huanxunTransfer, listHuanxunTransferDetail, attornId);
            }
        } catch (Exception e) {
            HuanXunTransferAttornException.handle(mapException);
            e.printStackTrace();
        }
    }


    /**
     * 自动还款转账(环迅返回方法) by cuibin
     */
    @RequestMapping(value = "/repaymentRequest", method = RequestMethod.POST)
    @ResponseBody
    public void repaymentRequest(HttpServletRequest request,HttpServletResponse response) {

        LOGGER.info("开始还款转账后台");
        String resultCode = request.getParameter("resultCode"); //响应吗(00000-成功、999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //响应信息描述
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String responsehx = request.getParameter("response"); //响应信息
        String responsehxJm = ""; //响应信息(解密)
        String UidTransfer = request.getParameter("UidTransfer"); //转账主键
        String receiveId = request.getParameter("receiveId"); //还款主键
        String UidTransferDetail = ""; //转账详情主键
        Map map = new HashMap();
        Map<String, String> mapTransferDetail = new HashMap<String, String>(); //转账详细信息
        String batchNo = ""; //商户转账批次号
        String projectNo = ""; //项目ID号
        String transferType = ""; //转账类型
        String transferAccDetail = ""; //请求提交数据（转账明细集合）
        String merBillNo = ""; //商户订单号
        String outIpsAcctNo = ""; //转出方IPS存管账户号
        String inIpsAcctNo = ""; //转入方IPS存管账户号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String ipsTrdAmt = ""; //IPS转账金额(IPS实际转账)
        Double ipsTrdAmtDou = 0.0d; //转账金额(Double型)
        String trdStatus = ""; //转账状态
        HuanxunTransfer huanxunTransfer = new HuanxunTransfer(); //转账实体对象
        List<HuanxunTransferDetail> listHuanxunTransferDetail = new ArrayList<HuanxunTransferDetail>(); //转账详情集合(实体对象)

        try {

            if (huanXunTransferService.queryIsTransfer(UidTransfer)) { //可以录入
                responsehxJm = Pdes.decrypt3DES(responsehx); //响应信息(解密)
                if (responsehxJm != null && !responsehxJm.equals("")) {
                    map = JsonToMap.getMapFromJsonObjStr(responsehxJm);
                    if (map != null) {
                        batchNo = (String) map.get("batchNo"); //商户转账批次号
                        projectNo = (String) map.get("projectNo"); //项目ID号
                        transferType = (String) map.get("transferType"); //转账类型

                        /********** 转账部分Start **********/
                        huanxunTransfer.setId(UidTransfer); //主键
                        huanxunTransfer.setResultcode(resultCode); //响应状态(000000-成功、999999-失败)
                        huanxunTransfer.setResultmsg(resultMsg); //响应消息描述
                        huanxunTransfer.setMerchantid(merchantID); //商户存管交易账号
                        huanxunTransfer.setBatchno(batchNo); //商户转账批次号
                        huanxunTransfer.setProjectno(projectNo); //项目ID号
                        huanxunTransfer.setTransfertype(transferType); //转账类型
                        huanxunTransfer.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                        /*********** 转账部分End ***********/

                        JSONArray jsonArray = (JSONArray) map.get("transferAccDetail"); //转账明细集合(集合形式)
                        transferAccDetail = jsonArray.toString(); //转账明细集合(转账明细集合)

                        if (transferAccDetail != null && !transferAccDetail.equals("")) { //转账详细信息
                            transferAccDetail = transferAccDetail.replaceAll("\\[", "");
                            transferAccDetail = transferAccDetail.replaceAll("\\]", "");
                            transferAccDetail = transferAccDetail.replaceAll("\\},\\{", "\\}、\\{");
                            LOGGER.info("处理后transferAccDetail为："+transferAccDetail);

                            LOGGER.info("开始存放数组");
                            String[] arr = transferAccDetail.split("、"); //数组(用于存放转账明细)
                            if (arr != null && arr.length > 0) {
                                for (int i = 0; i < arr.length; i++) {
                                    mapTransferDetail = JsonHelper.getObjectToMap(arr[i]); //用于存放转账明细
                                    if (mapTransferDetail != null) {
                                        merBillNo = mapTransferDetail.get("merBillNo"); //商户订单号
                                        outIpsAcctNo = mapTransferDetail.get("outIpsAcctNo"); //转出方IPS存管账户号
                                        inIpsAcctNo = mapTransferDetail.get("inIpsAcctNo"); //转入方IPS存管账号号
                                        ipsBillNo = mapTransferDetail.get("ipsBillNo"); //IPS订单号
                                        ipsDoTime = mapTransferDetail.get("ipsDoTime"); //IPS处理时间
                                        ipsTrdAmt = mapTransferDetail.get("ipsTrdAmt"); //IPS转账金额(String型)
                                        if (ipsTrdAmt != null && !ipsTrdAmt.equals("")) {
                                            ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //IPS转账金额(double型)
                                        }
                                        trdStatus = mapTransferDetail.get("trdStatus"); //转账状态(0-失败、1-成功)

                                        HuanxunTransferDetail huanxunTransferDetail = new HuanxunTransferDetail();
                                        UidTransferDetail = UUID.randomUUID().toString().replaceAll("-", ""); //主键(转账明细表)
                                        huanxunTransferDetail.setId(UidTransferDetail);
                                        huanxunTransferDetail.setTransferid(UidTransfer); //转账id
                                        huanxunTransferDetail.setMerbillno(merBillNo); //商户订单号
                                        huanxunTransferDetail.setOutipsacctno(outIpsAcctNo); //转出方IPS存管账户号
                                        huanxunTransferDetail.setInipsacctno(inIpsAcctNo); //转入方IPS存管账号号
                                        huanxunTransferDetail.setIpsbillno(ipsBillNo); //IPS订单号
                                        huanxunTransferDetail.setIpsdotime(ipsDoTime); //IPS处理时间
                                        huanxunTransferDetail.setIpstrdamt(ipsTrdAmtDou); //IPS转账金额
                                        huanxunTransferDetail.setTrdstatus(trdStatus); //转账状态(0-失败、1-成功)
                                        huanxunTransferDetail.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                        listHuanxunTransferDetail.add(huanxunTransferDetail);
                                    }
                                }
                            }
                            LOGGER.info("数组结束");
                        }
                    }
                }
                Boolean result = huanXunTransferService.saveHuanxunTransfer(huanxunTransfer,listHuanxunTransferDetail,receiveId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info(e.getMessage());
        }
    }


    /**
     * 根据冻结信息进行还款转账
     * @param request
     * @param response
     */
    @RequestMapping(value = "/repayhxPriceTransfer", method = RequestMethod.POST)
    @ResponseBody
    public void repayhxPriceTransfer(HttpServletRequest request,HttpServletResponse response) {
        System.out.println("根据冻结信息进行还款转账");
        String operationType = "trade.transfer"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String requesthx = ""; //请求信息
        String requesthxJm = ""; //请求信息(加密)
        String freezeRepayId = request.getParameter("freezeRepayId"); //环迅还款部分冻结主键
        String repayId = ""; //还款id
        String productId = ""; //产品id
        String yrepaymentTime = ""; //应还款日期
        String projectNo = ""; //项目ID号
        String freezeId = ""; //IPS原冻结订单号
        String transferType = "3"; //转账类型(3-还款)
        String s2SUrl = "http://120.76.137.147/huanxun/transferRepayResult"; //后台地址
        HuanXunFreezeMessageBean huanXunFreezeMessageBean = new HuanXunFreezeMessageBean(); //环迅冻结部分
        List<HuanXunTransferMessage> listHuanXunTransferMes = new ArrayList<HuanXunTransferMessage>();
        String batchNo = ""; //商户转账批次编号
        String merDate = ""; //转账日期
        String isAutoRepayment = "2"; //是否自动还款(1-是、2-否)
        String transferMode = "2"; //1-逐笔转账、2-批量转账
        String md5Zs = ""; //md5证书
        String transferAccDetail = ""; //转账明细集合
        String merBillNo = ""; //商户订单号
        String outIpsAcctNo = ""; //转出方IPS存管账户
        String outMerFee = "0"; //转出方平台手续费
        String inIpsAcctNo = ""; //转入方IPS存管账户
        String inMerFee = "0"; //转入方平台手续费
        String trdAmt = ""; //转账金额
        String transferId = ""; //转账表主键
        String url = "https://UFunds.ips.com.cn/p2p-deposit/gateway.htm"; //环迅接口调用地址
        String resultCode = ""; //响应状态
        String resultMsg = ""; //响应信息
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息
        String responsehx = ""; //返回信息

        /*************** 转账前备份部分Start ***************/
        Map<String,String> mapTransferBf = new HashMap<String,String>();
        List<Map<String,String>> listMapTransferDetailBf = new ArrayList<Map<String,String>>();
        /**************** 转账前备份部分End ****************/

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs(); //md5证书

            transferId = UUID.randomUUID().toString().replaceAll("-",""); //转账表主键

            System.out.println("准备查询还款冻结信息");
            huanXunFreezeMessageBean = huanXunFreezeService.queryFreezeRepay(freezeRepayId); //环迅冻结信息
            System.out.println("huanXunFreezeMessageBean==="+huanXunFreezeMessageBean);
            if (huanXunFreezeMessageBean != null){
                productId = huanXunFreezeMessageBean.getProductId(); //产品id
                yrepaymentTime = huanXunFreezeMessageBean.getyRepaymentTime(); //应还款日期
                repayId = huanXunFreezeMessageBean.getRepayId(); //还款id
                freezeId = huanXunFreezeMessageBean.getIpsBillNo(); //IPS原冻结订单号
                projectNo = huanXunFreezeMessageBean.getProjectNo(); //项目ID号
                batchNo = "181006" + UUID.randomUUID().toString().replaceAll("-",""); //商户转账批次编号
                merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //转账日期
                outIpsAcctNo = huanXunFreezeMessageBean.getIpsAcctNo(); //转出方IPS存管账户

                /*************** 转账信息备份Start ***************/
                mapTransferBf.put("transferId",transferId); //转账id
                mapTransferBf.put("batchNo",batchNo); //商户转账批次编号
                mapTransferBf.put("merDate",merDate); //转账日期
                mapTransferBf.put("projectNo",projectNo); //项目ID号
                mapTransferBf.put("transferType",transferType); //转账类型
                mapTransferBf.put("isAutoRepayment",isAutoRepayment); //是否自动还款(1-是、2-否)
                mapTransferBf.put("transferMode",transferMode); //转账类型(3-还款)
                /**************** 转账信息备份End ****************/

                System.out.println("productId==="+productId+",yrepaymentTime==="+yrepaymentTime);
                listHuanXunTransferMes = huanXunTransferService.getRepayTransferAcctDetail(productId,yrepaymentTime); //转账信息集合
                System.out.println("转账集合数量为："+listHuanXunTransferMes.size());

                if (listHuanXunTransferMes != null && listHuanXunTransferMes.size() > 0){
                    for (HuanXunTransferMessage huanXunTransferMessage : listHuanXunTransferMes){
                        merBillNo = "181006" + UUID.randomUUID().toString().replaceAll("-",""); //商户订单号
                        inIpsAcctNo = huanXunTransferMessage.getInIpsAcctNo(); //转入方IPS存管账户
                        trdAmt = huanXunTransferMessage.getTrdAmt(); //转账金额
                        inMerFee = huanXunTransferMessage.getInMerFee(); //转入方平台服务费
                        System.out.println("转入方平台服务费======"+inMerFee+",转账金额======"+trdAmt);

                        if (transferAccDetail == null || transferAccDetail.equals("")){
                            transferAccDetail = "{"
                                              + "\"merBillNo\":\"" + merBillNo + "\","
                                              + "\"freezeId\":\"" + freezeId + "\","
                                              + "\"outIpsAcctNo\":\"" + outIpsAcctNo + "\","
                                              + "\"outMerFee\":\"" + outMerFee + "\","
                                              + "\"inIpsAcctNo\":\"" + inIpsAcctNo + "\","
                                              + "\"inMerFee\":\"" + inMerFee + "\","
                                              + "\"trdAmt\":\"" + trdAmt + "\""
                                              + "}";
                        }else{
                            transferAccDetail += ",{"
                                              + "\"merBillNo\":\"" + merBillNo + "\","
                                              + "\"freezeId\":\"" + freezeId + "\","
                                              + "\"outIpsAcctNo\":\"" + outIpsAcctNo + "\","
                                              + "\"outMerFee\":\"" + outMerFee + "\","
                                              + "\"inIpsAcctNo\":\"" + inIpsAcctNo + "\","
                                              + "\"inMerFee\":\"" + inMerFee + "\","
                                              + "\"trdAmt\":\"" + trdAmt + "\""
                                              + "}";
                        }
                    }
                    s2SUrl = s2SUrl + "?transferId="+transferId+"&yrepaymentTime="+yrepaymentTime+"&repayId="+repayId; //后台通知地址

                    /*************** 转账详细信息Start ***************/
                    Map<String,String> mapTransferDetailBf = new HashMap<String,String>();
                    mapTransferDetailBf.put("merBillNo",merBillNo); //商户订单号
                    mapTransferDetailBf.put("freezeId",freezeId); //IPS原冻结订单号
                    mapTransferDetailBf.put("outIpsAcctNo",outIpsAcctNo); //转出方IPS存管账户号
                    mapTransferDetailBf.put("outMerFee",outMerFee); //转出方平台手续费
                    mapTransferDetailBf.put("inIpsAcctNo",inIpsAcctNo); //转入方IPS存管账户号
                    mapTransferDetailBf.put("inMerFee",inMerFee); //转入方平台手续费
                    mapTransferDetailBf.put("trdAmt",trdAmt); //转账金额
                    listMapTransferDetailBf.add(mapTransferDetailBf);
                    /**************** 转账详细信息End ****************/

                    String transferResult = TransferBf.transferResult(mapTransferBf,listMapTransferDetailBf); //转账后结果
                    if (transferResult != null && !transferResult.equals("")){
                        if (transferResult.equals("0")) { //成功
                            requesthx = "{"
                                    + "\"batchNo\":\"" + batchNo + "\","
                                    + "\"merDate\":\"" + merDate + "\","
                                    + "\"projectNo\":\"" + projectNo + "\","
                                    + "\"transferType\":\"" + transferType + "\","
                                    + "\"isAutoRepayment\":\"" + isAutoRepayment + "\","
                                    + "\"transferMode\":\"" + transferMode + "\","
                                    + "\"s2SUrl\":\"" + s2SUrl + "\","
                                    + "\"transferAccDetail\":"
                                    + "["
                                    + transferAccDetail
                                    + "]"
                                    + "}";

                            System.out.println("requesthx======"+requesthx);
                            requesthxJm = Pdes.encrypt3DES(requesthx); //3des加密
                            sign = operationType + merchantID + requesthxJm + md5Zs; //签名
                            signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密

                            List<HTTPParam> listHTTParam = new ArrayList<HTTPParam>();
                            HTTPParam httpParam1 = new HTTPParam("operationType", operationType); //操作类型
                            HTTPParam httpParam2 = new HTTPParam("merchantID", merchantID); //商户存管交易账号
                            HTTPParam httpParam3 = new HTTPParam("request", requesthxJm); //请求信息
                            HTTPParam httpParam4 = new HTTPParam("sign", signJm); //签名
                            listHTTParam.add(httpParam1);
                            listHTTParam.add(httpParam2);
                            listHTTParam.add(httpParam3);
                            listHTTParam.add(httpParam4);

                            String result = HttpRequest.sendPost(url, listHTTParam); //获取接口调用后返回信息

                            Map<String, String> map = JsonHelper.getObjectToMap(result); //将Json数据转换为Map格式
                            if (map != null) {
                                resultCode = map.get("resultCode"); //响应状态(000000-成功、999999-失败)
                                resultMsg = map.get("resultMsg"); //响应信息
                                responsehx = map.get("response"); //返回信息

                                mapException.put("resultCode",resultCode); //响应状态(000000-成功、999999-失败)
                                mapException.put("resultMsg",resultMsg); //响应信息
                                mapException.put("batchNo",batchNo); //商户转账批次号
                                mapException.put("projectNo",projectNo); //项目ID号
                                mapException.put("transferType",transferType); //转账类型(3-还款)
                                mapException.put("response",responsehx); //返回信息
                                mapException.put("freezeRepayId",freezeRepayId); //环迅冻结表主键

                                if (resultCode != null && !resultCode.equals("")){
                                    if (!resultCode.equals("000000")){
                                        HuanXunTransferRepayException.handle(mapException);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    System.out.println("未查询到任何结果");
                }
            }
        }catch (Exception e){
            HuanXunTransferRepayException.handle(mapException);
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("resultCode",resultCode);
        in.put("resultMsg",resultMsg);
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 还款转账后结果
     * @param request
     * @param response
     * @param modelMap
     */
    @RequestMapping(value = "/transferRepayResult", method = RequestMethod.POST)
    @ResponseBody
    public void transferRepayResult(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
        String freezeRepayId = request.getParameter("freezeRepayId"); //冻结还款id

        /******************** 服务器返回信息Start ********************/
        String resultCode = request.getParameter("resultCode"); //响应状态(000000-成功、999999-失败)
        String resultMsg = request.getParameter("resultMsg"); //响应消息描述
        String merchantID = request.getParameter("merchantID"); //商户存管交易账号
        String sign = request.getParameter("sign"); //签名
        String responsehx = request.getParameter("response"); //响应信息
        String responsehxJm = ""; //响应信息(解密)
        String batchNo = ""; //商户转账批次号
        String projectNo = ""; //项目ID号
        String transferType = ""; //转账类型
        String transferAccDetail = ""; //转账明细集合
        String merBillNo = ""; //商户订单号
        String outIpsAcctNo = ""; //转出方IPS存管账户号
        String inIpsAcctNo = ""; //转入方IPS存管账号
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String ipsTrdAmt = ""; //IPS转账金额(String型)
        Double ipsTrdAmtDou = 0.0d; //IPS转账金额(Double型)
        String trdStatus = ""; //转账状态(0-失败、1-成功)
        /********************* 服务器返回信息End *********************/

        String transferId = request.getParameter("transferId"); //转账id
        String productId = ""; //产品id
        String freezeId = ""; //冻结id
        String repayId = request.getParameter("repayId"); //还款id
        String yRepayTime = request.getParameter("yrepaymentTime"); //应还款日期
        Map map = new HashMap(); //Json集合
        Map<String,String> mapTransferDetail = new HashMap<String, String>(); //转账详细信息
        String UidTransferDetail = ""; //主键(转账详情)
        List<HuanXunTransferMessage> listHuanXunTransferMes = new ArrayList<HuanXunTransferMessage>(); //转账集合(消息对象)
        HuanxunTransfer huanxunTransfer = new HuanxunTransfer(); //转账集合(实体对象)
        List<HuanxunTransferDetail> listHuanxunTransferDetail = new ArrayList<HuanxunTransferDetail>(); //转账详情集合(实体对象)
        Map<String,Object> mapException = new HashMap<String,Object>(); //异常信息
        String result = "0"; //执行状态(0-成功、1-失败)

        try{

            output(response,"ipsCheckOk"); //接收成功

            HuanXunTransferMessage huanXunTransferMessage = new HuanXunTransferMessage();
            huanXunTransferMessage.setId(transferId); //主键
            listHuanXunTransferMes = huanXunTransferService.getTransferInfoBySome(huanXunTransferMessage);

            if (listHuanXunTransferMes.size() == 0) { //转账笔数
                if (responsehx != null && !responsehx.equals("")) { //响应信息
                    responsehxJm = Pdes.decrypt3DES(responsehx); //响应信息(解密)
                    if (responsehxJm != null && !responsehxJm.equals("")) {
                        map = JsonToMap.getMapFromJsonObjStr(responsehxJm);
                        if (map != null) {
                            batchNo = (String) map.get("batchNo"); //商户转账批次号
                            projectNo = (String) map.get("projectNo"); //项目ID号
                            transferType = (String) map.get("transferType"); //转账类型
                            huanxunTransfer.setId(transferId); //主键
                            huanxunTransfer.setResultcode(resultCode); //响应状态(000000-成功、999999-失败)
                            huanxunTransfer.setResultmsg(resultMsg); //响应消息描述
                            huanxunTransfer.setMerchantid(merchantID); //商户存管交易账号
                            huanxunTransfer.setBatchno(batchNo); //商户转账批次号
                            huanxunTransfer.setProjectno(projectNo); //项目ID号
                            huanxunTransfer.setTransfertype(transferType); //转账类型
                            huanxunTransfer.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间

                            JSONArray jsonArray = (JSONArray) map.get("transferAccDetail"); //转账明细集合(集合形式)
                            transferAccDetail = jsonArray.toString(); //转账明细集合(转账明细集合)
                            if (transferAccDetail != null && !transferAccDetail.equals("")) {
                                transferAccDetail = transferAccDetail.replaceAll("\\[", "");
                                transferAccDetail = transferAccDetail.replaceAll("\\]", "");
                                transferAccDetail = transferAccDetail.replaceAll("\\},\\{", "\\}、\\{");
                                String[] arr = transferAccDetail.split("、"); //数组(用于存放转账明细)
                                if (arr != null && arr.length > 0) {
                                    for (int i = 0; i < arr.length; i++) {
                                        mapTransferDetail = JsonHelper.getObjectToMap(arr[i]); //用于存放转账明细
                                        if (mapTransferDetail != null) {
                                            merBillNo = mapTransferDetail.get("merBillNo"); //商户订单号
                                            outIpsAcctNo = mapTransferDetail.get("outIpsAcctNo"); //转出方IPS存管账户号
                                            inIpsAcctNo = mapTransferDetail.get("inIpsAcctNo"); //转入方IPS存管账号号
                                            ipsBillNo = mapTransferDetail.get("ipsBillNo"); //IPS订单号
                                            ipsDoTime = mapTransferDetail.get("ipsDoTime"); //IPS处理时间

                                            ipsTrdAmt = mapTransferDetail.get("ipsTrdAmt"); //IPS转账金额(String型)
                                            if (ipsTrdAmt != null && !ipsTrdAmt.equals("")) {
                                                ipsTrdAmtDou = TypeTool.getDouble(ipsTrdAmt); //IPS转账金额(double型)
                                            }

                                            trdStatus = mapTransferDetail.get("trdStatus"); //转账状态(0-失败、1-成功)

                                            HuanxunTransferDetail huanxunTransferDetail = new HuanxunTransferDetail();
                                            UidTransferDetail = UUID.randomUUID().toString().replaceAll("-", ""); //主键(转账明细表)
                                            huanxunTransferDetail.setId(UidTransferDetail);
                                            huanxunTransferDetail.setTransferid(transferId); //转账id
                                            huanxunTransferDetail.setMerbillno(merBillNo); //商户订单号
                                            huanxunTransferDetail.setOutipsacctno(outIpsAcctNo); //转出方IPS存管账户号
                                            huanxunTransferDetail.setInipsacctno(inIpsAcctNo); //转入方IPS存管账号
                                            huanxunTransferDetail.setIpsbillno(ipsBillNo); //IPS订单号
                                            huanxunTransferDetail.setIpsdotime(ipsDoTime); //IPS处理时间
                                            huanxunTransferDetail.setIpstrdamt(ipsTrdAmtDou); //IPS转账金额
                                            huanxunTransferDetail.setTrdstatus(trdStatus); //转账状态(0-失败、1-成功)
                                            huanxunTransferDetail.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                            listHuanxunTransferDetail.add(huanxunTransferDetail);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                mapException.put("resultCode",resultCode); //响应状态(000000-成功、999999-失败)
                mapException.put("resultMsg",resultMsg); //响应信息
                mapException.put("batchNo",batchNo); //商户转账批次号
                mapException.put("projectNo",projectNo); //项目ID号
                mapException.put("transferType",transferType); //转账类型(3-还款)
                mapException.put("response",responsehx); //返回信息
                mapException.put("freezeRepayId",freezeRepayId); //环迅冻结表主键

                if (resultCode != null && !resultCode.equals("")){
                    if (!resultCode.equals("000000")){
                        HuanXunTransferRepayException.handle(mapException);
                    }
                }
                result = huanXunTransferService.repayhxPriceTransfer(huanxunTransfer,listHuanxunTransferDetail,mapException,repayId,yRepayTime);
            }
        }catch(Exception e){
            HuanXunTransferRepayException.handle(mapException);
            e.printStackTrace();
        }
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}