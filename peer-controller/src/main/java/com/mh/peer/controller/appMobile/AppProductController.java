package com.mh.peer.controller.appMobile;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.mh.peer.model.message.*;
import com.mh.peer.service.news.NewsService;
import com.mh.peer.service.platform.AdvertService;
import com.mh.peer.service.product.ProductAttornService;
import com.mh.peer.service.product.ProductReceiveService;
import com.mh.peer.service.product.ProductService;
import com.mh.peer.util.PublicAddress;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zhangerxin on 2016-6-17.
 */
@Controller
@RequestMapping("/app")
public class AppProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private AdvertService advertService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private ProductReceiveService productReceiveService;
    @Autowired
    private ProductAttornService productAttornService;

    /**
     * 获取首页内容
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getHomePage", method = RequestMethod.POST)
    @ResponseBody
    public void getHomePage(HttpServletRequest request,HttpServletResponse response){
        AppHomeMessageBean appHomeMessageBean = new AppHomeMessageBean();
        List<AppHomeMessageBean> listAppHomeMes = new ArrayList<AppHomeMessageBean>();
        String memberId = request.getParameter("memberId"); //会员id
        String lastDayPrice = "0"; //昨日收益
        String recommentProductStr = ""; //推荐项目
        String advertStr = ""; //广告
        String newStr = ""; //新闻
        String serverAddress = ""; //服务器地址

        PublicAddress publicAddress = new PublicAddress();
        serverAddress = publicAddress.getServerAddress();

        /******************** 昨日收益Start ********************/
        if(memberId != null && !memberId.equals("")){
            appHomeMessageBean.setMemberId(memberId); //会员id
            appHomeMessageBean = productReceiveService.getLastDayPrice(appHomeMessageBean);
            if (appHomeMessageBean != null){
                lastDayPrice = appHomeMessageBean.getLastDayPrice();
            }
        }
        /********************* 昨日收益End *********************/

        /******************** 推荐项目Start ********************/
        appHomeMessageBean = productService.getAppRecommentProduct(); //推荐产品部分
        recommentProductStr = "<tr onclick=\"doProductDetail('"+appHomeMessageBean.getProductId()+"')\">"
                            + "<td style=\"color:#B90D0C;\">"+appHomeMessageBean.getLoanRate()+"%</td>"
                            + "<td class=\"product_yuan\">"+appHomeMessageBean.getLoanPrice()+"元 &nbsp;&nbsp;"+appHomeMessageBean.getLoanTimeLength()+"</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>约定年化利率</td>"
                            + "<td class=\"btn_rx\"><input type=\"button\" value=\"热销中\" /></td>"
                            + "</tr>";
        /********************* 推荐项目End *********************/

        /******************** 广告部分Start ********************/
        listAppHomeMes = advertService.getHomeAdvert("SORT","ASC","1","5","1"); //广告集合
        if(listAppHomeMes != null && listAppHomeMes.size() > 0){
            for (AppHomeMessageBean appHomeMes : listAppHomeMes){
                advertStr = advertStr + "<td style=\"width:20%;\">"
                                      + "<dl>"
                                      + "<dt>"
                                      + "<a href=\"JavaScript:void(0)\" onclick=\"doSearchAdvertDetail('"+appHomeMes.getAdvertId()+"')\">"
                                      + "<img src=\""+serverAddress+appHomeMes.getAdvertFileUrl()+"\"/>"
                                      + "</a>"
                                      + "</dt>"
                                      + "<dd>"
                                      + "<div class=\"shop_title\">"
                                      + "<ul>"
                                      + "<li>"+appHomeMes.getAdvertTitle()+"</li>"
                                      + "</ul>"
                                      + "</div>"
                                      + "</dd>"
                                      + "</dl>"
                                      + "</td>";
            }
        }
        /********************* 广告部分End *********************/

        /******************** 新闻部分Start ********************/
        listAppHomeMes = newsService.getAppNews("create_date","DESC","1","8");
        if (listAppHomeMes != null && listAppHomeMes.size() > 0){
            for (AppHomeMessageBean appHomeMes : listAppHomeMes){
                newStr = newStr + "<li onclick=\"doSearchNewsDetail('"+appHomeMes.getNewsId()+"')\">"
                        + "<a href=\"JavaScript:void(0)\">"
                        + appHomeMes.getNewsTitle()
                        + "</a>"
                        + "<span class=\"caifu_span\">"
                        + appHomeMes.getNewsShowTime()
                        + "</span>"
                        + "</li>";
            }
        }
        /********************* 新闻部分End *********************/

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("lastDayPrice",lastDayPrice); //昨日收益
        in.put("recommentProduct", recommentProductStr); //推荐产品部分
        in.put("advertStr",advertStr); //广告部分
        in.put("newStr",newStr); //新闻部分
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取产品部分
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getProduct", method = RequestMethod.POST)
    @ResponseBody
    public void getProduct(HttpServletRequest request,HttpServletResponse response){
        String typeId = request.getParameter("typeId"); //产品类型id
        String typeName = ""; //产品类型名称
        String orderColumn = request.getParameter("orderColumn"); //排序字段
        String orderType = request.getParameter("orderType"); //排序类型
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String repayment = ""; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
        String repaymentName = ""; //还款方式名称
        String fullScale = ""; //是否满标(0-是、1-否)
        String flag = request.getParameter("flag"); //状态(0-上架、1-下架)
        AppProductMessageBean appProductMes = new AppProductMessageBean();
        List<AppProductMessageBean> listAppProductMes = new ArrayList<AppProductMessageBean>(); //产品集合
        String productStr = ""; //产品集合

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "3";
        }
        if (flag == null || flag.equals("")){ //状态(0-上架、1-下架)
            flag = "0";
        }

        appProductMes.setFlag(flag); //状态(0-上架、1-下架)
        appProductMes.setTypeId(typeId); //产品类型
        appProductMes.setOrderColumn(orderColumn); //排序字段
        appProductMes.setOrderType(orderType); //排序类型
        appProductMes.setPageIndex(pageIndex); //当前数
        appProductMes.setPageSize(pageSize); //每次显示数量

        listAppProductMes = productService.getAppProductList(appProductMes);
        if (listAppProductMes != null && listAppProductMes.size() > 0){
            for (AppProductMessageBean appProductMessageBean : listAppProductMes){
                repayment = appProductMessageBean.getRepayment(); //还款方式
                if (repayment != null && !repayment.equals("")){
                    if (repayment.equals("0")){ //按月还款、等额本息
                        repaymentName = "按月还款、等额本息";
                    }else if (repayment.equals("1")){
                        repaymentName = "按月付息、到期还本";
                    }else if (repayment.equals("2")){
                        repaymentName = "一次性还款";
                    }
                }

                typeId = appProductMessageBean.getTypeId(); //产品类型id
                if (typeId != null && !typeId.equals("")){
                    if (typeId.equals("26")){ //产品类型(车贷宝)
                        typeName = "车";
                    }else if (typeId.equals("27")){
                        typeName = "房";
                    }else if (typeId.equals("28")){
                        typeName = "融";
                    }else if (typeId.equals("29")){
                        typeName = "票";
                    }
                }

                productStr = productStr + "<div class=\"finac_info\" onclick=\"doProductDetail('"+appProductMessageBean.getId()+"')\">"
                           + "<div class=\"finac_con\">"
                           + "<div class=\"fi_title\">"
                           + "<p class=\"fi_che\">"+typeName+"</p>"
                           + "<ul class=\"fi_ul\">"
                           + "<li class=\"fi_yang\">"+appProductMessageBean.getTitle()+"</li>"
                           + "<li class=\"fi_hk\">还款方式："+repaymentName+"</li>"
                           + "</ul>"
                           + "</div>"
                           + "<table class=\"finac_table_sj\">"
                           + "<tr>"
                           + "<td><span class=\"ft_zhi\">"+appProductMessageBean.getLoanRate()+"</span>%</td>"
                           + "<td>借款期限："+appProductMessageBean.getLoanLength()+"</td>"
                           + "</tr>"
                           + "<tr>"
                           + "<td>年利率</td>"
                           + "<td>借款金额(元)：<span class=\"ft_zhi\">"+appProductMessageBean.getLoadFee()+"</span></td>"
                           + "</tr>"
                           + "</table>"
                           + "<div class=\"financ_btn_yqw\">";

                fullScale = appProductMessageBean.getFullScale(); //是否满标(0-是、1-否)
                if (fullScale != null && !fullScale.equals("")){
                    if (fullScale.equals("0")){ //满标
                        productStr = productStr + "<input type=\"button\" value=\"已抢完\" class=\"btn_yqw\" onclick=\"doProductDetail('\"+appProductMessageBean.getId()+\"')\"/>";
                    }else{
                        productStr = productStr + "<input type=\"button\" value=\"立即抢标\" class=\"btn_ljqb\" onclick=\"doProductDetail('\"+appProductMessageBean.getId()+\"')\"/>";
                    }
                }else{
                    productStr = productStr + "<input type=\"button\" value=\"立即抢标\" class=\"btn_ljqb\" onclick=\"doProductDetail('\"+appProductMessageBean.getId()+\"')\"/>";
                }
                productStr = productStr + "</div>"
                           + "</div>"
                           + "</div>";
            }
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("productStr",productStr); //产品部分拼接
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取产品详情
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getProductDetail", method = RequestMethod.POST)
    @ResponseBody
    public void getProductDetail(HttpServletRequest request,HttpServletResponse response){
        String productId = request.getParameter("productId"); //产品id
        String repaymentName = ""; //还款方式名称
        String fullScale = ""; //是否满标(0-是、1-否)
        String investProgress = ""; //进度
        String projectNo = ""; //项目ID号
        String productDetailStr = ""; //产品详解拼接
        AppProductDetailMessageBean appProductDetailMessageBean = productService.getProductDetail(productId);

        if(appProductDetailMessageBean.getRepayment() != null && !appProductDetailMessageBean.equals("")){
            if(appProductDetailMessageBean.getRepayment().equals("0")){
                repaymentName = "按月还款、等额本息";
            }else if(appProductDetailMessageBean.getRepayment().equals("1")){
                repaymentName = "按月付息、到期还本";
            }else if(appProductDetailMessageBean.getRepayment().equals("2")){
                repaymentName = "一次性还款";
            }
        }

        fullScale = appProductDetailMessageBean.getFullScale(); //是否满标
        investProgress = appProductDetailMessageBean.getInvestProgress(); //投标进度
        projectNo = appProductDetailMessageBean.getProjectNo(); //项目ID号

        productDetailStr = productDetailStr + "<input id=\"productId\" name=\"productId\" type=\"hidden\" value=\""+appProductDetailMessageBean.getId()+"\"/>"
                         + "<p class=\"p_d_title\">"+appProductDetailMessageBean.getTitle()+"</p>"
                         + "<div class=\"p_d_con\">"
                         + "<table class=\"p_detail_table\">"
                         + "<tr>"
                         + "<td style=\"text-align:left;\">"
                         + "<span class=\"p_d_t_txt\">剩余金额<b style=\"color: #C7C7C7;\">(元)</b></span>"
                         + "<span style=\"color:#FF2021;\">"+appProductDetailMessageBean.getSurplusPrice()+"</span>"
                         + "</td>"
                         + "<td style=\"text-align:center;\">"
                         + "<span class=\"p_d_sj\">"+appProductDetailMessageBean.getLoanRate()+"%</span>"
                         + "<span class=\"p_d_t_txt\" style=\"line-height:40px;\">年利率</span>"
                         + "</td>"
                         + "<td style=\"text-align:right;\">"
                         + "<span class=\"p_d_t_txt\">借款期限</span>"
                         + "<span class=\"p_d_t_txt\">"
                         + appProductDetailMessageBean.getLoanLength()
                         + "</span>"
                         + "</td>"
                         + "</tr>"
                         + "</table>"
                         + "<div class=\"p_d_biao\">"
                         + "<div id=\"indicatorContainerWrap\">"
                         + "<div class=\"rad-prg\" id=\"indicatorContainer\"></div>"
                         + "</div>"
                         + "</div>"
                         + "<span class=\"p_d_jd\" style=\" line-height:35px;\">投标进度</span>"
                         + "<table class=\"p_d_table2\">"
                         + "<tr>"
                         + "<td>借款编号：<span>"+appProductDetailMessageBean.getCode()+"</span></td>"
                         + "<td>需还本息(元)：<span>"+appProductDetailMessageBean.getRepayPrice()+"</span></td>"
                         + "</tr>"
                         + "<tr>"
                         + "<td>发标日期：<span>"+appProductDetailMessageBean.getFullTime()+"</span></td>"
                         + "<td>借款金额：<span>"+appProductDetailMessageBean.getLoanFee()+"</span></td>"
                         + "</tr>"
                         + "<tr>"
                         + "<td colspan=\"2\">还款方式：<span>"+repaymentName+"</span></td>"
                         + "</tr>"
                         + "</table>"
                         + "<table class=\"p_d_table3\">";


        if (fullScale != null && !fullScale.equals("")){
            if (fullScale.equals("0")){ //已满标
                productDetailStr = productDetailStr + "<tr>"
                                 + "<td>";

            }else{
                productDetailStr = productDetailStr + "<tr>"
                                 + "<td style=\"padding-top:10px;\">金额："
                                 + "<input type=\"text\" id=\"trdAmt\" placeholder=\"请输入您要投资的资金数额\" class=\"detail_txt\" />"
                                 + "</td>"
                                 + "</tr>"
                                 + "<tr>"
                                 + "<td>"
                                 + "<input type=\"button\" value=\"立即抢标\" class=\"btn_ljqb2\" id=\"buy_product\" onclick=\"doInvest()\"/>";
            }
        }else{
            productDetailStr = productDetailStr + "<tr>"
                             + "<td>";
        }

        productDetailStr = productDetailStr + "</td>"
                         + "</tr>"
                         + "</table>"
                         + "</div>";

        productDetailStr = productDetailStr + "<input id=\"projectNo\" name=\"projectNo\" type=\"hidden\" value=\""+projectNo+"\" />"
                         + "<input id=\"productId\" name=\"productId\" type=\"hidden\" value=\""+productId+"\"/>";

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("productDetailStr",productDetailStr); //产品详细信息部分拼接
        in.put("investProgress",investProgress); //投标进度
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取产品会员信息(App部分)
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getProductMemberInfo", method = RequestMethod.POST)
    @ResponseBody
    public void getProductMemberInfo(HttpServletRequest request,HttpServletResponse response){
        String productId = request.getParameter("productId"); //产品id
        AppProductMemberMessageBean appProductMemberMessageBean = productService.getProductMemberInfo(productId);
        String fullScale = ""; //是否满标(0-是、1-否)
        String sexName = ""; //性别名称
        String dregreeName = ""; //学历名称
        String childrenName = ""; //子女状况名称
        String carFlgName = ""; //是否有车名称
        String socailSecurityName = ""; //社保情况名称
        String marryStatusName = ""; //婚姻状况名称
        String houseConditionName = ""; //住房条件名称
        String monthSalaryName = ""; //月收入名称
        String memberStr = ""; //会员信息拼接
        String fkbzStr = ""; //风控步骤拼接

        if (appProductMemberMessageBean != null){
            if (appProductMemberMessageBean.getSex() != null && !appProductMemberMessageBean.getSex().equals("")){
                if (appProductMemberMessageBean.getSex().equals("0")){
                    sexName = "男";
                }else if (appProductMemberMessageBean.getSex().equals("1")){
                    sexName = "女";
                }
            }

            if (appProductMemberMessageBean.getDregree() != null && !appProductMemberMessageBean.getDregree().equals("")){
                if (appProductMemberMessageBean.getDregree().equals("0")){
                    dregreeName = "小学";
                }else if (appProductMemberMessageBean.getDregree().equals("1")){
                    dregreeName = "初中";
                }else if (appProductMemberMessageBean.getDregree().equals("2")){
                    dregreeName = "高中";
                }else if (appProductMemberMessageBean.getDregree().equals("3")){
                    dregreeName = "大专";
                }else if (appProductMemberMessageBean.getDregree().equals("4")){
                    dregreeName = "本科";
                }else if (appProductMemberMessageBean.getDregree().equals("5")){
                    dregreeName = "硕士";
                }else if (appProductMemberMessageBean.getDregree().equals("6")){
                    dregreeName = "博士";
                }else if (appProductMemberMessageBean.getDregree().equals("7")){
                    dregreeName = "博士后";
                }else if (appProductMemberMessageBean.getDregree().equals("8")){
                    dregreeName = "其他";
                }
            }

            if (appProductMemberMessageBean.getChildren() != null && !appProductMemberMessageBean.getChildren().equals("")){
                if (appProductMemberMessageBean.getChildren().equals("0")){
                    childrenName = "无";
                }else if (appProductMemberMessageBean.getChildren().equals("1")){
                    childrenName = "一个";
                }else if (appProductMemberMessageBean.getChildren().equals("2")){
                    childrenName = "两个";
                }else if (appProductMemberMessageBean.getChildren().equals("3")){
                    childrenName = "三个";
                }else if (appProductMemberMessageBean.getChildren().equals("4")){
                    childrenName = "三个以上";
                }
            }

            if (appProductMemberMessageBean.getCarFlg() != null && !appProductMemberMessageBean.getCarFlg().equals("")){
                if (appProductMemberMessageBean.getCarFlg().equals("0")){
                    carFlgName = "有";
                }else if (appProductMemberMessageBean.getCarFlg().equals("1")){
                    carFlgName = "无";
                }
            }

            if (appProductMemberMessageBean.getSocailSecurity() != null && !appProductMemberMessageBean.getSocailSecurity().equals("")){
                if (appProductMemberMessageBean.getSocailSecurity().equals("0")){
                    socailSecurityName = "无";
                }else if (appProductMemberMessageBean.getSocailSecurity().equals("1")){
                    socailSecurityName = "未缴满6个月";
                }else if (appProductMemberMessageBean.getSocailSecurity().equals("2")){
                    socailSecurityName = "缴满6个月以上";
                }
            }

            if (appProductMemberMessageBean.getMarryStatus() != null && !appProductMemberMessageBean.getMarryStatus().equals("")){
                if (appProductMemberMessageBean.getMarryStatus().equals("0")){
                    marryStatusName = "未婚";
                }else if (appProductMemberMessageBean.getMarryStatus().equals("1")){
                    marryStatusName = "已婚";
                }else if (appProductMemberMessageBean.getMarryStatus().equals("2")){
                    marryStatusName = "离婚";
                }else if (appProductMemberMessageBean.getMarryStatus().equals("3")){
                    marryStatusName = "丧偶";
                }
            }

            if (appProductMemberMessageBean.getHouseCondition() != null && !appProductMemberMessageBean.getHouseCondition().equals("")){
                if (appProductMemberMessageBean.getHouseCondition().equals("0")){
                    houseConditionName = "有商品房(无贷款)";
                }else if (appProductMemberMessageBean.getHouseCondition().equals("1")){
                    houseConditionName = "有商品房(有贷款)";
                }else if (appProductMemberMessageBean.getHouseCondition().equals("2")){
                    houseConditionName = "有其他(非商品)房";
                }else if (appProductMemberMessageBean.getHouseCondition().equals("3")){
                    houseConditionName = "与父母同住";
                }else if (appProductMemberMessageBean.getHouseCondition().equals("4")){
                    houseConditionName = "租房";
                }
            }

            if (appProductMemberMessageBean.getMonthSalary() != null && !appProductMemberMessageBean.getMonthSalary().equals("")){
                if (appProductMemberMessageBean.getMonthSalary().equals("0")){
                    monthSalaryName = "0-2000元以下";
                }else if (appProductMemberMessageBean.getMonthSalary().equals("1")){
                    monthSalaryName = "2000元-5000元";
                }else if (appProductMemberMessageBean.getMonthSalary().equals("2")){
                    monthSalaryName = "5000元-10000元";
                }else if (appProductMemberMessageBean.getMonthSalary().equals("3")){
                    monthSalaryName = "10000元以上";
                }
            }

            memberStr = memberStr + "<input id=\"productId\" name=\"productId\" type=\"hidden\" value=\""+productId+"\"/>"
                      + "<tr>"
                      + "<td colspan=\"2\">"
                      + "<span class=\"b_t_title\">&nbsp;&nbsp;借款人介绍</span>"
                      + "</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>姓名：</td>"
                      + "<td class=\"b_t_right\">"+appProductMemberMessageBean.getRealName()+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>性别：</td>"
                      + "<td class=\"b_t_right\">"+sexName+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<tr>"
                      + "<td>学历：</td>"
                      + "<td class=\"b_t_right\">"+dregreeName+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>电话：</td>"
                      + "<td class=\"b_t_right\">"+appProductMemberMessageBean.getTelephone()+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>子女状况：</td>"
                      + "<td class=\"b_t_right\">"+childrenName+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>是否有车：</td>"
                      + "<td class=\"b_t_right\">"+carFlgName+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>社保情况：</td>"
                      + "<td class=\"b_t_right\">"+socailSecurityName+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>婚姻状态：</td>"
                      + "<td class=\"b_t_right\">"+marryStatusName+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>住房条件：</td>"
                      + "<td class=\"b_t_right\">"+houseConditionName+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>月收入(元)：</td>"
                      + "<td class=\"b_t_right\">"+monthSalaryName+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td>邮箱：</td>"
                      + "<td class=\"b_t_right\">"+appProductMemberMessageBean.getEmail()+"</td>"
                      + "</tr>"
                      + "<tr>"
                      + "<td style=\"border:none;\">详细地址：</td>"
                      + "<td class=\"b_t_right\" style=\"border:none;\">"+appProductMemberMessageBean.getDetailAddress()+"</td>"
                      + "</tr>";

            fullScale = appProductMemberMessageBean.getFullScale(); //是否满标
            if (fullScale != null && !fullScale.equals("")){
                if (fullScale.equals("0")){ //满标
                    fkbzStr = "结论：<span style=\"color: #7c7c7c;\">经风控部综合评估，同意放款"+appProductMemberMessageBean.getLoadFee()+"元</span>";
                }
            }
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("memberStr",memberStr); //产品申请人信息部分拼接
        in.put("fkbzStr",fkbzStr); //风控步骤拼接
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取投资记录
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getProductInvestRecord", method = RequestMethod.POST)
    @ResponseBody
    public void getProductInvestRecord(HttpServletRequest request,HttpServletResponse response){
        String productId = request.getParameter("productId"); //产品id
        String pageIndex = request.getParameter("pageIndex"); //当前页
        String pageSize = request.getParameter("pageSize"); //每页显示数量
        AppProductBuyTjMessageBean appProductBuyTjMessageBean = productService.getProductBuyTj(productId); //投资金额部分统计
        String investSum = "0"; //投资总额
        String winvestSum = "0"; //未投资总额
        String productBuyStr = ""; //产品投资记录部分拼接
        if (appProductBuyTjMessageBean != null){
            investSum = appProductBuyTjMessageBean.getInvestSum(); //投资总额
            winvestSum = appProductBuyTjMessageBean.getWinvestSum(); //未投资总额
        }

        if (pageIndex != null && !pageIndex.equals("")){
            pageIndex = "1";
        }
        if (pageSize != null && !pageSize.equals("")){
            pageSize = "10";
        }
        List<AppProductBuyMessageBean> listAppProductBuyMes = productService.getProductBuyList(productId,pageIndex,pageSize);
        if (listAppProductBuyMes != null && listAppProductBuyMes.size() > 0){
            for (AppProductBuyMessageBean appProductBuyMessageBean : listAppProductBuyMes){
                productBuyStr = productBuyStr + "<tr>"
                              + "<td style=\"color:#FF2021;\">"+appProductBuyMessageBean.getBuyerName()+"</td>"
                              + "<td>"+appProductBuyMessageBean.getPrice()+"元</td>"
                              + "<td>"+appProductBuyMessageBean.getBuyTime()+"</td>"
                              + "</tr>";
            }
        }
        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("productBuyStr",productBuyStr); //产品投资记录部分拼接
        in.put("investSum",investSum); //投资总额
        in.put("winvestSum",winvestSum); //未投资金额
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 获取债权转让
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getProductAttorn", method = RequestMethod.POST)
    @ResponseBody
    public void getProductAttorn(HttpServletRequest request,HttpServletResponse response) {
        String pageIndex = request.getParameter("pageIndex"); //当前数
        String pageSize = request.getParameter("pageSize"); //每次显示数量
        String orderColumn = request.getParameter("orderColumn"); //排序字段
        String orderType = request.getParameter("orderType"); //排序类型
        String typeId = ""; //产品类型id
        String typeName = ""; //产品类型名称
        String shflag = ""; //审核状态(0-审核通过、1-审核不通过、2-未审核)
        String attornList = ""; //债权转让拼接

        if (orderColumn != null && !orderColumn.equals("")){
            if (orderColumn.equals("LOANRATE")){ //如果为年利率，则转换为预期收益率
                orderColumn = "EXPECTEDRATE";
            }else if (orderColumn.equals("LOANDAYS")){ //如果为期限，则转换为剩余期限
                orderColumn = "SURPLUSDAYS";
            }else if (orderColumn.equals("ONLINETIME")){ //如果为在线时间，则转换为创建时间
                orderColumn = "CREATETIME";
            }
        }

        List<AppProductAttornMessageBean> listAppProductAttornMes = productAttornService.getAppAttornList(null,null,pageIndex,pageSize,orderColumn,orderType);
        if (listAppProductAttornMes != null && listAppProductAttornMes.size()>0){
            for (AppProductAttornMessageBean appProductAttornMessageBean : listAppProductAttornMes){
                typeId = appProductAttornMessageBean.getTypeId(); //产品类型id
                shflag = appProductAttornMessageBean.getShFlag(); //审核状态
                if (typeId != null && !typeId.equals("")){
                    if (typeId != null && !typeId.equals("")){
                        if (typeId.equals("26")){ //产品类型(车贷宝)
                            typeName = "车";
                        }else if (typeId.equals("27")){
                            typeName = "房";
                        }else if (typeId.equals("28")){
                            typeName = "融";
                        }else if (typeId.equals("29")){
                            typeName = "票";
                        }
                    }
                }
                attornList = attornList + "<div class=\"finac_info\" onclick=\"doAttornDetail('"+appProductAttornMessageBean.getId()+"')\">"
                           + "<div class=\"finac_con\">"
                           + "<div class=\"fi_title\">"
                           + "<p class=\"fi_che\">"+typeName+"</p>"
                           + "<ul class=\"fi_ul\">"
                           + "<li class=\"fi_yang\">"+appProductAttornMessageBean.getTitle()+"</li>"
                           + "</ul>"
                           + "</div>"
                           + "<table class=\"finac_table_sj\">"
                           + "<tr>"
                           + "<td><span class=\"ft_zhi\">"+appProductAttornMessageBean.getExpectedRate()+"</span>%</td>"
                           + "<td>剩余期限：<b style=\"color:#FF2021;\">"+appProductAttornMessageBean.getSurplusDays()+"</b>天</td>"
                           + "</tr>"
                           + "<tr>"
                           + "<td>预期收益率</td>"
                           + "<td>转让金额(元)：<span class=\"ft_zhi\">"+appProductAttornMessageBean.getAttronPrice()+"</span></td>"
                           + "</tr>"
                           + "</table>"
                           + "<div class=\"financ_btn_yqw\">";

                if (shflag != null && !shflag.equals("")){
                    if (shflag.equals("0")){ //审核通过
                        attornList = attornList + "<input type=\"button\" value=\"已转让\" class=\"btn_yqw\" onclick=\"doAttornDetail('"+appProductAttornMessageBean.getId()+"')\"/>";
                    }else{
                        attornList = attornList + "<input type=\"button\" value=\"立即承接\" class=\"btn_ljqb\" onclick=\"doAttornDetail('"+appProductAttornMessageBean.getId()+"')\"/>";
                    }
                }
                attornList = attornList + "</div>"
                           + "</div>"
                           + "</div>";
            }
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("attornList",attornList); //债权转让拼接
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
