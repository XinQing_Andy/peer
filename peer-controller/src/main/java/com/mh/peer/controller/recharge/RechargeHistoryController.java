package com.mh.peer.controller.recharge;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.ViewHuanxunRechargeMes;
import com.mh.peer.service.recharge.RechargeHistoryService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.StringWriter;
import java.util.List;

/**
 * Created by Cuibin on 2016/7/27.
 */
@Controller
@RequestMapping("/rechargeHistory")
public class RechargeHistoryController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private RechargeHistoryService rechargeHistoryService;

    /**
     * 跳转到充值统计
     */
    @RequestMapping(value = "/queryRechargeHistory", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryRechargeHistory(@ModelAttribute ViewHuanxunRechargeMes viewHuanxunRechargeMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();

        String pageIndex = viewHuanxunRechargeMes.getPageIndex(); //当前数量
        String pageSize = viewHuanxunRechargeMes.getPageSize(); //每页显示数量
        String currentPage = viewHuanxunRechargeMes.getCurrentPage(); //当前页

        if (pageIndex == null || pageIndex.equals("")) {
            pageIndex = "0"; //当前数量
            currentPage = "1"; //当前页
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "10"; //每页显示数量
        }

        viewHuanxunRechargeMes.setPageIndex(pageIndex); //当前页
        viewHuanxunRechargeMes.setPageSize(pageSize); //每页显示数量

        List<ViewHuanxunRechargeMes> list = rechargeHistoryService.queryRechargeHistory(viewHuanxunRechargeMes);
        String count = rechargeHistoryService.queryRechargeHistoryCount(viewHuanxunRechargeMes);
        String pageStr = this.getShowPage(currentPage, Integer.parseInt(count)); //显示所属分页

        modelMap.put("totalCount", count); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr", pageStr); //显示所属分页
        modelMap.put("viewHuanxunRechargeList", list);

        try {
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/recharge/rechargeHistory.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }

    /**
     * 跳转到充值统计
     */
    @RequestMapping(value = "/queryRechargeHistoryByPage", method = RequestMethod.POST)
    @ResponseBody
    public ViewHuanxunRechargeMes queryRechargeHistoryByPage(@ModelAttribute ViewHuanxunRechargeMes viewHuanxunRechargeMes) {
        ViewHuanxunRechargeMes result = new ViewHuanxunRechargeMes();

        String pageIndex = viewHuanxunRechargeMes.getPageIndex(); //当前数量
        String pageSize = viewHuanxunRechargeMes.getPageSize(); //每页显示数量
        String currentPage = viewHuanxunRechargeMes.getCurrentPage(); //当前页

        if (pageIndex == null || pageIndex.equals("")) {
            pageIndex = "0"; //当前数量
            currentPage = "1"; //当前页
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "10"; //每页显示数量
        }

        viewHuanxunRechargeMes.setPageIndex(pageIndex); //当前页
        viewHuanxunRechargeMes.setPageSize(pageSize); //每页显示数量

        List<ViewHuanxunRechargeMes> list = rechargeHistoryService.queryRechargeHistory(viewHuanxunRechargeMes);
        String count = rechargeHistoryService.queryRechargeHistoryCount(viewHuanxunRechargeMes);
        String pageStr = this.getShowPage(currentPage, Integer.parseInt(count)); //显示所属分页
        String str = "";
        int i = 1;
        str = "<table class=\"table1\" style=\"border-collapse: collapse;\">" +
                "<tr class=\"table_first\">" +
                "<td style=\"width: 3%;\">序号</td>" +
                "<td>充值金额</td>" +
                "<td>充值时间</td>" +
                "<td>充值人</td>" +
                "<td>充值平台</td>" +
                "<td>身份证号</td>" +
                "</tr>";
        for (ViewHuanxunRechargeMes temp : list) {
            String source = temp.getSource();
            source = source.equals("0") ? "电脑端" : "手机端";
            str = str + "<tr>" +
                    "<td>" + i++ + "</td>" +
                    "<td>" + temp.getIpstrdamt() + "</td>" +
                    "<td>" + temp.getIpsdotime() + "</td>" +
                    "<td>" + temp.getRealname() + "</td>" +
                    "<td>" + source + "</td>" +
                    "<td>" + temp.getIdcard() + "</td>" +
                    "</tr>";

        }
        str = str + "</table>";
        if (str != null && !str.equals("")) {
            result.setTotalCount(count);
            result.setCurrentPage(currentPage);
            result.setPageStr(pageStr);
            result.setStr(str);
            result.setResult("success");
        } else {
            result.setResult("error");
        }

        return result;
    }

    /**
     * 获取需显示数字页码
     */
    public String getShowPage(String currentPage, int totalCount) {
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";
        if (currentPage != null && !currentPage.equals("")) { //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if (totalCount % 10 == 0) {
            totalPage = totalCount / 10;
        } else {
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if (totalPage <= 10) {
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        } else if (totalPage > 10) {
            if (currentin - 4 <= 1) {
                showStartPage = 1;
                showEndPage = 10;
            } else if (currentin - 4 > 1) {
                if (currentin + 5 > totalPage) {
                    showStartPage = currentin - (10 - (totalPage - currentin + 1));
                    showEndPage = totalPage;
                } else {
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for (int i = showStartPage; i <= showEndPage; i++) {
            if (currentin == i) {
                style = "style=\"color:#C63;font-weight:bold;\"";
            } else {
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGoPageRh('" + i + "')\" " + style + ">" + i + "</a></td>";
        }
        return str;
    }
}
