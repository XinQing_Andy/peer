package com.mh.peer.controller.member;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Cuibin on 2016/4/20.
 */
@Controller
@RequestMapping("/member")
public class AllMemberController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AllMemberController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private AllMemberService allMemberService;

    /**
     * 根据分页查询所有会员
     * @param memberInfoMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryAllMember", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAllMember(@ModelAttribute MemberInfoMessageBean memberInfoMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = memberInfoMes.getPageIndex(); //当前数量
        String pageSize = memberInfoMes.getPageSize(); //每页显示数量
        String currentPage = memberInfoMes.getCurrentPage(); //当前页

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
        }

        memberInfoMes.setPageSize(pageIndex); //当前数量
        memberInfoMes.setPageSize(pageSize); //每页显示数量
        memberInfoMes.setCurrentPage(currentPage); //当前页

        List<MemberInfoMessageBean> memberInfoList = allMemberService.queryMemberBysome(memberInfoMes); //会员集合
        modelMap.put("memberInfoList", memberInfoList); //会员集合

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/member/member.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }
    /**
     * 查询所有会员 cuibin
     */
    @RequestMapping(value = "/queryAllMemberBypage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAllMemberBypage(@ModelAttribute MemberInfoMessageBean memberInfoMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = memberInfoMes.getPageIndex(); //当前数量
        String pageSize = memberInfoMes.getPageSize(); //每页显示数量
        String currentPage = memberInfoMes.getCurrentPage(); //当前页
        String keyWord = memberInfoMes.getKeyWord();//搜索关键字
        String sort = memberInfoMes.getSort();//排序
        String startTime = memberInfoMes.getStartTime();//开始时间
        String endTime = memberInfoMes.getEndTime();//结束时间
        String isHome = memberInfoMes.getIsHome() == null ? "1" : memberInfoMes.getIsHome();//是否首页点击

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
        }
        if(keyWord != null && !"".equals(keyWord)){
            modelMap.put("keyWord", keyWord);
        }
        if(sort != null && !"".equals(sort)){
            modelMap.put("sort", sort);
        }
        if(startTime != null && !"".equals(startTime)){
            modelMap.put("startTime", startTime);
            startTime = startTime.replaceAll("-","");
            memberInfoMes.setStartTime(startTime);
        }
        if(endTime != null && !"".equals(endTime)){
            modelMap.put("endTime", endTime);
            endTime = endTime.replaceAll("-","");
            memberInfoMes.setEndTime(endTime);
        }
        memberInfoMes.setPageIndex(pageIndex); //当前数量
        memberInfoMes.setPageSize(pageSize); //每页显示数量
       // memberInfoMes.setCurrentPage(currentPage); //当前页

        List<MemberInfoMessageBean> memberInfoList = allMemberService.queryAllMember(memberInfoMes); //会员集合
        int count = allMemberService.queryAllMemberCount(memberInfoMes);
        String pageStr = this.getShowPage(currentPage, count);
        modelMap.put("memberInfoList", memberInfoList); //会员集合
        modelMap.put("pageStr", pageStr);
        modelMap.put("totalCount", count); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        //模板解析方法
        try {
            StringWriter stringWriter = null;
            //模板渲染
            if(isHome.equals("1")){
                stringWriter = jetbrickTool.getJetbrickTemp("/member/member.jetx", modelMap);
            }else{
                stringWriter = jetbrickTool.getJetbrickTemp("/member/member_info.jetx", modelMap);
            }

            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }

    /**
     * 查询一个 会员
     */
    @RequestMapping(value = "/queryOnlyMember", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean queryOnlyMember(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean) {
        return allMemberService.queryOnlyMember(memberInfoMessageBean);
    }

    /**
     * 锁定 会员
     */
    @RequestMapping(value = "/lockMember", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean lockMember(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean) {
        return allMemberService.lockMember(memberInfoMessageBean);
    }

    /**
     * 更新密码
     */
    @RequestMapping(value = "/updateMemberPassWord", method = RequestMethod.POST)
    @ResponseBody
    public MemberInfoMessageBean updateMemberPassWord(@ModelAttribute MemberInfoMessageBean memberInfoMessageBean) {
        return allMemberService.updateMemberPassWord(memberInfoMessageBean);
    }
    /**
     * 查询所有已锁定会员 cuibin
     */
    @RequestMapping(value = "/queryAllLockMember", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryAllLockMember(@ModelAttribute MemberInfoMessageBean memberInfoMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = memberInfoMes.getPageIndex(); //当前数量
        String pageSize = memberInfoMes.getPageSize(); //每页显示数量
        String currentPage = memberInfoMes.getCurrentPage(); //当前页
        String keyWord = memberInfoMes.getKeyWord();//搜索关键字
        String sort = memberInfoMes.getSort();//排序
        String startTime = memberInfoMes.getStartTime();//开始时间
        String endTime = memberInfoMes.getEndTime();//结束时间
        String lockFlg = memberInfoMes.getLockFlg();//是否锁定
        String isHome = memberInfoMes.getIsHome() == null ? "1" : memberInfoMes.getIsHome();//是否首页点击

        if(lockFlg == null || lockFlg.equals("")){
            lockFlg = "0";
            memberInfoMes.setLockFlg(lockFlg);
        }
        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
        }
        if(keyWord != null && !"".equals(keyWord)){
            modelMap.put("keyWord", keyWord);
        }
        if(sort != null && !"".equals(sort)){
            modelMap.put("sort", sort);
        }
        if(startTime != null && !"".equals(startTime)){
            modelMap.put("startTime", startTime);
            startTime = startTime.replaceAll("-","");
            memberInfoMes.setStartTime(startTime);
        }
        if(endTime != null && !"".equals(endTime)){
            modelMap.put("endTime", endTime);
            endTime = endTime.replaceAll("-","");
            memberInfoMes.setEndTime(endTime);
        }
        memberInfoMes.setPageSize(pageIndex); //当前数量
        memberInfoMes.setPageSize(pageSize); //每页显示数量
        // memberInfoMes.setCurrentPage(currentPage); //当前页

        List<MemberInfoMessageBean> memberInfoList = allMemberService.queryAllMember(memberInfoMes); //会员集合
        int count = allMemberService.queryAllMemberCount(memberInfoMes);
        String pageStr = this.getShowPage2(currentPage, count);
        modelMap.put("memberInfoList", memberInfoList); //会员集合
        modelMap.put("pageStr", pageStr);
        modelMap.put("totalCount", count); //数据数量
        modelMap.put("currentPage", currentPage); //当前页
        //模板解析方法
        try {
            StringWriter stringWriter = null;
            //模板渲染
            if(isHome.equals("1")){
                stringWriter = jetbrickTool.getJetbrickTemp("/member/member_locked.jetx", modelMap);
            }else{
                stringWriter = jetbrickTool.getJetbrickTemp("/member/member_locked_info.jetx", modelMap);
            }
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }

    /**
     * 获取需显示数字页码
     */
    public String getShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGoMemPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }

    /**
     * 获取需显示数字页码
     */
    public String getShowPage2(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 <= 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = currentin - (10 -(totalPage - currentin + 1));
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doGoMemlPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }
}
