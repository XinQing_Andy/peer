package com.mh.peer.controller.appMobile;

import com.mh.peer.model.message.AppPostalMessageBean;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.mh.peer.service.front.CashService;
import com.mh.peer.service.huanxun.HuanXunPostalService;
import com.mh.peer.service.huanxun.HuanXunRegistService;
import com.mh.peer.util.HuanXunInfo;
import com.mh.peer.util.MD5Util;
import com.mh.peer.util.Pdes;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-8-15.
 * App提现部分
 */
@Controller
@RequestMapping("/appPostal")
public class AppPostalController {

    @Autowired
    private CashService cashService;
    @Autowired
    private HuanXunRegistService huanXunRegistService;
    @Autowired
    private HuanXunPostalService huanXunPostalService;

    /**
     * 获取可提现的金额
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getPostaledPrice", method = RequestMethod.POST)
    @ResponseBody
    public void getPostaledPrice(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String postaledPrice = "0"; //可提现金额
        try{
            postaledPrice = cashService.getPostaledPrice(memberId);
        }catch(Exception e){
            e.printStackTrace();
        }

        HashMap<String,Object> in = null;
        in = new HashMap<String,Object>();
        in.put("postaledPrice",postaledPrice); //会员id
        output(response, JSONObject.fromObject(in).toString());
    }


    /**
     * 提现处理
     * @param request
     * @param response
     */
    @RequestMapping(value = "/postalHandle", method = RequestMethod.POST)
    @ResponseBody
    public void postalHandle(HttpServletRequest request,HttpServletResponse response){
        String memberId = request.getParameter("memberId"); //会员id
        String operationType = "trade.withdraw"; //操作类型
        String merchantID = "1810060028"; //商户存管交易账号
        String merBillNo = "181006"; //商户订单号
        String merDate = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd"); //提现日期
        String userType = "1"; //用户类型(1-个人、2-企业)
        String trdAmt = request.getParameter("trdAmt"); //提现金额
        String merFee = "0"; //平台手续费
        String merFeeType = "1"; //平台手续费类型(1-内扣、2-外扣)
        String ipsFeeType = "1"; //IPS手续费承担方(1-平台承担、2-用户承担)
        String ipsAcctNo = ""; //IPS存管账户号
        String webUrl = "http://jianbing88.com/appPostal/postalWebResult"; //浏览器返回地址
        String s2SUrl = "http://120.76.137.147/huanxun/postalhxResult"; //后台通知地址
        String sign = ""; //签名
        String signJm = ""; //签名(加密)
        String requestapphx = ""; //请求信息
        String requestapphxJm = ""; //请求信息(加密)
        String md5Zs = ""; //md5证书
        String glId = ""; //关联id
        String resultHx = "0"; //环迅结果(0-正常、1-未登录、2-未注册环迅)

        try{

            HuanXunInfo huanXunInfo = new HuanXunInfo();
            md5Zs = huanXunInfo.getMd5Zs(); //md5证书

            glId = UUID.randomUUID().toString().replaceAll("-",""); //关联id

            if (memberId != null && !memberId.equals("")){ //会员id
                webUrl = webUrl + "?memberId="+memberId; //浏览器返回地址
                s2SUrl = s2SUrl + "?memberId="+memberId+"&glId="+glId+"&source=0"; //后台通知地址

                HuanXunRegistMessageBean huanXunRegistMessageBean = new HuanXunRegistMessageBean();
                huanXunRegistMessageBean.setMemberId(memberId); //会员id
                //查询会员注册信息
                List<HuanXunRegistMessageBean> listHuanXunRegistMes = huanXunRegistService.getRegisterhxBySome(huanXunRegistMessageBean);
                if (listHuanXunRegistMes == null || listHuanXunRegistMes.size() == 0){
                    resultHx = "2"; //未注册环迅
                }else{
                    ipsAcctNo = listHuanXunRegistMes.get(0).getIpsAcctNo(); //IPS存管账号

                    merBillNo = merBillNo + UUID.randomUUID().toString().replaceAll("-","").substring(0,10); //商户订单号

                    requestapphx = "{\"merBillNo\":\""+merBillNo+"\",\"merDate\":\""+merDate+"\",\"userType\":\""+userType+"\","
                                 + "\"trdAmt\":\""+trdAmt+"\",\"merFee\":\""+merFee+"\",\"merFeeType\":\""+merFeeType+"\","
                                 + "\"ipsFeeType\":\""+ipsFeeType+"\",\"ipsAcctNo\":\""+ipsAcctNo+"\",\"webUrl\":\""+webUrl+"\","
                                 + "\"s2SUrl\":\""+s2SUrl+"\"}";

                    requestapphxJm = Pdes.encrypt3DES(requestapphx); //3des加密
                    sign = operationType + merchantID + requestapphxJm + md5Zs; //签名
                    signJm = MD5Util.MD5(sign).toLowerCase(); //签名加密
                }
            }else{
                resultHx = "1"; //未登录
            }

            HashMap<String,Object> in = null;
            in = new HashMap<String,Object>();
            in.put("sign",signJm); //签名
            in.put("request",requestapphxJm); //请求信息
            output(response, JSONObject.fromObject(in).toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 提现返回页面
     * @param request
     * @param response
     */
    @RequestMapping(value = "/postalWebResult", method = RequestMethod.POST)
    @ResponseBody
    public void postalWebResult(HttpServletRequest request,HttpServletResponse response){
        try{
            response.sendRedirect("postalWait"); //返回提现等待页面
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 环迅等待界面
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/postalWait", method = RequestMethod.GET)
    public String postalWait(HttpServletRequest request,HttpServletResponse response){
        return "appMobile/postal";
    }



    /**
     * 输出结果到response中
     * @param response
     * @param str
     */
    private void output(HttpServletResponse response, String str) {
        try {
            response.getOutputStream().write(str.getBytes("UTF-8") );
            response.getOutputStream().flush();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
