package com.mh.peer.controller.sys;

import com.mh.peer.model.business.LoginBusinessBean;
import com.mh.peer.model.entity.SysRole;
import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.*;
import com.mh.peer.service.sys.SysRoleService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.TypeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016/1/27.
 */
@Controller
@RequestMapping("/manage")
public class SysRoleController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SysRoleController.class);
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private SysRoleService sysRoleService;


    /**
     * 根据分页获取用户组信息(zhangerxin)
     * @param sysRolMese
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryRoleByPage", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryRoleByPage(@ModelAttribute SysRoleMessageBean sysRolMese, ModelMap modelMap){
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = sysRolMese.getPageIndex(); //当前数
        String pageSize = sysRolMese.getPageSize(); //每页显示数量
        String currentPage = sysRolMese.getCurrentPage(); //当前页
        String roleName = sysRolMese.getRoleName(); //组名

        if(pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if(pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
        }
        if(roleName != null && !roleName.equals("")){ //角色名称
            modelMap.addAttribute("roleName",roleName);
        }

        sysRolMese.setPageIndex(pageIndex); //当前页
        sysRolMese.setPageSize(pageSize); //每页显示数量
        List<SysRoleMessageBean> roleList = sysRoleService.getRoleInfoByPage(sysRolMese); //用户组集合(根据分页查询)
        int totalCount = sysRoleService.getCountSysRoleByPage(sysRolMese); //用户组数量
        String pageStr = this.getShowPage(currentPage,totalCount); //显示所属分页拼接

        //将list传递给前台渲染
        modelMap.put("roleList", roleList); //用户组集合
        modelMap.put("totalCount", totalCount); //用户组数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageStr",pageStr); //显示所属分页拼接

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(sysRolMese.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        }catch (Exception e){
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 根据id查询用户组信息
     * @param sysRolMese
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryRoleById", method = RequestMethod.POST)
    @ResponseBody
    public SysRoleMessageBean queryRoleById(@ModelAttribute SysRoleMessageBean sysRolMese, ModelMap modelMap){
        SysRoleMessageBean sysRoleMes = sysRoleService.queryRoleById(sysRolMese); //用户组集合(根据id查询)
        return sysRoleMes;
    }



    /**
     * 新增用户组
     * @return
     */
    @RequestMapping(value = "/addRole", method = RequestMethod.POST)
    @ResponseBody
    public SysRoleMessageBean onAddRole(@ModelAttribute SysRoleMessageBean role){
        role.setCreateUserId(servletUtil.getSession().getId());
        LoginBusinessBean loginBusinessBean = sysRoleService.addRole(role); //新增用户组
        role.setResult(loginBusinessBean.getResult());
        role.setInfo(loginBusinessBean.getInfo());
        return role;
    }

    /**
     * 删除用户
     * @param role
     * @return
     */
    @RequestMapping(value = "/delRole", method = RequestMethod.POST)
    @ResponseBody
    public SysRoleMessageBean onDelRole(@ModelAttribute SysRoleMessageBean role){
        role.setCreateUserId(servletUtil.getSession().getId());
        LoginBusinessBean loginBusinessBean = sysRoleService.delRole(role);
        role.setResult(loginBusinessBean.getResult());
        role.setInfo(loginBusinessBean.getInfo());
        return role;
    }

    /**
     * 修改用户组信息
     * @param role
     * @return
     */
    @RequestMapping(value = "/updateRole", method = RequestMethod.POST)
    @ResponseBody
    public SysRoleMessageBean onUpRole(@ModelAttribute SysRoleMessageBean role){
        role.setCreateUserId(servletUtil.getSession().getId());
        LoginBusinessBean loginBusinessBean = sysRoleService.updateRole(role);
        role.setResult(loginBusinessBean.getResult());
        role.setInfo(loginBusinessBean.getInfo());
        return role;
    }


    /**
     * 跳转到权限设置 by zhangerxin
     */
    @RequestMapping(value = "/openMenuSet", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openMenuSet(@ModelAttribute SysRoleMessageBean sysRoleMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        List<SysMenuMessageBean> listSysMenuMes = sysRoleService.queryAllMenu(); //一级菜单集合
        List<SysRoleMenuMessBean> listSysRoleMenuMes = sysRoleService.getAllRoleMenu(sysRoleMes.getId()); //获取角色菜单
        List<String> listRoleMenu = new ArrayList<String>();
        String menuFirstStr = ""; //一级菜单部分拼接
        String menuSecondStr = ""; //二级菜单部分拼接
        String menuThirdStr = ""; //三级菜单部分拼接
        String menuStr = ""; //菜单部分拼接
        int firstMenuId = 0; //一级菜单id
        int secondMenuId = 0; //二级菜单id
        int thirdMenuId = 0; //三级菜单id
        String classFirstProperty = ""; //一级菜单样式属性
        String classSecondProperty = ""; //二级菜单样式属性
        String classThirdProperty = ""; //三级菜单样式属性
        int rowspanFirst = 1; //一级菜单行高
        int rowspanSecond = 1;  //二级菜单行高
        int rowspanThird = 1;  //三级菜单行高
        int secondNum = 0; //第二级菜单部分序号
        String roleMenuId = ""; //角色菜单id

        /*************** 角色菜单部分Start **************/
        if (listSysRoleMenuMes != null && listSysRoleMenuMes.size() > 0){
            for (SysRoleMenuMessBean sysRoleMenuMessBean : listSysRoleMenuMes){
                listRoleMenu.add(sysRoleMenuMessBean.getMenuId()); //菜单id
            }
        }
        /**************** 角色菜单部分End ***************/

        if (listSysMenuMes != null && listSysMenuMes.size() > 0){
            /********** 一级菜单部分循环Start **********/
            for (SysMenuMessageBean firstSysMenuMes : listSysMenuMes){
                menuFirstStr = "<tr>"; //一级菜单部分拼接
                firstMenuId = firstSysMenuMes.getId(); //一级菜单id
                if (listRoleMenu != null && listRoleMenu.size() > 0){
                    if (listRoleMenu.contains(TypeTool.getString(firstMenuId))){
                        classFirstProperty = "style=\"background-color:#F25D5C\""; //样式属性
                        roleMenuId = roleMenuId + "," + firstMenuId; //角色菜单权限
                    }else{
                        classFirstProperty = "";
                    }
                }

                //二级菜单集合
                List<SysMenuMessageBean> listSysMenuMesSecond = sysRoleService.queryAllMenuByFid(TypeTool.getString(firstMenuId));
                rowspanFirst = sysRoleService.queryAllMenuByFidCount(TypeTool.getString(firstMenuId)); //一级菜单行高

                //一级菜单部分拼接
                menuFirstStr = "<td rowspan=\""+rowspanFirst+"\">"
                             + "<label>"
                             + "<span>"+firstSysMenuMes.getMenuName()+"</span>"
                             + "<i id=\""+firstSysMenuMes.getId()+"\" class=\"squar\" "+classFirstProperty+" onclick=\"doClickPower('"+firstSysMenuMes.getId()+"');\"></i>"
                             + "</label>"
                             + "</td>";

                menuStr = menuStr + menuFirstStr;  //菜单部分拼接
                if (listSysMenuMesSecond != null && listSysMenuMesSecond.size() > 0){
                    secondNum = 0; //第二级菜单部分序号
                    /********** 二级菜单部分循环Start **********/
                    for (SysMenuMessageBean secondSysMenuMes : listSysMenuMesSecond){
                        if (secondNum > 0){
                            menuStr = menuStr + "<tr>";
                        }
                        secondMenuId = secondSysMenuMes.getId(); //二级菜单id
                        if (listRoleMenu != null && listRoleMenu.size() > 0){
                            if (listRoleMenu.contains(TypeTool.getString(secondMenuId))){
                                classSecondProperty = "style=\"background-color:#F25D5C\""; //样式属性
                                roleMenuId = roleMenuId + "," + secondMenuId; //角色菜单权限
                            }else{
                                classSecondProperty = "";
                            }
                        }

                        //二级菜单部分拼接
                        menuSecondStr = "<td>"
                                      + "<label>"
                                      + "<i id=\""+secondSysMenuMes.getId()+"\" class=\"squar\" "+classSecondProperty+" onclick=\"doClickPower('"+secondSysMenuMes.getId()+"')\">"
                                      + "</i>"
                                      + "<span>"+secondSysMenuMes.getMenuName()+"</span>"
                                      + "</label>"
                                      + "</td>";
                        menuStr = menuStr + menuSecondStr;

                        //三级菜单集合
                        List<SysMenuMessageBean> listSysMenuMesThird = sysRoleService.queryAllMenuByFid(TypeTool.getString(secondMenuId));
                        if (listSysMenuMesThird != null && listSysMenuMesThird.size() > 0){
                            menuStr = menuStr + "<td colspan=\"4\">";
                            /********** 三级菜单部分循环Start **********/
                            for (SysMenuMessageBean thirdSysMenuMes : listSysMenuMesThird){
                                thirdMenuId = thirdSysMenuMes.getId(); //三级菜单id
                                if (listRoleMenu != null && listRoleMenu.size() > 0){
                                    if (listRoleMenu.contains(TypeTool.getString(thirdMenuId))){
                                        classThirdProperty = "style=\"background-color:#F25D5C\""; //样式属性
                                        roleMenuId = roleMenuId + "," + thirdMenuId; //角色菜单权限
                                    }else{
                                        classThirdProperty = "";
                                    }
                                }

                                //三级菜单部分拼接
                                menuThirdStr = "<label class=\"power_label\">"
                                             + "<i id=\""+thirdSysMenuMes.getId()+"\" class=\"squar\" "+classThirdProperty+" onclick=\"doClickPower('"+thirdSysMenuMes.getId()+"')\">"
                                             + "</i>"
                                             + "<span>"
                                             + ""+thirdSysMenuMes.getMenuName()+""
                                             + "</span>"
                                             + "</label>";

                                menuStr = menuStr + menuThirdStr;
                            }
                            /*********** 三级菜单部分循环End ***********/
                            menuStr = menuStr + "</td>";
                        }else{
                            menuThirdStr = "<td colspan=\"4\"></td>"; //三级菜单部分拼接
                            menuStr = menuStr + menuThirdStr;  //菜单部分
                        }

                        menuStr = menuStr + "</tr>"; //菜单部分拼接
                        secondNum++;  //二级菜单序号
                    }
                    /*********** 二级菜单部分循环End ***********/
                }else{
                    //二级菜单部分拼接
                    menuSecondStr = "<td></td>"
                                  + "<td colspan=\"4\"></td>"
                                  + "</tr>";
                    menuStr = menuStr + menuSecondStr;
                }
            }
            /*********** 一级菜单部分循环End ***********/
        }

        if (roleMenuId != null && !roleMenuId.equals("")){
            roleMenuId = roleMenuId.substring(1);
        }

        modelMap.put("menuStr",menuStr); //菜单部分拼接
        modelMap.put("roleId",sysRoleMes.getId()); //角色id
        modelMap.put("roleMenuId",roleMenuId); //角色菜单

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp("/sys/system_menu_set.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            LOGGER.error("模板解析失败！");
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 保存角色权限
     * @param sysRoleMenuMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/saveRolePower", method = RequestMethod.POST)
    @ResponseBody
    public SysRoleMenuMessBean saveRolePower(@ModelAttribute SysRoleMenuMessBean sysRoleMenuMes, ModelMap modelMap) {
        SysRoleMenuMessBean sysRoleMenuMessBean = sysRoleService.saveRolePower(sysRoleMenuMes);
        return sysRoleMenuMessBean;
    }



    /**
     * 获取需显示数字页码
     * @param currentPage
     * @param totalCount
     * @return
     */
    public String getShowPage(String currentPage,int totalCount){
        int currentin = 0; //当前页(整型)
        int totalPage = 0; //总页数
        int showStartPage = 0; //起始页
        int showEndPage = 0; //终止页
        String str = "";
        String style = "";

        if(currentPage != null && !currentPage.equals("")){ //当前页
            currentin = Integer.parseInt(currentPage);
        }

        /********** 获取总页数Start **********/
        if(totalCount%10 == 0){
            totalPage = totalCount / 10;
        }else{
            totalPage = totalCount / 10 + 1;
        }
        /*********** 获取总页数End ***********/

        if(totalPage <= 10){
            showStartPage = 1; //起始页
            showEndPage = totalPage; //终止页
        }else if(totalPage > 10){
            if(currentin - 4 < 1){
                showStartPage = 1;
                showEndPage = 10;
            }else if(currentin - 4 > 1){
                if(currentin + 5 > totalPage){
                    showStartPage = 10 -(totalPage - currentin + 1);
                    showEndPage = totalPage;
                }else{
                    showStartPage = currentin - 4;
                    showEndPage = currentin + 5;
                }
            }
        }

        for(int i=showStartPage;i<=showEndPage;i++){
            if(currentin == i){
                style = "style=\"color:#C63;font-weight:bold;\"";
            }else{
                style = "";
            }
            str = str + "<td style=\"width: 8%;\">"
                    + "<a href=\"JavaScript:void(0)\" onclick=\"doSysRoleGoPage('"+i+"')\" "+style+">"+i+"</a></td>";
        }
        return str;
    }
}
