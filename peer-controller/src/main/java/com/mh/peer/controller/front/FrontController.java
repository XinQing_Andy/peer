package com.mh.peer.controller.front;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.mh.peer.service.front.FrontService;
import com.mh.peer.service.front.SecuritySettingsService;
import com.mh.peer.service.huanxun.HuanXunFreezeService;
import com.mh.peer.service.member.AllMemberService;
import com.mh.peer.service.news.NewsService;
import com.mh.peer.service.platform.AdvertService;
import com.mh.peer.service.product.ProductAttornService;
import com.mh.peer.service.product.ProductService;
import com.mh.peer.service.product.ProductTypeService;
import com.mh.peer.service.sys.SysFileService;
import com.mh.peer.util.*;
import com.salon.frame.data.DMap;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by zhangerxin on 2016-4-15.
 */
@Controller
@RequestMapping("/front")
public class FrontController {

    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;
    @Autowired
    private ProductService productService;
    @Autowired
    private AdvertService advertService;
    @Autowired
    private SecuritySettingsService securitySettingsService;
    @Autowired
    private AllMemberService allMemberService;
    @Autowired
    private ProductTypeService productTypeService;
    @Autowired
    private ProductAttornService productAttornService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private SysFileService sysFileService;
    @Autowired
    private FrontService frontService;
    @Autowired
    private HuanXunFreezeService huanXunFreezeService;

    /**
     * 首页
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public String getFontList(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {

        try {

            /******************** 广告内容列表部分Start ********************/
            List<AdvertContentMessageBean> listAdvert = new ArrayList<AdvertContentMessageBean>();
            listAdvert = advertService.getListBySome("SORT", "ASC", "1", "3", "1"); //官方公告
            List<AdvertContentMessageBean> listAdvertNew = advertService.getListBySome("CREATETIME", "DESC", "1", "1", "1"); //公告部分
            modelMap.put("listAdvert", listAdvert);
            modelMap.put("listAdvertNew", listAdvertNew);
            /********************* 广告内容列表部分End *********************/

            /******************** 首页轮播图Start ********************/
            List<AdvertContentMessageBean> listAdvert2 = new ArrayList<AdvertContentMessageBean>();
            listAdvert2 = advertService.getListBySome("SORT", "ASC", "1", "3", "2");
            modelMap.put("listAdvert2", listAdvert2);
            /********************* 首页轮播图End *********************/

            /******************** 产品类别集合Start ********************/
            ProductTypeMessage productTypeMessage = new ProductTypeMessage(); //产品类别
            List<ProductTypeMessage> listProductTypeMes = new ArrayList<ProductTypeMessage>(); //产品类型集合
            listProductTypeMes = productTypeService.queryAllProductType(productTypeMessage); //产品类别集合
            modelMap.addAttribute("listProductTypeMes", listProductTypeMes);
            /********************* 产品类别集合End *********************/

            /******************** 获取推荐产品Start ********************/
            ProductRecommendMessageBean productRecommendMes = new ProductRecommendMessageBean();
            productRecommendMes = productService.getRecommend();
            modelMap.put("productRecommendMes", productRecommendMes);
            /********************* 获取推荐产品End *********************/

            /******************** 获取产品Start ********************/
            String fProductPj = ""; //房贷宝拼接
            String fProductProgress = ""; //房贷宝进度
            List<ProductMessageBean> fProductList = productService.queryProductByType("27"); //房贷宝
            List<ProductMessageBean> cProductList = productService.queryProductByType("26"); //车贷宝
            List<ProductMessageBean> rProductList = productService.queryProductByType("28"); //融贷宝
            List<ProductMessageBean> pProductList = productService.queryProductByType("29"); //票贷宝

            modelMap.put("fProduct", fProductList); //房贷宝
            modelMap.put("cProduct", cProductList); //车贷宝
            modelMap.put("rProduct", rProductList); //融贷宝
            modelMap.put("pProduct", pProductList); //票贷宝
            /******************** 获取产品End ********************/

            /******************** 获取财富资讯一级类别Start ********************/
            List<NewsTypeMessageBean> newsTypeList = newsService.queryOneType();
            modelMap.put("newsTypeList", newsTypeList);
            /******************** 获取财富资讯一级类别end ********************/

            /******************** 获取理财风云榜 start ********************/
            FinancingMessageBean financingMes = new FinancingMessageBean();
            financingMes.setPageIndex("1");
            financingMes.setPageSize("4");
            financingMes.setOrderColumn("INVESTPRICE"); //排序字段
            financingMes.setOrderType("DESC");
            List<FinancingMessageBean> listFinancingMes = allMemberService.queryAllManageMoney(financingMes);
            modelMap.put("listFinancingMes", listFinancingMes);
            /******************** 获取理财风云榜 end ********************/

            /******************** 债券转让 start ********************/
            ProductAttornRecordMessageBean productAttornRecordMes = new ProductAttornRecordMessageBean();
            productAttornRecordMes.setPageIndex("1");
            productAttornRecordMes.setPageSize("4");
            List<ProductAttornRecordMessageBean> listProductAttornRecordMes = productAttornService.getFrontAttornList("CREATETIME", "DESC", productAttornRecordMes);
            modelMap.addAttribute("listProductAttornRecordMes", listProductAttornRecordMes);
            /******************** 债券转让 end ********************/

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "front/home";
    }


    /**
     * 跳转到(我的账户)
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/goAccount", method = RequestMethod.GET)
    public String goAccount(HttpServletRequest request, ModelMap modelMap) {

        String memberId = ""; //会员id
        String nickName = ""; //昵称
        String investAmount = "0"; //投资收益
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");

        if (memberInfo != null) {
            memberId = memberInfo.getId(); //会员id
            if (memberId != null && !memberId.equals("")) {  //会员id
                /******************** 附件部分Start ********************/
                SysFileMessageBean sysFileMessageBean = new SysFileMessageBean();
                sysFileMessageBean.setGlid(memberId); //关联id
                sysFileMessageBean.setPageIndex("1"); //当前数
                sysFileMessageBean.setPageSize("1"); //每次显示数量
                sysFileMessageBean.setOrderColumn("CREATETIME"); //创建时间
                sysFileMessageBean.setOrderType("DESC"); //排序类型
                SysFileMessageBean sysFile = sysFileService.getSysFileById(sysFileMessageBean);
                modelMap.put("sysFile", sysFile); //用户头像部分
                /********************* 附件部分End *********************/

                /******************** 我的账户部分Start ********************/
                MyAccountMessageBean myAccountMes = new MyAccountMessageBean();
                myAccountMes.setId(memberId); //会员id
                MyAccountMessageBean myAccountMessageBean = allMemberService.getMemberInfo(myAccountMes);
                investAmount = TradeHandle.getInvestAmount(memberId); //投资收益总和
                modelMap.put("investAmount",investAmount); //投资收益总和

                /********** 日历部分Start **********/
                Map mapRl = this.getCalendar(request, memberId); //日历拼接
                String rlStr = (String) mapRl.get("rlStr"); //日历拼接
                String year = (String) mapRl.get("year"); //年份
                String month = (String) mapRl.get("month"); //月份
                Double receivePrice = (Double) mapRl.get("receivePrice"); //应回款金额
                Double repayPrice = (Double) mapRl.get("repayPrice"); //应还款金额
                int receiveCount = (int) mapRl.get("receiveCount"); //应回款数量
                int repayCount = (int) mapRl.get("repayCount"); //应还款数量
                String yReceivePrice = (String) mapRl.get("yReceivePrice"); //已回款金额
                String yReceiveCount = (String) mapRl.get("yReceiveCount"); //已回款数量
                String yRepayPrice = (String) mapRl.get("yRepayPrice"); //已还款金额
                String yRepayCount = (String) mapRl.get("yRepayCount"); //已还款数量
                /*********** 日历部分End ***********/

                nickName = myAccountMessageBean.getNickname(); //昵称
                memberInfo.setNickname(nickName);

                myAccountMessageBean.setReceivePrice(TypeTool.getString(receivePrice)); //应回款金额
                myAccountMessageBean.setRepayPrice(TypeTool.getString(repayPrice)); //应还款金额
                myAccountMessageBean.setReceiveCount(TypeTool.getString(receiveCount)); //应回款数量
                myAccountMessageBean.setRepayCount(TypeTool.getString(repayCount)); //应还款数量
                modelMap.put("myAccountMessageBean", myAccountMessageBean); //我的账户部分
                modelMap.put("yReceivePrice",yReceivePrice); //已回款金额
                modelMap.put("yReceiveCount",yReceiveCount); //已回款数量
                modelMap.put("yRepayPrice",yRepayPrice); //已还款金额
                modelMap.put("yRepayCount",yRepayCount); //已还款数量
                /********************* 我的账户部分End *********************/

                /******************** 产品类型部分Start ********************/
                ProductTypeMessage productTypeMessage = new ProductTypeMessage(); //产品类别
                List<ProductTypeMessage> listProductTypeMes = new ArrayList<ProductTypeMessage>(); //产品类型集合
                listProductTypeMes = productTypeService.queryAllProductType(productTypeMessage); //产品类别集合
                modelMap.addAttribute("listProductTypeMes", listProductTypeMes);
                /********************* 产品类型部分End *********************/

                /******************** 新闻类型部分Start ********************/
                List<NewsTypeMessageBean> newsTypeList = newsService.queryOneType();
                modelMap.put("newsTypeList", newsTypeList);
                /********************* 新闻类型部分End *********************/

                /******************** 安全等级 start ********************/
                int amount = 0;
                if (myAccountMessageBean.getRealFlg() != null && !myAccountMessageBean.getRealFlg().equals("")) {
                    if (myAccountMessageBean.getRealFlg().equals("0")) { //实名认证
                        amount++;
                    }
                }
                if (myAccountMessageBean.getTraderPassword() != null && !myAccountMessageBean.getTraderPassword().equals("")) { //交易密码
                    amount++;
                }
                if (myAccountMessageBean.getTelephoneFlg() != null && !myAccountMessageBean.getTelephoneFlg().equals("")) {
                    if (myAccountMessageBean.getTelephoneFlg().equals("0")) { //手机认证
                        amount++;
                    }
                }
                if (myAccountMessageBean.getMailFlg() != null && !myAccountMessageBean.getMailFlg().equals("")) {
                    if (myAccountMessageBean.getMailFlg().equals("0")) { //邮箱认证
                        amount++;
                    }
                }
                if (securitySettingsService.queryOnlyContact(memberId).getId() != null) { //安全设置
                    amount++;
                }
                if (securitySettingsService.queryIsQuestion(memberId)) { //安全问题
                    amount++;
                }
                modelMap.addAttribute("amount", amount);
                /******************** 安全等级 end ********************/

                /******************** 冻结金额 ********************/
                String ipstrdamt = huanXunFreezeService.queryFreeze(memberId);
                modelMap.put("ipstrdamt", ipstrdamt); //冻结金额
                /******************** 冻结金额 ********************/

                servletUtil.getSession().setAttribute("sessionMemberInfo",memberInfo);
                modelMap.put("rlStr", rlStr); //日历部分拼接
                modelMap.put("year", year); //年份
                modelMap.put("month", month); //月份
            }
        }
        return "front/account";
    }


    /**
     * 获取日历消息
     *
     * @param request
     */
    @RequestMapping(value = "/getRlInformation", method = RequestMethod.POST)
    @ResponseBody
    public MyAccountMessageBean getRlInformation(HttpServletRequest request) {
        /******************** 会员信息部分Start ********************/
        String memberId = ""; //会员id
        MemberInfo memberInfo = (MemberInfo) servletUtil.getSession().getAttribute("sessionMemberInfo");
        if (memberInfo != null) {
            memberId = memberInfo.getId(); //会员id
        }
        /********************* 会员信息部分End *********************/

        /******************** 我的账户部分Start ********************/
        Map mapRl = this.getCalendar(request, memberId); //日历拼接
        String rlStr = (String) mapRl.get("rlStr"); //日历拼接
        String year = (String) mapRl.get("year"); //年份
        String month = (String) mapRl.get("month"); //月份
        Double receivePrice = (Double) mapRl.get("receivePrice"); //应回款金额
        Double repayPrice = (Double) mapRl.get("repayPrice"); //应还款金额
        int receiveCount = (int) mapRl.get("receiveCount"); //回款数量
        int repayCount = (int) mapRl.get("repayCount"); //还款数量
        String yReceivePrice = (String) mapRl.get("yReceivePrice"); //已回款金额
        String yRepayPrice = (String) mapRl.get("yRepayPrice"); //已还款金额
        String yReceiveCount = (String) mapRl.get("yReceiveCount"); //已回款数量
        String yRepayCount = (String) mapRl.get("yRepayCount"); //已回款数量
        MyAccountMessageBean myAccountMessageBean = new MyAccountMessageBean();
        myAccountMessageBean.setRlStr(rlStr);
        myAccountMessageBean.setYear(year);
        myAccountMessageBean.setMonth(month);
        myAccountMessageBean.setReceivePrice(TypeTool.getString(receivePrice)); //应回款金额
        myAccountMessageBean.setRepayPrice(TypeTool.getString(repayPrice)); //应还款金额
        myAccountMessageBean.setReceiveCount(TypeTool.getString(receiveCount)); //应回款数量
        myAccountMessageBean.setRepayCount(TypeTool.getString(repayCount)); //应还款数量
        myAccountMessageBean.setYsPrice(yReceivePrice); //已回款金额
        myAccountMessageBean.setYsCount(yReceiveCount); //已回款数量
        myAccountMessageBean.setYhPrice(yRepayPrice); //已还款金额
        myAccountMessageBean.setYhCount(yRepayCount); //已还款数量
        /********************* 我的账户部分End *********************/

        return myAccountMessageBean;
    }


    /**
     * 前台查询公告列表(重写 by zhangerxin)
     * @param advertContentMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryFrontAdvertList", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryFrontAdvertList(@ModelAttribute AdvertContentMessageBean advertContentMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = advertContentMes.getPageIndex(); //当前数
        String pageSize = advertContentMes.getPageSize(); //每页显示数量
        String orderType = advertContentMes.getOrderType(); //排序类型
        String orderColumn = advertContentMes.getOrderColumn(); //排序字段
        String pageStr = ""; //分页部分拼接

        if (pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            advertContentMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            advertContentMes.setPageSize(pageSize);
        }
        if (orderType == null || orderType.equals("")){ //排序类型
            orderType = "ASC";
            advertContentMes.setOrderType(orderType);
        }
        if (orderColumn == null || orderColumn.equals("")){ //排序字段
            orderColumn = "SORT";
            advertContentMes.setOrderColumn(orderColumn);
        }

        //广告部分集合
        List<AdvertContentMessageBean> listAdvertContentMes = advertService.queryFrontAdvertList(advertContentMes);
        //广告数量
        int totalCount = advertService.queryFrontAdvertCount(advertContentMes);
        //分页部分
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo2(pageIndex,pageSize,totalCount,"goFrontAdvert","pageFrontAdvert","");

        modelMap.put("listAdvertContentMes",listAdvertContentMes); //广告集合
        modelMap.put("totalCount",totalCount); //广告数量
        modelMap.put("pageStr",pageStr); //分页部分

        //模板解析方法
        try {
            StringWriter stringWriter = null;
            stringWriter = jetbrickTool.getJetbrickTemp(advertContentMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 根据条件查询前台广告部分
     * @param advertContentMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryFrontAdvertBySome", method = RequestMethod.POST)
    @ResponseBody
    public AdvertContentMessageBean queryFrontAdvertBySome(@ModelAttribute AdvertContentMessageBean advertContentMes, ModelMap modelMap) {
        String pageIndex = advertContentMes.getPageIndex(); //当前数
        String pageSize = advertContentMes.getPageSize(); //每页显示数量
        String orderType = advertContentMes.getOrderType(); //排序类型
        String orderColumn = advertContentMes.getOrderColumn(); //排序字段
        String advertStr = ""; //广告部分拼接
        String pageStr = ""; //分页部分拼接

        if (pageIndex == null || pageIndex.equals("")){ //当前数
            pageIndex = "1";
            advertContentMes.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")){ //每页显示数量
            pageSize = "10";
            advertContentMes.setPageSize(pageSize);
        }
        if (orderType == null || orderType.equals("")){ //排序类型
            orderType = "ASC";
            advertContentMes.setOrderType(orderType);
        }
        if (orderColumn == null || orderColumn.equals("")){ //排序字段
            orderColumn = "SORT";
            advertContentMes.setOrderColumn(orderColumn);
        }

        //广告部分集合
        List<AdvertContentMessageBean> listAdvertContentMes = advertService.queryFrontAdvertList(advertContentMes);
        if (listAdvertContentMes != null && listAdvertContentMes.size() > 0){
            for (AdvertContentMessageBean advertContentMessageBean : listAdvertContentMes){
                advertStr = advertStr + "<li>"
                          + "<a href=\"JavaScript:void(0)\" onclick=\"queryFrontAdvertDetails('"+advertContentMessageBean.getId()+"')\">"
                          + advertContentMessageBean.getTitle()
                          + "</a>"
                          + "</li>";
            }
        }

        //广告数量
        int totalCount = advertService.queryFrontAdvertCount(advertContentMes);
        //分页部分
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo2(pageIndex,pageSize,totalCount,"goFrontAdvert","pageFrontAdvert","");

        advertContentMes.setAdvertStr(advertStr); //广告部分拼接
        advertContentMes.setPageStr(pageStr); //分页部分拼接

        return advertContentMes;
    }


    /**
     * 前台查询公告详情(重写 by zhangerxin)
     * @param advertContentMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/queryFrontAdvertDetails", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryFrontAdvertDetails(@ModelAttribute AdvertContentMessageBean advertContentMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();

        //广告部分
        AdvertContentMessageBean advertContentMessageBean = advertService.queryFrontAdvertDetails(advertContentMes);
        modelMap.put("advertContentMessageBean",advertContentMessageBean); //广告部分

        //模板解析方法
        try {
            StringWriter stringWriter = null;
            stringWriter = jetbrickTool.getJetbrickTemp(advertContentMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 查看更多项目列表
     */
    @RequestMapping(value = "/openProductMove", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openProductMove(@ModelAttribute ProductMessageBean productMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        //类别id
        String typeId = productMessageBean.getProductType();
        String pageIndex = productMessageBean.getPageIndex(); //当前数
        String pageSize = productMessageBean.getPageSize(); //每页显示数量
        String pageStr = ""; //分页部分拼接

        if (pageIndex == null || pageIndex.equals("")) {
            pageIndex = "1";
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "10";
        }
        productMessageBean.setProductTypeIdSelectPj(typeId); //产品类型id
        //查询产品列表
        List<ProductMessageBean> AllProductList = productService.getListBySome("CREATETIME", "DESC", pageIndex, pageSize, typeId);
        //查询产品数量
        int totalCount = productService.queryProductCount(productMessageBean);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo2(pageIndex, pageSize, totalCount, "pageProduct", "goProductPage", typeId); //投资部分分页
        modelMap.put("AllProductList", AllProductList);
        modelMap.put("pageStr", pageStr); //分页部分

        //模板解析方法
        try {

            StringWriter stringWriter = null;
            //模板渲染
            stringWriter = jetbrickTool.getJetbrickTemp("/front/product/product_more.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 首页查询更多债权列表
     *
     * @param producAttornMoretMessageBean
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openAttornMore", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openAttornMore(@ModelAttribute ProductAttornMoreMessageBean producAttornMoretMessageBean, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = producAttornMoretMessageBean.getPageIndex(); //当前数
        String pageSize = producAttornMoretMessageBean.getPageSize(); //每页显示数量
        String attornMoreStr = ""; //债权列表拼接
        String pageStr = ""; //分页部分拼接

        if (pageIndex == null || pageIndex.equals("")) {
            pageIndex = "1";
            producAttornMoretMessageBean.setPageIndex(pageIndex);
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "10";
            producAttornMoretMessageBean.setPageSize(pageSize);
        }

        //债权转让集合
        List<ProductAttornMoreMessageBean> listProductAttornMoreMes = productAttornService.getAttornMoreList(producAttornMoretMessageBean);
        //债权转让数量
        int totalCount = productAttornService.getAttornMoreCount(producAttornMoretMessageBean);
        FrontPage frontPage = new FrontPage();
        pageStr = frontPage.getPageInfo2(pageIndex, pageSize, totalCount, "pageAttornMore", "goAttornMorePage", null); //分页部分拼接
        modelMap.put("listProductAttornMoreMes", listProductAttornMoreMes); //债权转让列表
        modelMap.put("pageStr", pageStr); //分页部分拼接

        //模板解析方法
        try {

            StringWriter stringWriter = null;
            //模板渲染
            stringWriter = jetbrickTool.getJetbrickTemp("/front/product/product_Attorn_more.jetx", modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        producAttornMoretMessageBean.setAttornListStr(attornMoreStr); //列表部分拼接
        producAttornMoretMessageBean.setPageStr(pageStr);

        return bean;
    }


    /**
     * 获取日历
     * @param request
     * @param memberId
     * @return
     */
    public Map getCalendar(HttpServletRequest request, String memberId) {
        String year = request.getParameter("year"); //年份(字符型)
        int yearin = 0; //年份(整型)
        String month = request.getParameter("month"); //月份(字符型)
        int monthin = 0; //月份(整型)
        int ts = 0; //获取当前月有多少天
        String firstDayll = ""; //第一天(星期几)，字符型
        int firstDay = 0; //第一天(星期几)，整型
        int lastDaylm = 0; //上个月的最后一天(日期，具体几号),整型
        int Day = 0; //获取当天是几号
        String receiveCount = "0"; //应还款数量
        int receiveCountSum = 0; //应累计还款数量
        String repayCount = "0"; //应回款数量
        int repayCountSum = 0; //累计应回款数量
        String rq = ""; //本月第一天日期
        String receivePrice = ""; //应回款金额(String型)
        Double receivePriceDou = 0d; //应回款金额(Double型)
        String repayPrice = ""; //应还款金额(String型)
        Double repayPriceDou = 0d; //应还款金额(Double型)
        String yReceivePrice = "0"; //已回款金额
        String yReceiveCount = "0"; //已回款数量
        String yRepayPrice = "0"; //已还款金额
        String yRepayCount = "0"; //已还款数量
        int b = 0;
        int c = 1; //当前月第一天(具体几号)
        int d = 1; //下个月第一天
        String rlStr = ""; //日历拼接
        Map map = new HashMap(); //存放返回信息
        List<MyAccountMessageBean> listReceive = new ArrayList<MyAccountMessageBean>(); //应回款部分集合
        List<MyAccountMessageBean> listRepay = new ArrayList<MyAccountMessageBean>(); //应还款部分集合
        MyAccountMessageBean myAccountMesYReceive = new MyAccountMessageBean(); //已回款部分
        MyAccountMessageBean myAccountMesYRepay = new MyAccountMessageBean(); //已还款部分

        try {

            Calendar a = Calendar.getInstance();

            /********** 获取年份Start **********/
            if (year == null || year.equals("")) {
                yearin = a.get(Calendar.YEAR);
            } else {
                yearin = Integer.parseInt(year);
            }
            year = Integer.toString(yearin);
            /*********** 获取年份End ***********/

            /********** 获取月份Start **********/
            if (month == null || month.equals("")) {
                monthin = a.get(Calendar.MONTH) + 1;
            } else {
                monthin = Integer.parseInt(month);
            }
            month = Integer.toString(monthin);
            /*********** 获取月份End ***********/

            rq = year + "-" + month + "-" + "01";
            ts = queryTs(yearin, monthin); //天数
            String[] arr = new String[ts]; //回款部分数组
            String[] brr = new String[ts]; //还款部分数组

            /*************** 根据年份和月份获取第一天Start ***************/
            DMap dMapFirstDay = new DMap();
            dMapFirstDay = frontService.queryFirstDay(rq);
            if (dMapFirstDay != null) {
                firstDayll = dMapFirstDay.getValue("FIRSTDAY", 0); //第一天(星期几)，字符型
                firstDay = TypeTool.getInt(firstDayll) - 1; //第一天(星期几)，整型
            }
            /**************** 根据年份和月份获取第一天End ****************/

            lastDaylm = frontService.queryLastDayLm(rq); //上个月最后一天(整型)
            b = firstDay - 1;

            /*************** 应回款数量集合Start ***************/
            listReceive = frontService.getReceiveFlag(year, month, "", memberId);
            if (listReceive != null && listReceive.size() > 0) {
                for (MyAccountMessageBean myAccountMes : listReceive) {
                    Day = TypeTool.getInt(myAccountMes.getDay()); //获取当天是几号
                    receiveCount = myAccountMes.getReceiveCount(); //回款数量
                    receivePrice = myAccountMes.getReceivePrice(); //回款金额
                    if (receivePrice != null && !receivePrice.equals("")) {
                        receivePriceDou = receivePriceDou + TypeTool.getDouble(receivePrice);
                    }
                    if (!receiveCount.equals("0")) { //回款数量
                        arr[Day - 1] = "<span class=\"p_left_radius3\"></span>"; //回款部分标志样式
                        receiveCountSum = receiveCountSum + TypeTool.getInt(receiveCount); //累计回款数量
                    } else { //没有回款
                        arr[Day - 1] = "";
                    }
                }
                receivePriceDou = PublicsTool.round(receivePriceDou,2); //累计回款金额
            }
            /**************** 应回款数量集合End ****************/

            /*************** 应还款数量集合Start ***************/
            listRepay = frontService.getRepayFlag(year, month, "", memberId);
            if (listRepay != null && listRepay.size() > 0) {
                for (MyAccountMessageBean myAccountMes : listRepay) {
                    Day = TypeTool.getInt(myAccountMes.getDay()); //获取当天是几号
                    repayCount = myAccountMes.getRepayCount(); //还款数量
                    repayPrice = myAccountMes.getRepayPrice(); //还款金额
                    if (repayPrice != null && !repayPrice.equals("")) {
                        repayPriceDou = repayPriceDou + TypeTool.getDouble(repayPrice);
                    }
                    if (!repayCount.equals("0")) { //还款数量
                        brr[Day - 1] = "<span class=\"p_left_radius4\"></span>";
                        repayCountSum = repayCountSum + TypeTool.getInt(repayCount); //累计还款数量
                    } else { //没有还款
                        brr[Day - 1] = "";
                    }
                }
                repayPriceDou = PublicsTool.round(repayPriceDou,2); //还款金额
            }
            /**************** 应还款数量集合End ****************/

            /*************** 已回款信息Start ***************/
            myAccountMesYReceive = frontService.getReceiveInfo(year,month,memberId,"0");
            if (myAccountMesYReceive != null){
                yReceivePrice = myAccountMesYReceive.getYsPrice(); //已回款金额
                yReceiveCount = myAccountMesYReceive.getYsCount(); //已回款数量
            }
            /**************** 已回款信息End ****************/

            myAccountMesYRepay = frontService.getRepayInfo(year,month,memberId,"0");
            if (myAccountMesYRepay != null){
                yRepayPrice = myAccountMesYRepay.getYhPrice(); //已还款金额
                yRepayCount = myAccountMesYRepay.getYhCount(); //已还款数量
            }

            /*********** 行循环Start **********/
            for (int i = 0; i < 6; i++) {
                rlStr = rlStr + "<tr>";
                /***** 列循环Start *****/
                for (int j = 0; j < 7; j++) {
                    if (b >= 0) { //其他月
                        rlStr = rlStr + "<td style=\"color:#aeaeae;\">" + (lastDaylm - b) + "</td>";
                        b--;
                    } else { //本月
                        if (c <= ts) {
                            if (arr[c - 1] != null && !arr[c - 1].equals("") && brr[c - 1] != null && !brr[c - 1].equals("")) {
                                rlStr = rlStr + "<td class=\"td_color\">"
                                        + c
                                        + "<span class=\"p_left_radius3\" style=\"position:absolute;left:7px;\"></span>"
                                        + "<span class=\"p_left_radius4\" style=\"position:absolute;left:33px;\"></span>"
                                        + "</td>";
                            } else {
                                if (arr[c - 1] != null && !arr[c - 1].equals("")) {
                                    rlStr = rlStr + "<td class=\"td_color\">"
                                            + c
                                            + "<span class=\"p_left_radius3\"></span>"
                                            + "</td>";
                                } else if (brr[c - 1] != null && !brr[c - 1].equals("")) {
                                    rlStr = rlStr + "<td class=\"td_color\">"
                                            + c
                                            + "<span class=\"p_left_radius4\"></span>"
                                            + "</td>";
                                } else {
                                    rlStr = rlStr + "<td>" + c + "</td>";
                                }
                            }
                            c++;
                        } else {
                            rlStr = rlStr + "<td style=\"color:#aeaeae;\">" + d + "</td>";
                            d++;
                        }
                    }
                }
                /****** 列循环End ******/
                rlStr = rlStr + "</tr>";
            }
            /************ 行循环End ***********/

            map.put("rlStr",rlStr); //日历拼接
            map.put("year",year); //年份
            map.put("month",month); //月份
            map.put("receivePrice",receivePriceDou); //应回款金额
            map.put("repayPrice",repayPriceDou); //应还款金额
            map.put("receiveCount", receiveCountSum); //应回款数量
            map.put("repayCount",repayCountSum); //应还款数量
            map.put("yReceivePrice",yReceivePrice); //已回款金额
            map.put("yReceiveCount",yReceiveCount); //已回款数量
            map.put("yRepayPrice",yRepayPrice); //已还款金额
            map.put("yRepayCount",yRepayCount); //已还款数量
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }


    /**
     * 获取某个月有多少天
     *
     * @param year
     * @param month
     * @return
     */
    public int queryTs(int year, int month) {
        int ts = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                ts = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                ts = 30;
                break;
            case 2:
                ts = isLeapYear(year) ? 29 : 28;
                break;
        }
        return ts;
    }

    /**
     * 判断是否为闰年
     *
     * @return
     */
    public Boolean isLeapYear(int year) {
        if (year % 100 == 0) {
            if (year % 400 == 0) {
                return true;
            }
        } else {
            if (year % 4 == 0) {
                return true;
            }
        }
        return false;
    }


    /**
     * 查询更多理财排名
     * @param financingMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/openMoreRanking", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean openMoreRanking(@ModelAttribute FinancingMessageBean financingMes, ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String orderColumn = financingMes.getOrderColumn(); //排序字段
        String orderType = financingMes.getOrderType(); //排序类型
        String pageIndex = financingMes.getPageIndex(); //当前数量
        String pageSize = financingMes.getPageSize(); //每页显示数量
        String financingStr = ""; //理财部分分页

        if (pageIndex == null || pageIndex.equals("")) {
            pageIndex = "1"; //当前数量
            financingMes.setPageIndex(pageIndex); //当前页
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "10"; //每页显示数量
            financingMes.setPageSize(pageSize); //每页显示数量
        }
        if (orderColumn == null || orderColumn.equals("")){
            orderColumn = "INVESTPRICE";
            financingMes.setOrderColumn(orderColumn);
        }
        if (orderType == null || orderType.equals("")){
            orderType = "DESC";
            financingMes.setOrderType(orderType);
        }

        /******************** 获取理财风云榜 start ********************/
        List<FinancingMessageBean> listFinancingMes = allMemberService.queryAllManageMoney(financingMes);
        int totalCount = allMemberService.queryAllManageMoneyCount(financingMes); //理财风云榜数量
        FrontPage frontPage = new FrontPage();
        financingStr = frontPage.getPageInfo(pageIndex,pageSize,totalCount,"pageFinancing","goFinancinPage"); //理财部分分页
        modelMap.put("listFinancingMes",listFinancingMes); //理财风云榜集合
        modelMap.put("financingStr",financingStr); //理财风云榜
        /******************** 获取理财风云榜 end ********************/

        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(financingMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }

    /**
     * 环迅页面返回地址
     *
     * @param request
     * @param modelMap
     */
    @RequestMapping(value = "/registerhxWebResult", method = RequestMethod.POST)
    @ResponseBody
    public void registerhxWebResult(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws IOException {
        String memberId = request.getParameter("memberId"); //会员id
        HttpSession session = servletUtil.getSession(); //拿到会话
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(memberId);
        session.setAttribute("sessionMemberInfo", memberInfo);
        response.sendRedirect("goAccount");
    }


    /**
     * 环迅自动签约返回页面地址
     * @param request
     * @param response
     * @param modelMap
     * @throws IOException
     */
    @RequestMapping(value = "/motionSignhxWebResult", method = RequestMethod.POST)
    @ResponseBody
    public void motionSignhxWebResult(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws IOException {
        String memberId = request.getParameter("memberId"); //会员id
        HttpSession session = servletUtil.getSession();
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(memberId); //会员id
        session.setAttribute("sessionMemberInfo", memberInfo);
        response.sendRedirect("goAccount");
    }

    /**
     * 注册环迅 返回地址 by cuibin
     *
     * @param request
     * @param modelMap
     */
    @RequestMapping(value = "/regResult", method = RequestMethod.POST)
    public void regResult(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws IOException {
        String memberId = request.getParameter("memberId"); //会员id
        response.sendRedirect("regResultGet?memberId=" + memberId);
    }


    /**
     * 注册环迅返回页面地址
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/regResultGet", method = RequestMethod.GET)
    public String regResultGet(HttpServletRequest request, ModelMap modelMap) {
        String memberId = request.getParameter("memberId"); //会员id
        modelMap.put("memberId", memberId);
        return "front/huanxun/motion_sign";
    }


    /**
     * 测试
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
        return "front/test";
    }
}
