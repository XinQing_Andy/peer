package com.mh.peer.controller.product;

import com.mh.peer.jetbrick.JetbrickBean;
import com.mh.peer.model.message.ProductOverRepayMessageBean;
import com.mh.peer.model.message.ProductRepayMessageBean;
import com.mh.peer.service.product.ProductRepayService;
import com.mh.peer.util.JetbrickTool;
import com.mh.peer.util.ServletUtil;
import com.salon.frame.util.TypeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by zhangerxin on 2016-8-15.
 * 产品还款部分
 */
@Controller
@RequestMapping("/productRepay")
public class ProductRepayController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductRepayController.class);
    @Autowired
    private ProductRepayService productRepayService;
    @Autowired
    private ServletUtil servletUtil;
    @Autowired
    private JetbrickTool jetbrickTool;

    /**
     * 逾期账单查询
     * @param overRepayMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/getOverdue", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryProductInfo(@ModelAttribute ProductOverRepayMessageBean overRepayMes , ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = overRepayMes.getPageIndex(); //当前数量
        String pageSize = overRepayMes.getPageSize(); //每页显示数量
        String currentPage = overRepayMes.getCurrentPage(); //当前页
        int pageCount = 0; //总页数

        if (pageIndex == null || pageIndex.equals("")) { //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "10";
        }

        overRepayMes.setPageIndex(pageIndex); //当前页
        overRepayMes.setPageSize(pageSize); //每页显示数量
        List<ProductOverRepayMessageBean> overRepayList = productRepayService.getOverDueRepayList(overRepayMes); //逾期账单列表(根据分页查询)
        int totalCount = productRepayService.getOverRepayByPage(overRepayMes); //产品数量
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量

        modelMap.put("overRepayList", overRepayList); //产品集合
        modelMap.put("totalCount", totalCount); //产品数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageCount",pageCount); //总数量

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(overRepayMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }

        return bean;
    }


    /**
     * 还款中产品列表
     * @param repayingMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/getRepaying", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryRapayingInfo(@ModelAttribute ProductRepayMessageBean repayingMes , ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = repayingMes.getPageIndex(); //当前数量
        String pageSize = repayingMes.getPageSize(); //每页显示数量
        String currentPage = repayingMes.getCurrentPage(); //当前页
        int pageCount = 0; //总页数

        if (pageIndex == null || pageIndex.equals("")) { //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "10";
        }

        repayingMes.setPageIndex(pageIndex); //当前页
        repayingMes.setPageSize(pageSize); //每页显示数量
        List<ProductRepayMessageBean> repayingList = productRepayService.getRepayingList(repayingMes); //逾期账单列表(根据分页查询)
        int totalCount = productRepayService.getRepayingByPage(repayingMes); //产品数量
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量

        modelMap.put("repayingList", repayingList); //产品集合
        modelMap.put("totalCount", totalCount); //产品数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageCount",pageCount); //总数量

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(repayingMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 已结清产品列表
     * @param finishMes
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/getFinish", method = RequestMethod.POST)
    @ResponseBody
    public JetbrickBean queryFinishInfo(@ModelAttribute ProductRepayMessageBean finishMes , ModelMap modelMap) {
        JetbrickBean bean = new JetbrickBean();
        String pageIndex = finishMes.getPageIndex(); //当前数量
        String pageSize = finishMes.getPageSize(); //每页显示数量
        String currentPage = finishMes.getCurrentPage(); //当前页
        int pageCount = 0; //总页数

        if (pageIndex == null || pageIndex.equals("")) { //当前数
            pageIndex = "1";
            currentPage = "1";
        }
        if (pageSize == null || pageSize.equals("")) { //每页显示数量
            pageSize = "10";
        }

        finishMes.setPageIndex(pageIndex); //当前页
        finishMes.setPageSize(pageSize); //每页显示数量
        List<ProductRepayMessageBean> finishList = productRepayService.getFinishList(finishMes); //已结清产品列表(根据分页查询)
        int totalCount = productRepayService.getFinishByPage(finishMes); //已结清产品数量
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量

        modelMap.put("finishList", finishList); //产品集合
        modelMap.put("totalCount", totalCount); //产品数量
        modelMap.put("currentPage", currentPage); //当前页
        modelMap.put("pageCount",pageCount); //总数量

        //模板解析方法
        try {
            //模板渲染
            StringWriter stringWriter = jetbrickTool.getJetbrickTemp(finishMes.getMenuUrl(), modelMap);
            bean.setHtmlText(stringWriter.toString());
            bean.setInfo("成功");
            bean.setResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            bean.setInfo("失败");
            bean.setResult("error");
            bean.setHtmlText("");
        }
        return bean;
    }


    /**
     * 跳转到充值统计
     */
    @RequestMapping(value = "/queryProductInfoByPage", method = RequestMethod.POST)
    @ResponseBody
    public ProductOverRepayMessageBean queryProductInfoyByPage(@ModelAttribute ProductOverRepayMessageBean overRepayMes) {
        ProductOverRepayMessageBean result = new ProductOverRepayMessageBean();

        String pageIndex = overRepayMes.getPageIndex(); //当前数量
        String pageSize = overRepayMes.getPageSize(); //每页显示数量
        String currentPage = overRepayMes.getCurrentPage(); //当前页
        int pageCount = 0; //总页数

        if (pageIndex == null || pageIndex.equals("")) {
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "10"; //每页显示数量
        }

        overRepayMes.setPageIndex(pageIndex); //当前页
        overRepayMes.setPageSize(pageSize); //每页显示数量

        //逾期账单列表(根据分页查询)
        List<ProductOverRepayMessageBean> overRepayList = productRepayService.getOverDueRepayList(overRepayMes);
        //产品数量
        int totalCount = productRepayService.getOverRepayByPage(overRepayMes);
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量
        String str = "";
        int i = 1;
        str = "<table class=\"table1\" style=\"border-collapse: collapse;\">\n" +
                "<tr class=\"table_first\">\n" +
                "<td style=\"width: 3%;\">序号</td>\n" +
                "<td>借款人 </td>" +
                "<td>项目编号  </td>" +
                "<td>项目名称  </td>" +
                "<td>应还款日期  </td>" +
                "<td>逾期时长 </td>" +
                "<td>应还款金额 </td>" +
                "<td>借款金额 </td>" +
                "</tr>";
        for (ProductOverRepayMessageBean temp : overRepayList) {
            str = str + "<tr>" +
                    "<td>" + i++ + "</td>" +
                    "<td>" + temp.getLoanman() + "</td>" +
                    "<td>" + temp.getProductId() + "</td>" +
                    "<td>" + temp.getProductTitle() + "</td>" +
                    "<td>" + temp.getYrepaymentTime() + "</td>" +
                    "<td>" + temp.getOverDay() + "</td>" +
                    "<td>" + temp.getYremoney() + "</td>" +
                    "<td>" + temp.getLoanPrice() + "</td>" +
                    "</tr>";
        }
        str = str + "</table>";
        if (str != null && !str.equals("")) {
            result.setTotalCount(String.valueOf(totalCount));
            result.setCurrentPage(currentPage);
            result.setPageStr(String.valueOf(pageCount));
            result.setStr(str);
            result.setResult("success");
        } else {
            result.setResult("error");
        }

        return result;
    }

    /**
     * 跳转到充值统计
     */
    @RequestMapping(value = "/queryRepayingInfoByPage", method = RequestMethod.POST)
    @ResponseBody
    public ProductRepayMessageBean queryRepayingInfoByPage(@ModelAttribute ProductRepayMessageBean repayingMes) {
        ProductRepayMessageBean result = new ProductRepayMessageBean();

        String pageIndex = repayingMes.getPageIndex(); //当前数量
        String pageSize = repayingMes.getPageSize(); //每页显示数量
        String currentPage = repayingMes.getCurrentPage(); //当前页
        int pageCount = 0; //总页数

        if (pageIndex == null || pageIndex.equals("")) {
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "10"; //每页显示数量
        }

        repayingMes.setPageIndex(pageIndex); //当前页
        repayingMes.setPageSize(pageSize); //每页显示数量

        List<ProductRepayMessageBean> repayingList = productRepayService.getRepayingList(repayingMes); //逾期账单列表(根据分页查询)
        int totalCount = productRepayService.getRepayingByPage(repayingMes); //产品数量
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量
        String str = "";
        int i = 1;
        str = "<table class=\"table1\" style=\"border-collapse: collapse;\">\n" +
                "<tr class=\"table_first\">\n" +
                "<td style=\"width: 3%;\">序号</td>\n" +
                "<td>名称</td>" +
                "<td>编号</td>" +
                "<td>还款方式</td>" +
                "<td>借款金额</td>" +
                "<td>已还期数</td>" +
                "<td>借款人</td>" +
                "<td>年利率</td>" +
                "<td>下期还款日</td>" +
                "</tr>";
        for (ProductRepayMessageBean temp : repayingList) {
            str = str + "<tr>" +
                    "<td>" + i++ + "</td>" +
                    "<td>" + temp.getProductTitle() + "</td>" +
                    "<td>" + temp.getProductId() + "</td>" +
                    "<td>" + temp.getRepayment() + "</td>" +
                    "<td>" + temp.getLoadfee() + "</td>" +
                    "<td>" + temp.getRepayperiods() + "</td>" +
                    "<td>" + temp.getRealName() + "</td>" +
                    "<td>" + temp.getLoanrate() + "</td>" +
                    "<td>" + temp.getNextreceivetime() + "</td>" +
                    "</tr>";
        }
        str = str + "</table>";
        if (str != null && !str.equals("")) {
            result.setTotalCount(String.valueOf(totalCount));
            result.setCurrentPage(currentPage);
            result.setPageStr(String.valueOf(pageCount));
            result.setStr(str);
            result.setResult("success");
        } else {
            result.setResult("error");
        }

        return result;
    }


    /**
     * 查询已结清产品列表
     */
    @RequestMapping(value = "/queryFinishInfoByPage", method = RequestMethod.POST)
    @ResponseBody
    public ProductRepayMessageBean queryFinishInfoByPage(@ModelAttribute ProductRepayMessageBean finishMes) {
        ProductRepayMessageBean result = new ProductRepayMessageBean();
        String pageIndex = finishMes.getPageIndex(); //当前数量
        String pageSize = finishMes.getPageSize(); //每页显示数量
        String currentPage = finishMes.getCurrentPage(); //当前页
        int pageCount = 0; //总页数

        if (pageIndex == null || pageIndex.equals("")) {
            pageIndex = "1"; //当前数量
            currentPage = "1"; //当前页
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "10"; //每页显示数量
        }

        finishMes.setPageIndex(pageIndex); //当前页
        finishMes.setPageSize(pageSize); //每页显示数量

        List<ProductRepayMessageBean> repayingList = productRepayService.getFinishList(finishMes); //已结清账单列表(根据分页查询)
        int totalCount = productRepayService.getFinishByPage(finishMes); //已结清账单数量
        pageCount = this.getPageCount(pageSize,totalCount); //获取总数量
        String str = ""; //列表部分拼接
        int i = 1; //用于循环

        str = "<table class=\"table1\" style=\"border-collapse:collapse;\">\n"
            + "<tr class=\"table_first\">\n"
            + "<td style=\"width: 3%;\">序号</td>\n"
            + "<td>名称</td>"
            + "<td>编号</td>"
            + "<td>借款人</td>"
            + "<td>年利率</td>"
            + "<td>已还期数</td>"
            + "<td>借款金额</td>"
            + "</tr>";

        for (ProductRepayMessageBean temp : repayingList) {
            str = str + "<tr>"
                + "<td>"+i+"</td>"
                + "<td>"+temp.getProductTitle()+"</td>"
                + "<td>"+temp.getProductId()+"</td>"
                + "<td>"+temp.getRealName()+"</td>"
                + "<td>"+temp.getLoanrate()+"%</td>"
                + "<td>"+temp.getRepayperiods()+"期</td>"
                + "<td>"+temp.getLoadfee()+"元</td>"
                + "</tr>";
            i++;
        }
        str = str + "</table>";

        if (str != null && !str.equals("")) {
            result.setTotalCount(String.valueOf(totalCount));
            result.setCurrentPage(currentPage);
            result.setPageStr(String.valueOf(pageCount));
            result.setStr(str);
            result.setResult("success");
        } else {
            result.setResult("error");
        }
        return result;
    }


    /**
     * 获取总页数
     * @param pageSize
     * @param totalCount
     * @return
     */
    public int getPageCount(String pageSize, int totalCount) {
        int totalPage = 0; //总页数
        int pageSizein = 0; //每页显示数量(整型)

        if (pageSize != null && !pageSize.equals("")){
            pageSizein = TypeTool.getInt(pageSize);
        }

        if (totalCount % pageSizein == 0) {
            totalPage = totalCount / pageSizein;
        } else {
            totalPage = totalCount / pageSizein + 1;
        }

        return totalPage;
    }
}
