/**
 * Created by Cuibin on 2016/4/8.
 */

/**
 * 显示新闻类型添加窗口
 */
function showAdd(id,name){
    var addFid = $("#addFid").val(id);
    $("#add_super").val(name);
    $("#addPage").attr("style","display: block");
}

/**
 * 显示新闻类型编辑窗口
 */
function showEdit(name,describe,id,fid){
    $("#myModal_id").val(id);
    $("#myModal_name").val(name);
    $("#myModal_describe").val(describe);
    $("#myModal_fid").val(fid);
    $("#myModal").attr("style","display: block");
}

/**
 * 提交编辑后的类别(一级类别)
 */
function saveType(index){
    if(index == "1"){
        var id = $("#myModal_id").val();
        var fid = $("#myModal_fid").val();
        var name = $("#myModal_name").val();
        var describe = $("#myModal_describe").val();
        var reg = /^\s+$/gi;
        if(name == "" || reg.test(name)){
            alert("类别名称不能为空");
            return;
        }
        if(describe == "" || reg.test(describe)){
            alert("描述不能为空");
            return;
        }
    }else{
        var id = $("#myModal2_id").val();
        var fid = $("#myModal2_fid").val();
        var name = $("#myModal2_son_name").val();
        var describe = '';
        var reg = /^\s+$/gi;
        if(name == "" || reg.test(name)){
            alert("子类名称不能为空");
            return;
        }
    }

    var temp = {id:id,name:name,description:describe,fid:fid};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manage/saveType",
        data: temp2,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
            closeEdit();
            closeEdit2();
            openForecast("/platform/news_type.jetx","queryType");
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
}

/**
 *关闭父类别编辑窗口
 */
function closeEdit(){
    $("#myModal").attr("style","display: none");
}

/**
 *显示子类别编辑窗口
 */
function showEdit2(name,superName,id,fid){
    $("#myModal2_id").val(id);
    $("#myModal2_super").val(superName);
    $("#myModal2_son_name").val(name);
    $("#myModal2_fid").val(fid);
    $("#myModal2").attr("style","display: block");
}

/**
 *关闭子类别编辑窗口
 */
function closeEdit2(){
    $("#myModal2").attr("style","display: none");
}
/**
 * 删除新闻
 */
function deleteNews(id){
    if(!confirm("确定要删除该条新闻吗?")){
        return;
    }
    var newsMes = {id:id};
    $.ajax({
        type: "post",
        url: "/manage/deleteNews",
        data: newsMes,
        dataType: "json",
        success: function (data) {
            if(data.result == 'success'){
                alert('删除成功！');
                queryNewsList();
            }else{
                alert('删除失败！');
            }
        },
        err: function (data) {
            alert('删除失败！');
        }
    });
}


//保存新闻
function saveNews(){
    var kindId = $("#secondTypeId").val(); //二级类别id
    var title = $("#title").val(); //题目
    var content = CKEDITOR.instances.editor.getData(); //内容
    var autor = $("#author").val(); //作者
    var showTime = $("#showTime").val(); //显示时间

    if(kindId.length == 0){
        alert('请选择类别！');
        returnn;
    }
    if(title.length == 0){
        alert('请填写题目！');
        return;
    }
    if(autor.length == 0){
        alert('请填写作者！');
        return;
    }
    if(showTime.length == 0){
        alert('请填写显示时间');
        return;
    }
    if(content.length == 0){
        alert('请填写内容');
        return;
    }

    var newsMes = {kindId:kindId,title:title,content:content,createUser:autor,showDate:showTime};

    $.ajax({
        type: "post",
        url: "/manage/saveNews",
        data: newsMes,
        dataType: "json",
        success: function (data) {
            if(data.result == 'success'){
                alert(data.info);
                queryNewsList();
            }else{
                alert(data.info);
                queryNewsList();
            }
        },
        err: function (data) {
            alert(data.info)
        }
    });
}


//显示新闻列表
function queryNewsList(){
    var newsContentMes = {menuUrl:'platform/news.jetx'};
    $.ajax({
        type: "post",
        url: '/manage/queryNewsByPage',
        data: newsContentMes,
        dataType: "json",
        success: function (data) {
            $('#nowPage').html(data.htmlText);
        },
        err: function (data) {
            alert('查询失败!');
        }
    });
}

//显示新闻修改界面
function showNews(id){
    var newsContentMes = {id:id,menuUrl:'platform/news_edit.jetx'}; //新闻部分
    $.ajax({
        type: "post",
        url: '/manage/queryNewsById',
        data: newsContentMes,
        dataType: "json",
        success: function (data) {
            $('#nowPage').html(data.htmlText);
        },
        err: function (data) {
            alert('查询失败!');
        }
    });
}

/**
 * 修改新闻
 */
function updateNews(){
    var newsId = $("#newsId").val(); //新闻主键
    var title = $("#title").val(); //题目
    var author = $("#author").val(); //作者
    var showTime = $("#showTime").val(); //显示时间
    var content = CKEDITOR.instances.editor.getData(); //内容
    var secondTypeId = $("#secondTypeId").val(); //二级类别id

    if(secondTypeId.length == 0){
        alert('类别不能为空！');
        return;
    }
    if(title.length == 0){
        alert("题目不能为空！");
        return;
    }
    if(author.length == 0){
        alert("作者不能为空！");
        return;
    }
    if(showTime.length == 0){
        alert("显示时间不能为空！");
        return;
    }
    if(content.length == 0){
        alert("内容不能为空！");
        return;
    }

    var newsMes = {id:newsId,kindId:secondTypeId,title:title,createUser:author,showDate:showTime,content:content};

    $.ajax({
        type:"post",
        url:"/manage/updateNews",
        data:newsMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert(data.info);
                queryNewsList();
            }else{
                alert(data.info);
            }
        },
        err:function(data){
            alert(data.info);
        }
    });
}

//点击二级类别
function doSecondTypePd(){
    var firstTypeId = $("#firstTypeId").val(); //一级类别id
    if(firstTypeId.length == 0){
        alert('请选择一级类别！');
        return;
    }
}

//根据一级类别获取二级类别
function getSecondType(){
    var firstType = $("#firstTypeId").val(); //一级类别
    var secondType = $("#secondTypeId").val(); //二级类别
    var newsTypeMes = {fid:firstType,id:secondType};

    $.ajax({
        type: "post",
        url: '/manage/getSecondTypeByFirstType',
        data: newsTypeMes,
        dataType: "json",
        success: function (data) {
            $("#secondTypeId").html(data);
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}

//根据条件查询新闻
function queryNewsBySome(){
    var firstTypeId = $("#firstTypeId").val(); //一级类别
    var secondTypeId = $("#secondTypeId").val(); //二级类别
    var title = $("#title").val(); //题目
    var orderType = $("#orderType").val(); //排序类型
    var orderColumn = $("#orderColumn").val(); //排序字段
    var newsMes = {firstType:firstTypeId,kindId:secondTypeId,title:title,orderType:orderType,orderColumn:orderColumn};
    getNewsBySome(newsMes);
}

//默认排序
function selectmr(){
    var firstTypeId = $("#firstTypeId").val(); //一级类别
    var secondTypeId = $("#secondTypeId").val(); //二级类别
    var title = $("#title").val(); //题目
    $("#orderType").val(''); //排序类型
    $("#orderColumn").val(''); //排序字段

    /********** 样式处理Start **********/
    $("#limr").removeClass('active');
    $("#litj").removeClass('active');
    $("#lixs").removeClass('active');
    $("#limr").addClass('active');
    $("#litj").html('添加时间');
    $("#lixs").html('显示时间');
    /*********** 样式处理End ***********/

    var newsMes = {firstType:firstTypeId,kindId:secondTypeId,title:title,orderType:'',orderColumn:''};
    getNewsBySome(newsMes);
}

//按添加时间排序
function selecttj(){
    var firstTypeId = $("#firstTypeId").val(); //一级类别
    var secondTypeId = $("#secondTypeId").val(); //二级类别
    var title = $("#title").val(); //题目
    var strTj = $("#litj").html(); //添加时间
    var strXs = $("#lixs").html(); //显示时间
    var orderType = ''; //排序类型

    $("#orderColumn").val('create_date'); //排序字段

    /********** 样式处理Start **********/
    $("#limr").removeClass('active');
    $("#litj").removeClass('active');
    $("#lixs").removeClass('active');
    $("#litj").addClass('active');
    /*********** 样式处理End ***********/

    if(strTj.indexOf('↑') >= 0){ //添加时间
        strTj = '添加时间↓';
        strXs = '显示时间';
        $("#lixs").html(strXs);
        $("#litj").html(strTj);
        $("#orderType").val('DESC');
    }else if(strTj.indexOf('↓') >= 0){
        strTj = '添加时间↑';
        strXs = '显示时间';
        $("#lixs").html(strXs);
        $("#litj").html(strTj);
        $("#orderType").val('ASC');
    }else{
        strTj = strTj + '↑';
        $("#litj").html(strTj); //添加时间部分
        strXs = '显示时间';
        $("#lixs").html(strXs); //显示时间
        $("#orderType").val('ASC');
    }
    orderType = $("#orderType").val(); //排序类型
    var newsMes = {firstType:firstTypeId,kindId:secondTypeId,title:title,orderType:orderType,orderColumn:'create_date'};
    getNewsBySome(newsMes);
}

//按显示时间排序
function selectxs(){
    var firstTypeId = $("#firstTypeId").val(); //一级类别
    var secondTypeId = $("#secondTypeId").val(); //二级类别
    var title = $("#title").val(); //题目
    var strTj = $("#litj").html(); //添加时间
    var strXs = $("#lixs").html(); //显示时间
    var orderType = ''; //排序类型

    $("#orderColumn").val('show_date'); //排序字段

    /********** 样式处理Start **********/
    $("#limr").removeClass('active');
    $("#litj").removeClass('active');
    $("#lixs").removeClass('active');
    $("#lixs").addClass('active');
    /*********** 样式处理End ***********/

    if(strXs.indexOf('↑') >= 0){ //添加时间
        strTj = '添加时间';
        strXs = '显示时间↓';
        $("#lixs").html(strXs);
        $("#litj").html(strTj);
        $("#orderType").val('DESC');
    }else if(strXs.indexOf('↓') >= 0){
        strTj = '添加时间';
        strXs = '显示时间↑';
        $("#lixs").html(strXs);
        $("#litj").html(strTj);
        $("#orderType").val('ASC');
    }else{
        strXs = strXs + '↑';
        strTj = '添加时间';
        $("#litj").html(strTj); //添加时间部分
        $("#lixs").html(strXs); //显示时间
        $("#orderType").val('ASC');
    }
    orderType = $("#orderType").val(); //排序类型
    var newsMes = {firstType:firstTypeId,kindId:secondTypeId,title:title,orderType:orderType,orderColumn:'show_date'};
    getNewsBySome(newsMes);
}

//打开添加新闻部分
function openAddNews(){
    var newsMes = {menuUrl:'platform/news_add.jetx'};

    $.ajax({
        type: "post",
        url: "/manage/openAddNews",
        data: newsMes,
        dataType: "json",
        success: function (data) {
            $('#nowPage').html(data.htmlText);
        },
        err: function (data) {
            alert('打开页面失败！');
        }
    });
}

/**
 * 关闭添加窗口
 */
function closeAdd(){
    $("#addPage").attr("style","display: none");
}

/**
 * 更改是否显示
 */
function updateShow(id,index){
    if($('#checkShow'+index).is(':checked')){
        var availability = '0';
    }else{
        var availability = '1';
    }
    var temp = {id:id,availability:availability};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manage/updateShow",
        data: temp2,
        dataType: "json",
        success: function (data) {
            if(data.result == 'success'){
                alert(data.info);
            }
        },
        err: function (data) {

        }
    });
}

/**
 * 更改是否推荐
 */
function updateRecommend(id,index){
    if($('#checkIsR'+index).is(':checked')){
        var isRecommend = '0';
    }else{
        var isRecommend = '1';
    }
    var temp = {id:id,isRecommend:isRecommend};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manage/updateRecommend",
        data: temp2,
        dataType: "json",
        success: function (data) {
            if(data.result == 'success'){
                alert(data.info);
            }
        },
        err: function (data) {

        }
    });
}

/**
 * 类别管理添加二级类别
 */
function addTwoType(){
    var fid = $("#addFid").val();
    var name = $("#add_son_name").val();
    var temp = {fid:fid,name:name};
    $.ajax({
        type: "post",
        url: "/manage/addTwoType",
        data: temp,
        dataType: "json",
        success: function (data) {
            if(data.result == 'success'){
                alert(data.info);
                openForecast("/platform/news_type.jetx","queryType");
            }
        },
        err: function (data) {

        }
    });
}

/**
 * 删除二级类别
 */
function deleteTwoNewsType(id){
    if(!confirm("确定要删除吗")){
        return;
    }
    var temp = {id:id};
    $.ajax({
        type:"post",
        url: "/manage/deleteTwoNewsType",
        data:temp,
        dataType: "json",
        success:function(data){
            if(data.result == 'success'){
                openForecast("/platform/news_type.jetx","queryType");
            }
        },
        error:function(data){

        }
    });
}




/***************  分页部分Start ***************/
function getNewsBySome(newsMes){
    $.ajax({
        type:"post",
        url: "/manage/queryNewsBySome",
        data:newsMes,
        dataType:"json",
        success:function(data){
            $("#tbodyNews").html(data.info); //新闻内容
            $("#currentPage").html(data.currentPage); //当前页
            $("#pageCount").html(data.count); //总页数
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function NewsStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var firstTypeId = $("#firstTypeId").val(); //第一类别
    var secondTypeId = $("#secondTypeId").val(); //第二类别
    var title = $("#title").val(); //题目
    var newsMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,firstType:firstTypeId,kindId:secondTypeId,title:title};
    getNewsBySome(newsMes); //根据条件查询新闻
}

//尾页
function NewsEndPage(){
    var totalCount = $("#newsTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var firstTypeId = $("#firstTypeId").val(); //第一类别
    var secondTypeId = $("#secondTypeId").val(); //第二类别
    var title = $("#title").val(); //题目
    var newsMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,firstType:firstTypeId,kindId:secondTypeId,title:title};
    getNewsBySome(newsMes); //根据条件查询新闻
}

//上一页
function NewsUp(){
    var currentPage = $("#currentPage").html(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var firstTypeId = $("#firstTypeId").val(); //第一类别
    var secondTypeId = $("#secondTypeId").val(); //第二类别
    var title = $("#title").val(); //题目
    var newsMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,firstType:firstTypeId,kindId:secondTypeId,title:title};
    getNewsBySome(newsMes); //根据条件查询新闻
}

//下一页
function NewsDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").html(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#newsTotalCount").val(); //总数量
    var pageSize = "10";

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数s
    var firstTypeId = $("#firstTypeId").val(); //第一类别
    var secondTypeId = $("#secondTypeId").val(); //第二类别
    var title = $("#title").val(); //题目
    var newsMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,firstType:firstTypeId,kindId:secondTypeId,title:title};
    getNewsBySome(newsMes); //根据条件查询新闻
}

//跳转到指定页数
function doGoNewsPage(){
    var newsNum = $("#newsNum").val(); //需跳转页数
    var currentPage = newsNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    if(newsNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(newsNum)){
        alert('请输入合法的数字！');
        return;
    }
    var firstTypeId = $("#firstTypeId").val(); //第一类别
    var secondTypeId = $("#secondTypeId").val(); //第二类别
    var title = $("#title").val(); //题目
    var newsMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,firstType:firstTypeId,kindId:secondTypeId,title:title};
    getNewsBySome(newsMes); //根据条件查询新闻
}
/*************** 分页部分End ***************/
