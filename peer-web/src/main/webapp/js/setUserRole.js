/**
 * 初始化下拉列表
 * @param userInfo
 * @returns {*}
 */
function queryRoleInfo(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/getUserRole",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 初始化下拉列表
 * @param userInfo
 * @returns {*}
 */
function queryUserInfo(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/getUserInfo",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 关闭方法
 */
function closeSetUserRole() {
    $("#setUserId").empty();
    $("#setRoleId").empty();
    var e1 = document.getElementById("setUserRole");
    e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
}

/**
 * 关闭方法
 */
function closeUpdateUserRole() {
    $("#updateRoleId").empty();
    $("#updateUserId").empty();
    var e1 = document.getElementById("updateSetUserRole");
    e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
}

function initCombo() {
    var userInfo = "";
    $.when(queryRoleInfo(userInfo)).done(function(data) {
        var text = [];
        for (var i = 0; i < data.length; i++) {
            text.push('<option value="' + eval(data)[i].id + '">' + eval(data)[i].roleName
                + '</option>');
        }
        $("#setRoleId").append(text.join(''));
    });
    $.when(queryUserInfo(userInfo)).done(function(data) {
        var text = [];
        for (var i = 0; i < data.length; i++) {
            text.push('<option value="' + eval(data)[i].id + '">' + eval(data)[i].enUserName
                + '</option>');
        }
        $("#setUserId").append(text.join(''));
    });
}

/**
 * 用户组添加数据
 */
function addSetUserRole() {
    initCombo();
    var e1 = document.getElementById("setUserRole");
    e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
}

/**
 * 用户组添加数据
 */
function onAddSetUserRole() {
    var userInfo = $("#addSetForm").serialize();
    $.when(saveSetUserRole(userInfo)).done(function (data) {
        if (data.result == "error") {
            alert("添加失败" + data.info);
        } else {
            alert("添加成功");
            closeSetUserRole();
        }
    });
}

function saveSetUserRole(userInfo) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manager/addSetUserRole",
        data: userInfo,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 用户组修改数据
 */
function updateSetUserRole(id, userId, roleId) {
    var userInfo = "";
    $.when(queryRoleInfo(userInfo)).done(function(data) {
        var text = [];
        for (var i = 0; i < data.length; i++) {
            text.push('<option value="' + eval(data)[i].id + '">' + eval(data)[i].roleName
                + '</option>');
        }
        $("#updateRoleId").append(text.join(''));
    });
    $.when(queryUserInfo(userInfo)).done(function(data) {
        var text = [];
        for (var i = 0; i < data.length; i++) {
            text.push('<option value="' + eval(data)[i].id + '">' + eval(data)[i].enUserName
                + '</option>');
        }
        $("#updateUserId").append(text.join(''));
    });
    var e1 = document.getElementById("updateSetUserRole");
    e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
    var uid = document.getElementById("id");
    uid.setAttribute("value", id);
    var updateUserId = document.getElementById("updateUserId");
    updateUserId.value=userId;
    var updateRoleId = document.getElementById("updateRoleId");
    updateRoleId.value=roleId;
}

/**
 *用户组修改
 */
function onUpdateSetUserRole() {
    var userInfo = $("#updateSetForm").serialize();
    $.when(updateUserRole(userInfo)).done(function (data) {
        if (data.result == "error") {
            alert("修改失败" + data.info);
        } else {
            alert("修改成功");
            closeUpdateUserRole();
        }
    });
}

function updateUserRole(userInfo) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manager/updateSetUserRole",
        data: userInfo,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 用户组table表格删除一行数据
 */
function delSetUserRole(user) {
    var flag = confirm("是否删除该行数据？");
    if (flag) {
        var defer = $.Deferred();
        var parm = {id: user};
        $.ajax({
            type: "post",
            url: "/manager/delSetUserRole",
            data: $.param(parm),
            dataType: "json",
            success: function (data) {
                defer.resolve(data);
            },
            err: function (data) {
                defer.resolve("undefined");
            }
        });
        return defer.promise();
    }
}
