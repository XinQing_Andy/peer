/**
 * 订单状态跳转
 */
function openOrder(menuUrl,controlUrl,state ){
    var menu = {jetxName: menuUrl,id:state};
    console.log(menu);
    var userMenu = $.param(menu);
    $.when(seeOrder(userMenu, controlUrl)).done(function (data) {
        if (data.result == "error") {
        } else {
            $("#tabCon").empty();
            $('#tabCon').html(data.htmlText);
        }
    });
}
function seeOrder(userMenu, controlUrl) {
    console.log(userMenu, controlUrl);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: controlUrl,
        data: userMenu,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

