/**
 * Created by cuibin on 2016/5/16.
 */
/**
 * 开启添加银行窗口
 */
function openbankAdd(){
    $("#myModal").attr("style","display: block");
}

/**
 * 关闭添加银行窗口
 */
function closebankAdd(){
    $("#bankname").val("");
    $("#bz").val("");
    $("#myModal").attr("style","display: none");
}
/**
 * 开启更新银行窗口
 * @param id
 */
function openBankUpdate(id){
    var temp = {id:id};
    $.ajax({
        type: "post",
        url: "/bank/queryOnlyBank",
        data: temp,
        dataType: "json",
        success: function (data) {
            $("#bankname2").val(data.bankname);
            $("#bz2").val(data.bz);
            $("#did").val(data.id);

            $("#myModal2").attr("style","display: block");
        },
        err: function (data) {

        }
    });
}
/**
 * 关闭更新银行窗口
 */
function closeBankUpdate(){
    $("#myModal2").attr("style","display: none");
}
/**
 * 保存银行
 */
function saveBank(){
    var bankname = $("#bankname").val();
    var bz = $("#bz").val();
    var reg = /^\s+$/gi;
    if(bankname == "" || reg.test(bankname)){
        alert("银行名称不能为空");
        return;
    }
    if(reg.test(bz)){
        alert("请不要输入纯空格");
        return;
    }
    var temp = {bankname:bankname,bz:bz};
    $.ajax({
        type: "post",
        url: "/bank/saveBank",
        data: temp,
        dataType: "json",
        success: function (data) {
            closebankAdd();
            alert(data.info);
            openForecast("/bank/bank.jetx","/bank/queryAllBank");
        },
        err: function (data) {

        }
    });

}
/**
 * 更新银行
 */
function updateBank(){
    var bankname = $("#bankname2").val();
    var bz = $("#bz2").val();
    var did = $("#did").val();
    var reg = /^\s+$/gi;
    if(bankname == "" || reg.test(bankname)){
        alert("银行名称不能为空");
        return;
    }
    if(reg.test(bz)){
        alert("请不要输入纯空格");
        return;
    }
    var temp = {id:did,bankname:bankname,bz:bz};
    $.ajax({
        type: "post",
        url: "/bank/updateBank",
        data: temp,
        dataType: "json",
        success: function (data) {
            closeBankUpdate();
            alert(data.info);
            openForecast("/bank/bank.jetx","/bank/queryAllBank");
        },
        err: function (data) {

        }
    });

}
/**
 * 删除银行
 */
function deleteBank(id){
    if(!confirm("是否删除？"))
        return;
    var temp = {id:id};
    $.ajax({
        type: "post",
        url: "/bank/deleteBank",
        data: temp,
        dataType: "json",
        success: function (data) {
            alert(data.info);
            openForecast("/bank/bank.jetx","/bank/queryAllBank");
        },
        err: function (data) {

        }
    });
}
/**
 * 按照关键字查询银行
 *
 */
function queryBankByKeyWord(){
    var keyword = $("#keyword").val();
    var temp = {keyword:keyword};
    $.ajax({
        type: "post",
        url: "/bank/queryAllBank",
        data: temp,
        dataType: "json",
        success: function (data) {
            $("#nowPage").empty();
            $('#nowPage').html(data.htmlText);
        },
        err: function (data) {

        }
    });
}
/***************  分页部分Start ***************/
//根据条件查询广告
function getBankBySome(advert){
    $.ajax({
        type:"post",
        url: "/bank/queryAllBank",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function bankStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var keyword = $("#keyword").val();
    var advert = {keyword:keyword,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBankBySome(advert); //根据条件查询广告
}

//尾页
function bankEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var keyword = $("#keyword").val();
    var advert = {keyword:keyword,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBankBySome(advert); //根据条件查询广告
}

//上一页
function bankUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var keyword = $("#keyword").val();
    var advert = {keyword:keyword,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBankBySome(advert); //根据条件查询广告
}

//下一页
function bankDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var keyword = $("#keyword").val();
    var advert = {keyword:keyword,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBankBySome(advert); //根据条件查询广告
}

//跳转到指定页数
function doGoBankPage(page){
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var keyword = $("#keyword").val();
    var advert = {keyword:keyword,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBankBySome(advert); //根据条件查询广告
}


/*************** 分页部分End ***************/

