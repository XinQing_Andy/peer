/**
 * 后台主界面
 * Created by lenovo on 2015/12/10.
 */
$(document).ready(function () {
});

/**
 * 拿到父级菜单
 * @param menuId
 */
function callMenuInfo(menuId, menuUrl, isChildren, level, controlUrl, menuName) {
    console.log(menuId, menuUrl);
    var menu = {userId: '', menuParentId: menuId, jetxName: menuUrl, isChildren: isChildren, controlUrl: controlUrl};
    var userMenu = $.param(menu);
    //是不是首页
    if (menuId == 'index') {
        loadTwoLevelMenu(userMenu);
        return;
    }
    //一级菜单调用二级菜单
    if (level == 1) {
        if (isChildren == 'false') {
            $('#left_menu').html('无菜单');
        } else {
            console.log("一级菜单调用");
            loadTwoLevelMenu(userMenu);
        }
    }
    //二级菜单调用三级菜单
    if (level == 2) {
        if (isChildren == 'false') {
            //无三级菜单的情况需要加载二级菜单的右侧展示页面
            loadMainPage(userMenu, menuUrl, controlUrl, menuName);
        } else {
            loadThreeLevelMenu(menuId);
        }
    }
    //三级菜单调用
    if (level == 3) {
        if (isChildren == 'false') {
            loadMainPage(userMenu, menuUrl, controlUrl, menuName);
        }
    }

}
/**
 * 加载二级菜单
 */
function loadTwoLevelMenu(userMenu) {
    $.when(getMenuHtml(userMenu)).done(function (data) {
        if (data == "undefined") {
            $('#left_menu').html('无法访问服务资源..');
        }
        if (data.result == "error") {
            $('#left_menu').html('出错了...');
        } else {
            $('#left_menu').html(data.htmlText);
        }
    });
}
/**
 * 加载三级菜单
 * @param userMenu
 */
function loadThreeLevelMenu(menuId) {
    var menu = {userId: '', menuParentId: menuId, jetxName: 'menu/menu_children.jetx', isChildren: 'false'};
    var userMenu = $.param(menu);
    $.when(getMenuHtml(userMenu)).done(function (data) {
        if (data == "undefined") {
            //$('ul').remove('.zhan');
            $('#menu_' + menuId).after('无法访问服务资源..');
        }
        if (data.result == "error") {
            //$('ul').remove('.zhan');
            $('#menu_' + menuId).after('出错了...');
        } else {
            //$('ul').remove('.zhan');
            $('#menu_' + menuId).after(data.htmlText);
        }
    });
}
/**
 * 加载展示页面
 * @param userMenu
 */
function loadMainPage(userMenu, menuUrl, controlUrl, menuName) {
    //添加页签
    addTab(menuUrl, controlUrl, menuName, userMenu);
}
/**
 * 添加页签
 * @param userMenu
 * @param controlUrl
 */
function addTab(menuUrl, controlUrl, menuName, userMenu) {
    //拿到页头列表
    var $tablist = $('#tabList');
    var $tabLi = $tablist.find('li');
    if ($tabLi.length == 0) {
        $('#tabList').append("<ul><li name='" + menuUrl + "' value='" + controlUrl + "'><div class='tabItem'><div class='menu_text'>" + menuName + "</div><a href='javascript:void(0)' class='iconclose icon--11' onclick='closeTab(event);return false;'></a></div></li></ul>");
        addTabPage(0, userMenu, controlUrl);
    } else {
        //不存在-1是添加，如果存在就是对应的索引
        var index = isTabExist(menuName);
        if (index == -1) {
            $('#tabList > ul').append("<li name='" + menuUrl + "' value='" + controlUrl + "'><div class='tabItem'><div class='menu_text'>" + menuName + "</div><a href='javascript:void(0)' class='iconclose icon--11' onclick='closeTab(event);return false;'></a></div></li>");
            addTabPage($tabLi.length, userMenu, controlUrl);
        } else {
            //查找
            findTabPage(index);
        }
    }
    //加载事件
    loadTab();
}
/**
 * 查找页面
 * @param index
 */
function findTabPage(index) {
    $('#tabCon > div').each(function (idex) {
        if (index == idex) {
            $(this).removeClass('uncur');
            $(this).addClass('cur');
        } else {
            $(this).removeClass('cur');
            $(this).addClass('uncur');
        }
    });
    $("#tabList > ul > li ").each(function(idex){
        if (index == idex) {
            $(this).removeClass('unselected');
            $(this).addClass('selected');
        } else {
            $(this).removeClass('selected');
            $(this).addClass('unselected');
        }
    });

}
/**
 * 删除页面
 * @param index
 */
function removeTabPage(index){
    //删除
    $('#tabCon > div').each(function (idex) {
        if (index == idex) {
            $(this).remove();
        }
    });
    var count = $('#tabCon').children().length;
    if(count-1<0)
        return;
    //显示
    $('#tabCon > div').each(function (idex) {
        if(idex==0){
            $(this).removeClass('uncur');
            $(this).addClass('cur');
        }
    });
    $("#tabList > ul > li ").each(function(idex){
        if (idex==0) {
            $(this).removeClass('unselected');
            $(this).addClass('selected');
        } else {
            $(this).removeClass('selected');
            $(this).addClass('unselected');
        }
    });
}
/**
 * 加载页签界面
 * @param index
 */
function addTabPage(index, userMenu, controlUrl) {
    console.log("userMenu==" + userMenu);
    console.log("controlUrl==" + controlUrl);
    //添加页签内容
    $.when(getMainHtml(userMenu, controlUrl)).done(function (data) {
        if (data == "undefined") {
            $('#tabCon').append('<div>无法访问服务资源..</div>');
            findTabPage(index);
        }
        if (data.result == "error") {
            $('#tabCon').append('<div>出错了...</div>');
            findTabPage(index);
        } else {
            $('#tabCon').append('<div>' + data.htmlText + '</div>');
            findTabPage(index);
        }
    });
}
/**
 * 是否存在标签
 */
function isTabExist(menuName) {
    var index = -1;
    //查看头页签对象
    var $tab = $('#tab');
    //拿到页头列表
    var $tablist = $tab.children('.tabList');
    var $tabLi = $tablist.find('li');
    for (var i = 0; i < $tabLi.length; i++) {
        var mName = $($tabLi[i]).find('div .menu_text').html();
        if (menuName == mName) {
            index = i;
            break;
        }
    }
    return index;
}
/**
 * 得到TAB标签
 */
function loadTab() {
    //查看头页签对象
    var $tab = $('#tab');
    //拿到页头列表
    var $tablist = $tab.children('.tabList');
    var $tabLi = $tablist.find('li');
    //注册监听
    for (var i = 0; i < $tabLi.length; i++) {
        //赋值索引
        $tabLi[i].index = i;
        $tabLi[i].onclick = function () {
            mainPageShow(this);
        }
    }
}
/**
 * 关闭页签
 */
function closeTab(event) {
    //阻止事件冒泡
    event.stopPropagation();
    var $li = $(event.target).parent().parent();
    var index = $li[0].index;
    console.log("关闭" + index);
    //查看头页签对象
    var $tab = $('#tab');
    //拿到页头列表
    var $tablist = $tab.children('.tabList');
    var $tabLi = $tablist.find('li');
    //删除TAB页
    for (var i = $tabLi.length - 1; i >= 0; i--) {
        if ($tabLi.length - 1 == 0) {
            $($tabLi[i]).parent().remove();
            break;
        }
        if (i == index) {
            $($tabLi[i]).remove();
        }
    }
    //关闭页面
    removeTabPage(index);
    loadTab();
}
/**
 * 加载主界面
 */
function mainPageShow($tab) {
    findTabPage($tab.index);
}
/**
 * 得到菜单的HTML
 */
function getMainHtml(userMenu, controlUrl) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: controlUrl,
        data: userMenu,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 * 得到菜单的HTML
 */
function getMenuHtml(userMenu) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/modular/menu",
        data: userMenu,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 用户组table表格删除一行数据
 */
function onDelRole(user) {
    var flag = confirm("是否删除该行数据？");
    if (flag) {
        var defer = $.Deferred();
        var parm = {id: user};
        $.ajax({
            type: "post",
            url: "/manager/delUserRole",
            data: $.param(parm),
            dataType: "json",
            success: function (data) {
                defer.resolve(data);
            },
            err: function (data) {
                defer.resolve("undefined");
            }
        });
        return defer.promise();
    }
}

/**
 * 用户组修改数据
 */
function onUpdateRole(id, code, name) {
    var e1 = document.getElementById("update_permission");
    e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
    var roleId = document.getElementById("roleId");
    roleId.setAttribute("value", id);
    var roleCode = document.getElementById("roleCode");
    roleCode.setAttribute("value", code);
    var roleName = document.getElementById("roleName");
    roleName.setAttribute("value", name);
}

/**
 *用户组修改
 */
function onUpdateRoleForm() {
    var userInfo = $("#updateForm").serialize();
    $.when(updateRole(userInfo)).done(function (data) {
        if (data.result == "error") {
            alert("修改失败" + data.info);
        } else {
            alert("修改成功");
            var e1 = document.getElementById("update_permission");
            e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
        }
    });
}

function updateRole(userInfo) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manager/updateUserRole",
        data: userInfo,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 用户组添加数据
 */
function addPer() {
    var e1 = document.getElementById("permission");
    e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
}

/**
 * 用户组添加数据
 */
function onAddRole() {
    var userInfo = $("#addForm").serialize();
    $.when(addRole(userInfo)).done(function (data) {
        if (data.result == "error") {
            alert("添加失败" + data.info);
        } else {
            alert("添加成功");
            addPer();
        }
    });
}

function addRole(userInfo) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manager/addUserRole",
        data: userInfo,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

