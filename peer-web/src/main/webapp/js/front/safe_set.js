/**
 * Created by Cuibin on 2016/5/2.
 */

/**
 * 跳转到安全设置
 */
function goSecuritySettings(){
    $.ajax({
        type:"post",
        url:"/security/goSecuritySettings",
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
                location.href='/front/show';
            }
        },
        err:function(data){

        }
    });
}

/**
 * 跳转到修改密码
 */
function goPassWordUpdate(){
    $.ajax({
        type:"post",
        url:"/security/goPassWordUpdate",
        data:null,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
                location.href='/front/show';
            }
        },
        err:function(data){

        }
    });
}
/**
 *更新密码
 */
function updatePassWord(){
    if(!checkNowPassWord() || !checkUpdatePassWord()){
        return;
    }
    var password = $("#password").val();
    var newPassword = $("#newPassword").val();
    var newPassword2 = $("#newPassword2").val();
    var temp = {oldPassWord:password,password:newPassword};
    $.ajax({
        type:"post",
        url:"/security/updatePassWord",
        data:temp,
        dataType:"json",
        success:function(data){
            $("#confirm").attr({"disabled":"disabled"});
            alert(data.info);
        },
        err:function(data){

        }
    });
}
/**
 * 验证当前密码
 */
function checkNowPassWord(){
    var password = $("#password").val();
    var reg = /^\s+$/gi;
    if(password == "" || reg.test(password)){
        $("#passwordMessage").html("<font color='red'>当前登陆密码不能为空</font>");
        return false;
    }
    if(password.length > 14){
        return false;
    }
    $("#passwordMessage").html("");
    return true;
}
/**
 *验证修改密码
 */
function checkUpdatePassWord(){
    var newPassword = $("#newPassword").val();
    var newPassword2 = $("#newPassword2").val();
    var reg = /^\s+$/gi;
    if(newPassword == "" || reg.test(newPassword)){
        $("#passwordMessage").html("<font color='red'>新密码不能为空</font>");
        return false;
    }
    if(newPassword.length < 6){
        $("#passwordMessage").html("<font color='red'>新密码不能小于6位</font>");
        return false;
    }
    if(newPassword2 != newPassword){
        $("#passwordMessage").html("<font color='red'>两次输入的密码不一致</font>");
        return false;
    }
    if(newPassword.length > 14){
        return false;
    }
    $("#passwordMessage").html("");
    return true;
}
/**
 *========================================修改密码end====================================================
 */
/**
 *========================================设置紧急联系人start====================================================
 */

/**
 * 跳转到设置紧急联系人
 */
function goContact(){
    $.ajax({
        type:"post",
        url:"/security/goContact",
        data:null,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
                location.href='/front/show';
            }
        },
        err:function(data){

        }
    });
}
/**
 * 保存紧急联系人
 */
function saveContact(){
    if(!checkphoneNo())
        return;
    var name = $("#name").val();
    var phone = $("#phone").val();
    var relation = $("#relation").val();
    var temp = {telephone:phone,xm:name,relationship:relation};
    $.ajax({
        type:"post",
        url:"/security/saveContact",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
                location.href='/front/show';
            }
        },
        err:function(data){

        }
    });

}
/**
 * 验证手机号
 */
function checkphoneNo(){
    var name = $("#name").val();
    var phone = $("#phone").val();
    var relation = $("#relation").val();
    var reg = /^\s+$/gi;
    var reg2 = /^1\d{10}$/;
    var reg3 = /^[a-zA-Z\u4e00-\u9fa5]+$/;
    if(name == "" || reg.test(name)){
        $("#nameId").html("<font color='red'>姓名不能为空</font>");
        return false;
    }
    if(!reg3.test(name)){
        $("#nameId").html("<font color='red'>姓名格式不正确</font>");
        return false;
    }
    $("#nameId").html("");
    if(phone == "" || reg.test(phone)){
        $("#phoneId").html("<font color='red'>手机号不能为空</font>");
        return false;
    }
    if(!reg2.test(phone)){
        $("#phoneId").html("<font color='red'>手机格式不正确</font>");
        return false;
    }
    $("#phoneId").html("");
    if(relation == "none"){
        $("#relationId").html("<font color='red'>请选择与联系人关系</font>");
        return false;
    }
    $("#relationId").html("");
    return true;
}
/**
 *========================================设置紧急联系人end====================================================
 */
/**
 *========================================邮箱设置start====================================================
 */
/**
 * 跳转到邮箱设置
 */
function goEmailSet(){
    $.ajax({
        type:"post",
        url:"/security/goEmailSet",
        data:null,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
                location.href='/front/show';
            }

        },
        err:function(data){

        }
    });
}
/**
 * 跳转到去邮箱验证
 */
function goEmailCheck(){
    var email = $("#email").val();
    if(!checkEmailSet())
        return;
    var temp = {email:email};
    $.ajax({
        type:"post",
        url:"/security/goEmailCheck",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
            }

        },
        err:function(data){

        }
    });
}
/**
 * 再次发送邮件
 */
function twoGoEmailCheck(){
    var email = $("#email").val();
    var temp = {email:email};
    $.ajax({
        type:"post",
        url:"/security/goEmailCheck",
        data:temp,
        dataType:"json",
        success:function(data){
            $("#zcfs").attr("style","color: gray");
            $("#zcfs").html("再次发送");
        },
        err:function(data){

        }
    });
}
/**
 * 跳转邮箱设置成功页面
 */
function goEmailSetSuccess(){
    var email = $("#email").val();
    var temp = {email:email};
    $.ajax({
        type:"post",
        url:"/security/goEmailSetSuccess",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
            }
        },
        err:function(data){

        }
    });
}
/**
 * 验证邮箱
 */
function checkEmailSet(){
    var email = $("#email").val();
    var reg = /^\s+$/gi;
    var reg2 = /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/;
    if(reg.test(email) || email == ""){
        $("#emailMessage").html("<font color='red'>邮箱不能为空</font>");
        return false;
    }
    if(!reg2.test(email)){
        $("#emailMessage").html("<font color='red'>格式不正确</font>");
        return false;
    }
    $("#emailMessage").html("");
    return true;
}
/**
 * ================================================================
 */
/**
 * 请先去设置密保问题 倒计时
 */
function pleaseSetQuestionTime(){
    var num = $("#num").html();
    if(num > 0){
        num--;
        $("#num").html(num);
    }else{
        goProtect();
        return;
    }
    setTimeout(function() {pleaseSetQuestionTime()},1000);
}
/**
 * 保护问题获取验证码
 */
function questionGetCode() {
    $("#hqyzmcon").attr({"disabled": "true"});

    $.ajax({
        type: "post",
        url: "/security/setQuestionGetCode",
        success: function (data) {
            $("#message").html(data.info);
            if (data.result == "success") {
                questionTime();
            }

        }
        ,
        err: function (data) {
        }
    });
}
/**
 * 保护问题: 获取验证码倒计时
 */
function questionTime() {

    var num = $("#timeSum").val();
    if (num > 0) {
        num--;
        $("#hqyzmcon").val(num + "秒后可重发");
        $("#timeSum").val(num);
    } else {
        $("#hqyzmcon").removeAttr("disabled");
        $("#hqyzmcon").val("获取验证码");
        $("#timeSum").val(61);
        return;
    }
    setTimeout(function () {
        questionTime()
    }, 1000);
}
/**
 * 跳转到保护问题
 */
function goProtect(){
    $.ajax({
        type:"post",
        url:"/security/goProtect",
        data:null,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
            }

        },
        err:function(data){

        }
    });
}
/**
 * 跳转到填写保护问题
 */
function goSetProtect(){
    var code = $("#code").val();
    var reg = /^\s+$/gi;
    if(code == "" || reg.test(code)){
        $("#message").html("验证码不能为空");
        return;
    }
    var temp = {
        code: code
    };
    $.ajax({
        type:"post",
        url:"/security/goSetProtect",
        data: temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                $("#message").html(data.info);
            }

        },
        err:function(data){

        }
    });
}
/**
 * 跳转到确认问题
 */
function goConfirmProtect(){
    var questionone = $("#questionone").val();
    var questiontwo = $("#questiontwo").val();
    var questionthree = $("#questionthree").val();
    var answerone = $("#answerone").val();
    var answertwo = $("#answertwo").val();
    var answerthree = $("#answerthree").val();
    var reg = /^\s+$/gi;
    if(questionone == "none"){
        alert("请选择问题一");
        return;
    }
    if(questiontwo == "none"){
        alert("请选择问题二");
        return;
    }
    if(questionthree == "none"){
        alert("请选择问题三");
        return;
    }
    if(answerone == "" || reg.test(answerone)){
        alert("答案一不能为空");
        return;
    }
    if(answertwo == "" || reg.test(answertwo)){
        alert("答案二不能为空");
        return;
    }
    if(answerthree == "" || reg.test(answerthree)){
        alert("答案三不能为空");
        return;
    }
    if(answerone.length > 20 || answertwo.length > 20 || answerthree.length > 20){
        return;
    }
    var temp = {questionone:questionone,questiontwo:questiontwo,questionthree:questionthree,answerone:answerone,answertwo:answertwo,answerthree:answerthree};
    $.ajax({
        type:"post",
        url:"/security/goConfirmProtect",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
                location.href='/front/show';
            }

        },
        err:function(data){

        }
    });
}
/**
 * 保存问题
 */
function saveQuestion(){
    var questionone = $("#questiononeId").val();
    var questiontwo = $("#questiontwoId").val();
    var questionthree = $("#questionthreeId").val();
    var answerone = $("#answerone").val();
    var answertwo = $("#answertwo").val();
    var answerthree = $("#answerthree").val();

    var reg = /^\s+$/gi;
    if(questionone == "" || reg.test(questionone)){
        return;
    }
    if(questiontwo == "" || reg.test(questiontwo)){
        return;
    }
    if(questionthree == "" || reg.test(questionthree)){
        return;
    }
    if(answerone == "" || reg.test(answerone)){
        return;
    }
    if(answertwo == "" || reg.test(answertwo)){
        return;
    }
    if(answerthree == "" || reg.test(answerthree)){
        return;
    }
    if(questionone.length > 20 || questiontwo.length > 20 || questionthree.length > 20 || answerone.length > 20 ||
        answertwo.length > 20 || answerthree.length > 20){
            return;
    }

    var temp = {questionone:questionone,questiontwo:questiontwo,questionthree:questionthree,answerone:answerone,answertwo:answertwo,answerthree:answerthree};

    $.ajax({
        type:"post",
        url:"/security/saveQuestion",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
                location.href='/front/show';
            }

        },
        err:function(data){

        }
    });

}
/**
 * 倒计时
 */
function time(){
    var num = $("#num").html();
    if(num > 0){
        num--;
        $("#num").html(num);
    }else{
        goSecuritySettings();
        return;
    }
    setTimeout(function() {time()},1000);
}
/**
 * ============================================================================================
 */
/**
 * 请去设置手机号 倒计时
 */
function pleaseSetPhone(){
    var num = $("#num").html();
    if(num > 0){
        num--;
        $("#num").html(num);
    }else{
        openPhoneSet();
        return;
    }
    setTimeout(function() {pleaseSetPhone()},1000);
}
/**
 * 跳转到设置交易密码
 */
function goSetTransactionPwd(){
    $.ajax({
        type:"post",
        url:"/security/goSetTransactionPwd",
        data:null,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
                location.href='/front/show';
            }

        },
        err:function(data){

        }
    });
}
/**
 * 保存交易密码
 */
function saveTransactionPwd(){
    if(!checkTransactionPwd())
        return;

    var traderpassword = $("#traderpassword").val(); //交易密码
    var code = $("#code").val();
    var temp = {
        traderpassword:traderpassword,
        code:code
    };
    $.ajax({
        type:"post",
        url:"/security/saveTransactionPwd",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                $("#message").html(data.info);
            }

        },
        err:function(data){

        }
    });
}
/**
 * 验证交易密码
 */
function checkTransactionPwd(){
    var traderpassword = $("#traderpassword").val();
    var traderpasswordTwo = $("#traderpasswordTwo").val();

    var reg = /^\s+$/gi;
    if(traderpassword == "" || reg.test(traderpassword)){
        $("#traderpasswordMessage").html("<font color='red'>密码不能为空</font>");
        return false;
    }
    if(traderpassword.length < 6){
        $("#traderpasswordMessage").html("<font color='red'>密码长度不能小于6位</font>");
        return false;
    }
    if(traderpassword != traderpasswordTwo){
        $("#traderpasswordMessage").html("<font color='red'>两次输入的密码不一致</font>");
        return false;
    }
    if(traderpassword.length > 14){
        return false;
    }
    $("#traderpasswordMessage").html("");
    return true;
}

/**
 * 验证交易密码强度
 */
function checkIntension(pwd){
    var reg = /^\s+$/gi;
    if(pwd == "" || reg.test(pwd)){
        $("#intension").html('');
        return;
    }
    var rules=[{
        reg:/\d+/,
        weight:2
    },{
        reg:/[a-z]+/,
        weight:4
    },{
        reg:/[A-Z]+/,
        weight:8
    },{
        reg:/[~!@#\$%^&*\(\)\{\};,.\?\/'"]/,
        weight:16
    }];

    var strongLevel={
        '0-10':'1',
        '10-20':'2',
        '20-30':'3'
    };
    var testPasswords=[pwd];
    for(var i=0;i<testPasswords.length;i++){
        var weight=0;
        for(var j=rules.length-1;j>=0;j--){
            if(rules[j].reg.test(testPasswords[i])){
                weight|=rules[j].weight;
            }
        }
        var key='20-30';
        if(weight<=10)key='0-10';
        else if(weight<=20)key='10-20';
        //console.log(testPasswords[i],strongLevel[key]);
        var temp = strongLevel[key];
        if(temp == '1'){
            $("#intension").html('<p class="kuang"><span style="position: relative; color: #FFFFFF; font-size: 10px; top:-4px">低</span></p> <p class="kuang" style="background-color: #DCDCDC;"></p> <p class="kuang" style="background-color: #DCDCDC;"></p>');
        }else if(temp == '2'){
            $("#intension").html('<p class="kuang"></p><p class="kuang"><span style="position: relative; color: #FFFFFF; font-size: 10px; top:-4px">中</span></p><p class="kuang" style="background-color: #DCDCDC;"></p>');
        }else if(temp == '3'){
            $("#intension").html('<p class="kuang"></p> <p class="kuang"></p> <p class="kuang"><span style="position: relative; color: #FFFFFF; font-size: 10px; top:-4px">高</span></p>');
        }
        return;
    }
}
/**
 * 获取验证码
 */
function obtainCode() {
    $("#hqyzm").attr({"disabled": "true"});

    $.ajax({
        type: "post",
        url: "/security/sendMessageCheck",
        success: function (data) {
            $("#message").html(data.info);
            if(data.result == "success"){
                traTime();
            }

        }
        ,
        err: function (data) {
        }
    });

}
/**
 * 设置交易密码: 获取验证码倒计时
 */
function traTime() {

    var num = $("#timeSum").val();
    if (num > 0) {
        num--;
        $("#hqyzm").val(num + "秒后可重发");
        $("#timeSum").val(num);
    } else {
        $("#hqyzm").removeAttr("disabled");
        $("#hqyzm").val("获取验证码");
        $("#timeSum").val(61);
        return;
    }
    setTimeout(function () {
        traTime()
    }, 1000);
}


/**
 * 跳转到环迅注册
 */
function openRealName() {
    $.ajax({
        type: "POST",
        url: "/security/openRealName",
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText);
            } else {
                alert(data.info);
                location.href = '/front/show';
            }
        },
        err: function (data) {

        }
    });
}


/**
 * ===============================================================
 */


/**
 * =============================修改手机认证==================================
 */
/**
 * 跳转设置手机号
 */
function openPhoneSet(){

    $.ajax({
        type: "post",
        url: "/security/openPhoneSet",
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText);
            } else {
                alert(data.info);
            }

        },
        err: function (data) {

        }
    });
}
/**
 * 跳转到修改手机认证
 */
function openReplaceTelephone(){

    $.ajax({
        type: "post",
        url: "/security/openReplaceTelephone",
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText);
            } else {
                alert(data.info);
            }

        },
        err: function (data) {

        }
    });
}

/**
 * 跳转到短信修改手机号
 */
function openReplaceTelephoneNote(){

    $.ajax({
        type: "post",
        url: "/security/openReplaceTelephoneNote",
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText);
            } else {
                alert(data.info);
            }

        },
        err: function (data) {

        }
    });
}
/**
 * 修改手机认证:获取手机验证码
 */
function replaceGetCode(){
    $("#hqyzm").attr({"disabled": "true"});
    $.ajax({
        type: "post",
        url: "/security/replaceGetCode",
        success: function (data) {
            if (data.result == 'success') {
                $("#message").html(data.info);
                replaceTime();
            } else {
                alert(data.info);
            }

        },
        err: function (data) {

        }
    });
}

function replaceTime() {

    var num = $("#timeSum").val();
    if (num > 0) {
        num--;
        $("#hqyzm").val(num + "秒后可重发");
        $("#timeSum").val(num);
    } else {
        $("#hqyzm").removeAttr("disabled");
        $("#hqyzm").val("获取验证码");
        $("#timeSum").val(61);
        return;
    }
    setTimeout(function () {
        replaceTime()
    }, 1000);
}
/**
 * 跳转到 设置新手机号
 */
function openNewTraderpasswordSet (){
    if(!checkTPwd())
        return;
    var code = $("#code").val();
    var traderpassword = $("#traderpassword").val();

    var temp = {
        traderpassword:traderpassword,
        code:code
    };

    $.ajax({
        type:"post",
        url:"/security/openNewTraderpasswordSet",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                $("#message").html(data.info);
            }

        },
        err:function(data){

        }
    });
}
/**
 * 修改手机认证:验证交易密码
 * @returns {boolean}
 */
function checkTPwd(){
    var traderpassword = $("#traderpassword").val();

    var reg = /^\s+$/gi;
    if(traderpassword == "" || reg.test(traderpassword)){
        $("#message").html("<font color='red'>密码不能为空</font>");
        return false;
    }
    if(traderpassword.length > 14){
        return false;
    }
    $("#message").html("");
    return true;
}
/**
 * 设置新手机号:获取验证码
 */
function setPhoneGetCode(){
    $("#hqyzmTwo").attr({"disabled": "true"});
    var telephone = $("#telephone").val();
    var reg = /^\s+$/gi;
    if(telephone == "" || reg.test(telephone)){
        $("#message").html("手机号不能为空");
        $("#hqyzmTwo").removeAttr("disabled");
        return;
    }
    var reg2 = /^1\d{10}$/;
    if(!reg2.test(telephone)){
        $("#message").html("手机号格式不正确");
        $("#hqyzmTwo").removeAttr("disabled");
        return;
    }
    var temp = {
        telephone:telephone
    };
    $.ajax({
        type: "post",
        url: "/security/setPhoneGetCode",
        data:temp,
        dataType:"json",
        success: function (data) {
            if (data.result == 'success') {
                $("#message").html(data.info);
                setPhoneTime();
            } else {
                $("#message").html(data.info);
                $("#hqyzmTwo").removeAttr("disabled");
            }

        },
        err: function (data) {

        }
    });
}
function setPhoneTime() {

    var num = $("#timeSum").val();
    if (num > 0) {
        num--;
        $("#hqyzmTwo").val(num + "秒后可重发");
        $("#timeSum").val(num);
    } else {
        $("#hqyzmTwo").removeAttr("disabled");
        $("#hqyzmTwo").val("获取验证码");
        $("#timeSum").val(61);
        return;
    }
    setTimeout(function () {
        setPhoneTime()
    }, 1000);
}
/**
 * 保存新手机号
 */
function setNewPhone(){
    $("#tj").attr({"disabled": "true"});
    var telephone = $("#telephone").val();
    var code = $("#code").val();
    var reg = /^\s+$/gi;
    if(code == "" || reg.test(code)){
        $("#message").html("验证码不能为空");
        $("#tj").removeAttr("disabled");
        return;
    }
    if(telephone == "" || reg.test(telephone)){
        $("#message").html("手机号不能为空");
        $("#tj").removeAttr("disabled");
        return;
    }
    var temp = {
        telephone:telephone,
        code:code
    };

    $.ajax({
        type: "post",
        url: "/security/setNewPhone",
        data:temp,
        dataType:"json",
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText);
                setPhoneSuccesstime();
            } else {
                $("#message").html(data.info);
                $("#tj").removeAttr("disabled");
            }

        },
        err: function (data) {

        }
    });
}
/**
 * 保存手机号成功 倒计时
 */
function setPhoneSuccesstime(){
    var num = $("#num").html();
    if(num > 0){
        num--;
        $("#num").html(num);
    }else{
        goSecuritySettings();
        return;
    }
    setTimeout(function() {setPhoneSuccesstime()},1000);
}
/**
 * 跳转到密保问题修改手机号
 */
function goQuestionUpdatePhone(){
    $.ajax({
        type:"post",
        url:"/security/goQuestionUpdatePhone",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
            }

        },
        err:function(data){

        }
    });
}
/**
 * 验证密保
 */
function checkQuestion(){
    var traderpassword = $("#traderpassword").val();
    var idcard = $("#idcard").val();
    var code = $("#answer").val();

    var temp = {
        traderpassword:traderpassword,
        idcard:idcard,
        code:code
    };
    $.ajax({
        type:"post",
        url:"/security/checkQuestion",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                $("#message").html(data.info);
            }

        },
        err:function(data){

        }
    });
}

/**
 * =============================修改手机认证==================================
 */

/**
 * 安全等级
 */
var lock = 0;
function safeRank() {
    var i = $("#amount").val();
    if (i < 3) {
        $("#total_safe").val("33");
        $("#grade").html("低");
    } else if (i < 5) {
        $("#total_safe").val("66");
        $("#grade").html("中");
    } else {
        $("#total_safe").val("100");
        $("#grade").html("高");
    }
    if(lock == 0){
        run();
    }
    lock ++;

}

/**
 * 递归
 */
function run() {
    var bar = document.getElementById("bar_safe"); // 或得dom对象
    var total = document.getElementById("total_safe"); //隐藏域数值
    bar.style.width = parseInt(bar.style.width) + 1 + "%"; //给dom对象赋值
    total.innerHTML = bar.style.width; //更新隐藏域数值
    if (bar.style.width == $("#total_safe").val() + "%") {
        window.clearTimeout();
        lock = 0;
        return;
    }
    window.setTimeout("run()", 10);
}

