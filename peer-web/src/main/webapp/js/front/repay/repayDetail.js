/**
  * 还款明细
  * by zhangerxin
  *
  **/

//打开还款明细
function openRepayDetail(){
    var keyword = $("#keyword").val(); //关键字
    var keyWordDate = $("#keyWordDate").val(); //关键日期
    var value = $("#keyvalue").val();
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var pageSize = $("#pageSize").val();
    var pageIndex = $("#pageIndex").val();
    var repayDetailMes = {
        menuUrl:'front/repay/repayDetail.jetx',
        pageSize:pageSize,
        pageIndex:pageIndex,
        keyword:keyword,
        keyWordDate:keyWordDate,
        value:value,
        startTime:startTime,
        endTime:endTime
    };

    $.ajax({
        type:"post",
        url:"/repayRecord/queryRepayDetail",
        data:repayDetailMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
            }
        },
        err:function(data){
            alert(data.info);
        }
    });
}

//还款冻结
function doRepayFreeze(repayId){
    $.ajax({
        type:"post",
        url:"/huanxun/freezeRepay",
        data:"repayId="+repayId,
        dataType:"json",
        success:function(data){
            if(data.resultCode = '0'){ //成功
                $("#sign").val(data.sign); //签名
                $("#request").val(data.request); //请求信息
                $("#formRepay").submit();
            }else{
                alert(data.resultMsg);
            }
        },
        err:function(data){
            alert(data.resultMsg);
        }
    });
}