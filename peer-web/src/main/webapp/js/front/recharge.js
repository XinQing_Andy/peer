/**
 * Created by cuibin on 2016/6/17.
 */


/**
 * 跳转到充值
 * chargeType:充值类型
 */
function openRecharge(chargeType){

    var rechargeMes = {chargeType:chargeType,menuUrl:'/front/recharge/recharge.jetx'};

    $.ajax({
        type:"post",
        url:"/recharge/openRecharge",
        data:rechargeMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert('查询失败!');
            }
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}


/**
 * 跳转到充值记录
 */
function openRechargeHistory() {
    var huanxunRechargeMes = {menuUrl:'/front/recharge/recharge_history.jetx'};

    $.ajax({
        type:"post",
        url:"/recharge/openRechargeHistory",
        data:huanxunRechargeMes,
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText);
            } else {
                alert('查询失败')
            }
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}

/**
 * 根据时间查询充值记录
 * @param mes
 */
function queryRechargeHistoryByTime() {
    var startTime = $("#rhStartTime").val(); //充值起日期
    var endTime = $("#rhEndTime").val(); //充值止日期
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = ''
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }

    var viewHuanxunRechargeMes = {startTime:startTime,endTime:endTime,pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        data:viewHuanxunRechargeMes,
        url:"/recharge/searchRechargeBySome",
        dataType:"json",
        success: function (data) {
            if (data.result == 'success') {
                $('#ulRechargeStr').html(data.str); //充值记录拼接
                $('#ulPageStr').html(data.pageStr); //分页部分拼接
            } else {
                alert('查询失败');
            }
        },
        err: function (data) {
            alert('查询失败');
        }
    });
}


//还款充值价格改变
function doChangePrice(){
    var depositType = $("#chargeType").val(); //充值类型(1-普通充值、2-还款充值)
    var trdAmt = $("#recharge").val(); //充值金额
    if(depositType != '' || typeof (depositType) != 'undefined'){
        if(depositType == '2'){
            trdAmt = parseFloat(trdAmt) + 5;
            $("#recharge").val(trdAmt);
        }
    }
}



//充值(环迅部分)
function recharge(){
    var operationType = $("#operationType").val(); //操作类型
    var merchantID = $("#merchantID").val(); //商户存管交易账号
    var trdAmt = $("#recharge").val(); //充值金额
    var depositType = $("#chargeType").val(); //充值类型(1-普通充值、2-还款充值)
    var huanxunRechargeMes = {operationType:operationType,depositType:depositType,merchantID:merchantID,trdAmt:trdAmt};

    $.ajax({
        type:"post",
        data:huanxunRechargeMes,
        url:"/huanxun/rechargehx",
        dataType:"json",
        success: function (data) {
            if (data.code == '0') { //执行状态(0-成功、1-失败)
                $("#sign").val(data.sign); //签名
                $("#request").val(data.request); //请求信息
                $("#rechargeHx").submit();
            } else {
                alert(data.info);
            }
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}


//查询环迅银行
function queryBank(){
    $.ajax({
        type: "post",
        url: "/huanxun/queryBankhx",
        dataType: "json",
        success: function (data) {
            $("#sign").val(data.sign);
            $("#request").val(data.request);
            $("#bankhx").submit();
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}


/**
 *验证时间
 */
function rechargeHistoryCheckTime(){
    var val = $("#rhStartTime").val();
    var val2 = $("#rhEndTime").val();
    var time1 = new Date(val).getTime();
    var time2 = new Date(val2).getTime();
    if(time1 > time2 || time1 == time2){
        alert("开始时间必须小于结束时间");
        return false;
    }
    return true;
}


/********************   分页部分    ********************/

/**
 * 上一页
 */
function rechargeLastPage() {
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo <= 1) {
        $("#pageNo").html(1);
        return;
    } else {
        $("#pageNo").html(pageNo - 1);
    }

    queryRechargeHistoryByTime();
}
/**
 * 下一页
 */
function rechargeNextPage() {
    var totalPage = $("#totalPage").val();
    totalPage = parseInt(totalPage);
    if (totalPage < 2) {
        return;
    }
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo >= totalPage) {
        $("#pageNo").html(totalPage);
    } else {
        $("#pageNo").html(pageNo + 1);
    }

    queryRechargeHistoryByTime();
}
/**
 * 第一页
 */
function rechargeFirstPage() {
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo == 1) {
        return;
    }
    $("#pageNo").html(1);
    queryRechargeHistoryByTime();
}

/**
 * 最后一页
 */
function rechargeEndPage() {
    var totalPage = $("#totalPage").val();
    totalPage = parseInt(totalPage);
    if (totalPage < 2) {
        return;
    }
    $("#pageNo").html($("#totalPage").val());
    queryRechargeHistoryByTime();
}
/**
 * 跳转到指定页数
 */
function rechargeGoPage() {
    var totalPage = $("#totalPage").val();
    if (totalPage < 2) {
        return;
    }
    var inputPage = $("#inputPage").val();
    if (inputPage > totalPage || inputPage <= 0) {
        return;
    }

    $("#pageNo").html(inputPage);
    queryRechargeHistoryByTime();
}

/********************   分页部分    ********************/
/**
 * 按条件搜索
 */
