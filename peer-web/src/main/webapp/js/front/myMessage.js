/**
 * Created by Cuibin on 2016/6/24.
 */

/**
 * 跳转到我的消息
 */
function openMyMessage(){
    var readflag = $("#readflag").val(); //读取状态(0-已读、1-未读)
    var startTime = $("#startTime").val(); //发送起时间
    var endTime = $("#endTime").val(); //发送止时间
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    var myMessageMes = {orderColumn:'CREATETIME',delFlg:'1',readflag:readflag,startTime:startTime,
        endTime:endTime,pageIndex:pageIndex,pageSize:pageSize,orderType:'DESC'};
    $.ajax({
        type:"post",
        url:"/myMessage/openMyMessage",
        data: myMessageMes,
        success:function(data){
            if(data.result == "success"){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}

//更新分页内容
function checkPage(){
    $("#pageIndex").val('1');
    $("#pageSize").val('3');
}


/**
 * 跳转到消息详情
 */
function openMyMessageDetails(id,num){
    var myMessageMes = {id:id,pageIndex:num};
    $.ajax({
        type:"post",
        data: myMessageMes,
        url:"/myMessage/openMyMessageDetails",
        success:function(data){
            if(data.result == "success"){
                $(".p_right_wordbox").html(data.htmlText);
            }
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}


/**
 * 查询下一条详情
 * @param id
 * @param num
 */
function openNextMyMessageDetails(){
    var num = $("#num").val();
    var myMessageMes = {pageIndex:num,pageSize:1,delFlg:1};

    $.ajax({
        type:"post",
        data: myMessageMes,
        url:"/myMessage/openNextMyMessageDetails",
        success:function(data){
            if(data.result == "success"){
                $(".p_right_wordbox").html(data.htmlText);
            }
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}

//删除消息
function delMyMessage(){
    var id = $("#messageId").val(); //主键
    var myMessageMes = {id:id};

    if(confirm("删除后将不能恢复，是否确认删除？") == false){
        return;
    }

    $.ajax({
        type:"post",
        data: myMessageMes,
        url:"/myMessage/delMyMessageDetails",
        success:function(data){
            if(data.code == "-1"){
                alert("删除失败！");
            }else{
                alert("删除成功！");
                openMyMessage();
            }
        },
        err:function(data){
            alert('查询失败！');
            openMyMessage();
        }
    });
}

//全选
function doSelectAll() {
    var checkSelected = ''; //已选择的checkbox的id
    var selectAll = document.getElementById("selectAll"); //全选按钮
    var checkboxMessage = document.getElementsByName("checkboxMessage");
    if (selectAll.checked){
        for(var i = 0; i < checkboxMessage.length; i++){
            checkboxMessage[i].checked = true;
            if(checkSelected == '' || typeof (checkSelected) == 'undefined'){
                checkSelected = checkboxMessage[i].value;
            }else{
                checkSelected = checkSelected + ',' + checkboxMessage[i].value;
            }
        }
        $("#checkSelected").val(checkSelected);
    }else{
        for(var i = 0; i < checkboxMessage.length; i++){
            checkboxMessage[i].checked = false;
        }
        $("#checkSelected").val('');
    }
}

//根据每个复选框判断全选按钮是否被选中
function doSelect(){
    var flag = true; //用于判断全选按钮是否被选中
    var checkSelected = ''; //被选中的checkbox的id
    var checkboxMessage = document.getElementsByName("checkboxMessage"); //每个复选框
    var selectAll = document.getElementById("selectAll"); //全选按钮
    for (var i = 0; i < checkboxMessage.length; i++) {
        if (checkboxMessage[i].checked == true) { //被选中
            if(checkSelected == '' || typeof (checkSelected) == 'undefined'){
                checkSelected = checkboxMessage[i].value;
            }else{
                checkSelected = checkSelected + ',' + checkboxMessage[i].value;
            }
        }else{
            flag = false;
        }
    }
    $("#checkSelected").val(checkSelected); //被选中的checkbox的id
    if(flag == true){
        selectAll.checked = true;
    }else{
        selectAll.checked = false;
    }
}


//批量删除消息
function delBatchMyMessage(){
    var checkSelected = $("#checkSelected").val(); //被选中的checkbox的id
    var myMessageMes = {checkSelected:checkSelected};

    if(checkSelected == '' || typeof (checkSelected) == 'undefined'){
        alert('请选择要删除的消息！');
        return;
    }

    if(confirm("删除后将不能恢复，是否确认删除？") == false){
        return;
    }
    $.ajax({
        type:"post",
        data: myMessageMes,
        url:"/myMessage/delBatchMyMessage",
        success:function(data){
            if(data == "-1"){
                alert("删除失败！");
            }else{
                alert("删除成功！");
                openMyMessage();
            }
        },
        err:function(data){
            alert('删除失败！');
            openMyMessage();
        }
    });
}

//批量标记为已读
function delBatchUpdateReadFlg(){
    var checkSelected = $("#checkSelected").val(); //被选中的checkbox的id
    var myMessageMes = {checkSelected:checkSelected};
    if(checkSelected == '' || typeof (checkSelected) == 'undefined'){
        alert('请选择要标记的消息！');
        return;
    }
    $.ajax({
        type:"post",
        data: myMessageMes,
        url:"/myMessage/updateBatchReadFlg",
        success:function(data){
            if(data == "-1"){
                alert("标记失败！");
            }else{
                alert("标记成功！");
                openMyMessage();
            }
        },
        err:function(data){
            alert('删除失败！');
            openMyMessage();
        }
    });
}