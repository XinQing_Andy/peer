<!-- 债权转让 -->

//债权转让(上面按钮)
function prepareattorn(typeId,typeName){
    var productMes = {type:typeId,typeName:typeName,menuUrl:'front/attorn/attorn.jetx'}; //产品部分

    $.ajax({
        type:"post",
        url: "/attornRecord/queryAttornByPage",
        data:productMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $('#accountDiv').html(data.htmlText);
                if(typeId != ''){ //选择了其中一种产品类型
                    $("#attornProductType").removeClass('one_input');
                    $("#attornProductType").addClass('two_input');
                    $('#type'+typeId).removeClass('two_input');
                    $('#type'+typeId).addClass('one_input');
                }
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


//债权转让信息部分
function queryProductAttornById(id){
    var productMes = {id:id,menuUrl:'front/attorn/attorn_add.jetx'};
    $.ajax({
        type:"post",
        url:"/attornRecord/queryAttornInfo",
        data:productMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $('#accountDiv').html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


//债权转让(环迅)
function zhuanranghx(){
    /********** 环迅参数部分Start **********/
    var operationType = 'trade.freeze'; //操作类型
    var merchantID = '1810060028'; //商户存管交易账户
    var merBillNo = '181006'; //商户订单号
    var projectNo = $("#projectNo").val(); //项目id号
    var bizType = '2'; //业务类型(1-投标、2-债权)
    var regType = '1' //登记方式(1-手动、2-自动)
    var contractNo = ''; //合同号
    var authNo = ''; //授权号
    var trdAmt = $("#trdAmt").val(); //冻结金额
    var merFee = 0; //平台手续费
    var freezeMerType = '1'; //冻结方类型(1-用户、2-商户)
    var ipsAcctNo = $("#ipsAcctNo").val(); //冻结账号
    var otherIpsAcctNo = $("#otherIpsAcctNo").val(); //它方账号
    var webUrl = $("#webUrl").val(); //页面返回地址
    var s2SUrl = $("#s2SUrl").val(); //后台通知地址
    /*********** 环迅参数部分End ***********/

    var attornId = $("#attornId").val(); //债权id
    var huanxunFreezeMes = {operationType:operationType,merchantID:merchantID,merBillNo:merBillNo,
        projectNo:projectNo,bizType:bizType,regType:regType,contractNo:contractNo,
        authNo:authNo,trdAmt:trdAmt,merFee:merFee,freezeMerType:freezeMerType,attornId:attornId,
        ipsAcctNo:ipsAcctNo,otherIpsAcctNo:otherIpsAcctNo,webUrl:webUrl,s2SUrl:s2SUrl};

    $.ajax({
        type:"post",
        url: "/huanxun/freezeAttornhx",
        data: huanxunFreezeMes,
        dataType:"json",
        success:function(data){
            $("#sign").val(data.sign); //签名
            $("#request").val(data.request); //请求信息
            $("#freezehx").submit();
        },
        err:function(data){
            console.log(data);
        }
    });
}


//承接债权转让
function saveAttorn(id,buyer,applyMemberId,balance,price){
    var traderpassword = $("#traderpassword").val(); //交易密码
    var captcha = $("#captcha").val(); //验证码
    var productAttornRecordMes = {id:id,buyer:buyer,applyMemberId:applyMemberId,balance:balance,price:price,traderpassword:traderpassword,captcha:captcha};

    $.ajax({
        type:"post",
        url: "/attornRecord/addAttorn",
        data:productAttornRecordMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert('承接成功！');
                queryProductAttornById(id);
            }else{
                alert(data.info);
                queryProductAttornById(id);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//标签控制显示(1-借款信息、2-投资记录、3-还款列表)
function doConAttornShow(flag){
    if(flag == '1'){ //借款信息
        /**********样式部分Start**********/
        $("#borrowInfo").removeClass('det');
        $("#lendRecord").removeClass('det');
        $("#repaymentList").removeClass('det');
        $("#borrowInfo").addClass('det');
        /**********样式部分End**********/
        $("#borrowInfoDiv").css('display','block');
        $("#lendRecordDiv").css('display','none');
        $("#repaymentListDiv").css('display','none');
    }else if(flag == '2'){ //投资记录
        getInvestRecord();
    }else if(flag == '3'){ //还款列表
        getRepayList();
    }
}


//债权详情
function doQueryProductDetails(id,productBuyId,productId){
    var productAttornMes = {id:id,investId:productBuyId,productId:productId,menuUrl:'/front/attorn/attornProduct_details.jetx'};
    $.ajax({
        type:"post",
        url: "/attornRecord/doQueryAttornProductDetails",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}