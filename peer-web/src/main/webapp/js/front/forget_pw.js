/**
 * Created by Cuibin on 2016/7/4.
 */
/**
 * 跳转到忘记密码手机号验证
 */
function openforgetPw(){
    $.ajax({
        type:"post",
        url:"/security/openforgetPw",
        success:function(data){
            if(data.result == 'success'){
                $("#accountDiv").html(data.htmlText);
                $("#mainDiv").html(data.htmlText);

                $(".landing").slideUp(200);
                $("#isEnter").val("0");
                emptyMessage();
            }

        },
        err:function(data){

        }
    });
}


/**
 * 获取验证码
 */
function forgetPwGetCode(){
    $("#hqyzm").attr("class","pwd_yzm2");
    $("#hqyzm").attr({"disabled": "true"});
    var telephone = $("#telephone").val();
    var reg = /^\s+$/gi;
    if(telephone == "" || reg.test(telephone)){
        $("#message").html("手机号不能为空");
        $("#hqyzm").removeAttr("disabled");
        $("#hqyzm").attr("class","pwd_yzm");
        return;
    }
    var reg2 = /^1\d{10}$/;
    if(!reg2.test(telephone)){
        $("#message").html("手机号格式不正确");
        $("#hqyzm").removeAttr("disabled");
        $("#hqyzm").attr("class","pwd_yzm");
        return;
    }
    var vv = {
        telephone:telephone
    };
    $.ajax({
        type:"post",
        url:"/security/forgetPwGetCode",
        data:vv,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $("#xyb").attr("class","pwd_xyb");
                $("#xyb").removeAttr("disabled");
                $("#message").html(data.info);
                forgetPwtime($("#num").val());
            }else{
                $("#message").html(data.info);
                forgetPwtime($("#num").val());
            }

        },
        err:function(){

        }

    });

}
/**
 * 倒计时
 */
function forgetPwtime(num){

    if(num > 0){
        num--;
        $("#num").val(num);
        $("#hqyzm").val(num+"秒后重获取");
        $("#hqyzm").attr("class","pwd_yzm2");
        $("#hqyzm").attr({"disabled": "true"});
    }else{
        $("#hqyzm").attr("class","pwd_yzm");
        $("#hqyzm").val("获取验证码");
        $("#hqyzm").removeAttr("disabled");
        $("#num").val("61");
        return;
    }
    setTimeout(function() {forgetPwtime(num)},1000);
}
/**
 * 验证验证码
 */
function forgetPwCheckCode(){
    $("#xyb").attr({"disabled": "true"});
    var code = $("#code").val();
    var telephone = $("#telephone").val();
    var reg = /^\s+$/gi;
    if(telephone == "" || reg.test(telephone)){
        $("#message").html("手机号不能为空");
        $("#xyb").removeAttr("disabled");
        return;
    }
    var reg2 = /^1\d{10}$/;
    if(!reg2.test(telephone)){
        $("#message").html("手机号格式不正确");
        $("#xyb").removeAttr("disabled");
        return;
    }
    var reg3 = /^\s+$/gi;
    if(code == "" || reg3.test(code)){
        $("#message").html("验证码不能为空");
        $("#xyb").removeAttr("disabled");
        return;
    }
    if(code.length < 6){
        $("#message").html("验证码有误");
        $("#xyb").removeAttr("disabled");
        return;
    }
    var vv = {
        telephone:telephone,
        code:code
    };
    $.ajax({
        type:"post",
        url:"/security/forgetPwCheckCode",
        data:vv,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $("#mainDiv").html(data.htmlText);
                $("#accountDiv").html(data.htmlText);
            }else{
                $("#message").html(data.info);
                $("#xyb").removeAttr("disabled");
            }

        },
        err:function(){

        }

    });
}
/**
 * 重置密码
 */
function reSetPw(){
    if(!forgetPwcheckUpw())
        return;
    var password = $("#password").val();
    var temp = {
        password:password
    };
    $.ajax({
        type:"post",
        url:"/security/reSetPw",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $("#mainDiv").html(data.htmlText);
                $("#accountDiv").html(data.htmlText);
                successForgetPwtime();
            }else{
                $("#message").html(data.info);
            }

        },
        err:function(){

        }
    });
}

/**
 * 验证修改密码
 */
function forgetPwcheckUpw(){
    var password = $("#password").val();
    var passwordTwo = $("#passwordTwo").val();
    var reg = /^\s+$/gi;
    if(password == "" || reg.test(password)){
        $("#message").html("新密码不能为空");
        return false;
    }
    if(password.length < 6){
        $("#message").html("新密码不能小于6位");
        return false;
    }
    if(passwordTwo != password){
        $("#message").html("两次输入的密码不一致");
        return false;
    }
    if(password.length > 14){
        return false;
    }
    $("#message").html("");
    return true;
}

/**
 * 修改成功倒计时
 */

function successForgetPwtime(){
    var num = $("#num").html();
    if(num > 0){
        num--;
        $("#num").html(num);
    }else{
        location.href='/front/show';
        return;
    }
    setTimeout(function() {successForgetPwtime()},1000);
}
