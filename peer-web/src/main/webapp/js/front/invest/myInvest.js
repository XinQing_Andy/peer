/**
  * 我的投资
  * @autor zhangerxin
  */


//我的项目中项目选择
function myInvestXmselect(bz){
    if(bz != '' && typeof(bz) != "undefined"){
        if(bz == '1'){ //回收中的项目
            doGoMyInvest();
        }else if(bz == '2'){ //投标中的项目
            doProductBuytb();
        }else if(bz == '3'){ //已结清的项目
            doProductBuyjq();
        }else if(bz == '4'){ //已申请的项目
            doProductBuysq();
        }
    }
}


//项目类型选择
function docheckType(lx,typeId){
    var arr = new Array();
    var typeIdStr = $("#typeIdStr").val(); //产品类型id拼接
    arr = typeIdStr.split(','); //数组，用于存放产品类型id
    var typeIdSelect = '';
    if(lx != '' && typeof (lx) != "undefined"){
        if(typeId == ''){ //全部
            $("#li"+lx+"0").removeClass('msim_txt'); //全部
            $("#li"+lx+"0").addClass('msim_all');
            $("#li"+lx+"zq").removeClass('msim_all'); //债权转让
            $("#li"+lx+"0").addClass('msim_txt');
            for(var i=0;i<arr.length;i++){
                $("#li"+lx+arr[i]).removeClass('msim_all');
                $("#li"+lx+arr[i]).addClass('msim_txt');
            }
            typeIdSelect = '';
        }else{
            if(typeId == 'zq'){ //债权
                $("#li"+lx+"0").removeClass('msim_all'); //全部
                $("#li"+lx+"0").addClass('msim_txt');
                $("#li"+lx+"zq").removeClass('msim_txt'); //债权
                $("#li"+lx+"zq").addClass('msim_all');
                for(var i=0;i<arr.length;i++){
                    $("#li"+lx+arr[i]).removeClass('msim_all');
                    $("#li"+lx+arr[i]).addClass('msim_txt');
                }
                typeIdSelect = '';
            }else{
                $("#li"+lx+"0").removeClass('msim_all'); //全部
                $("#li"+lx+"0").addClass('msim_txt');
                $("#li"+lx+"zq").removeClass('msim_all'); //债权
                $("#li"+lx+"0").addClass('msim_txt');
                for(var i=0;i<arr.length;i++){
                    if(typeId == arr[i]){
                        $("#li"+lx+arr[i]).removeClass('msim_txt');
                        $("#li"+lx+arr[i]).addClass('msim_all');
                    }else{
                        $("#li"+lx+arr[i]).removeClass('msim_all');
                        $("#li"+lx+arr[i]).addClass('msim_txt');
                    }
                }
                typeIdSelect = typeId;
            }
        }
    }
    if(lx == 'hs'){ //回收
        $("#typeIdSelecths").val(typeIdSelect);
        doProductBuyhs();
    }else if(lx == 'jq'){
        $("#typeIdSelectjq").val(typeIdSelect);
        doProductBuyjq();
    }
}


//根据条件查询回收中的项目
function doProductBuyhs(){

    /******************** 查询条件Start ********************/
    var SelectDateTypeHs = $("#SelectDateTypeHs option:selected").val(); //查询日期类型
    var SelectStartDateValueHs = $("#SelectStartDateValueHs").val(); //查询起日期
    var SelectEndDateValueHs = $("#SelectEndDateValueHs").val(); //查询止日期
    var SelectKeyWordHs = $("#SelectKeyWordHs option:selected").val(); //关键词选项
    var keyWordValueHs = $("#keyWordValueHs").val(); //关键词内容
    var typeIdSelect = $("#typeIdSelecths").val(); //回收中的项目类型
    /********************* 查询条件End *********************/

    var nextReceiveSTime = ''; //下次还款起日期
    var nextReceiveETime = ''; //下次还款止日期
    var buySTime = ''; //满标起日期
    var buyETime = ''; //满标止日期
    var title = ''; //题目
    var code = ''; //编码

    $("#tbPageStr").html(''); //投标项目分页
    $("#jqPageStr").html(''); //结清项目分页
    var pageIndex = $("#pageIndex").val(); //当前数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }

    if(SelectDateTypeHs != '' && typeof(SelectDateTypeHs) != "undefined"){
        if(SelectDateTypeHs == '0'){ //下期回款日
            nextReceiveSTime = SelectStartDateValueHs;
            nextReceiveETime = SelectEndDateValueHs;
        }else if(SelectDateTypeHs == '1'){ //购买日期
            buySTime = SelectStartDateValueHs;
            buyETime = SelectEndDateValueHs;
        }
    }

    if(SelectKeyWordHs != '' && typeof(SelectKeyWordHs) != "undefined"){
        if(SelectKeyWordHs == '0'){ //名称
            title = keyWordValueHs;
        }else if(SelectKeyWordHs == '1'){ //编码
            code = keyWordValueHs;
        }
    }

    var productInvestMes = {nextReceiveSTime:nextReceiveSTime,nextReceiveETime:nextReceiveETime,buyStartTime:buySTime,
                            pageIndex:pageIndex,pageSize:pageSize, buyEndTime:buyETime,title:title,productType:typeIdSelect,
                            code:code};

    $.ajax({
        type:"post",
        url: "/invert/getRecoverProduct",
        data:productInvestMes,
        dataType:"json",
        success:function(data){
            $("#tbodyHs").html(data.str); //列表部分
            $("#hsPageStr").html(data.pageStr); //分页部分
        },
        err:function(data){
            console.log(data);
        }
    });
}


//根据条件查询投标中的项目
function doProductBuytb(){
    var buyStartTime = $("#buyStartTime").val(); //购买起时间
    var buyEndTime = $("#buyEndTime").val(); //购买止时间
    var keyWord = $("#keyWord option:selected").val();//关键字
    var content = $("#content").val(); //搜索内容
    var title = ''; //标的题目
    var code = ''; //标的编码
    if(keyWord == '0'){ //标的名称
        title = content;
        code = '';
    }else if(keyWord == '1'){ //标的编码
        title = '';
        code = content;
    }
    $("#hsPageStr").html(''); //回收项目分页
    $("#jqPageStr").html(''); //结清项目分页
    $("#sqPageStr").html(''); //申请项目分页
    var pageIndex = $("#pageIndex").val(); //当前数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }

    var productInvestMes = {buyStartTime:buyStartTime,buyEndTime:buyEndTime,title:title,code:code,pageIndex:pageIndex,
                            pageSize:pageSize,orderColumn:'BUYTIME',orderType:'DESC',fullScale:'1'};
    $.ajax({
        type:"post",
        url: "/invert/getProcessProduct",
        data:productInvestMes,
        dataType:"json",
        success:function(data){
            $("#xmtbtbody").html(data.str); //列表部分内容
            $("#tbPageStr").html(data.str2); //分页分页部分
            $("#hsPageStr").html(''); //回收分页部分
            $("#jqPageStr").html(''); //结清分页部分
            $("#sqPageStr").html(''); //申请分页部分

            /********** 样式部分控制Start **********/
            $("#xmhsli").removeClass('active'); //回收
            $("#xmtbli").removeClass('active'); //投标
            $("#xmtbli").addClass('active'); //投标
            $("#xmjqli").removeClass('active'); //结清
            $("#xmsqli").removeClass('active'); //结清
            $('#xmhs').css('display','none');
            $('#xmtb').css('display','block');
            $('#xmjq').css('display','none');
            $('#xmsq').css('display','none');
            /*********** 样式部分控制End ***********/
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}


//根据条件查询结清的项目
function doProductBuyjq(){

    /******************** 查询条件Start ********************/
    var SelectDateTypeJq = $("#SelectDateTypeJq option:selected").val(); //查询日期类型
    var SelectStartDateValueJq = $("#SelectStartDateValueJq").val(); //查询起日期
    var SelectEndDateValueJq = $("#SelectEndDateValueJq").val(); //查询止日期
    var SelectKeyWordJq = $("#SelectKeyWordJq option:selected").val(); //关键词选项
    var keyWordValueJq = $("#keyWordValueJq").val(); //关键词内容
    var typeIdSelect = $("#typeIdSelectjq").val(); //结清的项目类型
    /********************* 查询条件End *********************/

    var squareSTime = ''; //结清起日期
    var squareETime = ''; //结清止日期
    var buySTime = ''; //满标起日期
    var buyETime = ''; //满标止日期
    var title = ''; //题目
    var code = ''; //编码

    $("#hsPageStr").html(''); //回收项目分页
    $("#tbPageStr").html(''); //投标项目分页
    $("#sqPageStr").html(''); //申请项目分页
    var pageIndex = $("#pageIndex").val(); //当前数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }

    if(SelectDateTypeJq != '' && typeof(SelectDateTypeJq) != "undefined"){
        if(SelectDateTypeJq == '0'){ //结清日期
            squareSTime = SelectStartDateValueJq;
            squareETime = SelectEndDateValueJq;
        }else if(SelectDateTypeJq == '1'){ //购买日期
            buySTime = SelectStartDateValueJq;
            buyETime = SelectEndDateValueJq;
        }
    }

    if(SelectKeyWordJq != '' && typeof(SelectKeyWordJq) != "undefined"){
        if(SelectKeyWordJq == '0'){ //名称
            title = keyWordValueJq;
        }else if(SelectKeyWordJq == '1'){ //编码
            code = keyWordValueJq;
        }
    }

    var productInvestMes = {squareSDate:squareSTime,squareEDate:squareETime,buyStartTime:buySTime,
                            buyEndTime:buyETime,title:title,productType:typeIdSelect,code:code,
                            pageIndex:pageIndex,pageSize:pageSize,menuUrl:'/front/invest/myInvest.jetx'};

    $.ajax({
        type:"post",
        url: "/invert/getSquareProduct",
        data:productInvestMes,
        dataType:"json",
        success:function(data){
            $("#tbodyJq").html(data.str); //列表部分
            $("#jqPageStr").html(data.str2); //分页部分

            $("#xmhsli").removeClass('active'); //回收
            $("#xmtbli").removeClass('active'); //投标
            $("#xmjqli").addClass('active'); //结清
            $("#xmsqli").removeClass('active'); //投标
            $('#xmhs').css('display','none');
            $('#xmtb').css('display','none');
            $('#xmjq').css('display','block');
            $('#xmsq').css('display','none');
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}

//查询已申请的项目
function doProductBuysq(){

    /*************** 查询条件Start ***************/
    var applyStartTime = $("#applyStartTime").val(); //申请起日期
    var applyEndTime = $("#applyEndTime").val(); //申请止日期
    var selectApply = $("#selectApply option:selected").val(); //关键词选项
    var applyContent = $("#applyContent").val(); //关键词内容
    /**************** 查询条件End ****************/

    var title = ''; //名称
    var code = ''; //编码

    if(selectApply != '' && typeof (selectApply) != 'undefined'){
        if(selectApply == '0'){
            title = applyContent;
        }else if(selectApply == '1'){
            code = applyContent;
        }
    }

    $("#hsPageStr").html(''); //回收项目分页
    $("#tbPageStr").html(''); //投标项目分页
    $("#jqPageStr").html(''); //已结清项目分页
    var pageIndex = $("#pageIndex").val(); //当前数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }
    var productMes = {applyStartTime:applyStartTime,applyEndTime:applyEndTime,title:title,code:code,
                      pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        url:"/applyProduct/getApplyProduct",
        data:productMes,
        dataType:"json",
        success:function(data){
            $("#xmsqtbody").html(data.listStr); //列表部分
            $("#sqPageStr").html(data.pageStr); //分页部分
            $("#xmhsli").removeClass('active'); //回收
            $("#xmtbli").removeClass('active'); //投标
            $("#xmjqli").removeClass('active'); //结清
            $("#xmsqli").removeClass('active'); //申请
            $("#xmsqli").addClass('active');
            $('#xmhs').css('display','none');
            $('#xmtb').css('display','none');
            $('#xmjq').css('display','none');
            $('#xmsq').css('display','block');
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}

//获取已申请项目详情
function doQueryApplyProductDetails(productId){
    $.ajax({
        type:"post",
        url:"/applyProduct/queryApplyProductDetails",
        data:'productId='+productId,
        dataType:"json",
        success:function(data){
            $(".p_right_wordbox").html(data.htmlText);
        },
        err:function(data){
            console.log(data);
        }
    });
}


//根据条件获取已申请项目详情中还款列表
function doQueryApplyProductDetailsBySome(productId){
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }
    $.ajax({
        type:"post",
        url:"/applyProduct/doQueryApplyProductDetailsBySome",
        data:'productId='+productId+"&pageIndex="+pageIndex+"&pageSize="+pageSize,
        dataType:"json",
        success:function(data){
            $("#tbodyPageStr").html(data.str); //列表部分拼接
            $("#ulPageStr").html(data.pageStr); //分页部分拼接
        },
        err:function(data){
            console.log(data);
        }
    });
}


//查看投资项目详情
function doQueryInvestProductDetails(id,productId,contractType){
    $.ajax({
        type:"post",
        url:"/invert/queryInvestProductDetails",
        data:'investId='+id+'&productId='+productId+'&contractType='+contractType,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
                if(contractType == ''){
                    $("#contracta").html(''); //合同部分隐藏
                    $("#contractb").html(''); //合同部分隐藏
                }
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//根据条件查询投资项目详情
function doQueryInvestProductDetailsByPage(id){
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }

    $.ajax({
        type:"post",
        url:"/invert/doQueryInvestProductDetailsByPage",
        data:'investId='+id+"&pageIndex="+pageIndex+"&pageSize="+pageSize,
        dataType:"json",
        success:function(data){
            $("#tbodyProductReceiveInfo").html(data.str); //列表部分拼接
            $("#ulPageStr").html(data.pageStr); //分页部分拼接
        },
        err:function(data){
            console.log(data);
        }
    });
}



//打开项目合同界面
function openProductConTract(investId,productId){
    var url = ''; //后台跳转地址
    var contractType = $("#contractType").val(); //合同类型(invest-投资、attorn-债权)
    if(contractType != '' && typeof(contractType) != 'undefined'){
        if(contractType == 'invest'){ //投资部分合同
            url = '/conTract/openProductContract?investId='+investId+'&productId='+productId;
        }
    }
    window.open(url);
}
