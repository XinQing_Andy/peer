<!-- 我要投资 -->

//我要投资(上面按钮)
function prepareInvest(typeId,typeName){
    //产品部分
    var productMes = {productTypeIdSelectPj:typeId,typeName:typeName,onlineSelect:'0',menuUrl:'front/invest/invest.jetx'};
    $.ajax({
        type:"post",
        url: "/invert/queryInvestByPage",
        data:productMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $('#accountDiv').html(data.htmlText);

                /******************** 产品类型部分Start ********************/
                if(typeId != '' && typeof(typeId) != "undefined"){
                    $("#productType0").removeClass('one_input');
                    $("#productType0").addClass("two_input");
                    $("#productType"+typeId).removeClass('two_input');
                    $("#productType"+typeId).addClass("one_input");
                }
                $("#productTypeIdSelectPj").val(typeId); //产品类型拼接
                /********************* 产品类型部分End *********************/
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


//根据条件查询
function invertBySome(){
    var pageIndex = $("#pageIndex").val(); //当前数
    var pageSize = $("#pageSize").val(); //每次显示数量*/
    var productTypeIdSelectPj = $("#productTypeIdSelectPj").val(); //已选择的产品类型拼接
    var loanrateSelectPj = $("#loanrateSelectPj").val(); //已选择的年利率拼接
    var loanDateSelectPj = $("#loanDateSelectPj").val(); //已选择的期限拼接
    var repaymentSelectPj = $("#repaymentSelectPj").val(); //已选择的还款方式拼接
    var onlineSelect = $("#onlineSelect").val(); //标的状态(0-已发布、1-即将发布)

    /********** 用于判断是否多选Start **********/
    var checkMutli = document.getElementById('checkMutli'); //多选
    var selectMutli = '1'; //0-多选、1-单选
    if(checkMutli.checked){
        selectMutli = '0'
    }else{
        selectMutli = '1';
    }
    /*********** 用于判断是否多选End ***********/

    var productMes = {productTypeIdSelectPj:productTypeIdSelectPj,loanrateSelectPj:loanrateSelectPj,
                      loanDateSelectPj:loanDateSelectPj,repaymentSelectPj:repaymentSelectPj,onlineSelect:onlineSelect,
                      selectMutli:selectMutli,pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        url: "/invert/queryInvestBySome",
        data:productMes,
        dataType:"json",
        success:function(data){
            $("#tbodyProduct").html(data.info); //列表信息
            $("#ulPageStr").html(data.pageStr); //分页部分拼接
        },
        err:function(data){
            console.log(data);
        }
    });
}


//判断多选按钮是否被选中
function selectMutli(){
    var checkMutli = document.getElementById('checkMutli'); //多选
    var selectMutli = '1'; //0-多选、1-单选
    if(checkMutli.checked){ //多选按钮被选中
        selectMutli = '0'
    }else{ //多选按钮未被选中
        selectMutli = '1';
    }
    return selectMutli;
}


//控制项目类型选中部分
function checkProductType(id){
    var productTypeIdPj = $("#productTypeIdPj").val(); //所有产品类型拼接
    var productTypeIdSelectPj = ''; //被选择的产品类型拼接
    var arr = []; //用于存放项目类型

    if(productTypeIdPj != '' && typeof (productTypeIdPj) != "undefined"){ //所有产品类型拼接
        arr = productTypeIdPj.split(','); //产品类型
        if(selectMutli() == '0'){ //多选按钮被选中
            if(id == '0'){ //不限
                $("#productType0").removeClass('two_input');
                $("#productType0").addClass("one_input");
                for(var i=0;i<arr.length;i++){
                    $("#productType"+arr[i]).removeClass('one_input');
                    $("#productType"+arr[i]).addClass('two_input');
                }
                productTypeIdSelectPj = ''; //被选择的产品类型拼接
            }else{
                if($("#productType"+id).hasClass('one_input')){ //已被选中
                    $("#productType"+id).removeClass('one_input');
                    $("#productType"+id).addClass("two_input");
                }else{ //未被选中
                    $("#productType"+id).removeClass('two_input');
                    $("#productType"+id).addClass("one_input");
                    $("#productType0").removeClass('one_input');
                    $("#productType0").addClass("two_input");
                }
                /********** 已选择的产品类型循环进行拼接Start **********/
                for(var i=0;i<arr.length;i++){
                    if($("#productType"+arr[i]).hasClass('one_input')){
                        if(productTypeIdSelectPj == '' || typeof (productTypeIdSelectPj) == 'undefined'){
                            productTypeIdSelectPj = arr[i];
                        }else{
                            productTypeIdSelectPj = productTypeIdSelectPj + ',' + arr[i];
                        }
                    }
                }
                /*********** 已选择的产品类型循环进行拼接End ***********/
            }
        }else{ //单选
            if(id == '0'){ //不限
                $("#productType0").removeClass('two_input');
                $("#productType0").addClass("one_input");
                for(var i=0;i<arr.length;i++){
                    $("#productType"+arr[i]).removeClass('one_input');
                    $("#productType"+arr[i]).addClass('two_input');
                }
                productTypeIdSelectPj = ''; //被选择的产品类型拼接
            }else{
                if($("#productType"+id).hasClass('one_input')){ //已被选中
                    $("#productType"+id).removeClass('one_input');
                    $("#productType"+id).addClass("two_input");
                    productTypeIdSelectPj = '';
                }else{ //未被选中
                    productTypeIdSelectPj = id; //已选择的产品类型
                    $("#productType"+id).removeClass('two_input');
                    $("#productType"+id).addClass("one_input");
                    $("#productType0").removeClass('one_input');
                    $("#productType0").addClass("two_input");

                    /********** 产品类型部分循环Start **********/
                    for(var i=0;i<arr.length;i++){
                        if(arr[i] != id){
                            $("#productType"+arr[i]).removeClass('one_input');
                            $("#productType"+arr[i]).addClass("two_input");
                        }
                    }
                    /********** 产品类型部分循环End **********/
                }
            }
        }
    }
    if(productTypeIdSelectPj == ''){
        $("#productType0").removeClass('two_input');
        $("#productType0").addClass("one_input");
    }
    $("#productTypeIdSelectPj").val(productTypeIdSelectPj);
    invertBySome(); //查询
}


//控制年利率选中部分
function checkloanrate(id){
    var loanratePj = $("#loanratePj").val(); //所有年利率拼接
    var loanrateSelectPj = ''; //被选中的年利率拼接
    var arr = []; //用于存放年利率
    if(loanratePj != '' && typeof (loanratePj) != "undefined"){
        arr = loanratePj.split(','); //年利率
        if(selectMutli() == '0') { //多选按钮被选中
            if(id == '0'){ //不限
                $("#loanrate0").removeClass('two_input');
                $("#loanrate0").addClass("one_input");
                for(var i=0;i<arr.length;i++){
                    $("#loanrate"+arr[i]).removeClass('one_input');
                    $("#loanrate"+arr[i]).addClass('two_input');
                }
                loanrateSelectPj = ''; //被选择的年利率拼接
            }else{
                if($("#loanrate"+id).hasClass('one_input')){ //已被选中
                    $("#loanrate"+id).removeClass('one_input');
                    $("#loanrate"+id).addClass("two_input");
                }else{ //未被选中
                    $("#loanrate"+id).removeClass('two_input');
                    $("#loanrate"+id).addClass("one_input");
                    $("#loanrate0").removeClass('one_input');
                    $("#loanrate0").addClass("two_input");
                }
                /********** 已选择的年利率循环进行拼接Start **********/
                for(var i=0;i<arr.length;i++){
                    if($("#loanrate"+arr[i]).hasClass('one_input')){
                        if(loanrateSelectPj == '' || typeof (loanrateSelectPj) == 'undefined'){
                            loanrateSelectPj = arr[i];
                        }else{
                            loanrateSelectPj = loanrateSelectPj + ',' + arr[i];
                        }
                    }
                }
                /*********** 已选择的年利率循环进行拼接End ***********/
            }
        }else{ //单选按钮被选中
            if(id == '0'){ //不限
                $("#loanrate0").removeClass('two_input');
                $("#loanrate0").addClass("one_input");
                for(var i=0;i<arr.length;i++){
                    $("#loanrate"+arr[i]).removeClass('one_input');
                    $("#loanrate"+arr[i]).addClass('two_input');
                }
                loanrateSelectPj = ''; //被选择的产品类型拼接
            }else{
                if($("#loanrate"+id).hasClass('one_input')){ //已被选中
                    $("#loanrate"+id).removeClass('one_input');
                    $("#loanrate"+id).addClass("two_input");
                    loanrateSelectPj = '';
                }else{ //未被选中
                    loanrateSelectPj = id;
                    $("#loanrate"+id).removeClass('two_input');
                    $("#loanrate"+id).addClass("one_input");
                    $("#loanrate0").removeClass('one_input');
                    $("#loanrate0").addClass("two_input");
                    for(var i=0;i<arr.length;i++){
                        if(arr[i] != id){
                            $("#loanrate"+arr[i]).removeClass('one_input');
                            $("#loanrate"+arr[i]).addClass("two_input");
                        }
                    }
                }
            }
        }
    }
    if(loanrateSelectPj == ''){
        $("#loanrate0").removeClass('two_input');
        $("#loanrate0").addClass("one_input");
    }
    $("#loanrateSelectPj").val(loanrateSelectPj);
    invertBySome(); //查询
}

//控制期限选中部分
function checkloanDate(id){
    var loanDatePj = $("#loanDatePj").val(); //所有期限拼接
    var loanDateSelectPj = ''; //被选中的期限拼接
    var arr = []; //用于存放期限
    if(loanDatePj != '' && typeof (loanDatePj) != "undefined"){
        arr = loanDatePj.split(','); //期限
        if(selectMutli() == '0') { //多选按钮被选中
            if(id == '0'){ //不限
                $("#loanDate0").removeClass('two_input');
                $("#loanDate0").addClass("one_input");
                for(var i=0;i<arr.length;i++){
                    $("#loanDate"+arr[i]).removeClass('one_input');
                    $("#loanDate"+arr[i]).addClass('two_input');
                }
                loanDateSelectPj = ''; //被选择的期限拼接
            }else{
                if($("#loanDate"+id).hasClass('one_input')){ //已被选中
                    $("#loanDate"+id).removeClass('one_input');
                    $("#loanDate"+id).addClass("two_input");
                }else{ //未被选中
                    $("#loanDate"+id).removeClass('two_input');
                    $("#loanDate"+id).addClass("one_input");
                    $("#loanDate0").removeClass('one_input');
                    $("#loanDate0").addClass("two_input");
                }
                /********** 已选择的期限循环进行拼接Start **********/
                for(var i=0;i<arr.length;i++){
                    if($("#loanDate"+arr[i]).hasClass('one_input')){
                        if(loanDateSelectPj == '' || typeof (loanDateSelectPj) == 'undefined'){
                            loanDateSelectPj = arr[i];
                        }else{
                            loanDateSelectPj = loanDateSelectPj + ',' + arr[i];
                        }
                    }
                }
                /*********** 已选择的期限循环进行拼接End ***********/
            }
        }else{ //单选按钮被选中
            if(id == '0'){ //不限
                $("#loanDate0").removeClass('two_input');
                $("#loanDate0").addClass("one_input");
                for(var i=0;i<arr.length;i++){
                    $("#loanDate"+arr[i]).removeClass('one_input');
                    $("#loanDate"+arr[i]).addClass('two_input');
                }
                loanDateSelectPj = ''; //被选择的产品类型拼接
            }else{
                if($("#loanDate"+id).hasClass('one_input')){ //已被选中
                    $("#loanDate"+id).removeClass('one_input');
                    $("#loanDate"+id).addClass("two_input");
                    loanDateSelectPj = '';
                }else{ //未被选中
                    loanDateSelectPj = id;
                    $("#loanDate"+id).removeClass('two_input');
                    $("#loanDate"+id).addClass("one_input");
                    $("#loanDate0").removeClass('one_input');
                    $("#loanDate0").addClass("two_input");
                    for(var i=0;i<arr.length;i++){
                        if(arr[i] != id){
                            $("#loanDate"+arr[i]).removeClass('one_input');
                            $("#loanDate"+arr[i]).addClass("two_input");
                        }
                    }
                }
            }
        }
    }
    if(loanDateSelectPj == ''){
        $("#loanDate0").removeClass('two_input');
        $("#loanDate0").addClass("one_input");
    }
    $("#loanDateSelectPj").val(loanDateSelectPj);
    invertBySome(); //查询
}

//控制还款方式
function checkrepayment(id){
    var repaymentPj = $("#repaymentPj").val(); //所有还款方式拼接
    var repaymentSelectPj = ''; //被选中的还款方式拼接
    var arr = []; //用于存放还款方式
    if(repaymentPj != '' && typeof (repaymentPj) != "undefined"){
        arr = repaymentPj.split(','); //还款方式
        if(selectMutli() == '0') { //多选按钮被选中
            if(id == '0'){ //不限
                $("#repayment0").removeClass('two_input');
                $("#repayment0").addClass("one_input");
                for(var i=0;i<arr.length;i++){
                    $("#repayment"+arr[i]).removeClass('one_input');
                    $("#repayment"+arr[i]).addClass('two_input');
                }
                repaymentSelectPj = ''; //被选择的还款方式拼接
            }else{
                if($("#repayment"+id).hasClass('one_input')){ //已被选中
                    $("#repayment"+id).removeClass('one_input');
                    $("#repayment"+id).addClass("two_input");
                }else{ //未被选中
                    $("#repayment"+id).removeClass('two_input');
                    $("#repayment"+id).addClass("one_input");
                    $("#repayment0").removeClass('one_input');
                    $("#repayment0").addClass("two_input");
                }
                /********** 已选择的还款方式循环进行拼接Start **********/
                for(var i=0;i<arr.length;i++){
                    if($("#repayment"+arr[i]).hasClass('one_input')){
                        if(repaymentSelectPj == '' || typeof (repaymentSelectPj) == 'undefined'){
                            repaymentSelectPj = arr[i];
                        }else{
                            repaymentSelectPj = repaymentSelectPj + ',' + arr[i];
                        }
                    }
                }
                /*********** 已选择的还款方式循环进行拼接End ***********/
            }
        }else{ //单选按钮被选中
            if(id == '0'){ //不限
                $("#repayment0").removeClass('two_input');
                $("#repayment0").addClass("one_input");
                for(var i=0;i<arr.length;i++){
                    $("#repayment"+arr[i]).removeClass('one_input');
                    $("#repayment"+arr[i]).addClass('two_input');
                }
                repaymentSelectPj = ''; //被选择的还款方式拼接
            }else{
                if($("#repayment"+id).hasClass('one_input')){ //已被选中
                    $("#repayment"+id).removeClass('one_input');
                    $("#repayment"+id).addClass("two_input");
                    repaymentSelectPj = '';
                }else{ //未被选中
                    repaymentSelectPj = id;
                    $("#repayment"+id).removeClass('two_input');
                    $("#repayment"+id).addClass("one_input");
                    $("#repayment0").removeClass('one_input');
                    $("#repayment0").addClass("two_input");
                    for(var i=0;i<arr.length;i++){
                        if(arr[i] != id){
                            $("#repayment"+arr[i]).removeClass('one_input');
                            $("#repayment"+arr[i]).addClass("two_input");
                        }
                    }
                }
            }
        }
    }
    if(repaymentSelectPj == ''){
        $("#repayment0").removeClass('two_input');
        $("#repayment0").addClass("one_input");
    }
    $("#repaymentSelectPj").val(repaymentSelectPj);
    invertBySome(); //查询
}

//检查投标状态
function checkOnline(id){
    if(id == '0') { //已发布
        $("#online0").removeClass('two_input');
        $("#online0").addClass("one_input");
        $("#online1").removeClass('one_input');
        $("#online1").addClass("two_input");
    }else { //即将发布
        $("#online1").removeClass('two_input');
        $("#online1").addClass("one_input");
        $("#online0").removeClass('one_input');
        $("#online0").addClass("two_input");
    }
    $("#onlineSelect").val(id);
    invertBySome(); //查询
}

//多选按钮
function checkSelect(){
    if(selectMutli() == '1'){ //单选
        var arr = []; //用于存放项目类型
        var brr = []; //用于存放年利率
        var crr = []; //用于存放期限
        var drr = []; //用于存放还款方式

        /********** 产品类型部分Start **********/
        $("#productType0").removeClass('two_input');
        $("#productType0").addClass("one_input");
        var productTypeIdPj = $("#productTypeIdPj").val(); //所有产品类型拼接
        var productTypeIdSelectPj = ''; //已选择的产品类型
        arr = productTypeIdPj.split(','); //项目类型
        for(var i=0;i<arr.length;i++){
            $("#productType"+arr[i]).removeClass('one_input');
            $("#productType"+arr[i]).addClass('two_input');
        }
        productTypeIdSelectPj = '';
        $("#productTypeIdSelectPj").val(productTypeIdSelectPj);
        /*********** 产品类型部分End ***********/

        /********** 年利率部分Start **********/
        $("#loanrate0").removeClass('two_input');
        $("#loanrate0").addClass("one_input");
        var loanratePj = $("#loanratePj").val(); //所有年利率拼接
        var loanrateSelectPj = ''; //已选择的年利率
        brr = loanratePj.split(','); //年利率
        for(var i=0;i<brr.length;i++){
            $("#loanrate"+brr[i]).removeClass('one_input');
            $("#loanrate"+brr[i]).addClass('two_input');
        }
        loanrateSelectPj = '';
        $("#loanrateSelectPj").val(loanrateSelectPj);
        /*********** 年利率部分End ***********/

        /********** 年利率部分Start **********/
        $("#loanDate0").removeClass('two_input');
        $("#loanDate0").addClass("one_input");
        var loanDatePj = $("#loanDatePj").val(); //所有期限拼接
        var loanDateSelectPj = ''; //已选择的期限
        crr = loanDatePj.split(','); //期限
        for(var i=0;i<crr.length;i++){
            $("#loanDate"+crr[i]).removeClass('one_input');
            $("#loanDate"+crr[i]).addClass('two_input');
        }
        loanDateSelectPj = '';
        $("#loanDateSelectPj").val(loanDateSelectPj);
        /*********** 年利率部分End ***********/

        /********** 还款方式部分Start **********/
        $("#repayment0").removeClass('two_input');
        $("#repayment0").addClass("one_input");
        var repaymentPj = $("#repaymentPj").val(); //所有还款方式拼接
        var repaymentSelectPj = ''; //已选择的还款方式
        drr = repaymentPj.split(','); //还款方式
        for(var i=0;i<drr.length;i++){
            $("#repayment"+crr[i]).removeClass('one_input');
            $("#repayment"+crr[i]).addClass('two_input');
        }
        repaymentSelectPj = '';
        $("#repaymentSelectPj").val(repaymentSelectPj);
        /*********** 还款方式部分End ***********/
    }
    invertBySome();
}


//我要投标信息部分
function queryProductInvestById(id){
    var productMes = {id:id,menuUrl:'front/invest/invest_add.jetx'};

    $.ajax({
        type:"post",
        url: "/invert/queryInvestProductInfo",
        data:productMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $('#accountDiv').html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


//新增投资购买
function saveInvest(){
    var productId = $("#productId").val(); //产品id
    var price = $("#price").val(); //充值金额
    var traderpassword = $("#traderpassword").val(); //交易密码
    var captcha = $("#captcha").val(); //验证码
    var wbiddingFee = $("#wbiddingFee").val(); //未投资金额

    if(parseFloat(price) > parseFloat(wbiddingFee)){
        alert('投资金额大于项目可投金额，请核对后重新输入！');
        return;
    }

    var productInvestMes = {productId:productId,traderpassword:traderpassword,price:price,captcha:captcha};

    $.ajax({
        type:"post",
        url: "/invert/addInvest",
        data:productInvestMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert('投资成功！');
                queryProductInvestById(productId);
            }else{
                alert(data.info);
                queryProductInvestById(productId);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//标签控制显示(1-借款信息、2-出借记录、3-还款列表)
function doConShow(flag){
    if(flag == '1'){ //借款信息
        /**********样式部分Start**********/
        $("#borrowInfo").removeClass('det');
        $("#lendRecord").removeClass('det');
        $("#repaymentList").removeClass('det');
        $("#borrowInfo").addClass('det');
        /**********样式部分End**********/
        $("#borrowInfoDiv").css('display','block');
        $("#lendRecordDiv").css('display','none');
        $("#repaymentListDiv").css('display','none');
    }else if(flag == '2'){ //投资记录
        getInvestRecord();
    }else if(flag == '3'){
        getRepayList();
    }
}


//自动填入
function autoWrite(){
    var wbiddingFee = $("#wbiddingFee").val(); //未投资金额
    $("#price").val(wbiddingFee);
}


//投资记录
function getInvestRecord(){
    var productId = $("#productId").val(); //产品id
    var pageIndex = $("#pageIndexInvest").val(); //起始数
    var pageSize = $("#pageSizeInvest").val(); //每页显示数量
    var productInvestMes = {productId:productId,pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        url: "/invert/queryInvestRecord",
        data:productInvestMes,
        dataType:"json",
        success:function(data){
            $('#tbodyInvest').html(data.str); //投资记录列表部分
            $('#pageRepayStr').html(data.pageStr); //还款列表分页部分
            $("#pageInvestStr").html(data.str2); //投资记录分页部分
            $('#spanInvestPrice').html(data.yInvestPrice); //已投标金额
            $('#spanWinvestPrice').html(data.wInvestPrice); //未投标金额

            /********** 标签控制Start **********/
            $("#borrowInfo").removeClass('det');
            $("#lendRecord").removeClass('det');
            $("#repaymentList").removeClass('det');
            $("#lendRecord").addClass('det');
            $("#borrowInfoDiv").css('display','none');
            $("#lendRecordDiv").css('display','block');
            $("#repaymentListDiv").css('display','none');
            /********** 标签控制End **********/
        },
        err:function(data){
            console.log(data);
        }
    });
}


//还款列表
function getRepayList(){
    var productId = $("#productId").val(); //产品id
    var pageIndex = $("#pageIndexRepay").val(); //起始数
    var pageSize = $("#pageSizeRepay").val(); //还款列表
    var productRepayMes = {productId:productId,pageIndex:pageIndex,pageSize:pageSize,menuUrl:'front/invest/invest_add.jetx'};

    $.ajax({
        type:"post",
        url: "/repayRecord/queryRepayByPage",
        data:productRepayMes,
        dataType:"json",
        success:function(data){
            $('#tbodyRepay').html(data.htmlText); //还款部分列表
            $("#pageInvestStr").html(''); //投资记录分页部分
            $('#pageRepayStr').html(data.pageStr); //还款列表分页部分
            $("#spanPriceSum").html(data.priceSum); //需还本息

            /********** 标签控制Start **********/
            $("#borrowInfo").removeClass('det');
            $("#lendRecord").removeClass('det');
            $("#repaymentList").removeClass('det');
            $("#repaymentList").addClass('det');
            $("#borrowInfoDiv").css('display','none');
            $("#lendRecordDiv").css('display','none');
            $("#repaymentListDiv").css('display','block');
            /********** 标签控制End **********/
        },
        err:function(data){
            console.log(data);
        }
    });
}



/********** by cuibin Start **********/

/**
 * 跳转到回款明细 by cuibin
 */
function openInvestReceive(){
    $.ajax({
        type:"post",
        url:"/invert/openInvestReceive",
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
            }
        },
        err:function(data){

        }
    });
}


//冻结金额
function freeze(){
    var productId = $("#productId").val(); //产品id
    var operationType = $("#operationType").val(); //操作类型
    var merchantID = $("#merchantID").val(); //商户存管交易账号
    var merBillNo = $("#merBillNo").val(); //商户订单号
    var projectNo = $("#projectNo").val(); //项目ID号
    var bizType = $("#bizType").val(); //业务类型
    var regType = $("#regType").val(); //登记方式(1-手动、2-自动)
    var contractNo = $("#contractNo").val(); //合同号
    var authNo = $("#authNo").val(); //授权号
    var merFee = $("#merFee").val(); //平台手续费
    var freezeMerType = $("#freezeMerType").val(); //冻结方类型(1-用户、2-商户)
    var ipsAcctNo = $("#ipsAcctNo").val(); //冻结账号
    var otherIpsAcctNo = $("#otherIpsAcctNo").val(); //它方账号
    var webUrl = $("#webUrl").val(); //页面返回地址
    var s2SUrl = $("#s2SUrl").val(); //后台通知地址
    var trdAmt = $("#price").val(); //投资价格
    var captcha = $("#captcha").val(); //验证码
    var balance = $("#balance").val(); //余额
    var wbiddingFee = $("#wbiddingFee").val(); //项目可投金额

    if(trdAmt == '' || typeof (trdAmt) == 'undefined'){
        alert('请填写投资金额！');
        return;
    }

    if(parseFloat(balance) < parseFloat(trdAmt)){
        alert('可用余额不足，请先进行充值操作！');
        return;
    }

    if(parseFloat(wbiddingFee) < parseFloat(trdAmt)){
        alert('投资金额应小于项目可投金额！');
        return;
    }

    if(captcha == '' || typeof (captcha) == 'undefined'){
        alert('请填写验证码！');
        return;
    }

    var huanXunFreezeMes = {operationType:operationType,merchantID:merchantID,merBillNo:merBillNo,projectNo:projectNo,
                            bizType:bizType,regType:regType,contractNo:contractNo,authNo:authNo,merFee:merFee,
                            freezeMerType:freezeMerType,ipsAcctNo:ipsAcctNo,otherIpsAcctNo:otherIpsAcctNo,webUrl:webUrl,
                            s2SUrl:s2SUrl,productId:productId,trdAmt:trdAmt,captcha:captcha};

    $.ajax({
        type:"post",
        url:"/huanxun/freezehx",
        data:huanXunFreezeMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $("#sign").val(data.sign); //签名
                $("#request").val(data.request); //请求信息
                $("#freezehx").submit();
            }else{
                alert(data.info);
            }
        },
        err:function(data){
            alert('投资失败！');
        }
    });
}


/**
 * 回款信息统计(重做 by zhangerxin)
 */
function receiveTjInfo(){
    var productReceiveMes = {menuUrl:'/front/invest/invest_receive.jetx'}
    $.ajax({
        type:"post",
        url:"/invert/receiveTjInfo",
        dataType:"json",
        data:productReceiveMes,
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText); //控制右侧内容显示
            } else {
                alert('查询失败');
            }
        },
        err: function (data) {
            alert('查询失败');
        }
    });
}


/**
 * 回款明细中项目选择
 */
function investReceiveXmselect(index) {
    if (index == '1') { //待回收统计
        $("#yReceivePage").html(''); //已回收明细分页部分
        $("#dReceivePage").html(''); //待回收明细分页部分
        $('#oneDiv').css('display','block'); //待回收统计
        $('#threeDiv').css('display','none'); //待回收明细
        $('#twoDiv').css('display','none'); //已回收明细
        $('#oneli').attr('class','active');
        $('#threeli').attr('class', '');
        $('#twoli').attr('class', '');
        receiveTjInfo();
    } else if (index == '2') { //待回收明细
        $("#yReceivePage").html(''); //已回收明细分页部分
        $("#oneDiv").css('display','none');
        $("#twoDiv").css('display','block');
        $("#threeDiv").css('display','none');
        $("#oneli").removeClass('active');
        $("#twoli").removeClass('active');
        $("#twoli").addClass('active');
        $("#threeli").removeClass('active');
        querydReceiveList();
    } else if (index == '3') {  //已回收明细
        $("#dReceivePage").html(''); //待回收明细分页部分
        $("#oneDiv").css('display','none');
        $("#twoDiv").css('display','none');
        $("#threeDiv").css('display','block');
        $("#oneli").removeClass('active');
        $("#twoli").removeClass('active');
        $("#threeli").removeClass('active');
        $("#threeli").addClass('active');
        queryyReceiveList();
    }
}



/**
 *  按条件查询 待回收明细
 */
function querydReceiveList(){
    var dReceiveStartTime = $("#dReceiveStartTime").val(); //待收起日期
    var dReceiveEndTime = $("#dReceiveEndTime").val(); //待收止日期
    var dreceiveTitle = $("#dreceiveCode").val(); //编码
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }
    var productReceiveMes = {startTime:dReceiveStartTime,endTime:dReceiveEndTime,title:dreceiveTitle,isRecycle:'0',
                              pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        url:"/invert/querydReceiveList",
        data:productReceiveMes,
        dataType:"json",
        success:function(data){
            $("#tbodydReceive").html(data.str); //待收列表拼接
            $("#dReceivePage").html(data.str2); //分页部分拼接
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}


/**
 *  按条件查询 已回收明细
 */
function queryyReceiveList(){
    var yReceiveStartTime = $("#yReceiveStartTime").val(); //已收起日期
    var yReceiveEndTime = $("#yReceiveEndTime").val(); //已收止日期
    var yreceiveTitle = $("#yreceiveTitle").val(); //编码
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }

    var productReceiveMes = {startTime:yReceiveStartTime,endTime:yReceiveEndTime,title:yreceiveTitle,isRecycle:'1',
                              pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        url:"/invert/queryyReceiveList",
        data:productReceiveMes,
        dataType:"json",
        success:function(data){
            $("#tbodyyReceive").html(data.str); //待收列表拼接
            $("#yReceivePage").html(data.str2); //分页部分拼接
        },
        err:function(data){
            alert('查询失败');
        }
    });
}

