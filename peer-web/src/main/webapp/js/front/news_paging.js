/**
 * Created by Cuibin on 2016/7/12.
 */

/********************   财富咨询 分页部分    ********************/


/**
 * 上一页
 */
function newsLastPage() {
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo <= 1) {
        $("#pageNo").html(1);
        return;
    } else {
        $("#pageNo").html(pageNo - 1);
    }

    queryforntNewsByType();
}
/**
 * 下一页
 */
function newsNextPage() {
    var totalPage = $("#totalPage").val();
    totalPage = parseInt(totalPage);
    if (totalPage < 2) {
        return;
    }
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo >= totalPage) {
        $("#pageNo").html(totalPage);
    } else {
        $("#pageNo").html(pageNo + 1);
    }

    queryforntNewsByType();
}
/**
 * 第一页
 */
function newsFirstPage() {
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo == 1) {
        return;
    }
    $("#pageNo").html(1);
    queryforntNewsByType();
}

/**
 * 最后一页
 */
function newsEndPage() {
    var totalPage = $("#totalPage").val();
    totalPage = parseInt(totalPage);
    if (totalPage < 2) {
        return;
    }
    $("#pageNo").html($("#totalPage").val());
    queryforntNewsByType();
}

/**
 * 跳转到指定页数
 */
function newsGoPage() {
    var totalPage = $("#totalPage").val();
    if (totalPage < 2) {
        return;
    }
    var inputPage = $("#inputPage").val();
    if (inputPage > totalPage || inputPage <= 0) {
        return;
    }

    $("#pageNo").html(inputPage);
    queryforntNewsByType();
}


/**
 * 查询财富资讯 带分页
 */
function queryforntNewsByType() {
    var pageNo = $("#pageNo").html(); //当前页
    pageNo = parseInt(pageNo);
    var pageSize = $("#pageSize").val();//每页多少个
    pageSize = parseInt(pageSize);
    var pageIndex = 1 + (pageNo - 1) * pageSize; //从第几个开始
    var kindId = $("#kindId").val(); //类别id
    var firstType = $("#firstType").val();

    var temp = {
        pageIndex: pageIndex,
        pageSize: pageSize,
        kindId: kindId,
        firstType: firstType
    };

    $.ajax({
        type: "post",
        url: "/treasure/queryNewsByPage",
        data: temp,
        dataType: "json",
        success: function (data) {
            if (data.result == "success") {
                $(".p_right_wordbox").html(data.htmlText);
                $("#pageNo").html(pageNo);
            }
        },
        err: function (data) {

        }
    });
}


/********************   财富咨询 分页部分    ********************/
