/**
  * 我的账户
  * by zhangerxin
  */

//跳转到我的投资
function doGoMyInvest(){
    var productMes = {menuUrl:'front/invest/myInvest.jetx'};
    $.ajax({
        type:"post",
        url:"/invert/openMyInvest",
        data:productMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}


//前一年
function AccountLastYearScoll(){
    var year = $("#myAccountYear").val(); //年份
    var month = $("#myAccountMonth").val(); //月份
    if(year != ''){
        year = parseInt(year) - 1;
    }

    $.ajax({
        type:"post",
        url:"/front/getRlInformation",
        data:"year="+year+"&month="+month,
        dataType:"json",
        success:function(data){
            $("#accountTbody").html(data.rlStr); //日历拼接
            $("#myAccountYearSpan").html(data.year); //日历年份
            $("#myAccountYear").val(data.year); //日历年份
            $("#spanYear").html(data.year); //日历年份
            $("#spanReceiveCount").html(data.receiveCount); //应回款数量
            $("#spanReceivePrice").html(data.receivePrice+'元'); //应回收金额
            $("#spanRepayCount").html(data.repayCount); //应还款数量
            $("#spanRepayPrice").html(data.repayPrice+'元'); //应还款金额
            $("#spanYReceiveCount").html(data.ysCount); //已回款数量
            $("#spanYReceivePrice").html(data.ysPrice); //已回款金额
            $("#spanYRepayCount").html(data.yhCount); //已还款数量
            $("#spanYRepayPrice").html(data.yhPrice); //已还款金额
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}

//后一年
function AccountNextYearScoll(){
    var year = $("#myAccountYear").val(); //年份
    var month = $("#myAccountMonth").val(); //月份
    if(year != ''){
        year = parseInt(year) + 1;
    }

    $.ajax({
        type:"post",
        url:"/front/getRlInformation",
        data:"year="+year+"&month="+month,
        dataType:"json",
        success:function(data){
            $("#accountTbody").html(data.rlStr); //日历拼接
            $("#myAccountYearSpan").html(data.year); //日历年份
            $("#myAccountYear").val(data.year); //日历年份
            $("#spanYear").html(data.year); //日历年份
            $("#spanReceiveCount").html(data.receiveCount); //应回款数量
            $("#spanReceivePrice").html(data.receivePrice+'元'); //应回收金额
            $("#spanRepayCount").html(data.repayCount); //应还款数量
            $("#spanRepayPrice").html(data.repayPrice+'元'); //应还款金额
            $("#spanYReceiveCount").html(data.ysCount); //已回款数量
            $("#spanYReceivePrice").html(data.ysPrice); //已回款金额
            $("#spanYRepayCount").html(data.yhCount); //已还款数量
            $("#spanYRepayPrice").html(data.yhPrice); //已还款金额
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}

//上个月
function AccountLastMonthScoll(){
    var year = $("#myAccountYear").val(); //年份
    var month = $("#myAccountMonth").val(); //月份
    if(month != ''){
        if(month == '1'){
            month = '12';
        }else{
            month = parseInt(month) - 1;
        }
    }

    $.ajax({
        type:"post",
        url:"/front/getRlInformation",
        data:"year="+year+"&month="+month,
        dataType:"json",
        success:function(data){
            $("#accountTbody").html(data.rlStr); //日历拼接
            $("#myAccountMonthSpan").html(data.month); //日历月份
            $("#myAccountMonth").val(data.month); //日历月份
            $("#spanMonth").html(data.month); //日历年份
            $("#spanReceiveCount").html(data.receiveCount); //应回款数量
            $("#spanReceivePrice").html(data.receivePrice+'元'); //应回收金额
            $("#spanRepayCount").html(data.repayCount); //应还款数量
            $("#spanRepayPrice").html(data.repayPrice+'元'); //应还款金额
            $("#spanYReceiveCount").html(data.ysCount); //已回款数量
            $("#spanYReceivePrice").html(data.ysPrice); //已回款金额
            $("#spanYRepayCount").html(data.yhCount); //已还款数量
            $("#spanYRepayPrice").html(data.yhPrice); //已还款金额
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}

//下个月
function AccountNextMonthScoll(){
    var year = $("#myAccountYear").val(); //年份
    var month = $("#myAccountMonth").val(); //月份
    if(month != ''){
        if(month == '12'){
            month = '1';
        }else{
            month = parseInt(month) + 1;
        }
    }

    $.ajax({
        type:"post",
        url:"/front/getRlInformation",
        data:"year="+year+"&month="+month,
        dataType:"json",
        success:function(data){
            $("#accountTbody").html(data.rlStr); //日历拼接
            $("#myAccountMonthSpan").html(data.month); //日历月份
            $("#myAccountMonth").val(data.month); //日历月份
            $("#spanMonth").html(data.month); //日历年份
            $("#spanReceiveCount").html(data.receiveCount); //应回款数量
            $("#spanReceivePrice").html(data.receivePrice+'元'); //应回收金额
            $("#spanRepayCount").html(data.repayCount); //应还款数量
            $("#spanRepayPrice").html(data.repayPrice+'元'); //应还款金额
            $("#spanYReceiveCount").html(data.ysCount); //已回款数量
            $("#spanYReceivePrice").html(data.ysPrice); //已回款金额
            $("#spanYRepayCount").html(data.yhCount); //已还款数量
            $("#spanYRepayPrice").html(data.yhPrice); //已还款金额
        },
        err:function(data){
            alert('查询失败！');
        }
    });
}

/**
 * 安全等级
 */
var lock = 0;
function accountSafeRank() {
    var i = $("#amount").val();

    if (i < 3) {
        $("#total_safe").val("33");
        $("#grade").html("低");
    } else if (i < 5) {
        $("#total_safe").val("66");
        $("#grade").html("中");
    } else {
        $("#total_safe").val("100");
        $("#grade").html("高");
    }
    if(lock == 0){
        accountRun();
    }
    lock++;
}
/**
 * 递归
 */
function accountRun() {
    var bar = document.getElementById("bar_account"); // 或得dom对象
    var total = document.getElementById("total_safe"); //隐藏域数值
    bar.style.width = parseInt(bar.style.width) + 1 + "%"; //给dom对象赋值
    total.innerHTML = bar.style.width; //更新隐藏域数值
    if (bar.style.width == $("#total_safe").val() + "%") {
        window.clearTimeout();
        lock = 0;
        return;
    }
    window.setTimeout("accountRun()", 10);
}