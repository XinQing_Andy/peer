/**
 * Created by Cuibin on 2016/8/4.
 */

/**
 * 获取自动签约信息
 */
function motionSign(){
    var memberId = $("#memberId").val(); //会员id
    $.ajax({
        type:"post",
        url:"/huanxun/motionSign?memberId="+memberId,
        dataType:"json",
        success:function(data){
            $("#operationType").val(data.operationType);
            $("#merchantID").val(data.merchantID);
            $("#request").val(data.request);
            $("#sign").val(data.sign);
            $("#myForm").submit();
        },
        err:function(){
            alert('签约加密失败！');
        }
    });
}