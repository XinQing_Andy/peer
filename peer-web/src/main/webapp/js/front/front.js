/**
 * Created by cuibin on 2016/5/9.
 */


/**
 * 跳转到我的账户
 * @param userId
 */
function goAccount(message) {
    if (message == 1) {
        $(".landing").slideDown(200);
        return;
    }
    location.href = '/front/goAccount';
}


/**
 * 跳转到我的账户  从注册成功页面
 * @param userId
 */
function goAccount2(userName, password) {
    var reg = /^\s+$/gi;
    if (userName == "" || reg.test(userName) || password == "" || reg.test(password)) {
        return;
    }
    var temp = {userName: userName, password: password};
    var temp2 = $.param(temp);
    $.ajax({
        type: "post",
        url: "/user/userLogin",
        data: temp2,
        dataType: "json",
        success: function (data) {
            if (data.result == 'success') {
                location.href = '/front/goAccount';
            }
        },
        err: function (data) {

        }
    });
}


//查询广告列表(前台)
function queryFrontAdvertList(typeId){
    var advertMes = {typeId:typeId,menuUrl:'/front/advert/advert_more.jetx'};
    $.ajax({
        type: "post",
        url: "/front/queryFrontAdvertList",
        data: advertMes,
        dataType: "json",
        success: function (data) {
            if (data.result == "success") {
                $("#mainDiv").html(data.htmlText);
            }
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}

//根据条件查询前台广告
function queryFrontAdvertBySome(typeId){
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    var advertMes = {typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,menuUrl:'/front/advert/advert_more.jetx'};

    $.ajax({
        type: "post",
        url: "/front/queryFrontAdvertBySome",
        data: advertMes,
        dataType: "json",
        success: function (data) {
            $("#ulStr").html(data.advertStr); //广告拼接
            $("#divPageStr").html(data.pageStr); //分页部分
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}

//搜索广告详情
function queryFrontAdvertDetails(advertId){
    var advertMes = {id:advertId,menuUrl:'/front/advert/advert_content.jetx'};
    $.ajax({
        type: "post",
        url: "/front/queryFrontAdvertDetails",
        data: advertMes,
        dataType: "json",
        success: function (data) {
            $("#mainDiv").html(data.htmlText);
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}


/**
 * 首页跳转查询更多产品列表
 */
function openProductMove(productType){
    var pageIndex = $("#pageIndex").val(); //当前数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == "undefined"){
        pageIndex = '';
    }
    if(typeof (pageSize) == "undefined"){
        pageSize = '';
    }
    var temp = {productType:productType,pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        url:"/front/openProductMove",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == "success"){
                $("#mainDiv").html(data.htmlText);
            }
        },
        err:function(data){
            alert('查询失败！')
        }
    });
}

/**
 * 首页跳转查询更多债权
 */
function openAttornMore(){
    var pageIndex = $("#pageIndex").val(); //当前数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == "undefined"){
        pageIndex = '';
    }
    if(typeof (pageSize) == "undefined"){
        pageSize = '';
    }
    var attornMes = {orderColumn:'CREATETIME',orderType:'DESC',pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        url:"/front/openAttornMore",
        data:attornMes,
        dataType:"json",
        success:function(data){
            $("#mainDiv").html(data.htmlText);
        },
        err:function(data){
            alert('查询失败！')
        }
    });
}

/**
 * 左标题栏跳转查询更多产品列表
 */
function openLeftProductMore(productType){
    var temp = {productType:productType};

    $.ajax({
        type:"post",
        url:"/front/openProductMove",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == "success"){
                $("#mainDiv").html(data.htmlText);
            }
        },
        err:function(data){
            alert('查询失败！')
        }
    });
}


//搜索财富资讯(重写by zhangerxin)
function queryWealth(newId){

    var weathInformationMes = {firstTypeId:newId,meneUrl:'/front/treasure/treasure.jetx'};

    $.ajax({
        type:"post",
        url:"/treasure/queryWeathByPage",
        data:weathInformationMes,
        dataType:"json",
        success: function (data) {
            if (data.result == "success") {
                $("#mainDiv").removeClass();
                $("#mainDiv").attr("class", "p_main");
                $("#mainDiv").attr("style", "margin-top:20px");
                $("#mainDiv").html(data.htmlText);
            }
        },
        err:function (data) {
            alert('查询失败！');
        }
    });
}


/**
 * 按条件搜索财富资讯(重写 by zhangerxin)
 */
function queryWealthBySome(firstTypeId,secondTypeId) {

    var pageIndex = $("#pageIndex").val(); //当前数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }
    var weathInformationMes = {firstTypeId:firstTypeId,secondTypeId:secondTypeId,pageIndex:pageIndex,pageSize:pageSize,
                                meneUrl:'/front/treasure/treasure.jetx'};
    var ulStr = '';

    $.ajax({
        type: "post",
        url: "/treasure/queryWeathBySome",
        data:weathInformationMes,
        dataType: "json",
        success: function (data) {
            ulStr = data.weathInformationStr
                  + '<div class=\"p_fanye\">'
                  + data.pageStr
                  + '</div>';
            $("#ulStr").html(ulStr);
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}

//搜索财富资讯详细信息
function queryWealthDetails(weathInformationId){
    var weathInformationMes = {id:weathInformationId,meneUrl:'/front/treasure/treasure_particulars.jetx'};

    $.ajax({
        type: "post",
        url: "/treasure/queryWealthDetails",
        data:weathInformationMes,
        dataType: "json",
        success: function (data) {
            $("#mainDiv").html(data.htmlText);
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}


/**
 * 跳转到更多理财风云榜
 */
function openMoreRanking() {
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }

    var financingMes = {menuUrl:'/front/ranking/more_ranking.jetx',pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        url:"/front/openMoreRanking",
        data:financingMes,
        success: function (data) {
            if (data.result == "success") {
                $("#mainDiv").html(data.htmlText);
            }
        }
    });
}



/**
 * 显示二维码
 */
$(function(){
    $("#f_yuan2").hover(function(){
        $("#ewm_tc").show();
    });
    $("#f_yuan2").mouseleave(function(){
        $("#ewm_tc").hide();
    });
});


//投资推荐项目,跳转到投资详情页
function investRecomment(){
    $.ajax({
        type: "post",
        url: "/front/openMoreRanking",
        success: function (data) {
            if (data.result == "success") {
                $("#mainDiv").html(data.htmlText);
            }
        }
    });
}
