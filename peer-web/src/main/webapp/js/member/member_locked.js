/**
 * Created by cuibin on 2016/6/14.
 */

/**
 *选择排序方式
 */
function memLockSelSort(id) {
    var temp;
    for (var i = 1; i <= 3; i++) {
        if (id != i) {
            $("#" + i).attr("class", "");
            temp = $("#" + i).html();
            temp = temp.replace("↓", "↑");
            $("#" + i).html(temp);
        }
    }
    var myclass = $("#" + id).attr("class");
    if (myclass == 'active') {
        var sort = $("#" + id).html();
        var message = sort.substring(sort.length - 1, sort.length);
        if (message == "↑") {
            sort = sort.replace("↑", "↓");
        } else {
            sort = sort.replace("↓", "↑");
        }
        $("#" + id).html(sort);
    }
    $("#" + id).attr("class", "active");
    queryMemlByCon();
}

/**
 * 解锁会员
 */
function deblockingMember(id,lockFlg){
    if(lockFlg == 0){
        if(!window.confirm('确认要解锁该用户吗')){
            return;
        }
        lockFlg = 1;
    }
    var temp = {id:id,lockFlg:lockFlg};
    $.ajax({
        type:"post",
        url:"/member/lockMember",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert(data.info);
                queryMemlByCon();
            } else {
                alert(data.info);
            }
        },
        err:function(data){

        }
    });
}



/*************** 分页部分Start ***************/
function getMemlBySome(advert) {
    if(!CheckTime()){
        return;
    }
    $.ajax({
        type: "post",
        url: "/member/queryAllLockMember",
        data: advert,
        dataType: "json",
        success: function (data) {
            if (data.result == 'success') {
                $('#memDiv').html(data.htmlText);
            } else {
                $('#memDiv').after(data.info);
            }
        },
        err: function (data) {
            console.log(data);
        }
    });
}

//首页
function memlStartPage() {
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var keyWord = $("#keyWord").val(); //题目
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        lockFlg: '0',
        isHome: '0'
    };
    getMemlBySome(advert);
}

//尾页
function memlEndPage() {
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if (parseInt(totalCount) % 10 == 0) {
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    } else {
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var keyWord = $("#keyWord").val(); //题目
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        lockFlg: '0',
        isHome: '0'
    };
    getMemlBySome(advert);
}

//上一页
function memlUp() {
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if (parseInt(currentPage) == 1) {
        pageIndex = "1";
        currentPage = "1";
    } else {
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var keyWord = $("#keyWord").val(); //题目
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        lockFlg: '0',
        isHome: '0'
    };
    getMemlBySome(advert);
}

//下一页
function memlDown() {
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var keyWord = $("#keyWord").val(); //题目

    if (parseInt(totalCount) % 10 == 0) {
        totalPage = parseInt(totalCount) / 10; //总页数
    } else {
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if (parseInt(currentPage) < parseInt(totalPage)) {
        currentPage = parseInt(currentPage) + 1; //当前页
    } else {
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        lockFlg: '0',
        isHome: '0'
    };
    getMemlBySome(advert);
}

//按条件查询用户列表
function queryMemlByCon() {

    if(!CheckTime()){
        return;
    }

    var sort = null;
    var isDesc = "0";
    for (var i = 1; i <= 3; i++) {
        var val = $("#" + i).attr("class");
        if (val == "active") {
            sort = $("#" + i).html();
            if (sort.length > 4) {
                var message = sort.substring(sort.length - 1, sort.length);
                if (message == "↓") {
                    isDesc = "1";
                }
                sort = sort.substring(0, sort.length - 1);
            }
        }
    }

    var keyWord = $("#keyWord").val();
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var temp = {keyWord: keyWord, startTime: startTime, endTime: endTime, lockFlg: '0', sort: sort, isHome: '0'};
    $.ajax({
        type: "post",
        url: "/member/queryAllLockMember",
        data: temp,
        dataType: "json",
        success: function (data) {
            $("#memDiv").empty();
            $('#memDiv').html(data.htmlText);
        },
        err: function (data) {

        }
    });
}
//跳转到指定页数
function doGoMemlPage(page) {
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var keyWord = $("#keyWord").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        lockFlg: '0',
        isHome: '0'

    };
    getMemlBySome(advert);
}
/*************** 分页部分End ***************/