/**
 * Created by Cuibin on 2016/4/20.
 */

/**
 *选择排序方式
 */
function selSort(id){

    var temp;
    for (var i = 1; i <= 5; i++) {
        if (id != i) {
            $("#" + i).attr("class", "");
            temp = $("#" + i).html();
            temp = temp.replace("↓", "↑");
            $("#" + i).html(temp);
        }
    }
    var myclass = $("#" + id).attr("class");
    if (myclass == 'active') {
        var sort = $("#" + id).html();
        var message = sort.substring(sort.length - 1, sort.length);
        if (message == "↑") {
            sort = sort.replace("↑", "↓");
        } else {
            sort = sort.replace("↓", "↑");
        }
        $("#" + id).html(sort);
    }
    $("#"+id).attr("class","active");
    queryMemByCon();
}
/**
 *验证时间
 */
function CheckTime(){
    var val = $("#startTime").val();
    var val2 = $("#endTime").val();
    var time1 = new Date(val).getTime();
    var time2 = new Date(val2).getTime();
    if(time1 > time2 || time1 == time2){
        alert("开始时间必须小于结束时间");
        return false;
    }
    return true;
}
/**
 * 锁定会员
 */
function lockMember(id,lockFlg){
    if(lockFlg == 0){
        if(!window.confirm('确认要解锁该用户吗')){
            return;
        }
        lockFlg = 1;
    }else if(lockFlg == 1){
        if(!window.confirm('确认要锁定该用户吗')){
            return;
        }
        lockFlg = 0;
    }
    var temp = {id:id,lockFlg:lockFlg};
    $.ajax({
        type:"post",
        url:"/member/lockMember",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert(data.info);
                queryMemByCon();
            } else {
                alert(data.info);
            }
        },
        err:function(data){

        }
    });
}
/**
 * 查看详情
 */
function lookParticulars(id) {
    var temp = {id: id};
    $.ajax({
        type: "post",
        url: "/member/queryOnlyMember",
        data: temp,
        dataType: "json",
        success: function (data) {
            if (data.result == 'success') {
                $("#username").html(data.username);
                $("#email").html(data.email);
                $("#realname").html(data.realname);
                if (data.sex == 0) {
                    $("#sex").html('男');
                } else {
                    $("#sex").html('女');
                }
                $("#idcard").html(data.idcard);
                $("#city").html(data.city);
                if (data.dregree == 0) {
                    $("#dregree").html('小学');
                } else if (data.dregree == 1) {
                    $("#dregree").html('初中');
                } else if (data.dregree == 2) {
                    $("#dregree").html('高中');
                } else if (data.dregree == 3) {
                    $("#dregree").html('大专');
                } else if (data.dregree == 4) {
                    $("#dregree").html('本科');
                } else if (data.dregree == 5) {
                    $("#dregree").html('硕士');
                } else if (data.dregree == 6) {
                    $("#dregree").html('博士');
                } else if (data.dregree == 7) {
                    $("#dregree").html('博士后');
                } else if (data.dregree == 8) {
                    $("#dregree").html('其他');
                }

                if (data.marryStatus == 0) {
                    $("#marryStatus").html('未婚');
                } else if (data.marryStatus == 1) {
                    $("#marryStatus").html('已婚');
                } else if (data.marryStatus == 2) {
                    $("#marryStatus").html('离婚');
                } else if (data.marryStatus == 3) {
                    $("#marryStatus").html('丧偶');
                }

                if (data.carFlg == 0) {
                    $("#carFlg").html('有车');
                } else if (data.carFlg == 1) {
                    $("#carFlg").html('无车');
                }

                if (data.houseCondition == 0) {
                    $("#houseCondition").html('有商品房(无贷款)');
                } else if (data.houseCondition == 1) {
                    $("#houseCondition").html('有商品房(有贷款)');
                } else if (data.houseCondition == 2) {
                    $("#houseCondition").html('有其它(非商品)房');
                } else if (data.houseCondition == 3) {
                    $("#houseCondition").html('与父母同住');
                } else if (data.houseCondition == 4) {
                    $("#houseCondition").html('租房');
                }
                $("#telephone").html(data.telephone);
                $("#myModal").attr("style", "display: block");
            } else {
                alert(data.info);
            }
        },
        err: function (data) {

        }
    });

}
/**
 *  关闭查看详情
 */
function closeLookParticulars() {
    $("#myModal").attr("style", "display: none");
    $("#email").html("");
    $("#realname").html("");
    $("#sex").html("");
    $("#idcard").html("");
    $("#city").html("");
    $("#dregree").html("");
    $("#marryStatus").html("");
    $("#carFlg").html("");
    $("#houseCondition").html("");
    $("#telephone").html("");
}
/**
 * 打开修改密码
 */
function openUpdatePassword(id) {
    $("#userId").val(id);
    $("#myModal3").attr("style", "display: block");

}
/**
 * 关闭修改密码
 */
function closeUpdatePassword() {
    $("#myModal3").attr("style", "display: none");
    $("#password").val("");
    $("#userId").val("");

}
/**
 * 修改密码
 */
function UpdatePassword() {
    var userId = $("#userId").val();
    var password = $("#password").val();
    var reg = /^\s+$/gi;
    if (password == "" || reg.test(password)) {
        alert("密码不能为空");
        return;
    }
    if (password.length < 6) {
        alert("密码长度不能小于6位");
        return;
    }
    if (password.length > 14) {
        return;
    }
    var temp = {
        id: userId,
        password: password
    };
    $.ajax({
        type:"post",
        url: "/member/updateMemberPassWord",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                closeUpdatePassword();
                alert(data.info);
            }else{
                alert(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

/*************** 会员 分页部分Start ***************/
function getMemBySome(advert){
    if(!CheckTime()){
        return;
    }
    $.ajax({
        type:"post",
        url: "/member/queryAllMemberBypage",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#memDiv').html(data.htmlText);
            }else{
                $('#memDiv').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function memStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var keyWord = $("#keyWord").val(); //题目
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        isHome: "0"
    };
    getMemBySome(advert);
}

//尾页
function memEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var keyWord = $("#keyWord").val(); //题目
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        isHome: "0"
    };
    getMemBySome(advert);
}

//上一页
function memUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var keyWord = $("#keyWord").val(); //题目
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        isHome: "0"
    };
    getMemBySome(advert);
}

//下一页
function memDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var keyWord = $("#keyWord").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        isHome: "0"
    };
    getMemBySome(advert);
}

//按条件查询用户列表
function queryMemByCon() {
    if(!CheckTime()){
        return;
    }
    var sort = null;
    var isDesc = "0";
    for (var i = 1; i <= 5; i++) {
        var val = $("#" + i).attr("class");
        if (val == "active") {
            sort = $("#" + i).html();
            if (sort.length > 4) {
                var message = sort.substring(sort.length - 1, sort.length);
                if (message == "↓") {
                    isDesc = "1";
                }
                sort = sort.substring(0, sort.length - 1);
            }
        }
    }

    var keyWord = $("#keyWord").val();
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var temp = {keyWord: keyWord, startTime: startTime, endTime: endTime, sort: sort, isDesc: isDesc, isHome: '0'};
    $.ajax({
        type: "post",
        url: "/member/queryAllMemberBypage",
        data: temp,
        dataType: "json",
        success: function (data) {
            $("#memDiv").empty();
            $('#memDiv').html(data.htmlText);
        },
        err: function (data) {

        }
    });
}
//跳转到指定页数
function doGoMemPage(page){
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var keyWord = $("#keyWord").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {
        keyWord: keyWord,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        startTime: startTime,
        endTime: endTime,
        isHome: "0"
    };
    getMemBySome(advert);
}
/*************** 会员 分页部分End ***************/

