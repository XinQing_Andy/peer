/**
  *平台收入
  *张尔鑫 2016-09-18
  */

//根据条件查询平台收入
function queryPlatIncomeBySome(pageIndex,pageSize,currentPage){
    var orderType = $("#orderType").val(); //排序类型
    var orderColumn = $("#orderColumn").val(); //排序字段
    var startTime = $("#startTime").val(); //开始时间
    var endTime = $("#endTime").val(); //截止时间
    var platIncome = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,orderType:orderType,orderColumn:orderColumn,
                       startTime:startTime,endTime:endTime};
    $.ajax({
        type: "post",
        url: "/platIncome/queryPlatIncomeTjBySome",
        data: platIncome,
        dataType: "json",
        success: function (data) {
            $("#tbodyplatIncome").html(data.platStatisticStr);
            $("#currentPage2").html(data.currentPage); //当前页
            $("#totalCount").val(data.totalCount); //总数量
        },
        err: function (data) {
            alert('查询失败！')
        }
    });
}

//判断日期排序类型
function changeTime(){
    $("#li1").removeClass('active');
    $("#li2").removeClass('active');
    $("#li2").addClass('active');
    var timeType = $("#li2").html(); //时间类型
    if(timeType != '' && typeof (timeType) != 'undefined'){
        if(timeType.indexOf('↑') >= 0){
            $("#li2").html('日期↓');
            $("#orderType").val('DESC'); //排序类型
            $("#orderColumn").val('DATATIME'); //排序字段
        }else if(timeType.indexOf('↓') >= 0){
            $("#li2").html('日期↑');
            $("#orderType").val('ASC'); //排序类型
            $("#orderColumn").val('DATATIME'); //排序字段
        }else{
            timeType = timeType + '↑';
            $("#li2").html(timeType);
            $("#orderType").val('ASC'); //排序类型
            $("#orderColumn").val('DATATIME'); //排序字段
        }
    }
    queryPlatIncomeBySome('1','10','1');
}

//默认排序
function normalSort(){
    $("#li1").removeClass('active');
    $("#li2").removeClass('active');
    $("#li1").addClass('active');
    $("#orderType").val('DESC'); //排序类型
    $("#orderColumn").val('DATATIME'); //排序字段
    $("#li2").html('日期');
    queryPlatIncomeBySome('1','10','1');
}


/*************** 分页部分Start ***************/
//首页
function platIncomeStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    queryPlatIncomeBySome(pageIndex,pageSize,currentPage);
}
//尾页
function platIncomeEndPage(){
    var totalCount = $("#totalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    queryPlatIncomeBySome(pageIndex,pageSize,currentPage);
}
//上一页
function platIncomeUp(){
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }
    var pageSize = "10"; //每页显示数量
    queryPlatIncomeBySome(pageIndex,pageSize,currentPage);
}
//下一页
function platIncomeDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#totalCount").val(); //总数量
    var pageSize = "10";

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }
    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    queryPlatIncomeBySome(pageIndex,pageSize,currentPage);
}
//跳转到指定页数
function doPlatIncomeGoPage(){
    var platIncomeNum = $('#platIncomeNum').val(); //跳转到第几页
    if(platIncomeNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(platIncomeNum)){
        alert('请输入合法的数字！');
        return;
    }
    var currentPage = platIncomeNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    queryPlatIncomeBySome(pageIndex,pageSize,currentPage);
}
/*********** 产品投资End ***********/
/**************** 分页部分End ****************/