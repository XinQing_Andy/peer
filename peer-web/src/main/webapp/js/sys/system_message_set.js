/**
 * Created by cuibin on 2016/5/30.
 */

function updateMessageSet(){

    var username = $("#username").val();
    var password = $("#password").val();
    var id = $("#updateMessageId").val();
    var flag = $("#flag").val();

    var reg = /^\s+$/gi;
    if(username == "" || reg.test(username)){
        alert("用户名不能为空");
        return;
    }
    if(password == "" || reg.test(password)){
        alert("密码不能为空");
        return;
    }
    if(id == "" || reg.test(id)){
        return;
    }

    var temp = {
        username:username,
        password:password,
        id:id,
        flag:flag
    };

    $.ajax({
        type: "post",
        url: "/messageSet/updateMessageSet",
        data: temp,
        dataType: "json",
        success: function (data) {
            if(data.result == 'success'){
                alert(data.info);
            }
        },
        err: function (data) {

        }
    });

}
