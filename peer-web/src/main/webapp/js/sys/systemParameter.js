/**
 * Created by zhangerxin on 2016/4/6.
 */

//设置问题参数
function doSetSysParameter(){
    var id = $("#id").val(); //主键
    var keyword = $("#keyword").val(); //关键字
    var activeFlg = ''; //是否启用密码错误次数超限锁定(0-启用、1-未启用)
    var times = $("#times").val(); //密码输入错误次数
    var time = $("#time").val(); //密码输入错误时长

    /*************** 判断radio是否被选中Start ***************/
    var radios = document.getElementsByName("activeFlg");
    var tag = false;
    var val;
    for(radio in radios) {
        if(radios[radio].checked) {
            tag = true;
            activeFlg = radios[radio].value;
            break;
        }
    }
    /**************** 判断radio是否被选中End ****************/

    var sysPara = {id:id,keyword:keyword,activeFlg:activeFlg,times:times,time:time};

    $.ajax({
        type:"post",
        url: "/sysSecurityParameter/setSecurityParameter",
        data:sysPara,
        dataType:"json",
        success:function(data){
            if(data.result == "error"){
                alert(data.info);
            }else{
                alert(data.info);
                //queryRoleInfo();
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}