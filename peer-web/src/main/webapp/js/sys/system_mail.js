/**
 * Created by cuibin on 2016/4/25.
 */
/**
 *  添加 邮件设置
 */
function addSystemEmail(){
    var id = $("#id").val();
    var address = $("#address").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var pop = $("#pop").val();
    var smtp = $("#smtp").val();
    var reg = /^\s+$/gi;
    if(email == "" || reg.test(email)){
        alert("email不能为空");
        return;
    }
    if(password == "" || reg.test(password)){
        alert("密码不能为空");
        return;
    }
    var temp = {id:id,address:address,email:email,password:password,pop:pop,smtp:smtp};
    $.ajax({
        type:"post",
        url: "/email/addSystemEmail",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert(data.info);
            }
        },
        err:function(data){

        }
    });
}
