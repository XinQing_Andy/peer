/** 真实姓名 **/

//环迅部分注册
function registHx(){
    var merBillNo = $("#merBillNo").val(); //商户订单号
    var userType = $("#userType").val(); //用户类型(1-个人、2-企业)
    var userName = $("#userName").val(); //用户名
    var mobileNo = $("#mobileNo").val(); //手机号
    var identNo = $("#identNo").val(); //身份证号
    var bizType = $("#bizType").val(); //业务类型(1-P2P、2-众筹)
    var realName = $("#realName").val(); //真实姓名
    var enterName = $("#enterName").val(); //企业名称
    var orgCode = $("#orgCode").val(); //营业执照编码
    var isAssureCom = $("#isAssureCom").val(); //是否为担保企业
    var webUrl = $("#webUrl").val(); //页面返回地址
    var s2SUrl = $("#s2SUrl").val(); //后台通知地址

    var huanXunRegistMes = {merBillNo:merBillNo,userType:userType,userName:userName,mobileNo:mobileNo,
                            identNo:identNo,bizType:bizType,realName:realName,enterName:enterName,
                            orgCode:orgCode,isAssureCom:isAssureCom,webUrl:webUrl,s2SUrl:s2SUrl};

    if(userName == '' || typeof (userName) == 'undefined'){
        alert('用户名不完善，请前去个人基本资料进行信息完善');
        return;
    }
    if(mobileNo == '' || typeof (mobileNo) == 'undefined'){
        alert('手机号不完善，请前去安全设置进行手机认证！');
        return;
    }
    if(identNo == '' || typeof (identNo) == 'undefined'){
        alert('身份证号不完善，请前去个人基本资料进行信息完善');
        return;
    }
    if(realName == '' || typeof (realName) == 'undefined'){
        alert('真实姓名不完善，请前去个人基本资料进行信息完善');
        return;
    }

    $.ajax({
        type:"post",
        url:"/huanxun/registerhx",
        data: huanXunRegistMes,
        dataType:"json",
        success:function(data){
            $("#operationType").val(data.operationType); //操作类型
            $("#merchantID").val(data.merchantID); //商户存管交易账号
            $("#sign").val(data.sign); //签名
            $("#request").val(data.request); //请求信息
            $("#registForm").submit();
        },
        err:function(data){
            alert('注册失败，请稍后重试！');
        }
    });
}