/**
 * Created by lenovo on 2015/12/31.
 */

/**
 * 查看按钮切换
 * @param menuUrl
 * @param controlUrl
 * @returns {*}
 */
function getDksqs(menuUrl, controlUrl) {
    $("#tabCon").empty();
    var menu = {jetxName: menuUrl};
    var userMenu = $.param(menu);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: controlUrl,
        data: userMenu,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
            $('#tabCon').append('<div>' + data.htmlText + '</div>');
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 * 提交
 * @param id
 * @returns {*}
 */
function subdksqs(id) {
    var flag = confirm("是否提交？");
    if (flag) {
        var defer = $.Deferred();
        var parm = {id: id};
        console.log(parm);
        $.ajax({
            type: "post",
            url: "/manager/upLoan",
            data: $.param(parm),
            dataType: "json",
            success: function (data) {
                defer.resolve(data);
            },
            err: function (data) {
                defer.resolve("undefined");
            }
        });
        return defer.promise();
    }
}

/**
 * 删除
 * @param id
 */
function deldksqs(id) {
    var flag = confirm("是否删除？");
    if (flag) {
        var defer = $.Deferred();
        var parm = {id: id};
        $.ajax({
            type: "post",
            url: "/manager/delLoan",
            data: $.param(parm),
            dataType: "json",
            success: function (data) {
                defer.resolve(data);
            },
            err: function (data) {
                defer.resolve("undefined");
            }
        });
        return defer.promise();
    }
}
/**
 * 新增
 */
function addDksqs(menuUrl, controlUrl, menuName, id) {
    var menu = {jetxName: menuUrl, id: id};
    var userMenu = $.param(menu);
    addTab(menuUrl, controlUrl, menuName, userMenu);
}

/**
 * 保存新增
 */
function saveDksqs() {
    var userInfo = $("#addDksqs").serialize();
    $.when(addsqs(userInfo)).done(function (data) {
        if (data.result == "error") {
            alert("添加失败" + data.info);
        } else {
            alert("添加成功");
        }
    });
}

function addsqs(userInfo) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manager/addLoan",
        data: userInfo,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 打开修改页面
 * @param userInfo
 */
function updateDksqsPage(menuUrl, controlUrl, menuName, id) {
    var menu = {jetxName: menuUrl, id: id};
    var userMenu = $.param(menu);
    addTab(menuUrl, controlUrl, menuName, userMenu);
}

/**
 * 修改保存
 */
function updateDksqs() {
    var userInfo = $("#updateDksqs").serialize();
    console.log(userInfo);
    $.when(updateDksqsInfo(userInfo)).done(function (data) {
        if (data.result == "error") {
            alert("修改失败" + data.info);
        } else {
            alert("修改成功");
        }
    });
}


function updateDksqsInfo(userInfo) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manager/upDateLoan",
        data: userInfo,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 *详情
 */
function seeDksqsPage(menuUrl, controlUrl, menuName, id) {
    var menu = {jetxName: menuUrl, id: id};
    var userMenu = $.param(menu);
    addTab(menuUrl, controlUrl, menuName, userMenu);
}