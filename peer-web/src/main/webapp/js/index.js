/**
 * 登录
 * Created by WangMeng on 2015/12/5.
 */
$(document).ready(function(){
    //登录
    $("#loginUp").on({
        "click":onLogin
    });
});
/**
 * 登录方法
 */
function onLogin(){
    var userInfo=$("#index_login").serialize();
    console.log(userInfo);
    $.when(getAdmin(userInfo)).done(function(data){
        if(data.result=="error"){
            $('#login_info').html(data.info);
        }else{
            location.href ="/manager/homePage";
        }
    });
}
/**
 * 登录
 * @param userInfo
 * @returns {*}
 */
function getAdmin(userInfo){
    var defer= $.Deferred();
    $.ajax({
        type:"post",
        url:"/manager/login",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}