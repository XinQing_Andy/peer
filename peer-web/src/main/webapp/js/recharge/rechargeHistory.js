/**
 * Created by Cuibin on 2016/7/27.
 */

/*************** 会员 分页部分Start ***************/
function getBySomeRh(advert) {
    if (!CheckTime()) {
        return;
    }
    $.ajax({
        type: "post",
        url: "/member/queryAllMemberBypage",
        data: advert,
        dataType: "json",
        success: function (data) {
            if (data.result == 'success') {
                $('#memDiv').html(data.htmlText);
            } else {
                $('#memDiv').after(data.info);
            }
        },
        err: function (data) {
            console.log(data);
        }
    });
}

//首页
function startPageRh() {
    $("#pageIndex").val("1");
    $("#pageSize").val("10");
    $("#currentPage").val("1");
    queryRhByCon();
}

//尾页
function endPageRh() {
    var totalCount = $("#totalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if (parseInt(totalCount) % 10 == 0) {
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    } else {
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    $("#pageIndex").val(pageIndex);
    $("#pageSize").val(pageSize);
    $("#currentPage").val(currentPage);
    queryRhByCon();
}

//上一页
function UpRh() {
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if (parseInt(currentPage) == 1) {
        pageIndex = "1";
        currentPage = "1";
    } else {
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量

    $("#pageIndex").val(pageIndex);
    $("#pageSize").val(pageSize);
    $("#currentPage").val(currentPage);

    queryRhByCon();
}

//下一页
function DownRh() {
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#totalCount").val(); //总数量
    var pageSize = "10";

    if (parseInt(totalCount) % 10 == 0) {
        totalPage = parseInt(totalCount) / 10; //总页数
    } else {
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if (parseInt(currentPage) < parseInt(totalPage)) {
        currentPage = parseInt(currentPage) + 1; //当前页
    } else {
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    $("#pageIndex").val(pageIndex);
    $("#pageSize").val(pageSize);
    $("#currentPage").val(currentPage);
    queryRhByCon();
}

//按条件查询用户列表
function queryRhByCon() {
    if (!rhCheckTime()) {
        return;
    }
    for (var j = 1; j < 4; j++) {
        if ($("#l" + j).attr("class") == "active") {
            var id = $("#l" + j).attr("id");
            var orderColumn = "";
            if (id == "l1") {
                orderColumn = "";
            } else if (id == "l2") {
                orderColumn = "IPSDOTIME";
            } else if (id == "l3") {
                orderColumn = "IPSTRDAMT";
            }
        }
    }
    var realname = $("#realname").val();
    var idcard = $("#idcard").val();
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var pageIndex = $("#pageIndex").val();
    var pageSize = $("#pageSize").val();

    var temp = {
        realname: realname,
        idcard:idcard,
        startTime: startTime,
        endTime: endTime,
        orderType: $("#orderType").val(),
        orderColumn: orderColumn,
        pageSize:pageSize,
        pageIndex:pageIndex
    };
    $.ajax({
        type: "post",
        url: "/rechargeHistory/queryRechargeHistoryByPage",
        data: temp,
        dataType: "json",
        success: function (data) {
            $("#str").html("");
            $('#str').html(data.str);
            $('#totalCount').val(data.totalCount);
            $('#currentPage').val(data.currentPage);
            $('#pageStr').html(data.pageStr);
        },
        err: function (data) {

        }
    });
}
//跳转到指定页数
function doGoPageRh(page) {

    var currentPage = page; //当前页
    $("#pageSize").val("10");//每页显示数量
    $("#pageIndex").val((parseInt(currentPage) - 1) * 10 + 1);//当前数

    queryRhByCon();
}
/*************** 会员 分页部分End ***************/

function rhCheckTime() {
    var val = $("#startTime").val();
    var val2 = $("#endTime").val();
    var time1 = new Date(val).getTime();
    var time2 = new Date(val2).getTime();
    if (time1 > time2 || time1 == time2) {
        alert("开始时间必须小于结束时间");
        return false;
    }
    return true;
}
/**
 *
 */
function selectBqRh(id) {
    for (var i = 1; i < 4; i++) {
        $("#l" + i).attr("class", "");
    }
    $("#" + id).attr("class", "active");

    var temp = $("#" + id).html();
    var last = temp.substr(temp.length - 1, temp.length);

    if (last == '↓') {
        last = '↑';
        var head = temp.substr(0, temp.length -1);
        $("#" + id).html(head + last);
        $("#orderType").val("ASC");
    } else if (last == '↑') {
        last = '↓';
        var head = temp.substr(0, temp.length -1);
        $("#" + id).html(head + last);
        $("#orderType").val("DESC");
    } else if (last != '↓' && last != '↑') {
        last = '↑';
        $("#" + id).html(temp + last);
        $("#orderType").val("ASC");
    }

    for (var j = 1; j < 4; j++) {
        if (("l" + j) != id) {
            var temp = $("#l" + j).html();
            var last = temp.substr(temp.length - 1, temp.length);

            if (last == '↓' || last == '↑') {
                temp = temp.substr(0, temp.length - 1);
                $("#l" + j).html(temp);
            }
        }
    }

    queryRhByCon();
}

