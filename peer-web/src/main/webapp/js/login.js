/**
 * 登录
 * Created by WangMeng on 2015/12/5.
 */
$(document).ready(function(){
    //登录
    $("#submit").on({
        "click":onLogin
    });
    //验证码
    $("#imgObj").on({
        "click":getverifyCode
    });
});


/**
 * 得到验证码
 */
function getverifyCode(){
    $("#imgObj").attr("src","/finance/verifyCode?"+Math.random());
}

/**
 * 登录方法
 */
function onLogin(e){
    // 不执行与事件关联的默认动作
    e.preventDefault();
    var userInfo = $("#loginForm").serialize();
    $.when(getAdmin(userInfo)).done(function(data){
        if(data.result=="error"){
            console.log(data);
            alert("编辑失败" + data.info);
        }else{
            location.href ="/manage/mainPage";
        }
    });
}

//回车登录
function keyLogin(){

    var userInfo = $("#loginForm").serialize();

    $.when(getAdmin(userInfo)).done(function(data){
        if(data.result=="error"){
            console.log(data);
            alert("编辑失败" + data.info);
        }else{
            location.href ="/manage/mainPage";
        }
    });
}

/**
 * 登录
 * @param userInfo
 * @returns {*}
 */
function getAdmin(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url:"/manage/login",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}