/**
 * Created by Cuibin on 2016/7/8.
 */


/**
 *   登录
 */
function login() {
    var userName = $("#userName").val(); //用户名
    var password = $("#password").val(); //密码
    var temp = {userName: userName, password: password};
    $.ajax({
        type: "post",
        url: "/weixin/userLogin",
        data: temp,
        dataType: "json",
        timeout:15000,
        success: function (data) {
            if (data.result == 'success') {
                location.href = "/weixin/goAsset";
            } else {
                $("#message").html(data.info);
            }
        },
        err: function (data) {
            $("#message").html("请检查网络连接");
        }
    });

}