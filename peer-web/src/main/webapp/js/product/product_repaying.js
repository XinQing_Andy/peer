/**
 * Created by Buyatao's on 2016/9/18.
 */

/*************** 还款中 分页部分Start ***************/

//首页
function ProductRepayingStartPage() {
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repaying.jetx'
    };
    queryRepayingInfoBySomes(product); //根据条件查询产品
}
//尾页
function ProductRepayingEndPage() {
    var totalCount = $("#productTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if (parseInt(totalCount) % 10 == 0) {
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    } else {
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repaying.jetx'
    };
    queryRepayingInfoBySomes(product); //根据条件查询产品
}
//上一页
function ProductRepayingUp() {
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if (parseInt(currentPage) == 1) {
        pageIndex = "1";
        currentPage = "1";
    } else {
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repaying.jetx'
    };
    queryRepayingInfoBySomes(product); //根据条件查询产品
}
//下一页
function ProductRepayingDown() {
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#productTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if (parseInt(totalCount) % 10 == 0) {
        totalPage = parseInt(totalCount) / 10; //总页数
    } else {
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if (parseInt(currentPage) < parseInt(totalPage)) {
        currentPage = parseInt(currentPage) + 1; //当前页
    } else {
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repaying.jetx'
    };
    queryRepayingInfoBySomes(product); //根据条件查询产品
}
//跳转到指定页数
function doProductRepayingGoPage(pageCount) {
    var productRepayingNum = $('#productRepayingNum').val(); //跳转到第几页
    if (productRepayingNum == '') {
        alert('请输入要跳转的页数！');
        return;
    }
    if (!validate(productRepayingNum)) {
        alert('请输入合法的数字！');
        return;
    }
    if (parseInt(productRepayingNum) > parseInt(pageCount)) {
        alert('跳转的页数不存在！');
        return;
    }
    var currentPage = productRepayingNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var title = $("#title").val(); //题目
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repaying.jetx'
    };
    queryRepayingInfoBySomes(product); //根据条件查询产品
}

//按条件查询还款中产品列表
function queryRepayingInfoBySomes() {
    if (!CashCheckTime()) {
        return;
    }
    var productCode = $("#productCode").val();
    var productTitle = $("#productTitle").val();
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var pageIndex = $("#pageIndex").val();
    var pageSize = $("#pageSize").val();

    var temp = {
        productCode: productCode,
        productTitle: productTitle,
        startTime: startTime,
        endTime: endTime,
        pageSize: pageSize,
        pageIndex: pageIndex,
        menuUrl: 'product/product_repaying.jetx'

    };
    $.ajax({
        type: "post",
        url: "/productRepay/queryRepayingInfoByPage",
        data: temp,
        dataType: "json",
        success: function (data) {
            //$("#str").html("");
            $('#str').html(data.str);
            $('#totalCount').val(data.totalCount);
            $('#currentPage').val(data.currentPage);
            $('#pageStr').html(data.pageStr);
        },
        err: function (data) {
            alert("error");
        }
    });
}
//跳转到指定页数
function doGoPageCash(page) {
    var currentPage = page; //当前页
    $("#pageSize").val("10");//每页显示数量
    $("#pageIndex").val((parseInt(currentPage) - 1) * 10 + 1);//当前数
    queryRepayInfo();
}
/*************** 还款中 分页部分End ***************/

//导出还款中产品列表
function productRepaying(){
    var where = "WHERE 1 = 1"; //查询条件
    var title = $("#productTitle").val(); //题目
    var code = $("#productCode").val(); //编码
    if(title != '' && typeof (title) != 'undefined'){
        where = where + " AND TITLE LIKE '%"+title+"%'";
    }
    if(code != '' && typeof (code) != 'undefined'){
        where = where + " AND CODE LIKE '"+code+"'";
    }
    where = where + " AND REPAYFLG = '0'";
    $("#ExportTitle").val('名称;TITLE,编码;CODE,还款方式;REPAYMENTNAME,已还期数;REPAYPERIODS,借款人;REALNAME,年利率;LOANRATE,下期还款日;NEXTRECEIVETIME');
    $("#tableName").val('view_product_repay_export');
    $("#where").val(where);
    $("#productRepaying").submit();
}


function CashCheckTime() {
    var val = $("#startTime").val();
    var val2 = $("#endTime").val();
    var time1 = new Date(val).getTime();
    var time2 = new Date(val2).getTime();
    if (time1 > time2 || time1 == time2) {
        alert("开始时间必须小于结束时间");
        return false;
    }
    return true;
}


/**
 *
 */
function selectBqC(id) {
    for (var i = 1; i < 4; i++) {
        $("#l" + i).attr("class", "");
    }
    $("#" + id).attr("class", "active");
    var temp = $("#" + id).html();
    var last = temp.substr(temp.length - 1, temp.length);
    if (last == '↓') {
        last = '↑';
        var head = temp.substr(0, temp.length - 1);
        $("#" + id).html(head + last);
        $("#orderType").val("ASC");
    } else if (last == '↑') {
        last = '↓';
        var head = temp.substr(0, temp.length - 1);
        $("#" + id).html(head + last);
        $("#orderType").val("DESC");
    } else if (last != '↓' && last != '↑') {
        last = '↑';
        $("#" + id).html(temp + last);
        $("#orderType").val("ASC");
    }
    for (var j = 1; j < 4; j++) {
        if (("l" + j) != id) {
            var temp = $("#l" + j).html();
            var last = temp.substr(temp.length - 1, temp.length);

            if (last == '↓' || last == '↑') {
                temp = temp.substr(0, temp.length - 1);
                $("#l" + j).html(temp);
            }
        }
    }
    queryRepayingInfoBySomes();
}

