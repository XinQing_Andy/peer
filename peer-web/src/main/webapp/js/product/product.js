/**
 * Created by zhangerxin on 2015/12/28.
 * 产品部分
 */


//查询申请人
function queryApplyMember() {
    $.ajax({
        type: "post",
        url: "/manage/querySetRoleList",
        data: sysRoleMes,
        dataType: "json",
        success: function (data) {
            $("#tableAddSet").html(data.htmlText);
            $("#totalCountRole").val(data.totalCount); //角色总数量
            $("#currentPageRole").val(data.currentPage); //当前页
            $("#trAddSetting").html(data.roleStr); //角色翻页拼接
            $("#qfFlag").val('add');
            var bDiv = document.getElementById('myModal');
            bDiv.style.display = "block";
        },
        err: function (data) {
            console.log(data);
        }
    });
}

//按条件查询产品列表
function queryProductInfo() {
    var keyWord = $("#title").val(); //查询关键字
    var shFlag = $("#shFlag").val(); //审核状态(0-审核通过、1-审核未通过、2-待审核)
    var product = {title: keyWord, shflag:shFlag, menuUrl: 'product/product.jetx'};

    $.ajax({
        type: "post",
        url: "/product/queryProductByPage",
        data: product,
        dataType: "json",
        success: function (data) {
            if (data.result == 'success') {
                $('#nowPage').html(data.htmlText);
                $("#title").val(keyWord); //题目
                $("#shFlag").val(shFlag); //审核状态
            } else {
                $('#nowPage').html(data.info);
            }
        },
        err: function (data) {
            console.log(data);
        }
    });
}


//进入新增页面
function openProduct() {
    var productMes = {menuUrl: 'product/product_add.jetx'};

    $.ajax({
        type: "post",
        url: "/product/openProduct",
        data: productMes,
        dataType: "json",
        success: function (data) {
            if (data.result == 'success') {
                $('#nowPage').html(data.htmlText);
            } else {
                $('#nowPage').after(data.info);
            }
        },
        err: function (data) {
            console.log(data);
        }
    });
}

//新增产品
function addProductInfo() {
    var title = $("#title").val(); //借款标名称
    var code = $("#code").val(); //借款标编码
    var productType = $("#productType").val(); //借款标类型
    var crowds = $("#crowds").val(); //适合人群
    var highpoint = $("#highpoint").val(); //产品亮点
    var creditstart = $("#creditstart").val(); //额度范围(起始)
    var creditend = $("#creditend").val(); //额度范围(截止)
    var loanrate = $("#loanrate").val(); //贷款利率
    var loanyear = $("#loanyear").val(); //贷款期限(年)
    var loanmonth = $("#loanmonth").val(); //贷款期限(月)
    var loadday = $("#loadday").val(); //贷款期限(日)
    var bidday = $("#bidday").val(); //投标时间
    var checktime = $("#checktime").val(); //审核时间
    var repayment = ''; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
    var bidfee = $("#bidfee").val(); //最低投标金额
    var splitcount = $("#splitcount").val(); //借款标最高拆分份数
    var counterfee = $("#counterfee").val(); //手续费
    var applyMember = $("#productMemberId").val(); //申请人
    var applycondition = $("#applycondition").val(); //申请人条件
    var onlinetime = $("#onlinetime").val(); //上线日期
    var sponsor = $("#sponsor").val(); //担保人
    var fullScale = '1'; //是否满标
    var sort = $("#sort").val(); //排序
    var pathPj = $("#pathPj").val(); //路径拼接(相对路径,绝对路径;相对路径,绝对路径)

    /******************** 还款方式部分Start ********************/
    var number = '';
    var radio = document.getElementsByName("repayment");
    for (var i = 0; i < radio.length; i++) {
        if (radio[i].checked) {
            number = radio[i].value;
            break;
        }
    }
    repayment = number; //0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款
    /********************* 还款方式部分End *********************/

    var productMes = {
        title: title,
        crowds: crowds,
        code: code,
        productType: productType,
        highpoint: highpoint,
        creditstart: creditstart,
        creditend: creditend,
        loanrate: loanrate,
        loanyear: loanyear,
        loanmonth: loanmonth,
        loadday: loadday,
        bidday: bidday,
        checktime: checktime,
        repayment: repayment,
        bidfee: bidfee,
        splitcount: splitcount,
        counterfee: counterfee,
        fullScale: fullScale,
        applycondition: applycondition,
        onlinetime: onlinetime,
        sponsor: sponsor,
        sort: sort,
        imgUrl: pathPj,
        applyMember: applyMember
    };

    /*************** 保存前验证Start ***************/
    if (title.length == 0) { //借款标名称
        alert('借款标名称不能为空！');
        return;
    }
    if (code.length == 0) { //借款标编码
        alert('借款标编码不能为空！');
        return;
    }
    if (productType.length == 0) { //借款标类型
        alert('借款标类型不能为空！');
        return;
    }
    if (crowds.length == 0) { //适合人群
        alert('适合人群不能为空！');
        return;
    }
    if (creditstart.length == 0 || creditend.length == 0) { //额度范围
        alert('额度范围不能为空！');
        return;
    }
    if (loanrate.length == 0) { //贷款利率
        alert('贷款利率不能为空！');
        return;
    }
    if (checktime.length == 0) { //审核时间
        alert('审核时间不能为空！');
        return;
    }
    if (repayment.length == 0) { //还款方式
        alert('请选择还款方式！');
        return;
    }
    if (bidfee.length == 0) { //最低投标金额
        alert('最低投标金额不能为空！');
        return;
    }
    if(splitcount.length == 0){ //借款标最高拆分份数
        alert('借款标最高拆分份数不能为空！');
        return;
    }
    if (applyMember.length == 0) { //申请人
        alert('申请人不能为空！');
        return;
    }
    if (onlinetime.length == 0) { //上线日期
        alert('上线日期不能为空！');
        return;
    }
    if (sort.length == 0) { //排序
        alert('排序不能为空！');
        return;
    }
    /**************** 保存前验证End ****************/

    $.ajax({
        type: "post",
        url: "/product/saveProduct",
        data: productMes,
        dataType: "json",
        success: function (data) {
            if (data.result == 'success') {
                alert(data.info);
                queryProductInfo();
            } else {
                alert('新增失败');
            }
        },
        err: function (data) {
            alert(data.info);
        }
    });
}


//环迅项目登记(已完善)
function huanXunDj(projectId) {
    var operationType = $("#operationType").val(); //操作类型
    var merchantID = $("#merchantID").val(); //商户存管交易账号
    var huanxunRegProjectMes = {id: projectId, operationType: operationType, merchantID: merchantID};
    $("#hxdj").css('display','none'); //环迅登记隐藏掉

    $.ajax({
        type:"post",
        url:"/huanxun/regProjecthx",
        data:huanxunRegProjectMes,
        dataType:"json",
        success:function(data){
            if(data.code == '0'){
                alert('环迅登记成功！');
                queryProductInfo();
            }else{
                $("#hxdj").css('display','');
                alert('环迅登记失败！');
            }
        },
        err: function (data) {
            alert('登记失败');
        }
    });
}


//上传附件
function addProductFile(productFile) {
    var path = ''; //路径
    var pathPj = $("#pathPj").val(); //路径拼接
    var start = 0; //开始标识
    var end = 0; //结束标识
    var length = 0; //截取长度
    var arr = [];
    var str = ''; //内容拼接

    $.ajaxFileUpload({
        url:'/product/addProductFile',
        secureuri:false,
        fileElementId:productFile,
        dataType:'text',
        success:function(data, status){

            start = data.indexOf('[');
            end = data.indexOf(']');

            if(start>-1 && end>-1){
                length = end - start + 1; //路径长度
                path = data.substr(start+1,length); //路径拼接

                /********** 将路径拼接好后直接赋值Start **********/
                if(pathPj == '' || typeof (pathPj) == "undefined"){
                    pathPj = path;
                }else{
                    pathPj = pathPj + ';' + path;
                }
                $("#pathPj").val(pathPj);
                /*********** 将路径拼接好后直接赋值End ***********/
                arr = path.split(',');

                str = '<li>'
                    + '<div class=\"playerdetail\">'
                    + '<div class=\"detailimg\" onclick=\"showimagepage(this)\"><img src=\"..'+arr[0]+'\" style=\"width:160px;height:160px\" border=\"0\"/></div>'
                    + '</div>'
                    + '</li>';
                $("#count1").prepend(str);
            }
        },
        error:function(data, status, e){
            alert('上传失败！');
        }
    });
}


//删除附件
function doDeleteFj(fileId,jdPath){
    var sysFile = {id:fileId,imgurljd:jdPath};

    $.ajax({
        type:"post",
        url: "/sysFile/delSysFile",
        data:sysFile,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                alert('删除成功！');
                $("#li"+fileId).remove();
            }else{
                alert('删除失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//验证数字
function validate(str){
    var reg = new RegExp("^[0-9]*$");
    if(!reg.test(str)){
        return false;
    }
    if(!/^[0-9]*$/.test(str)){
        return false;
    }
    return true;
}


//根据主键获取产品信息
function showProductById(id){
    var product = {id:id,menuUrl:'product/product_edit.jetx'};
    var xdUrl = ''; //相对路径
    $.ajax({
        type:"post",
        url: "/product/queryProductById",
        data:product,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').html(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


//更新产品信息
function updateProductInfo(){
    var id = $("#id").val(); //主键
    var code = $("#code").val(); //借款标编码
    var title = $("#title").val(); //借款标名称
    var productType = $("#productType").val(); //借款标类型
    var crowds = $("#crowds").val(); //适合人群
    var highpoint = $("#highpoint").val(); //产品亮点
    var creditstart = $("#creditstart").val(); //额度范围(起始)
    var creditend = $("#creditend").val(); //额度范围(截止)
    var loanrate = $("#loanrate").val(); //贷款利率
    var loanyear = $("#loanyear").val(); //贷款期限(年)
    var loanmonth = $("#loanmonth").val(); //贷款期限(月)
    var loadday = $("#loadday").val(); //贷款期限(日)
    var bidday = $("#bidday").val(); //投标时间
    var checktime = $("#checktime").val(); //审核时间
    var repayment = ''; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)
    var bidfee = $("#bidfee").val(); //最低投标金额
    var splitcount = $("#splitcount").val(); //借款标最高拆分份数
    var counterfee = $("#counterfee").val(); //手续费
    var applyMember = $("#productMemberId").val(); //申请人
    var applycondition = $("#applycondition").val(); //申请人条件
    var onlinetime = $("#onlinetime").val(); //上线日期
    var sponsor = $("#sponsor").val(); //担保人
    var sort = $("#sort").val(); //排序
    var pathPj = $("#pathPj").val(); //附件拼接

    var number = '';
    var radio = document.getElementsByName("repayment");
    for(var i=0;i<radio.length;i++){
        if(radio[i].checked){
            number = radio[i].value;
            break;
        }
    }
    repayment = number; //还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款)

    if(id.length == 0){ //主键
        alert('编辑信息错误！');
        return;
    }
    if(title.length == 0){
        alert('借款标名称不能为空！');
        return;
    }
    if(code.length == 0){ //借款标编号
        alert('借款标编码不能为空！');
        return;
    }
    if(productType.length == 0){ //借款标类型
        alert('借款标类型不能为空！');
        return;
    }
    if(creditstart.length == 0 || creditend.length == 0){ //额度范围
        alert('额度范围不能为空！');
        return;
    }
    if(loanrate.length == 0){ //贷款利率
        alert('贷款利率不能为空！');
        return;
    }
    if(checktime.length == 0){ //审核时间
        alert('审核时间不能为空！');
        return;
    }
    if(repayment.length == 0){ //还款方式
        alert('请选择还款方式！');
        return;
    }
    if(bidfee.length == 0){ //最低投标金额
        alert('最低投标金额不能为空！');
        return;
    }
    if(splitcount.length == 0){ //借款标最高拆分份数
        alert('借款标最高拆分份数不能为空！');
        return;
    }
    if(counterfee.length == 0){ //手续费
        alert('手续费不能为空！');
        return;
    }
    if(applyMember.length == 0){ //申请人
        alert('请选择申请人！');
        return;
    }
    if(onlinetime.length == 0){ //上线日期
        alert('上线日期不能为空！');
        return;
    }
    if(sponsor.length == 0){ //担保人
        alert('担保人不能为空！');
        return;
    }
    if(sort.length == 0){ //排序
        alert('排序不能为空！');
        return;
    }

    var productMes = {id:id,title:title,crowds:crowds,code:code,productType:productType,highpoint:highpoint,creditstart:creditstart,
        creditend:creditend,loanrate:loanrate,loanyear:loanyear,loanmonth:loanmonth,loadday:loadday,bidday:bidday,
        checktime:checktime,repayment:repayment,bidfee:bidfee,splitcount:splitcount,counterfee:counterfee,
        applycondition:applycondition,onlinetime:onlinetime,sponsor:sponsor,sort:sort,imgUrl:pathPj,applyMember:applyMember};

    $.ajax({
        type:"post",
        url:"/product/updateProduct",
        data:productMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert(data.info);
                queryProductInfo();
            }
        },
        err:function(data){
            alert(data.info);
        }
    });
}

//产品信息取消
function doProductCancel(){
    var product = {menuUrl:'product/product.jetx'};

    $.ajax({
        type:"post",
        url: "/product/queryProductByPage",
        data:product,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').html(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//设置产品状态(0-上架、1-下架)
function setProductFlag(id,flag,type){
    var product = {id:id,flag:flag,type:type}; //产品信息

    $.ajax({
        type:"post",
        url: "/product/setProductFlag",
        data:product,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert('设置成功！');
                queryProductInfo();
            }else{
                alert(data.info);
            }
        },
        err:function(data){
            alert(data.info);
            console.log(data);
        }
    });
}


//弹出审核窗口
function openProductShFlag(id){
    $("#idSh").val(id); //主键
    var bDiv = document.getElementById('productSh');
    bDiv.style.display = "block";
}


//产品审核
function ProductSh(shFlag){
    var id = $("#idSh").val(); //主键
    var shyj = $("#shyj").val(); //审核意见
    var url = ''; //后台跳转地址

    if(shFlag == '0'){ //审核通过
        url = "/huanxun/transferBuyhx";
        var huanxunFreezeMes = {productId:id};
    }else if(shFlag == '1'){
        url = "/huanxun/unfreeze";
        var huanxunFreezeMes = {id:id};
    }

    $.ajax({
        type:"post",
        url:url,
        data:huanxunFreezeMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert(data.info);
            }else{
                alert(data.info);
            }
            queryProductInfo();
        },
        err:function(data){
            console.log(data);
        }
    });
}

//产品审核部分取消
function cancelProduct(){
    var bDiv = document.getElementById('productSh');
    bDiv.style.display = "none";
}


//添加附件
function addProductFj(){
    var num = number + 1;
    var str = '<tr id=\"trProduct'+num+'\">'
            + '<td class=\"pay_right\"></td>'
            + '<td class=\"btn_td\">'
            + '<input type=\"file\" id=\"upProduct'+num+'\" name="upProduct'+num+'" />'
            + '</td>'
            + '</tr>';
    $("#trProduct"+number).after(str);
}



/******************** 会员部分Start ********************/
//申请人设置部分
function queryApplyMember(){
    var pageIndex = "1"; //当前数
    var pageSize = "10"; //每页显示数量
    var currentPage = "1"; //当前页
    var productMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};

    $.ajax({
        type:"post",
        url: "/product/queryAllMember",
        data:productMes,
        dataType:"json",
        success:function(data){
            $("#memberSet").html(data.htmlText); //显示列表部分
            $("#trMemberSetting").html(data.pageMemberStr); //分页
            $("#currentPageMember").val(data.currentPage); //当前页
            $("#totalCountPageMember").val(data.totalCount); //总数量
            var bDiv = document.getElementById('ProductMemberSet');
            bDiv.style.display = "block";
        },
        err:function(data){
            console.log(data);
        }
    });
}

//选择相应的会员
function doCheckProductMember(id,name){
    $("#productMemberId").val(id); //会员id
    $("#productMemberName").val(name); //会员名称
}

//确定(会员部分)
function productMemberConfirm(){
    var memberName = $("#productMemberName").val(); //会员名称
    $("#productMemberSetting2").html(memberName);
    var bDiv = document.getElementById('ProductMemberSet');
    bDiv.style.display = "none";
}

//取消(会员部分)
function productMemberCancel(bz){
    if(bz == 'add'){ //添加中会员
        var bDiv = document.getElementById('ProductMemberSet');
        bDiv.style.display = "none";
    }else if(bz == 'edit'){
        var bDiv = document.getElementById('ProductMemberSet');
        bDiv.style.display = "none";
    }
}
/********************* 会员部分End *********************/


/******************** 分页部分(会员)Start ********************/
function getProductMemberBySome(member) {
    $.ajax({
        type: "post",
        url: "/product/queryAllMember",
        data: member,
        dataType: "json",
        success: function (data) {
            $("#memberSet").html(data.htmlText); //显示列表部分
            $("#trMemberSetting").html(data.pageMemberStr); //分页部分拼接
            $("#currentPageMember").val(data.currentPage); //当前页
            $("#totalCountPageMember").val(data.totalCount); //总数量
            var bDiv = document.getElementById('ProductMemberSet');
            bDiv.style.display = "block";
        },
        err: function (data) {
            console.log(data);
        }
    });
}

//首页(跳转标志)
function ProductMemberStartPage(tzbz) {
    var menuUrl = ''; //跳转路径
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    if (tzbz == 'add') { //新增页面
        menuUrl = 'product/product_add.jetx'
    } else if (tzbz == 'edit') { //修改页面
        menuUrl = 'product/product_edit.jetx'
    }
    var member = {pageIndex: pageIndex, pageSize: pageSize, currentPage: currentPage, menuUrl: menuUrl};
    getProductMemberBySome(member); //根据条件查询产品
}

//尾页(跳转标志)
function ProductMemberEndPage(tzbz) {
    var totalCount = $("#totalCountPageMember").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    var menuUrl = ''; //跳转路径
    if (parseInt(totalCount) % 10 == 0) {
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    } else {
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }

    var pageSize = '10'; //每页显示数量
    if (tzbz == 'add') { //新增页面
        menuUrl = 'product/product_add.jetx'
    } else if (tzbz == 'edit') { //修改页面
        menuUrl = 'product/product_edit.jetx'
    }
    var member = {pageIndex: pageIndex, pageSize: pageSize, currentPage: currentPage, menuUrl: menuUrl};
    getProductMemberBySome(member); //根据条件查询产品
}

//上一页
function ProductMemberUp(tzbz) {
    var currentPage = $("#currentPageMember").val(); //当前页
    var pageIndex = "1"; //当前数
    var menuUrl = ''; //跳转路径

    if (parseInt(currentPage) == 1) {
        pageIndex = "1";
        currentPage = "1";
    } else {
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    if (tzbz == 'add') { //新增页面
        menuUrl = 'product/product_add.jetx';
    } else if (tzbz == 'edit') { //修改页面
        menuUrl = 'product/product_edit.jetx';
    }

    var member = {pageIndex: pageIndex, pageSize: pageSize, currentPage: currentPage, menuUrl: menuUrl};
    getProductMemberBySome(member); //根据条件查询产品
}

//下一页
function ProductMemberDown(tzbz) {
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPageMember").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#totalCountPageMember").val(); //总数量
    var pageSize = "10";
    var menuUrl = '';

    if (parseInt(totalCount) % 10 == 0) {
        totalPage = parseInt(totalCount) / 10; //总页数
    } else {
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if (parseInt(currentPage) < parseInt(totalPage)) {
        currentPage = parseInt(currentPage) + 1; //当前页
    } else {
        currentPage = totalPage; //当前页
    }

    if (tzbz == 'add') { //新增页面
        menuUrl = 'product/product_add.jetx';
    } else if (tzbz == 'edit') { //修改页面
        menuUrl = 'product/product_edit.jetx';
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var member = {pageIndex: pageIndex, pageSize: pageSize, currentPage: currentPage, menuUrl: menuUrl};
    getProductMemberBySome(member); //根据条件查询产品
}

//跳转到指定页数
function doProductMemberGoPage(page) {
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var productMes = {pageIndex: pageIndex, pageSize: pageSize, currentPage: currentPage};
    getProductMemberBySome(productMes); //根据条件查询产品
}
/********************* 分页部分(会员)End *********************/


//跳转到投资记录
function openInvestHistory(productId) {
    var productInvestMes = {productId:productId};

    $.ajax({
        type: "post",
        url: "/product/openInvestHistory",
        data: productInvestMes,
        dataType: "json",
        success: function (data) {
            if (data.result == 'success') {
                $('#nowPage').html(data.htmlText);
            } else {
                $('#nowPage').html(data.info);
            }
        },
        err: function (data) {
            console.log(data);
        }
    });
}

//导出投资历史
function exportInvestHistory(){
    var where = "WHERE 1 = 1"; //查询条件
    var startTime = $("#startTime").val(); //开始时间
    var endTime = $("#endTime").val(); //终止时间
    if(startTime != '' && typeof (startTime) != 'undefined'){
        where = where + " AND DATE(CREATETIME) >= DATE('"+startTime+"')";
    }
    if(endTime != '' && typeof (endTime) != 'undefined'){
        where = where + " AND DATE(CREATETIME) <= DATE('"+endTime+"')";
    }
    $("#ExportTitle").val('名称;TITLE,编码;CODE,投资人;BUYERNAME,投资金额;PRICE,投资时间;CREATETIME,年利率;LOANRATE');
    $("#tableName").val('view_product_buy_export');
    $("#where").val(where);
    $("#exportInvestHistory").submit();
}

function queryInvestHistoryByCon() {
    if (!InvestHistoryCheckTime()) {
        return;
    }

    var productId = $("#productId").val();
    var startTime = $("#startTime").val();
    var endTime = $("#endTime").val();
    var pageIndex = $("#pageIndex").val();
    var pageSize = $("#pageSize").val();

    var temp = {
        startTime: startTime,
        endTime: endTime,
        pageSize:pageSize,
        pageIndex:pageIndex,
        productId:productId
    };

    $.ajax({
        type: "post",
        url: "/product/queryInvestHistory",
        data: temp,
        dataType: "json",
        success: function (data) {
            $("#str").html("");
            $('#str').html(data.str);
            $('#totalCount').val(data.totalCount);
            $('#currentPage').val(data.currentPage);
            $('#productId').val(data.productId);
            $('#pageStr').html(data.pageStr);
        },
        err: function (data) {

        }
    });
}

//首页
function startPageInvestHistory() {
    $("#pageIndex").val("1");
    $("#pageSize").val("10");
    $("#currentPage").val("1");
    queryInvestHistoryByCon();
}

//尾页
function endPageInvestHistory() {
    var totalCount = $("#totalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if (parseInt(totalCount) % 10 == 0) {
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    } else {
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    $("#pageIndex").val(pageIndex);
    $("#pageSize").val(pageSize);
    $("#currentPage").val(currentPage);
    queryInvestHistoryByCon();
}

//上一页
function UpInvestHistory() {
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if (parseInt(currentPage) == 1) {
        pageIndex = "1";
        currentPage = "1";
    } else {
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量

    $("#pageIndex").val(pageIndex);
    $("#pageSize").val(pageSize);
    $("#currentPage").val(currentPage);

    queryInvestHistoryByCon();
}

//下一页
function DownInvestHistory() {
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#totalCount").val(); //总数量
    var pageSize = "10";

    if (parseInt(totalCount) % 10 == 0) {
        totalPage = parseInt(totalCount) / 10; //总页数
    } else {
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if (parseInt(currentPage) < parseInt(totalPage)) {
        currentPage = parseInt(currentPage) + 1; //当前页
    } else {
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    $("#pageIndex").val(pageIndex);
    $("#pageSize").val(pageSize);
    $("#currentPage").val(currentPage);
    queryInvestHistoryByCon();
}


//跳转到指定页数
function doGoPageInvestHistory(page) {

    var currentPage = page; //当前页
    $("#pageSize").val("10");//每页显示数量
    $("#pageIndex").val((parseInt(currentPage) - 1) * 10 + 1);//当前数

    queryInvestHistoryByCon();
}

function InvestHistoryCheckTime() {
    var val = $("#startTime").val();
    var val2 = $("#endTime").val();
    var time1 = new Date(val).getTime();
    var time2 = new Date(val2).getTime();
    if (time1 > time2 || time1 == time2) {
        alert("开始时间必须小于结束时间");
        return false;
    }
    return true;
}

//设为推荐
function setRecomment(productId,newShare){
    $.ajax({
        type: "post",
        url: "/product/setNewShare",
        data: "newShare="+newShare+"&productId="+productId,
        dataType: "json",
        success: function (data) {
            if(data.result == '0'){
                alert('设置推荐成功');
                queryProductInfo();
            }else{
                alert('设置推荐失败！');
            }
        },
        err: function (data) {
            alert('设置推荐失败！');
        }
    });
}

//产品部分导出
function exportProduct(){
    var where = "WHERE 1 = 1"; //查询条件
    var title = $("#title").val(); //题目
    var shFlag = $("#shFlag").val(); //审核条件
    if(title != '' && typeof (title) != 'undefined'){
        where = where + " AND TITLE LIKE '%"+title+"%'";
    }
    if(shFlag != '' && typeof (shFlag) != 'undefined'){
        where = where + " AND SHFLAG = '"+shFlag+"'";
    }
    $("#ExportTitle").val('名称;TITLE,添加时间;CREATETIME,年利率;LOANRATE,借款额度;CREDITSCOPE,状态;FLAGNAME,是否审核;SHFLAGNAME,排序;SORT');
    $("#tableName").val('view_product_export');
    $("#where").val(where);
    $("#exportProduct").submit();
}

/************************* 分页部分Start *************************/

/********** 产品Start **********/
//根据条件查询产品
function getProductBySome(product){
    $.ajax({
        type:"post",
        url: "/product/queryProductByPage",
        data:product,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//首页
function ProductStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var product = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'product/product.jetx'};
    getProductBySome(product); //根据条件查询产品
}
//尾页
function ProductEndPage(){
    var totalCount = $("#productTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var product = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'product/product.jetx'};
    getProductBySome(product); //根据条件查询产品
}
//上一页
function ProductUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var product = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'product/product.jetx'};
    getProductBySome(product); //根据条件查询产品
}
//下一页
function ProductDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#productTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var product = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'product/product.jetx'};
    getProductBySome(product); //根据条件查询产品
}
//跳转到指定页数
function doProductGoPage(){
    var productNum = $('#productNum').val(); //跳转到第几页
    if(productNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(productNum)){
        alert('请输入合法的数字！');
        return;
    }
    var currentPage = productNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var title = $("#title").val(); //题目
    var product = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'product/product.jetx'};
    getProductBySome(product); //根据条件查询产品
}
/*********** 产品End ***********/

/********** 产品投资Start **********/
//根据条件获取产品投资记录
function getProductInvestBySome(productInvestMes){
    $.ajax({
        type: "post",
        url: "/product/openInvestHistory",
        data: productInvestMes,
        dataType: "json",
        success: function (data) {
            if (data.result == 'success') {
                $('#nowPage').html(data.htmlText);
            } else {
                $('#nowPage').html(data.info);
            }
        },
        err: function (data) {
            console.log(data);
        }
    });
}
//首页
function ProductInvestStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var productId = $("#productId").val(); //产品id
    var startTime = $("#startTime").val(); //开始时间
    var endTime = $("#endTime").val(); //截止时间
    var productInvestMes = {productId:productId,startTime:startTime,endTime:endTime};
    getProductInvestBySome(productInvestMes);
}
//尾页
function ProductInvestEndPage(){
    var totalCount = $("#totalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var productId = $("#productId").val(); //产品id
    var startTime = $("#startTime").val(); //开始时间
    var endTime = $("#endTime").val(); //截止时间
    var productInvestMes = {productId:productId,startTime:startTime,endTime:endTime};
    getProductInvestBySome(productInvestMes);
}
//上一页
function ProductInvestUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }
    var pageSize = "10"; //每页显示数量
    var productId = $("#productId").val(); //产品id
    var startTime = $("#startTime").val(); //开始时间
    var endTime = $("#endTime").val(); //截止时间
    var productInvestMes = {productId:productId,startTime:startTime,endTime:endTime};
    getProductInvestBySome(productInvestMes);
}
//下一页
function ProductInvestDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#totalCount").val(); //总数量
    var pageSize = "10";

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }
    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var productId = $("#productId").val(); //产品id
    var startTime = $("#startTime").val(); //开始时间
    var endTime = $("#endTime").val(); //截止时间
    var productInvestMes = {productId:productId,startTime:startTime,endTime:endTime};
    getProductInvestBySome(productInvestMes);
}
//跳转到指定页数
function doProductInvestGoPage(){
    var productInvestNum = $('#productInvestNum').val(); //跳转到第几页
    if(productInvestNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(productInvestNum)){
        alert('请输入合法的数字！');
        return;
    }
    var currentPage = productInvestNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var productId = $("#productId").val(); //产品id
    var startTime = $("#startTime").val(); //开始时间
    var endTime = $("#endTime").val(); //截止时间
    var productInvestMes = {productId:productId,startTime:startTime,endTime:endTime};
    getProductInvestBySome(productInvestMes);
}
/*********** 产品投资End ***********/

/************************* 分页部分End *************************/
