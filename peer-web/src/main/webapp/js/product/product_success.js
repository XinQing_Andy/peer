

//按条件查询产品转让列表
function queryProductAttornSuccessInfo(){
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var shFlag = '0'; //审核状态(0-审核通过、1-审核未通过、2未审核)
    if(keyWordName == '' || typeof (keyWordName) == 'undefined'){
        alert('请选择债权类型！');
        return;
    }
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }
    var productAttornMes = {title:title,code:code,shFlag:shFlag,menuUrl:'product/product_debting.jetx'};

    $.ajax({
        type:"post",
        url: "/productAttorn/queryProductAttornBySort",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $('#tbodydebSuccess').html(data.attornStr);
        },
        err:function(data){
            console.log(data);
        }
    });
}


//获取审核成功的债权
function queryDebtSuccess(id,orderColumn){
    var content = $("#"+id).html(); //标签内容
    var orderType = 'ASC';
    var li = '';
    var shFlag = '0';

    for(var i=1;i<=4;i++){
        li = 'li'+i; //标签id
        if(li == id){
            $("#"+li).addClass('active');
            if(orderColumn != ''){
                if(content.indexOf("↑")>0){ //升序。将变为降序
                    content = content.replace("↑","↓");
                    orderType = "DESC";
                }else if(content.indexOf("↓")>0){ //降序，即将升序
                    content = content.replace("↓","↑");
                    orderType = "ASC";
                }
            }
            $("#"+li).html(content);
        }else{
            $("#"+li).removeClass('active');
        }
    }

    var productAttornMes = {orderColumn:orderColumn,orderType:orderType,shFlag:shFlag};

    $.ajax({
        type:"post",
        url: "/productAttorn/queryProductAttornBySort",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $('#tbodydebSuccess').html(data.attornStr);
        },
        err:function(data){
            console.log(data);
        }
    });
}


/******************** 分页部分Start ********************/
//按条件查询产品待转让列表
function queryProductAttornSuccessInfoByPage(productAttornMes){
    $.ajax({
        type:"post",
        url: "/productAttorn/queryProductAttornBySort",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $('#tbodydebSuccess').html(data.attornStr); //债权列表
            $("#currentPage2").html(data.currentPage); //当前页
        },
        err:function(data){
            console.log(data);
        }
    });
}
//首页
function ProductAttornStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }
    var productAttornMes = {title:title,code:code,shFlag:'0',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornSuccessInfoByPage(productAttornMes); //根据条件查询产品
}
//尾页
function ProductAttornEndPage(){
    var totalCount = $("#productAttornTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }
    var productAttornMes = {title:title,code:code,shFlag:'0',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornSuccessInfoByPage(productAttornMes); //根据条件查询产品
}
//上一页
function ProductAttornUp(){
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }
    var productAttornMes = {title:title,code:code,shFlag:'0',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornSuccessInfoByPage(productAttornMes); //根据条件查询产品
}
//下一页
function ProductAttornDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#productAttornTotalCount").val(); //总数量
    var pageSize = "10";
    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数

    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }

    var productAttornMes = {title:title,code:code,shFlag:'0',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornSuccessInfoByPage(productAttornMes); //根据条件查询产品
}
//跳转到指定页数
function doProductAttornGoPage(){
    var productAttornNum = $('#productAttornNum').val(); //跳转到第几页
    if(productAttornNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(productAttornNum)){
        alert('请输入合法的数字！');
        return;
    }
    var currentPage = productAttornNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }
    var productAttornMes = {title:title,code:code,shFlag:'0',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornSuccessInfoByPage(productAttornMes); //根据条件查询产品
}
/********************* 分页部分End *********************/