/**
 * Created by cuibin on 2016/4/29.
 */
/**
 * 删除 产品类别
 */
function deleteProductType(id){
    if(!confirm("确定要删除吗"))
        return;
    var temp = {id:id};
    $.ajax({
        type:"post",
        url: "/productType/deleteProductType",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                alert(data.info);
                openForecast("product/productType.jetx","/productType/queryProductTypeByPage");
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });

}
/**
 * 添加 产品类别
 */
function addProductType(){
    var id = $("#did").val();
    if(id != ""){
        return;
    }
    var typeName = $("#typeName").val();
    var reg = /^\s+$/gi;
    if(typeName == "" || reg.test(typeName)){
        alert("产品类别不能为空");
        return;
    }
    var temp = {typeName:typeName};
    $.ajax({
        type:"post",
        url: "/productType/addProductType",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                alert(data.info);
                openForecast("product/productType.jetx","/productType/queryProductTypeByPage");
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });

}
/**
 * 更新 产品类别
 */
function updateProductType(){
    var id = $("#did").val();
    if(id == ""){
        return;
    }
    var typeName = $("#typeName").val();
    var reg = /^\s+$/gi;
    if(typeName == "" || reg.test(typeName)){
        alert("产品类别不能为空");
        return;
    }
    var temp = {typeName:typeName,id:id};
    $.ajax({
        type:"post",
        url: "/productType/updateProductType",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                alert(data.info);
                openForecast("product/productType.jetx","/productType/queryProductTypeByPage");
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });

}
function showaddProductType(){
    $("#myModal").attr("style","display: block");
    $("#typeName").val("");
}
function closeaddProductType(){
    $("#myModal").attr("style","display: none");
}
function showUpdateProductType(id,name){
    $("#myModal").attr("style","display: block");
    $("#typeName").val(name);
    $("#did").val(id);
}


/***************  分页部分Start ***************/
//根据条件查询广告
function productgetBySome(advert){
    $.ajax({
        type:"post",
        url: "/productType/queryProductTypeByPage",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function productStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    productgetBySome(advert); //根据条件查询广告
}

//尾页
function productEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    productgetBySome(advert); //根据条件查询广告
}

//上一页
function productUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    productgetBySome(advert); //根据条件查询广告
}

//下一页
function productDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    productgetBySome(advert); //根据条件查询广告
}

//跳转到指定页数
function productdoGoPage(page){
    var title = $("#title").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    productgetBySome(advert); //根据条件查询广告
}

//按条件查询用户列表
function productqueryInfo(){
    var sysKeyWord = $("#title").val(); //查询关键字
    var sysUserMes = {title:sysKeyWord};
    $.ajax({
        type:"post",
        url: "/productType/queryProductTypeByPage",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
/*************** 分页部分End ***************/