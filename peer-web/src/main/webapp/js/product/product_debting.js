
//按条件查询产品待转让列表
function queryProductAttornInfo(){
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }

    var productAttornMes = {title:title,code:code,shFlag:'2',menuUrl:'product/product_debting.jetx'};

    $.ajax({
        type:"post",
        url: "/productAttorn/queryProductAttornBySort",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $('#tbodydebting').html(data.attornStr);
        },
        err:function(data){
            console.log(data);
        }
    });
}

//弹出债权转让审核窗口
function openProductAttornShFlag(id){
    $("#attornIdSh").val(id); //主键(债权转让)
    var bDiv = document.getElementById('productAttornSh');
    bDiv.style.display = "block";
}

//债权审核
function ProductAttornSh(shFlag){
    if(shFlag == "0"){
        var id = $("#attornIdSh").val(); //主键
        var shyj = $("#shyj").val(); //审核意见
        var huanXunTransferMes = {attornId:id};
        var url = ''; //调用地址

        if(shFlag == '0'){ //审核通过
            url = '/huanxun/transferAttornhx';
        }else{
            url = '/huanxun/unfreezeAttorn';
        }

        $.ajax({
            type:"post",
            url: url,
            data:huanXunTransferMes,
            dataType:"json",
            success:function(data){
                if(data.result == 'success'){
                    alert(data.info);
                    queryProductAttornInfo(); //查询债权转让
                }else{
                    alert(data.info);
                }
            },
            err:function(data){
                alert(data.info);
                console.log(data);
            }
        });
    }else if(shFlag == "1"){
        var id = $("#attornIdSh").val(); //主键
        var temp = {id:id,shFlag:shFlag};
        $.ajax({
            type:"post",
            url: "/huanxun/unfreezeAttorn",
            data:temp,
            dataType:"json",
            success:function(data){
                alert(data.info);
                queryProductAttornInfo(); //查询债权转让
            },
            err:function(data){
                console.log(data);
            }
        });
    }
}



//债权取消
function cancelProductAttorn(){
    var bDiv = document.getElementById('productAttornSh');
    bDiv.style.display = "none";
}

//获取待审核的债权
function queryDebting(id,orderColumn){
    var content = $("#"+id).html(); //标签内容
    var orderType = 'ASC';
    var li = '';
    var shFlag = '2';

    for(var i=1;i<=4;i++){
        li = 'li'+i; //标签id
        if(li == id){
            $("#"+li).addClass('active');
            if(orderColumn != ''){
                if(content.indexOf("↑")>0){ //升序。将变为降序
                    content = content.replace("↑","↓");
                    orderType = "DESC";
                }else if(content.indexOf("↓")>0){ //降序，即将升序
                    content = content.replace("↓","↑");
                    orderType = "ASC";
                }
            }
            $("#"+li).html(content);
        }else{
            $("#"+li).removeClass('active');
        }
    }

    var productAttornMes = {orderColumn:orderColumn,orderType:orderType,shFlag:shFlag};

    $.ajax({
        type:"post",
        url: "/productAttorn/queryProductAttornBySort",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $('#tbodydebting').html(data.attornStr);
        },
        err:function(data){
            console.log(data);
        }
    });
}

/******************** 获取分页Start ********************/

/********** 债权分页Start **********/
//按条件查询产品待转让列表
function queryProductAttornInfoByPage(productAttornMes){
    $.ajax({
        type:"post",
        url: "/productAttorn/queryProductAttornBySort",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $('#tbodydebting').html(data.attornStr); //债权列表
            $("#currentPage2").html(data.currentPage); //当前页
        },
        err:function(data){
            console.log(data);
        }
    });
}
//首页
function ProductWaitAttornStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }
    var productAttornMes = {title:title,code:code,shFlag:'2',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornInfoByPage(productAttornMes); //根据条件查询产品
}
//尾页
function ProductWaitAttornEndPage(){
    var totalCount = $("#productAttornTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }
    var productAttornMes = {title:title,code:code,shFlag:'2',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornInfoByPage(productAttornMes); //根据条件查询产品
}
//上一页
function ProductWaitAttornUp(){
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }
    var productAttornMes = {title:title,code:code,shFlag:'2',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornInfoByPage(productAttornMes); //根据条件查询产品
}
//下一页
function ProductWaitAttornDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#productAttornTotalCount").val(); //总数量
    var pageSize = "10";
    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }
    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数

    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }

    var productAttornMes = {title:title,code:code,shFlag:'2',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornInfoByPage(productAttornMes); //根据条件查询产品
}
//跳转到指定页数
function doProductWaitAttornGoPage(){
    var productAttornNum = $('#productAttornNum').val(); //跳转到第几页
    if(productAttornNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(productAttornNum)){
        alert('请输入合法的数字！');
        return;
    }
    var currentPage = productAttornNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var keyWordName = $("#keyWordName").val(); //查询关键字类型
    var keyWordValue = $("#keyWordValue").val(); //查询关键字名称
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != ''){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName = '1'){ //编码
            code = keyWordValue;
        }
    }
    var productAttornMes = {title:title,code:code,shFlag:'2',pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryProductAttornInfoByPage(product); //根据条件查询产品
}
/*********** 债权分页End ***********/


/********** 待承接债权分页Start **********/
//获取待承接债权
function queryAttornUnderTakedBySome(pageIndex,pageSize,currentPage){
    var keyWordName = $("#keyWordName").val(); //关键字
    var keyWordValue = $("#keyWordValue").val(); //关键值
    var title = ''; //题目
    var code = ''; //编码
    if(keyWordName != '' && typeof (keyWordName) != "undefined"){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName == '1'){ //编码
            code = keyWordValue;
        }
    }

    var productAttornMes = {title:title,code:code,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};

    $.ajax({
        type:"post",
        url: "/productAttorn/queryAttornUnderTakedBySome",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $('#tbodyAttornTaked').html(data.attornStr);
        },
        err:function(data){
            console.log(data);
        }
    });
}
//首页
function ProductAttornUnderTakedStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    queryAttornUnderTakedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
//尾页
function ProductAttornUnderTakedEndPage(){
    var totalCount = $("#totalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var productAttornMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    queryAttornUnderTakedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
//上一页
function ProductAttornUnderTakedUp(){
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    queryAttornUnderTakedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
//下一页
function ProductAttornUnderTakedDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#totalCount").val(); //总数量
    var pageSize = "10";
    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }
    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    alert('pageIndex=='+pageIndex+',pageSize=='+pageSize+',currentPage=='+currentPage);
    queryAttornUnderTakedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
//跳转到指定页数
function doProductAttornUnderTakedGoPage(){
    var productAttornUnderTakedNum = $('#productAttornUnderTakedNum').val(); //跳转到第几页
    if(productAttornUnderTakedNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(productAttornUnderTakedNum)){
        alert('请输入合法的数字！');
        return;
    }
    var currentPage = productAttornUnderTakedNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    queryAttornUnderTakedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
/*********** 待承接债权分页End ***********/

/********** 失败的债权分页Start **********/
//获取失败的债权
function queryAttornFailedBySome(pageIndex,pageSize,currentPage){
    var keyWordName = $("#keyWordName").val(); //关键字
    var keyWordValue = $("#keyWordValue").val(); //关键值
    var title = ''; //题目
    var code = ''; //编码
    var shFlag = '1'; //审核状态(0-审核通过、1-审核不通过、2-未审核)
    if(keyWordName != '' && typeof (keyWordName) != "undefined"){
        if(keyWordName == '0'){ //标题
            title = keyWordValue;
        }else if(keyWordName == '1'){ //编码
            code = keyWordValue;
        }
    }

    var productAttornMes = {title:title,code:code,shFlag:shFlag,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};

    $.ajax({
        type:"post",
        url: "/productAttorn/queryAttornFailBySome",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $('#tbodyAttornFail').html(data.attornStr);
        },
        err:function(data){
            console.log(data);
        }
    });
}
//首页
function ProductAttornFailStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    queryAttornFailedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
//尾页
function ProductAttornFailEndPage(){
    var totalCount = $("#totalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    queryAttornFailedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
//上一页
function ProductAttornFailUp(){
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    queryAttornFailedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
//下一页
function ProductAttornFailDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage2").html(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#totalCount").val(); //总数量
    var pageSize = "10";
    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }
    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    queryAttornFailedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
//跳转到指定页数
function doProductAttornFailGoPage(){
    var productAttornFailNum = $('#productAttornFailNum').val(); //跳转到第几页
    if(productAttornFailNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(productAttornFailNum)){
        alert('请输入合法的数字！');
        return;
    }
    var currentPage = productAttornFailNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    queryAttornFailedBySome(pageIndex,pageSize,currentPage); //根据条件查询产品
}
/*********** 失败的债权分页End ***********/

/********************* 获取分页End *********************/

