(function($){
	$.fn.extend({
        listMultiplePlugin: function (options) {
            var rams = {
                animatePadding: 10,
                defaultinput:'defaultinput',
                one_input:'one_input',
                two_input:'two_input',
				pageIndex:'1',
				pageSize:'3',
                trbutton:'trbutton',
                hoverColor: "Black",
                checkboxObject:"morecbox",
                label:"tbody,tr,td,table,input",
                propertiesValues:"name|type|hidden|checkbox|text|button|id|ref|value|checked|action|",
                mark:":|,|~|!|@|#|$|%|^|&|*|(|)|_|+|-|=|;|'|[|]|{|}",
                url:"",
                adata:"",
                urljetx:"",
                selectjson:"",
                indiv:"",
                listMultipleObject:Object,
                splitArr:function(){
                	rams.label=rams.label.split(',');
                	rams.propertiesValues=rams.propertiesValues.split('|');
                	rams.mark=rams.mark.split('|');
                },
				stextlinks:function(el)
                {
                	rams.selectjson="";
                	el.children("tr").each(function(indextr, eltr) {
                		if(indextr!=0)
                		{
                			var sbutton="";
                			$(eltr).children("td").each(function(index, el) {
                			
	                			if(index!=0)
	                			{
	                				if($(el).children().attr("class")==rams.one_input)
	                				{
	                					sbutton+=","+$(el).children().attr("ref");
	                				}
	                			}
                			});
                			$("#"+rams.trbutton+indextr).val(sbutton.substring(1));
                			rams.selectjson+=",\""+rams.trbutton+indextr+"\":\""+sbutton.substring(1)+"\"";
                		}	
                	});

					$("#selectjson").val(rams.selectjson.substring(1));
					$("#urljetx").val(rams.urljetx);

					$.ajax({
					        type:"post",
					        url: rams.url,
					        data:$.parseJSON("{"+rams.selectjson.substring(1)+","+rams.urljetx+"}"),
					        dataType:"json",
					        success:function(data){
					            if(data.result == 'success'){
					                $(rams.indiv).html(data.htmlText);
					            }else{
					                alert('查询失败！');
					            }
					        },
					        err:function(data){
					            console.log(data);
					        }
					    });
                }

            };

            var options = $.extend(rams, options);

            return this.each(function () {
            	rams.splitArr();
            	rams.listMultipleObject=$(this);
            	rams.listMultipleObject.children(rams.label[0]).children(rams.label[1]).each(function(index, el) {
            		if(index==0)
            		{
						$(this).find("input[type=checkbox]").change(function(event) {

								$(this).val()==0?$(this).val(1):$(this).val(0);
								// rams.listMultipleObject.children(rams.label[0]).find('input[type=button]').each(function(index, el) {
								// 	$(this).attr(rams.propertiesValues[7])==rams.defaultinput?$(this).attr('class',rams.one_input):$(this).attr('class',rams.two_input);
								// });
						});
            		}
            		else
            		{
            		   $("input[ref="+rams.checkboxObject+"]").parent().append('<input type="hidden" value="" id="'+rams.trbutton+index+'" name="'+rams.trbutton+index+'">');
            		   	$(el).each(function(indextr, eltr) {
            		   		if(index!=0)
            		   		{
	            		   		$(eltr).children().click(function(event) {
	            		   			if($("input[ref="+rams.checkboxObject+"]").val()==0)//单选
	            		   			{
										if($(this).children().attr("class")==rams.two_input)
										{
											$(el).children().each(function(indexinput, elinput) {
												indexinput!=0&$(elinput).children().attr("class",rams.two_input);
											});
											$(this).children().attr('class',rams.one_input);
											rams.stextlinks(rams.listMultipleObject.children(rams.label[0]));
										}
										else
										{
											$(el).children().each(function(indexinput, elinput) {
												(indexinput!=0)&indexinput==1?$(elinput).children().attr("class",rams.one_input):
												$(elinput).children().attr("class",rams.two_input)
											});
											rams.stextlinks(rams.listMultipleObject.children(rams.label[0]));
										}
	            		   			}
	            		   			else
	            		   			{
											if($(this).children().attr('ref')==rams.defaultinput)
											{
												$(el).children().each(function(indexinput, elinput) {
													indexinput!=0&indexinput==1?$(elinput).children().attr("class",rams.one_input):$(elinput).children().attr("class",rams.two_input);
												});
												rams.stextlinks(rams.listMultipleObject.children(rams.label[0]));
											}
											else
											{
												if($(this).children().attr('class')==rams.one_input)
												{
													$(this).children().attr('class',rams.two_input);
													var i=0;
													$(el).children().each(function(indexinput, elinput) {
														if(indexinput!=0&&indexinput!=1)
														{
															if($(elinput).children().attr("class")==rams.one_input)
															{
																i++;
															}
														}
													});
													if(i==0)
													{
														$(el).children().each(function(indexinput, elinput) {
															indexinput!=0&indexinput==1?$(elinput).children().attr("class",rams.one_input):$(elinput).children().attr("class",rams.two_input);
														});
													}	
												}
												else
												{
													$(this).children().attr('class',rams.one_input);
													$(el).children().each(function(indexinput, elinput) {
															if(indexinput==1){$(elinput).children().attr("class",rams.two_input)};
														});
												}

												rams.stextlinks(rams.listMultipleObject.children(rams.label[0]));
											}
	            		   			}
	            		   		});
            		   		}
            		   	});
            		}
            	});
            });
        }
    });
})(jQuery);