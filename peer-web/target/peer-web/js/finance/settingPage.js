/**
 * Created by Cuibin on 2016/4/12.
 */
/*************** 按会员等级费用维护 分页部分Start ***************/
//根据条件查询广告
function getBySome(advert){
    $.ajax({
        type:"post",
        url: "/finance/openMemberFeeLevel",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function StartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBySome(advert); //根据条件查询广告
}

//尾页
function EndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBySome(advert); //根据条件查询广告
}

//上一页
function Up(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBySome(advert); //根据条件查询广告
}

//下一页
function Down(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBySome(advert); //根据条件查询广告
}

//跳转到指定页数
function doGoPage(page){
    var title = $("#title").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getBySome(advert); //根据条件查询广告
}

//按条件查询用户列表
function queryInfo(){
    var sysKeyWord = $("#title").val(); //查询关键字
    var sysUserMes = {title:sysKeyWord};
    $.ajax({
        type:"post",
        url: "/finance/openMemberFeeLevel",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
/*************** 分页部分End ***************/

/*************** 会员费用关联 分页部分Start ***************/
//根据条件查询广告
function getassBySome(advert){
    $.ajax({
        type:"post",
        url: "/finance/queryAllMemberFeeRelation",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function assStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getassBySome(advert); //根据条件查询广告
}

//尾页
function assEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getassBySome(advert); //根据条件查询广告
}

//上一页
function assUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getassBySome(advert); //根据条件查询广告
}

//下一页
function assDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getassBySome(advert); //根据条件查询广告
}

//按条件查询用户列表
function assqueryInfo(){
    var sysKeyWord = $("#title").val(); //查询关键字
    var sysUserMes = {title:sysKeyWord};
    $.ajax({
        type:"post",
        url: "/finance/queryAllMemberFeeRelation",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//跳转到指定页数
function doGoassPage(page){
    var title = $("#title").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getdaBySome(advert); //根据条件查询广告
}
/*************** 分页部分End ***************/
/*************** 大字典 分页部分Start ***************/
//根据条件查询广告
function getdaBySome(advert){
    $.ajax({
        type:"post",
        url: "/finance/queryAllDictionary",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function daStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getdaBySome(advert); //根据条件查询广告
}

//尾页
function daEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getdaBySome(advert); //根据条件查询广告
}

//上一页
function daUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getdaBySome(advert); //根据条件查询广告
}

//下一页
function daDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getdaBySome(advert); //根据条件查询广告
}

//按条件查询用户列表
function daqueryInfo(){
    var sysKeyWord = $("#title").val(); //查询关键字
    var sysUserMes = {title:sysKeyWord};
    $.ajax({
        type:"post",
        url: "/finance/queryAllDictionary",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//跳转到指定页数
function doGodaPage(page){
    var title = $("#title").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getdaBySome(advert); //根据条件查询广告
}
/*************** 分页部分End ***************/
/*************** 会员等级维护 分页部分Start ***************/
//根据条件查询广告
function getvipBySome(advert){
    $.ajax({
        type:"post",
        url: "/finance/queryAllVipMaintain",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function vipStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getvipBySome(advert); //根据条件查询广告
}

//尾页
function vipEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getvipBySome(advert); //根据条件查询广告
}

//上一页
function vipUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getvipBySome(advert); //根据条件查询广告
}

//下一页
function vipDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getvipBySome(advert); //根据条件查询广告
}

//按条件查询用户列表
function vipqueryInfo(){
    var sysKeyWord = $("#title").val(); //查询关键字
    var sysUserMes = {title:sysKeyWord};
    $.ajax({
        type:"post",
        url: "/finance/queryAllVipMaintain",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//跳转到指定页数
function doGovipPage(page){
    var title = $("#title").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getvipBySome(advert); //根据条件查询广告
}
/*************** 分页部分End ***************/
/*************** 取号原则 分页部分Start ***************/
//根据条件查询广告
function getnoBySome(advert){
    $.ajax({
        type:"post",
        url: "/finance/queryAllTakeNo",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function noStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}

//尾页
function noEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}

//上一页
function noUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}

//下一页
function noDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}

//按条件查询用户列表
function noqueryInfo(){
    var sysKeyWord = $("#title").val(); //查询关键字
    var sysUserMes = {title:sysKeyWord};
    $.ajax({
        type:"post",
        url: "/finance/queryAllTakeNo",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//跳转到指定页数
function doGonoPage(page){
    var title = $("#title").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}
/*************** 分页部分End ***************/
/*************** 取号原则 分页部分Start ***************/
//根据条件查询广告
function getnoBySome(advert){
    $.ajax({
        type:"post",
        url: "/finance/queryAllTakeNo",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function noStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}

//尾页
function noEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}

//上一页
function noUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}

//下一页
function noDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}

//按条件查询用户列表
function noqueryInfo(){
    var sysKeyWord = $("#title").val(); //查询关键字
    var sysUserMes = {title:sysKeyWord};
    $.ajax({
        type:"post",
        url: "/finance/queryAllTakeNo",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//跳转到指定页数
function doGonoPage(page){
    var title = $("#title").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getnoBySome(advert); //根据条件查询广告
}
/*************** 分页部分End ***************/
/*************** 固定费用过账表 分页部分Start ***************/
//根据条件查询广告
function getpostBySome(advert){
    $.ajax({
        type:"post",
        url: "/finance/queryAllSysManageAmt",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function postStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getpostBySome(advert); //根据条件查询广告
}

//尾页
function postEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getpostBySome(advert); //根据条件查询广告
}

//上一页
function postUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getpostBySome(advert); //根据条件查询广告
}

//下一页
function postDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getpostBySome(advert); //根据条件查询广告
}

//按条件查询用户列表
function postqueryInfo(){
    var sysKeyWord = $("#title").val(); //查询关键字
    var sysUserMes = {title:sysKeyWord};
    $.ajax({
        type:"post",
        url: "/finance/queryAllSysManageAmt",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//跳转到指定页数
function doGopostPage(page){
    var title = $("#title").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getpostBySome(advert); //根据条件查询广告
}
/*************** 分页部分End ***************/