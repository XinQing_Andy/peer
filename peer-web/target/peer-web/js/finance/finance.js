/**
 * Created by Cuibin on 2016/4/12.
 */
/**
 * 按会员等级费用维护 开启关闭添加窗口
 */
function openAddMflWin(){
    $("#myModal").attr("style","display: block");
    $("#did").val("");
    $("#feiName").val("");
    $("#isStart").val("");
}
function closeAddMflWin(){
    $("#myModal").attr("style","display: none");
}
/**
 * 提交 按会员等级费用维护
 */
function submitAddMfl(){
    var id = $("#did").val();
    if(id != ""){
        return;
    }
    var feiName = $("#feiName").val();
    var isStart = $("#isStart").val();
    var reg = /^\s+$/gi;
    if(feiName == "" || reg.test(feiName)){
        alert("费用名称不能为空");
        return;
    }
    if(isStart == "" || isStart == null){
        alert("请选择是否启用");
        return;
    }
    var temp = {feeName:feiName,activeFlg:isStart};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/addMemberFeeLevel",
        data: temp2,
        dataType: "json",
        success: function (data) {
            closeAddMflWin();
            openForecast("/system/system_spend.jetx","/finance/openMemberFeeLevel");
        },
        err: function (data) {
        }
    });
}
/**
 * 打开编辑窗口 按会员等级费用维护
 */
function queryOnlyMemberFeeLevel(dcId){
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/queryOnlyMemberFeeLevel",
        data: temp2,
        dataType: "json",
        success: function (data) {
            $("#did").val(data.id);
            $("#feiName").val(data.feeName);
            $("#isStart").val(data.activeFlg);
            if(data.activeFlg == "是"){
                $("#activeFlg option[value='是']").attr("selected",true);
                $("#activeFlg option[value='否']").attr("selected",false);
            }else{
                $("#activeFlg option[value='否']").attr("selected",true);
                $("#activeFlg option[value='是']").attr("selected",false);
            }
            $("#myModal").attr("style","display: block");
        },
        err: function (data) {
        }
    });
}
/**
 * 提交修改 按会员等级费用维护
 */
function updateMemberFeeLevel(){
    var id = $("#did").val();
    if(id == ""){
        return;
    }
    var feeName = $("#feiName").val();
    var activeFlg = $("#isStart").val();
    var reg = /^\s+$/gi;
    if(feeName == "" || reg.test(feeName)){
        alert("费用名称不能为空");
        return;
    }
    if(activeFlg == "" || activeFlg == null){
        alert("请选择是否启用");
        return;
    }
    var temp = {id:id,feeName:feeName,activeFlg:activeFlg};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/updateMemberFeeLevel",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_spend.jetx","/finance/openMemberFeeLevel");
        },
        err: function (data) {
        }
    });
}
/**
 * 删除 按会员等级费用维护
 * @param dcId
 */
function deleteMemberFeeLevel(dcId){
    if(!confirm("确认删除吗?")){
        return;
    }
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/deleteMemberFeeLevel",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_spend.jetx","/finance/openMemberFeeLevel");
        },
        err: function (data) {
        }
    });
}
/** 打开关闭大字典**/
function opendictionary(){
    $("#myModal").attr("style","display: block");
    $("#did").val("");
    $("#groupType").val("");
    $("#memberId").val("");
    $("#memberName").val("");
    $("#activeFlg").val("");
    $("#remark").val("");
}
function closedictionary(){
    $("#myModal").attr("style","display: none");
}
/**
 * 提交 大字典
 */
function submitdic(){
    var id = $("#did").val();
    if(id != ""){
        return;
    }
    var groupType = $("#groupType").val();
    var memberId = $("#memberId").val();
    var memberName = $("#memberName").val();
    var activeFlg = $("#activeFlg").val();
    var remark = $("#remark").val();
    var reg = /^\s+$/gi;
    if(groupType == "" || reg.test(groupType)){
        alert("组类别不能为空");
        return;
    }
    if(memberId == "" || reg.test(memberId)){
        alert("每个组内的成员id不能为空");
        return;
    }
    if(memberName == "" || reg.test(memberName)){
        alert("每个组内的成员名称不能为空");
        return;
    }
    if(activeFlg == "" || activeFlg == null){
        alert("请选择是否启用");
        return;
    }
    if(reg.test(remark)){
        alert("备注不能为纯空格");
        return;
    }
    var temp = {groupType:groupType,memberId:memberId,memberName:memberName,activeFlg:activeFlg,remark:remark};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/addDictionary",
        data: temp2,
        dataType: "json",
        success: function (data) {
            closedictionary();
            openForecast("/system/system_dictionary.jetx","/finance/queryAllDictionary");
        },
        err: function (data) {
        }
    });
}

/**
 * 删除大字典
 * @param dcId
 */
function deleteDc(dcId){
    if(!confirm("确认删除吗?")){
        return;
    }
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/deleteDictionary",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_dictionary.jetx","/finance/queryAllDictionary");
        },
        err: function (data) {
        }
    });
}
/**
 * 打开 编辑大字典窗口
 * @param dcId
 */
function openDcUpdate(dcId){
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    $.ajax({
        type: "post",
        url: "/finance/queryOnlyDictionary",
        data: temp2,
        dataType: "json",
        success: function (data) {
            $("#did").val(data.id);
            $("#groupType").val(data.groupType);
            $("#memberId").val(data.memberId);
            $("#memberName").val(data.memberName);
            if(data.activeFlg == "是"){
                $("#activeFlg option[value='是']").attr("selected",true);
                $("#activeFlg option[value='否']").attr("selected",false);
            }else{
                $("#activeFlg option[value='否']").attr("selected",true);
                $("#activeFlg option[value='是']").attr("selected",false);
            }
            $("#remark").val(data.remark);
            $("#myModal").attr("style","display: block");
        },
        err: function (data) {
        }
    });
}
/**
 *大字典 ：验证输入
 *
 */
function removeInput(val){
    var ver = /^[0-9]*$/;
    if(!ver.test(val)){
        $("#memberId").val("");
    }
}
/**
 *  更新大字典
 */
function updateDictionary(){
    var id = $("#did").val();
    if(id == ""){
        return;
    }
    var groupType = $("#groupType").val();
    var memberId = $("#memberId").val();
    var memberName = $("#memberName").val();
    var activeFlg = $("#activeFlg").val();
    var remark = $("#remark").val();
    var reg = /^\s+$/gi;
    if(groupType == "" || reg.test(groupType)){
        alert("组类别不能为空");
        return;
    }
    if(memberId == "" || reg.test(memberId)){
        alert("每个组内的成员id不能为空");
        return;
    }
    if(memberName == "" || reg.test(memberName)){
        alert("每个组内的成员名称不能为空");
        return;
    }
    if(activeFlg == "" || activeFlg == null){
        alert("请选择是否启用");
        return;
    }
    if(reg.test(remark)){
        alert("备注不能为纯空格");
        return;
    }
    var temp = {id:id,groupType:groupType,memberId:memberId,memberName:memberName,activeFlg:activeFlg,remark:remark};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/updateDictionary",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_dictionary.jetx","/finance/queryAllDictionary");
        },
        err: function (data) {
        }
    });
}
/**
 *  打开 会员费用关联 窗口
 */
function openAddMemWindow(){
    $("#myModal").attr("style","display: block");
    $("#levelProportion").val("");
    $("#feeId").val("");
    $("#levelId").val("");
}
function closeAddMemWindow(){
    $("#myModal").attr("style","display: none");
}
/**
 *提交 会员费用关联
 */
function submitMem(){
    var id = $("#did").val();
    if(id != ""){
        return;
    }
    var levelProportion = $("#levelProportion").val();
    var feeId = $("#feeId").val();
    var levelId = $("#levelId").val();
    var reg = /^\s+$/gi;
    if(levelId == "" || levelId == null){
        alert("会员等级id不能为空");
        return;
    }
    if(feeId == "" || feeId == null){
        alert("费用id不能为空");
        return;
    }
    if(levelProportion == "" || reg.test(levelProportion)){
        alert("比例不能为空");
        return;
    }
    var temp = {levelProportion:levelProportion,feeId:feeId,levelId:levelId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/addMemberFeeRelation",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_association.jetx","/finance/queryAllMemberFeeRelation");
        },
        err: function (data) {
        }
    });
}
/**
 *打开 会员费用关联
 * @param dcId
 */
function queryOnlyMemberFeeRelation(dcId){
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/queryOnlyMemberFeeRelation",
        data: temp2,
        dataType: "json",
        success: function (data) {
            $("#did").val(data.id);
            $("#feeId").val(data.feeId);
            $("#levelId").val(data.levelId);
            $("#levelProportion").val(data.levelProportion);
            $("#myModal").attr("style","display: block");
        },
        err: function (data) {
        }
    });
}
/**
 *  更新会员费用关联
 */
function updateMemberFeeRelation(){
    var id = $("#did").val();
    if(id == ""){
        return;
    }
    var feeId = $("#feeId").val();
    var levelId = $("#levelId").val();
    var levelProportion = $("#levelProportion").val();
    var reg = /^\s+$/gi;
    if(levelId == "" || levelId == null){
        alert("会员等级id不能为空");
        return;
    }
    if(feeId == "" || feeId == null){
        alert("费用id不能为空");
        return;
    }
    if(levelProportion == "" || reg.test(levelProportion)){
        alert("比例不能为空");
        return;
    }
    var temp = {id:id,feeId:feeId,levelId:levelId,levelProportion:levelProportion};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/updateMemberFeeRelation",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_association.jetx","/finance/queryAllMemberFeeRelation");
        },
        err: function (data) {
        }
    });
}
/**
 * 会员费用关联 删除
 */
function deleteMemberFeeRelation(dcId){
    if(!confirm("确认删除吗?")){
        return;
    }
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/deleteMemberFeeRelation",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_association.jetx","/finance/queryAllMemberFeeRelation");
        },
        err: function (data) {
        }
    });
}
/**
 *  验证输入的比例
 */
function Checkbili() {

    $("#levelProportion").css("ime-mode", "disabled");
    $("#levelProportion").bind("keypress", function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (!$.browser.msie && (e.keyCode == 0x8))
        {
            return;
        }
        return code >= 48 && code <= 57 || code == 46;
    });
    $("#levelProportion").bind("blur", function () {
        if (this.value.lastIndexOf(".") == (this.value.length - 1)) {
            this.value = this.value.substr(0, this.value.length - 1);
        } else if (isNaN(this.value)) {
            this.value = " ";
        }
    });
    $("#levelProportion").bind("paste", function () {
        var s = clipboardData.getData('text');
        if (!/\D/.test(s));
        value = s.replace(/^0*/, '');
        return false;
    });
    $("#levelProportion").bind("dragenter", function () {
        return false;
    });
    $("#levelProportion").bind("keyup", function () {
        this.value = this.value.replace(/[^\d.]/g, "");
        //必须保证第一个为数字而不是.
        this.value = this.value.replace(/^\./g, "");
        //保证只有出现一个.而没有多个.
        this.value = this.value.replace(/\.{2,}/g, ".");
        //保证.只出现一次，而不能出现两次以上
        this.value = this.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
    });
}

/**
 *  保存固定费用过账表
 */
function addSysManageAmt(){
    var id = $("#did").val();
    if(id != ""){
        return;
    }
    var feeName = $("#feeName").val();
    var feeAmt = $("#feeAmt").val();
    var activeFlg = $("#activeFlg").val();
    var reg = /^\s+$/gi;
    if(feeName == "" || reg.test(feeName)){
        alert("费用名称不能为空");
        return;
    }
    if(feeAmt == "" || reg.test(feeAmt)){
        alert("费用金额不能为空");
        return;
    }
    if(activeFlg == "" || activeFlg == null){
        alert("请选择是否启用");
        return;
    }
    var temp = {feeName:feeName,feeAmt:feeAmt,activeFlg:activeFlg};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/addSysManageAmt",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("system/system_post.jetx","/finance/queryAllSysManageAmt");
        },
        err: function (data) {
        }
    });
}
/**
 *打开固定费用过账表窗口
 * @param dcId
 */
function openSysManageAmtUpdate(dcId){
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/queryOnlySysManageAmt",
        data: temp2,
        dataType: "json",
        success: function (data) {
            $("#did").val(data.id);
            $("#feeName").val(data.feeName);
            $("#feeAmt").val(data.feeAmt);
            if(data.activeFlg == "是"){
                $("#activeFlg option[value='是']").attr("selected",true);
                $("#activeFlg option[value='否']").attr("selected",false);
            }else{
                $("#activeFlg option[value='否']").attr("selected",true);
                $("#activeFlg option[value='是']").attr("selected",false);
            }
            $("#myModal").attr("style","display: block");
        },
        err: function (data) {
        }
    });
}
/**
 * 修改固定费用过账
 */
function updateSysManageAmt(){
    var id = $("#did").val();
    if(id == ""){
        return;
    }
    var feeName = $("#feeName").val();
    var feeAmt = $("#feeAmt").val();
    var activeFlg = $("#activeFlg").val();
    var reg = /^\s+$/gi;
    if(feeName == "" || reg.test(feeName)){
        alert("费用名称不能为空");
        return;
    }
    if(feeAmt == "" || reg.test(feeAmt)){
        alert("费用金额不能为空");
        return;
    }
    if(activeFlg == "" || activeFlg == null){
        alert("请选择是否启用");
        return;
    }
    var temp = {id:id,feeName:feeName,feeAmt:feeAmt,activeFlg:activeFlg};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/updateSysManageAmt",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("system/system_post.jetx","/finance/queryAllSysManageAmt");
        },
        err: function (data) {
        }
    });
}
/**
 *
 */
function openSysManageAmtWin(){
    $("#myModal").attr("style","display: block");
    $("#did").val("");
    $("#feeName").val("");
    $("#feeAmt").val("");
    $("#activeFlg").val("");
}
function closeSysManageAmtWin(){
    $("#myModal").attr("style","display: none");
}
/**
 * 固定费用过账表 删除
 */
function deleteSysManageAmt(dcId){
    if(!confirm("确认删除吗?")){
        return;
    }
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/deleteSysManageAmt",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("system/system_post.jetx","/finance/queryAllSysManageAmt");
        },
        err: function (data) {
        }
    });
}

/**
 *  打开关闭 vip等级维护 添加窗口
 */
function openMemberLevelWin(){
    $("#myModal").attr("style","display: block");
    $("#did").val("");
    $("#levelName").val("");
    $("#levelRequest").val("");
}
function closeMemberLevelWin(){
    $("#myModal").attr("style","display: none");
}
/**
 *  打开 vip等级维护 修改
 */
function openMemberLevelUpdate(dcId){
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/queryOnlyVipMaintain",
        data: temp2,
        dataType: "json",
        success: function (data) {
            $("#did").val(data.id);
            $("#levelName").val(data.levelName);
            $("#levelRequest").val(data.levelRequest);
            $("#myModal").attr("style","display: block");
        },
        err: function (data) {
        }
    });
}
/**
 *  更新vip等级维护
 */
function updateVipMaintain(){
    var id = $("#did").val();
    if(id == ""){
        return;
    }
    var levelName = $("#levelName").val();
    var levelRequest = $("#levelRequest").val();
    var reg = /^\s+$/gi;
    if(levelName == "" || reg.test(levelName)){
        alert("等级名称不能为空");
        return;
    }
    if(levelRequest == "" || reg.test(levelRequest)){
        alert("等级最低要求不能为空");
        return;
    }
    var temp = {id:id,levelName:levelName,levelRequest:levelRequest};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/updateVipMaintain",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_member.jetx","/finance/queryAllVipMaintain");
        },
        err: function (data) {
        }
    });
}
/**
 *  保存vip等级维护
 */
function addVipMaintain(){
    var id = $("#did").val();
    if(id != ""){
        return;
    }
    var levelName = $("#levelName").val();
    var levelRequest = $("#levelRequest").val();
    var reg = /^\s+$/gi;
    if(levelName == "" || reg.test(levelName)){
        alert("等级名称不能为空");
        return;
    }
    if(levelRequest == "" || reg.test(levelRequest)){
        alert("等级最低要求不能为空");
        return;
    }
    var temp = {levelName:levelName,levelRequest:levelRequest};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/addVipMaintain",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_member.jetx","/finance/queryAllVipMaintain");
        },
        err: function (data) {
        }
    });
}
/**
 * vip等级维护 删除
 */
function deleteVipMaintain(dcId){
    if(!confirm("确认删除吗?")){
        return;
    }
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/deleteVipMaintain",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("/system/system_member.jetx","/finance/queryAllVipMaintain");
        },
        err: function (data) {
        }
    });
}
/**
 *  打开 取号原则 窗口
 */
function openTakeNowin(){
    $("#myModal").attr("style","display: block");
    $("#did").val("");
    $("#codeType").val("");
    $("#statusMethod").val("");
    $("#currNo").val("");
    $("#noLength").val("");
    $("#startNo").val("");
    $("#maxNo").val("");
    $("#noFormat").val("");
    $("#feeAmt").val("");
    $("#activeFlg").val("");
    $("#noDesc").val("");
}
function closeTakeNowin(){
    $("#myModal").attr("style","display: none");
}
/**
 * 保存 取号原则
 */
function addTakeNo(){
    var id = $("#did").val();
    if(id != ""){
        return;
    }
    var codeType = $("#codeType").val();
    var statusMethod = $("#statusMethod").val();
    var currNo = $("#currNo").val();
    var noLength = $("#noLength").val();
    var startNo = $("#startNo").val();
    var maxNo = $("#maxNo").val();
    var noFormat = $("#noFormat").val();
    var feeAmt = $("#feeAmt").val();
    var activeFlg = $("#activeFlg").val();
    var noDesc = $("#noDesc").val();
    var reg = /^\s+$/gi;
    if(codeType == "" || reg.test(codeType)){
        alert("取号类别不能为空");
        return;
    }
    if(statusMethod == "" || reg.test(statusMethod)){
        alert("循环原则不能为空");
        return;
    }
    if(currNo == "" || reg.test(currNo)){
        alert("当前号码不能为空");
        return;
    }
    if(noLength == "" || reg.test(noLength)){
        alert("长度不能为空");
        return;
    }
    if(startNo == "" || reg.test(startNo)){
        alert("开始号码不能为空");
        return;
    }
    if(maxNo == "" || reg.test(maxNo)){
        alert("最大号码不能为空");
        return;
    }
    if(noDesc == "" || reg.test(noDesc)){
        alert("备注不能为空");
        return;
    }
    if(noFormat == "" || noFormat == null){
        alert("请选择格式");
        return;
    }
    var temp = {codeType:codeType,statusMethod:statusMethod,currNo:currNo,noLength:noLength,
        startNo:startNo,maxNo:maxNo,noFormat:noFormat,feeAmt:feeAmt,activeFlg:activeFlg,noDesc:noDesc};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/addTakeNo",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("system/system_number.jetx","/finance/queryAllTakeNo");
        },
        err: function (data) {
        }
    });
}
/**
 *  更新取号原则
 */
function updateTakeNo(){
    var id = $("#did").val();
    if(id == ""){
        return;
    }
    var codeType = $("#codeType").val();
    var statusMethod = $("#statusMethod").val();
    var currNo = $("#currNo").val();
    var noLength = $("#noLength").val();
    var startNo = $("#startNo").val();
    var maxNo = $("#maxNo").val();
    var noFormat = $("#noFormat").val();
    var feeAmt = $("#feeAmt").val();
    var activeFlg = $("#activeFlg").val();
    var noDesc = $("#noDesc").val();
    var reg = /^\s+$/gi;
    if(codeType == "" || reg.test(codeType)){
        alert("取号类别不能为空");
        return;
    }
    if(statusMethod == "" || reg.test(statusMethod)){
        alert("循环原则不能为空");
        return;
    }
    if(currNo == "" || reg.test(currNo)){
        alert("当前号码不能为空");
        return;
    }
    if(noLength == "" || reg.test(noLength)){
        alert("长度不能为空");
        return;
    }
    if(startNo == "" || reg.test(startNo)){
        alert("开始号码不能为空");
        return;
    }
    if(maxNo == "" || reg.test(maxNo)){
        alert("最大号码不能为空");
        return;
    }
    if(noDesc == "" || reg.test(noDesc)){
        alert("备注不能为空");
        return;
    }
    if(noFormat == "" || noFormat == null){
        alert("请选择格式");
        return;
    }
    var temp = {id:id,codeType:codeType,statusMethod:statusMethod,currNo:currNo,noLength:noLength,
        startNo:startNo,maxNo:maxNo,noFormat:noFormat,feeAmt:feeAmt,activeFlg:activeFlg,noDesc:noDesc};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/updateTakeNo",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("system/system_number.jetx","/finance/queryAllTakeNo");
        },
        err: function (data) {
        }
    });
}
/**
 *  打开 会员费用关联表 修改
 */
function queryOnlyTakeNo(dcId){
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/queryOnlyTakeNo",
        data: temp2,
        dataType: "json",
        success: function (data) {
            $("#did").val(data.id);
            $("#codeType").val(data.codeType);
            $("#statusMethod").val(data.statusMethod);
            $("#currNo").val(data.currNo);
            $("#noLength").val(data.noLength);
            $("#startNo").val(data.startNo);
            $("#maxNo").val(data.maxNo);
            if(data.noFormat == "1"){
                $("#noFormat option[value='1']").attr("selected",true);
                $("#noFormat option[value='0']").attr("selected",false);
            }else{
                $("#noFormat option[value='0']").attr("selected",true);
                $("#noFormat option[value='1']").attr("selected",false);
            }
            $("#feeAmt").val(data.feeAmt);
            $("#activeFlg").val(data.activeFlg);
            $("#noDesc").val(data.noDesc);
            $("#myModal").attr("style","display: block");

        },
        err: function (data) {
        }
    });
}
/**
 * 取号原则 删除
 */
function deleteTakeNo(dcId){
    if(!confirm("确认删除吗?")){
        return;
    }
    var temp = {id:dcId};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/finance/deleteTakeNo",
        data: temp2,
        dataType: "json",
        success: function (data) {
            openForecast("system/system_number.jetx","/finance/queryAllTakeNo");
        },
        err: function (data) {
        }
    });
}
