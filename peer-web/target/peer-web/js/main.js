/**
 * Created by WangMeng on 2016/1/13.
 */
var LastLeftID = "";

$(document).ready(function(){
    var $userInfo = $("#userinfo");
    var userId = $userInfo.attr("name"); //用户id
    var menu = {userId:userId,menuUrl:'include/menu.jetx'};
    var userMenu = $.param(menu);
    $.ajax({
        type: "post",
        url: "/manage/menu",
        data: userMenu,
        dataType: "json",
        success: function (data) {
            console.log(data);
            if(data.result=='success'){
                $('#top').html(data.htmlText); //左侧菜单部分
            }else{
                $('#top').after(data.info);
            }
        },
        err: function (data) {
           console.log(data);
        }
    });
});

/**
 * 得到菜单
 * @param menuId
 * @param classleven
 * @param childcount
 */
function callMenuInfo(menuId,classleven,menuUrl,childcount,controlUrl,menuName){
    console.log(menuId+"=="+classleven+"=="+menuUrl+"=="+childcount+"=="+controlUrl);
    var usermenu = {userId: '', parentId: menuId, menuUrl: menuUrl, childcount: childcount, controlUrl: controlUrl, nowPage: '1'};

    //一级菜单
    if(classleven=="1"){
        $('#title_info').html('<li id="topmenu">'+menuName+'</li>');
        if(menuId == '1'){ //首页
            $("#container1").css('display','');
            $("#container2").css('display','none');
        }else{
            $("#container1").css('display','none');
            $("#container2").css('display','');
        }

        loadTwoLevelMenu(usermenu, menuId, controlUrl); //加载二级菜单
    }

    // 二级菜单
    if(classleven=="2"){
        if(childcount > 0){
            if($('#secondMenu').length > 0){
                $('#secondMenu').remove();
            }
            if ($('#thirdMenu').length > 0) {
                $('#thirdMenu').remove();
            }
            if ($('#fouthMenu').length > 0) {
                $('#fouthMenu').remove();
            }
            $('#topmenu').after('<li id="secondMenu"><i class="icon-IDC-icon-11"></i>'+menuName+'</li>');
            // 调用三级菜单
            loadThreeLevelMenu(usermenu, menuId, controlUrl);

        }else{
            if($('#secondMenu').length > 0){
                $('#secondMenu').remove();
            }
            if ($('#thirdMenu').length > 0) {
                $('#thirdMenu').remove();
            }
            if ($('#fouthMenu').length > 0) {
                $('#fouthMenu').remove();
            }
            $('#topmenu').after('<li id="secondMenu"><i class="icon-IDC-icon-11"></i>'+menuName+'</li>');
            //loadMainPage(usermenu, menuId, controlUrl);
            loadSecondMainPage(usermenu, menuId, controlUrl);
        }
    }
    // 三级菜单
    if(classleven=="3") {
       /* if ($('#thirdMenu').length > 0) {
            $('#thirdMenu').remove();
        }
        if ($('#fouthMenu').length > 0) {
            $('#fouthMenu').remove();
        }
        $('#secondMenu').after('<li id="thirdMenu"><i class="icon-IDC-icon-11"></i>' + menuName + '</li>');*/
        loadThirdMainPage(usermenu, menuId, controlUrl);
    }
}

/**
 * 调用二级菜单
 */
function loadTwoLevelMenu(usermenu, menuId, controlUrl){
    $.when(getMenuHtml(usermenu, controlUrl)).done(function(data){
        if(data == 'undefined'){
            //$('#menu_'+menuId).after('无法访问服务资源..');
        }else if(data.result == 'error'){
            //$('#menu_'+menuId).after('出错了...');
        }else{
            /*if($('#left_ul').length > 0){
                $('#left_ul').remove();
            }*/
            //$('#menu_'+menuId).after(data.htmlText);
            $('#left').html(data.htmlText);
        }
    })
}

/**
 * 调用三级菜单
 * @param usermenu
 * @param menuId
 * @param controlUrl
 */
function loadThreeLevelMenu(usermenu, menuId, controlUrl){
    $.when(getMenuHtml(usermenu, controlUrl)).done(function(data){
        if(data == 'undefined'){
            // $('#right').html('无法访问服务资源..');
            $('#menuchild_'+menuId).html('无法访问服务资源..');
        }else if(data.result == 'error'){
            //$('#right').html('出错了...');
            $('#menuchild_'+menuId).html('无法访问服务资源..');
        }else{
            /*if($('#thirdMenu_'+menuId).length > 0){
                $('#thirdMenu_'+menuId).remove();
            }*/
            //$('#right').html(data.htmlText);
            $('#menuchild_'+menuId).html(data.htmlText);
            DoMenu('menuchild_'+menuId);
        }
    })
}

//根据menuid显示三级菜单相应内容
function DoMenu(emid) {
    var obj = document.getElementById(emid);
    obj.className = (obj.className.toLowerCase() == "expanded" ? "collapsed" : "expanded");
    if ((LastLeftID != "") && (emid != LastLeftID)) //关闭上一个Menu
    {
        document.getElementById(LastLeftID).className = "collapsed";
    }
    LastLeftID = emid;
}


//得到菜单(通用)
function getMenuHtml(usermenu, controlUrl){
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: controlUrl,
        data: usermenu,
        dataType: "json",
        success: function (data) {
            console.log(data);
            defer.resolve(data);
        },
        err: function (data) {
            console.log('1111 == '+data);
            defer.resolve("undefined");
        }
    });

    return defer.promise();
}

/**
 * 点击三级菜单调用页面
 * @param usermenu
 * @param menuId
 * @param controlUrl
 */
function loadThirdMainPage(usermenu, menuId, controlUrl){
    $.when(getMenuHtml(usermenu, controlUrl)).done(function(data){
        /*if($('.table').length > 0){
            $('.table').remove();
        }
        if($('.work_ul').length > 0){
            $('.work_ul').remove();
        }
        if($('.work_pl').length > 0){
            $('.work_pl').remove();
        }

        if($('.right_back').length > 0){
            $('.right_back').remove();
        }
        if($('.work_chaxun_ul').length > 0){
            $('.work_chaxun_ul').remove();
        }
        if($('.updown').length > 0){
            $('.updown').remove();
        }
        if($('.work_jijia').length > 0){
            $('.work_jijia').remove();
        }

        if($('.work_chaxun').length > 0){
            $('.work_chaxun').remove();
        }
        if($('.work_submit').length > 0){
            $('.work_submit').remove();
        }
        if($('#myModal').length > 0){
            $('#myModal').remove();
        }
        if($('#bill_style').length > 0){
            $('#bill_style').remove();
        }
        if($('#chargeTitle').length > 0){
            $('#chargeTitle').remove();
        }
        if($('#incom_title').length > 0){
            $('#incom_title').remove();
        }*/
        if(data == 'undefined'){
            $('#right_fe').after('无法访问服务资源..');
        }else if(data.result == 'error'){
            $('#right_fe').after('出错了...');
        }else{
            $('#right_fe').after(data.htmlText);
            $('#nowPage').html(data.htmlText);
        }
    });
}

/**
 * 点击二级菜单调用页面
 * @param usermenu
 * @param menuId
 * @param controlUrl
 */
function loadSecondMainPage(usermenu, menuId, controlUrl){
    $.when(getMenuHtml(usermenu, controlUrl)).done(function(data){
        if(data == 'undefined'){
            $('#right_fe').after('无法访问服务资源..');
        }else if(data.result == 'error'){
            $('#right_fe').after('出错了...');
        }else{
            $('#right_fe').after(data.htmlText);
            $('#nowPage').html(data.htmlText);
        }
    });
}


/**
 * 调用菜单页面
 * @param usermenu
 * @param menuId
 * @param controlUrl
 */
function loadMainPage(usermenu, menuId, controlUrl){
    $.when(getMenuHtml(usermenu, controlUrl)).done(function(data){
        if(data == 'undefined'){
            $('#right').html('无法访问服务资源..');
        }else if(data.result == 'error'){
            $('#right').html('出错了...');
        }else{
            $('#right').html(data.htmlText);
            $('#nowPage').html(1);
            $('#menu_'+menuId).addClass('left_ul_now');
        }
    });
}