/**
 * Created by Cuibin on 2016/7/6.
 */

/**
 *  按照条件查询推荐产品
 */
function queryProductByType() {
    key = 0;
    $("#pageIndex").val(1);
    var typeId = $("#typeId").val();
    var orderColumn = $("#orderColumn").val();
    var orderType = $("#orderType").val();
    var temp = {
        typeId: typeId,
        orderColumn: orderColumn,
        orderType: orderType
    };
    $.ajax({
        type: "post",
        url: "/weixin/queryProductByType",
        data: temp,
        dataType: "json",
        success: function (data) {
            if (data.result == "success") {
                $("#myDiv").empty();
                $("#myDiv").html(data.htmlText);
                $("#count").val(data.count);
            }
        },
        err: function (data) {
        }
    });
}

/**
 *选择类别
 */
function selectType(typeId) {
    key = 0;
    $("#attorn").val(0);
    $("#pageIndex").val(1);
    $("#typeId").val(typeId);
    queryProductByType();

}

function selCo(id) {
    for (var i = 1; i < 7; i++) {
        $("#l" + i).attr("style", "");
    }
    $("#" + id).attr("style", "color:#ff2021");
}


/**
 *选择排序
 */
function selectSort(orderColumn, id) {
    if (id != $("#orderTypeMessage").val()) {
        $("#orderType").val("");
    }
    for (var k = 1; k < 4; k++) {
        if (k != id) {
            var temp = $("#" + k).val();
            var f = temp.substr(temp.length - 1, temp.length);
            if (f == "↑" || f == "↓") {
                temp = temp.substr(0, temp.length - 1);
                $("#" + k).val(temp);
            }
        }
    }
    $("#orderTypeMessage").val(id);
    $("#orderColumn").val(orderColumn);
    var orderType = $("#orderType").val();
    if (orderType == "") {
        $("#" + id).val($("#" + id).val() + "↑");
        orderType = "ASC";
    } else if (orderType == "ASC") {
        var temp = $("#" + id).val();
        temp = temp.replace("↑", "↓");
        $("#" + id).val(temp);
        orderType = "DESC";
    } else if (orderType == "DESC") {
        var temp = $("#" + id).val();
        temp = temp.replace("↓", "↑");
        $("#" + id).val(temp);
        orderType = "ASC";
    }
    $("#orderType").val(orderType);

    var attorn = $("#attorn").val();
    if (attorn == "1") {
        queryAttornByType();
    } else {
        queryProductByType();
    }


}
/**
 * 查看产品详情
 */
function openProductParticulars(id) {
    var temp = {id: id};
    $.ajax({
        type: "post",
        url: "/weixin/productDetails",
        data: temp,
        dataType: "json",
        success: function (data) {
            if (data.result == "success") {
                $("#myBody").empty();
                $("#myBody").html(data.htmlText);
            }
        },
        err: function (data) {
        }
    });
}
/**
 * 查看产品详情
 */
function openProductParticulars1(id) {
    location.href = "/weixin/productDetails1?id=" + id;

}

/**
 * 查看借款信息
 */
function openProductBorrow(id) {
    location.href = "/weixin/openProductBorrow?id=" + id;
}

/**
 *
 */
function openLoanDetails(productid) {
    location.href = "/weixin/openLoanDetails?productid=" + productid;
}

/**
 * 查询更多产品列表
 */
function queryMoreProduct() {
    var pageIndex = parseInt($("#pageIndex").val()); //从
    var pageSize = 4; //取四条
    var count = parseInt($("#count").val()); //总数

    pageIndex = pageIndex + 4;
    if (count - pageIndex <= 0) {
        $("#mes").html("没有更多数据");
        return;
    } else if (count - pageIndex < 3) {
        pageSize = count - pageIndex + 1;
    }
    var typeId = $("#typeId").val();//产品类型id
    var orderColumn = $("#orderColumn").val();//排序字段
    var orderType = $("#orderType").val();//排序类型

    var temp = {
        pageIndex: pageIndex,
        pageSize: pageSize,
        typeId: typeId,
        orderColumn: orderColumn,
        orderType: orderType
    };

    $("#pageIndex").val(pageIndex);
    $.ajax({
        type: "post",
        url: "/weixin/queryProductByType",
        data: temp,
        dataType: "json",
        time: 15000,
        success: function (data) {
            if (data.result == "success") {
                $("#count").val(data.count);
                $("#myDiv").append(data.htmlText);
                key = 0;
            } else {
                $("#mes").html(data.info);
                key = 0;
            }
        },
        err: function (data) {
            $("#mes").html("网络连接超时");
        }
    });
}


/**
 * 查询债权转让
 */
function queryAttornByType() {
    key = 0;
    $("#pageIndex").val(1);
    $("#attorn").val(1);
    var orderColumn = $("#orderColumn").val();
    var orderType = $("#orderType").val();

    if (orderColumn == "ONLINETIME") {
        orderColumn = "CREATETIME";
    } else if (orderColumn == "LOANDAYS") {
        orderColumn = "SURPLUSDAYS";
    } else if (orderColumn == "LOANRATE") {
        orderColumn = "EXPECTEDRATE";
    }

    var temp = {
        orderColumn: orderColumn,
        orderType: orderType
    };

    $.ajax({
        type: "post",
        url: "/weixin/queryProductAttornByType",
        data: temp,
        dataType: "json",
        success: function (data) {
            if (data.result == "success") {
                $("#myDiv").empty();
                $("#myDiv").html(data.htmlText);
                $("#count").val(data.count);
            }
        },
        err: function (data) {
        }
    });
}
/**
 * 查询更多债权转让
 */
function queryMoreAttorn() {
    var pageIndex = parseInt($("#pageIndex").val()); //从
    var pageSize = 4; //取四条
    var count = parseInt($("#count").val()); //总数

    pageIndex = pageIndex + 4;
    if (count - pageIndex <= 0) {
        $("#mes").html("没有更多数据");
        return;
    } else if (count - pageIndex < 3) {
        pageSize = count - pageIndex + 1;
    }
    var orderColumn = $("#orderColumn").val();//排序字段
    var orderType = $("#orderType").val();//排序类型

    var temp = {
        pageIndex: pageIndex,
        pageSize: pageSize,
        orderColumn: orderColumn,
        orderType: orderType
    };

    $("#pageIndex").val(pageIndex);
    $.ajax({
        type: "post",
        url: "/weixin/queryProductAttornByType",
        data: temp,
        dataType: "json",
        time: 15000,
        success: function (data) {
            if (data.result == "success") {
                $("#count").val(data.count);
                $("#myDiv").append(data.htmlText);
                key = 0;
            } else {
                $("#mes").html(data.info);
                key = 0;
            }
        },
        err: function (data) {
            $("#mes").html("网络连接超时");
        }
    });
}
/**
 * 查看债权转让详情
 */
function queryAttornDetails(id) {
    location.href = "/weixin/queryAttornDetails?id=" + id;
}
/**
 *债权转让投资记录
 */
function openAttornLoanDetails(productid) {
    location.href = "/weixin/openAttornLoanDetails?productid=" + productid;
}

/**
 *
 */
function selColor(id) {
    for (var i = 1; i < 4; i++) {
        $("#" + i).attr("style", "");
    }
    $("#" + id).attr("style", "background-color: #ff2021");

}

var key = 0;