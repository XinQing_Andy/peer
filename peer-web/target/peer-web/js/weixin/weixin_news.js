/**
 * Created by Cuibin on 2016/7/8.
 */


/**
 * 查询一个新闻
 */
function queryOnlyNews(id) {
    location.href = "/weixin/queryOnlyNews?id=" + id;

}

/**
 * 查询更多新闻
 */
function queryMoreNews() {
    var pageIndex = parseInt($("#pageIndex").val()); //从
    var pageSize = 8; //取八条
    var count = parseInt($("#count").val()); //总数

    pageIndex = pageIndex + 8;
    if (count - pageIndex == 0 || count - pageIndex < 0) {
        $("#mes").html("没有更多数据");
        return;
    } else if (count - pageIndex < 7) {
        pageSize = count - pageIndex + 1;
    }
    var temp = {
        pageIndex: pageIndex,
        pageSize: pageSize
    };


    $("#pageIndex").val(pageIndex);
    $.ajax({
        type: "post",
        url: "/weixin/queryMoreNews",
        data: temp,
        dataType: "json",
        time: 15000,
        success: function (data) {
            if (data.result == "success") {
                $("#count").val(data.count);
                $("#myDiv").append(data.htmlText);
                key = 0;
            } else {
                $("#mes").html(data.info);
                key = 0;
            }
        },
        err: function (data) {
            $("#mes").html("网络连接超时");
        }
    });
}

var key = 0;
