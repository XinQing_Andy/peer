/**
 * Created by lenovo on 2015/12/15.
 */

/**
 * 员工table表格删除一行数据
 */
function onDelUser(user) {
    var defer = $.Deferred();
    var parm = {id: user};
    $.ajax({
        type: "post",
        url: "/manager/deleteUser",
        data: $.param(parm),
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 *员工添加数据
 */
function overlay() {
    var e1 = document.getElementById("addUser");
    e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";

}

/**
 * 员工添加数据
 */
function onAddUser() {
    var userInfo = $("#addYg").serialize();
    $.when(addUser(userInfo)).done(function (data) {
        console.log(data);
        if (data.result == "error") {
            alert("添加失败" + data.info);
        } else {
            alert("添加成功");
        }
    });
}

function addUser(userInfo) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manager/addUser",
        data: userInfo,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 员工修改数据
 */
function onUpdateUserInformation(id, userName, password, realName, qq, phoneNo, active, eamil, flg) {
    if (flg = "Y") {
        var e1 = document.getElementById("update_user");
        e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
        var sysId = document.getElementById("idw");
        sysId.setAttribute("value", id);
        var sysName = document.getElementById("userName");
        sysName.setAttribute("value", userName);
        var sysPw = document.getElementById("passW");
        sysPw.setAttribute("value", password);
        var sysRn = document.getElementById("realN");
        sysRn.setAttribute("value", realName);
        var sysQq = document.getElementById("q");
        sysQq.setAttribute("value", qq);
        var sysPno = document.getElementById("phoneN");
        sysPno.setAttribute("value", phoneNo);
        var sysActive = document.getElementById("activeId");
        sysActive.setAttribute("value", active);
        var sysEmail = document.getElementById("uEmail");
        sysEmail.setAttribute("value", eamil);
    } else {
        var e1 = document.getElementById("update_user");
        e1.style.visibility = (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
        var sysId = document.getElementById("idw");
        sysId.setAttribute("value", id);
        var sysName = document.getElementById("userName");
        sysName.setAttribute("value", userName);
        var sysPw = document.getElementById("passW");
        sysPw.setAttribute("value", password);
        var sysRn = document.getElementById("realN");
        sysRn.setAttribute("value", realName);
        var sysQq = document.getElementById("q");
        sysQq.setAttribute("value", qq);
        var sysPno = document.getElementById("phoneN");
        sysPno.setAttribute("value", phoneNo);
        var sysActive = document.getElementById("activeId");
        sysActive.setAttribute("value", active);
        var sysEmail = document.getElementById("uEmail");
        sysEmail.setAttribute("value", eamil);
        var button = document.getElementById("saveButton");
        button.style.visibility == hidden;
    }

}

/**
 *员工修改数据
 */
function userInfoUpdate() {
    var userInfo = $("#updateYg").serialize();
    $.when(updateUserInformation(userInfo)).done(function (data) {
        if (data.result == "error") {
            alert("修改失败" + data.info);
        } else {
            alert("修改成功");
        }
    });
}

function updateUserInformation(userInfo) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manager/userInfo",
        data: userInfo,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/*
 * 重置密码
 * */
function updatePw(user) {
    var defer = $.Deferred();
    var parm = {id: user};
    $.ajax({
        type: "post",
        url: "/manager/updatePw",
        data: $.param(parm),
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/*
 * 禁用
 * */
function upActive(user) {
    var defer = $.Deferred();
    var parm = {id: user};
    $.ajax({
        type: "post",
        url: "/manager/upActive",
        data: $.param(parm),
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

