
/**
 * Created by Cuibin on 2016/5/31.
 */

/**
 *  加载echar 屏幕左侧
 */
function uploadEchar(time, moneyMem, regMem) {
    // 初始化
    var myChart = echarts.init(document.getElementById('member1'));
    option = {
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['新增投资数量', '新增注册会员']
        },
        toolbox: {
            show: true,
            feature: {
                mark: {
                    show: true
                },
                dataView: {
                    show: true,
                    readOnly: false
                },
                magicType: {
                    show: true,
                    type: ['line', 'bar']
                },
                restore: {
                    show: true
                },
                saveAsImage: {
                    show: true
                }
            }
        },
        calculable: true,
        xAxis: [{
            type: 'category',
            boundaryGap: false,
            data: time
        }],
        yAxis: [{
            type: 'value',
            axisLabel: {
                formatter: '{value}'
            }

        }],
        series: [{
            name: '新增投资数量',
            type: 'line',
            data: moneyMem,
            //markPoint: {
            //    data: [{
            //        type: 'max',
            //        name: '最大值'
            //    }, {
            //        type: 'min',
            //        name: '最小值'
            //    }]
            //},

        }, {
            name: '新增注册会员',
            type: 'line',
            data: regMem,

        }]
    };
    myChart.setOption(option);

}
function selectBq(v) {

    if (v == null || v == "") {
        v = '1';
    }
    var time = null;
    var moneyMem = null;
    var regMem = null;
    if (v == '1') {
        $.ajax({
            type: "post",
            url: "/manage/queryYesterdayReg",
            dataType: "json",
            success: function (data) {
                regMem = data.yesterdayReg;
                moneyMem = data.lastDayMoney;
                time = ['2:00', '4:00', '6:00', '8:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00', '0:00'];
                uploadEchar(time, moneyMem, regMem);
            },
            err: function (data) {

            }
        });


    } else if (v == '2') {
        $.ajax({
            type: "post",
            url: "/manage/queryLastWeekReg",
            dataType: "json",
            success: function (data) {
                //alert(data.lastWeekReg);
                regMem = data.lastWeekReg;
                moneyMem = data.lastWeekMoney;
                time = data.showDate;
                uploadEchar(time, moneyMem, regMem);
            },
            err: function (data) {

            }
        });
    } else if (v == '3') {
        $.ajax({
            type: "post",
            url: "/manage/queryLastMonthReg",
            dataType: "json",
            success: function (data) {
                regMem = data.lastMonthReg;
                moneyMem = data.lastMonthMoney;
                time = data.showDate;

                uploadEchar(time, moneyMem, regMem);
            },
            err: function (data) {

            }
        });

    }
}
/**
 *  首次加载
 */
function firstLoad(jsonStr) {
    var obj = JSON.parse(jsonStr);
    var yesterdayReg = obj.yesterdayReg;
    var lastDayMoney = obj.lastDayMoney;
    var lastDayInvest = obj.lastDayInvest;
    lastDayInvest = lastDayInvest.replace("[","").replace("]","");
    lastDayInvest = lastDayInvest.split(",");

    yesterdayReg = yesterdayReg.replace("[","").replace("]","");
    yesterdayReg = yesterdayReg.split(",");


    try
    {
        var time = ['2:00', '4:00', '6:00', '8:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00', '0:00'];
        uploadEchar(time, lastDayMoney, yesterdayReg);
    }
    catch (e)
    {

    }
    var time2 = ['2:00', '4:00', '6:00', '8:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00', '0:00'];
    var recharge = [1, 2, 2, 5, 3, 2, 0];
    uploadEcharRight(time2, lastDayInvest, recharge);
}

/**
 *  加载echar 屏幕右侧
 */
function uploadEcharRight(time, invest, recharge) {
    // 初始化
    var myChart = echarts.init(document.getElementById('member2'));
    option = {
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['投资金额', '充值金额']
        },
        toolbox: {
            show: true,
            feature: {
                mark: {
                    show: true
                },
                dataView: {
                    show: true,
                    readOnly: false
                },
                magicType: {
                    show: true,
                    type: ['line', 'bar']
                },
                restore: {
                    show: true
                },
                saveAsImage: {
                    show: true
                }
            }
        },
        calculable: true,
        xAxis: [{
            type: 'category',
            boundaryGap: false,
            data: time
        }],
        yAxis: [{
            type: 'value',
            axisLabel: {
                formatter: '{value}'
            }

        }],
        series: [{
            name: '投资金额',
            type: 'line',
            data: invest,
            //markPoint: {
            //    data: [{
            //        type: 'max',
            //        name: '最大值'
            //    }, {
            //        type: 'min',
            //        name: '最小值'
            //    }]
            //}

        }, {
            name: '充值金额',
            type: 'line',
            data: recharge,

        }]
    };
    myChart.setOption(option);

}

function selectBqRight(x) {
    if (x == null || x == "") {
        x = 1;
    }
    var time = null;
    var invest = null;
    var recharge = null;

    if (x == '1') {

        $.ajax({
            type: "post",
            url: "/manage/queryLastDayInvest",
            dataType: "json",
            success: function (data) {
                time = ['2:00', '4:00', '6:00', '8:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00', '0:00'];
                invest = data.lastDayInvest;
                recharge = [1, 2, 2, 5, 3, 2, 0];
                uploadEcharRight(time, invest, recharge);
            },
            err: function (data) {

            }
        });

    } else if (x == '2') {
        $.ajax({
            type: "post",
            url: "/manage/queryLastWeekInvest",
            dataType: "json",
            success: function (data) {
                time = data.showDate;
                invest = data.lastWeekInvest;
                recharge = [1, 2, 2, 5, 3, 2, 0];
                uploadEcharRight(time, invest, recharge);
            },
            err: function (data) {

            }
        });

    } else if (x == '3') {
        $.ajax({
            type: "post",
            url: "/manage/queryLastMonthInvest",
            dataType: "json",
            success: function (data) {
                time = data.showDate;
                invest = data.lastMonthInvest;
                recharge = [1, 2, 2, 5, 3, 2, 0];
                uploadEcharRight(time, invest, recharge);
            },
            err: function (data) {

            }
        });


    }
}

