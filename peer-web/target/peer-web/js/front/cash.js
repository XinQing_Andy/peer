/**
 * Created by Cuibin on 2016/6/20.
 */


/**
 * 跳转到提现
 */
function openCash(){
    $.ajax({
        type: "post",
        url: "/cash/openCash",
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText);
            } else {
                alert(data.info);
            }
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}


/**
 * 跳转到提现记录
 */
function openCashHistory(){
    var huanXunPostalMes = {menuUrl:'/front/cash/cash_history.jetx'};
    $.ajax({
        type:"post",
        url:"/cash/queryPostalRecordByPage",
        data: huanXunPostalMes,
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText);
            } else {
                alert(data.info);
            }
        },
        err: function (data) {
            alert('查询失败');
        }
    });
}

/**
 * 按照条件查询提现记录
 */
function queryPostalRecordByPage(){
    var startTime = $("#cashStartTime").val(); //起时间
    var endTime = $("#cashEndTime").val(); //止时间
    var pageIndex = $('#pageIndex').val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }
    var huanXunPostalMes = {startTime:startTime,endTime:endTime,pageIndex:pageIndex,pageSize:pageSize};

    $.ajax({
        type:"post",
        url:"/cash/queryPostalRecordBySome",
        data:huanXunPostalMes,
        dataType:"json",
        success:function (data) {
            if (data.result == 'success') {
                $('#tbodyHuanXunPostal').html(data.str); //列表部分拼接
                $('#ulPageStr').html(data.pageStr); //分页部分
            } else {
                alert(data.info);
            }
        },
        err: function (data) {
            alert(data.info);
        }
    });
}


/**
 * 提现
 */
function userCash() {
    var operationType = $("#operationType").val(); //操作类型
    var merchantID = $("#merchantID").val(); //商户存管交易账号
    var trdAmt = $("#userSprice").val(); //提现金额
    var huanxunpostalMes = {operationType:operationType,merchantID:merchantID,trdAmt:trdAmt};

    $.ajax({
        type: "post",
        url: "/huanxun/postalhx",
        data:huanxunpostalMes,
        dataType:"json",
        success: function (data) {
            if (data.code == '0') {
                $("#sign").val(data.sign); //签名
                $("#request").val(data.request); //请求信息
                $("#postalForm").submit();
            } else {
                alert(data.info);
            }
        },
        error:function(data){
            alert(data.info);
        }
    });
}

function checkUserBalance(){
    var balance = $("#balance").html();
    var userSprice = $("#userSprice").val();

    if(balance - userSprice < 0){
        $("#message").html("提现金额不能大于可用余额");
        return false;
    }
    return true;
}
/**
 *验证时间
 */
function cashCheckTime(){
    var val = $("#cashStartTime").val();
    var val2 = $("#cashEndTime").val();
    var time1 = new Date(val).getTime();
    var time2 = new Date(val2).getTime();
    if(time1 > time2 || time1 == time2){
        alert("开始时间必须小于结束时间");
        return false;
    }
    return true;
}

/**
 * 验证输入
 */
function cashCheckInput() {
    $("#userSprice").css("ime-mode", "disabled");
    $("#userSprice").bind("keypress", function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (!$.browser.msie && (e.keyCode == 0x8)) {
            return;
        }
        return code >= 48 && code <= 57 || code == 46;
    });
    $("#userSprice").bind("blur", function () {
        if (this.value.lastIndexOf(".") == (this.value.length - 1)) {
            this.value = this.value.substr(0, this.value.length - 1);
        } else if (isNaN(this.value)) {
            this.value = " ";
        }
    });
    $("#userSprice").bind("paste", function () {
        var s = clipboardData.getData('text');
        if (!/\D/.test(s));
        value = s.replace(/^0*/, '');
        return false;
    });
    $("#userSprice").bind("dragenter", function () {
        return false;
    });
    $("#userSprice").bind("keyup", function () {
        this.value = this.value.replace(/[^\d.]/g, "");
        this.value = this.value.replace(/^\./g, "");
        this.value = this.value.replace(/\.{2,}/g, ".");
        this.value = this.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
    });
}

/********************   分页部分    ********************/

/**
 * 上一页
 */
function cashLastPage() {
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo <= 1) {
        $("#pageNo").html(1);
        return;
    } else {
        $("#pageNo").html(pageNo - 1);
    }

    queryPostalRecordByPage();
}
/**
 * 下一页
 */
function cashNextPage() {
    var totalPage = $("#totalPage").val();
    totalPage = parseInt(totalPage);
    if (totalPage < 2) {
        return;
    }
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo >= totalPage) {
        $("#pageNo").html(totalPage);
    } else {
        $("#pageNo").html(pageNo + 1);
    }

    queryPostalRecordByPage();
}
/**
 * 第一页
 */
function cashFirstPage() {
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo == 1) {
        return;
    }
    $("#pageNo").html(1);
    queryPostalRecordByPage();
}

/**
 * 最后一页
 */
function cashEndPage() {
    var totalPage = $("#totalPage").val();
    totalPage = parseInt(totalPage);
    if (totalPage < 2) {
        return;
    }
    $("#pageNo").html($("#totalPage").val());
    queryPostalRecordByPage();
}
/**
 * 跳转到指定页数
 */
function cashGoPage() {
    var totalPage = $("#totalPage").val();
    if (totalPage < 2) {
        return;
    }
    var inputPage = $("#inputPage").val();
    if (inputPage > totalPage || inputPage <= 0) {
        return;
    }

    $("#pageNo").html(inputPage);
    queryPostalRecordByPage();
}

/********************   分页部分    ********************/

