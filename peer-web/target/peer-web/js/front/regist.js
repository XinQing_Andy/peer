/**
 * Created by cuibin on 2016/4/25.
 */
/**
 * 发送邮件
 */
function sendEmailCheck(){
    $("#reg_step2").attr({"disabled": "true"});//禁用按钮
    var username = $("#username").val(); //用户名
    var nickname = $("#nickname").val(); //昵称
    var password = $("#password").val(); //密码
    var certCode = $("#certCode").val(); //验证码
    var reg = /^\s+$/gi;
    if(certCode == "" || reg.test(certCode)){
        $("#yzMessage").html("<font color='red'>请填写验证码</font>");
        return;
    }
    /********** 发送邮件Start **********/
    $.ajax({
        type:"post",
        url:"/regist/sendEmailCheck",
        data:"username="+username+"&nickname="+nickname+"&password="+password+"&certCode="+certCode,
        dataType:"json",
        success:function(data){
           if(data == '0'){ //发送成功
               regTime();
               $("#yzMessage").html("");
           }else if(data == '1'){ //发送失败;
               $("#yzMessage").html('<font style="color:red">邮件发送失败，请稍后重试</font>');
               $("#reg_step2").removeAttr("disabled");
               k = 0;
           }else if(data == '2'){
               $("#yzMessage").html('<font style="color:red">验证码不正确</font>');
               reloadcode();
               $("#reg_step2").removeAttr("disabled");
               k = 0;
           }
        },
        err:function(data){ //发送失败
            $("#yzMessage").html('<font style="color:red">邮件发送失败，请稍后重试</font>');
        }
    });
    /*********** 发送邮件End ***********/
}
/**
 * 验证昵称
 */
function checkNickname(){

    var nickname = $("#nickname").val();
    var reg = /^\s+$/gi;
    if(nickname == "" || reg.test(nickname)){
        $("#nicknameMessage").html("<font color='red'>昵称不能为空</font>");
        return false;
    }
    if (nickname.length > 7) {
        $("#nicknameMessage").html("<font color='red'>昵称长度不能大于7位</font>");
        return false;
    }
    $("#nicknameMessage").html("");
    return true;
}
/**
 * 验证用户名
 */
function checkUsername(){
    var username = $("#username").val();
    var reg = /^\s+$/gi;
    if(username == "" || reg.test(username)){
        $("#message").html("<font color='red'>账号不能为空</font>");
        return false;
    }
    var reg2 = /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/;
    var reg3 = /^1\d{10}$/;
    if(!reg2.test(username) && !reg3.test(username)){
        $("#message").html("<font color='red'>账号格式不正确</font>");
        return false;
    }
    checkUsernameIs();
    var val = $("#ud").val();
    if(val == '0'){
        return false;
    }
    $("#message").html("");
    return true;

}
/**
 * 验证用户名是否存在
 */
function checkUsernameIs(){
    var username = $("#username").val();
    var temp = {username:username};
    $.ajax({
        type:"post",
        url:"/regist/checkUsername",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $("#message").html("<font color='#228b22'>"+ data.info +"</font>");
                $("#ud").val("1");
            }else{
                $("#message").html("<font color='red'>"+ data.info +"</font>");
                $("#ud").val("0");
            }
        },
        err:function(data){

        }
    });
}
/**
 * 验证密码
 */
function checkPassword(){
    var password = $("#password").val();
    var confirmPassword = $("#confirmPassword").val();
    var reg = /^\s+$/gi;
    if(password == "" || reg.test(password)){
        $("#passwordMessage").html("<font color='red'>密码不能为空</font>");
        return false;
    }
    if(password.length < 6){
        $("#passwordMessage").html("<font color='red'>密码不能小于6位</font>");
        return false;
    }
    if(password != confirmPassword){
        $("#passwordMessage").html("<font color='red'>两次输入的密码不一致</font>");
        return false;
    }
    $("#passwordMessage").html("");
    return true;
}

function goUp(){
    window.history.go(-1);
}

function goLogin(){
    $(".landing").slideDown(200);
}

//生成验证码
function reloadcode(){
    var verify = document.getElementById('code');
    verify.setAttribute('src','/regist/makeCertPic?it='+Math.random());
}

/**
 * 跳转到 邮箱验证||手机验证
 */
function openCheckPage(){
    if(!checkNickname()||!checkUsername()||!checkPassword()){
        return;
    }

    if(!($('#is_server').is(':checked'))){
        alert('请同意注册协议和隐私条款');
        return;
    }
    var username = $("#username").val();
    var temp = {username:username};
    $.ajax({
        type:"post",
        url:"/regist/checkUsername",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $("#message").html("<font color='#228b22'>"+ data.info +"</font>");
                $("#ud").val("1");

                var reg3 = /^1\d{10}$/;
                var username = $("#username").val();
                if(reg3.test(username)){
                    $("#reg_step1").attr("action","openPhoneCheck");
                }else{
                    $("#reg_step1").attr("action","openEmailCheck");

                }
                $("#reg_step1").submit();
            }else{
                $("#message").html("<font color='red'>"+ data.info +"</font>");
                $("#ud").val("0");
            }
        },
        err:function(data){

        }
    });
}

/**
 * 倒计时
 */
function regTime(){
    var num = $("#num").val();
    if(num > 0){
        num--;
        $("#reg_step2").val(num+"秒后可重新发送");
        $("#num").val(num);
    }else{
        $("#reg_step2").removeAttr("disabled");
        k = 0;
        $("#reg_step2").val("验证");
        $("#num").val(61);
        reloadcode();
        return;
    }
    setTimeout(function() {regTime()},1000);
}

/**
 *  防止连续点击注册
 */
var k = 0;
function sendLock() {
    k++;
    if (k == 1) {
        sendEmailCheck();
    }
}

/**
 * 发送手机验证码
 */
function sendMessageCheck() {
    $("#sjyz").attr({"disabled": "true"});
    messageTime();
    var username = $("#username").val();
    var temp = {
        username: username
    };
    $.ajax({
        type: "post",
        url: "/regist/sendMessageCheck",
        data: temp,
        dataType: "json",
        success: function (data) {

        },
        err: function (data) {
        }
    });
}
/**
 * 短信页面倒计时
 */
function messageTime() {
    var num = $("#num").val();
    if (num > 0) {
        num--;
        $("#sjyz").val(num + "秒后可重新发送");
        $("#num").val(num);
    } else {
        $("#sjyz").removeAttr("disabled");
        $("#sjyz").val("获取验证码");
        $("#num").val(61);
        return;
    }
    setTimeout(function () {
        messageTime()
    }, 1000);
}

/**
 * 验证手机验证码
 */
function checkCode() {
    var code = $("#code").val();
    var temp = {
        code: code
    };
    $.ajax({
        type: "post",
        url: "/regist/checkCode",
        data: temp,
        dataType: "json",
        success: function (data) {
            if (data.result == "success") {
                //提交
                $("#myForm").submit();
            } else {
                alert(data.info);
            }
        },
        err: function (data) {
        }
    });
}


