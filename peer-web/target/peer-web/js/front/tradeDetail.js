/**
 * Created by Cuibin on 2016/6/20.
 */

/**
 * 跳转到资金明细
 */
function openTradeDetail() {
    var tradeDetailMes = {menuUrl:'/front/tradeDetail/tradeDetail.jetx'};
    $.ajax({
        type:"post",
        url:"/tradeDetail/openTradeDetail",
        data:tradeDetailMes,
        success: function (data) {
            if (data.result == 'success') {
                $(".p_right_wordbox").html(data.htmlText);
            } else {
                alert(data.info);
            }
        },
        err: function (data) {
            alert(data.info);
        }
    });
}


/**
 * 按照条件查询资金明细
 */
function queryTradeDetailByType() {
    var startTime = $("#tdStartTime").val(); //交易起时间
    var endTime = $("#tdEndTime").val(); //交易止时间
    var type = $("#type").val(); //类型
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量
    if(typeof (pageIndex) == "undefined"){
        pageIndex = '';
    }
    if(typeof (pageSize) == "undefined"){
        pageSize = '';
    }

    var tradeDetailMes = {startTime:startTime,endTime:endTime,pageIndex:pageIndex,pageSize:pageSize,type:type};

    $.ajax({
        type:"post",
        url:"/tradeDetail/queryTradeDetail",
        data:tradeDetailMes,
        dataType:"json",
        success: function (data) {
            $("#tbodyTradeDetail").html(data.str); //列表部分拼接
            $("#ulPageStr").html(data.pageStr); //分页部分拼接
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}

/**
 * 选择标签类型
 * @param type
 */
function selectTradeDetailBq(type) {
    $("#type").val(type); //类型
    if(type != '' && typeof (type) != 'undefined'){
        for(var i=0;i<6;i++){
            if(parseInt(type) == i){
                $("#aType"+i).css('color','#F00');
            }else{
                $("#aType"+i).css('color','#000');
            }
        }
        $("#aType").css('color','#000');
    }else{
        $("#aType").css('color','#F00');
        for(var i=0;i<6;i++){
            $("#aType"+i).css('color','#000');
        }
    }
    queryTradeDetailByType();
}


/**
 * 验证时间
 */
function tradeDetailCheckTime() {
    var val = $("#tdStartTime").val();
    var val2 = $("#tdEndTime").val();
    var time1 = new Date(val).getTime();
    var time2 = new Date(val2).getTime();
    if (time1 > time2 || time1 == time2) {
        alert("开始时间必须小于结束时间");
        return false;
    }
    return true;
}
/********************   分页部分    ********************/

/**
 * 上一页
 */
function fundLastPage() {
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo <= 1) {
        $("#pageNo").html(1);
        return;
    } else {
        $("#pageNo").html(pageNo - 1);
    }

    queryTradeDetailByType();
}
/**
 * 下一页
 */
function fundNextPage() {
    var totalPage = $("#totalPage").val();
    totalPage = parseInt(totalPage);
    if (totalPage < 2) {
        return;
    }
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo >= totalPage) {
        $("#pageNo").html(totalPage);
    } else {
        $("#pageNo").html(pageNo + 1);
    }

    queryTradeDetailByType();
}
/**
 * 第一页
 */
function fundFirstPage() {
    var pageNo = $("#pageNo").html();
    pageNo = parseInt(pageNo);
    if (pageNo == 1) {
        return;
    }
    $("#pageNo").html(1);
    queryTradeDetailByType();
}

/**
 * 最后一页
 */
function fundEndPage() {
    var totalPage = $("#totalPage").val();
    totalPage = parseInt(totalPage);
    if (totalPage < 2) {
        return;
    }
    $("#pageNo").html($("#totalPage").val());
    queryTradeDetailByType();
}
/**
 * 跳转到指定页数
 */
function fundGoPage() {
    var totalPage = $("#totalPage").val();
    if (totalPage < 2) {
        return;
    }
    var inputPage = $("#inputPage").val();
    if (inputPage > totalPage || inputPage <= 0) {
        return;
    }

    $("#pageNo").html(inputPage);
    queryTradeDetailByType();
}

/********************   分页部分    ********************/

