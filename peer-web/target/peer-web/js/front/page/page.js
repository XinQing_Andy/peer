//前台分页部分

//分页部分(我要投资)
function pageInvest(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    invertBySome();

}
//跳到第几页(我要投资)
function goInvestPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    invertBySome();
}




//分页部分(债权)
function pageAttorn(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    $.ajax({
        type:"post",
        url:'/attornRecord/queryAttornWhereByPage',
        data:$.parseJSON("{"+$("#selectjson").val()+","+$("#urljetx").val()+",\"pageIndex\":\""+pageIndex+"\",\"pageSize\":\""+pageSize+"\"}"),
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $("#attorn_table_two").html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//跳到第几页(债权)
function goAttornPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    pageAttorn(page_text,pageSize);
}


//分页部分(还款)
function pageRepay(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndexRepay").val(pageIndex);
    }
    $("#pageSizeRepay").val(pageSize); //每页显示数量
    getRepayList();
}
//跳到第几页(还款)
function goRepayPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndexRepay").val(pageIndex);
            $("#pageSizeRepay").val(pageSize);
        }
    }
    getRepayList();
}



//分页部分(债权中投资记录)
function pageInvestRecord(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndexInvest").val(pageIndex);
    }
    $("#pageSizeInvest").val(pageSize); //每页显示数量
    getInvestRecord();
}
//跳到第几页(债券中投资记录)
function goInvestRecordPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndexInvest").val(pageIndex);
            $("#pageSizeInvest").val(pageSize);
        }
    }
    getInvestRecord();
}


//分页部分(更多产品)
function pageProduct(page,pageSize,glId){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    openProductMove(glId);
}
//跳转到第几页(更多产品)
function goProductPage(totalPage,pageSize,glId){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    openProductMove(glId);
}



//分页部分(更多债权)
function pageAttornMore(page,pageSize,glId){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    openAttornMore();
}
//跳转到第几页(更多债权)
function goAttornMorePage(totalPage,pageSize,glId){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    openAttornMore();
}


//分页部分(我的消息)
function pageMemberMessage(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    openMyMessage();
}
//跳转到第几页(我的消息)
function goMemberMessagePage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    openMyMessage();
}

//分页部分(可转让的债权)
function pageNotAttorn(page,pageSize){
    alert(1);
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryNotAttorn();
}
//跳转到第几页(可转让的债权)
function goNotAttornPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryNotAttorn();
}

//分页部分(还款明细)
function pageRepayDetail(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    openRepayDetail();

}
//跳到第几页(还款明细)
function goRepayDetailPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    openRepayDetail();
}

//分页部分(已购买的债权)
function pageInvestedAttorn(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryAlreadyBuyByPage();
}

function goInvestedAttornPage(){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryAlreadyBuyByPagek();
}


//分页部分(投标中的项目)
function pageInvestTb(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    doProductBuytb();
}

function goInvestTbPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    doProductBuytb();
}


//分页部分(回收中的项目)
function pageInvestHs(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    doGoMyInvest();
}
function goInvestHsPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    doGoMyInvest();
}


//分页部分(结清的项目)
function pageInvestJq(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    doProductBuyjq();
}
function goInvestJqPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    doProductBuyjq();
}


//分页部分(申请的项目)
function pageApply(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    doProductBuysq();
}
function goApplyPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    doProductBuysq();
}


//已转让债权
function pageAlreadyTransferAttorn(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryAlreadyTransferByPage();
}
function goAlreadyTransferAttornPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryAlreadyTransferByPage();
}


//财富资讯
function pageWeathInformation(page,pageSize,firstId,secondId){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryWealthBySome(firstId,secondId);
}
function goWeathInformationPage(totalPage,pageSize,firstId,secondId){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryWealthBySome(firstId,secondId);
}

//广告部分
function goFrontAdvert(page,pageSize,typeId){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryFrontAdvertBySome(typeId);
}
function pageFrontAdvert(totalPage,pageSize,typeId){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryFrontAdvertBySome(typeId);
}


//理财风云榜
function pageFinancing(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    openMoreRanking();
}
function goFinancinPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    openMoreRanking();
}

//充值部分
function pageRecharge(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryRechargeHistoryByTime();
}
function goRechargePage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryRechargeHistoryByTime();
}

//提现部分
function pagePostal(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryPostalRecordByPage();
}
function goPostalPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryPostalRecordByPage();
}

//交易记录
function pageTrade(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryTradeDetailByType();
}
function goTradePage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryTradeDetailByType();
}

//回款部分详细信息
function pageReceive(page,pageSize,param1){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    doQueryInvestProductDetailsByPage(param1);
}
function goReceivePage(totalPage,pageSize,param1){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    doQueryInvestProductDetailsByPage(param1);
}


//还款部分详细信息
function pageRepay(page,pageSize,param1){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    doQueryApplyProductDetailsBySome(param1);
}
function goRepayPage(totalPage,pageSize,param1){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    doQueryApplyProductDetailsBySome(param1);
}


//待回款部分详细信息
function pagedReceive(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    querydReceiveList();
}
function godReceivePage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    querydReceiveList();
}

//已回款部分
function pageyReceive(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryyReceiveList();
}
function goyReceivePage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryyReceiveList();
}


//转让中债权
function pageTransferAttorn(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryAttorning();
}
function goTransferAttorn(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryAttorning();
}


//已购买的债权
function pageInvestedAttorn(page,pageSize){
    var pageIndex = 1; //当前数
    if(page != '' && typeof (page) != 'undefined'){
        pageIndex = (parseInt(page)-1) * parseInt(pageSize) + 1;
        $("#pageIndex").val(pageIndex);
    }
    $("#pageSize").val(pageSize); //每页显示数量
    queryAlreadyBuyByPage();
}
function goInvestedAttornPage(totalPage,pageSize){
    var pageIndex = 1; //当前数
    var page_text = $("#page_text").val(); //跳到第几页
    if(page_text == '' || typeof (page_text) == 'undefined'){
        alert('请输入要跳转的页数!');
    }else{
        if(parseInt(page_text) > parseInt(totalPage)){
            alert('输入的页数不合法！');
        }else{
            pageIndex = (parseInt(page_text)-1) * parseInt(pageSize) + 1;
            $("#pageIndex").val(pageIndex);
            $("#pageSize").val(pageSize);
        }
    }
    queryAlreadyBuyByPage();
}