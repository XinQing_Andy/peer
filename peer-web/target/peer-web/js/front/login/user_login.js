/**
 * Created by cuibin on 2016/4/22.
 */

/**
 * 登录
 */
function userLogin(){
    var username = $("#loginUsername").val();
    var password = $("#loginPassword").val();
    var reg = /^\s+$/gi;
    if(username == "" || reg.test(username) || password == "" || reg.test(password)){
        $("#loginMessage").html("<font color='red'>用户名或密码不能为空</font>");
        $("#isEnter").val("1");
        return;
    }
    var temp = {userName:username,password:password};
    var napoleon = $.param(temp);
    $.ajax({
        type:"post",
        url:"/user/userLogin",
        data:napoleon,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                location.replace('/front/show');
            }else{
                $("#loginMessage").html("<font color='red'>"+ data.info +"</font>");
                $("#isEnter").val("1");
            }
        },
        err:function(data){

        }
    });
}

/**
 * 清空标记
 */
function emptyMessage(){
    $("#loginMessage").html("");
    $("#loginPassword").val("");
}

/**
 * 退出登录
 */
function loginOut(){
    $.ajax({
        type:"post",
        url:"/user/loginOut",
        data:null,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                location.replace('/front/show');
            }
        },
        err:function(data){

        }
    });
}

/** 打开登陆框 **/
function openLogin(){
    $(".landing").slideDown(200);
    $("#isEnter").val("1");
}

/** 关闭登陆框 **/
function closeLogin(){
    $(".landing").slideUp(200);
    $("#isEnter").val("0");
    emptyMessage();
}

/** 回车键登录 **/
document.onkeydown=function(event){
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if(e && e.keyCode==13){ // enter 键
        var isEnter = $("#isEnter").val();
        if(isEnter == '1'){
            $(".btn").trigger("click");
            $("#isEnter").val("0");
        }

    }
};