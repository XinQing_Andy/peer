/**
 * Created by Cuibin on 2016/5/1.
 */


/**
 * 跳转到个人基本资料
 */
function goPersonal(){
    $.ajax({
        type:"post",
        url:"/personal/goPersonal",
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
                location.href='/front/show';
            }
        },
        err:function(data){
        }
    });
}


/**
 * 查询城市
 */
function queryCity(fid){
    if(fid == 'none'){
        $("#city").html("");
        return;
    }
    var temp = {provinceid:fid};
    $.ajax({
        type:"post",
        url:"/personal/queryCity",
        data:temp,
        dataType:"json",
        success:function(data){
            $("#city").html(data.cityPj);
        },
        err:function(data){
        }
    });
}

/**
 *更新个人基本资料
 */
function updatePersona(){
    var realname = $("#realname").val();
    var idcard = $("#idcard").val();

    var sex = $("#sex").val();
    var birthday = $("#birthday").val();
    var province = $("#province").val();
    var city = $("#city").val();
    var detailAddress = $("#detailAddress").val();
    var dregree = $("#dregree").val();
    var school = $("#school").val();
    var marryStatus = $("#marryStatus").val();
    var children = $("#children").val();
    var monthSalary = $("#monthSalary").val();
    var socialSecurity = $("#socialSecurity").val();
    var houseCondition = $("#houseCondition").val();
    var carFlg = $("#carFlg").val();
    var temp = {idcard:idcard,realname:realname,sex:sex,birthday:birthday,province:province,city:city,
        detailAddress:detailAddress,dregree:dregree,school:school,marryStatus:marryStatus,children:children,monthSalary:monthSalary,
        socialSecurity:socialSecurity,houseCondition:houseCondition,carFlg:carFlg};
    $.ajax({
        type:"post",
        url:"/personal/updatePersona",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == "success"){
                alert(data.info);
            }
        },
        err:function(data){
        }
    });
}


/**
 * 验证个人资料
 */
function Checkpersona(){
    var realname = $("#realname").val();
    var idcard = $("#idcard").val();

    var sex = $("#sex").val();
    var birthday = $("#birthday").val();
    var city = $("#city").val();
    var detailAddress = $("#detailAddress").val();
    var dregree = $("#dregree").val();
    var school = $("#school").val();
    var marryStatus = $("#marryStatus").val();
    var children = $("#children").val();
    var monthSalary = $("#monthSalary").val();
    var socialSecurity = $("#socialSecurity").val();
    var houseCondition = $("#houseCondition").val();
    var carFlg = $("#carFlg").val();
    var reg = /^\s+$/gi;
    if(realname == "" || reg.test(realname)){
        alert("真实姓名不能为空");
        return;
    }
    if(idcard == "" || reg.test(idcard)){
        alert("身份证号不能为空");
        return;
    }
    if(sex == "none"){
        alert("性别不能为空");
        return;
    }

    if(birthday == "" || reg.test(birthday)){
        alert("出生日期不能为空");
        return;
    }
    if(city == null){
        alert("请选择地区");
        return;
    }
    updatePersona();
}


/**
 *  头像上传
 */
function txUp(){
    if($("#txHidden").val() == '0'){
        return;
    }
    $.ajaxFileUpload({
        url:'/personal/updatePortrait',
        secureuri:false,
        fileElementId:'portrait',
        dataType:'text',
        success:function(data, status){
            if(confirm("头像设置成功,是否刷新页面,所有未提交信息将会丢失")){
                goPersonal();
            }
        },
        error:function(data, status, e){
            alert('文件上传失败，请重试');
        }
    });

}

/**
 *验证图片
 */
function Checktx(imgFile) {
    var filextension=imgFile.value.substring(imgFile.value.lastIndexOf("."),imgFile.value.length);
    filextension=filextension.toLowerCase();
    if ((filextension!='.jpg')&&(filextension!='.gif')&&(filextension!='.jpeg')&&(filextension!='.png')&&(filextension!='.bmp')){
        alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢");
        imgFile.focus();
        $("#txHidden").val("0");
        return;
    }
    var temp = imgFile.files[0].size;
    var fileSize = temp/1024;
    if(fileSize > 512){
        alert("头像大小不能超过512kb");
        $("#txHidden").val("0");
        return;
    }
    $("#txHidden").val("1");
}


/******************** by Zhangerxin Start ********************/

//债权转让页面跳转
function doMyGoDebt(){
    var debt = {menuUrl:'/front/dabet/dabet.jetx'};
    $.ajax({
        type:"post",
        url:"/attornRecord/queryNotAttornByPage",
        data:debt,
        dataType:"json",
        success:function(data){
            $(".p_right_wordbox").html(data.htmlText);
        },
        err:function(data){
            alert(data.info)
        }
    });
}
/********************* by Zhangerxin End *********************/