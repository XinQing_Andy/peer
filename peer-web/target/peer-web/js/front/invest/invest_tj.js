/**
 * Created by cuibin on 2016/6/12.
 */
/**
 * 跳转到投资统计
 */
function queryinvestTj(){
    $.ajax({
        type:"post",
        url:"/invert/openinvestTj",
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
            }
        },
        err:function(data){
            alert('查询失败');
        }
    });
}
/**
 * 切换到债权转让统计
 */
function openTransferTj (){
    $(".touzi-tj-hide").css("display","block");
    $(".touzi-tj-show").css("display","none");
    $("#oneLi").attr("class","l-tab-r");
    $("#twoLi").attr("class","l-tab-l l-hover");
}
/**
 * 切换到投资统计
 */
function openinvestTj(){
    $(".touzi-tj-hide").css("display","none");
    $(".touzi-tj-show").css("display","block");
    $("#twoLi").attr("class","l-tab-r");
    $("#oneLi").attr("class","l-tab-l l-hover");
}