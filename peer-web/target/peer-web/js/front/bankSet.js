/**
 * Created by cuibin on 2016/5/18.
 */

/**
 *  跳转到银行卡设置首页
 */
function openBankSet(){
    $.ajax({
        type:"post",
        url:"/bankSet/openBankSet",
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
            }
        },
        err:function(data){

        }
    });
}


/**
 * 跳转到添加银行卡
 */
function openBankAdd(){
    $.ajax({
        type:"post",
        url:"/bankSet/openBankAdd",
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert(data.info);
            }
        },
        err:function(data){
            alert('查询失败');
        }
    });
}

//设置提现银行卡
function setPostalBankNo(){
    $.ajax({
        type:"post",
        url:"/huanxun/login",
        dataType:"json",
        success:function(data){
            if(data.code == '0'){ //成功
                $("#userName").val(data.userName); //用户名
                $("#merchantId").val('1810060028'); //商户存管交易账号
                $("#bankSet").submit();
            }else{
                alert(data.info);
            }
        },
        err:function(data){

        }
    });
}


/**
 * 城市级联
 * @param fid
 */
function openBankAddQueryCity(fid){
    var temp = {provinceid:fid};
    $.ajax({
        type:"post",
        url:"/bankSet/openBankAddQueryCity",
        data:temp,
        dataType:"json",
        success:function(data){
            $("#city").html(data.cityPj);
        },
        err:function(data){

        }
    });
}

/**
 * 保存银行 disabled="disabled"
 */
function saveFrontBank(){
    $("#bankSaveButton").attr("disabled","disabled");
    var bankid = $("#bankid").val();
    var province = $("#province").val();
    var city = $("#city").val();
    var bankaddress = $("#bankaddress").val();

    var reg = /^\s+$/gi;
    if(bankid == "none"){
        alert("银行名称不能为空");
        $("#bankSaveButton").removeAttr("disabled");
        return;
    }
    if(province == "none"){
        alert("提现银行卡地区不能为空");
        $("#bankSaveButton").removeAttr("disabled");
        return;
    }
    if(city == "none"){
        alert("提现银行卡地区不能为空");
        $("#bankSaveButton").removeAttr("disabled");
        return;
    }
    if(bankaddress == "" || reg.test(bankaddress)){
        alert("开户行支行名称不能为空");
        $("#bankSaveButton").removeAttr("disabled");
        return;
    }
    var temp = {
        bankid:bankid,
        province:province,
        city:city,
        bankaddress:bankaddress
    };
    $.ajax({
        type:"post",
        url:"/bankSet/saveFrontBank",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert(data.info);
                openBankSet();
            }else{
                alert(data.info);
            }
        },
        err:function(data){

        }
    });

}
/**
 * 打开更新银行界面
 */
function openUpdatebank(){

    $.ajax({
        type:"post",
        url:"/bankSet/openUpdatebank",
        dataType:"json",
        success:function(data){
            $(".p_right_wordbox").html(data.htmlText);
        },
        err:function(data){

        }
    });
}
/**
 * 更新银行
 */
function frontUpdateBank(){
    $("#bankUpdateButton").attr("disabled","disabled");

    var bankid = $("#bankid").val();
    var province = $("#province").val();
    var city = $("#city").val();
    var bankaddress = $("#bankaddress").val();

    var reg = /^\s+$/gi;

    if(bankid == "none"){
        alert("银行名称不能为空");
        $("#bankUpdateButton").removeAttr("disabled");
        return;
    }
    if(province == "none"){
        alert("提现银行卡地区不能为空");
        $("#bankUpdateButton").removeAttr("disabled");
        return;
    }
    if(city == "none"){
        alert("提现银行卡地区不能为空");
        $("#bankUpdateButton").removeAttr("disabled");
        return;
    }
    if(bankaddress == "" || reg.test(bankaddress)){
        alert("开户行支行名称不能为空");
        $("#bankUpdateButton").removeAttr("disabled");
        return;
    }

    var temp = {
        bankid:bankid,
        province:province,
        city:city,
        bankaddress:bankaddress,
    };
    $.ajax({
        type:"post",
        url:"/bankSet/frontUpdateBank",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                alert(data.info);
                openBankSet();
            }else{
                alert(data.info);
                $("#bankUpdateButton").removeAttr("disabled");
            }
        },
        err:function(data){

        }
    });

}
/**
 * 更新银行卡获取手机验证码
 */
function updateBankGetCode(){
    $("#hqyzm").attr({"disabled": "true"});
    $.ajax({
        type: "post",
        url: "/bankSet/updateBankGetCode",
        success: function (data) {
            if (data.result == 'success') {
                $("#message").html(data.info);
                updateBankTime();
            } else {
                $("#message").html(data.info);

            }

        },
        err: function (data) {

        }
    });
}

function updateBankTime() {

    var num = $("#timeSum").val();
    if (num > 0) {
        num--;
        $("#hqyzm").val(num + "秒后可重发");
        $("#timeSum").val(num);
    } else {
        $("#hqyzm").removeAttr("disabled");
        $("#hqyzm").val("获取验证码");
        $("#timeSum").val(61);
        return;
    }
    setTimeout(function () {
        updateBankTime()
    }, 1000);
}

