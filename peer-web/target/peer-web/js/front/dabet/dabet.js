<!-- 债权转让 -->

//获取可转让的债权
function queryAttornFq(){
    //产品部分(type:类型0-持有、1-转让中、2-已转让,str:审核状态)
    var productAttornMes = {type:'0',str:'0',orderColumn:'BUYTIME',orderType:'DESC',investFlag:'0',menuUrl:'front/dabet/dabet.jetx'};

    $.ajax({
        type:"post",
        url:"/attornRecord/queryAttornFq",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $(".p_right_wordbox").html(data.htmlText);
        },
        err:function(data){
            console.log(data);
        }
    });
}


//根据条件获取所有可转让的债权
function queryNotAttorn(){
    var investTime = $("#investTimeOne").val(); //查询日期
    var startTime = $("#startTimeOne").val(); //查询起日期
    var endTime = $("#endTimeOne").val(); //查询止日期
    var keyWordDabet = $("#keyWordDabetOne").val(); //关键字
    var keyWordValue = $("#keyWordValueOne").val(); //查询关键字值
    var buyStartTime = ''; //投资起日期
    var buyEndTime = ''; //投资止日期
    var expireStsrtTime = ''; //到期起日期
    var expireEndTime = ''; //到期止日期
    var title = ''; //题目
    var code = ''; //编码
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量

    if(typeof (pageIndex) == 'undefined'){
        pageIndex = '';
    }
    if(typeof (pageSize) == 'undefined'){
        pageSize = '';
    }

    /********** 查询条件Start **********/
    if(investTime != ''){ //查询日期
        if(investTime == '1'){ //投资时间
            buyStartTime = startTime; //投资起日期
            buyEndTime = endTime; //投资止日期
        }else if(investTime == '2'){ //到期时间
            expireStsrtTime = startTime; //到期起日期
            expireEndTime = endTime; //到期止日期
        }
    }

    if(keyWordDabet != ''){ //关键字
        if(keyWordDabet == '1'){ //标的编号
            code = keyWordValue;
        }else if(keyWordDabet == '2'){ //标的名称
            title = keyWordValue;
        }
    }
    /*********** 查询条件End ***********/

    //产品部分(str:审核状态)
    var productAttornMes = {buyStartTime:buyStartTime,buyEndTime:buyEndTime,expireStsrtTime:expireStsrtTime,expireEndTime:expireEndTime,investFlag:'0',
                            code:code,title:title,type:'0',str:'0',orderColumn:'BUYTIME',orderType:'DESC',pageIndex:pageIndex,pageSize:pageSize};
    $.ajax({
        type:"post",
        url: "/attornRecord/queryNotAttornByPage",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $("#oneDiv").html(data.str); //查询列表
            $("#pageNotAttorn").html(data.str2); //分页部分

            $("#liAttorn1").removeClass('active'); //可转让的债权
            $("#liAttorn1").addClass('active'); //可转让的债权
            $("#liAttorn2").removeClass('active'); //转让中的债权
            $("#liAttorn3").removeClass('active'); //已购买的债权
            $("#liAttorn4").removeClass('active'); //已转让的债权
            $("#divAttorn1").css('display','block');
            $("#divAttorn2").css('display','none');
            $("#divAttorn3").css('display','none');
            $("#divAttorn4").css('display','none');
        },
        err:function(data){
            console.log(data);
        }
    });
}


/**
 * 根据条件获取所有转让中的债权
 */
function queryAttorning(){
    var investTime = $("#investTimeTwo").val(); //查询日期
    var startTime = $("#startTimeTwo").val(); //查询起日期
    var endTime = $("#endTimeTwo").val(); //查询止日期
    var keyWordDabet = $("#keyWordDabetTwo").val(); //关键字
    var keyWordValue = $("#keyWordValueTwo").val(); //查询关键字值
    var attornStartTime = ''; //转让起日期
    var attornEndTime = ''; //转让止日期
    var expireStsrtTime = ''; //到期起日期
    var expireEndTime = ''; //到期止日期
    var title = ''; //题目
    var code = ''; //编码
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量

    /*************** 查询条件Start ***************/
    if(investTime != ''){ //查询日期
        if(investTime == '1'){ //转让时间
            attornStartTime = startTime; //投资起日期
            attornEndTime = endTime; //投资止日期
        }else if(investTime == '2'){ //到期时间
            expireStsrtTime = startTime; //到期起日期
            expireEndTime = endTime; //到期止日期
        }
    }

    if(keyWordDabet != ''){ //关键字
        if(keyWordDabet == '1'){ //标的编号
            code = keyWordValue;
        }else if(keyWordDabet == '2'){ //标的名称
            title = keyWordValue;
        }
    }
    /**************** 查询条件End ****************/

    var productAttornMes = {attornStartTime:attornStartTime,attornEndTime:attornEndTime,expireStartTime:expireStsrtTime,expireEndTime:expireEndTime,
                            code:code,title:title,type:'1',pageIndex:pageIndex,pageSize:pageSize};
    $.ajax({
        type:"post",
        url: "/attornRecord/queryTransferAttornByPage",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $("#twoDiv").html(data.str); //列表部分拼接
            $("#pageAttorning").html(data.pageStr); //分页部分拼接
        },
        err:function(data){
            console.log(data);
        }
    });
}


/**
 * 已购买的债权
 */
function queryAlreadyBuyByPage(){
    var investTime = $("#investTimeThree").val(); //时间类型(1-购买时间、2-到期时间)
    var startTime = $("#startTimeThree").val(); //起日期
    var endTime = $("#endTimeThree").val(); //止日期
    var keyWord = $("#keyWordDabetThree").val(); //关键字类型(1-编号、2-名称)
    var value = $("#keyWordValueThree").val(); //关键字值
    var pageIndex = $("#pageIndex").val(); //当前页
    var pageSize = $("#pageSize").val(); //每页显示数量
    var undertaketimeStart = ''; //承接起时间
    var undertaketimeEnd = ''; //承接止时间
    var expireStartTime = ''; //到期起日期
    var expireEndTime = ''; //到期止日期
    var title = ''; //题目
    var code = ''; //编码

    if(typeof (pageIndex) == "undefined"){
        pageIndex = "";
    }
    if(typeof (pageSize) == "undefined"){
        pageSize = "";
    }

    if(investTime != ''){
        if(investTime == '1'){ //购买时间(承接时间)
            undertaketimeStart = startTime;
            undertaketimeEnd = endTime;
        }else if(investTime == '2'){ //到期时间
            expireStartTime = startTime;
            expireEndTime = endTime;
        }
    }

    if(keyWord != ''){
        if(keyWord == '1'){ //编号
            code = value;
        }else if(keyWord == '2'){ //名称
            title = value;
        }
    }

    var productAttornMes = {undertaketimeStart:undertaketimeStart,undertaketimeEnd:undertaketimeEnd,expireStartTime:expireStartTime,
                            expireEndTime:expireEndTime,code:code,title:title,shFlag:'0'};

    $.ajax({
        type:"post",
        url:"/attornRecord/queryInvestedAttornByPage",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $("#tbodyBuyedAttorn").html(data.str); //列表部分拼接
            $("#ulInvestedAttorn").html(data.pageStr); //分页部分拼接
        },
        err:function(data){
            console.log(data);
        }
    });
}


/**
 * 债权详细内容
 * @param attornId
 * @param buyId
 * @param productId
 */
function doQueryAttornProductDetails(attornId,investId,productId){

    $.ajax({
        type:"post",
        url:"/attornRecord/doQueryAttornProductDetails",
        data:"attornId="+attornId+"&investId="+investId+"&productId="+productId,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){ //成功
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            alert(data);
        }
    });
}

//打开债权合同
function openAttornConTract(attornId,investId,productId){
    var url = ''; //后台跳转地址
    var contractType = $("#contractType").val(); //合同类型
    url = '/conTract/openAttornContract?attornId='+attornId+'&investId='+investId+'&productId='+productId;
    window.open(url);
}


//已转让债权
function queryAlreadyTransferByPage(){
    var investTime = $("#investTimeFour").val(); //日期类型
    var startTime = $("#startTimeFour").val(); //查询起日期
    var endTime = $("#endTimeFour").val(); //查询止日期
    var keyWordDabet = $("#keyWordDabetFour").val(); //关键字
    var keyWordValue = $("#keyWordValueFour").val(); //关键值
    var attornStartTime = ''; //转让起日期
    var attornEndTime = ''; //转让止日期
    var expireStsrtTime = ''; //到期起日期
    var expireEndTime = ''; //到期止日期
    var title = ''; //题目
    var code = ''; //编码

    /********** 查询条件Start **********/
    if(investTime != ''){ //查询日期
        if(investTime == '1'){
            attornStartTime = startTime; //转让起日期
            attornEndTime = endTime; //转让止日期
        }else if(investTime == '2'){ //到期时间
            expireStsrtTime = startTime; //到期起日期
            expireEndTime = endTime; //到期止日期
        }
    }

    if(keyWordDabet != ''){ //关键字
        if(keyWordDabet == '1'){ //标的编号
            code = keyWordValue;
        }else if(keyWordDabet == '2'){ //标的名称
            title = keyWordValue;
        }
    }
    /*********** 查询条件End ***********/

    var productAttornMes = {attornStartTime:attornStartTime,attornEndTime:attornEndTime,expireStsrtTime:expireStsrtTime,
                            expireEndTime:expireEndTime,code:code,title:title,shFlag:'0'};

    $.ajax({
        type:"post",
        url: "/attornRecord/queryAlreadyTransferAttorn",
        data:productAttornMes,
        dataType:"json",
        success:function(data){
            $("#fourDiv").html(data.str); //列表部分拼接
            $("#alreadyTransfer").html(data.pageStr); //分页部分拼接
        },
        err:function(data){
            console.log(data);
        }
    });
}


//根据选择的债权类型显示相应的内容
function goDebtTab(tab){
    if(tab == '1'){ //可转让的债权
        $("#liAttorn1").removeClass('active');
        $("#liAttorn1").addClass('active');
        $("#liAttorn2").removeClass('active');
        $("#liAttorn3").removeClass('active');
        $("#liAttorn4").removeClass('active');
        $("#divAttorn1").css('display','none');
        $("#divAttorn2").css('display','block');
        $("#divAttorn3").css('display','none');
        $("#divAttorn4").css('display','none');
        $("#pageAttorning").html(''); //转账中的债权部分分页
        $("#ulInvestedAttorn").html(''); //已购买的债权部分分页
        $("#alreadyTransfer").html(''); //已转让的债权部分分页
        queryNotAttorn();
    }else if(tab == '2'){ //转让中的债权
        $("#liAttorn1").removeClass('active');
        $("#liAttorn2").removeClass('active');
        $("#liAttorn2").addClass('active');
        $("#liAttorn3").removeClass('active');
        $("#liAttorn4").removeClass('active');
        $("#divAttorn1").css('display','none');
        $("#divAttorn2").css('display','block');
        $("#divAttorn3").css('display','none');
        $("#divAttorn4").css('display','none');
        $("#pageNotAttorn").html(''); //可转让的债权部分分页
        $("#ulInvestedAttorn").html(''); //已购买的债权部分分页
        $("#alreadyTransfer").html(''); //已转让的债权部分分页
        queryAttorning();
    }else if(tab == '3'){ //已购买的债权
        $("#liAttorn1").removeClass('active');
        $("#liAttorn2").removeClass('active');
        $("#liAttorn3").removeClass('active');
        $("#liAttorn3").addClass('active');
        $("#liAttorn4").removeClass('active');
        $("#divAttorn1").css('display','none');
        $("#divAttorn2").css('display','none');
        $("#divAttorn3").css('display','block');
        $("#divAttorn4").css('display','none');
        $("#pageNotAttorn").html(''); //可转让的债权部分分页
        $("#pageAttorning").html(''); //转账中的债权部分分页
        $("#alreadyTransfer").html(''); //已转让的债权部分分页
        queryAlreadyBuyByPage();
    }else if(tab == '4'){ //已转让的债权
        $("#liAttorn1").removeClass('active');
        $("#liAttorn2").removeClass('active');
        $("#liAttorn3").removeClass('active');
        $("#liAttorn4").removeClass('active');
        $("#liAttorn4").addClass('active');
        $("#divAttorn1").css('display','none');
        $("#divAttorn2").css('display','none');
        $("#divAttorn3").css('display','none');
        $("#divAttorn4").css('display','block');
        $("#pageNotAttorn").html(''); //可转让的债权部分分页
        $("#pageAttorning").html(''); //转账中的债权部分分页
        $("#ulInvestedAttorn").html(''); //已购买的债权部分分页
        queryAlreadyTransferByPage();
    }
}


function dabetdetails(id)
{
    var productMes = {id:id,menuUrl:'front/dabet/dabet_details.jetx'};

    $.ajax({
        type:"post",
        url: "/attornRecord/queryAttronDetails",
        data:productMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    })

}


//进入债权转让操作页面
function dabetattornOper(id) {
    var productInvestMes = {id:id,menuUrl:'front/dabet/dabet_attornoper.jetx'};

    $.ajax({
        type:"post",
        url: "/attornRecord/queryAttronOper",
        data:productInvestMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//获取预期收益率
function getExpectedRate(){
    var expectedRate = ''; //预期收益率
    var priceMin = $("#priceMin").val(); //最低转让价格
    var priceMax = $("#priceMax").val(); //最高转让价格
    var surplusPricipal = $("#surplusPricipal").val(); //剩余本金
    var loanRate = $("#loanRate").val(); //贷款利率
    var nextReceiveTime = $("#nextReceiveTime").val(); //下次回款日期
    var attornPrice = $("#attornPrice").val(); //转让价格

   if(parseFloat(attornPrice)<parseFloat(priceMin)){
        alert('转让价格需大于最低转让价格!');
        $("#attornPrice").val('');
        return;
   }
    if(parseFloat(attornPrice)>parseFloat(priceMax)){
        alert('转让价格需小于最高转让价格!');
        $("#attornPrice").val('');
        return;
    }
    if(attornPrice == '' || typeof (attornPrice) == 'undefined'){
        $("#tdexpectedRate").html('');
        return;
    }


    $.ajax({
        type:"post",
        url: "/attornRecord/getExpectedRate",
        data: "surplusPricipal="+surplusPricipal+"&loanRate="+loanRate+"&nextReceiveTime="+nextReceiveTime+"&attornPrice="+attornPrice,
        dataType:"json",
        success:function(data){
            expectedRate = data.expectedRate; //预期收益率
            $("#expectedRate").val(expectedRate);
            expectedRate = expectedRate + '%';
            $("#tdexpectedRate").html(expectedRate);
        },
        err:function(data){
            console.log(data);
        }
    });
}


//债权转让 详情页面 type
function dabetinvestdetails(id,type)
{
    // if(type="queryNotAttornByPage")
    // {//查询所有待转让的债权

    // }
    // else if(type=="queryMyAttornByPage")
    // {//已购买 已转让 债权

    // }
    // else if(type=="queryTransferByPage")
    // {//转让中债权

    // }
    var productMes = {id:id,type:type,menuUrl:'front/dabet/dabet_details.jetx'};

    $.ajax({
        type:"post",
        url: "/attornRecord/queryInvestDetails",
        data:productMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    })

}

//转让 详情页面 type
function dabetmake(id,type)
{
    var productMes = {id:id,type:type,menuUrl:'front/dabet/dabet_details.jetx'};

    $.ajax({
        type:"post",
        url: "/attornRecord/queryInvestDetails",
        data:productMes,
        dataType:"json",
        success:function(data){
            if(data.result == 'success'){
                $(".p_right_wordbox").html(data.htmlText);
            }else{
                alert('查询失败！');
            }
        },
        err:function(data){
            console.log(data);
        }
    })

}

//发起债权转让
function zhuanrang() {

    var buyId = $("#buyId").val(); //产品购买id
    var price = $("#attornPrice").val(); //转让价格
    var expectedRate = $("#expectedRate").val(); //预期收益率

    if(expectedRate.length == 0){
        alert('预期收益率不能为空！');
        return;
    }

    $.ajax({
        type:"post",
        url: "/attornRecord/confirmAttorn",
        data:{productBuyId:buyId,price:price,expectedRate:expectedRate},
        dataType:"json",
        success:function(data){
            if(data.code == 'success'){
                alert(data.info);
                queryAttornFq();
            }else{
                alert(data.info);
                queryAttornFq();
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

/********************* by zhangerxin End *********************/

/*********************by cuibin *********************/

/*********************by cuibin end*********************/