/**
 * Created by lenovo on 2016/3/9.
 */
/**
 * 生成订单
 */
function orderOpen(addId) {
    var radioName;
    var packageId;
    var info = document.getElementsByName("splitFlg");
    for(var i = 0; i < info.length; i ++) {
        if (info[i].checked) {
            radioName = info[i].value;
            console.log(radioName)
        }
    }
    var id=document.getElementsByName("id");
    for(var i = 0; i < id.length; i ++) {
        if (id[i].checked) {
            packageId = id[i].value;
            console.log(packageId)
        }
    }
    var defer = $.Deferred();
    var parm = {id:packageId,addId: addId,splitFlg:radioName};
    $.ajax({
        type: "post",
        url: "/manager/updateAddInfo",
        data: $.param(parm),
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
            $("#tabCon").empty();
            $('#tabCon').html(data.htmlText);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();

}
/*
function checkboxInfo(userInfo, conUrl, userMenu) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: conUrl,
        data: userMenu,
        dataType: userInfo,
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
*/
function chk(){
    var obj=document.getElementsByName('id'); //选择所有name="'id'"的对象，返回数组
//取到对象数组后，我们来循环检测它是不是被选中
    var s='';
    for(var i=0; i<obj.length; i++){
        if(obj[i].checked) s+=obj[i].value+','; //如果选中，将value添加到变量s中
    }
//那么现在来检测s的值就知道选中的复选框的值了
    alert(s==''?'你还没有选择任何内容！':s);
}

function jqchk(){ //jquery获取复选框值
    var chk_value =[];
    $('input[name="test"]:checked').each(function(){
        chk_value.push($(this).val());
    });
    alert(chk_value.length==0 ?'你还没有选择任何内容！':chk_value);
}