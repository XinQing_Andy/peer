/**
 * Created by zhangerxin on 2016/4/6.
 */


//按条件查询用户列表
function querySysUserInfo(){
    var sysKeyWord = document.getElementById("sysKeyWord").value; //查询关键字
    var sysUserMes = {sysKeyWord:sysKeyWord,menuUrl:'sys/system.jetx'};

    $.ajax({
        type:"post",
        url: "/manage/queryUserByPage",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//弹出增加框
function doAddSys(){
    var bDiv = document.getElementById('myModalAdd');
    bDiv.style.display = "block";
}

//用户部分点击“取消”后的操作
function doSysCancel(style){
    if(style == 'add'){
        var bDiv = document.getElementById('myModalAdd');
        bDiv.style.display = "none";
    }else if(style == 'edit'){
        var bDiv = document.getElementById('myModalEdit');
        bDiv.style.display = "none";
    }
}

//角色列表部分点击“取消”后的操作
function doSysSetCancel(style){
    if(style == 'add'){
        var bDiv = document.getElementById('myModalAddSet');
        bDiv.style.display = "none";
    }else if(style == 'edit'){
        var bDiv = document.getElementById('myModalEditSet');
        bDiv.style.display = "none";
    }
}

//新增弹出框设置部分
function doAddSet(){
    var pageIndex = "1"; //当前数
    var pageSize = "10"; //每页显示数量
    var currentPage = "1"; //当前页
    var sysRoleMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};

    /********** 查询角色Start **********/
    $.ajax({
        type:"post",
        url: "/manage/querySetRoleList",
        data:sysRoleMes,
        dataType:"json",
        success:function(data){
            $("#tableAddSet").html(data.htmlText);
            $("#totalCountRole").val(data.totalCount); //角色总数量
            $("#currentPageRole").val(data.currentPage); //当前页
            $("#trAddSetting").html(data.roleStr); //角色翻页拼接
            $("#qfFlag").val('add');
            var bDiv = document.getElementById('myModal');
            bDiv.style.display = "block";
        },
        err:function(data){
            console.log(data);
        }
    });
    /********** 查询角色End **********/

    var bDiv = document.getElementById('myModalAddSet');
    bDiv.style.display = "block";
}


//编辑弹出框设置部分
function doEditSet(){
    var pageIndex = "1"; //当前页doEditSys
    var pageSize = "10"; //每页显示数量
    var currentPage = "1"; //当前页
    var roleId = $("#roleIdEdit").val(); //角色id
    var sysRoleMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,id:roleId};

    /********** 查询角色Start **********/
    $.ajax({
        type:"post",
        url: "/manage/querySetEditRoleList",
        data:sysRoleMes,
        dataType:"json",
        success:function(data){

            $("#tableEditSet").html(data.htmlText);
            $("#totalCountRole").val(data.totalCount); //角色总数量
            $("#currentPageRole").val(data.currentPage); //当前页
            $("#trEditSetting").html(data.roleStr); //角色翻页拼接
            $("#roleId").val(roleId); //当前用户角色

            var bDiv = document.getElementById('myModalEdit');
            bDiv.style.display = "block";
            $("#qfFlag").val('edit'); //状态
        },
        err:function(data){
            console.log(data);
        }
    });
    /********** 查询角色End **********/

    var bDiv = document.getElementById('myModalEditSet');
    bDiv.style.display = "block";
}


/************* 角色部分Start *************/

//选择角色时操作(角色id,角色名称,类型)
function doCheck(roleId,RoleName,style){
    if(style == 'add'){
        $("#roleIdAdd").val(roleId);
        $("#settingAdd2").html(RoleName);
    }else if(style == 'edit'){
        $("#roleIdEdit").val(roleId);
        $("#settingEdit2").html(RoleName);
    }
}

//角色列表部分点击“确定”后的操作
function doCkeckConfirm(style){
    if(style == 'add'){
        var bDiv = document.getElementById('myModalAddSet');
        bDiv.style.display = "none";
    }else if(style == 'edit'){
        var bDiv = document.getElementById('myModalEditSet');
        bDiv.style.display = "none";
    }
}


//新增用户(保存操作)
function addSysConfirm(){
    var userName = $("#userNameAdd").val(); //登录名
    var isAdmin = $("#isAdminAdd").val(); //管理级别(0-超级管理员、1-普通管理员)
    var realName = $("#realNameAdd").val(); //真实姓名
    var cphone = $("#cphoneAdd").val(); //手机
    var email = $("#emailAdd").val(); //邮箱
    var roleId = $("#roleIdAdd").val(); //角色id
    var sysUserMes = {userName:userName,isAdmin:isAdmin,realName:realName,cphone:cphone,email:email,roleId:roleId};

    if(userName.length == 0){
        alert('请输入管理员账号!');
        return;
    }
    if(isAdmin.length == 0){
        alert('请选择管理员级别!');
        return;
    }
    if(roleId.length == 0){
        alert('请设置管理员组!');
        return;
    }
    if(realName.length == 0){
        alert('请填写真实姓名!');
        return;
    }

    $.ajax({
        type:"post",
        url: "/manage/addUser",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result == "error"){
                alert(data.info);
            }else{
                alert(data.info);
                querySysUserInfo();
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//编辑用户
function doEditSys(id){
    var sysUserMes = {id:id};

    $.ajax({
        type:"post",
        url: "/manage/queryUserById",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            $("#idEdit").val(id); //主键
            $("#userNameEdit").val(data.userName); //用户名
            $("#roleIdEdit").val(data.roleId); //角色id
            $("#isAdminEdit").find("option[text='1']").attr("selected",true);
            $("#settingEdit2").html(data.roleName); //角色名称
            $("#realNameEdit").val(data.realName); //真实姓名
            $("#cphoneEdit").val(data.cphone); //手机
            $("#emailEdit").val(data.email); //邮箱

            /********** 打开编辑窗口Start **********/
            var bDiv = document.getElementById('myModalEdit');
            bDiv.style.display = "block";
            /*********** 打开编辑窗口End ***********/
        },
        err:function(data){
            console.log(data);
        }
    });
}

//编辑中确定操作(保存操作)
function editSysConfirm(){
    var id = $("#idEdit").val(); //主键
    var userName = $("#userNameEdit").val(); //用户名
    var isAdmin = $("#isAdminEdit").val(); //是否为管理员
    var roleId = $("#roleIdEdit").val(); //角色id
    var realName = $("#realNameEdit").val(); //真实姓名
    var cphone = $("#cphoneEdit").val(); //手机
    var email = $("#emailEdit").val(); //邮箱

    var sysUserMes = {id:id,userName:userName,isAdmin:isAdmin,realName:realName,cphone:cphone,email:email,roleId:roleId};

    if(id.length == 0){
        alert('该用户信息未获取到!');
        return;
    }
    if(userName.length == 0){
        alert('请填写管理员账号!');
        return;
    }
    if(isAdmin.length == 0){
        alert('请选择管理员等级!');
        return;
    }
    if(roleId.length == 0){
        alert('请选择管理员组!');
        return;
    }
    if(realName.length == 0){
        alert('请填写真实姓名!');
        return;
    }

    $.ajax({
        type:"post",
        url: "/manage/updateUser",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result == "error"){
                alert(data.info);
            }else{
                alert(data.info);
                querySysUserInfo();
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

/**
 * 设置用户状态
 * @param id
 * @param isOnline
 */
function settingFlag(id,isOnline){
    var sysUserMes = {id:id,isOnline:isOnline};

    $.ajax({
        type:"post",
        url: "/manage/settingSysFlag",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result == "error"){
                alert(data.info);
            }else{
                alert(data.info);
                querySysUserInfo();
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//打开重置密码窗口
function openResetPwd(id){
    var bDiv = document.getElementById('myPassWord');
    bDiv.style.display = "block";
    $("#IdPwd").val(id);
}

//重置密码
function doResetPwd(){
    var id = $("#IdPwd").val(); //主键
    var newPwd = $("#newPwd").val(); //新密码
    var confirmPwd = $("#confirmPwd").val(); //确认密码
    if(newPwd != confirmPwd){
        alert('所设密码和确定密码不一致，请核对后重新输入！');
        return;
    }
    var sysUser = {id:id,passWord:newPwd};

    $.ajax({
        type:"post",
        url: "/manage/resetPwd",
        data:sysUser,
        dataType:"json",
        success:function(data){
            if(data.result == "error"){
                alert(data.info);
            }else{
                alert(data.info);
                querySysUserInfo();
            }
        },
        err:function(data){
            console.log(data);
        }
    });

}


/******************** 分页部分(用户)Start ********************/

//首页
function SysStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var sysKeyWord = document.getElementById("sysKeyWord").value; //查询关键字
    var sysUserMes = {sysKeyWord:sysKeyWord,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system.jetx'};
    getSysUserInfoBySome(sysUserMes); //根据条件查询用户
}
//尾页
function SysEndPage(){
    var totalCount = $("#sysTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var sysKeyWord = document.getElementById("sysKeyWord").value; //查询关键字
    var sysUserMes = {sysKeyWord:sysKeyWord,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system.jetx'};
    getSysUserInfoBySome(sysUserMes); //根据条件查询用户
}
//根据条件查询用户
function getSysUserInfoBySome(sysUserMes){
    $.ajax({
        type:"post",
        url: "/manage/queryUserByPage",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//上一页
function SystemUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var sysKeyWord = document.getElementById("sysKeyWord").value; //查询关键字
    var sysUserMes = {sysKeyWord:sysKeyWord,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system.jetx'};
    getSysUserInfoBySome(sysUserMes); //根据条件查询用户
}
//下一页
function SystemDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#sysTotalCount").val(); //总数量
    var pageSize = "10";
    var sysKeyWord = document.getElementById("sysKeyWord").value; //查询关键字

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var sysUserMes = {sysKeyWord:sysKeyWord,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system.jetx'};
    getSysUserInfoBySome(sysUserMes); //根据条件查询用户
}
//跳转到指定页数
function doSysGoPage(){
    var systemNum = $('#systemNum').val(); //跳转到第几页
    if(systemNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(systemNum)){
        alert('请输入合法的数字！');
        return;
    }
    var sysKeyWord = document.getElementById("sysKeyWord").value; //查询关键字
    var currentPage = systemNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var sysUserMes = {sysKeyWord:sysKeyWord,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system.jetx'};
    getSysUserInfoBySome(sysUserMes); //根据条件查询用户
}
/******************** 分页部分(用户)End ********************/



/*************** 分页部分(用户中角色)Start ***************/

//查询角色
function getSysUserRole(sysUserRoleMes){
    $.ajax({
        type:"post",
        url: "/manage/querySetRoleList",
        data:sysUserRoleMes,
        dataType:"json",
        success:function(data){
            $("#tableAddSet").html(data.htmlText);
            $("#totalCountRole").val(data.totalCount); //角色总数量
            $("#currentPageRole").val(data.currentPage); //当前页
            $("#trAddSetting").html(data.roleStr); //角色翻页拼接
            var bDiv = document.getElementById('myModal');
            bDiv.style.display = "block";
        },
        err:function(data){
            console.log(data);
        }
    });
}

//查询角色(编辑部分)
function getSysUserRoleEdit(sysUserRoleMes){
    $.ajax({
        type:"post",
        url: "/manage/querySetEditRoleList",
        data:sysUserRoleMes,
        dataType:"json",
        success:function(data){
            $("#tableEditSet").html(data.htmlText);
            $("#totalCountRole").val(data.totalCount); //角色总数量
            $("#currentPageRole").val(data.currentPage); //当前页
            $("#trEditSetting").html(data.roleStr); //角色翻页拼接

            var bDiv = document.getElementById('myModal');
            bDiv.style.display = "block";
            $("#qfFlag").val('edit'); //状态
        },
        err:function(data){
            console.log(data);
        }
    });
}


//首页
function SysUserRoleStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var qfFlag = $("#qfFlag").val(); //区分状态
    var roleId = $("#roleId").val(); //角色id
    var sysUserRoleMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,id:roleId};
    if(qfFlag == 'add'){
        getSysUserRole(sysUserRoleMes); //查询用户角色
    }else if(qfFlag == 'edit'){
        getSysUserRoleEdit(sysUserRoleMes)
    }

}

//尾页
function SysUserRoleEndPage(){
    var totalCount = $("#totalCountRole").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    var qfFlag = $("#qfFlag").val(); //区分状态
    var roleId = $("#roleId").val(); //角色id
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }

    var pageSize = '10'; //每页显示数量
    var sysUserRoleMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,id:roleId};
    if(qfFlag == 'add'){
        getSysUserRole(sysUserRoleMes); //查询用户角色
    }else if(qfFlag == 'edit'){
        getSysUserRoleEdit(sysUserRoleMes)
    }
}

//上一页
function SysUserRoleUp(){
    var currentPage = $("#currentPageRole").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var qfFlag = $("#qfFlag").val(); //区分状态
    var roleId = $("#roleId").val(); //角色id
    var sysUserRoleMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,id:roleId};

    if(qfFlag == 'add'){
        getSysUserRole(sysUserRoleMes); //查询用户角色
    }else if(qfFlag == 'edit'){
        getSysUserRoleEdit(sysUserRoleMes)
    }
}

//下一页
function SysUserRoleDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPageRole").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#totalCountRole").val(); //总数量
    var pageSize = "10";

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var qfFlag = $("#qfFlag").val(); //区分状态
    var roleId = $("#roleId").val(); //角色id
    var sysUserRoleMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,id:roleId};
    if(qfFlag == 'add'){
        getSysUserRole(sysUserRoleMes); //查询用户角色
    }else if(qfFlag == 'edit'){
        getSysUserRoleEdit(sysUserRoleMes)
    }
}

//跳转到指定页数
function doSysUserRoleGoPage(page){
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var qfFlag = $("#qfFlag").val(); //区分状态
    var roleId = $("#roleId").val(); //角色id
    var sysUserRoleMes = {pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,id:roleId};
    if(qfFlag == 'add'){
        getSysUserRole(sysUserRoleMes); //查询用户角色
    }else if(qfFlag == 'edit'){
        getSysUserRoleEdit(sysUserRoleMes)
    }
}
/******************** 分页部分(用户中角色)End ********************/





function doTs(){
    alert('此功能正在研发阶段！');
}

