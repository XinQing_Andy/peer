/**
 * Created by zhangerxin on 2016/4/9.
 */
/*window.onload = function() {
    var aDiv = document.getElementById('sys_btnadd2');
    aDiv.onclick = function() {
        var bDiv = document.getElementById('myModal');
        bDiv.style.display = "block";
    }
    var oDiv = document.getElementById('sys_quxiao5');
    oDiv.onclick = function() {
        var bDiv = document.getElementById('myModal');
        bDiv.style.display = "none";
    }
}*/

//根据安全问题搜索
function querySystemProblem(){
    var sysProblem = $("#sysProblem").val(); //安全问题
    var systemProblem = {question:sysProblem,menuUrl:'sys/system_problem.jetx'};

    $.ajax({
        type:"post",
        url: "/sysSecurityQuestion/querySecurityQuestionByPage",
        data:systemProblem,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//新增问题(弹出窗口)
function doaddSysProblem(){
    var bDiv = document.getElementById('myModalAdd');
    bDiv.style.display = "block";
}

//取消
function doCancelSysProblem(){
    var bDiv = document.getElementById('myModalAdd');
    bDiv.style.display = "none";
}

//新增(保存成功后操作)
function doSaveSysProblem(){
    var question = $("#question").val(); //问题
    var systemProblem = {question:question};

    $.when(AddSysProblem(systemProblem)).done(function(data){
        if(data.result == "error"){
            alert(data.info);
        }else{
            alert(data.info);
            querySystemProblem();
        }
    });
}

//新增(保存)
function AddSysProblem(sysRoleMes){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/sysSecurityQuestion/addSysSecurityQuestion",
        data:sysRoleMes,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

//设置问题(0-启用、1-暂停)
function doSysProblemSetting(id,active_flg){
    var systemProblem = {id:id,active_flg:active_flg};

    $.when(setSysProblem(systemProblem)).done(function(data){
        if(data.result == "error"){
            alert(data.info);
        }else{
            alert(data.info);
            querySystemProblem();
        }
    });
}

//设置状态
function setSysProblem(sysRoleMes){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/sysSecurityQuestion/setSysSecurityQuestion",
        data:sysRoleMes,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}


/*************** 分页部分Start ***************/
//根据条件查询安全问题
function getSysProblemBySome(systemProblem){
    $.ajax({
        type:"post",
        url: "/sysSecurityQuestion/querySecurityQuestionByPage",
        data:systemProblem,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


//首页
function SysProblemStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var sysProblem = $("#sysProblem").val(); //安全问题
    var systemProblem = {question:sysProblem,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_problem.jetx'};
    getSysProblemBySome(systemProblem); //根据条件查询安全问题
}

//尾页
function SysProblemEndPage(){
    var totalCount = $("#totalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var sysProblem = $("#sysProblem").val(); //安全问题
    var systemProblem = {question:sysProblem,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_problem.jetx'};
    getSysProblemBySome(systemProblem); //根据条件查询安全问题
}

//上一页
function SysProblemUpPage(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var sysProblem = $("#sysProblem").val(); //安全问题
    var systemProblem = {question:sysProblem,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_problem.jetx'};
    getSysProblemBySome(systemProblem); //根据条件查询安全问题
}

//下一页
function SysProblemDownPage(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#totalCount").val(); //总数量
    var pageSize = "10";

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var sysProblem = $("#sysProblem").val(); //安全问题
    var systemProblem = {question:sysProblem,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_problem.jetx'};
    getSysProblemBySome(systemProblem); //根据条件查询安全问题
}

//跳转到指定页数
function doSysProblemGoPage(page){
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var sysProblem = $("#sysProblem").val(); //安全问题
    var systemProblem = {question:sysProblem,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_problem.jetx'};
    getSysProblemBySome(systemProblem); //根据条件查询安全问题
}
/*************** 分页部分End ***************/
