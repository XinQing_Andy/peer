//按条件搜索角色
function queryRoleInfo(){
	var roleName = $("#roleName").val(); //角色名称
	var sysRoleMes = {roleName:roleName,menuUrl:'sys/system_admin.jetx'};

	$.ajax({
		type:"post",
		url: "/manage/queryRoleByPage",
		data:sysRoleMes,
		dataType:"json",
		success:function(data){
			if(data.result == 'success'){
				$('#nowPage').html(data.htmlText);
			}else{
				$('#nowPage').after(data.info);
			}
		},
		err:function(data){
			console.log(data);
		}
	});
}

//弹出角色增加框
function doAddSysTemRoleAdmin(){
	var bDiv = document.getElementById('myModalAdd');
	bDiv.style.display = "block"
}


//弹出编辑框
function doEdit(id){
	var sysRoleMes = {id:id}; //主键
	$.ajax({
		type:"post",
		url: "/manage/queryRoleById",
		data:sysRoleMes,
		dataType:"json",
		success:function(data){
			$("#roleIdEdit").val(id); //主键
			$("#roleNameEdit").val(data.roleName); //角色名称
			$("#roleDescriptionEdit").val(data.roleDescription); //角色描述
			var bDiv = document.getElementById('myModalEdit');
			bDiv.style.display = "block";
		},
		err:function(data){
			console.log(data);
		}
	});
}

//新增框取消
function cancelSystemAdminAdd(){
	var bDiv = document.getElementById('myModalAdd');
	bDiv.style.display = "none";
}

//新增角色
function Addconfirm(){
	var roleName = $("#roleNameAdd").val(); //角色名称
	var roleDescription = $("#roleDescriptionAdd").val(); //角色描述
	var sysRoleMes = {roleName:roleName,roleDescription:roleDescription};

	if(roleName.length == 0){
		alert('请输入管理员组!');
		return;
	}

	$.when(Add(sysRoleMes)).done(function(data){
		if(data.result == "error"){
			alert(data.info);
		}else{
			alert(data.info);
			queryRoleInfo();
		}
	});
}


//修改角色
function Editconfirm(){
	var id = $("#roleIdEdit").val(); //主键
	var roleName = $("#roleNameEdit").val(); //角色名称
	var roleDescription = $("#roleDescriptionEdit").val(); //角色描述
	var sysRoleMes = {id:id,roleName:roleName,roleDescription:roleDescription};

	if(id.length == 0){
		alert('用户组信息获取失败!');
		return;
	}
	if(roleName.length == 0){
		alert('请填写用户组!');
		return;
	}

	$.when(EditSysAdmin(sysRoleMes)).done(function(data){
		if(data.result == "error"){
			alert(data.info);
		}else{
			alert(data.info);
			queryRoleInfo();
		}
	});
}

//编辑框取消
function cancelSystemAdminEdit(){
	var bDiv = document.getElementById('myModalEdit');
	bDiv.style.display = "none";
}

//删除角色
function doDel(id){
	var sysRoleMes = {id:id};

	if(confirm("确认删除该角色？")==false){
		return;
	}

	$.when(Del(sysRoleMes)).done(function(data){
		if(data.result == "error"){
			alert(data.info);
		}else{
			alert(data.info);
			queryRoleInfo();
		}
	});
}


//增加角色
function Add(sysRoleMes){
	var defer = $.Deferred();
	$.ajax({
		type:"post",
		url: "/manage/addRole",
		data:sysRoleMes,
		dataType:"json",
		success:function(data){
			defer.resolve(data);
		},
		err:function(data){
			defer.resolve("undefined");
		}
	});
	return defer.promise();
}

//修改角色
function EditSysAdmin(sysRoleMes){
	var defer = $.Deferred();
	$.ajax({
		type:"post",
		url: "/manage/updateRole",
		data:sysRoleMes,
		dataType:"json",
		success:function(data){
			defer.resolve(data);
		},
		err:function(data){
			defer.resolve("undefined");
		}
	});
	return defer.promise();
}

//删除角色
function Del(sysRoleMes){
	var defer = $.Deferred();
	$.ajax({
		type:"post",
		url: "/manage/delRole",
		data:sysRoleMes,
		dataType:"json",
		success:function(data){
			defer.resolve(data);
		},
		err:function(data){
			defer.resolve("undefined");
		}
	});
	return defer.promise();
}

/**
 * 跳转到权限设置
 */
function openMenuSet(roleId){
	var sysRoleMes = {id:roleId};

	$.ajax({
		type:"post",
		url:"/manage/openMenuSet",
		dataType:"json",
		data:sysRoleMes,
		success:function(data){
			if(data.result == 'success'){
				$("#nowPage").empty();
				$('#nowPage').html(data.htmlText);
			}else{
				alert(data.info);
			}
		},
		err:function(data){
			alert(data.info);
		}
	});
}

//单击权限部分
function doClickPower(menuId){
	var roleId = $("#roleId").val(); //角色id
	var roleMenuId = $("#roleMenuId").val(); //角色菜单i

	if($("#"+menuId).css('background-color') == 'rgb(242, 93, 92)'){
		$("#"+menuId).css('background-color','rgba(0, 0, 0, 0)');
		var arr = new Array();
		if(roleMenuId != '' && typeof (roleMenuId) != 'undefined'){
			arr = roleMenuId.split(',');
			for(var i=0;i<arr.length;i++){
				if(arr[i] == menuId){
					arr.splice(i,1);
				}
			}
		}

		if(arr.length != 0){
			for(var i=0;i<arr.length;i++){
				if(i==0){
					roleMenuId = arr[i];
				}else{
					roleMenuId = roleMenuId + ',' + arr[i];
				}
			}
		}else{
			roleMenuId = '';
		}
	}else if($("#"+menuId).css('background-color') == 'rgba(0, 0, 0, 0)'){
		$("#"+menuId).css('background-color','rgb(242, 93, 92)');
		if(roleMenuId == '' || typeof (roleMenuId) == 'undefined'){
			roleMenuId = menuId;
		}else{
			roleMenuId = roleMenuId + ',' + menuId; //角色菜单id
		}
	}
	$("#roleMenuId").val(roleMenuId);
}

//保存角色权限
function saveRolePower(){
	var roleId = $("#roleId").val(); //角色id
	var roleMenuId = $("#roleMenuId").val(); //角色权限id
	var sysRoleMenuMes = {roleId:roleId,menuId:roleMenuId};

	$.ajax({
		type:"post",
		url:"/manage/saveRolePower",
		dataType:"json",
		data:sysRoleMenuMes,
		success:function(data){
			if(data.result == 'error'){ //失败
				alert(data.info);
			}else{
				alert(data.info);
			}
		},
		err:function(data){
			alert(data.info);
		}
	});
}

/*************** 分页部分Start ***************/
//根据条件查询用户组
function getSysRoleBySome(sysRole){
	$.ajax({
		type:"post",
		url: "/manage/queryRoleByPage",
		data:sysRole,
		dataType:"json",
		success:function(data){
			if(data.result=='success'){
				$('#nowPage').html(data.htmlText);
			}else{
				$('#nowPage').after(data.info);
			}
		},
		err:function(data){
			console.log(data);
		}
	});
}

//首页
function SysRoleStartPage(){
	var pageIndex = '1'; //当前数
	var pageSize = '10'; //每页显示数量
	var currentPage = '1'; //当前页
	var roleName = $("#roleName").val(); //角色名称
	var sysRole = {roleName:roleName,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_admin.jetx'};
	getSysRoleBySome(sysRole); //根据条件查询产品
}

//尾页
function SysRoleEndPage(){
	var totalCount = $("#totalCount").val(); //总数量
	var pageIndex = '1'; //当前数
	var currentPage = '1'; //当前页
	if(parseInt(totalCount) % 10 == 0){
		currentPage = parseInt(totalCount) / 10; //当前页
		pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
	}else{
		currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
		pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
	}
	var pageSize = '10'; //每页显示数量
	var roleName = $("#roleName").val(); //角色名称
	var sysRole = {roleName:roleName,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_admin.jetx'};
	getSysRoleBySome(sysRole); //根据条件查询产品
}

//上一页
function SysRoleUpPage(){
	var currentPage = $("#currentPage").val(); //当前页
	var pageIndex = "1"; //当前数
	if(parseInt(currentPage) == 1){
		pageIndex = "1";
		currentPage = "1";
	}else{
		currentPage = parseInt(currentPage) - 1; //当前页
		pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
	}

	var pageSize = "10"; //每页显示数量
	var roleName = $("#roleName").val(); //角色名称
	var sysRole = {roleName:roleName,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_admin.jetx'};
	getSysRoleBySome(sysRole); //根据条件查询产品
}

//下一页
function SysRoleDownPage(){
	var totalPage = "1"; //总页数
	var currentPage = $("#currentPage").val(); //当前页
	var pageIndex = "1"; //当前数
	var totalCount = $("#totalCount").val(); //总数量
	var pageSize = "10";
	var roleName = $("#roleName").val(); //角色名称

	if(parseInt(totalCount) % 10 == 0){
		totalPage = parseInt(totalCount) / 10; //总页数
	}else{
		totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
	}

	if(parseInt(currentPage) < parseInt(totalPage)){
		currentPage = parseInt(currentPage) + 1; //当前页
	}else{
		currentPage = totalPage; //当前页
	}

	pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
	var sysRole = {roleName:roleName,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_admin.jetx'};
	getSysRoleBySome(sysRole); //根据条件查询产品
}

//跳转到指定页数
function doSysRoleGoPage(page){
	var roleName = $("#roleName").val(); //角色名称
	var currentPage = page; //当前页
	var pageSize = "10"; //每页显示数量
	var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
	var sysRole = {roleName:roleName,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'sys/system_admin.jetx'};
	getSysRoleBySome(sysRole); //根据条件查询产品
}
/*************** 分页部分End ***************/
