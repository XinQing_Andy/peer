/**
 * Created by lenovo on 2016/2/16.
 */

/**
 *打开模板
 */

function openForecast(menuUrl, controlUrl) {
    var menu = {jetxName: menuUrl};
    var userMenu = $.param(menu);
    $.when(openFore(userMenu, controlUrl)).done(function (data) {
        if (data.result == "error") {
        } else {
            $("#nowPage").empty();
            $('#nowPage').html(data.htmlText);
        }
    });
}
function openFore(userMenu, controlUrl) {
    console.log(userMenu, controlUrl);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: controlUrl,
        data: userMenu,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 * 查看按钮切换
 */
function getHomePage(menuUrl, controlUrl) {
    $("#tabCon").empty();
    var menu = {jetxName: menuUrl};
    var userMenu = $.param(menu);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: controlUrl,
        data: userMenu,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
            $('#tabCon').append('<div>' + data.htmlText + '</div>');
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 保存到货预报新增
 */
function saveForecast() {
    var userInfo = $("#addForecast").serialize();
    console.log(userInfo);
    $.when(addForecast(userInfo)).done(function (data) {
        if (data.result == "error") {
            alert("添加失败" + data.info);
        } else {
            alert("添加成功");
        }
    });
}

function addForecast(userInfo) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/manager/addForecast",
        data: userInfo,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 * 查看待入库
 * @param menuUrl
 * @param controlUrl
 * @param id
 * @constructor
 */
function StorageSee(menuUrl, controlUrl, id) {
    var menu = {jetxName: menuUrl, id: id};

    console.log(menuUrl, controlUrl);
    var userMenu = $.param(menu);
    $.when(Storage(userMenu, controlUrl)).done(function (data) {
        if (data.result == "error") {
        } else {
            $("#tabCon").empty();
            $('#tabCon').html(data.htmlText);
        }
    });
}
function Storage(userMenu, controlUrl) {
    console.log(userMenu, controlUrl);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: controlUrl,
        data: userMenu,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 保存新增用户收货地址
 */
function addUserAddress(){
    var userInfo = $("#addUserAddress").serialize();
    console.log(userInfo);
    $.when(addAddress(userInfo)).done(function(data){
        if(data.result == "error"){
            alert("添加失败"+data.info);
        }else{
            alert("添加成功");
            openForecast('includes/order_claimOrderUser.jetx','getUserWare');
        }
    });
}

function addAddress(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/addUserWare",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 * 打开用户收货地址修改页面
 * @param menuUrl
 * @param controlUrl
 * @param id
 */
function openAddress(menuUrl, controlUrl,id){
    var menu = {jetxName: menuUrl,id:id};
    console.log(menuUrl, controlUrl,id);
    var userMenu = $.param(menu);
    $.when(open(userMenu, controlUrl)).done(function (data) {
        if (data.result == "error") {
        } else {
            $("#tabCon").empty();
            $('#tabCon').html(data.htmlText);
        }
    });
}
function open(userMenu, controlUrl) {
    console.log(userMenu, controlUrl);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: controlUrl,
        data: userMenu,
        dataType: "json",
        success: function (data) {
            defer.resolve(data);
        },
        err: function (data) {
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 * 删除用户收货地址
 * @param id
 * @returns {*}
 */
function deleteAddress(id){
    var flag = confirm("是否删除？");
    if (flag) {
        var defer = $.Deferred();
        var parm = {id: id};
        $.ajax({
            type: "post",
            url: "/manager/delectUserWare",
            data: $.param(parm),
            dataType: "json",
            success: function (data) {
                defer.resolve(data);
                alert("删除成功");
                openForecast('includes/order_claimOrderUser.jetx','getUserWare');
            },
            err: function (data) {
                defer.resolve("undefined");
                alert("删除失败");
                openForecast('includes/order_claimOrderUser.jetx','getUserWare');
            }
        });
        return defer.promise();
    }
}
/**
 * 修改用户收货地址
 */
function updateUserAddress(){
    var userInfo = $("#updateUserAddress").serialize();
    $.when(update(userInfo)).done(function(data){
        if(data.result == "error"){
            alert("修改失败"+data.info);
        }else{
            alert("修改成功");
            openForecast('includes/order_claimOrderUser.jetx','getUserWare');
        }
    });
}
function update(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/updateUserWare",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 * 保存用户修改后的个人信息
 */
function updatePersonal(){
    var userInfo = $("#updatePersonal").serialize();
    $.when(Personal(userInfo)).done(function(data){
        if(data.result == "error"){
            alert("修改失败"+data.info);
        }else{
            alert("修改成功");
            openForecast('includes/account_personalInfo.jetx','getOneUserInfo');
        }
    });
}
function Personal(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/updatePersonal",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 * 修改用户密码
 */
function passwordUpdate(){
    var userInfo = $("#passwordUpdate").serialize();
    $.when(updatePassword(userInfo)).done(function(data){
        if(data.result == "error"){
            alert("修改失败"+data.info);
        }else{
            alert("修改成功");
            openForecast('includes/account_personalInfo.jetx','getOneUserInfo');
        }
    });
}
function updatePassword(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/changeOnePassword",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}
/**
 * 付款
 */
function payment(orderInfoId){
    var flag = confirm("是否付款？");
    if (flag) {
        var defer = $.Deferred();
        var parm = {id: orderInfoId};
        $.ajax({
            type: "post",
            url: "/manager/getPayment",
            data: $.param(parm),
            dataType: "json",
            success: function (data) {
                defer.resolve(data);
                alert("付款成功");
                openForecast('includes/order_generatedUser.jetx','getOrder');
            },
            err: function (data) {
                defer.resolve("undefined");
                alert("付款失败");
            }
        });
        return defer.promise();
    }
}