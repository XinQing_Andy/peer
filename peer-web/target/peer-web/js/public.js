/**
 * Created by WangMeng on 2015/12/10.
 */
/**
 * 当前时间
 * @param divId
 */
function nowTime($domId){
    var date = new Date(); //日期对象
    var now = "";
    var weekday=new Array(7);
    weekday[0]="星期日";
    weekday[1]="星期一";
    weekday[2]="星期二";
    weekday[3]="星期三";
    weekday[4]="星期四";
    weekday[5]="星期五";
    weekday[6]="星期六";
    var nowDay=date.getDay();
    now = date.getFullYear()+"年";
    now = now + (date.getMonth()+1)+"月";
    now = now + date.getDate()+"日";
    now = now + '  ' + weekday[nowDay];
//now = now + date.getHours()+"时";
//now = now + date.getMinutes()+"分";
//now = now + date.getSeconds()+"秒";
    $domId.html(now);
}
