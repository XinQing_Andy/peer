/**
 * 修改数据
 */
function onUpdateMenu(id,name){
    var e1 = document.getElementById("update_module");
    e1.style.visibility =  (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
    var menuId = document.getElementById("moduleId");
    menuId.setAttribute("value",id);
    var menuName = document.getElementById("updateMenuName");
    menuName.setAttribute("value",name);
}

/**
 *修改
 */
function onUpdateMenuForm(){
    var userInfo = $("#updateModuleForm").serialize();
    $.when(updateMenu(userInfo)).done(function(data){
        if(data.result == "error"){
            alert("修改失败"+data.info);
        }else{
            alert("修改成功");
            var e1 = document.getElementById("update_module");
            e1.style.visibility =  (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
        }
    });
}

function updateMenu(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/updateModuleMenu",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 用户组添加数据
 */
function addModule(){
    var e1 = document.getElementById("module");
    e1.style.visibility =  (e1.style.visibility == "visible"  ) ? "hidden" : "visible";
}

/**
 * 用户组添加数据
 */
function onAddMenu(){
    var userInfo = $("#addModuleForm").serialize();
    $.when(addMenu(userInfo)).done(function(data){
        if(data.result == "error"){
            alert("添加失败"+data.info);
        }else{
            alert("添加成功");
            addModule();
        }
    });
}

function addMenu(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/addModuleMenu",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 模块下架
 */
function onDownMenu(id){
    var flag = confirm("是否将该产品下架？");
    if (flag) {
        var defer = $.Deferred();
        var parm = {id: id};
        $.ajax({
            type: "post",
            url: "/manager/onDownMenu",
            data: $.param(parm),
            dataType: "json",
            success: function (data) {
                defer.resolve(data);
            },
            err: function (data) {
                defer.resolve("undefined");
            }
        });
        return defer.promise();
    }
}
