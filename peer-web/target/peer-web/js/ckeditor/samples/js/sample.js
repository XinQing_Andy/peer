﻿/**
 * Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */


var initSample = ( function() {
	/* exported initSample */

	if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 )
		CKEDITOR.tools.enableHtml5Elements( document );

// The trick to keep the editor in the sample quite small
// unless user specified own height.
//	CKEDITOR.config.height = 600;
//	CKEDITOR.config.width =900;
// // Defines a toolbar with only one strip containing the "Source" button, a
// // separator, and the "Bold" and "Italic" buttons.
// CKEDITOR.config.toolbar = [
// 	[ 'Source', '-', 'Bold', 'Italic' ]
// ];
//
// // Similar to the example above, defines a "Basic" toolbar with only one strip containing three buttons.
// // Note that this setting is composed by "toolbar_" added to the toolbar name, which in this case is called "Basic".
// // This second part of the setting name can be anything. You must use this name in the CKEDITOR.config.toolbar setting
// // in order to instruct the editor which `toolbar_(name)` setting should be used.
// CKEDITOR.config.toolbar_Basic = [
// 	[ 'Source', '-', 'Bold', 'Italic' ]
// ];
// // Load toolbar_Name where Name = Basic.
// CKEDITOR.config.toolbar = 'Basic';


// Default setting.
//	CKEDITOR.config.toolbarGroups = [
//		{ name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
//		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
//		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
//		// { name: 'forms' },
//		// '/',
//		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
//		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
//		{ name: 'links' },
//		{ name: 'insert' },
//		// '/',
//		{ name: 'styles' },
//		{ name: 'colors' },
//		{ name: 'tools' },
//		{ name: 'others' }
//		// ,
//		// {name: 'links', items: ['Link', 'Unlink']}
//	];

	CKEDITOR.config.font_names = 'Arial;Times NewRoman;Verdana;宋体;仿宋体;微软雅黑;';


	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function(uploadurl,uploadurl1) {
		// var uploadurl="";
		var editorElement = CKEDITOR.document.getById( 'editor' );

		// :(((
		if ( isBBCodeBuiltIn ) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=http://ckeditor.com]CKEditor[/url].'
			);
		}

		// Depending on the wysiwygare plugin availability initialize classic or inline editor.
		if ( wysiwygareaAvailable ) {
			CKEDITOR.replace( 'editor',{
				language: 'zh-cn',
				height: 450,
				width:800,
				filebrowserBrowseUrl: uploadurl,
				filebrowserUploadUrl: uploadurl1,
				toolbarGroups:[
					{ name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
					{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
					// { name: 'forms' },
					// '/',
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'links' },
					{ name: 'insert' },
					// '/',
					{ name: 'styles' },
					{ name: 'colors' },
					{ name: 'tools' },
					{ name: 'others' }
					// ,
					// {name: 'links', items: ['Link', 'Unlink']}
				]
			} );
		} else {
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'editor' );
			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}

		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();