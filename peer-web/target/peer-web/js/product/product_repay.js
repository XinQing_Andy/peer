/**
 * Created by Shan's on 2016/8/30.
 */


/*************** 逾期还款 分页部分Start ***************/
//首页
function ProductOverRepayStartPage() {
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repay.jetx'
    };
    queryRepayInfoBySomes(product); //根据条件查询产品
}
//尾页
function ProductOverRepayEndPage() {
    var totalCount = $("#productTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if (parseInt(totalCount) % 10 == 0) {
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    } else {
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repay.jetx'
    };
    queryRepayInfoBySomes(product); //根据条件查询产品
}
//上一页
function ProductOverRepayUp() {
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if (parseInt(currentPage) == 1) {
        pageIndex = "1";
        currentPage = "1";
    } else {
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repay.jetx'
    };
    queryRepayInfoBySomes(product); //根据条件查询产品
}
//下一页
function ProductOverRepayDown() {
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#productTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if (parseInt(totalCount) % 10 == 0) {
        totalPage = parseInt(totalCount) / 10; //总页数
    } else {
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if (parseInt(currentPage) < parseInt(totalPage)) {
        currentPage = parseInt(currentPage) + 1; //当前页
    } else {
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repay.jetx'
    };
    queryRepayInfoBySomes(product); //根据条件查询产品
}
//跳转到指定页数
function doProductOverRepayGoPage(pageCount) {
    var productOverRepayNum = $('#productOverRepayNum').val(); //跳转到第几页
    if (productOverRepayNum == '') {
        alert('请输入要跳转的页数！');
        return;
    }
    if (!validate(productOverRepayNum)) {
        alert('请输入合法的数字！');
        return;
    }
    if (parseInt(productOverRepayNum) > parseInt(pageCount)) {
        alert('跳转的页数不存在！');
        return;
    }
    var currentPage = productOverRepayNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var title = $("#title").val(); //题目
    var product = {
        title: title,
        pageIndex: pageIndex,
        pageSize: pageSize,
        currentPage: currentPage,
        menuUrl: 'product/product_repay.jetx'
    };
    queryRepayInfoBySomes(product); //根据条件查询产品
}


//按条件查询逾期账单列表
function queryRepayInfoBySomes() {
    if (!CashCheckTime()) {
        return;
    }
    var loanman = $("#loanman").val(); //借款人
    var idcardNumber = $("#idcardNumber").val(); //身份证号
    var productTitle = $("#productTitle").val(); //题目
    var startTime = $("#startTime").val(); //开始时间
    var endTime = $("#endTime").val(); //结束时间
    var pageIndex = $("#pageIndex").val(); //起始数
    var pageSize = $("#pageSize").val(); //每页显示数量

    var temp = {
        loanman: loanman,
        idcardNumber: idcardNumber,
        productTitle: productTitle,
        startTime: startTime,
        endTime: endTime,
        pageSize: pageSize,
        pageIndex: pageIndex,
        menuUrl: 'product/product_repay.jetx'
    };

    $.ajax({
        type: "post",
        url: "/productRepay/queryProductInfoByPage",
        data: temp,
        dataType: "json",
        success: function (data) {
            $('#str').html(data.str);
            $('#totalCount').val(data.totalCount);
            $('#currentPage').val(data.currentPage);
            $('#pageStr').html(data.pageStr);
        },
        err: function (data) {
            alert('查询失败！');
        }
    });
}
//跳转到指定页数
function doGoPageCash(page) {
    var currentPage = page; //当前页
    $("#pageSize").val("10");//每页显示数量
    $("#pageIndex").val((parseInt(currentPage) - 1) * 10 + 1);//当前数
    queryRepayInfo();
}
/*************** 逾期还款 分页部分End ***************/


//导出逾期还款
function exportOverDaysRepay(){
    var where = "WHERE 1 = 1"; //查询条件
    var startTime = $("#startTime").val(); //开始时间
    var endTime = $("#endTime").val(); //截止时间
    var loanman = $("#loanman").val(); //借款人
    var idcardNumber = $("#idcardNumber").val(); //身份证号
    var productTitle = $("#productTitle").val(); //项目名称

    if(startTime != '' && typeof (startTime) != 'undefined'){ //开始时间
        where = where + " AND DATE(YREPAYMENTTIME) >= DATE('"+startTime+"')";
    }
    if(endTime != '' && typeof (endTime) != 'undefined'){ //截止时间
        where = where + " AND DATE(YREPAYMENTTIME) <= DATE('"+endTime+"')";
    }
    if(loanman != '' && typeof (loanman) != 'undefined'){ //姓名
        where = where + " AND REALNAME LIKE '%"+loanman+"%'";
    }
    if(idcardNumber != '' && typeof (idcardNumber) != 'undefined'){ //身份证号
        where = where + " AND IDCARD = '"+idcardNumber+"'";
    }
    $("#ExportTitle").val('名称;TITLE,编码;CODE,借款人;REALNAME,应还款日期;YREPAYMENTTIME,逾期时长;OVERDAYS,应还款金额;YREPAYMENTPRICE,借款金额;LOANPRICE');
    $("#tableName").val('view_product_repay_export');
    $("#where").val(where);
    $("#exportOverDaysRepay").submit();
}

function CashCheckTime() {
    var val = $("#startTime").val();
    var val2 = $("#endTime").val();
    var time1 = new Date(val).getTime();
    var time2 = new Date(val2).getTime();
    if (time1 > time2 || time1 == time2) {
        alert("开始时间必须小于结束时间");
        return false;
    }
    return true;
}





/**
 *
 */
function selectBqC(id) {

    for (var i = 1; i < 4; i++) {
        $("#l" + i).attr("class", "");
    }
    $("#" + id).attr("class", "active");

    var temp = $("#" + id).html();
    var last = temp.substr(temp.length - 1, temp.length);

    if (last == '↓') {
        last = '↑';
        var head = temp.substr(0, temp.length - 1);
        $("#" + id).html(head + last);
        $("#orderType").val("ASC");
    } else if (last == '↑') {
        last = '↓';
        var head = temp.substr(0, temp.length - 1);
        $("#" + id).html(head + last);
        $("#orderType").val("DESC");
    } else if (last != '↓' && last != '↑') {
        last = '↑';
        $("#" + id).html(temp + last);
        $("#orderType").val("ASC");
    }

    for (var j = 1; j < 4; j++) {
        if (("l" + j) != id) {
            var temp = $("#l" + j).html();
            var last = temp.substr(temp.length - 1, temp.length);

            if (last == '↓' || last == '↑') {
                temp = temp.substr(0, temp.length - 1);
                $("#l" + j).html(temp);
            }
        }
    }

    queryRepayInfoBySomes();
}

