/**
 * Created by Cuibin on 2016/6/29.
 */


/**
 * 跳转到 成功的债权转让标 详情
 */
function openProductAttornDetails(id){
    var temp = {
        id:id
    };
    $.ajax({
        type:"post",
        url: "/productAttorn/openProductAttornDetails",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').html(data.info);
            }
        },
        err:function(data){

        }
    });
}

/**
 * 跳转到转让标详情
 */
function openProductTransferDetails(){
    $("#two").attr("style","display: block");
    $("#one").attr("style","display: none");
    $("#twoli").attr("class","active");
    $("#oneli").attr("class","");
}

/**
 * 跳转到竞拍记录
 */
function openHistory(){
    $("#two").attr("style","display: none");
    $("#one").attr("style","display: block");
    $("#oneli").attr("class","active");
    $("#twoli").attr("class","");
}