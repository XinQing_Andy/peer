/**
 * Created by Cuibin on 2016/4/19.
 */
/**
 * 删除常见问题
 */
function deleteQuestion(id){
    if(!confirm("确认删除吗")){
        return;
    }
    var temp = {id:id};
    $.ajax({
        type:"post",
        url: "/question/deleteQuestion",
        data:temp,
        dataType:"json",
        success:function(data){
            openForecast("platform/problem.jetx","/question/queryQuestionByPage");
        },
        err:function(data){

        }
    });
}
/**
 * 打开问题添加页面
 */
function openAddQuestion(){
    var temp = {menuUrl:"platform/problem_add.jetx"};
    $.ajax({
        type:"post",
        url: "/question/openAddQuestion",
        data:temp,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#tab').html(data.htmlText);
            }else{
                $('#tab').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
/**
 * 保存问题
 */
function addQuestion(){
    var question = $("#question").val();
    var autor = $("#autor").val();
    var sort = $("#sort").val();
    var answer = $("#answer").val();
    var reg = /^\s+$/gi;
    if(question == "" || reg.test(question)){
        alert("问题不能为空");
        return;
    }
    if(autor == "" || reg.test(autor)){
        alert("作者不能为空");
        return;
    }
    if(sort == "" || reg.test(sort)){
        alert("排序不能为空");
        return;
    }
    if(answer == "" || reg.test(answer)){
        alert("答案不能为空");
        return;
    }
    var temp = {answer:answer,sort:sort,question:question,autor:autor};
    $.ajax({
        type:"post",
        url: "/question/addQuestion",
        data:temp,
        dataType:"json",
        success:function(data){
            alert(data.info);
            openForecast("platform/problem.jetx","/question/queryQuestionByPage");
        },
        err:function(data){
            alert(data.info);
        }
    });
}
/**
 * 查询单个问题
 */
function queryOnlyQuestion(id){
    var temp = {id:id,menuUrl:"platform/problem_update.jetx"};
    $.ajax({
        type:"post",
        url: "/question/queryOnlyQuestion",
        data:temp,
        dataType:"json",
        success:function(data){
            $('#nowPage').html(data.htmlText);
        },
        err:function(data){
            alert(data.info);
        }
    });
}
/**
 * 更新问题
 */
function updateQuestion(){
    var question = $("#question").val();
    var autor = $("#autor").val();
    var sort = $("#sort").val();
    var answer = $("#answer").val();
    var id = $("#id").val();
    var reg = /^\s+$/gi;
    if(question == "" || reg.test(question)){
        alert("问题不能为空");
        return;
    }
    if(autor == "" || reg.test(autor)){
        alert("作者不能为空");
        return;
    }
    if(sort == "" || reg.test(sort)){
        alert("排序不能为空");
        return;
    }
    if(answer == "" || reg.test(answer)){
        alert("答案不能为空");
        return;
    }
    var temp = {id:id,answer:answer,sort:sort,question:question,autor:autor};
    $.ajax({
        type:"post",
        url: "/question/updateQuestion",
        data:temp,
        dataType:"json",
        success:function(data){
            alert(data.info);
            openForecast("platform/problem.jetx","/question/queryQuestionByPage");
        },
        err:function(data){
            alert(data.info);
        }
    });
}
/**
 * 验证排序
 */
function CheckSort(value){
    var z = /^[0-9]*$/;
    if(!z.test(value)){
        value = value.length - 1;
        $("#sort").val(value);
    }
}
/**
 * 是否启用
 */
function updateStart(id,index){
    if($('#check'+index).is(':checked')){
        var flag = '1';
    }else{
        var flag = '0';
    }
    var temp = {id:id,flag:flag};
    var temp2 = $.param(temp);
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: "/question/updateStart",
        data: temp2,
        dataType: "json",
        success: function (data) {
            if(data.result == 'success'){
                alert(data.info);
            }
        },
        err: function (data) {

        }
    });
}
/*************** 分页部分Start ***************/
//根据条件查询广告
function getquBySome(advert){
    $.ajax({
        type:"post",
        url: "/question/queryQuestionByPage",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function quStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getquBySome(advert); //根据条件查询广告
}

//尾页
function quEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getquBySome(advert); //根据条件查询广告
}

//上一页
function quUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getquBySome(advert); //根据条件查询广告
}

//下一页
function quDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getquBySome(advert); //根据条件查询广告
}

//按条件查询用户列表
function ququeryInfo(){
    var sysKeyWord = $("#title").val(); //查询关键字
    var sysUserMes = {title:sysKeyWord};
    $.ajax({
        type:"post",
        url: "/question/queryQuestionByPage",
        data:sysUserMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}
//跳转到指定页数
function doGoquPage(page){
    var title = $("#title").val(); //题目
    var currentPage = page; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage};
    getquBySome(advert); //根据条件查询广告
}
/*************** 分页部分End ***************/