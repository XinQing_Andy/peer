/**
 * Created by zhangerxin on 2015/12/28.
 */

//按条件查询产品列表
function queryAdvertInfo(){

    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id
    var advert = {title:title,typeId:typeId,menuUrl:'platform/advert.jetx'};

    $.ajax({
        type:"post",
        url: "/advert/queryAdvertByPage",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').html(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//查询广告部分
function getAdvertList(){
    var advert = {menuUrl:'platform/advert.jetx'};

    $.ajax({
        type:"post",
        url: "/advert/queryAdvertByPage",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').html(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//打开新增页面
function openAdvertAdd(){
    var advertMes = {menuUrl:'platform/advert_add.jetx'};

    $.ajax({
        type:"post",
        url: "/advert/openAdvertContent",
        data:advertMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//保存广告
function doSaveAdvert(){
    var typeid = $("#advertType").val(); //广告类别id
    var title = $("#title").val(); //标题
    var autor = $("#autor").val(); //作者
    var content = CKEDITOR.instances.editor.getData(); //内容
    var sort = $("#sort").val(); //排序
    var advert = 'typeid='+typeid+'&title='+title+'&autor='+autor+'&&sort='+sort+'&content='+content;

    $.ajaxFileUpload({
        url:'/advert/UploadAdvert?'+advert,
        type:'POST',
        secureuri:false,
        data:advert,
        fileElementId:'advertFile',
        dataType:'text',
        success:function(data, status){
            if(data.indexOf('success') > -1){
                alert('保存成功！');
                queryAdvertInfo();
            }else{
                alert('保存失败！');
            }
        },
        error:function(data, status, e){
            alert('保存失败！');
        }
    });
}


/**
 * 打开修改界面
 */
function openAdvertEdit(id){
    var advertMes = {id:id,menuUrl:'platform/advert_eidt.jetx'};

    $.ajax({
        type:"post",
        url: "/advert/queryAdvertById",
        data:advertMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


/**
 * 修改广告
 */
function doUpdateAdvert(){
    var id = $("#id").val(); //主键
    var fileId = $("#fileId").val(); //附件id
    var imgUrl = $("#imgUrlJd").val(); //附件地址(绝对地址)
    var typeId = $("#advertType").val(); //广告类型id
    var title = $("#title").val(); //广告
    var autor = $("#autor").val(); //作者
    var sort = $("#sort").val(); //排序
    var content = CKEDITOR.instances.editor.getData(); //内容

    var advert = 'id='+id+'&fileId='+fileId+'&imgUrl='+imgUrl+'&typeId='+typeId+'&title='+title+'&autor='+autor+'&content='+content+'&sort='+sort;

    $.ajaxFileUpload({
        url:'/advert/UploadAdvertEdit?'+advert,
        secureuri:false,
        fileElementId:'advertFile',
        dataType:'text',
        success:function(data, status){
            if(data.indexOf('success') > -1){
                alert('保存成功！');
                queryAdvertInfo();
            }else{
                alert('保存失败！');
            }
        },
        error:function(data, status, e){
            alert('保存失败！');
        }
    });
}

/**
 * 设置广告状态
 * @param id
 * @param flag
 */
function setAdvert(id,flag){
    var advert = {id:id,flag:flag};

    $.ajax({
        type:"post",
        url: "/advert/setAdvertFlag",
        data:advert,
        dataType:"json",
        success:function(data){
            alert('设置成功！');
            queryAdvertInfo();
        },
        err:function(data){
            console.log(data);
        }
    });
}


/*************** 分页部分Start ***************/
//根据条件查询广告
function getAdvertBySome(advert){
    $.ajax({
        type:"post",
        url: "/advert/queryAdvertByPage",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页
function AdvertStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/advert.jetx'};
    getAdvertBySome(advert); //根据条件查询广告
}

//尾页
function AdvertEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/advert.jetx'};
    getAdvertBySome(advert); //根据条件查询广告
}

//上一页
function AdvertUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/advert.jetx'};
    getAdvertBySome(advert); //根据条件查询广告
}

//下一页
function AdvertDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/advert.jetx'};
    getAdvertBySome(advert); //根据条件查询广告
}

//跳转到指定页数
function doAdvertGoPage(){
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id
    var advertNum = $('#advertNum').val(); //跳转到第几页
    if(advertNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }

    if(!validate(advertNum)){
        alert('请输入合法的数字！');
        return;
    }
    var currentPage = advertNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/advert.jetx'};
    getAdvertBySome(advert); //根据条件查询广告
}
/*************** 分页部分End ***************/