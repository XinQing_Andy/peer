/**
 * Created by zhangerxin on 2015/12/28.
 */

//按条件查询保障列表
function querySecurityInfo(){
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型
    var advert = {title:title,typeId:typeId,menuUrl:'platform/security.jetx'};

    $.ajax({
        type:"post",
        url: "/security/queryAdvertByPage",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').html(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


//查询保障部分
function getAdvertList(){
    var advert = {menuUrl:'platform/security.jetx'};

    $.ajax({
        type:"post",
        url: "/security/queryAdvertByPage",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').html(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


//打开新增页面
function openSecurityAdd(){
    var advertMes = {menuUrl:'platform/security_add.jetx'};

    $.ajax({
        type:"post",
        url: "/security/openAdvertContent",
        data:advertMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//新增保障
function doSecurityAdvert(){
    var typeid = $("#advertType").val(); //广告类别id
    var title = $("#title").val(); //标题
    var autor = $("#autor").val(); //作者
    var content = CKEDITOR.instances.editor.getData(); //内容
    var sort = $("#sort").val(); //排序

    var advert = 'typeid='+typeid+'&title='+title+'&autor='+autor+'&content='+content+'&sort='+sort;

    $.ajaxFileUpload({
        url:'/security/UploadAdvert?'+advert,
        secureuri:false,
        fileElementId:'advertFile',
        dataType:'text',
        success:function(data, status){
            if(data.indexOf('success') > -1){
                alert('保存成功！');
                querySecurityInfo();
            }else{
                alert('保存失败！');
            }
        },
        error:function(data, status, e){
            alert('保存失败！');
        }
    });
}

/**
 * 打开修改界面
 */
function openSecurityEdit(id){
    var advertMes = {id:id,menuUrl:'platform/security_eidt.jetx'};

    $.ajax({
        type:"post",
        url: "/security/queryAdvertById",
        data:advertMes,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}


/**
 * 修改广告
 */
function doUpdateSecurity(){
    var id = $("#id").val(); //主键
    var fileId = $("#fileId").val(); //附件id
    var imgUrl = $("#imgUrl").val(); //附件地址(绝对地址)
    var typeId = $("#advertType").val(); //广告类型id
    var title = $("#title").val(); //广告
    var autor = $("#autor").val(); //作者
    var sort = $("#sort").val(); //排序
    var content = CKEDITOR.instances.editor.getData(); //内容

    var advert = 'id='+id+'&fileId='+fileId+'&imgUrl='+imgUrl+'&typeId='+typeId+'&title='+title+'&autor='+autor+'&content='+content+'&sort='+sort;

    $.ajaxFileUpload({
        url:'/security/UploadAdvertEdit?'+advert,
        secureuri:false,
        fileElementId:'advertFile',
        dataType:'text',
        success:function(data, status){
            if(data.indexOf('success') > -1){
                alert('保存成功！');
                querySecurityInfo();
            }else{
                alert('保存失败！');
            }
        },
        error:function(data, status, e){
            alert('保存失败！');
        }
    });
}

/**
 * 设置广告状态
 * @param id
 * @param flag
 */
function setSecurity(id,flag){
    var advert = {id:id,flag:flag};

    $.ajax({
        type:"post",
        url: "/security/setAdvertFlag",
        data:advert,
        dataType:"json",
        success:function(data){
            alert('设置成功！');
            querySecurityInfo();
        },
        err:function(data){
            console.log(data);
        }
    });
}



/*************** 分页部分Start ***************/
//根据条件查询广告
function getSecurityBySome(advert){
    $.ajax({
        type:"post",
        url: "/security/queryAdvertByPage",
        data:advert,
        dataType:"json",
        success:function(data){
            if(data.result=='success'){
                $('#nowPage').html(data.htmlText);
            }else{
                $('#nowPage').after(data.info);
            }
        },
        err:function(data){
            console.log(data);
        }
    });
}

//首页(保障)
function SecurityStartPage(){
    var pageIndex = '1'; //当前数
    var pageSize = '10'; //每页显示数量
    var currentPage = '1'; //当前页
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/security.jetx'};
    getSecurityBySome(advert); //根据条件查询广告
}

//尾页
function SecurityEndPage(){
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageIndex = '1'; //当前数
    var currentPage = '1'; //当前页
    if(parseInt(totalCount) % 10 == 0){
        currentPage = parseInt(totalCount) / 10; //当前页
        pageIndex = (parseInt(totalCount) / 10 - 1) * 10 + 1; //当前数
    }else{
        currentPage = parseInt(parseInt(totalCount) / 10) + 1; //当前页
        pageIndex = parseInt(parseInt(totalCount) / 10) * 10 + 1; //当前数
    }
    var pageSize = '10'; //每页显示数量
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/security.jetx'};
    getSecurityBySome(advert); //根据条件查询广告
}

//上一页
function SecurityUp(){
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    if(parseInt(currentPage) == 1){
        pageIndex = "1";
        currentPage = "1";
    }else{
        currentPage = parseInt(currentPage) - 1; //当前页
        pageIndex = (parseInt(currentPage) - 1) * 10 + 1;
    }

    var pageSize = "10"; //每页显示数量
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/security.jetx'};
    getSecurityBySome(advert); //根据条件查询广告
}

//下一页
function SecurityDown(){
    var totalPage = "1"; //总页数
    var currentPage = $("#currentPage").val(); //当前页
    var pageIndex = "1"; //当前数
    var totalCount = $("#advertTotalCount").val(); //总数量
    var pageSize = "10";
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id

    if(parseInt(totalCount) % 10 == 0){
        totalPage = parseInt(totalCount) / 10; //总页数
    }else{
        totalPage = parseInt(parseInt(totalCount) / 10) + 1; //总页数
    }

    if(parseInt(currentPage) < parseInt(totalPage)){
        currentPage = parseInt(currentPage) + 1; //当前页
    }else{
        currentPage = totalPage; //当前页
    }

    pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/security.jetx'};
    getSecurityBySome(advert); //根据条件查询广告
}

//跳转到指定页数
function doSecurityGoPage(){
    var title = $("#title").val(); //题目
    var typeId = $("#advertType").val(); //类型id
    var securityNum = $('#securityNum').val(); //跳转到第几页
    if(securityNum == ''){
        alert('请输入要跳转的页数！');
        return;
    }
    if(!validate(securityNum)){
        alert('请输入合法的数字！');
        return;
    }
    var currentPage = securityNum; //当前页
    var pageSize = "10"; //每页显示数量
    var pageIndex = (parseInt(currentPage) - 1) * 10 + 1; //当前数
    var advert = {title:title,typeId:typeId,pageIndex:pageIndex,pageSize:pageSize,currentPage:currentPage,menuUrl:'platform/security.jetx'};
    getSecurityBySome(advert); //根据条件查询广告
}
/*************** 分页部分End ***************/