
/**
 * 打开代理授权协议书
 * @param
 * @returns
 */
function addDlsqxys(menuUrl, controlUrl, menuName,id){
    var menu = {jetxName: menuUrl,id:id};
    var userMenu = $.param(menu);
    addTab(menuUrl, controlUrl, menuName, userMenu);
}

/**
 * 添加
 * @param
 * @returns
 */
function saveWsdlsqxy(){
    var userInfo = $("#addWsdlsqxy").serialize();
    console.log("<><><>"+userInfo);
    $.when(addWssqxy(userInfo)).done(function(data){
        if(data.result == "error"){
            alert("添加失败"+data.info);
        }else{
            alert("添加成功");
        }
    });
}

function addWssqxy(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/addPrdAgreeMent",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 打开修改页面
 * @param userInfo
 */
function updateWsdlsqxyPage(menuUrl, controlUrl, menuName,id){
    var menu = {jetxName: menuUrl,id: id};
    var userMenu = $.param(menu);
    addTab(menuUrl, controlUrl, menuName, userMenu);
}

/**
 * 修改保存
 */
function updateWsdlsqxy(){
    var userInfo = $("#updateWsdlsq").serialize();
    $.when(updateWsdlInfo(userInfo)).done(function(data){
        if(data.result == "error"){
            alert("修改失败"+data.info);
        }else{
            alert("修改成功");
        }
    });
}

function updateWsdlInfo(userInfo){
    var defer = $.Deferred();
    $.ajax({
        type:"post",
        url: "/manager/upDatePrdAgreeMent",
        data:userInfo,
        dataType:"json",
        success:function(data){
            defer.resolve(data);
        },
        err:function(data){
            defer.resolve("undefined");
        }
    });
    return defer.promise();
}

/**
 * 提交
 * @param id
 * @returns {*}
 */
function subWsdlsqxy(id){
    var flag = confirm("是否提交？");
    if (flag) {
        var defer = $.Deferred();
        var parm = {id: id};
        $.ajax({
            type: "post",
            url: "/manager/upPrdAgreeMent",
            data: $.param(parm),
            dataType: "json",
            success: function (data) {
                defer.resolve(data);
                alert("提交成功");
            },
            err: function (data) {
                defer.resolve("undefined");
                alert("提交失败");
            }
        });
        return defer.promise();
    }
}

/**
 * 删除
 * @param id
 */
function delWsdlsqxy(id){
    var flag = confirm("是否删除？");
    if (flag) {
        var defer = $.Deferred();
        var parm = {id: id};
        $.ajax({
            type: "post",
            url: "/manager/delPrdAgreeMent",
            data: $.param(parm),
            dataType: "json",
            success: function (data) {
                defer.resolve(data);
                alert("删除成功");
            },
            err: function (data) {
                defer.resolve("undefined");
                alert("删除失败");
            }
        });
        return defer.promise();
    }
}
/**
 * 详情
 * @param
 * @returns
 */