package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-13.
 * 还款列表
 */
public class AppProductRepayMessageBean {

    /** 主键 **/
    private String id;
    /** 产品id **/
    private String productId;
    /** 本金 **/
    private String principal;
    /** 利息 **/
    private String interest;
    /** 还款金额 **/
    private String repayPrice;
    /** 应还款时间 **/
    private String yrepaymentTime;
    /** 实际还款金额 **/
    private String repaymentPrice;
    /** 实际还款日期 **/
    private String repaymentTime;
    /** 产品题目 **/
    private String title;
    /** 产品编码 **/
    private String code;
    /** 申请人 **/
    private String applymember;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPrincipal() {
        return principal;
    }
    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getInterest() {
        return interest;
    }
    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getRepayPrice() {
        return repayPrice;
    }
    public void setRepayPrice(String repayPrice) {
        this.repayPrice = repayPrice;
    }

    public String getYrepaymentTime() {
        return yrepaymentTime;
    }
    public void setYrepaymentTime(String yrepaymentTime) {
        this.yrepaymentTime = yrepaymentTime;
    }

    public String getRepaymentPrice() {
        return repaymentPrice;
    }
    public void setRepaymentPrice(String repaymentPrice) {
        this.repaymentPrice = repaymentPrice;
    }

    public String getRepaymentTime() {
        return repaymentTime;
    }
    public void setRepaymentTime(String repaymentTime) {
        this.repaymentTime = repaymentTime;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getApplymember() {
        return applymember;
    }
    public void setApplymember(String applymember) {
        this.applymember = applymember;
    }

    @Override
    public String toString() {
        return "AppProductRepayMessageBean{" +
                "id='" + id + '\'' +
                ", productId='" + productId + '\'' +
                ", principal='" + principal + '\'' +
                ", interest='" + interest + '\'' +
                ", repayPrice='" + repayPrice + '\'' +
                ", yrepaymentTime='" + yrepaymentTime + '\'' +
                ", repaymentPrice='" + repaymentPrice + '\'' +
                ", repaymentTime='" + repaymentTime + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", applymember='" + applymember + '\'' +
                '}';
    }
}
