package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-6-17.
 */
public class InvestProductMessageBean {

    /** 主键 **/
    private String id;
    /** 题目 **/
    private String title;
    /** 编码 **/
    private String code;
    /** 类型id **/
    private String productType;
    /** 贷款金额 **/
    private String loanFee;
    /** 贷款利率 **/
    private String loanRate;
    /** 贷款期限 **/
    private String loanLength;
    /** 是否满标(0-是、1-否) **/
    private String fullScale;
    /** 满标时间 **/
    private String fullTime;
    /** 审核状态 **/
    private String shFlag;
    /** 适合人群 **/
    private String crowds;
    /** 还款方式 **/
    private String repayment;
    /** 担保人 **/
    private String sponsor;
    /** 投标进度 **/
    private String bidProgress;
    /** 借款本息 **/
    private String pricipalInterest;
    /** 额度范围(起始) **/
    private String creditstart;
    /** 额度范围(截止) **/
    private String creditend;
    /** 项目可投金额 **/
    private String wbiddingFee;
    /** 待还本息 **/
    private String waitingRepayPrice;
    /** 剩余天数 **/
    private String surplusDay;
    /** 下期还款日 **/
    private String repayTime;
    /** 投资数量 **/
    private String investCount;
    /** 账户可用余额 **/
    private String balance;
    /** 还款状态(0-还款中、1-借款中、2-已还清) **/
    private String repayFlg;
    /** 会员对象 **/
    private MemberInfoMessageBean memberInfoMessageBean;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getProductType() {
        return productType;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getLoanFee() {
        return loanFee;
    }
    public void setLoanFee(String loanFee) {
        this.loanFee = loanFee;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getLoanLength() {
        return loanLength;
    }
    public void setLoanLength(String loanLength) {
        this.loanLength = loanLength;
    }

    public String getFullScale() {
        return fullScale;
    }
    public void setFullScale(String fullScale) {
        this.fullScale = fullScale;
    }

    public String getFullTime() {
        return fullTime;
    }
    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getCrowds() {
        return crowds;
    }
    public void setCrowds(String crowds) {
        this.crowds = crowds;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getSponsor() {
        return sponsor;
    }
    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getBidProgress() {
        return bidProgress;
    }
    public void setBidProgress(String bidProgress) {
        this.bidProgress = bidProgress;
    }

    public String getPricipalInterest() {
        return pricipalInterest;
    }
    public void setPricipalInterest(String pricipalInterest) {
        this.pricipalInterest = pricipalInterest;
    }

    public String getCreditstart() {
        return creditstart;
    }
    public void setCreditstart(String creditstart) {
        this.creditstart = creditstart;
    }

    public String getCreditend() {
        return creditend;
    }
    public void setCreditend(String creditend) {
        this.creditend = creditend;
    }

    public String getWbiddingFee() {
        return wbiddingFee;
    }
    public void setWbiddingFee(String wbiddingFee) {
        this.wbiddingFee = wbiddingFee;
    }

    public String getWaitingRepayPrice() {
        return waitingRepayPrice;
    }
    public void setWaitingRepayPrice(String waitingRepayPrice) {
        this.waitingRepayPrice = waitingRepayPrice;
    }

    public String getSurplusDay() {
        return surplusDay;
    }
    public void setSurplusDay(String surplusDay) {
        this.surplusDay = surplusDay;
    }

    public String getRepayTime() {
        return repayTime;
    }
    public void setRepayTime(String repayTime) {
        this.repayTime = repayTime;
    }

    public String getInvestCount() {
        return investCount;
    }
    public void setInvestCount(String investCount) {
        this.investCount = investCount;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getRepayFlg() {
        return repayFlg;
    }
    public void setRepayFlg(String repayFlg) {
        this.repayFlg = repayFlg;
    }

    public MemberInfoMessageBean getMemberInfoMessageBean() {
        return memberInfoMessageBean;
    }
    public void setMemberInfoMessageBean(MemberInfoMessageBean memberInfoMessageBean) {
        this.memberInfoMessageBean = memberInfoMessageBean;
    }

    @Override
    public String toString() {
        return "InvestProductMessageBean{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", productType='" + productType + '\'' +
                ", loanFee='" + loanFee + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", loanLength='" + loanLength + '\'' +
                ", fullScale='" + fullScale + '\'' +
                ", fullTime='" + fullTime + '\'' +
                ", shFlag='" + shFlag + '\'' +
                ", crowds='" + crowds + '\'' +
                ", repayment='" + repayment + '\'' +
                ", sponsor='" + sponsor + '\'' +
                ", bidProgress='" + bidProgress + '\'' +
                ", pricipalInterest='" + pricipalInterest + '\'' +
                ", creditstart='" + creditstart + '\'' +
                ", creditend='" + creditend + '\'' +
                ", wbiddingFee='" + wbiddingFee + '\'' +
                ", waitingRepayPrice='" + waitingRepayPrice + '\'' +
                ", surplusDay='" + surplusDay + '\'' +
                ", repayTime='" + repayTime + '\'' +
                ", investCount='" + investCount + '\'' +
                ", balance='" + balance + '\'' +
                ", repayFlg='" + repayFlg + '\'' +
                ", memberInfoMessageBean=" + memberInfoMessageBean +
                '}';
    }
}
