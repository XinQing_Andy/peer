package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-10-2.
 */
public class WeathInformationMessageBean {

    /** 信息 **/
    private String info;
    /** 编码 **/
    private String result;
    /** 菜单地址 **/
    private String meneUrl;
    /** 新闻id **/
    private String id;
    /** 题目 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 一级类别id **/
    private String firstTypeId;
    /** 二级类别id **/
    private String secondTypeId;
    /** 显示时间 **/
    private String showDate;
    /** 排序类型 **/
    private String orderType;
    /** 排序字段 **/
    private String orderColumn;
    /** 起始数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 财富资讯部分拼接 **/
    private String weathInformationStr;
    /** 分页部分拼接 **/
    private String pageStr;

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getMeneUrl() {
        return meneUrl;
    }
    public void setMeneUrl(String meneUrl) {
        this.meneUrl = meneUrl;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getFirstTypeId() {
        return firstTypeId;
    }
    public void setFirstTypeId(String firstTypeId) {
        this.firstTypeId = firstTypeId;
    }

    public String getSecondTypeId() {
        return secondTypeId;
    }
    public void setSecondTypeId(String secondTypeId) {
        this.secondTypeId = secondTypeId;
    }

    public String getShowDate() {
        return showDate;
    }
    public void setShowDate(String showDate) {
        this.showDate = showDate;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getWeathInformationStr() {
        return weathInformationStr;
    }
    public void setWeathInformationStr(String weathInformationStr) {
        this.weathInformationStr = weathInformationStr;
    }

    public String getPageStr() {
        return pageStr;
    }
    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    @Override
    public String toString() {
        return "WeathInformationMessageBean{" +
                "info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", meneUrl='" + meneUrl + '\'' +
                ", id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", firstTypeId='" + firstTypeId + '\'' +
                ", secondTypeId='" + secondTypeId + '\'' +
                ", showDate='" + showDate + '\'' +
                ", orderType='" + orderType + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", weathInformationStr='" + weathInformationStr + '\'' +
                ", pageStr='" + pageStr + '\'' +
                '}';
    }
}
