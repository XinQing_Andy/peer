package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-8.
 */
public class AppSysFileMessageBean {

    /** 主键 **/
    private String id;
    /** 关联id **/
    private String glId;
    /** 图片地址 **/
    private String imgUrl;
    /** 创建时间 **/
    private String createTime;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getGlId() {
        return glId;
    }
    public void setGlId(String glId) {
        this.glId = glId;
    }

    public String getImgUrl() {
        return imgUrl;
    }
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "AppSysFileMessageBean{" +
                "id='" + id + '\'' +
                ", glId='" + glId + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
