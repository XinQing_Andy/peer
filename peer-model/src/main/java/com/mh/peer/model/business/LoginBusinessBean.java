package com.mh.peer.model.business;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.SysDept;
import com.mh.peer.model.entity.SysRoleUser;
import com.mh.peer.model.entity.SysUser;

import java.util.List;

/**
 * Created by zhaojiaming on 2016/1/24.
 */
public class LoginBusinessBean {
    /**
     *返回成功或失败(error,success)
     */
    private String result;

    /**
     * 返回结果说明
     */
    private String info;

    /**
     * 用户
     */
    private SysUser sysUser;

    /**
     * 部门
     */
    private SysDept sysDept;

    /**
     * 用户角色
     */
    private List<SysRoleUser> roleUserListr;

    /**
     * 角色对菜单
     */
    private List<UserInfoBusiness> userInfoList;

    /**
     *
     */
    private SysUser user;

    /**
     *
     */
    private MemberInfo memberInfo;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public SysDept getSysDept() {
        return sysDept;
    }

    public void setSysDept(SysDept sysDept) {
        this.sysDept = sysDept;
    }

    public List<SysRoleUser> getRoleUserListr() {
        return roleUserListr;
    }

    public void setRoleUserListr(List<SysRoleUser> roleUserListr) {
        this.roleUserListr = roleUserListr;
    }

    public List<UserInfoBusiness> getUserInfoList() {
        return userInfoList;
    }

    public void setUserInfoList(List<UserInfoBusiness> userInfoList) {
        this.userInfoList = userInfoList;
    }

    public SysUser getUser() {
        return user;
    }

    public void setUser(SysUser user) {
        this.user = user;
    }

    public MemberInfo getMemberInfo() {
        return memberInfo;
    }

    public void setMemberInfo(MemberInfo memberInfo) {
        this.memberInfo = memberInfo;
    }

    @Override
    public String toString() {
        return "LoginBusinessBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", sysUser=" + sysUser +
                ", sysDept=" + sysDept +
                ", roleUserListr=" + roleUserListr +
                ", userInfoList=" + userInfoList +
                ", user=" + user +
                ", memberInfo=" + memberInfo +
                '}';
    }
}
