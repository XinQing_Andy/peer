package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-9-18.
 * 平台收入
 */
public class PlatIncomeMessageBean {

    /** 菜单 **/
    private String menuUrl;
    /** 平台收入 **/
    private String platIncome;
    /** 未投资充值手续费 **/
    private String unInvestChargeFee;
    /** 充值手续费 **/
    private String reChargeMerFee;
    /** 提现手续费 **/
    private String postalMerFee;
    /** 债权转让管理费 **/
    private String attornMerFee;
    /** 数据日期 **/
    private String dataTime;
    /** 操作时间 **/
    private String operateTime;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 排序类型 **/
    private String orderType;
    /** 排序字段 **/
    private String orderColumn;
    /** 当前页 **/
    private String currentPage;
    /** 起始数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 统计列表部分拼接 **/
    private String platStatisticStr;
    /** 统计列表部分数量 **/
    private String totalCount;
    /** 总页数 **/
    private String pageCount;


    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getPlatIncome() {
        return platIncome;
    }
    public void setPlatIncome(String platIncome) {
        this.platIncome = platIncome;
    }

    public String getUnInvestChargeFee() {
        return unInvestChargeFee;
    }
    public void setUnInvestChargeFee(String unInvestChargeFee) {
        this.unInvestChargeFee = unInvestChargeFee;
    }

    public String getReChargeMerFee() {
        return reChargeMerFee;
    }
    public void setReChargeMerFee(String reChargeMerFee) {
        this.reChargeMerFee = reChargeMerFee;
    }

    public String getPostalMerFee() {
        return postalMerFee;
    }
    public void setPostalMerFee(String postalMerFee) {
        this.postalMerFee = postalMerFee;
    }

    public String getAttornMerFee() {
        return attornMerFee;
    }
    public void setAttornMerFee(String attornMerFee) {
        this.attornMerFee = attornMerFee;
    }

    public String getDataTime() {
        return dataTime;
    }
    public void setDataTime(String dataTime) {
        this.dataTime = dataTime;
    }

    public String getOperateTime() {
        return operateTime;
    }
    public void setOperateTime(String operateTime) {
        this.operateTime = operateTime;
    }

    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPlatStatisticStr() {
        return platStatisticStr;
    }
    public void setPlatStatisticStr(String platStatisticStr) {
        this.platStatisticStr = platStatisticStr;
    }

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getPageCount() {
        return pageCount;
    }
    public void setPageCount(String pageCount) {
        this.pageCount = pageCount;
    }

    @Override
    public String toString() {
        return "PlatIncomeMessageBean{" +
                "menuUrl='" + menuUrl + '\'' +
                ", platIncome='" + platIncome + '\'' +
                ", unInvestChargeFee='" + unInvestChargeFee + '\'' +
                ", reChargeMerFee='" + reChargeMerFee + '\'' +
                ", postalMerFee='" + postalMerFee + '\'' +
                ", attornMerFee='" + attornMerFee + '\'' +
                ", dataTime='" + dataTime + '\'' +
                ", operateTime='" + operateTime + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", orderType='" + orderType + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", platStatisticStr='" + platStatisticStr + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", pageCount='" + pageCount + '\'' +
                '}';
    }
}
