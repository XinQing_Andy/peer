package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/7/26.
 */
public class ViewHuanxunRechargeMes {

    /** 页面地址 **/
    private String menuUrl;
    /** 主键 **/
    private String id;
    /** 关联id **/
    private String glid;
    /** 充值用户id **/
    private String memberid;
    /** 来源(0-电脑端、1-手机端) **/
    private String source;
    /** 响应码 **/
    private String resultcode;
    /** 响应信息描述 **/
    private String resultmsg;
    /** 商户存管交易账号 **/
    private String merchantid;
    /** 签名 **/
    private String sign;
    /** 商户订单号 **/
    private String merbillno;
    /** 充值类型(1-普通充值、2-还款充值) **/
    private String deposittype;
    /** 渠道种类(1-个人网银、2-企业网银) **/
    private String channeltype;
    /** 充值银行 **/
    private String bankcode;
    /** IPS订单号 **/
    private String ipsbillno;
    /** IPS处理时间 **/
    private String ipsdotime;
    /** 用户IPS实际到账金额 **/
    private String ipstrdamt;
    /** IPS手续费金额 **/
    private String ipsfee;
    /** 平台手续费 **/
    private String merfee;
    /** 充值状态(0-失败、1-成功、2-处理中) **/
    private String trdstatus;
    /** 创建时间 **/
    private String createtime;
    /** 是否第一次进入 **/
    private String isFirst;
    /** 充值人 **/
    private String realname;
    /** 身份证号 **/
    private String idcard;
    /**  排序字段  **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 返回消息 **/
    private String info;
    /** 返回标志 **/
    private String result;

    /** 显示所属分页 **/
    private String pageStr;
    /** 拼接内容 **/
    private String str;


    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGlid() {
        return glid;
    }

    public void setGlid(String glid) {
        this.glid = glid;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getResultcode() {
        return resultcode;
    }

    public void setResultcode(String resultcode) {
        this.resultcode = resultcode;
    }

    public String getResultmsg() {
        return resultmsg;
    }

    public void setResultmsg(String resultmsg) {
        this.resultmsg = resultmsg;
    }

    public String getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getMerbillno() {
        return merbillno;
    }

    public void setMerbillno(String merbillno) {
        this.merbillno = merbillno;
    }

    public String getDeposittype() {
        return deposittype;
    }

    public void setDeposittype(String deposittype) {
        this.deposittype = deposittype;
    }

    public String getChanneltype() {
        return channeltype;
    }

    public void setChanneltype(String channeltype) {
        this.channeltype = channeltype;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getIpsbillno() {
        return ipsbillno;
    }

    public void setIpsbillno(String ipsbillno) {
        this.ipsbillno = ipsbillno;
    }

    public String getIpsdotime() {
        return ipsdotime;
    }

    public void setIpsdotime(String ipsdotime) {
        this.ipsdotime = ipsdotime;
    }

    public String getIpstrdamt() {
        return ipstrdamt;
    }

    public void setIpstrdamt(String ipstrdamt) {
        this.ipstrdamt = ipstrdamt;
    }

    public String getIpsfee() {
        return ipsfee;
    }

    public void setIpsfee(String ipsfee) {
        this.ipsfee = ipsfee;
    }

    public String getMerfee() {
        return merfee;
    }

    public void setMerfee(String merfee) {
        this.merfee = merfee;
    }

    public String getTrdstatus() {
        return trdstatus;
    }

    public void setTrdstatus(String trdstatus) {
        this.trdstatus = trdstatus;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(String isFirst) {
        this.isFirst = isFirst;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getOrderColumn() {
        return orderColumn;
    }

    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }



    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getPageStr() {
        return pageStr;
    }

    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return "ViewHuanxunRechargeMes{" +
                "menuUrl='" + menuUrl + '\'' +
                ", id='" + id + '\'' +
                ", glid='" + glid + '\'' +
                ", memberid='" + memberid + '\'' +
                ", source='" + source + '\'' +
                ", resultcode='" + resultcode + '\'' +
                ", resultmsg='" + resultmsg + '\'' +
                ", merchantid='" + merchantid + '\'' +
                ", sign='" + sign + '\'' +
                ", merbillno='" + merbillno + '\'' +
                ", deposittype='" + deposittype + '\'' +
                ", channeltype='" + channeltype + '\'' +
                ", bankcode='" + bankcode + '\'' +
                ", ipsbillno='" + ipsbillno + '\'' +
                ", ipsdotime='" + ipsdotime + '\'' +
                ", ipstrdamt='" + ipstrdamt + '\'' +
                ", ipsfee='" + ipsfee + '\'' +
                ", merfee='" + merfee + '\'' +
                ", trdstatus='" + trdstatus + '\'' +
                ", createtime='" + createtime + '\'' +
                ", isFirst='" + isFirst + '\'' +
                ", realname='" + realname + '\'' +
                ", idcard='" + idcard + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", pageStr='" + pageStr + '\'' +
                ", str='" + str + '\'' +
                '}';
    }
}
