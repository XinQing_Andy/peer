package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-6.
 */
public class AppProductDetailMessageBean {

    /** 主键 **/
    private String id;
    /** 题目 **/
    private String title;
    /** 贷款利率 **/
    private String loanRate;
    /** 剩余投资金额 **/
    private String surplusPrice;
    /** 产品投标进度 **/
    private String investProgress;
    /** 贷款期限(年) **/
    private String loanYear;
    /** 贷款期限(月) **/
    private String loanMonth;
    /** 贷款期限(日) **/
    private String loanDay;
    /** 贷款时长 **/
    private String loanLength;
    /** 是否满标(0-是、1-否) **/
    private String fullScale;
    /** 编号 **/
    private String code;
    /** 需还本息 **/
    private String repayPrice;
    /** 满标日期(发标日期) **/
    private String fullTime;
    /** 贷款金额 **/
    private String loanFee;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 项目ID号 **/
    private String projectNo;
    /** 最低投标金额 **/
    private String bidFee;
    /** 最大拆分份数 **/
    private String splitCount;
    /** 申请人 **/
    private String applyMember;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getSurplusPrice() {
        return surplusPrice;
    }
    public void setSurplusPrice(String surplusPrice) {
        this.surplusPrice = surplusPrice;
    }

    public String getInvestProgress() {
        return investProgress;
    }
    public void setInvestProgress(String investProgress) {
        this.investProgress = investProgress;
    }

    public String getLoanYear() {
        return loanYear;
    }
    public void setLoanYear(String loanYear) {
        this.loanYear = loanYear;
    }

    public String getLoanMonth() {
        return loanMonth;
    }
    public void setLoanMonth(String loanMonth) {
        this.loanMonth = loanMonth;
    }

    public String getLoanDay() {
        return loanDay;
    }
    public void setLoanDay(String loanDay) {
        this.loanDay = loanDay;
    }

    public String getLoanLength() {
        return loanLength;
    }
    public void setLoanLength(String loanLength) {
        this.loanLength = loanLength;
    }

    public String getFullScale() {
        return fullScale;
    }
    public void setFullScale(String fullScale) {
        this.fullScale = fullScale;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getRepayPrice() {
        return repayPrice;
    }
    public void setRepayPrice(String repayPrice) {
        this.repayPrice = repayPrice;
    }

    public String getFullTime() {
        return fullTime;
    }
    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getLoanFee() {
        return loanFee;
    }
    public void setLoanFee(String loanFee) {
        this.loanFee = loanFee;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getProjectNo() {
        return projectNo;
    }
    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getBidFee() {
        return bidFee;
    }
    public void setBidFee(String bidFee) {
        this.bidFee = bidFee;
    }

    public String getSplitCount() {
        return splitCount;
    }
    public void setSplitCount(String splitCount) {
        this.splitCount = splitCount;
    }

    public String getApplyMember() {
        return applyMember;
    }
    public void setApplyMember(String applyMember) {
        this.applyMember = applyMember;
    }

    @Override
    public String toString() {
        return "AppProductDetailMessageBean{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", surplusPrice='" + surplusPrice + '\'' +
                ", investProgress='" + investProgress + '\'' +
                ", loanYear='" + loanYear + '\'' +
                ", loanMonth='" + loanMonth + '\'' +
                ", loanDay='" + loanDay + '\'' +
                ", loanLength='" + loanLength + '\'' +
                ", fullScale='" + fullScale + '\'' +
                ", code='" + code + '\'' +
                ", repayPrice='" + repayPrice + '\'' +
                ", fullTime='" + fullTime + '\'' +
                ", loanFee='" + loanFee + '\'' +
                ", repayment='" + repayment + '\'' +
                ", projectNo='" + projectNo + '\'' +
                ", bidFee='" + bidFee + '\'' +
                ", splitCount='" + splitCount + '\'' +
                ", applyMember='" + applyMember + '\'' +
                '}';
    }
}
