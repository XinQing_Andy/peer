package com.mh.peer.model.business;

/**
 * Created by zhangerxin on 2016-4-1.
 */
public class SysSecurityQuestionBusinessBean {

    /*
     * 返回成功或失败(error,success)
     * */
    private String result;
    /*
     *  错误消息
     * */
    private String info;

    /**返回主键**/
    private String id;
    /**返回关键词**/
    private String keyword;
    /**返回问题**/
    private String question;
    /**返回是否启用**/
    private String active_flg;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getQuestion() {
        return question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }

    public String getActive_flg() {
        return active_flg;
    }
    public void setActive_flg(String active_flg) {
        this.active_flg = active_flg;
    }

    @Override
    public String toString() {
        return "SysSecurityQuestionBusinessBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", keyword='" + keyword + '\'' +
                ", question='" + question + '\'' +
                ", active_flg='" + active_flg + '\'' +
                '}';
    }
}
