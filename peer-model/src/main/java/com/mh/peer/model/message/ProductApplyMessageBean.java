package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-9-7.
 * 已申请项目
 */
public class ProductApplyMessageBean {

    /** 产品id **/
    private String productId;
    /** 题目 **/
    private String title;
    /** 编码 **/
    private String code;
    /** 申请日期 **/
    private String applyTime;
    /** 申请起日期 **/
    private String applyStartTime;
    /** 申请止日期 **/
    private String applyEndTime;
    /** 申请人id **/
    private String applyMemberId;
    /** 年利率 **/
    private String loanRate;
    /** 贷款时长 **/
    private String loanLength;
    /** 贷款金额 **/
    private String loanPrice;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 列表部分拼接 **/
    private String listStr;
    /** 分页部分拼接 **/
    private String pageStr;
    /** 当前数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getApplyTime() {
        return applyTime;
    }
    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getApplyStartTime() {
        return applyStartTime;
    }
    public void setApplyStartTime(String applyStartTime) {
        this.applyStartTime = applyStartTime;
    }

    public String getApplyEndTime() {
        return applyEndTime;
    }
    public void setApplyEndTime(String applyEndTime) {
        this.applyEndTime = applyEndTime;
    }

    public String getApplyMemberId() {
        return applyMemberId;
    }
    public void setApplyMemberId(String applyMemberId) {
        this.applyMemberId = applyMemberId;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getLoanLength() {
        return loanLength;
    }
    public void setLoanLength(String loanLength) {
        this.loanLength = loanLength;
    }

    public String getLoanPrice() {
        return loanPrice;
    }
    public void setLoanPrice(String loanPrice) {
        this.loanPrice = loanPrice;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getListStr() {
        return listStr;
    }
    public void setListStr(String listStr) {
        this.listStr = listStr;
    }

    public String getPageStr() {
        return pageStr;
    }
    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "ProductApplyMessageBean{" +
                "productId='" + productId + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", applyTime='" + applyTime + '\'' +
                ", applyStartTime='" + applyStartTime + '\'' +
                ", applyEndTime='" + applyEndTime + '\'' +
                ", applyMemberId='" + applyMemberId + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", loanLength='" + loanLength + '\'' +
                ", loanPrice='" + loanPrice + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", listStr='" + listStr + '\'' +
                ", pageStr='" + pageStr + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                '}';
    }
}
