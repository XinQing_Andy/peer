package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-22.
 * 环迅注册
 */
public class HuanXunRegistMessageBean {

    /** 主键 **/
    private String id;
    /** 商户订单号 **/
    private String merBillNo;
    /** 开户日期 **/
    private String merDate;
    /** 用户类型(1-个人、2-企业) **/
    private String userType;
    /** 用户名 **/
    private String userName;
    /** 手机号 **/
    private String mobileNo;
    /** 身份证号 **/
    private String identNo;
    /** 业务类型(1-P2P、2-众筹) **/
    private String bizType;
    /** 真实姓名 **/
    private String realName;
    /** 企业名称 **/
    private String enterName;
    /** 营业执照编码 **/
    private String orgCode;
    /** 是否为担保企业 **/
    private String isAssureCom;
    /** 页面返回地址 **/
    private String webUrl;
    /** 后台通知地址 **/
    private String s2SUrl;
    /** 操作类型 **/
    private String operationType;
    /** 商户存管交易账号 **/
    private String merchantID;
    /** 签名 **/
    private String sign;
    /** 请求信息 **/
    private String request;
    /** 会员id **/
    private String memberId;
    /** IPS虚拟账号 **/
    private String ipsAcctNo;
    /** 余额 **/
    private String balance;
    /** 执行编码 **/
    private String code;
    /** 执行信息 **/
    private String info;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getMerBillNo() {
        return merBillNo;
    }
    public void setMerBillNo(String merBillNo) {
        this.merBillNo = merBillNo;
    }

    public String getMerDate() {
        return merDate;
    }
    public void setMerDate(String merDate) {
        this.merDate = merDate;
    }

    public String getUserType() {
        return userType;
    }
    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNo() {
        return mobileNo;
    }
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getIdentNo() {
        return identNo;
    }
    public void setIdentNo(String identNo) {
        this.identNo = identNo;
    }

    public String getBizType() {
        return bizType;
    }
    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getEnterName() {
        return enterName;
    }
    public void setEnterName(String enterName) {
        this.enterName = enterName;
    }

    public String getOrgCode() {
        return orgCode;
    }
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getIsAssureCom() {
        return isAssureCom;
    }
    public void setIsAssureCom(String isAssureCom) {
        this.isAssureCom = isAssureCom;
    }

    public String getWebUrl() {
        return webUrl;
    }
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getS2SUrl() {
        return s2SUrl;
    }
    public void setS2SUrl(String s2SUrl) {
        this.s2SUrl = s2SUrl;
    }

    public String getOperationType() {
        return operationType;
    }
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getMerchantID() {
        return merchantID;
    }
    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getSign() {
        return sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }

    public String getMemberId() {
        return memberId;
    }
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getIpsAcctNo() {
        return ipsAcctNo;
    }
    public void setIpsAcctNo(String ipsAcctNo) {
        this.ipsAcctNo = ipsAcctNo;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }


    @Override
    public String toString() {
        return "HuanXunRegistMessageBean{" +
                "id='" + id + '\'' +
                ", merBillNo='" + merBillNo + '\'' +
                ", merDate='" + merDate + '\'' +
                ", userType='" + userType + '\'' +
                ", userName='" + userName + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", identNo='" + identNo + '\'' +
                ", bizType='" + bizType + '\'' +
                ", realName='" + realName + '\'' +
                ", enterName='" + enterName + '\'' +
                ", orgCode='" + orgCode + '\'' +
                ", isAssureCom='" + isAssureCom + '\'' +
                ", webUrl='" + webUrl + '\'' +
                ", s2SUrl='" + s2SUrl + '\'' +
                ", operationType='" + operationType + '\'' +
                ", merchantID='" + merchantID + '\'' +
                ", sign='" + sign + '\'' +
                ", request='" + request + '\'' +
                ", memberId='" + memberId + '\'' +
                ", ipsAcctNo='" + ipsAcctNo + '\'' +
                ", balance='" + balance + '\'' +
                ", code='" + code + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
