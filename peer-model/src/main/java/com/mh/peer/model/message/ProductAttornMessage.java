package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-6-23.
 * 后台审核部分
 */
public class ProductAttornMessage {

    /** 返回成功或失败 **/
    private String result;
    /** 返回结果说明 **/
    private String info;
    /** 返回地址 **/
    private String menuUrl;
    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String productBuyId;
    /** 产品id **/
    private String productId;
    /** 转让价格 **/
    private String price;
    /** 预期收益率 **/
    private String expectedRate;
    /** 审核状态 **/
    private String shFlag;
    /** 审核意见 **/
    private String shyj;
    /** 承接人id **/
    private String underTakeMember;
    /** 承接时间 **/
    private String underTakeTime;
    /** 创建时间 **/
    private String createTime;
    /** 题目 **/
    private String title;
    /** 标题 **/
    private String code;
    /** 购买人姓名 **/
    private String buyerName;
    /** 承接人姓名 **/
    private String underTakeMemberName;
    /** 年利率 **/
    private String loanRate;
    /** 借款期限(年) **/
    private String loanYear;
    /** 借款期限(月) **/
    private String loanMonth;
    /** 借款期限(日) **/
    private String loanDay;
    /** 借款期限(时长) **/
    private String loanLength;
    /** 借款额度 **/
    private String loadFee;
    /** 剩余期数 **/
    private String surplusQs;
    /** 待收本金 **/
    private String waitingPrincipal;
    /** 剩余时间 **/
    private String surplusDays;
    /** 拼接字符串 **/
    private String attornStr;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 起始数 **/
    private String pageIndex;
    /** 每次显示数量 **/
    private String pageSize;
    /** 当前页 **/
    private String currentPage;
    /** 总数量 **/
    private String totalCount;


    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductBuyId() {
        return productBuyId;
    }
    public void setProductBuyId(String productBuyId) {
        this.productBuyId = productBuyId;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getExpectedRate() {
        return expectedRate;
    }
    public void setExpectedRate(String expectedRate) {
        this.expectedRate = expectedRate;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getShyj() {
        return shyj;
    }
    public void setShyj(String shyj) {
        this.shyj = shyj;
    }

    public String getUnderTakeMember() {
        return underTakeMember;
    }
    public void setUnderTakeMember(String underTakeMember) {
        this.underTakeMember = underTakeMember;
    }

    public String getUnderTakeTime() {
        return underTakeTime;
    }
    public void setUnderTakeTime(String underTakeTime) {
        this.underTakeTime = underTakeTime;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getBuyerName() {
        return buyerName;
    }
    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getUnderTakeMemberName() {
        return underTakeMemberName;
    }
    public void setUnderTakeMemberName(String underTakeMemberName) {
        this.underTakeMemberName = underTakeMemberName;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getLoanYear() {
        return loanYear;
    }
    public void setLoanYear(String loanYear) {
        this.loanYear = loanYear;
    }

    public String getLoanMonth() {
        return loanMonth;
    }
    public void setLoanMonth(String loanMonth) {
        this.loanMonth = loanMonth;
    }

    public String getLoanDay() {
        return loanDay;
    }
    public void setLoanDay(String loanDay) {
        this.loanDay = loanDay;
    }

    public String getLoanLength() {
        return loanLength;
    }
    public void setLoanLength(String loanLength) {
        this.loanLength = loanLength;
    }

    public String getLoadFee() {
        return loadFee;
    }
    public void setLoadFee(String loadFee) {
        this.loadFee = loadFee;
    }

    public String getSurplusQs() {
        return surplusQs;
    }
    public void setSurplusQs(String surplusQs) {
        this.surplusQs = surplusQs;
    }

    public String getWaitingPrincipal() {
        return waitingPrincipal;
    }
    public void setWaitingPrincipal(String waitingPrincipal) {
        this.waitingPrincipal = waitingPrincipal;
    }

    public String getSurplusDays() {
        return surplusDays;
    }
    public void setSurplusDays(String surplusDays) {
        this.surplusDays = surplusDays;
    }

    public String getAttornStr() {
        return attornStr;
    }
    public void setAttornStr(String attornStr) {
        this.attornStr = attornStr;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public String toString() {
        return "ProductAttornMessage{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", id='" + id + '\'' +
                ", productBuyId='" + productBuyId + '\'' +
                ", productId='" + productId + '\'' +
                ", price='" + price + '\'' +
                ", expectedRate='" + expectedRate + '\'' +
                ", shFlag='" + shFlag + '\'' +
                ", shyj='" + shyj + '\'' +
                ", underTakeMember='" + underTakeMember + '\'' +
                ", underTakeTime='" + underTakeTime + '\'' +
                ", createTime='" + createTime + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", buyerName='" + buyerName + '\'' +
                ", underTakeMemberName='" + underTakeMemberName + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", loanYear='" + loanYear + '\'' +
                ", loanMonth='" + loanMonth + '\'' +
                ", loanDay='" + loanDay + '\'' +
                ", loanLength='" + loanLength + '\'' +
                ", loadFee='" + loadFee + '\'' +
                ", surplusQs='" + surplusQs + '\'' +
                ", waitingPrincipal='" + waitingPrincipal + '\'' +
                ", surplusDays='" + surplusDays + '\'' +
                ", attornStr='" + attornStr + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", totalCount='" + totalCount + '\'' +
                '}';
    }
}
