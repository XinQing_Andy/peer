package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/4/12
 */
public class MemberFeeRelationMessageBean {
    /** 主键 **/
    private int id;
    /** 会员等级id **/
    private int levelId;
    /** 费用id **/
    private int feeId;
    /** 比例 **/
    private Double levelProportion;
    /** 比例 前后台传值用 **/
    private String lpStr;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /**  **/
    private String jetxName;
    /**  **/
    private String menuUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public int getFeeId() {
        return feeId;
    }

    public void setFeeId(int feeId) {
        this.feeId = feeId;
    }

    public Double getLevelProportion() {
        return levelProportion;
    }

    public void setLevelProportion(Double levelProportion) {
        this.levelProportion = levelProportion;
    }

    public String getLpStr() {
        return lpStr;
    }

    public void setLpStr(String lpStr) {
        this.lpStr = lpStr;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    @Override
    public String toString() {
        return "MemberFeeRelationMessageBean{" +
                "id=" + id +
                ", levelId=" + levelId +
                ", feeId=" + feeId +
                ", levelProportion=" + levelProportion +
                ", lpStr='" + lpStr + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", jetxName='" + jetxName + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                '}';
    }
}
