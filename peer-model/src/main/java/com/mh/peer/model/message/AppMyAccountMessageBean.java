package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-14.
 */
public class AppMyAccountMessageBean {

    /** 主键 **/
    private String id;
    /** 余额 **/
    private String balance;
    /** 待收总额 **/
    private String receivedPrice;
    /** 账户资产 **/
    private String accountPrice;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getReceivedPrice() {
        return receivedPrice;
    }
    public void setReceivedPrice(String receivedPrice) {
        this.receivedPrice = receivedPrice;
    }

    public String getAccountPrice() {
        return accountPrice;
    }
    public void setAccountPrice(String accountPrice) {
        this.accountPrice = accountPrice;
    }

    @Override
    public String toString() {
        return "AppMyAccountMessageBean{" +
                "id='" + id + '\'' +
                ", balance='" + balance + '\'' +
                ", receivedPrice='" + receivedPrice + '\'' +
                ", accountPrice='" + accountPrice + '\'' +
                '}';
    }
}
