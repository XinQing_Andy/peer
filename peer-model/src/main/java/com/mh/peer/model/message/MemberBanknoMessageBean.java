package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/5/18.
 */
public class MemberBanknoMessageBean {

    /** 主键 **/
    private String id;
    /** 会员id **/
    private String memberid;
    /** 银行卡号 **/
    private String cardno;
    /** 银行id **/
    private String bankid;
    /** 银行编号 **/
    private String bankNo;
    /** 省id **/
    private String province;
    /** 城市id **/
    private String city;
    /** 开户行支行名称 **/
    private String bankaddress;
    /** 交易密码 **/
    private String tradepassword;
    /** 银行名称 **/
    private String bankName;
    /** 真实名称 **/
    private String realName;
    /** 余额 **/
    private String balance;
    /** 创建用户 **/
    private String createuser;
    /** 添加时间 **/
    private String createtime;
    /** 更新人 **/
    private String updateuser;
    /** 最后更新时间 **/
    private String updatetime;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /** 页面：当前银行卡账号  **/
    private String nowCardno;
    /** 验证码  **/
    private String code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getBankid() {
        return bankid;
    }

    public String getBankNo() {
        return bankNo;
    }
    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBankaddress() {
        return bankaddress;
    }

    public void setBankaddress(String bankaddress) {
        this.bankaddress = bankaddress;
    }

    public String getTradepassword() {
        return tradepassword;
    }
    public void setTradepassword(String tradepassword) {
        this.tradepassword = tradepassword;
    }

    public String getBankName() {
        return bankName;
    }
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(String updateuser) {
        this.updateuser = updateuser;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getNowCardno() {
        return nowCardno;
    }

    public void setNowCardno(String nowCardno) {
        this.nowCardno = nowCardno;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "MemberBanknoMessageBean{" +
                "id='" + id + '\'' +
                ", memberid='" + memberid + '\'' +
                ", cardno='" + cardno + '\'' +
                ", bankid='" + bankid + '\'' +
                ", bankNo='" + bankNo + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", bankaddress='" + bankaddress + '\'' +
                ", tradepassword='" + tradepassword + '\'' +
                ", bankName='" + bankName + '\'' +
                ", realName='" + realName + '\'' +
                ", balance='" + balance + '\'' +
                ", createuser='" + createuser + '\'' +
                ", createtime='" + createtime + '\'' +
                ", updateuser='" + updateuser + '\'' +
                ", updatetime='" + updatetime + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", nowCardno='" + nowCardno + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
