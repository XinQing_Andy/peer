package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/6/20.
 */
public class PostalRecordMessageBean {

    /** 主键 **/
    private String id;
    /** 商户订单号 **/
    private String merbillno;
    /** 实际提现金额 **/
    private String sprice;
    /** 手续费 **/
    private String procedurefee;
    /** 状态(0-提现成功、1-提现失败) **/
    private String flag;
    /** 提现人 **/
    private String memberid;
    /** 提现银行 **/
    private String bankid;
    /** 来源(0-电脑端、1-手机端) **/
    private String source;
    /** 提现时间 **/
    private String time;
    /** 创建人 **/
    private String createuser;
    /** 创建时间 **/
    private String createtime;
    /** 用户输入提现金额 **/
    private String userSprice;
    /** 银行名称 **/
    private String bankName;
    /** 提现总额 **/
    private String postalprice;

    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 是否第一次点击 **/
    private String isFirst;

    /**  **/
    private String info;
    /**  **/
    private String result;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMerbillno() {
        return merbillno;
    }

    public void setMerbillno(String merbillno) {
        this.merbillno = merbillno;
    }

    public String getSprice() {
        return sprice;
    }

    public void setSprice(String sprice) {
        this.sprice = sprice;
    }

    public String getProcedurefee() {
        return procedurefee;
    }

    public void setProcedurefee(String procedurefee) {
        this.procedurefee = procedurefee;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUserSprice() {
        return userSprice;
    }

    public void setUserSprice(String userSprice) {
        this.userSprice = userSprice;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getPostalprice() {
        return postalprice;
    }

    public void setPostalprice(String postalprice) {
        this.postalprice = postalprice;
    }

    public String getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(String isFirst) {
        this.isFirst = isFirst;
    }
}
