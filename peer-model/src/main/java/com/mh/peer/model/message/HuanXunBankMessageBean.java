package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-24.
 */
public class HuanXunBankMessageBean {

    /** 签名 **/
    private String sign;
    /** 请求信息 **/
    private String request;

    public String getSign() {
        return sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "HuanXunBankMessageBean{" +
                "sign='" + sign + '\'' +
                ", request='" + request + '\'' +
                '}';
    }
}
