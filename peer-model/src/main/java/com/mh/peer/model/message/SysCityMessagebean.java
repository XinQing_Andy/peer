package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/5/3.
 */
public class SysCityMessagebean {
    /** 主键 **/
    private String id;
    /** 省id **/
    private String provinceid;
    /** 城市名称 **/
    private String cityname;
    /**  **/
    private String areacode;
    /** 城市拼接 **/
    private String cityPj;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvinceid() {
        return provinceid;
    }

    public void setProvinceid(String provinceid) {
        this.provinceid = provinceid;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode;
    }

    public String getCityPj() {
        return cityPj;
    }
    public void setCityPj(String cityPj) {
        this.cityPj = cityPj;
    }

    @Override
    public String toString() {
        return "SysCityMessagebean{" +
                "id='" + id + '\'' +
                ", provinceid='" + provinceid + '\'' +
                ", cityname='" + cityname + '\'' +
                ", areacode='" + areacode + '\'' +
                ", cityPj='" + cityPj + '\'' +
                '}';
    }
}
