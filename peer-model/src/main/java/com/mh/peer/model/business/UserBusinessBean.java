package com.mh.peer.model.business;

import com.mh.peer.model.entity.SysRoleUser;
import com.mh.peer.model.entity.SysDept;
import com.mh.peer.model.entity.SysUser;

import java.util.List;

/**
 * Created by yangbin on 2015/12/11.
 */
public class UserBusinessBean {
    /*
    * 返回成功或失败(error,success)
    * */
    private String result;
    /*
    *   错误消息
    * */
    private String info;
    /*
    * 用户
    * */
    private List<SysUser> sysUser;
    /*
    * 部门
    * */
    private List<SysDept>sysDept;
    /*
     * 权限对用户
     * */
    private List<SysRoleUser> roleUser;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<SysUser> getSysUser() {
        return sysUser;
    }

    public void setSysUser(List<SysUser> sysUser) {
        this.sysUser = sysUser;
    }

    public List<SysDept> getSysDept() {
        return sysDept;
    }

    public void setSysDept(List<SysDept> sysDept) {
        this.sysDept = sysDept;
    }

    public List<SysRoleUser> getRoleUser() {
        return roleUser;
    }

    public void setRoleUser(List<SysRoleUser> roleUser) {
        this.roleUser = roleUser;
    }

    @Override
    public String toString() {
        return "StaffBusinessBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", sysUser=" + sysUser +
                ", sysDept=" + sysDept +
                ", roleUser=" + roleUser +
                '}';
    }
}
