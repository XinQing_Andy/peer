package com.mh.peer.model.business;

/**
 * Created by zhangerxin on 2016-6-24.
 */
public class ProductMyAttornBusinessBean {

    /** 返回信息 **/
    private String info;
    /** 返回编码 **/
    private String code;
    /** 返回标志 **/
    private String result;
    /** 返回页面 **/
    private String menuUrl;
    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String productBuyId;
    /** 转让金额 **/
    private String price;
    /** 预期收益率 **/
    private String expectedRate;
    /** 审核状态 **/
    private String shFlag;
    /** 审核意见 **/
    private String shyj;
    /** 承接人id **/
    private String underTakeMember;
    /** 承接时间 **/
    private String underTakeTime;
    /** 创建时间 **/
    private String createTime;
    /** 交易密码 **/
    private String tradePassword;

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductBuyId() {
        return productBuyId;
    }
    public void setProductBuyId(String productBuyId) {
        this.productBuyId = productBuyId;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getExpectedRate() {
        return expectedRate;
    }
    public void setExpectedRate(String expectedRate) {
        this.expectedRate = expectedRate;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getShyj() {
        return shyj;
    }
    public void setShyj(String shyj) {
        this.shyj = shyj;
    }

    public String getUnderTakeMember() {
        return underTakeMember;
    }
    public void setUnderTakeMember(String underTakeMember) {
        this.underTakeMember = underTakeMember;
    }

    public String getUnderTakeTime() {
        return underTakeTime;
    }
    public void setUnderTakeTime(String underTakeTime) {
        this.underTakeTime = underTakeTime;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getTradePassword() {
        return tradePassword;
    }
    public void setTradePassword(String tradePassword) {
        this.tradePassword = tradePassword;
    }

    @Override
    public String toString() {
        return "ProductMyAttornBusinessBean{" +
                "info='" + info + '\'' +
                ", code='" + code + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", id='" + id + '\'' +
                ", productBuyId='" + productBuyId + '\'' +
                ", price='" + price + '\'' +
                ", expectedRate='" + expectedRate + '\'' +
                ", shFlag='" + shFlag + '\'' +
                ", shyj='" + shyj + '\'' +
                ", underTakeMember='" + underTakeMember + '\'' +
                ", underTakeTime='" + underTakeTime + '\'' +
                ", createTime='" + createTime + '\'' +
                ", tradePassword='" + tradePassword + '\'' +
                '}';
    }
}
