package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-25.
 */
public class HuanXunPostalMessageBean {

    /** 执行状态(0-成功、1-失败) **/
    private String code;
    /** 操作类型 **/
    private String operationType;
    /** 商户存管交易账号 **/
    private String merchantID;
    /** 请求信息 **/
    private String request;
    /** 提现金额 **/
    private String trdAmt;
    /** 主键 **/
    private String id;
    /** 关联id **/
    private String glid;
    /** 会员id **/
    private String memberid;
    /** 来源(0-电脑端、1-手机端) **/
    private String source;
    /** 响应状态(000000-成功、999999-失败) **/
    private String resultcode;
    /** 响应信息描述 **/
    private String resultmsg;
    /** 商户存管交易账号 **/
    private String merchantid;
    /** 签名 **/
    private String sign;
    /** 商户订单号 **/
    private String merbillno;
    /** IPS订单号 **/
    private String ipsbillno;
    /** IPS处理时间 **/
    private String ipsdotime;
    /** 平台手续费 **/
    private String merfee;
    /** IPS手续费 **/
    private String ipsfee;
    /** IPS存管账户号 **/
    private String ipsacctno;
    /** 用户到账金额 **/
    private String ipstrdamt;
    /** 提现状态 **/
    private String trdstatus;
    /** 创建时间 **/
    private String createtime;
    /** 提现总额 **/
    private String cashSum;
    /** 手续费 **/
    private String ipsfeeSum;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 真实姓名 **/
    private String realname;
    /** 身份证号 **/
    private String idcard;
    /** 提现金额 总计 **/
    private String cashTotal;
    /** 用户到账金额 总计 **/
    private String ipstrdamtTotal;
    /** 手续费 总计 **/
    private String ipsfeeTotal;
    /** 拼接字符串 **/
    private String str;
    /** 分页字符串 **/
    private String pageStr;
    /** 是否第一次点击 **/
    private String isFirst;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 总提现金额 **/
    private String sumFee;
    /** 最后到账金额 **/
    private String endFee;
    /** 页面地址 **/
    private String menuUrl;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getOperationType() {
        return operationType;
    }
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getMerchantID() {
        return merchantID;
    }
    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }

    public String getTrdAmt() {
        return trdAmt;
    }
    public void setTrdAmt(String trdAmt) {
        this.trdAmt = trdAmt;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getGlid() {
        return glid;
    }
    public void setGlid(String glid) {
        this.glid = glid;
    }

    public String getMemberid() {
        return memberid;
    }
    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public String getResultcode() {
        return resultcode;
    }
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode;
    }

    public String getResultmsg() {
        return resultmsg;
    }
    public void setResultmsg(String resultmsg) {
        this.resultmsg = resultmsg;
    }

    public String getMerchantid() {
        return merchantid;
    }
    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    public String getSign() {
        return sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getMerbillno() {
        return merbillno;
    }
    public void setMerbillno(String merbillno) {
        this.merbillno = merbillno;
    }

    public String getIpsbillno() {
        return ipsbillno;
    }
    public void setIpsbillno(String ipsbillno) {
        this.ipsbillno = ipsbillno;
    }

    public String getIpsdotime() {
        return ipsdotime;
    }
    public void setIpsdotime(String ipsdotime) {
        this.ipsdotime = ipsdotime;
    }

    public String getMerfee() {
        return merfee;
    }
    public void setMerfee(String merfee) {
        this.merfee = merfee;
    }

    public String getIpsfee() {
        return ipsfee;
    }
    public void setIpsfee(String ipsfee) {
        this.ipsfee = ipsfee;
    }

    public String getIpsacctno() {
        return ipsacctno;
    }
    public void setIpsacctno(String ipsacctno) {
        this.ipsacctno = ipsacctno;
    }

    public String getIpstrdamt() {
        return ipstrdamt;
    }
    public void setIpstrdamt(String ipstrdamt) {
        this.ipstrdamt = ipstrdamt;
    }

    public String getTrdstatus() {
        return trdstatus;
    }
    public void setTrdstatus(String trdstatus) {
        this.trdstatus = trdstatus;
    }

    public String getCreatetime() {
        return createtime;
    }
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getCashSum() {
        return cashSum;
    }
    public void setCashSum(String cashSum) {
        this.cashSum = cashSum;
    }

    public String getIpsfeeSum() {
        return ipsfeeSum;
    }
    public void setIpsfeeSum(String ipsfeeSum) {
        this.ipsfeeSum = ipsfeeSum;
    }

    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getRealname() {
        return realname;
    }
    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getIdcard() {
        return idcard;
    }
    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getCashTotal() {
        return cashTotal;
    }
    public void setCashTotal(String cashTotal) {
        this.cashTotal = cashTotal;
    }

    public String getIpstrdamtTotal() {
        return ipstrdamtTotal;
    }
    public void setIpstrdamtTotal(String ipstrdamtTotal) {
        this.ipstrdamtTotal = ipstrdamtTotal;
    }

    public String getIpsfeeTotal() {
        return ipsfeeTotal;
    }
    public void setIpsfeeTotal(String ipsfeeTotal) {
        this.ipsfeeTotal = ipsfeeTotal;
    }

    public String getStr() {
        return str;
    }
    public void setStr(String str) {
        this.str = str;
    }

    public String getPageStr() {
        return pageStr;
    }
    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    public String getIsFirst() {
        return isFirst;
    }
    public void setIsFirst(String isFirst) {
        this.isFirst = isFirst;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getSumFee() {
        return sumFee;
    }
    public void setSumFee(String sumFee) {
        this.sumFee = sumFee;
    }

    public String getEndFee() {
        return endFee;
    }
    public void setEndFee(String endFee) {
        this.endFee = endFee;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    @Override
    public String toString() {
        return "HuanXunPostalMessageBean{" +
                "code='" + code + '\'' +
                ", operationType='" + operationType + '\'' +
                ", merchantID='" + merchantID + '\'' +
                ", request='" + request + '\'' +
                ", trdAmt='" + trdAmt + '\'' +
                ", id='" + id + '\'' +
                ", glid='" + glid + '\'' +
                ", memberid='" + memberid + '\'' +
                ", source='" + source + '\'' +
                ", resultcode='" + resultcode + '\'' +
                ", resultmsg='" + resultmsg + '\'' +
                ", merchantid='" + merchantid + '\'' +
                ", sign='" + sign + '\'' +
                ", merbillno='" + merbillno + '\'' +
                ", ipsbillno='" + ipsbillno + '\'' +
                ", ipsdotime='" + ipsdotime + '\'' +
                ", merfee='" + merfee + '\'' +
                ", ipsfee='" + ipsfee + '\'' +
                ", ipsacctno='" + ipsacctno + '\'' +
                ", ipstrdamt='" + ipstrdamt + '\'' +
                ", trdstatus='" + trdstatus + '\'' +
                ", createtime='" + createtime + '\'' +
                ", cashSum='" + cashSum + '\'' +
                ", ipsfeeSum='" + ipsfeeSum + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", realname='" + realname + '\'' +
                ", idcard='" + idcard + '\'' +
                ", cashTotal='" + cashTotal + '\'' +
                ", ipstrdamtTotal='" + ipstrdamtTotal + '\'' +
                ", ipsfeeTotal='" + ipsfeeTotal + '\'' +
                ", str='" + str + '\'' +
                ", pageStr='" + pageStr + '\'' +
                ", isFirst='" + isFirst + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", sumFee='" + sumFee + '\'' +
                ", endFee='" + endFee + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                '}';
    }
}
