package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-6-24.
 * 我的投资部分
 */
public class ProductMyInvestMessageBean {

    /** 主键 **/
    private String id;
    /** 题目 **/
    private String title;
    /** 编码 **/
    private String code;
    /** 贷款利率 **/
    private String loanRate;
    /** 待收本金 **/
    private String waitingPricipal;
    /** 待收利息 **/
    private String waitingInterest;
    /** 当前应收利息 **/
    private String currentInterest;
    /** 剩余期数 **/
    private String surplusPeriods;
    /** 到期日期 **/
    private String expireDate;
    /** 状态 **/
    private String receiveFlag;
    /** 最低转让价格 **/
    private String priceMin;
    /** 最高转让价格 **/
    private String priceMax;
    /** 下次还款日期 **/
    private String nextReceiveTime;
    /** 菜单 **/
    private String menuUrl;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getWaitingPricipal() {
        return waitingPricipal;
    }
    public void setWaitingPricipal(String waitingPricipal) {
        this.waitingPricipal = waitingPricipal;
    }

    public String getWaitingInterest() {
        return waitingInterest;
    }
    public void setWaitingInterest(String waitingInterest) {
        this.waitingInterest = waitingInterest;
    }

    public String getCurrentInterest() {
        return currentInterest;
    }
    public void setCurrentInterest(String currentInterest) {
        this.currentInterest = currentInterest;
    }

    public String getSurplusPeriods() {
        return surplusPeriods;
    }
    public void setSurplusPeriods(String surplusPeriods) {
        this.surplusPeriods = surplusPeriods;
    }

    public String getExpireDate() {
        return expireDate;
    }
    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getReceiveFlag() {
        return receiveFlag;
    }
    public void setReceiveFlag(String receiveFlag) {
        this.receiveFlag = receiveFlag;
    }

    public String getPriceMin() {
        return priceMin;
    }
    public void setPriceMin(String priceMin) {
        this.priceMin = priceMin;
    }

    public String getPriceMax() {
        return priceMax;
    }
    public void setPriceMax(String priceMax) {
        this.priceMax = priceMax;
    }

    public String getNextReceiveTime() {
        return nextReceiveTime;
    }
    public void setNextReceiveTime(String nextReceiveTime) {
        this.nextReceiveTime = nextReceiveTime;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    @Override
    public String toString() {
        return "ProductMyInvestMessageBean{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", waitingPricipal='" + waitingPricipal + '\'' +
                ", waitingInterest='" + waitingInterest + '\'' +
                ", currentInterest='" + currentInterest + '\'' +
                ", surplusPeriods='" + surplusPeriods + '\'' +
                ", expireDate='" + expireDate + '\'' +
                ", receiveFlag='" + receiveFlag + '\'' +
                ", priceMin='" + priceMin + '\'' +
                ", priceMax='" + priceMax + '\'' +
                ", nextReceiveTime='" + nextReceiveTime + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                '}';
    }
}
