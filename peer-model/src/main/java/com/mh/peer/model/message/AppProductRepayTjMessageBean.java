package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-13.
 * 还款统计
 */
public class AppProductRepayTjMessageBean {

    /** 待还金额 **/
    private String dhPrice;
    /** 待还投资笔数 **/
    private String dhRepayCount;
    /** 待还期数 **/
    private String dhPeriod;
    /** 已还金额 **/
    private String yhPrice;
    /** 已还投资笔数 **/
    private String yhRepayCount;
    /** 已还期数 **/
    private String yhPeriod;

    public String getDhPrice() {
        return dhPrice;
    }
    public void setDhPrice(String dhPrice) {
        this.dhPrice = dhPrice;
    }

    public String getDhRepayCount() {
        return dhRepayCount;
    }
    public void setDhRepayCount(String dhRepayCount) {
        this.dhRepayCount = dhRepayCount;
    }

    public String getDhPeriod() {
        return dhPeriod;
    }
    public void setDhPeriod(String dhPeriod) {
        this.dhPeriod = dhPeriod;
    }

    public String getYhPrice() {
        return yhPrice;
    }
    public void setYhPrice(String yhPrice) {
        this.yhPrice = yhPrice;
    }

    public String getYhRepayCount() {
        return yhRepayCount;
    }
    public void setYhRepayCount(String yhRepayCount) {
        this.yhRepayCount = yhRepayCount;
    }

    public String getYhPeriod() {
        return yhPeriod;
    }
    public void setYhPeriod(String yhPeriod) {
        this.yhPeriod = yhPeriod;
    }

    @Override
    public String toString() {
        return "AppProductRepayMessageBean{" +
                "dhPrice='" + dhPrice + '\'' +
                ", dhRepayCount='" + dhRepayCount + '\'' +
                ", dhPeriod='" + dhPeriod + '\'' +
                ", yhPrice='" + yhPrice + '\'' +
                ", yhRepayCount='" + yhRepayCount + '\'' +
                ", yhPeriod='" + yhPeriod + '\'' +
                '}';
    }
}
