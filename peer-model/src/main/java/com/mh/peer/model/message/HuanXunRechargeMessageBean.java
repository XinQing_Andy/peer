package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-24.
 */
public class HuanXunRechargeMessageBean {

    /** 程序执行状态(0-成功、1-失败) **/
    private String code;
    /** 程序执行信息 **/
    private String info;
    /** 操作类型 **/
    private String operationType;
    /** 商户存管交易账号 **/
    private String merchantID;
    /** 商户订单号 **/
    private String merBillNo;
    /** 充值日期 **/
    private String merDate;
    /** 充值类型(1-普通充值、2-还款充值) **/
    private String depositType;
    /** 充值渠道(1-个人网银、2-企业网银) **/
    private String channelType;
    /** 充值银行号 **/
    private String bankCode;
    /** 用户类型(1-个人、2-企业) **/
    private String userType;
    /** IPS存管账户号 **/
    private String ipsAcctNo;
    /** 充值金额 **/
    private String trdAmt;
    /** IPS手续费承担方 **/
    private String ipsFeeType;
    /** 平台手续费 **/
    private String merFee;
    /** 平台手续费收取方式 **/
    private String merFeeType;
    /** 发起方 **/
    private String taker;
    /** 页面返回地址 **/
    private String webUrl;
    /** 后台通知地址 **/
    private String s2SUrl;
    /** IPS订单号 **/
    private String ipsBillNo;
    /** IPS处理时间 **/
    private String ipsDoTime;
    /** IPS充值金额 **/
    private String ipsTrdmt;
    /** IPS手续费金额 **/
    private String ipsFee;
    /** 充值状态(0-失败、1-成功、2-处理中) **/
    private String trdStatus;
    /** 签名 **/
    private String sign;
    /** 请求信息 **/
    private String request;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getOperationType() {
        return operationType;
    }
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getMerchantID() {
        return merchantID;
    }
    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerBillNo() {
        return merBillNo;
    }
    public void setMerBillNo(String merBillNo) {
        this.merBillNo = merBillNo;
    }

    public String getMerDate() {
        return merDate;
    }
    public void setMerDate(String merDate) {
        this.merDate = merDate;
    }

    public String getDepositType() {
        return depositType;
    }
    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public String getChannelType() {
        return channelType;
    }
    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getBankCode() {
        return bankCode;
    }
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getUserType() {
        return userType;
    }
    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getIpsAcctNo() {
        return ipsAcctNo;
    }
    public void setIpsAcctNo(String ipsAcctNo) {
        this.ipsAcctNo = ipsAcctNo;
    }

    public String getTrdAmt() {
        return trdAmt;
    }
    public void setTrdAmt(String trdAmt) {
        this.trdAmt = trdAmt;
    }

    public String getIpsFeeType() {
        return ipsFeeType;
    }
    public void setIpsFeeType(String ipsFeeType) {
        this.ipsFeeType = ipsFeeType;
    }

    public String getMerFee() {
        return merFee;
    }
    public void setMerFee(String merFee) {
        this.merFee = merFee;
    }

    public String getMerFeeType() {
        return merFeeType;
    }
    public void setMerFeeType(String merFeeType) {
        this.merFeeType = merFeeType;
    }

    public String getTaker() {
        return taker;
    }
    public void setTaker(String taker) {
        this.taker = taker;
    }

    public String getWebUrl() {
        return webUrl;
    }
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getS2SUrl() {
        return s2SUrl;
    }
    public void setS2SUrl(String s2SUrl) {
        this.s2SUrl = s2SUrl;
    }

    public String getIpsBillNo() {
        return ipsBillNo;
    }
    public void setIpsBillNo(String ipsBillNo) {
        this.ipsBillNo = ipsBillNo;
    }

    public String getIpsDoTime() {
        return ipsDoTime;
    }
    public void setIpsDoTime(String ipsDoTime) {
        this.ipsDoTime = ipsDoTime;
    }

    public String getIpsTrdmt() {
        return ipsTrdmt;
    }
    public void setIpsTrdmt(String ipsTrdmt) {
        this.ipsTrdmt = ipsTrdmt;
    }

    public String getIpsFee() {
        return ipsFee;
    }
    public void setIpsFee(String ipsFee) {
        this.ipsFee = ipsFee;
    }

    public String getTrdStatus() {
        return trdStatus;
    }
    public void setTrdStatus(String trdStatus) {
        this.trdStatus = trdStatus;
    }

    public String getSign() {
        return sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "HuanXunRechargeMessageBean{" +
                "code='" + code + '\'' +
                ", info='" + info + '\'' +
                ", operationType='" + operationType + '\'' +
                ", merchantID='" + merchantID + '\'' +
                ", merBillNo='" + merBillNo + '\'' +
                ", merDate='" + merDate + '\'' +
                ", depositType='" + depositType + '\'' +
                ", channelType='" + channelType + '\'' +
                ", bankCode='" + bankCode + '\'' +
                ", userType='" + userType + '\'' +
                ", ipsAcctNo='" + ipsAcctNo + '\'' +
                ", trdAmt='" + trdAmt + '\'' +
                ", ipsFeeType='" + ipsFeeType + '\'' +
                ", merFee='" + merFee + '\'' +
                ", merFeeType='" + merFeeType + '\'' +
                ", taker='" + taker + '\'' +
                ", webUrl='" + webUrl + '\'' +
                ", s2SUrl='" + s2SUrl + '\'' +
                ", ipsBillNo='" + ipsBillNo + '\'' +
                ", ipsDoTime='" + ipsDoTime + '\'' +
                ", ipsTrdmt='" + ipsTrdmt + '\'' +
                ", ipsFee='" + ipsFee + '\'' +
                ", trdStatus='" + trdStatus + '\'' +
                ", sign='" + sign + '\'' +
                ", request='" + request + '\'' +
                '}';
    }
}
