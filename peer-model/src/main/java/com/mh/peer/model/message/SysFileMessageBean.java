package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/4/11.
 */
public class SysFileMessageBean {

    /** 结果 **/
    private String result;
    /** 信息 **/
    private String info;
    /** 主键 **/
    private String id;
    /** 关联id **/
    private String glid;
    /** 图片地址 **/
    private String imgurl;
    /** 绝对路径 **/
    private String imgurljd;
    /** 图片类型(0-新闻、1-产品) **/
    private String type;
    /** 排序 **/
    private String sort;
    /** 创建用户 **/
    private String createuser;
    /** 创建时间 **/
    private String createtime;
    /** 图片名称 **/
    private String imgName;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 当前数 **/
    private String pageIndex;
    /** 每次显示数量 **/
    private String pageSize;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGlid() {
        return glid;
    }

    public void setGlid(String glid) {
        this.glid = glid;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getImgurljd() {
        return imgurljd;
    }

    public void setImgurljd(String imgurljd) {
        this.imgurljd = imgurljd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSort() {
        return sort;
    }
    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "SysFileMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", glid='" + glid + '\'' +
                ", imgurl='" + imgurl + '\'' +
                ", imgurljd='" + imgurljd + '\'' +
                ", type='" + type + '\'' +
                ", sort='" + sort + '\'' +
                ", createuser='" + createuser + '\'' +
                ", createtime='" + createtime + '\'' +
                ", imgName='" + imgName + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                '}';
    }
}
