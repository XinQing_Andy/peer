package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/6/17.
 */
public class TradeDetailMessageBean {

    /** 主键 **/
    private String id;
    /** 关联id **/
    private String glid;
    /** 流水号 **/
    private String serialnum;
    /** 交易类型(0-充值、1-提现) **/
    private String type;
    /** 实际金额 **/
    private String sprice;
    /** 手续费 **/
    private String procedurefee;
    /** 状态(0-成功、1-失败) **/
    private String flag;
    /** 会员id **/
    private String memberid;
    /** 来源(0-电脑端、1-手机端) **/
    private String source;
    /** 交易时间 **/
    private String time;
    /** 备注 **/
    private String bz;
    /** 创建人 **/
    private String createuser;
    /** 创建时间 **/
    private String createtime;
    /** 余额 **/
    private String balance;
    /** 累计金额收益 **/
    private String earningsTotal;
    /** 冻结金额 **/
    private String frozenSum;
    /** 可用余额 **/
    private String availableBalance;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 当前数量 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 是否第一次点击 **/
    private String isFirst;
    /** 模版名 **/
    private String jetxName;
    /**  **/
    private String result;
    /**  **/
    private String info;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 页面地址 **/
    private String menuUrl;
    /** 列表部分拼接 **/
    private String str;
    /** 分页部分拼接 **/
    private String pageStr;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getGlid() {
        return glid;
    }
    public void setGlid(String glid) {
        this.glid = glid;
    }

    public String getSerialnum() {
        return serialnum;
    }
    public void setSerialnum(String serialnum) {
        this.serialnum = serialnum;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getSprice() {
        return sprice;
    }
    public void setSprice(String sprice) {
        this.sprice = sprice;
    }

    public String getProcedurefee() {
        return procedurefee;
    }
    public void setProcedurefee(String procedurefee) {
        this.procedurefee = procedurefee;
    }

    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMemberid() {
        return memberid;
    }
    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    public String getBz() {
        return bz;
    }
    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getCreateuser() {
        return createuser;
    }
    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCreatetime() {
        return createtime;
    }
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getEarningsTotal() {
        return earningsTotal;
    }
    public void setEarningsTotal(String earningsTotal) {
        this.earningsTotal = earningsTotal;
    }

    public String getFrozenSum() {
        return frozenSum;
    }
    public void setFrozenSum(String frozenSum) {
        this.frozenSum = frozenSum;
    }

    public String getAvailableBalance() {
        return availableBalance;
    }
    public void setAvailableBalance(String availableBalance) {
        this.availableBalance = availableBalance;
    }

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getIsFirst() {
        return isFirst;
    }
    public void setIsFirst(String isFirst) {
        this.isFirst = isFirst;
    }

    public String getJetxName() {
        return jetxName;
    }
    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getStr() {
        return str;
    }
    public void setStr(String str) {
        this.str = str;
    }

    public String getPageStr() {
        return pageStr;
    }
    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    @Override
    public String toString() {
        return "TradeDetailMessageBean{" +
                "id='" + id + '\'' +
                ", glid='" + glid + '\'' +
                ", serialnum='" + serialnum + '\'' +
                ", type='" + type + '\'' +
                ", sprice='" + sprice + '\'' +
                ", procedurefee='" + procedurefee + '\'' +
                ", flag='" + flag + '\'' +
                ", memberid='" + memberid + '\'' +
                ", source='" + source + '\'' +
                ", time='" + time + '\'' +
                ", bz='" + bz + '\'' +
                ", createuser='" + createuser + '\'' +
                ", createtime='" + createtime + '\'' +
                ", balance='" + balance + '\'' +
                ", earningsTotal='" + earningsTotal + '\'' +
                ", frozenSum='" + frozenSum + '\'' +
                ", availableBalance='" + availableBalance + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", isFirst='" + isFirst + '\'' +
                ", jetxName='" + jetxName + '\'' +
                ", result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", str='" + str + '\'' +
                ", pageStr='" + pageStr + '\'' +
                '}';
    }
}
