package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-8-31.
 */
public class ConTractMessageBean {

    /** 产品id **/
    private String productId;
    /** 产品投资id **/
    public String productBuyId;
    /** 借款人姓名 **/
    private String applyMemberName;
    /** 借款人用户名 **/
    private String applyMemberUserName;
    /** 借款金额 **/
    private String loanPrice;
    /** 借款期限 **/
    private String loanLength;
    /** 满标日期(起息日) **/
    private String fullTime;
    /** 还款方式 **/
    private String repayment;
    /** 借款利率 **/
    private String loanRate;
    /** 借款用途 **/
    private String loanPurpose;
    /** 合同签订日期 **/
    private String conTractTime;
    /** 投资人用户名 **/
    private String buyerUserName;
    /** 投资人姓名 **/
    private String buyerXm;
    /** 投资人身份证号 **/
    private String buyerIDcard;
    /** 投资金额 **/
    private String buyPrice;
    /** 承接人用户名 **/
    private String attornerUserName;
    /** 承接人姓名 **/
    private String attornerXm;
    /** 承接人身份证号 **/
    private String attornerIDcard;
    /** 承接价格 **/
    private String attornPrice;
    /** 承接时间 **/
    private String attornTime;
    /** 债权本金 **/
    private String attornPrincipal;

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductBuyId() {
        return productBuyId;
    }
    public void setProductBuyId(String productBuyId) {
        this.productBuyId = productBuyId;
    }

    public String getApplyMemberName() {
        return applyMemberName;
    }
    public void setApplyMemberName(String applyMemberName) {
        this.applyMemberName = applyMemberName;
    }

    public String getApplyMemberUserName() {
        return applyMemberUserName;
    }
    public void setApplyMemberUserName(String applyMemberUserName) {
        this.applyMemberUserName = applyMemberUserName;
    }

    public String getLoanPrice() {
        return loanPrice;
    }
    public void setLoanPrice(String loanPrice) {
        this.loanPrice = loanPrice;
    }

    public String getLoanLength() {
        return loanLength;
    }
    public void setLoanLength(String loanLength) {
        this.loanLength = loanLength;
    }

    public String getFullTime() {
        return fullTime;
    }
    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getLoanPurpose() {
        return loanPurpose;
    }
    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    public String getConTractTime() {
        return conTractTime;
    }
    public void setConTractTime(String conTractTime) {
        this.conTractTime = conTractTime;
    }

    public String getBuyerUserName() {
        return buyerUserName;
    }
    public void setBuyerUserName(String buyerUserName) {
        this.buyerUserName = buyerUserName;
    }

    public String getBuyerXm() {
        return buyerXm;
    }
    public void setBuyerXm(String buyerXm) {
        this.buyerXm = buyerXm;
    }

    public String getBuyerIDcard() {
        return buyerIDcard;
    }
    public void setBuyerIDcard(String buyerIDcard) {
        this.buyerIDcard = buyerIDcard;
    }

    public String getBuyPrice() {
        return buyPrice;
    }
    public void setBuyPrice(String buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getAttornerUserName() {
        return attornerUserName;
    }
    public void setAttornerUserName(String attornerUserName) {
        this.attornerUserName = attornerUserName;
    }

    public String getAttornerXm() {
        return attornerXm;
    }
    public void setAttornerXm(String attornerXm) {
        this.attornerXm = attornerXm;
    }

    public String getAttornerIDcard() {
        return attornerIDcard;
    }
    public void setAttornerIDcard(String attornerIDcard) {
        this.attornerIDcard = attornerIDcard;
    }

    public String getAttornPrice() {
        return attornPrice;
    }
    public void setAttornPrice(String attornPrice) {
        this.attornPrice = attornPrice;
    }

    public String getAttornTime() {
        return attornTime;
    }
    public void setAttornTime(String attornTime) {
        this.attornTime = attornTime;
    }

    public String getAttornPrincipal() {
        return attornPrincipal;
    }
    public void setAttornPrincipal(String attornPrincipal) {
        this.attornPrincipal = attornPrincipal;
    }

    @Override
    public String toString() {
        return "ConTractMessageBean{" +
                "productId='" + productId + '\'' +
                ", productBuyId='" + productBuyId + '\'' +
                ", applyMemberName='" + applyMemberName + '\'' +
                ", applyMemberUserName='" + applyMemberUserName + '\'' +
                ", loanPrice='" + loanPrice + '\'' +
                ", loanLength='" + loanLength + '\'' +
                ", fullTime='" + fullTime + '\'' +
                ", repayment='" + repayment + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", loanPurpose='" + loanPurpose + '\'' +
                ", conTractTime='" + conTractTime + '\'' +
                ", buyerUserName='" + buyerUserName + '\'' +
                ", buyerXm='" + buyerXm + '\'' +
                ", buyerIDcard='" + buyerIDcard + '\'' +
                ", buyPrice='" + buyPrice + '\'' +
                ", attornerUserName='" + attornerUserName + '\'' +
                ", attornerXm='" + attornerXm + '\'' +
                ", attornerIDcard='" + attornerIDcard + '\'' +
                ", attornPrice='" + attornPrice + '\'' +
                ", attornTime='" + attornTime + '\'' +
                ", attornPrincipal='" + attornPrincipal + '\'' +
                '}';
    }
}
