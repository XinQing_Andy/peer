package com.mh.peer.model.message;

/**
 * Created by buyatao on 2016-8-30.
 */
public class ProductOverRepayMessageBean {

    /**
     * 主键
     **/
    private String id;
    /**
     * 借款人
     **/
    private String loanman;
    /**
     * 项目编号
     **/
    private String productId;
    /**
     * 项目名称
     **/
    private String productTitle;
    /**
     * 应还款日期
     **/
    private String yrepaymentTime;
    /**
     * 逾期时长
     **/
    private String overDay;
    /**
     * 应还款金额
     **/
    private String yremoney;
    /**
     * 借款金额
     **/
    private String loanPrice;
    /**
     * 身份证号
     **/
    private String idcardNumber;
    /**
     * 开始时间
     **/
    private String startTime;
    /**
     * 结束时间
     **/
    private String endTime;
    /**
     * 总数量
     **/
    private String totalCount;
    /**
     * 当前页
     **/
    private String currentPage;
    /**
     * 当前页
     **/
    private String pageIndex;
    /**
     * 每页显示数量
     **/
    private String pageSize;
    /**
     * 分页部分拼接
     **/
    private String pageStr;
    /**
     * 拼接字符串
     **/
    private String str;
    /**
     * 菜单地址
     **/
    private String menuUrl;
    /**
     * 返回标志
     **/
    private String result;

    public String getTotalCount() {
        return totalCount;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageStr() {
        return pageStr;
    }

    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoanman() {
        return loanman;
    }

    public void setLoanman(String loanman) {
        this.loanman = loanman;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getYrepaymentTime() {
        return yrepaymentTime;
    }

    public void setYrepaymentTime(String yrepaymentTime) {
        this.yrepaymentTime = yrepaymentTime;
    }

    public String getOverDay() {
        return overDay;
    }

    public void setOverDay(String overDay) {
        this.overDay = overDay;
    }

    public String getYremoney() {
        return yremoney;
    }

    public void setYremoney(String yremoney) {
        this.yremoney = yremoney;
    }

    public String getLoanPrice() {
        return loanPrice;
    }

    public void setLoanPrice(String loanPrice) {
        this.loanPrice = loanPrice;
    }

    public String getIdcardNumber() {
        return idcardNumber;
    }

    public void setIdcardNumber(String idcardNumber) {
        this.idcardNumber = idcardNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "ProductOverRepayMessageBean{" +
                "id='" + id + '\'' +
                ", loanman='" + loanman + '\'' +
                ", productId='" + productId + '\'' +
                ", productTitle='" + productTitle + '\'' +
                ", yrepaymentTime='" + yrepaymentTime + '\'' +
                ", overDay='" + overDay + '\'' +
                ", yremoney='" + yremoney + '\'' +
                ", loanPrice='" + loanPrice + '\'' +
                ", idcardNumber='" + idcardNumber + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", pageStr='" + pageStr + '\'' +
                ", str='" + str + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
