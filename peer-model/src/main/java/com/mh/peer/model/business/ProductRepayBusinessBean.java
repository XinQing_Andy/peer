package com.mh.peer.model.business;

/**
 * Created by zhangerxin on 2016-5-19.
 */
public class ProductRepayBusinessBean {

    /** 返回信息 **/
    private String info;
    /** 返回标志 **/
    private String result;
    /** 返回页面 **/
    private String menuUrl;
    /** 主键 **/
    private String id;
    /** 产品id **/
    private String productId;
    /** 本金 **/
    private String principal;
    /** 利息 **/
    private String interest;
    /** 应还款日期 **/
    private String yrepaymentTime;
    /** 实际还款金额 **/
    private String repaymentPrice;
    /** 实际还款日期 **/
    private String srepaymentTime;
    /** 创建日期 **/
    private String createTime;
    /** 最后更新日期 **/
    private String updateTime;

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPrincipal() {
        return principal;
    }
    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getInterest() {
        return interest;
    }
    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getYrepaymentTime() {
        return yrepaymentTime;
    }
    public void setYrepaymentTime(String yrepaymentTime) {
        this.yrepaymentTime = yrepaymentTime;
    }

    public String getRepaymentPrice() {
        return repaymentPrice;
    }
    public void setRepaymentPrice(String repaymentPrice) {
        this.repaymentPrice = repaymentPrice;
    }

    public String getSrepaymentTime() {
        return srepaymentTime;
    }
    public void setSrepaymentTime(String srepaymentTime) {
        this.srepaymentTime = srepaymentTime;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ProductRepayBusinessBean{" +
                "info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", id='" + id + '\'' +
                ", productId='" + productId + '\'' +
                ", principal='" + principal + '\'' +
                ", interest='" + interest + '\'' +
                ", yrepaymentTime='" + yrepaymentTime + '\'' +
                ", repaymentPrice='" + repaymentPrice + '\'' +
                ", srepaymentTime='" + srepaymentTime + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
