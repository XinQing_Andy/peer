package com.mh.peer.model.message;

/**
 * Created by WangMeng on 2016/1/13.
 */
public class MenuBeanMessage {
    /**
     * 返回信息(success error)
     */
    private String result;
    /**
     * 详细消息
     */
    private String info;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 权限ID
     */
    private String roleId;
    /**
     * 菜单ID
     */
    private String menuId;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 图标样式
     */
    private String iconStyle;
    /**
     * 菜单级别
     */
    private String classLeven;
    /**
     * 子菜单个数
     */
    private String childCount;
    /**
     * 模板名称
     */
    private String menuUrl;
    /**
     * 控制类URL
     */
    private String controlUrl;

    /**
     * 父级菜单ID
     */
    private String parentId;

    private String nowPage;

    private String pageCount;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(String iconStyle) {
        this.iconStyle = iconStyle;
    }

    public String getClassLeven() {
        return classLeven;
    }

    public void setClassLeven(String classLeven) {
        this.classLeven = classLeven;
    }

    public String getChildCount() {
        return childCount;
    }

    public void setChildCount(String childCount) {
        this.childCount = childCount;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getControlUrl() {
        return controlUrl;
    }

    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getNowPage() {
        return nowPage;
    }

    public void setNowPage(String nowPage) {
        this.nowPage = nowPage;
    }

    public String getPageCount() {
        return pageCount;
    }

    public void setPageCount(String pageCount) {
        this.pageCount = pageCount;
    }

    @Override
    public String toString() {
        return "MenuBeanMessage{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", userId='" + userId + '\'' +
                ", roleId='" + roleId + '\'' +
                ", menuId='" + menuId + '\'' +
                ", menuName='" + menuName + '\'' +
                ", iconStyle='" + iconStyle + '\'' +
                ", classLeven='" + classLeven + '\'' +
                ", childCount='" + childCount + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", controlUrl='" + controlUrl + '\'' +
                ", parentId='" + parentId + '\'' +
                ", nowPage='" + nowPage + '\'' +
                ", pageCount='" + pageCount + '\'' +
                '}';
    }
}
