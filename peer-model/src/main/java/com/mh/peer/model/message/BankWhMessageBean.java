package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/5/16.
 */
public class BankWhMessageBean {
    /** 主键 **/
    private String id;
    /** 银行名称 **/
    private String bankname;
    /** 备注 **/
    private String bz;
    /** 操作时间 **/
    private Object operatime;

    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /** 菜单 **/
    private String menuUrl;
    /**  **/
    private String jetxName;
    /** 控制类URL **/
    private String controlUrl;
    /** 关键字 **/
    private String keyword;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public Object getOperatime() {
        return operatime;
    }

    public void setOperatime(Object operatime) {
        this.operatime = operatime;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getControlUrl() {
        return controlUrl;
    }

    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public String toString() {
        return "BankWhMessageBean{" +
                "id='" + id + '\'' +
                ", bankname='" + bankname + '\'' +
                ", bz='" + bz + '\'' +
                ", operatime=" + operatime +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", jetxName='" + jetxName + '\'' +
                ", controlUrl='" + controlUrl + '\'' +
                ", keyword='" + keyword + '\'' +
                '}';
    }
}
