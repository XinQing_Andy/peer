package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/5/30.
 */
public class MessageSetMessageBean {

    /** 主键 **/
    private String id;
    /** 用户名 **/
    private String username;
    /** 密码 **/
    private String password;
    /** 是否启用（0-是、1-否） **/
    private String flag;
    /**  **/
    private String result;
    /**  **/
    private String info;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "MessageSetMessageBean{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", flag='" + flag + '\'' +
                ", result='" + result + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
