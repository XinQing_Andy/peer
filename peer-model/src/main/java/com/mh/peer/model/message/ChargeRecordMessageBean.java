package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/6/17.
 */
public class ChargeRecordMessageBean {
    /** 主键 **/
    private String id;
    /** 商户订单号 **/
    private String merbillno;
    /** 实际充值金额 **/
    private String sprice;
    /** 手续费 **/
    private String procedurefee;
    /** 充值状态(0-充值成功、1-充值失败) **/
    private String flag;
    /** 会员id **/
    private String memberid;
    /** 来源(0-电脑端、1-手机端) **/
    private String source;
    /** 充值时间 **/
    private String time;
    /** 创建人 **/
    private String createuser;
    /** 创建时间 **/
    private String createtime;
    /** 充值类型 **/
    private String chargeType;
    /** 执行结果 **/
    private String result;
    /** 返回信息 **/
    private String info;
    /** 页面跳转地址 **/
    private String menuUrl;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getMerbillno() {
        return merbillno;
    }
    public void setMerbillno(String merbillno) {
        this.merbillno = merbillno;
    }

    public String getSprice() {
        return sprice;
    }
    public void setSprice(String sprice) {
        this.sprice = sprice;
    }

    public String getProcedurefee() {
        return procedurefee;
    }
    public void setProcedurefee(String procedurefee) {
        this.procedurefee = procedurefee;
    }

    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMemberid() {
        return memberid;
    }
    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    public String getCreateuser() {
        return createuser;
    }
    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCreatetime() {
        return createtime;
    }
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getChargeType() {
        return chargeType;
    }
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }


    @Override
    public String toString() {
        return "ChargeRecordMessageBean{" +
                "id='" + id + '\'' +
                ", merbillno='" + merbillno + '\'' +
                ", sprice='" + sprice + '\'' +
                ", procedurefee='" + procedurefee + '\'' +
                ", flag='" + flag + '\'' +
                ", memberid='" + memberid + '\'' +
                ", source='" + source + '\'' +
                ", time='" + time + '\'' +
                ", createuser='" + createuser + '\'' +
                ", createtime='" + createtime + '\'' +
                ", chargeType='" + chargeType + '\'' +
                ", result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                '}';
    }
}
