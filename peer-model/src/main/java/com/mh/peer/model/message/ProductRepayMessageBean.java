package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-5-19.
 */
public class ProductRepayMessageBean {

    /** 主键 **/
    private String id;
    /** 产品id **/
    private String productId;
    /** 本金 **/
    private String principal;
    /** 利息 **/
    private String interest;
    /** 应还款本息 **/
    private String yprice;
    /** 应还款日期 **/
    private String yrepaymentTime;
    /** 实际还款金额 **/
    private String repaymentPrice;
    /** 实际还款日期 **/
    private String srepaymentTime;
    /** 需还款总额 **/
    private String priceSum;
    /** 创建日期 **/
    private String createTime;
    /** 最后更新日期 **/
    private String updateTime;
    /** 申请人id **/
    private String applyMember;
    /** 真实姓名 **/
    private String realName;
    /** 当前页 **/
    private String currentPage;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 菜单地址 **/
    private String menuUrl;
    /** 内容 **/
    private String htmlText;
    /** 分页部分拼接 **/
    private String pageStr;
    /** 项目名称 **/
    private String productTitle;
    /** 编号 **/
    private String productCode;
    /** 开始时间 **/
    private String startTime;
    /** 截止时间 **/
    private String endTime;
    private String keyword;
    private String keyWordDate;
    private String value;
    /** 还款状态 **/
    private String repayflg;
    /** 还款方式 **/
    private String repayment;
    /** 借款金额 **/
    private String loadfee;
    /** 已还期数 **/
    private String repayperiods;
    /** 年利率 **/
    private String loanrate;
    /** 下期还款日 **/
    private String nextreceivetime;
    /** 总数量 **/
    private String totalCount;
    /**
     * 拼接字符串
     **/
    private String str;
    /**
     * 返回标志
     **/
    private String result;

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getStr() {
        return str;
    }
    public void setStr(String str) {
        this.str = str;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getLoadfee() {
        return loadfee;
    }
    public void setLoadfee(String loadfee) {
        this.loadfee = loadfee;
    }

    public String getRepayperiods() {
        return repayperiods;
    }
    public void setRepayperiods(String repayperiods) {
        this.repayperiods = repayperiods;
    }

    public String getLoanrate() {
        return loanrate;
    }
    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getNextreceivetime() {
        return nextreceivetime;
    }
    public void setNextreceivetime(String nextreceivetime) {
        this.nextreceivetime = nextreceivetime;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getRepayflg() {
        return repayflg;
    }
    public void setRepayflg(String repayflg) {
        this.repayflg = repayflg;
    }

    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyWordDate() {
        return keyWordDate;
    }
    public void setKeyWordDate(String keyWordDate) {
        this.keyWordDate = keyWordDate;
    }

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public String getProductTitle() {
        return productTitle;
    }
    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductCode() {
        return productCode;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPrincipal() {
        return principal;
    }
    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getInterest() {
        return interest;
    }
    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getYprice() {
        return yprice;
    }
    public void setYprice(String yprice) {
        this.yprice = yprice;
    }

    public String getYrepaymentTime() {
        return yrepaymentTime;
    }
    public void setYrepaymentTime(String yrepaymentTime) {
        this.yrepaymentTime = yrepaymentTime;
    }

    public String getRepaymentPrice() {
        return repaymentPrice;
    }
    public void setRepaymentPrice(String repaymentPrice) {
        this.repaymentPrice = repaymentPrice;
    }

    public String getSrepaymentTime() {
        return srepaymentTime;
    }
    public void setSrepaymentTime(String srepaymentTime) {
        this.srepaymentTime = srepaymentTime;
    }

    public String getPriceSum() {
        return priceSum;
    }
    public void setPriceSum(String priceSum) {
        this.priceSum = priceSum;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getApplyMember() {
        return applyMember;
    }
    public void setApplyMember(String applyMember) {
        this.applyMember = applyMember;
    }

    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getHtmlText() {
        return htmlText;
    }
    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public String getPageStr() {
        return pageStr;
    }
    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }


    @Override
    public String toString() {
        return "ProductRepayMessageBean{" +
                "id='" + id + '\'' +
                ", productId='" + productId + '\'' +
                ", principal='" + principal + '\'' +
                ", interest='" + interest + '\'' +
                ", yprice='" + yprice + '\'' +
                ", yrepaymentTime='" + yrepaymentTime + '\'' +
                ", repaymentPrice='" + repaymentPrice + '\'' +
                ", srepaymentTime='" + srepaymentTime + '\'' +
                ", priceSum='" + priceSum + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", applyMember='" + applyMember + '\'' +
                ", realName='" + realName + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", htmlText='" + htmlText + '\'' +
                ", pageStr='" + pageStr + '\'' +
                ", productTitle='" + productTitle + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", keyword='" + keyword + '\'' +
                ", keyWordDate='" + keyWordDate + '\'' +
                ", value='" + value + '\'' +
                ", repayflg='" + repayflg + '\'' +
                ", repayment='" + repayment + '\'' +
                ", loadfee='" + loadfee + '\'' +
                ", repayperiods='" + repayperiods + '\'' +
                ", loanrate='" + loanrate + '\'' +
                ", nextreceivetime='" + nextreceivetime + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", str='" + str + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
