package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-8.
 */
public class AppAdvertMessageBean {

    /** 主键 **/
    private String id;
    /** 类型id **/
    private String typeId;
    /** 题目 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 状态(0-有效、1-无效) **/
    private String flag;
    /** 创建时间 **/
    private String createTime;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "AppAdvertMessageBean{" +
                "id='" + id + '\'' +
                ", typeId='" + typeId + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", flag='" + flag + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
