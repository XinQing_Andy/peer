package com.mh.peer.model.message;

import java.util.List;

/**
 * Created by Cuibin on 2016/4/8.
 */
public class NewsContentMessageBean {
    /** 主键 **/
    private String id;
    /** 内容 **/
    private String content;
    /** 作者 **/
    private String createUser;
    /** 创建日期 **/
    private String createDate;
    /** 类型 **/
    private String type;
    /** 有效标识 0：无效  1：有效 **/
    private String availability;
    /** 是否优先 0：取消优先 1：优先 **/
    private String first;
    /** 优先时间 **/
    private String firstDate;
    /** 浏览次数 **/
    private String lookCount;
    /** 关联id(附件存储的是首页显示图片) **/
    private String rid;
    /** 类别id **/
    private String kindId;
    /** 是否推荐 1：推荐 0：不推荐 **/
    private String isRecommend;
    /** 显示时间 **/
    private String showDate;
    /** 角色名称 **/
    private String roleName;
    /** 控制类URL **/
    private String controlUrl;
    /** 按某条件排序 **/
    private String sort;
    /** 搜索关键字 **/
    private String keyword;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 菜单 **/
    private String menuUrl;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /**  **/
    private String jetxName;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /**  **/
    private String title;
    /** 类别名称 **/
    private String bName;
    /**  **/
    private List list;
    /**  **/
    private String count;
    /** 是否降序排列 1是 0否 **/
    private String isDesc;
    /** 是否从主页点击 **/
    private String isHome;
    /** 一级类别 **/
    private String firstType;
    /** 排序类型 **/
    private String orderType;
    /** 排序字段 **/
    private String orderColumn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(String firstDate) {
        this.firstDate = firstDate;
    }

    public String getLookCount() {
        return lookCount;
    }

    public void setLookCount(String lookCount) {
        this.lookCount = lookCount;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getKindId() {
        return kindId;
    }

    public void setKindId(String kindId) {
        this.kindId = kindId;
    }

    public String getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(String isRecommend) {
        this.isRecommend = isRecommend;
    }

    public String getShowDate() {
        return showDate;
    }

    public void setShowDate(String showDate) {
        this.showDate = showDate;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getControlUrl() {
        return controlUrl;
    }

    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getIsDesc() {
        return isDesc;
    }

    public void setIsDesc(String isDesc) {
        this.isDesc = isDesc;
    }

    public String getIsHome() {
        return isHome;
    }

    public void setIsHome(String isHome) {
        this.isHome = isHome;
    }

    public String getFirstType() {
        return firstType;
    }

    public void setFirstType(String firstType) {
        this.firstType = firstType;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    @Override
    public String toString() {
        return "NewsContentMessageBean{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                ", createUser='" + createUser + '\'' +
                ", createDate='" + createDate + '\'' +
                ", type='" + type + '\'' +
                ", availability='" + availability + '\'' +
                ", first='" + first + '\'' +
                ", firstDate='" + firstDate + '\'' +
                ", lookCount='" + lookCount + '\'' +
                ", rid='" + rid + '\'' +
                ", kindId='" + kindId + '\'' +
                ", isRecommend='" + isRecommend + '\'' +
                ", showDate='" + showDate + '\'' +
                ", roleName='" + roleName + '\'' +
                ", controlUrl='" + controlUrl + '\'' +
                ", sort='" + sort + '\'' +
                ", keyword='" + keyword + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", jetxName='" + jetxName + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", title='" + title + '\'' +
                ", bName='" + bName + '\'' +
                ", list=" + list +
                ", count='" + count + '\'' +
                ", isDesc='" + isDesc + '\'' +
                ", isHome='" + isHome + '\'' +
                ", firstType='" + firstType + '\'' +
                '}';
    }
}
