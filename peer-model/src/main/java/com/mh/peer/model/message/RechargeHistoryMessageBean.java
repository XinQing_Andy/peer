package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/6/18.
 *
 * 充值记录消息对象
 */
public class RechargeHistoryMessageBean {

    /** 实际充值金额 **/
    private String sprice;
    /** 手续费 **/
    private String procedurefee;
    /** 商户订单号 **/
    private String merbillno;
    /** 充值总额 **/
    private String spriceSum;
    /** 充值状态(0-充值成功、1-充值失败) **/
    private String flag;
    /** 会员id **/
    private String memberid;
    /** 来源(0-电脑端、1-手机端) **/
    private String source;
    /** 充值时间 **/
    private String time;
    /** 创建人 **/
    private String createuser;
    /** 创建时间 **/
    private String createtime;
    /** 是否第一次进入 **/
    private String isFirst;

    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;


    /** 返回消息 **/
    private String info;
    /** 返回标志 **/
    private String result;

    public String getSprice() {
        return sprice;
    }

    public void setSprice(String sprice) {
        this.sprice = sprice;
    }

    public String getProcedurefee() {
        return procedurefee;
    }

    public void setProcedurefee(String procedurefee) {
        this.procedurefee = procedurefee;
    }

    public String getMerbillno() {
        return merbillno;
    }

    public void setMerbillno(String merbillno) {
        this.merbillno = merbillno;
    }

    public String getSpriceSum() {
        return spriceSum;
    }

    public void setSpriceSum(String spriceSum) {
        this.spriceSum = spriceSum;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(String isFirst) {
        this.isFirst = isFirst;
    }

    @Override
    public String toString() {
        return "RechargeHistoryMessageBean{" +
                "sprice='" + sprice + '\'' +
                ", procedurefee='" + procedurefee + '\'' +
                ", merbillno='" + merbillno + '\'' +
                ", spriceSum='" + spriceSum + '\'' +
                ", flag='" + flag + '\'' +
                ", memberid='" + memberid + '\'' +
                ", source='" + source + '\'' +
                ", time='" + time + '\'' +
                ", createuser='" + createuser + '\'' +
                ", createtime='" + createtime + '\'' +
                ", isFirst='" + isFirst + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
