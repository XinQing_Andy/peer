package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-31.
 */
public class HuanXunTransferMessage {

    /** 主键 **/
    private String id;
    /** 冻结id **/
    private String freezeId;
    /** 转入方IPS存管账户 **/
    private String inIpsAcctNo;
    /** 转账金额 **/
    private String trdAmt;
    /** 转出方平台服务费 **/
    private String outMerFee;
    /** 转入方平台服务费 **/
    private String inMerFee;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getFreezeId() {
        return freezeId;
    }
    public void setFreezeId(String freezeId) {
        this.freezeId = freezeId;
    }

    public String getInIpsAcctNo() {
        return inIpsAcctNo;
    }
    public void setInIpsAcctNo(String inIpsAcctNo) {
        this.inIpsAcctNo = inIpsAcctNo;
    }

    public String getTrdAmt() {
        return trdAmt;
    }
    public void setTrdAmt(String trdAmt) {
        this.trdAmt = trdAmt;
    }

    public String getOutMerFee() {
        return outMerFee;
    }
    public void setOutMerFee(String outMerFee) {
        this.outMerFee = outMerFee;
    }

    public String getInMerFee() {
        return inMerFee;
    }
    public void setInMerFee(String inMerFee) {
        this.inMerFee = inMerFee;
    }

    @Override
    public String toString() {
        return "HuanXunTransferMessage{" +
                "id='" + id + '\'' +
                ", freezeId='" + freezeId + '\'' +
                ", inIpsAcctNo='" + inIpsAcctNo + '\'' +
                ", trdAmt='" + trdAmt + '\'' +
                '}';
    }
}
