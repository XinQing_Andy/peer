package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-5-9.
 */
public class ProductInvestMessageBean {

    /** 返回信息 **/
    private String info;
    /** 返回标志 **/
    private String result;
    /** 返回页面 **/
    private String menuUrl;
    /** 主键 **/
    private String id;
    /** 产品id **/
    private String productId;
    /** 产品题目 **/
    private String title;
    /** 产品编码 **/
    private String code;
    /** 借款金额 **/
    private String loadFee;
    /** 交易密码 **/
    private String traderpassword;
    /** 购买人id **/
    private String buyer;
    /** 购买人姓名 **/
    private String buyerName;
    /** 购买价格 **/
    private String price;
    /** 还款时长 **/
    private String loadLength;
    /** 贷款利率 **/
    private String loanrate;
    /** 购买时间 **/
    private String buyTime;
    /** 购买起时间 **/
    private String buyStartTime;
    /** 购买止时间 **/
    private String buyEndTime;
    /** 到期起时间 **/
    private String expireStsrtTime;
    /** 到期止时间 **/
    private String expireEndTime;
    /** 到期日期 **/
    private String expireTime;
    /** 类型 0-持有 1-转让中 2-已转让 **/
    private String type;
    /** 验证码 **/
    private String captcha;
    /** 是否满标(0-是、1-否) **/
    private String fullScale;
    /** 满标日期 **/
    private String fullTime;
    /** 产品类型 **/
    private String productType;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 投资进度 **/
    private String investProgress;
    /** 操作时间 **/
    private String createTime;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 当前页 **/
    private String currentPage;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 查询列表拼接 **/
    private String strList;
    /** 到期时间 **/
    private String endTime;
    /** 回款状态 **/
    private String receiveFlag;
    /** 待收投资 单位：笔 **/
    private String waitPriceNum;
    /** 待收总额 **/
    private String waitPrice;
    /** 已收总额 **/
    private String ysPrice;
    /** 应收总额 **/
    private String shouldPrice;
    /** 待收期数 每笔还剩的待收 **/
    private String waitpriceperiods;
    /** 待收本金 **/
    private String waitingpricipal;
    /** 待收利息 **/
    private String waitinginterest;
    /**（笔）是否待收 **/
    private String receivebz;
    /** 字符串拼接 **/
    private String str;
    private String str2;
    /** 已投资金额 **/
    private String yInvestPrice;
    /** 未投资金额 **/
    private String wInvestPrice;
    /** 承接时间 start **/
    private String undertaketimeStart;
    /** 承接时间 end **/
    private String undertaketimeEnd;
    /** 是否从首页点击 **/
    private String isSy;
    /** 投资状态(0-回收中、1-投标中、2-已结清) **/
    private String investFlag;
    /** 下次还款日期 **/
    private String nextReceiveTime;
    /** 下次还款起日期 **/
    private String nextReceiveSTime;
    /** 下次还款止日期 **/
    private String nextReceiveETime;
    /** 结清日期 **/
    private String squareDate;
    /** 结清起日期 **/
    private String squareSDate;
    /** 结清止日期 **/
    private String squareEDate;
    /**最大转让价格**/
    private String maxattornprice;
    /**最大转让价格**/
    private String minattornprice;
    /**剩余期数**/
    private String receivedatesum;
    /**下期还款日**/
    private String nextReceiveDate;
    /** 当前应收利息**/
    private String currentwaiting;

    public String getCurrentwaiting() {
        return currentwaiting;
    }
    public void setCurrentwaiting(String currentwaiting) {
        this.currentwaiting = currentwaiting;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getLoadFee() {
        return loadFee;
    }
    public void setLoadFee(String loadFee) {
        this.loadFee = loadFee;
    }

    public String getTraderpassword() {
        return traderpassword;
    }
    public void setTraderpassword(String traderpassword) {
        this.traderpassword = traderpassword;
    }

    public String getBuyer() {
        return buyer;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getBuyerName() {
        return buyerName;
    }
    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getLoadLength() {
        return loadLength;
    }
    public void setLoadLength(String loadLength) {
        this.loadLength = loadLength;
    }

    public String getLoanrate() {
        return loanrate;
    }
    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getBuyTime() {
        return buyTime;
    }
    public void setBuyTime(String buyTime) {
        this.buyTime = buyTime;
    }

    public String getBuyStartTime() {
        return buyStartTime;
    }
    public void setBuyStartTime(String buyStartTime) {
        this.buyStartTime = buyStartTime;
    }

    public String getBuyEndTime() {
        return buyEndTime;
    }
    public void setBuyEndTime(String buyEndTime) {
        this.buyEndTime = buyEndTime;
    }

    public String getExpireStsrtTime() {
        return expireStsrtTime;
    }
    public void setExpireStsrtTime(String expireStsrtTime) {
        this.expireStsrtTime = expireStsrtTime;
    }

    public String getExpireEndTime() {
        return expireEndTime;
    }
    public void setExpireEndTime(String expireEndTime) {
        this.expireEndTime = expireEndTime;
    }

    public String getExpireTime() {
        return expireTime;
    }
    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getCaptcha() {
        return captcha;
    }
    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public String getFullScale() {
        return fullScale;
    }
    public void setFullScale(String fullScale) {
        this.fullScale = fullScale;
    }

    public String getFullTime() {
        return fullTime;
    }
    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getProductType() {
        return productType;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getInvestProgress() {
        return investProgress;
    }
    public void setInvestProgress(String investProgress) {
        this.investProgress = investProgress;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getStrList() {
        return strList;
    }
    public void setStrList(String strList) {
        this.strList = strList;
    }

    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getReceiveFlag() {
        return receiveFlag;
    }
    public void setReceiveFlag(String receiveFlag) {
        this.receiveFlag = receiveFlag;
    }

    public String getWaitPriceNum() {
        return waitPriceNum;
    }
    public void setWaitPriceNum(String waitPriceNum) {
        this.waitPriceNum = waitPriceNum;
    }

    public String getWaitPrice() {
        return waitPrice;
    }
    public void setWaitPrice(String waitPrice) {
        this.waitPrice = waitPrice;
    }

    public String getYsPrice() {
        return ysPrice;
    }
    public void setYsPrice(String ysPrice) {
        this.ysPrice = ysPrice;
    }

    public String getShouldPrice() {
        return shouldPrice;
    }
    public void setShouldPrice(String shouldPrice) {
        this.shouldPrice = shouldPrice;
    }

    public String getWaitpriceperiods() {
        return waitpriceperiods;
    }
    public void setWaitpriceperiods(String waitpriceperiods) {
        this.waitpriceperiods = waitpriceperiods;
    }

    public String getWaitingpricipal() {
        return waitingpricipal;
    }
    public void setWaitingpricipal(String waitingpricipal) {
        this.waitingpricipal = waitingpricipal;
    }

    public String getWaitinginterest() {
        return waitinginterest;
    }
    public void setWaitinginterest(String waitinginterest) {
        this.waitinginterest = waitinginterest;
    }

    public String getReceivebz() {
        return receivebz;
    }
    public void setReceivebz(String receivebz) {
        this.receivebz = receivebz;
    }

    public String getStr() {
        return str;
    }
    public void setStr(String str) {
        this.str = str;
    }

    public String getStr2() {
        return str2;
    }
    public void setStr2(String str2) {
        this.str2 = str2;
    }

    public String getyInvestPrice() {
        return yInvestPrice;
    }
    public void setyInvestPrice(String yInvestPrice) {
        this.yInvestPrice = yInvestPrice;
    }

    public String getwInvestPrice() {
        return wInvestPrice;
    }
    public void setwInvestPrice(String wInvestPrice) {
        this.wInvestPrice = wInvestPrice;
    }

    public String getUndertaketimeStart() {
        return undertaketimeStart;
    }
    public void setUndertaketimeStart(String undertaketimeStart) {
        this.undertaketimeStart = undertaketimeStart;
    }

    public String getUndertaketimeEnd() {
        return undertaketimeEnd;
    }
    public void setUndertaketimeEnd(String undertaketimeEnd) {
        this.undertaketimeEnd = undertaketimeEnd;
    }

    public String getIsSy() {
        return isSy;
    }
    public void setIsSy(String isSy) {
        this.isSy = isSy;
    }

    public String getInvestFlag() {
        return investFlag;
    }
    public void setInvestFlag(String investFlag) {
        this.investFlag = investFlag;
    }

    public String getNextReceiveTime() {
        return nextReceiveTime;
    }
    public void setNextReceiveTime(String nextReceiveTime) {
        this.nextReceiveTime = nextReceiveTime;
    }

    public String getNextReceiveSTime() {
        return nextReceiveSTime;
    }
    public void setNextReceiveSTime(String nextReceiveSTime) {
        this.nextReceiveSTime = nextReceiveSTime;
    }

    public String getNextReceiveETime() {
        return nextReceiveETime;
    }
    public void setNextReceiveETime(String nextReceiveETime) {
        this.nextReceiveETime = nextReceiveETime;
    }

    public String getSquareDate() {
        return squareDate;
    }
    public void setSquareDate(String squareDate) {
        this.squareDate = squareDate;
    }

    public String getSquareSDate() {
        return squareSDate;
    }
    public void setSquareSDate(String squareSDate) {
        this.squareSDate = squareSDate;
    }

    public String getSquareEDate() {
        return squareEDate;
    }
    public void setSquareEDate(String squareEDate) {
        this.squareEDate = squareEDate;
    }

    public String getMaxattornprice() {
        return maxattornprice;
    }

    public void setMaxattornprice(String maxattornprice) {
        this.maxattornprice = maxattornprice;
    }

    public String getMinattornprice() {
        return minattornprice;
    }

    public void setMinattornprice(String minattornprice) {
        this.minattornprice = minattornprice;
    }

    public String getReceivedatesum() {
        return receivedatesum;
    }

    public void setReceivedatesum(String receivedatesum) {
        this.receivedatesum = receivedatesum;
    }

    public String getNextReceiveDate() {
        return nextReceiveDate;
    }

    public void setNextReceiveDate(String nextReceiveDate) {
        this.nextReceiveDate = nextReceiveDate;
    }

    @Override
    public String toString() {
        return "ProductInvestMessageBean{" +
                "info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", id='" + id + '\'' +
                ", productId='" + productId + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", loadFee='" + loadFee + '\'' +
                ", traderpassword='" + traderpassword + '\'' +
                ", buyer='" + buyer + '\'' +
                ", buyerName='" + buyerName + '\'' +
                ", price='" + price + '\'' +
                ", loadLength='" + loadLength + '\'' +
                ", loanrate='" + loanrate + '\'' +
                ", buyTime='" + buyTime + '\'' +
                ", buyStartTime='" + buyStartTime + '\'' +
                ", buyEndTime='" + buyEndTime + '\'' +
                ", expireStsrtTime='" + expireStsrtTime + '\'' +
                ", expireEndTime='" + expireEndTime + '\'' +
                ", expireTime='" + expireTime + '\'' +
                ", type='" + type + '\'' +
                ", captcha='" + captcha + '\'' +
                ", fullScale='" + fullScale + '\'' +
                ", fullTime='" + fullTime + '\'' +
                ", productType='" + productType + '\'' +
                ", repayment='" + repayment + '\'' +
                ", investProgress='" + investProgress + '\'' +
                ", createTime='" + createTime + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", strList='" + strList + '\'' +
                ", endTime='" + endTime + '\'' +
                ", receiveFlag='" + receiveFlag + '\'' +
                ", waitPriceNum='" + waitPriceNum + '\'' +
                ", waitPrice='" + waitPrice + '\'' +
                ", ysPrice='" + ysPrice + '\'' +
                ", shouldPrice='" + shouldPrice + '\'' +
                ", waitpriceperiods='" + waitpriceperiods + '\'' +
                ", waitingpricipal='" + waitingpricipal + '\'' +
                ", waitinginterest='" + waitinginterest + '\'' +
                ", receivebz='" + receivebz + '\'' +
                ", str='" + str + '\'' +
                ", str2='" + str2 + '\'' +
                ", yInvestPrice='" + yInvestPrice + '\'' +
                ", wInvestPrice='" + wInvestPrice + '\'' +
                ", undertaketimeStart='" + undertaketimeStart + '\'' +
                ", undertaketimeEnd='" + undertaketimeEnd + '\'' +
                ", isSy='" + isSy + '\'' +
                ", investFlag='" + investFlag + '\'' +
                ", nextReceiveTime='" + nextReceiveTime + '\'' +
                ", nextReceiveSTime='" + nextReceiveSTime + '\'' +
                ", nextReceiveETime='" + nextReceiveETime + '\'' +
                ", squareDate='" + squareDate + '\'' +
                ", squareSDate='" + squareSDate + '\'' +
                ", squareEDate='" + squareEDate + '\'' +
                ", currentwaiting='" + currentwaiting + '\'' +
                '}';
    }
}
