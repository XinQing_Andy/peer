package com.mh.peer.model.message;


/**
 * Created by Cuibin on 2016/7/8.
 */
public class WeixinNewsMes {

    /** 主键 **/
    private String id;
    /** 内容 **/
    private String content;
    /** 作者 **/
    private String createUser;
    /** 创建日期 **/
    private String createDate;
    /** 类型 **/
    private String type;
    /** 有效标识 0：无效  1：有效 **/
    private String availability;
    /** 是否优先 0：取消优先 1：优先 **/
    private String first;
    /** 优先时间 **/
    private String firstDate;
    /** 浏览次数 **/
    private int lookCount;
    /** 关联id(附件存储的是首页显示图片) **/
    private int rid;
    /** 类别id **/
    private int kindId;
    /** 是否推荐 1：推荐 0：不推荐 **/
    private String isRecommend;
    /** 显示时间 **/
    private String showDate;
    /** 角色名称 **/
    private String roleName;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 题目 **/
    private String title;
    /** 图片地址 **/
    private String img;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /** 产品总数 **/
    private String count;
    /** HTML内容 **/
    private String htmlText;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(String firstDate) {
        this.firstDate = firstDate;
    }

    public int getLookCount() {
        return lookCount;
    }

    public void setLookCount(int lookCount) {
        this.lookCount = lookCount;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public int getKindId() {
        return kindId;
    }

    public void setKindId(int kindId) {
        this.kindId = kindId;
    }

    public String getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(String isRecommend) {
        this.isRecommend = isRecommend;
    }

    public String getShowDate() {
        return showDate;
    }

    public void setShowDate(String showDate) {
        this.showDate = showDate;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    @Override
    public String toString() {
        return "WeixinNewsMes{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                ", createUser='" + createUser + '\'' +
                ", createDate='" + createDate + '\'' +
                ", type='" + type + '\'' +
                ", availability='" + availability + '\'' +
                ", first='" + first + '\'' +
                ", firstDate='" + firstDate + '\'' +
                ", lookCount=" + lookCount +
                ", rid=" + rid +
                ", kindId=" + kindId +
                ", isRecommend='" + isRecommend + '\'' +
                ", showDate='" + showDate + '\'' +
                ", roleName='" + roleName + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", title='" + title + '\'' +
                ", img='" + img + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", count='" + count + '\'' +
                ", htmlText='" + htmlText + '\'' +
                '}';
    }
}
