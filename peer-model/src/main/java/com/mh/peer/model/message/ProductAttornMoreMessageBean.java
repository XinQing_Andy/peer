package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-20.
 * 查询更多债权
 */
public class ProductAttornMoreMessageBean {

    /** 债权id **/
    private String id;
    /** 题目 **/
    private String title;
    /** 承接人 **/
    private String underTakeMember;
    /** 承接时间 **/
    private String underTakeTime;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 起始数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 转让类型(0-已转让、1-带转让) **/
    private String attornType;
    /** 债权转让列表拼接 **/
    private String attornListStr;
    /** 分页部分拼接 **/
    private String pageStr;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnderTakeMember() {
        return underTakeMember;
    }
    public void setUnderTakeMember(String underTakeMember) {
        this.underTakeMember = underTakeMember;
    }

    public String getUnderTakeTime() {
        return underTakeTime;
    }
    public void setUnderTakeTime(String underTakeTime) {
        this.underTakeTime = underTakeTime;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getAttornType() {
        return attornType;
    }
    public void setAttornType(String attornType) {
        this.attornType = attornType;
    }

    public String getAttornListStr() {
        return attornListStr;
    }
    public void setAttornListStr(String attornListStr) {
        this.attornListStr = attornListStr;
    }

    public String getPageStr() {
        return pageStr;
    }
    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }
}
