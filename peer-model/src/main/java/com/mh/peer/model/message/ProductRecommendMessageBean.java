package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-10-12.
 * 推荐产品
 */
public class ProductRecommendMessageBean {

    /** 产品id **/
    private String id;
    /** 产品题目 **/
    private String title;
    /** 贷款利率 **/
    private String loanRate;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 还款方式名称 **/
    private String repaymentName;
    /** 贷款期限 **/
    private String loadLenth;
    /** 剩余还款 **/
    private String surplusRepayPrice;
    /** 贷款金额 **/
    private String loanPrice;
    /** 投资进度 **/
    private String investProgress;
    /** 是否满标(0-是、1-否) **/
    private String fullScale;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getRepaymentName() {
        return repaymentName;
    }
    public void setRepaymentName(String repaymentName) {
        this.repaymentName = repaymentName;
    }

    public String getLoadLenth() {
        return loadLenth;
    }
    public void setLoadLenth(String loadLenth) {
        this.loadLenth = loadLenth;
    }

    public String getSurplusRepayPrice() {
        return surplusRepayPrice;
    }
    public void setSurplusRepayPrice(String surplusRepayPrice) {
        this.surplusRepayPrice = surplusRepayPrice;
    }

    public String getLoanPrice() {
        return loanPrice;
    }
    public void setLoanPrice(String loanPrice) {
        this.loanPrice = loanPrice;
    }

    public String getInvestProgress() {
        return investProgress;
    }
    public void setInvestProgress(String investProgress) {
        this.investProgress = investProgress;
    }

    public String getFullScale() {
        return fullScale;
    }
    public void setFullScale(String fullScale) {
        this.fullScale = fullScale;
    }

    @Override
    public String toString() {
        return "ProductRecommendMessageBean{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", repayment='" + repayment + '\'' +
                ", repaymentName='" + repaymentName + '\'' +
                ", loadLenth='" + loadLenth + '\'' +
                ", surplusRepayPrice='" + surplusRepayPrice + '\'' +
                ", loanPrice='" + loanPrice + '\'' +
                ", investProgress='" + investProgress + '\'' +
                ", fullScale='" + fullScale + '\'' +
                '}';
    }
}
