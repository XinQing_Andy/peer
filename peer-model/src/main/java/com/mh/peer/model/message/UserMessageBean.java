package com.mh.peer.model.message;


import com.mh.peer.model.entity.SysRole;

import java.util.List;

/**员工消息对象
 * Created by yangbin on 2015/12/11.
 */
public class UserMessageBean {
    /*
    * 模板名称
    * */
    private  String jetxName;
    /*
    *    返回成功或失败(error,success)
    * */
    private String result;
    /*
      * 错误消息
      * */
    private String info;
    /*
    * 用户id
    * */
    private int id;
    /*
    * 用户名称
    * */
    private String userName;
    /*
    * 密码
    * */
    private String password;
    /*
    * 真实姓名
    * */
    private  String realName;
    /*
    *   qq
    * */
    private String qq;
    /*
    * 电话
    * */
    private String phoneNo;
    /*
    * 所在部门
    * */
    private int dept;
    /*
    * 是否禁用(y启用/禁用)
    * */
    private String state;
    /*
    * 时间
    * */
    private String time;
    /*
    * 密码口令
    * */
    private  String passwNo;
    /*
    * 权限Id
    * */
    private int roleId;
    /*
    * EMAIL
    * */
    private String email;
    private List<SysRole> sysRoleList;

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public int getDept() {
        return dept;
    }

    public void setDept(int dept) {
        this.dept = dept;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPasswNo() {
        return passwNo;
    }

    public void setPasswNo(String passwNo) {
        this.passwNo = passwNo;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<SysRole> getSysRoleList() {
        return sysRoleList;
    }

    public void setSysRoleList(List<SysRole> sysRoleList) {
        this.sysRoleList = sysRoleList;
    }

    @Override
    public String toString() {
        return "UserMessageBean{" +
                "jetxName='" + jetxName + '\'' +
                ", result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id=" + id +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", realName='" + realName + '\'' +
                ", qq='" + qq + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", dept=" + dept +
                ", state='" + state + '\'' +
                ", time='" + time + '\'' +
                ", passwNo='" + passwNo + '\'' +
                ", roleId=" + roleId +
                ", email='" + email + '\'' +
                ", sysRoleList=" + sysRoleList +
                '}';
    }
}
