package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-9.
 * 我的信息
 */
public class AppMemberInfoMessageBean {

    /** 主键 **/
    private String id;
    /** 昵称 **/
    private String nickName;
    /** 积分 **/
    private String points;
    /** 余额 **/
    private String balance;
    /** 累计收益 **/
    private String interest;
    /** 待收总额 **/
    private String receivedPrice;
    /** 投资笔数 **/
    private String investCount;
    /** 需还款金额 **/
    private String repayedPrice;
    /** 发起债权转让笔数 **/
    private String attornCount;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPoints() {
        return points;
    }
    public void setPoints(String points) {
        this.points = points;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getInterest() {
        return interest;
    }
    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getReceivedPrice() {
        return receivedPrice;
    }
    public void setReceivedPrice(String receivedPrice) {
        this.receivedPrice = receivedPrice;
    }

    public String getInvestCount() {
        return investCount;
    }
    public void setInvestCount(String investCount) {
        this.investCount = investCount;
    }

    public String getRepayedPrice() {
        return repayedPrice;
    }
    public void setRepayedPrice(String repayedPrice) {
        this.repayedPrice = repayedPrice;
    }

    public String getAttornCount() {
        return attornCount;
    }
    public void setAttornCount(String attornCount) {
        this.attornCount = attornCount;
    }

    @Override
    public String toString() {
        return "AppMemberInfoMessageBean{" +
                "id='" + id + '\'' +
                ", nickName='" + nickName + '\'' +
                ", points='" + points + '\'' +
                ", balance='" + balance + '\'' +
                ", interest='" + interest + '\'' +
                ", receivedPrice='" + receivedPrice + '\'' +
                ", investCount='" + investCount + '\'' +
                ", repayedPrice='" + repayedPrice + '\'' +
                ", attornCount='" + attornCount + '\'' +
                '}';
    }
}
