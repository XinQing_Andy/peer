package com.mh.peer.model.business;

/**
 * Created by zhangerxin on 2016-3-31.
 */
public class SysSecurityParameterBusinessBean {

    /*
    * 返回成功或失败(error,success)
    * */
    private String result;
    /*
    *  错误消息
    * */
    private String info;
    /**返回主键**/
    private String id;
    /** 关键字 **/
    private String keyword;
    /** 是否启用密码错误次数超限锁定 **/
    private String activeFlg;
    /** 密码错误次数 **/
    private String times;
    /** 密码输入锁定时长 **/
    private String time;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getActiveFlg() {
        return activeFlg;
    }
    public void setActiveFlg(String activeFlg) {
        this.activeFlg = activeFlg;
    }

    public String getTimes() {
        return times;
    }
    public void setTimes(String times) {
        this.times = times;
    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "SysSecurityParameterBusinessBean{" +
                "id='" + id + '\'' +
                ", keyword='" + keyword + '\'' +
                ", activeFlg='" + activeFlg + '\'' +
                ", times=" + times +
                ", time=" + time +
                '}';
    }
}
