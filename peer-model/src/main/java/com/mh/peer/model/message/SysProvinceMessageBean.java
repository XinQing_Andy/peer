package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/5/3.
 */
public class SysProvinceMessageBean {
    /** 主键 **/
    private String id;
    /** 省名称 **/
    private String provinceName;
    /** 顺序 **/
    private String sort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "SysProvinceMessageBean{" +
                "id='" + id + '\'' +
                ", provinceName='" + provinceName + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }
}
