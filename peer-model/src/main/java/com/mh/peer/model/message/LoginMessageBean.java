package com.mh.peer.model.message;

/**
 * Created by zhaojiaming on 2016/1/24.
 */
public class  LoginMessageBean {
    /**
     *返回成功或失败(error,success)
     */
    private String result;

    /**
     * 返回结果说明
     */
    private String info;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户ID
     */
    private String id;

    /**
     * 密码
     */
    private String password;

    /**
     * 验证码
     */
    private String verifyCode;

    /**
     * 记住我
     */
    private String remember;
    /**
     * 消息ID
     */
    private String messageId;
    /**
     * ip
     */
    private String ip;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getRemember() {
        return remember;
    }

    public void setRemember(String remember) {
        this.remember = remember;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "LoginMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", userName='" + userName + '\'' +
                ", id='" + id + '\'' +
                ", password='" + password + '\'' +
                ", verifyCode='" + verifyCode + '\'' +
                ", remember='" + remember + '\'' +
                ", messageId='" + messageId + '\'' +
                ", ip='" + ip + '\'' +
                '}';
    }
}
