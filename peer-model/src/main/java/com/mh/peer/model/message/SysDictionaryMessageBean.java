package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/4/12.
 * 大字典消息对象
 */
public class SysDictionaryMessageBean {
    /** 主键 **/
    private int id;
    /** 组类别 **/
    private String groupType;
    /** 成员id **/
    private int memberId;
    /** 成员名称 **/
    private String memberName;
    /** 启用注记 **/
    private String activeFlg;
    /** 备注 **/
    private String remark;
    /**  **/
    private String info;
    /**  **/
    private String result;

    private String menuUrl;

    private String jetxName;

    public String getActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(String activeFlg) {
        this.activeFlg = activeFlg;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "SysDictionaryMessageBean{" +
                "activeFlg='" + activeFlg + '\'' +
                ", id=" + id +
                ", groupType='" + groupType + '\'' +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", remark='" + remark + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", jetxName='" + jetxName + '\'' +
                '}';
    }
}
