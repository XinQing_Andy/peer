package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/4/12.
 */
public class SysTakeNoMessageBean {
    /** 主键 **/
    private int id;
    /** 取号类别 **/
    private String codeType;
    /** 循环原则 **/
    private String statusMethod;
    /** 当前号码 **/
    private String currNo;
    /** 长度 **/
    private String noLength;
    /** 开始号码 **/
    private String startNo;
    /** 最大号码 **/
    private String maxNo;
    /** 备注 **/
    private String noDesc;
    /** 格式 0：无年月日  1：有年月日 **/
    private String noFormat;
    /** 创建人 **/
    private int createUser;
    /** 创建日期 **/
    private String createDate;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /**  **/
    private String menuUrl;
    /**  **/
    private String jetxName;

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUser() {
        return createUser;
    }

    public void setCreateUser(int createUser) {
        this.createUser = createUser;
    }

    public String getCurrNo() {
        return currNo;
    }

    public void setCurrNo(String currNo) {
        this.currNo = currNo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getMaxNo() {
        return maxNo;
    }

    public void setMaxNo(String maxNo) {
        this.maxNo = maxNo;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getNoDesc() {
        return noDesc;
    }

    public void setNoDesc(String noDesc) {
        this.noDesc = noDesc;
    }

    public String getNoFormat() {
        return noFormat;
    }

    public void setNoFormat(String noFormat) {
        this.noFormat = noFormat;
    }

    public String getNoLength() {
        return noLength;
    }

    public void setNoLength(String noLength) {
        this.noLength = noLength;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStartNo() {
        return startNo;
    }

    public void setStartNo(String startNo) {
        this.startNo = startNo;
    }

    public String getStatusMethod() {
        return statusMethod;
    }

    public void setStatusMethod(String statusMethod) {
        this.statusMethod = statusMethod;
    }

    @Override
    public String toString() {
        return "SysTakeNoMessageBean{" +
                "codeType='" + codeType + '\'' +
                ", id=" + id +
                ", statusMethod='" + statusMethod + '\'' +
                ", currNo='" + currNo + '\'' +
                ", noLength='" + noLength + '\'' +
                ", startNo='" + startNo + '\'' +
                ", maxNo='" + maxNo + '\'' +
                ", noDesc='" + noDesc + '\'' +
                ", noFormat='" + noFormat + '\'' +
                ", createUser=" + createUser +
                ", createDate='" + createDate + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", jetxName='" + jetxName + '\'' +
                '}';
    }
}
