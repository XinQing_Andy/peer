package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-8-5.
 */
public class ProductAutoRepayMessageBean {

    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String buyId;
    /** 项目ID号 **/
    private String projectNo;
    /** 本金 **/
    private String principal;
    /** 利息 **/
    private String interest;
    /** 应回款日期 **/
    private String yreceiveTime;
    /** 实际回款金额 **/
    private String receivePrice;
    /** 实际回款日期 **/
    private String sreceiveTime;
    /** 购买人 **/
    private String buyer;
    /** 借款人 **/
    private String applyMember;
    /** 转出方账号 **/
    private String outIpsAcctNo;
    /** 转入方账号 **/
    private String inIpsAcctNo;
    /** 转出方自动签约订单号 **/
    private String outAutoSignBillNo;
    /** 转出方自动签约授权号 **/
    private String outAutoSignAuthNo;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getBuyId() {
        return buyId;
    }
    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }

    public String getProjectNo() {
        return projectNo;
    }
    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getPrincipal() {
        return principal;
    }
    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getInterest() {
        return interest;
    }
    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getYreceiveTime() {
        return yreceiveTime;
    }
    public void setYreceiveTime(String yreceiveTime) {
        this.yreceiveTime = yreceiveTime;
    }

    public String getReceivePrice() {
        return receivePrice;
    }
    public void setReceivePrice(String receivePrice) {
        this.receivePrice = receivePrice;
    }

    public String getSreceiveTime() {
        return sreceiveTime;
    }
    public void setSreceiveTime(String sreceiveTime) {
        this.sreceiveTime = sreceiveTime;
    }

    public String getBuyer() {
        return buyer;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getApplyMember() {
        return applyMember;
    }
    public void setApplyMember(String applyMember) {
        this.applyMember = applyMember;
    }

    public String getOutIpsAcctNo() {
        return outIpsAcctNo;
    }
    public void setOutIpsAcctNo(String outIpsAcctNo) {
        this.outIpsAcctNo = outIpsAcctNo;
    }

    public String getInIpsAcctNo() {
        return inIpsAcctNo;
    }
    public void setInIpsAcctNo(String inIpsAcctNo) {
        this.inIpsAcctNo = inIpsAcctNo;
    }

    public String getOutAutoSignBillNo() {
        return outAutoSignBillNo;
    }
    public void setOutAutoSignBillNo(String outAutoSignBillNo) {
        this.outAutoSignBillNo = outAutoSignBillNo;
    }

    public String getOutAutoSignAuthNo() {
        return outAutoSignAuthNo;
    }
    public void setOutAutoSignAuthNo(String outAutoSignAuthNo) {
        this.outAutoSignAuthNo = outAutoSignAuthNo;
    }

    @Override
    public String toString() {
        return "ProductAutoRepayMessageBean{" +
                "id='" + id + '\'' +
                ", buyId='" + buyId + '\'' +
                ", projectNo='" + projectNo + '\'' +
                ", principal='" + principal + '\'' +
                ", interest='" + interest + '\'' +
                ", yreceiveTime='" + yreceiveTime + '\'' +
                ", receivePrice='" + receivePrice + '\'' +
                ", sreceiveTime='" + sreceiveTime + '\'' +
                ", buyer='" + buyer + '\'' +
                ", applyMember='" + applyMember + '\'' +
                ", outIpsAcctNo='" + outIpsAcctNo + '\'' +
                ", inIpsAcctNo='" + inIpsAcctNo + '\'' +
                ", outAutoSignBillNo='" + outAutoSignBillNo + '\'' +
                ", outAutoSignAuthNo='" + outAutoSignAuthNo + '\'' +
                '}';
    }
}
