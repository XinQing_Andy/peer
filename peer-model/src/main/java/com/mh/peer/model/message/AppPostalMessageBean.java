package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-8-15.
 * App提现部分
 */
public class AppPostalMessageBean {

    /** 主键 **/
    private String id;
    /** 提现金额 **/
    private String postalPrice;
    /** 余额 **/
    private String balance;
    /** 会员id **/
    private String memberId;
    /** 来源(0-电脑端、1-手机端) **/
    private String source;
    /** 签名 **/
    private String sign;
    /** 请求信息 **/
    private String request;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getPostalPrice() {
        return postalPrice;
    }
    public void setPostalPrice(String postalPrice) {
        this.postalPrice = postalPrice;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getMemberId() {
        return memberId;
    }
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public String getSign() {
        return sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "AppPostalMessageBean{" +
                "id='" + id + '\'' +
                ", postalPrice='" + postalPrice + '\'' +
                ", balance='" + balance + '\'' +
                ", memberId='" + memberId + '\'' +
                ", source='" + source + '\'' +
                ", sign='" + sign + '\'' +
                ", request='" + request + '\'' +
                '}';
    }
}
