package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/7/15.
 */
public class WeixinProductAttornDetail {

    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String productbuyid;
    /** 转让金额 **/
    private String attornprice;
    /** 预期收益率 **/
    private String expectedrate;
    /** 产品id **/
    private String productid;
    /** 题目 **/
    private String title;
    /** 贷款利率 **/
    private String loanrate;
    /**  **/
    private String surplusdays;
    /**  **/
    private String surplusprincipal;
    /**  **/
    private String receivedprice;
    /**  **/
    private String nextreceivetime;
    /** 借款标编号 **/
    private String code;
    /**  **/
    private String loanfee;
    /**  **/
    private String fulltime;
    /** 贷款期限(年) **/
    private String loanyear;
    /** 贷款期限(月) **/
    private String loanmonth;
    /** 贷款期限(日) **/
    private String loadday;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 当前数 **/
    private String pageIndex;
    /** 每次显示数量 **/
    private String pageSize;
    /** 每次显示数量 **/
    private String typeId;
    /** 借款期限 **/
    private String loadLength;
    /** 总数量 **/
    private String count;
    /** html文本 **/
    private String htmlText;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /** 借款期限 **/
    private String loanLength;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductbuyid() {
        return productbuyid;
    }

    public void setProductbuyid(String productbuyid) {
        this.productbuyid = productbuyid;
    }

    public String getAttornprice() {
        return attornprice;
    }

    public void setAttornprice(String attornprice) {
        this.attornprice = attornprice;
    }

    public String getExpectedrate() {
        return expectedrate;
    }

    public void setExpectedrate(String expectedrate) {
        this.expectedrate = expectedrate;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLoanrate() {
        return loanrate;
    }

    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getSurplusdays() {
        return surplusdays;
    }

    public void setSurplusdays(String surplusdays) {
        this.surplusdays = surplusdays;
    }

    public String getSurplusprincipal() {
        return surplusprincipal;
    }

    public void setSurplusprincipal(String surplusprincipal) {
        this.surplusprincipal = surplusprincipal;
    }

    public String getReceivedprice() {
        return receivedprice;
    }

    public void setReceivedprice(String receivedprice) {
        this.receivedprice = receivedprice;
    }

    public String getNextreceivetime() {
        return nextreceivetime;
    }

    public void setNextreceivetime(String nextreceivetime) {
        this.nextreceivetime = nextreceivetime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLoanfee() {
        return loanfee;
    }

    public void setLoanfee(String loanfee) {
        this.loanfee = loanfee;
    }

    public String getFulltime() {
        return fulltime;
    }

    public void setFulltime(String fulltime) {
        this.fulltime = fulltime;
    }

    public String getLoanyear() {
        return loanyear;
    }

    public void setLoanyear(String loanyear) {
        this.loanyear = loanyear;
    }

    public String getLoanmonth() {
        return loanmonth;
    }

    public void setLoanmonth(String loanmonth) {
        this.loanmonth = loanmonth;
    }

    public String getLoadday() {
        return loadday;
    }

    public void setLoadday(String loadday) {
        this.loadday = loadday;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getOrderColumn() {
        return orderColumn;
    }

    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getLoadLength() {
        return loadLength;
    }

    public void setLoadLength(String loadLength) {
        this.loadLength = loadLength;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getLoanLength() {
        return loanLength;
    }

    public void setLoanLength(String loanLength) {
        this.loanLength = loanLength;
    }

    @Override
    public String toString() {
        return "WeixinProductAttornDetail{" +
                "id='" + id + '\'' +
                ", productbuyid='" + productbuyid + '\'' +
                ", attornprice='" + attornprice + '\'' +
                ", expectedrate='" + expectedrate + '\'' +
                ", productid='" + productid + '\'' +
                ", title='" + title + '\'' +
                ", loanrate='" + loanrate + '\'' +
                ", surplusdays='" + surplusdays + '\'' +
                ", surplusprincipal='" + surplusprincipal + '\'' +
                ", receivedprice='" + receivedprice + '\'' +
                ", nextreceivetime='" + nextreceivetime + '\'' +
                ", code='" + code + '\'' +
                ", loanfee='" + loanfee + '\'' +
                ", fulltime='" + fulltime + '\'' +
                ", loanyear='" + loanyear + '\'' +
                ", loanmonth='" + loanmonth + '\'' +
                ", loadday='" + loadday + '\'' +
                ", repayment='" + repayment + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", typeId='" + typeId + '\'' +
                ", loadLength='" + loadLength + '\'' +
                ", count='" + count + '\'' +
                ", htmlText='" + htmlText + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", loanLength='" + loanLength + '\'' +
                '}';
    }
}
