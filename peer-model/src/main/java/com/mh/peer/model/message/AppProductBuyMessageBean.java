package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-7.
 */
public class AppProductBuyMessageBean {

    /** 产品投资id **/
    private String id;
    /** 产品题目 **/
    private String title;
    /** 年利率 **/
    private String loanRate;
    /** 产品id **/
    private String productId;
    /** 产品类型id **/
    private String typeId;
    /** 到期时间 **/
    private String endTime;
    /** 投资人 **/
    private String buyer;
    /** 投资人姓名 **/
    private String buyerName;
    /** 投资金额 **/
    private String price;
    /** 投资时间 **/
    private String buyTime;
    /** 类型 0-持有 1-转让中 2-已转让 **/
    private String type;
    /** 创建时间 **/
    private String createTime;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getBuyer() {
        return buyer;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getBuyerName() {
        return buyerName;
    }
    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getBuyTime() {
        return buyTime;
    }
    public void setBuyTime(String buyTime) {
        this.buyTime = buyTime;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "AppProductBuyMessageBean{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", productId='" + productId + '\'' +
                ", typeId='" + typeId + '\'' +
                ", endTime='" + endTime + '\'' +
                ", buyer='" + buyer + '\'' +
                ", buyerName='" + buyerName + '\'' +
                ", price='" + price + '\'' +
                ", buyTime='" + buyTime + '\'' +
                ", type='" + type + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
