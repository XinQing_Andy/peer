package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/4/12.
 */
public class MemberLevelMessageBean {
    /** 主键 **/
    private int id;
    /** 等级名称 **/
    private String levelName;
    /** 等级最低要求 **/
    private String levelRequest;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /**  **/
    private String jetxName;
    /**  **/
    private String menuUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getLevelRequest() {
        return levelRequest;
    }

    public void setLevelRequest(String levelRequest) {
        this.levelRequest = levelRequest;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "MemberLevelMessageBean{" +
                "id=" + id +
                ", levelName='" + levelName + '\'' +
                ", levelRequest='" + levelRequest + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", jetxName='" + jetxName + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                '}';
    }
}
