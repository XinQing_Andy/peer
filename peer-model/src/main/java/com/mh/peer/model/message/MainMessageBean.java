package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/6/7.
 * 后台首页消息对象
 */
public class MainMessageBean {
    /** 最近30天投资金额 **/
    private String registMemberSum;
    /** 会员总数 **/
    private String memberCount;
    /** 成功借款标数量 **/
    private String porductCount;
    /** 成功借款总额 **/
    private String feeSum;
    /** 待审核借款标数量 **/
    private String awaitBorrowSum;
    /** 将要到期借款标数量 **/
    private String DueDateSum;
    /** 满标借款标数量 **/
    private String fullScaleSum;
    /** 转让申请数量 **/
    private String transferSum;
    /** 待放款借款标 **/
    private String waitLoan;
    /** 到期还款金额 **/
    private String expireRefund;
    /** 待还总额 **/
    private String waitTotal;

    public String getRegistMemberSum() {
        return registMemberSum;
    }

    public void setRegistMemberSum(String registMemberSum) {
        this.registMemberSum = registMemberSum;
    }

    public String getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(String memberCount) {
        this.memberCount = memberCount;
    }

    public String getPorductCount() {
        return porductCount;
    }

    public void setPorductCount(String porductCount) {
        this.porductCount = porductCount;
    }

    public String getFeeSum() {
        return feeSum;
    }

    public void setFeeSum(String feeSum) {
        this.feeSum = feeSum;
    }

    public String getAwaitBorrowSum() {
        return awaitBorrowSum;
    }

    public void setAwaitBorrowSum(String awaitBorrowSum) {
        this.awaitBorrowSum = awaitBorrowSum;
    }

    public String getDueDateSum() {
        return DueDateSum;
    }

    public void setDueDateSum(String dueDateSum) {
        DueDateSum = dueDateSum;
    }

    public String getFullScaleSum() {
        return fullScaleSum;
    }

    public void setFullScaleSum(String fullScaleSum) {
        this.fullScaleSum = fullScaleSum;
    }

    public String getTransferSum() {
        return transferSum;
    }

    public void setTransferSum(String transferSum) {
        this.transferSum = transferSum;
    }

    public String getWaitLoan() {
        return waitLoan;
    }

    public void setWaitLoan(String waitLoan) {
        this.waitLoan = waitLoan;
    }

    public String getExpireRefund() {
        return expireRefund;
    }

    public void setExpireRefund(String expireRefund) {
        this.expireRefund = expireRefund;
    }

    public String getWaitTotal() {
        return waitTotal;
    }

    public void setWaitTotal(String waitTotal) {
        this.waitTotal = waitTotal;
    }

    @Override
    public String toString() {
        return "MainMessageBean{" +
                "registMemberSum='" + registMemberSum + '\'' +
                ", memberCount='" + memberCount + '\'' +
                ", porductCount='" + porductCount + '\'' +
                ", feeSum='" + feeSum + '\'' +
                ", awaitBorrowSum='" + awaitBorrowSum + '\'' +
                ", DueDateSum='" + DueDateSum + '\'' +
                ", fullScaleSum='" + fullScaleSum + '\'' +
                ", transferSum='" + transferSum + '\'' +
                ", waitLoan='" + waitLoan + '\'' +
                ", expireRefund='" + expireRefund + '\'' +
                ", waitTotal='" + waitTotal + '\'' +
                '}';
    }
}
