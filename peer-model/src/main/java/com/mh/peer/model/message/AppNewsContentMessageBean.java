package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-15.
 */
public class AppNewsContentMessageBean {

    /** 新闻id **/
    private String id;
    /** 题目 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 有效标识 0：无效  1：有效 **/
    private String availability;
    /** 显示时间 **/
    private String showDate;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getAvailability() {
        return availability;
    }
    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getShowDate() {
        return showDate;
    }
    public void setShowDate(String showDate) {
        this.showDate = showDate;
    }

    @Override
    public String toString() {
        return "AppNewsContentMessageBean{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", availability='" + availability + '\'' +
                ", showDate='" + showDate + '\'' +
                '}';
    }
}
