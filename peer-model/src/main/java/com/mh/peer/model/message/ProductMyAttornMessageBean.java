package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-6-24.
 * 我的债权
 */
public class ProductMyAttornMessageBean {

    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String productBuyId;
    /** 转让金额 **/
    private String price;
    /** 预期收益率 **/
    private String expectedRate;
    /** 审核状态 **/
    private String shFlag;
    /** 审核意见 **/
    private String shyj;
    /** 承接人id **/
    private String underTakeMember;
    /** 承接时间 **/
    private String underTakeTime;
    /** 创建人 **/
    private String createUser;
    /** 创建时间 **/
    private String createTime;
    /** 交易密码 **/
    private String tradePassword;
    /** 余额 **/
    private String balance;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductBuyId() {
        return productBuyId;
    }
    public void setProductBuyId(String productBuyId) {
        this.productBuyId = productBuyId;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getExpectedRate() {
        return expectedRate;
    }
    public void setExpectedRate(String expectedRate) {
        this.expectedRate = expectedRate;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getShyj() {
        return shyj;
    }
    public void setShyj(String shyj) {
        this.shyj = shyj;
    }

    public String getUnderTakeMember() {
        return underTakeMember;
    }
    public void setUnderTakeMember(String underTakeMember) {
        this.underTakeMember = underTakeMember;
    }

    public String getUnderTakeTime() {
        return underTakeTime;
    }
    public void setUnderTakeTime(String underTakeTime) {
        this.underTakeTime = underTakeTime;
    }

    public String getCreateUser() {
        return createUser;
    }
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getTradePassword() {
        return tradePassword;
    }
    public void setTradePassword(String tradePassword) {
        this.tradePassword = tradePassword;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "ProductMyAttornMessageBean{" +
                "id='" + id + '\'' +
                ", productBuyId='" + productBuyId + '\'' +
                ", price='" + price + '\'' +
                ", expectedRate='" + expectedRate + '\'' +
                ", shFlag='" + shFlag + '\'' +
                ", shyj='" + shyj + '\'' +
                ", underTakeMember='" + underTakeMember + '\'' +
                ", underTakeTime='" + underTakeTime + '\'' +
                ", createUser='" + createUser + '\'' +
                ", createTime='" + createTime + '\'' +
                ", tradePassword='" + tradePassword + '\'' +
                ", balance='" + balance + '\'' +
                '}';
    }
}
