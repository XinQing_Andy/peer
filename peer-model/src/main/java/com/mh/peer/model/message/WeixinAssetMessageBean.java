package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/7/10.
 */
public class WeixinAssetMessageBean {
    /** 主键 **/
    private String id;
    /** 昵称 **/
    private String nickname;
    /** 积分 **/
    private String points;
    /** 当前登录时间 **/
    private String nowTime;
    /** 管理费率 **/
    private String manageRate;
    /** 累计金额收益 **/
    private String interestTotal;
    /** 可用余额 **/
    private String balance;
    /** 待收总额 **/
    private String dsPrice;
    /** 待还总额 **/
    private String dhPrice;
    /** 账户净资产 **/
    private String accountPrice;
    /** 投资数量 **/
    private String investTotal;
    /** 转让数量 **/
    private String attornTotal;
    /** 借款数量 **/
    private String productTotal;
    /** 日历拼接 **/
    private String rlStr;
    /** 年份 **/
    private String year;
    /** 月份 **/
    private String month;
    /** 日 **/
    private String day;
    /** 回款数量 **/
    private String receiveCount;
    /** 还款数量 **/
    private String repayCount;
    /** 回款金额 **/
    private String receivePrice;
    /** 还款金额 **/
    private String repayPrice;
    /** 创建时间 **/
    private String createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getNowTime() {
        return nowTime;
    }

    public void setNowTime(String nowTime) {
        this.nowTime = nowTime;
    }

    public String getManageRate() {
        return manageRate;
    }

    public void setManageRate(String manageRate) {
        this.manageRate = manageRate;
    }

    public String getInterestTotal() {
        return interestTotal;
    }

    public void setInterestTotal(String interestTotal) {
        this.interestTotal = interestTotal;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDsPrice() {
        return dsPrice;
    }

    public void setDsPrice(String dsPrice) {
        this.dsPrice = dsPrice;
    }

    public String getDhPrice() {
        return dhPrice;
    }

    public void setDhPrice(String dhPrice) {
        this.dhPrice = dhPrice;
    }

    public String getAccountPrice() {
        return accountPrice;
    }

    public void setAccountPrice(String accountPrice) {
        this.accountPrice = accountPrice;
    }

    public String getInvestTotal() {
        return investTotal;
    }

    public void setInvestTotal(String investTotal) {
        this.investTotal = investTotal;
    }

    public String getAttornTotal() {
        return attornTotal;
    }

    public void setAttornTotal(String attornTotal) {
        this.attornTotal = attornTotal;
    }

    public String getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(String productTotal) {
        this.productTotal = productTotal;
    }

    public String getRlStr() {
        return rlStr;
    }

    public void setRlStr(String rlStr) {
        this.rlStr = rlStr;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getReceiveCount() {
        return receiveCount;
    }

    public void setReceiveCount(String receiveCount) {
        this.receiveCount = receiveCount;
    }

    public String getRepayCount() {
        return repayCount;
    }

    public void setRepayCount(String repayCount) {
        this.repayCount = repayCount;
    }

    public String getReceivePrice() {
        return receivePrice;
    }

    public void setReceivePrice(String receivePrice) {
        this.receivePrice = receivePrice;
    }

    public String getRepayPrice() {
        return repayPrice;
    }

    public void setRepayPrice(String repayPrice) {
        this.repayPrice = repayPrice;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "WeixinAssetMessageBean{" +
                "id='" + id + '\'' +
                ", nickname='" + nickname + '\'' +
                ", points='" + points + '\'' +
                ", nowTime='" + nowTime + '\'' +
                ", manageRate='" + manageRate + '\'' +
                ", interestTotal='" + interestTotal + '\'' +
                ", balance='" + balance + '\'' +
                ", dsPrice='" + dsPrice + '\'' +
                ", dhPrice='" + dhPrice + '\'' +
                ", accountPrice='" + accountPrice + '\'' +
                ", investTotal='" + investTotal + '\'' +
                ", attornTotal='" + attornTotal + '\'' +
                ", productTotal='" + productTotal + '\'' +
                ", rlStr='" + rlStr + '\'' +
                ", year='" + year + '\'' +
                ", month='" + month + '\'' +
                ", day='" + day + '\'' +
                ", receiveCount='" + receiveCount + '\'' +
                ", repayCount='" + repayCount + '\'' +
                ", receivePrice='" + receivePrice + '\'' +
                ", repayPrice='" + repayPrice + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
