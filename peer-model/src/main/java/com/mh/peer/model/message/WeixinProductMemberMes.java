package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/7/7.
 */
public class WeixinProductMemberMes {

    /** 主键 **/
    private String id;
    /** 申请人 **/
    private String applymember;
    /** 是否满标(0-是、1-否) **/
    private String fullscale;
    /** 审核状态(0-审核通过、1-审核未通过、2-未审核) **/
    private String shflag;
    /**  **/
    private String loadfee;
    /** 真实姓名 **/
    private String realname;
    /** 性别(0-男、1-女) **/
    private String sex;
    /** 学历(0-小学、1-初中、2-高中、3-大专、4-本科、5-硕士、6-博士、7-博士后、8-其他) **/
    private String dregree;
    /** 手机号 **/
    private String telephone;
    /** 子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上) **/
    private String children;
    /** 是否有车(0-有、1-无) **/
    private String carFlg;
    /** 社保情况(0-无、1-未缴满6个月、2-缴满6个月以上) **/
    private String socialSecurity;
    /** 婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶) **/
    private String marryStatus;
    /** 住房条件(0-有商品房(无贷款)、1-有商品房(有贷款)、2-有其他(非商品)房、3-与父母同住、4-租房) **/
    private String houseCondition;
    /** 月收入(0-2000元以下、1-2000元-5000元、2-5000元-10000元、3-10000元以上) **/
    private String monthSalary;
    /** 邮箱地址 **/
    private String email;
    /** 详细地址 **/
    private String detailAddress;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApplymember() {
        return applymember;
    }

    public void setApplymember(String applymember) {
        this.applymember = applymember;
    }

    public String getFullscale() {
        return fullscale;
    }

    public void setFullscale(String fullscale) {
        this.fullscale = fullscale;
    }

    public String getShflag() {
        return shflag;
    }

    public void setShflag(String shflag) {
        this.shflag = shflag;
    }

    public String getLoadfee() {
        return loadfee;
    }

    public void setLoadfee(String loadfee) {
        this.loadfee = loadfee;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDregree() {
        return dregree;
    }

    public void setDregree(String dregree) {
        this.dregree = dregree;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getCarFlg() {
        return carFlg;
    }

    public void setCarFlg(String carFlg) {
        this.carFlg = carFlg;
    }

    public String getSocialSecurity() {
        return socialSecurity;
    }

    public void setSocialSecurity(String socialSecurity) {
        this.socialSecurity = socialSecurity;
    }

    public String getMarryStatus() {
        return marryStatus;
    }

    public void setMarryStatus(String marryStatus) {
        this.marryStatus = marryStatus;
    }

    public String getHouseCondition() {
        return houseCondition;
    }

    public void setHouseCondition(String houseCondition) {
        this.houseCondition = houseCondition;
    }

    public String getMonthSalary() {
        return monthSalary;
    }

    public void setMonthSalary(String monthSalary) {
        this.monthSalary = monthSalary;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    @Override
    public String toString() {
        return "WeixinProductMemberMes{" +
                "id='" + id + '\'' +
                ", applymember='" + applymember + '\'' +
                ", fullscale='" + fullscale + '\'' +
                ", shflag='" + shflag + '\'' +
                ", loadfee='" + loadfee + '\'' +
                ", realname='" + realname + '\'' +
                ", sex='" + sex + '\'' +
                ", dregree='" + dregree + '\'' +
                ", telephone='" + telephone + '\'' +
                ", children='" + children + '\'' +
                ", carFlg='" + carFlg + '\'' +
                ", socialSecurity='" + socialSecurity + '\'' +
                ", marryStatus='" + marryStatus + '\'' +
                ", houseCondition='" + houseCondition + '\'' +
                ", monthSalary='" + monthSalary + '\'' +
                ", email='" + email + '\'' +
                ", detailAddress='" + detailAddress + '\'' +
                '}';
    }
}
