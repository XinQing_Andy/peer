package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-9-1.
 * 已投资(购买)的债权
 */
public class AttornInvestedMessageBean {

    /** 债权主键 **/
    private String id;
    /** 投资主键 **/
    private String buyId;
    /** 产品主键 **/
    private String productId;
    /** 题目 **/
    private String title;
    /** 编码 **/
    private String code;
    /** 年利率 **/
    private String loanRate;
    /** 待收本息 **/
    private String price;
    /** 债权承接金额 **/
    private String underPrice;
    /** 债权承接时间 **/
    private String underTime;
    /** 审核状态 **/
    private String shFlag;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getBuyId() {
        return buyId;
    }
    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnderPrice() {
        return underPrice;
    }
    public void setUnderPrice(String underPrice) {
        this.underPrice = underPrice;
    }

    public String getUnderTime() {
        return underTime;
    }
    public void setUnderTime(String underTime) {
        this.underTime = underTime;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    @Override
    public String toString() {
        return "AttornInvestedMessageBean{" +
                "id='" + id + '\'' +
                ", buyId='" + buyId + '\'' +
                ", productId='" + productId + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", price='" + price + '\'' +
                ", underPrice='" + underPrice + '\'' +
                ", underTime='" + underTime + '\'' +
                ", shFlag='" + shFlag + '\'' +
                '}';
    }
}
