package com.mh.peer.model.message;

import java.util.List;

/**
 * Created by Cuibin on 2016/4/12.
 */
public class MemberFeeLevelMessageBean {
    /** 主键 **/
    private int id;
    /** 费用名称 **/
    private String feeName;
    /** 是否启用 **/
    private String activeFlg;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /**  **/
    private String jetxName;

    private String menuUrl;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;

    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;

    private String title;
    /**  **/
    private String bName;
    /**  **/
    private List<MemberFeeLevelMessageBean> list;
    /**  **/
    private int count;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public String getActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(String activeFlg) {
        this.activeFlg = activeFlg;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }

    public List<MemberFeeLevelMessageBean> getList() {
        return list;
    }

    public void setList(List<MemberFeeLevelMessageBean> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "MemberFeeLevelMessageBean{" +
                "id=" + id +
                ", feeName='" + feeName + '\'' +
                ", activeFlg='" + activeFlg + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", jetxName='" + jetxName + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", title='" + title + '\'' +
                ", bName='" + bName + '\'' +
                ", list=" + list +
                ", count=" + count +
                '}';
    }
}
