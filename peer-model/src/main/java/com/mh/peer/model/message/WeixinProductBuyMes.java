package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/7/7.
 */
public class WeixinProductBuyMes {

    /** 主键 **/
    private String id;
    /** 产品id **/
    private String productid;
    /** 购买人id **/
    private String buyer;
    /**  **/
    private String buyername;
    /** 购买金额 **/
    private String price;
    /** 购买时间 **/
    private String buytime;
    /** 类型 0-持有 1-转让中 2-已转让 **/
    private String type;
    /** 操作时间 **/
    private String createtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getBuyername() {
        return buyername;
    }

    public void setBuyername(String buyername) {
        this.buyername = buyername;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBuytime() {
        return buytime;
    }

    public void setBuytime(String buytime) {
        this.buytime = buytime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    @Override
    public String toString() {
        return "WeixinProductBuyMes{" +
                "id='" + id + '\'' +
                ", productid='" + productid + '\'' +
                ", buyer='" + buyer + '\'' +
                ", buyername='" + buyername + '\'' +
                ", price='" + price + '\'' +
                ", buytime='" + buytime + '\'' +
                ", type='" + type + '\'' +
                ", createtime='" + createtime + '\'' +
                '}';
    }
}
