package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/5/19.
 */
public class SysMenuMessageBean {
    /** ID主键 **/
    private int id;
    /** 父级菜单id **/
    private String parentId;
    /** 菜单名称 **/
    private String menuName;
    /** 菜单的URL **/
    private String menuUrl;
    /** 控制类的URL **/
    private String controlUrl;
    /** 图标样式 **/
    private String iconStyle;
    /** 建立菜单人员 **/
    private String createUserId;
    /** 建立菜单时间 **/
    private String createTime;
    /** 是否被删除（0：未删除；1：已逻辑删除） **/
    private String isDelete;
    /** 状态（0：启用；1：禁用） **/
    private String state;
    /** 菜单层级 **/
    private String levelMenu;
    /** 菜单序号 **/
    private int menuSeq;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getControlUrl() {
        return controlUrl;
    }

    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    public String getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(String iconStyle) {
        this.iconStyle = iconStyle;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLevelMenu() {
        return levelMenu;
    }

    public void setLevelMenu(String levelMenu) {
        this.levelMenu = levelMenu;
    }

    public int getMenuSeq() {
        return menuSeq;
    }

    public void setMenuSeq(int menuSeq) {
        this.menuSeq = menuSeq;
    }

    @Override
    public String toString() {
        return "SysMenuMessageBean{" +
                "id=" + id +
                ", parentId='" + parentId + '\'' +
                ", menuName='" + menuName + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", controlUrl='" + controlUrl + '\'' +
                ", iconStyle='" + iconStyle + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createTime='" + createTime + '\'' +
                ", isDelete='" + isDelete + '\'' +
                ", state='" + state + '\'' +
                ", levelMenu='" + levelMenu + '\'' +
                ", menuSeq=" + menuSeq +
                '}';
    }
}
