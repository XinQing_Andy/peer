package com.mh.peer.model.message;

public class ProductAttornRecordMessageBean {

    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;
    /** 模板名称 **/
    private String jetxName;
    /** 二级菜单URL **/
    private String menuUrl;
    /** 主键 **/
    private String id;
    /** 产品id **/
    private String productbuyId;
    /** 价格 **/
    private String price;
    /** 审核状态 **/
    private String shFlag;
    /** 审核意见 **/
    private String shyj;
    /** 承接人 **/
    private String undertakeMember;
    /** 承接时间 **/
    private String undertakeTime;
    /** 创建时间 **/
    private String createTime;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 预期收益率 **/
    private String expectedrate;
    /** 产品id **/
    private String productid;
    /** 题目 **/
    private String title;
    /** 待收本息 **/
    private String waitprice;
    /** 到期时间 **/
    private String endtime;
    /** 剩余日期(天) **/
    private String surplusdays;
    /** 剩余期数 **/
    private String surplusperiods;
    /** 到期起时间 **/
    private String expireStartTime;
    /** 到期止时间 **/
    private String expireEndTime;
    /** 承接时间起 **/
    private String undertaketimeStart;
    /** 承接时间止 **/
    private String undertaketimeEnd;
    /** 产品类型 **/
    private String type;
    /** 投资人 **/
    private String buyer;
    private String isSy;//是否第一次跳转
    /** 待收本息 **/
    private String waitPrice;
    /** 止日期 **/
    private String endTime;
    /** 回款状态 **/
    private String receiveFlag;
    /** 贷款利率 **/
    private String loanrate;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 产品编码**/
    private String code;
    /**债权转让条件**/
    private String trbutton1;
    private String trbutton2;
    private String trbutton3;
    private String trbutton4;
    /** 是否多选(0-多选、1-单选) **/
    private String selectMutli;
    /** 类型名称 **/
    private String typeName;
    /** 验证码 **/
    private String captcha;
    /** 会员信息 **/
    private MemberInfoMessageBean memberInfoMessageBean;
    /** 余额 **/
    private String balance;
    /** 满标日期 **/
    private String fullTime;
    /** 借款金额 **/
    private String loadFee;
    /** 借款期限 **/
    private String loadLength;
    /** 剩余本金 **/
    private String waitingPricipal;
    /** 图片地址 **/
    private String imgUrl;
    /** 列表部分拼接 **/
    private String str;
    /** 分页部分拼接 **/
    private String pageStr;
    /** 转让起时间 **/
    private String attornStartTime;
    /** 转让止时间 **/
    private String attornEndTime;


    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductbuyId() {
        return productbuyId;
    }
    public void setProductbuyId(String productbuyId) {
        this.productbuyId = productbuyId;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getShyj() {
        return shyj;
    }
    public void setShyj(String shyj) {
        this.shyj = shyj;
    }

    public String getUndertakeMember() {
        return undertakeMember;
    }
    public void setUndertakeMember(String undertakeMember) {
        this.undertakeMember = undertakeMember;
    }

    public String getUndertakeTime() {
        return undertakeTime;
    }
    public void setUndertakeTime(String undertakeTime) {
        this.undertakeTime = undertakeTime;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderColumn() {
        return orderColumn;
    }

    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getExpectedrate() {
        return expectedrate;
    }

    public void setExpectedrate(String expectedrate) {
        this.expectedrate = expectedrate;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWaitprice() {
        return waitprice;
    }

    public void setWaitprice(String waitprice) {
        this.waitprice = waitprice;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getSurplusdays() {
        return surplusdays;
    }

    public void setSurplusdays(String surplusdays) {
        this.surplusdays = surplusdays;
    }

    public String getSurplusperiods() {
        return surplusperiods;
    }

    public void setSurplusperiods(String surplusperiods) {
        this.surplusperiods = surplusperiods;
    }

    public String getExpireStartTime() {
        return expireStartTime;
    }

    public void setExpireStartTime(String expireStartTime) {
        this.expireStartTime = expireStartTime;
    }

    public String getExpireEndTime() {
        return expireEndTime;
    }

    public void setExpireEndTime(String expireEndTime) {
        this.expireEndTime = expireEndTime;
    }

    public String getUndertaketimeStart() {
        return undertaketimeStart;
    }

    public void setUndertaketimeStart(String undertaketimeStart) {
        this.undertaketimeStart = undertaketimeStart;
    }

    public String getUndertaketimeEnd() {
        return undertaketimeEnd;
    }

    public void setUndertaketimeEnd(String undertaketimeEnd) {
        this.undertaketimeEnd = undertaketimeEnd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getIsSy() {
        return isSy;
    }

    public void setIsSy(String isSy) {
        this.isSy = isSy;
    }

    public String getWaitPrice() {
        return waitPrice;
    }

    public void setWaitPrice(String waitPrice) {
        this.waitPrice = waitPrice;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getReceiveFlag() {
        return receiveFlag;
    }

    public void setReceiveFlag(String receiveFlag) {
        this.receiveFlag = receiveFlag;
    }

    public String getLoanrate() {
        return loanrate;
    }

    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getTrbutton1() {
        return trbutton1;
    }
    public void setTrbutton1(String trbutton1) {
        this.trbutton1 = trbutton1;
    }

    public String getTrbutton2() {
        return trbutton2;
    }
    public void setTrbutton2(String trbutton2) {
        this.trbutton2 = trbutton2;
    }

    public String getTrbutton3() {
        return trbutton3;
    }
    public void setTrbutton3(String trbutton3) {
        this.trbutton3 = trbutton3;
    }

    public String getTrbutton4() {
        return trbutton4;
    }
    public void setTrbutton4(String trbutton4) {
        this.trbutton4 = trbutton4;
    }

    public String getSelectMutli() {
        return selectMutli;
    }

    public void setSelectMutli(String selectMutli) {
        this.selectMutli = selectMutli;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public MemberInfoMessageBean getMemberInfoMessageBean() {
        return memberInfoMessageBean;
    }

    public void setMemberInfoMessageBean(MemberInfoMessageBean memberInfoMessageBean) {
        this.memberInfoMessageBean = memberInfoMessageBean;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getFullTime() {
        return fullTime;
    }

    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getLoadFee() {
        return loadFee;
    }

    public void setLoadFee(String loadFee) {
        this.loadFee = loadFee;
    }

    public String getLoadLength() {
        return loadLength;
    }

    public void setLoadLength(String loadLength) {
        this.loadLength = loadLength;
    }

    public String getWaitingPricipal() {
        return waitingPricipal;
    }
    public void setWaitingPricipal(String waitingPricipal) {
        this.waitingPricipal = waitingPricipal;
    }

    public String getImgUrl() {
        return imgUrl;
    }
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getStr() {
        return str;
    }
    public void setStr(String str) {
        this.str = str;
    }

    public String getPageStr() {
        return pageStr;
    }
    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    public String getAttornStartTime() {
        return attornStartTime;
    }
    public void setAttornStartTime(String attornStartTime) {
        this.attornStartTime = attornStartTime;
    }

    public String getAttornEndTime() {
        return attornEndTime;
    }
    public void setAttornEndTime(String attornEndTime) {
        this.attornEndTime = attornEndTime;
    }

    @Override
    public String toString() {
        return "ProductAttornRecordMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", jetxName='" + jetxName + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", id='" + id + '\'' +
                ", productbuyId='" + productbuyId + '\'' +
                ", price='" + price + '\'' +
                ", shFlag='" + shFlag + '\'' +
                ", shyj='" + shyj + '\'' +
                ", undertakeMember='" + undertakeMember + '\'' +
                ", undertakeTime='" + undertakeTime + '\'' +
                ", createTime='" + createTime + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", expectedrate='" + expectedrate + '\'' +
                ", productid='" + productid + '\'' +
                ", title='" + title + '\'' +
                ", waitprice='" + waitprice + '\'' +
                ", endtime='" + endtime + '\'' +
                ", surplusdays='" + surplusdays + '\'' +
                ", surplusperiods='" + surplusperiods + '\'' +
                ", expireStartTime='" + expireStartTime + '\'' +
                ", expireEndTime='" + expireEndTime + '\'' +
                ", undertaketimeStart='" + undertaketimeStart + '\'' +
                ", undertaketimeEnd='" + undertaketimeEnd + '\'' +
                ", type='" + type + '\'' +
                ", buyer='" + buyer + '\'' +
                ", isSy='" + isSy + '\'' +
                ", waitPrice='" + waitPrice + '\'' +
                ", endTime='" + endTime + '\'' +
                ", receiveFlag='" + receiveFlag + '\'' +
                ", loanrate='" + loanrate + '\'' +
                ", repayment='" + repayment + '\'' +
                ", code='" + code + '\'' +
                ", trbutton1='" + trbutton1 + '\'' +
                ", trbutton2='" + trbutton2 + '\'' +
                ", trbutton3='" + trbutton3 + '\'' +
                ", trbutton4='" + trbutton4 + '\'' +
                ", selectMutli='" + selectMutli + '\'' +
                ", typeName='" + typeName + '\'' +
                ", captcha='" + captcha + '\'' +
                ", memberInfoMessageBean=" + memberInfoMessageBean +
                ", balance='" + balance + '\'' +
                ", fullTime='" + fullTime + '\'' +
                ", loadFee='" + loadFee + '\'' +
                ", loadLength='" + loadLength + '\'' +
                ", waitingPricipal='" + waitingPricipal + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", str='" + str + '\'' +
                '}';
    }
}
