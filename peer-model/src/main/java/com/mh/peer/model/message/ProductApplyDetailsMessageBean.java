package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-9-9.
 * 已申请项目详情
 */
public class ProductApplyDetailsMessageBean {

    /** 回款id **/
    private String repayId;
    /** 已还金额 **/
    private String yRepayPrice;
    /** 待还金额 **/
    private String dRepayPrice;
    /** 已还本金 **/
    private String yRepayPrincipal;
    /** 待还本金 **/
    private String dRepayPrincipal;
    /** 已还利息 **/
    private String yRepayInterest;
    /** 待还利息 **/
    private String dRepayInterest;
    /** 应还款金额 **/
    private String yprice;
    /** 管理费 **/
    private String managePrice;
    /** 应还时间 **/
    private String yRepayTime;
    /** 待还时间 **/
    private String dRepayTime;

    public String getRepayId() {
        return repayId;
    }
    public void setRepayId(String repayId) {
        this.repayId = repayId;
    }

    public String getyRepayPrice() {
        return yRepayPrice;
    }
    public void setyRepayPrice(String yRepayPrice) {
        this.yRepayPrice = yRepayPrice;
    }

    public String getdRepayPrice() {
        return dRepayPrice;
    }
    public void setdRepayPrice(String dRepayPrice) {
        this.dRepayPrice = dRepayPrice;
    }

    public String getyRepayPrincipal() {
        return yRepayPrincipal;
    }
    public void setyRepayPrincipal(String yRepayPrincipal) {
        this.yRepayPrincipal = yRepayPrincipal;
    }

    public String getdRepayPrincipal() {
        return dRepayPrincipal;
    }
    public void setdRepayPrincipal(String dRepayPrincipal) {
        this.dRepayPrincipal = dRepayPrincipal;
    }

    public String getyRepayInterest() {
        return yRepayInterest;
    }
    public void setyRepayInterest(String yRepayInterest) {
        this.yRepayInterest = yRepayInterest;
    }

    public String getdRepayInterest() {
        return dRepayInterest;
    }
    public void setdRepayInterest(String dRepayInterest) {
        this.dRepayInterest = dRepayInterest;
    }

    public String getYprice() {
        return yprice;
    }
    public void setYprice(String yprice) {
        this.yprice = yprice;
    }

    public String getManagePrice() {
        return managePrice;
    }
    public void setManagePrice(String managePrice) {
        this.managePrice = managePrice;
    }

    public String getyRepayTime() {
        return yRepayTime;
    }
    public void setyRepayTime(String yRepayTime) {
        this.yRepayTime = yRepayTime;
    }

    public String getdRepayTime() {
        return dRepayTime;
    }
    public void setdRepayTime(String dRepayTime) {
        this.dRepayTime = dRepayTime;
    }

    @Override
    public String toString() {
        return "ProductApplyDetailsMessageBean{" +
                "repayId='" + repayId + '\'' +
                ", yRepayPrice='" + yRepayPrice + '\'' +
                ", dRepayPrice='" + dRepayPrice + '\'' +
                ", yRepayPrincipal='" + yRepayPrincipal + '\'' +
                ", dRepayPrincipal='" + dRepayPrincipal + '\'' +
                ", yRepayInterest='" + yRepayInterest + '\'' +
                ", dRepayInterest='" + dRepayInterest + '\'' +
                ", yprice='" + yprice + '\'' +
                ", managePrice='" + managePrice + '\'' +
                ", yRepayTime='" + yRepayTime + '\'' +
                ", dRepayTime='" + dRepayTime + '\'' +
                '}';
    }
}
