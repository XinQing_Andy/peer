package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-7.
 */
public class AppProductMemberMessageBean {

    /** 产品主键 **/
    private String id;
    /** 是否满标(0-是、1-否) **/
    private String fullScale;
    /** 贷款金额 **/
    private String loadFee;
    /** 产品申请人id **/
    private String applyMember;
    /** 真实姓名 **/
    private String realName;
    /** 性别 **/
    private String sex;
    /** 学历 **/
    private String dregree;
    /** 电话 **/
    private String telephone;
    /** 子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上) **/
    private String children;
    /** 是否有车 **/
    private String carFlg;
    /** 社保情况 **/
    private String socailSecurity;
    /** 婚姻状态 **/
    private String marryStatus;
    /** 住房条件 **/
    private String houseCondition;
    /** 月收入 **/
    private String monthSalary;
    /** 邮箱 **/
    private String email;
    /** 详细地址 **/
    private String detailAddress;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getFullScale() {
        return fullScale;
    }
    public void setFullScale(String fullScale) {
        this.fullScale = fullScale;
    }

    public String getLoadFee() {
        return loadFee;
    }
    public void setLoadFee(String loadFee) {
        this.loadFee = loadFee;
    }

    public String getApplyMember() {
        return applyMember;
    }
    public void setApplyMember(String applyMember) {
        this.applyMember = applyMember;
    }

    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDregree() {
        return dregree;
    }
    public void setDregree(String dregree) {
        this.dregree = dregree;
    }

    public String getTelephone() {
        return telephone;
    }
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getChildren() {
        return children;
    }
    public void setChildren(String children) {
        this.children = children;
    }

    public String getCarFlg() {
        return carFlg;
    }
    public void setCarFlg(String carFlg) {
        this.carFlg = carFlg;
    }

    public String getSocailSecurity() {
        return socailSecurity;
    }
    public void setSocailSecurity(String socailSecurity) {
        this.socailSecurity = socailSecurity;
    }

    public String getMarryStatus() {
        return marryStatus;
    }
    public void setMarryStatus(String marryStatus) {
        this.marryStatus = marryStatus;
    }

    public String getHouseCondition() {
        return houseCondition;
    }
    public void setHouseCondition(String houseCondition) {
        this.houseCondition = houseCondition;
    }

    public String getMonthSalary() {
        return monthSalary;
    }
    public void setMonthSalary(String monthSalary) {
        this.monthSalary = monthSalary;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getDetailAddress() {
        return detailAddress;
    }
    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    @Override
    public String toString() {
        return "AppProductMemberMessageBean{" +
                "id='" + id + '\'' +
                ", fullScale='" + fullScale + '\'' +
                ", loadFee='" + loadFee + '\'' +
                ", applyMember='" + applyMember + '\'' +
                ", realName='" + realName + '\'' +
                ", sex='" + sex + '\'' +
                ", dregree='" + dregree + '\'' +
                ", telephone='" + telephone + '\'' +
                ", children='" + children + '\'' +
                ", carFlg='" + carFlg + '\'' +
                ", socailSecurity='" + socailSecurity + '\'' +
                ", marryStatus='" + marryStatus + '\'' +
                ", houseCondition='" + houseCondition + '\'' +
                ", monthSalary='" + monthSalary + '\'' +
                ", email='" + email + '\'' +
                ", detailAddress='" + detailAddress + '\'' +
                '}';
    }
}
