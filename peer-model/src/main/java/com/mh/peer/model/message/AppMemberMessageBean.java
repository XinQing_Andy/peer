package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-6-20.
 */
public class AppMemberMessageBean {

    /** 信息 **/
    private String info;
    /** 返回标志 **/
    private String resultCode;
    /** 会员id **/
    private String id;
    /** 会员姓名 **/
    private String memberUserName;
    /** 密码 **/
    private String password;
    /** 手机 **/
    private String telephone;
    /** 登录类型 **/
    private String loginType;
    /** 是否删除(0-是、1-否) **/
    private String delFlg;

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getResultCode() {
        return resultCode;
    }
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getMemberUserName() {
        return memberUserName;
    }
    public void setMemberUserName(String memberUserName) {
        this.memberUserName = memberUserName;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getLoginType() {
        return loginType;
    }
    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getDelFlg() {
        return delFlg;
    }
    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    @Override
    public String toString() {
        return "AppMemberMessageBean{" +
                "info='" + info + '\'' +
                ", resultCode='" + resultCode + '\'' +
                ", id='" + id + '\'' +
                ", memberUserName='" + memberUserName + '\'' +
                ", password='" + password + '\'' +
                ", telephone='" + telephone + '\'' +
                ", loginType='" + loginType + '\'' +
                '}';
    }
}
