package com.mh.peer.model.message;

/**
 * 安全问题
 * Created by zhangerxin on 2016-4-1.
 */
public class SysSecurityQuestionMessageBean {

    /*
    * 返回成功或失败(error,success)
    * */
    private String result;
    /*
     *  错误消息
     * */
    private String info;


    /**返回主键**/
    private String id;
    /**返回问题**/
    private String question;
    /**返回是否启用**/
    private String active_flg;
    /**问题格式**/
    private String format;
    /**创建用户**/
    private String createUser;
    /**创建时间**/
    private String createTime;
    /**更新用户**/
    private String updateUser;
    /**最后更新时间**/
    private String updateTime;
    /**二级菜单URL**/
    private String menuUrl;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 序号 **/
    private String num;
    /** 当前页 **/
    private String currentPage;



    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }

    public String getActive_flg() {
        return active_flg;
    }
    public void setActive_flg(String active_flg) {
        this.active_flg = active_flg;
    }

    public String getFormat() {
        return format;
    }
    public void setFormat(String format) {
        this.format = format;
    }

    public String getCreateUser() {
        return createUser;
    }
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public String toString() {
        return "SysSecurityQuestionMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", question='" + question + '\'' +
                ", active_flg='" + active_flg + '\'' +
                ", format='" + format + '\'' +
                ", createUser='" + createUser + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateUser='" + updateUser + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", num='" + num + '\'' +
                ", currentPage='" + currentPage + '\'' +
                '}';
    }
}
