package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/6/29.
 */
public class ProductAttornDetailsMes {

    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String productbuyid;
    /** 转让金额 **/
    private String price;
    /** 预期收益率 **/
    private String expectedrate;
    /** 审核状态(0-审核通过、1-审核不通过、2-待审核) **/
    private String shflag;
    /** 审核意见 **/
    private String shyj;
    /** 承接人id **/
    private String undertakemember;
    /** 承接时间 **/
    private String undertaketime;
    /** 创建时间 **/
    private String createtime;
    /** 购买人id **/
    private String buyer;
    /** 产品id **/
    private String productid;
    /** 题目 **/
    private String title;
    /** 借款标编号 **/
    private String code;
    /** 贷款利率 **/
    private String loanrate;
    /** 类型id **/
    private String type;
    /** 满标日期 **/
    private String fulltime;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 贷款期限(年) **/
    private String loanyear;
    /** 贷款期限(月) **/
    private String loanmonth;
    /** 贷款期限(日) **/
    private String loadday;
    /** 借款金额 **/
    private String loadfee;
    /** 申请人 **/
    private String applymember;
    /** 真实姓名 **/
    private String realname;
    /** 性别(0-男、1-女) **/
    private String sex;
    /** 手机号 **/
    private String telephone;
    /** 邮箱地址 **/
    private String email;
    /** 学历(0-小学、1-初中、2-高中、3-大专、4-本科、5-硕士、6-博士、7-博士后、8-其他) **/
    private String dregree;
    /** 婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶) **/
    private String marryStatus;
    /** 子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上) **/
    private String children;
    /** 月收入(0-2000元以下、1-2000元-5000元、2-5000元-10000元、3-10000元以上) **/
    private String monthSalary;
    /** 社保情况(0-无、1-未缴满6个月、2-缴满6个月以上) **/
    private String socialSecurity;
    /** 住房条件(0-有商品房(无贷款)、1-有商品房(有贷款)、2-有其他(非商品)房、3-与父母同住、4-租房) **/
    private String houseCondition;
    /** 是否有车(0-有、1-无) **/
    private String carFlg;
    /** 详细地址 **/
    private String detailAddress;
    /** 账户余额 **/
    private String balance;
    /** 待收本息 **/
    private String waitprice;
    /** 到期时间 **/
    private String endtime;
    /** 剩余日期 **/
    private String surplusdays;
    /** 剩余期数 **/
    private String surplusperiods;
    /** 待收本金 **/
    private String waitingpricipal;
    /** 借款人条件说明 **/
    private String applycondition;
    /** 投资人姓名 **/
    private String buyname;
    /** 竞拍出价 **/
    private String auctionPrice;
    /** 竞拍时间 **/
    private String buytime;
    /** 竞拍状态 **/
    private String receiveflag;

    /** 会员昵称 **/
    private String nickname;
    /** 信用积分 **/
    private String points;
    /** 借款期限 **/
    private String loadLength;
    /** 发标日期 **/
    private String fullTime;
    /** 适合人群 **/
    private String crowds;
    /** 担保人 **/
    private String sponsor;
    /**额度范围(起始)**/
    private String creditstart;
    /**额度范围(截止)**/
    private String creditend;
    /**产品投标进度**/
    private String investprogress;
    /**产品投资金额总和**/
    private String investprice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductbuyid() {
        return productbuyid;
    }

    public void setProductbuyid(String productbuyid) {
        this.productbuyid = productbuyid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getExpectedrate() {
        return expectedrate;
    }

    public void setExpectedrate(String expectedrate) {
        this.expectedrate = expectedrate;
    }

    public String getShflag() {
        return shflag;
    }

    public void setShflag(String shflag) {
        this.shflag = shflag;
    }

    public String getShyj() {
        return shyj;
    }

    public void setShyj(String shyj) {
        this.shyj = shyj;
    }

    public String getUndertakemember() {
        return undertakemember;
    }

    public void setUndertakemember(String undertakemember) {
        this.undertakemember = undertakemember;
    }

    public String getUndertaketime() {
        return undertaketime;
    }

    public void setUndertaketime(String undertaketime) {
        this.undertaketime = undertaketime;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLoanrate() {
        return loanrate;
    }

    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFulltime() {
        return fulltime;
    }

    public void setFulltime(String fulltime) {
        this.fulltime = fulltime;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getLoanyear() {
        return loanyear;
    }

    public void setLoanyear(String loanyear) {
        this.loanyear = loanyear;
    }

    public String getLoanmonth() {
        return loanmonth;
    }

    public void setLoanmonth(String loanmonth) {
        this.loanmonth = loanmonth;
    }

    public String getLoadday() {
        return loadday;
    }

    public void setLoadday(String loadday) {
        this.loadday = loadday;
    }

    public String getLoadfee() {
        return loadfee;
    }

    public void setLoadfee(String loadfee) {
        this.loadfee = loadfee;
    }

    public String getApplymember() {
        return applymember;
    }

    public void setApplymember(String applymember) {
        this.applymember = applymember;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDregree() {
        return dregree;
    }

    public void setDregree(String dregree) {
        this.dregree = dregree;
    }

    public String getMarryStatus() {
        return marryStatus;
    }

    public void setMarryStatus(String marryStatus) {
        this.marryStatus = marryStatus;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getMonthSalary() {
        return monthSalary;
    }

    public void setMonthSalary(String monthSalary) {
        this.monthSalary = monthSalary;
    }

    public String getSocialSecurity() {
        return socialSecurity;
    }

    public void setSocialSecurity(String socialSecurity) {
        this.socialSecurity = socialSecurity;
    }

    public String getHouseCondition() {
        return houseCondition;
    }

    public void setHouseCondition(String houseCondition) {
        this.houseCondition = houseCondition;
    }

    public String getCarFlg() {
        return carFlg;
    }

    public void setCarFlg(String carFlg) {
        this.carFlg = carFlg;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getWaitprice() {
        return waitprice;
    }

    public void setWaitprice(String waitprice) {
        this.waitprice = waitprice;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getSurplusdays() {
        return surplusdays;
    }

    public void setSurplusdays(String surplusdays) {
        this.surplusdays = surplusdays;
    }

    public String getSurplusperiods() {
        return surplusperiods;
    }

    public void setSurplusperiods(String surplusperiods) {
        this.surplusperiods = surplusperiods;
    }

    public String getWaitingpricipal() {
        return waitingpricipal;
    }

    public void setWaitingpricipal(String waitingpricipal) {
        this.waitingpricipal = waitingpricipal;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getLoadLength() {
        return loadLength;
    }

    public void setLoadLength(String loadLength) {
        this.loadLength = loadLength;
    }

    public String getFullTime() {
        return fullTime;
    }

    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getCrowds() {
        return crowds;
    }

    public void setCrowds(String crowds) {
        this.crowds = crowds;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getCreditstart() {
        return creditstart;
    }

    public void setCreditstart(String creditstart) {
        this.creditstart = creditstart;
    }

    public String getCreditend() {
        return creditend;
    }

    public void setCreditend(String creditend) {
        this.creditend = creditend;
    }

    public String getInvestprogress() {
        return investprogress;
    }

    public void setInvestprogress(String investprogress) {
        this.investprogress = investprogress;
    }

    public String getInvestprice() {
        return investprice;
    }

    public void setInvestprice(String investprice) {
        this.investprice = investprice;
    }

    public String getApplycondition() {
        return applycondition;
    }

    public void setApplycondition(String applycondition) {
        this.applycondition = applycondition;
    }

    public String getBuyname() {
        return buyname;
    }

    public void setBuyname(String buyname) {
        this.buyname = buyname;
    }

    public String getAuctionPrice() {
        return auctionPrice;
    }

    public void setAuctionPrice(String auctionPrice) {
        this.auctionPrice = auctionPrice;
    }

    public String getBuytime() {
        return buytime;
    }

    public void setBuytime(String buytime) {
        this.buytime = buytime;
    }

    public String getReceiveflag() {
        return receiveflag;
    }

    public void setReceiveflag(String receiveflag) {
        this.receiveflag = receiveflag;
    }
}
