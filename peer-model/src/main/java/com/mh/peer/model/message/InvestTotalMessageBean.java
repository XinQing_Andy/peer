package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/6/13.
 * 投资统计消息对象
 */
public class InvestTotalMessageBean {
    //平均受益率
    private String avgBenefit;
    //投资总额
    private String investTotal;
    //投标中本金
    private String inaBid;
    //待回收本金
    private String toBeRecycled;
    //已回收的本金
    private String retired;

    //投标中本金 百分比
    private String inaBidPercent;
    //待回收本金 百分比
    private String toBeRecycledPercent;
    //已回收的本金 百分比
    private String retiredPercent;

    //待回收本金
    private String principalRecycled;
    //待回收利息
    private String interestRecycled;
    //待回收总额
    private String moneyRecycled;

    public String getAvgBenefit() {
        return avgBenefit;
    }

    public void setAvgBenefit(String avgBenefit) {
        this.avgBenefit = avgBenefit;
    }

    public String getInvestTotal() {
        return investTotal;
    }

    public void setInvestTotal(String investTotal) {
        this.investTotal = investTotal;
    }

    public String getInaBid() {
        return inaBid;
    }

    public void setInaBid(String inaBid) {
        this.inaBid = inaBid;
    }

    public String getToBeRecycled() {
        return toBeRecycled;
    }

    public void setToBeRecycled(String toBeRecycled) {
        this.toBeRecycled = toBeRecycled;
    }

    public String getRetired() {
        return retired;
    }

    public void setRetired(String retired) {
        this.retired = retired;
    }

    public String getInaBidPercent() {
        return inaBidPercent;
    }

    public void setInaBidPercent(String inaBidPercent) {
        this.inaBidPercent = inaBidPercent;
    }

    public String getToBeRecycledPercent() {
        return toBeRecycledPercent;
    }

    public void setToBeRecycledPercent(String toBeRecycledPercent) {
        this.toBeRecycledPercent = toBeRecycledPercent;
    }

    public String getRetiredPercent() {
        return retiredPercent;
    }

    public void setRetiredPercent(String retiredPercent) {
        this.retiredPercent = retiredPercent;
    }

    public String getPrincipalRecycled() {
        return principalRecycled;
    }

    public void setPrincipalRecycled(String principalRecycled) {
        this.principalRecycled = principalRecycled;
    }

    public String getInterestRecycled() {
        return interestRecycled;
    }

    public void setInterestRecycled(String interestRecycled) {
        this.interestRecycled = interestRecycled;
    }

    public String getMoneyRecycled() {
        return moneyRecycled;
    }

    public void setMoneyRecycled(String moneyRecycled) {
        this.moneyRecycled = moneyRecycled;
    }
}
