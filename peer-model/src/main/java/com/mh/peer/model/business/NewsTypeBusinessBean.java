package com.mh.peer.model.business;

/**
 * Created by wanghongjian on 2016/3/25.
 */
public class NewsTypeBusinessBean {
    /**
     *返回成功或失败(error,success)
     */
    private String result;

    /**
     * 返回结果说明
     */
    private String info;


    /** 主键 **/
    private String id;
    /** 名称 **/
    private String name;
    /** 父级id **/
    private String fid;


    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getFid() {
        return fid;
    }
    public void setFid(String fid) {
        this.fid = fid;
    }

    @Override
    public String toString() {
        return "NewsTypeBusinessBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", fid='" + fid + '\'' +
                '}';
    }
}
