package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-5-19.
 */
public class SysListenerMessageBean {

    private String listenerTime; //监听时间

    public String getListenerTime() {
        return listenerTime;
    }
    public void setListenerTime(String listenerTime) {
        this.listenerTime = listenerTime;
    }

    @Override
    public String toString() {
        return "SysListenerMessageBean{" +
                "listenerTime='" + listenerTime + '\'' +
                '}';
    }
}
