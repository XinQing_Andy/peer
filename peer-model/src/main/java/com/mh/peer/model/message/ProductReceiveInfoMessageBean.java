package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-8-30.
 */
public class ProductReceiveInfoMessageBean {

    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String productBuyId;
    /** 应回款本金 **/
    private String principal;
    /** 应回款利息 **/
    private String interest;
    /** 应回款金额 **/
    private String yreceivementPrice;
    /** 应回款日期 **/
    private String yReceiveTime;
    /** 实际回款金额 **/
    private String receivementPrice;
    /** 实际还款利息 **/
    private String sinterest;
    /** 实际回款日期 **/
    private String sreceivementTime;
    /** 管理费 **/
    private String managePrice;
    /** 还款状态 **/
    private String receiveFlag;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductBuyId() {
        return productBuyId;
    }
    public void setProductBuyId(String productBuyId) {
        this.productBuyId = productBuyId;
    }

    public String getPrincipal() {
        return principal;
    }
    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getInterest() {
        return interest;
    }
    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getYreceivementPrice() {
        return yreceivementPrice;
    }
    public void setYreceivementPrice(String yreceivementPrice) {
        this.yreceivementPrice = yreceivementPrice;
    }

    public String getyReceiveTime() {
        return yReceiveTime;
    }
    public void setyReceiveTime(String yReceiveTime) {
        this.yReceiveTime = yReceiveTime;
    }

    public String getReceivementPrice() {
        return receivementPrice;
    }
    public void setReceivementPrice(String receivementPrice) {
        this.receivementPrice = receivementPrice;
    }

    public String getSinterest() {
        return sinterest;
    }
    public void setSinterest(String sinterest) {
        this.sinterest = sinterest;
    }

    public String getSreceivementTime() {
        return sreceivementTime;
    }
    public void setSreceivementTime(String sreceivementTime) {
        this.sreceivementTime = sreceivementTime;
    }

    public String getManagePrice() {
        return managePrice;
    }
    public void setManagePrice(String managePrice) {
        this.managePrice = managePrice;
    }

    public String getReceiveFlag() {
        return receiveFlag;
    }
    public void setReceiveFlag(String receiveFlag) {
        this.receiveFlag = receiveFlag;
    }

    @Override
    public String toString() {
        return "ProductReceiveInfoMessageBean{" +
                "id='" + id + '\'' +
                ", productBuyId='" + productBuyId + '\'' +
                ", principal='" + principal + '\'' +
                ", interest='" + interest + '\'' +
                ", yreceivementPrice='" + yreceivementPrice + '\'' +
                ", yReceiveTime='" + yReceiveTime + '\'' +
                ", receivementPrice='" + receivementPrice + '\'' +
                ", sinterest='" + sinterest + '\'' +
                ", sreceivementTime='" + sreceivementTime + '\'' +
                ", managePrice='" + managePrice + '\'' +
                ", receiveFlag='" + receiveFlag + '\'' +
                '}';
    }
}
