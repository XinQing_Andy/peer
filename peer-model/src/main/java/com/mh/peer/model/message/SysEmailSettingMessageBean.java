package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/4/25.
 */
public class SysEmailSettingMessageBean {
    /** 主键 **/
    private int id;
    /** 邮箱登录网址 **/
    private String address;
    /** E-mail **/
    private String email;
    /** 密码 **/
    private String password;
    /** POP3服务器 **/
    private String pop;
    /** SMTP服务器 **/
    private String smtp;
    /** 创建人 **/
    private String creatuser;
    /** 创建时间 **/
    private String createtime;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /**  **/
    private String menuUrl;
    /**  **/
    private String jetxName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPop() {
        return pop;
    }

    public void setPop(String pop) {
        this.pop = pop;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getCreatuser() {
        return creatuser;
    }

    public void setCreatuser(String creatuser) {
        this.creatuser = creatuser;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    @Override
    public String toString() {
        return "SysEmailSettingMessageBean{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", pop='" + pop + '\'' +
                ", smtp='" + smtp + '\'' +
                ", creatuser='" + creatuser + '\'' +
                ", createtime='" + createtime + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", jetxName='" + jetxName + '\'' +
                '}';
    }
}
