package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-9-18.
 */
public class ProductSaleMessageBean {

    /** 菜单 **/
    private String menuUrl;
    /** 产品类型id **/
    private String typeId;
    /** 产品名称 **/
    private String typeName;
    /** 放款数量 **/
    private String grantCount;
    /** 已借款总额 **/
    private String loanPrice;
    /** 借款标数量 **/
    private String loanCount;
    /** 投标会员数 **/
    private String investMemberCount;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 当前数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 当前页 **/
    private String currentPage;

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getGrantCount() {
        return grantCount;
    }
    public void setGrantCount(String grantCount) {
        this.grantCount = grantCount;
    }

    public String getLoanPrice() {
        return loanPrice;
    }
    public void setLoanPrice(String loanPrice) {
        this.loanPrice = loanPrice;
    }

    public String getLoanCount() {
        return loanCount;
    }
    public void setLoanCount(String loanCount) {
        this.loanCount = loanCount;
    }

    public String getInvestMemberCount() {
        return investMemberCount;
    }
    public void setInvestMemberCount(String investMemberCount) {
        this.investMemberCount = investMemberCount;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public String toString() {
        return "ProductSaleMessageBean{" +
                "menuUrl='" + menuUrl + '\'' +
                ", typeId='" + typeId + '\'' +
                ", typeName='" + typeName + '\'' +
                ", grantCount='" + grantCount + '\'' +
                ", loanPrice='" + loanPrice + '\'' +
                ", loanCount='" + loanCount + '\'' +
                ", investMemberCount='" + investMemberCount + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", currentPage='" + currentPage + '\'' +
                '}';
    }
}
