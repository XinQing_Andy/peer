package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/4/12.
 */
public class SysManageAmtMessageBean {
    /** 主键 **/
    private int id;
    /** 费用名称 **/
    private String feeName;
    /** 费用金额 **/
    private Double feeAmt;
    /** 是否启用 **/
    private String activeFlg;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /**  **/
    private String menuUrl;
    /**  **/
    private String jetxName;

    public String getActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(String activeFlg) {
        this.activeFlg = activeFlg;
    }

    public Double getFeeAmt() {
        return feeAmt;
    }

    public void setFeeAmt(Double feeAmt) {
        this.feeAmt = feeAmt;
    }

    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "SysManageAmtMessageBean{" +
                "activeFlg='" + activeFlg + '\'' +
                ", id=" + id +
                ", feeName='" + feeName + '\'' +
                ", feeAmt=" + feeAmt +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", jetxName='" + jetxName + '\'' +
                '}';
    }
}
