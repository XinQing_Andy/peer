package com.mh.peer.model.message;

/**
 * Created by ${zhangerxin} on 2016-6-6.
 */
public class MemberInfoMessageBean {

    /** 主键 **/
    private String id;
    /** 用户名 **/
    private String username;
    /** 昵称 **/
    private String nickname;
    /** 真实姓名 **/
    private String realname;
    /** 密码 **/
    private String password;
    /** 身份证号 **/
    private String idcard;
    /** 手机号 **/
    private String telephone;
    /** 邮箱地址 **/
    private String email;
    /** 性别(0-男、1-女) **/
    private String sex;
    /** 出生日期 **/
    private String birthday;
    /** 省id **/
    private String province;
    /** 城市id **/
    private String city;
    /** 城市内容拼接 **/
    private String cityPj;
    /** 详细地址 **/
    private String detailAddress;
    /** 学历(0-小学、1-初中、2-高中、3-大专、4-本科、5-硕士、6-博士、7-博士后、8-其他) **/
    private String dregree;
    /** 毕业院校 **/
    private String school;
    /** 婚姻状况(0-未婚、1-已婚、2-离婚、3-丧偶) **/
    private String marryStatus;
    /** 子女状况(0-无、1-一个、2-两个、3-三个、4-三个以上) **/
    private String children;
    /** 月收入(0-2000元以下、1-2000元-5000元、3-5000元-10000元、4-10000元以上) **/
    private String monthSalary;
    /** 社保情况(0-无、1-未缴满6个月、2-缴满6个月以上) **/
    private String socialSecurity;
    /** 住房条件(0-有商品房(无贷款)、1-有商品房(有贷款)、2-有其他(非商品)房、3-与父母同住、4-租房) **/
    private String houseCondition;
    /** 是否有车(0-有、1-无) **/
    private String carFlg;
    /** 是否实名化认证(0-是、1-否) **/
    private String realFlg;
    /** 是否更改密码(0-是、1-否) **/
    private String passwdFlg;
    /** 是否手机认证(0-是、1-否) **/
    private String telephoneFlg;
    /** 是否邮件认证(0-是、1-否) **/
    private String mailFlg;
    /** 是否锁定(0-是、1-否) **/
    private String lockFlg;
    /** 是否删除(0-是、1-否) **/
    private String delFlg;
    /** 创建时间 **/
    private String createtime;

    /** 链接地址 **/
    private String menuUrl;
    /** 当前数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 显示内容 **/
    private String htmlText;
    /** 会员信息 **/
    private String pageMemberStr;
    /** 旧密码 **/
    private String oldPassWord;
    /** 交易密码 **/
    private String traderpassword;
    /** 关键字 **/
    private String keyWord;
    /** 排序 **/
    private String sort;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 上次登陆时间 **/
    private String lastLogin;
    /**当前mysql数据库时间**/
    private String currentDate;
    /**账户余额**/
    private String balance;
    /**积分**/
    private String points;
    /**管理费率**/
    private String managerate;
    /**累计金额收益**/
    private String interests;
    /**待收总额**/
    private String receivedPrice;
    /**待还总额**/
    private String repayedPrice;
    /**mysql系统年**/
    private String year;
    /**mysql系统月**/
    private String month;
    /** 投资总额 **/
    private String investprice;
    /**  累计投标数量  **/
    private String investcount;
    /**  累计投标金额  **/
    private String investsum;
    /**  是否降序排列  **/
    private String isDesc;
    /**  是否首页点击  **/
    private String isHome;
    /**  短信验证码  **/
    private String code;

    /**  **/
    private String info;
    /**  **/
    private String result;

    public String getInvestprice() {
        return investprice;
    }

    public void setInvestprice(String investprice) {
        this.investprice = investprice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityPj() {
        return cityPj;
    }

    public void setCityPj(String cityPj) {
        this.cityPj = cityPj;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getDregree() {
        return dregree;
    }

    public void setDregree(String dregree) {
        this.dregree = dregree;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getMarryStatus() {
        return marryStatus;
    }

    public void setMarryStatus(String marryStatus) {
        this.marryStatus = marryStatus;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getMonthSalary() {
        return monthSalary;
    }

    public void setMonthSalary(String monthSalary) {
        this.monthSalary = monthSalary;
    }

    public String getSocialSecurity() {
        return socialSecurity;
    }

    public void setSocialSecurity(String socialSecurity) {
        this.socialSecurity = socialSecurity;
    }

    public String getHouseCondition() {
        return houseCondition;
    }

    public void setHouseCondition(String houseCondition) {
        this.houseCondition = houseCondition;
    }

    public String getCarFlg() {
        return carFlg;
    }

    public void setCarFlg(String carFlg) {
        this.carFlg = carFlg;
    }

    public String getRealFlg() {
        return realFlg;
    }

    public void setRealFlg(String realFlg) {
        this.realFlg = realFlg;
    }

    public String getPasswdFlg() {
        return passwdFlg;
    }

    public void setPasswdFlg(String passwdFlg) {
        this.passwdFlg = passwdFlg;
    }

    public String getTelephoneFlg() {
        return telephoneFlg;
    }

    public void setTelephoneFlg(String telephoneFlg) {
        this.telephoneFlg = telephoneFlg;
    }

    public String getMailFlg() {
        return mailFlg;
    }

    public void setMailFlg(String mailFlg) {
        this.mailFlg = mailFlg;
    }

    public String getLockFlg() {
        return lockFlg;
    }

    public void setLockFlg(String lockFlg) {
        this.lockFlg = lockFlg;
    }

    public String getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public String getPageMemberStr() {
        return pageMemberStr;
    }

    public void setPageMemberStr(String pageMemberStr) {
        this.pageMemberStr = pageMemberStr;
    }

    public String getOldPassWord() {
        return oldPassWord;
    }

    public void setOldPassWord(String oldPassWord) {
        this.oldPassWord = oldPassWord;
    }

    public String getTraderpassword() {
        return traderpassword;
    }

    public void setTraderpassword(String traderpassword) {
        this.traderpassword = traderpassword;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }
    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    //账户余额
    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
    //积分
    public String getPoints() {
        return points;
    }
    public void setPoints(String points) {
        this.points = points;
    }

    //管理费率
    public String getManagerate() {
        return managerate;
    }
    public void setManagerate(String managerate) {
        this.managerate = managerate;
    }


    //累计金额收益
    public String getInterests() {
        return interests;
    }
    public void setInterests(String interests) {
        this.interests = interests;
    }

    //待收总额
    public String getReceivedPrice() {
        return receivedPrice;
    }
    public void setReceivedPrice(String receivedPrice) {
        this.receivedPrice = receivedPrice;
    }

    //待还总额
    public String getRepayedPrice() {
        return repayedPrice;
    }
    public void setRepayedPrice(String repayedPrice) {
        this.repayedPrice = repayedPrice;
    }

    //mysql系统年
    public String getYear() {
        return year;
    }
    public void setYear(String year) {
        this.year = year;
    }

    //mysql系统月
    public String getMonth() {
        return month;
    }
    public void setMonth(String month) {
        this.month = month;
    }

    public String getInvestcount() {
        return investcount;
    }

    public void setInvestcount(String investcount) {
        this.investcount = investcount;
    }

    public String getInvestsum() {
        return investsum;
    }

    public void setInvestsum(String investsum) {
        this.investsum = investsum;
    }

    public String getIsDesc() {
        return isDesc;
    }

    public void setIsDesc(String isDesc) {
        this.isDesc = isDesc;
    }

    public String getIsHome() {
        return isHome;
    }

    public void setIsHome(String isHome) {
        this.isHome = isHome;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "MemberInfoMessageBean{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", nickname='" + nickname + '\'' +
                ", realname='" + realname + '\'' +
                ", password='" + password + '\'' +
                ", idcard='" + idcard + '\'' +
                ", telephone='" + telephone + '\'' +
                ", email='" + email + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", cityPj='" + cityPj + '\'' +
                ", detailAddress='" + detailAddress + '\'' +
                ", dregree='" + dregree + '\'' +
                ", school='" + school + '\'' +
                ", marryStatus='" + marryStatus + '\'' +
                ", children='" + children + '\'' +
                ", monthSalary='" + monthSalary + '\'' +
                ", socialSecurity='" + socialSecurity + '\'' +
                ", houseCondition='" + houseCondition + '\'' +
                ", carFlg='" + carFlg + '\'' +
                ", realFlg='" + realFlg + '\'' +
                ", passwdFlg='" + passwdFlg + '\'' +
                ", telephoneFlg='" + telephoneFlg + '\'' +
                ", mailFlg='" + mailFlg + '\'' +
                ", lockFlg='" + lockFlg + '\'' +
                ", delFlg='" + delFlg + '\'' +
                ", createtime='" + createtime + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", htmlText='" + htmlText + '\'' +
                ", pageMemberStr='" + pageMemberStr + '\'' +
                ", oldPassWord='" + oldPassWord + '\'' +
                ", traderpassword='" + traderpassword + '\'' +
                ", keyWord='" + keyWord + '\'' +
                ", sort='" + sort + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", lastLogin='" + lastLogin + '\'' +
                ", currentDate='" + currentDate + '\'' +
                ", balance='" + balance + '\'' +
                ", points='" + points + '\'' +
                ", managerate='" + managerate + '\'' +
                ", interests='" + interests + '\'' +
                ", receivedPrice='" + receivedPrice + '\'' +
                ", repayedPrice='" + repayedPrice + '\'' +
                ", year='" + year + '\'' +
                ", month='" + month + '\'' +
                ", investprice='" + investprice + '\'' +
                ", investcount='" + investcount + '\'' +
                ", investsum='" + investsum + '\'' +
                ", isDesc='" + isDesc + '\'' +
                ", isHome='" + isHome + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
