package com.mh.peer.model.message;

/**
 * Created by wanghongjian on 2016/3/25.
 */
public class NewsTypeMessageBean {

    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;

    /** 主键 **/
    private String id;
    /** 名称 **/
    private String name;
    /** 父级id **/
    private String fid;
    /** 类别的描述 **/
    private String description;
    private String jetxName;
    /**二级菜单URL**/
    private String menuUrl;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getFid() {
        return fid;
    }
    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getJetxName() {
        return jetxName;
    }
    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    @Override
    public String toString() {
        return "NewsTypeMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", fid=" + fid +
                ", description='" + description + '\'' +
                ", jetxName='" + jetxName + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                '}';
    }
}
