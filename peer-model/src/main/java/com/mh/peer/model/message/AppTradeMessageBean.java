package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-12.
 */
public class AppTradeMessageBean {

    /** 主键 **/
    private String id;
    /** 交易类型 **/
    private String type;
    /** 实际金额 **/
    private String sprice;
    /** 手续费 **/
    private String procedureFee;
    /** 交易金额 **/
    private String tradePrice;
    /** 会员id **/
    private String memberId;
    /** 交易时间 **/
    private String time;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getSprice() {
        return sprice;
    }
    public void setSprice(String sprice) {
        this.sprice = sprice;
    }

    public String getProcedureFee() {
        return procedureFee;
    }
    public void setProcedureFee(String procedureFee) {
        this.procedureFee = procedureFee;
    }

    public String getTradePrice() {
        return tradePrice;
    }
    public void setTradePrice(String tradePrice) {
        this.tradePrice = tradePrice;
    }

    public String getMemberId() {
        return memberId;
    }
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "AppTradeMessageBean{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", sprice='" + sprice + '\'' +
                ", procedureFee='" + procedureFee + '\'' +
                ", tradePrice='" + tradePrice + '\'' +
                ", memberId='" + memberId + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
