package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-8-7.
 * 还款部分
 */
public class HuanXunRepayMessageBean {

    /** 还款id **/
    private String repayId;
    /** 还款人id **/
    private String repayer;
    /** 余额 **/
    private String balance;

    public String getRepayId() {
        return repayId;
    }
    public void setRepayId(String repayId) {
        this.repayId = repayId;
    }

    public String getRepayer() {
        return repayer;
    }
    public void setRepayer(String repayer) {
        this.repayer = repayer;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "HuanXunRepayMessageBean{" +
                "repayId='" + repayId + '\'' +
                ", repayer='" + repayer + '\'' +
                ", balance='" + balance + '\'' +
                '}';
    }
}
