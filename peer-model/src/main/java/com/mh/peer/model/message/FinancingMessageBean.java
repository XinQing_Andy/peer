package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-10-13.
 * 理财风云榜
 */
public class FinancingMessageBean {

    /** 菜单url **/
    private String menuUrl;
    /** 会员id **/
    private String memberId;
    /** 用户名 **/
    private String userName;
    /** 昵称 **/
    private String nickName;
    /** 投资金额 **/
    private String investPrice;
    /** 排序类型 **/
    private String orderType;
    /** 排序字段 **/
    private String orderColumn;
    /** 起始数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 排名 **/
    private String num;

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMemberId() {
        return memberId;
    }
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getInvestPrice() {
        return investPrice;
    }
    public void setInvestPrice(String investPrice) {
        this.investPrice = investPrice;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "FinancingMessageBean{" +
                "menuUrl='" + menuUrl + '\'' +
                ", memberId='" + memberId + '\'' +
                ", userName='" + userName + '\'' +
                ", nickName='" + nickName + '\'' +
                ", investPrice='" + investPrice + '\'' +
                ", orderType='" + orderType + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", num='" + num + '\'' +
                '}';
    }
}
