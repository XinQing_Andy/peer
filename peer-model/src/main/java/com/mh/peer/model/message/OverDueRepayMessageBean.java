package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-8-15.
 * 逾期账单
 */
public class OverDueRepayMessageBean {

    /** 主键 **/
    private String id;
    /** 应还款日期 **/
    private String yRepaymentTime;
    /** 应还款金额 **/
    private String yRepaymentPrice;
    /** 逾期时长(天) **/
    private String overDays;
    /** 产品题目 **/
    private String productTitle;
    /** 产品编码 **/
    private String code;
    /** 贷款金额 **/
    private String loanPrice;
    /** 贷款利率 **/
    private String loanRate;
    /** 菜单地址 **/
    private String menuUrl;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 当前数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getyRepaymentTime() {
        return yRepaymentTime;
    }
    public void setyRepaymentTime(String yRepaymentTime) {
        this.yRepaymentTime = yRepaymentTime;
    }

    public String getyRepaymentPrice() {
        return yRepaymentPrice;
    }
    public void setyRepaymentPrice(String yRepaymentPrice) {
        this.yRepaymentPrice = yRepaymentPrice;
    }

    public String getOverDays() {
        return overDays;
    }
    public void setOverDays(String overDays) {
        this.overDays = overDays;
    }

    public String getProductTitle() {
        return productTitle;
    }
    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getLoanPrice() {
        return loanPrice;
    }
    public void setLoanPrice(String loanPrice) {
        this.loanPrice = loanPrice;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "OverDueRepayMessageBean{" +
                "id='" + id + '\'' +
                ", yRepaymentTime='" + yRepaymentTime + '\'' +
                ", yRepaymentPrice='" + yRepaymentPrice + '\'' +
                ", overDays='" + overDays + '\'' +
                ", productTitle='" + productTitle + '\'' +
                ", code='" + code + '\'' +
                ", loanPrice='" + loanPrice + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                '}';
    }
}
