package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-14.
 * 债权转让详细信息
 */
public class AppProductAttornDetailMessageBean {

    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String productBuyId;
    /** 转让价格 **/
    private String attronPrice;
    /** 预期收益率 **/
    private String expectedRate;
    /** 产品id **/
    private String productId;
    /** 产品题目 **/
    private String title;
    /** 年利率 **/
    private String loanRate;
    /** 剩余期限 **/
    private String surplusDays;
    /** 审核状态(0-审核通过、1-审核未通过、2-未审核) **/
    private String shFlag;
    /** 剩余本金 **/
    private String surplusPrincipal;
    /** 待收本息 **/
    private String receivedPrice;
    /** 下期还款日 **/
    private String nextReceiveTime;
    /** 产品编码 **/
    private String code;
    /** 贷款金额 **/
    private String loanFee;
    /** 发标日期 **/
    private String fullTime;
    /** 贷款期限(年) **/
    private String loanYear;
    /** 贷款期限(月) **/
    private String loanMonth;
    /** 贷款时长 **/
    private String loanLength;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 项目ID号 **/
    private String projectNo;
    /** 它方账号 **/
    private String otherIpsAcctNo;
    /** 借款人 **/
    private String applyMemberId;
    /** 投资人 **/
    private String buyer;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductBuyId() {
        return productBuyId;
    }
    public void setProductBuyId(String productBuyId) {
        this.productBuyId = productBuyId;
    }

    public String getAttronPrice() {
        return attronPrice;
    }
    public void setAttronPrice(String attronPrice) {
        this.attronPrice = attronPrice;
    }

    public String getExpectedRate() {
        return expectedRate;
    }
    public void setExpectedRate(String expectedRate) {
        this.expectedRate = expectedRate;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getSurplusDays() {
        return surplusDays;
    }
    public void setSurplusDays(String surplusDays) {
        this.surplusDays = surplusDays;
    }

    public String getSurplusPrincipal() {
        return surplusPrincipal;
    }
    public void setSurplusPrincipal(String surplusPrincipal) {
        this.surplusPrincipal = surplusPrincipal;
    }

    public String getReceivedPrice() {
        return receivedPrice;
    }
    public void setReceivedPrice(String receivedPrice) {
        this.receivedPrice = receivedPrice;
    }

    public String getNextReceiveTime() {
        return nextReceiveTime;
    }
    public void setNextReceiveTime(String nextReceiveTime) {
        this.nextReceiveTime = nextReceiveTime;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getLoanFee() {
        return loanFee;
    }
    public void setLoanFee(String loanFee) {
        this.loanFee = loanFee;
    }

    public String getFullTime() {
        return fullTime;
    }
    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getLoanYear() {
        return loanYear;
    }
    public void setLoanYear(String loanYear) {
        this.loanYear = loanYear;
    }

    public String getLoanMonth() {
        return loanMonth;
    }
    public void setLoanMonth(String loanMonth) {
        this.loanMonth = loanMonth;
    }

    public String getLoanLength() {
        return loanLength;
    }
    public void setLoanLength(String loanLength) {
        this.loanLength = loanLength;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getProjectNo() {
        return projectNo;
    }
    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getOtherIpsAcctNo() {
        return otherIpsAcctNo;
    }
    public void setOtherIpsAcctNo(String otherIpsAcctNo) {
        this.otherIpsAcctNo = otherIpsAcctNo;
    }

    public String getApplyMemberId() {
        return applyMemberId;
    }
    public void setApplyMemberId(String applyMemberId) {
        this.applyMemberId = applyMemberId;
    }

    public String getBuyer() {
        return buyer;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    @Override
    public String toString() {
        return "AppProductAttornDetailMessageBean{" +
                "id='" + id + '\'' +
                ", productBuyId='" + productBuyId + '\'' +
                ", attronPrice='" + attronPrice + '\'' +
                ", expectedRate='" + expectedRate + '\'' +
                ", productId='" + productId + '\'' +
                ", title='" + title + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", surplusDays='" + surplusDays + '\'' +
                ", shFlag='" + shFlag + '\'' +
                ", surplusPrincipal='" + surplusPrincipal + '\'' +
                ", receivedPrice='" + receivedPrice + '\'' +
                ", nextReceiveTime='" + nextReceiveTime + '\'' +
                ", code='" + code + '\'' +
                ", loanFee='" + loanFee + '\'' +
                ", fullTime='" + fullTime + '\'' +
                ", loanYear='" + loanYear + '\'' +
                ", loanMonth='" + loanMonth + '\'' +
                ", loanLength='" + loanLength + '\'' +
                ", repayment='" + repayment + '\'' +
                ", projectNo='" + projectNo + '\'' +
                ", otherIpsAcctNo='" + otherIpsAcctNo + '\'' +
                ", applyMemberId='" + applyMemberId + '\'' +
                ", buyer='" + buyer + '\'' +
                '}';
    }
}
