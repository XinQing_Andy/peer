package com.mh.peer.model.business;

/**
 * Created by WangMeng on 2016/1/13.
 */
public class UserInfoBusiness {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 权限名称
     */
    private String roleName;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 权限ID
     */
    private String roleId;
    /**
     * 菜单ID
     */
    private String menuId;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 父级菜单ID
     */
    private String parentId;
    /**
     * 科室ID
     */
    private String deptId;
    /**
     * 科室名称
     */
    private String deptName;
    /**
     * 功能ID
     */
    private String functionId;
    /**
     * 功能名称
     */
    private String functionName;
    /**
     * 菜单业务模板名称
     */
    private String menuUrl;
    /**
     * 菜单业务模板控制类URL
     */
    private String controlUrl;
    /**
     * 菜单样式图标
     */
    private String iconstyle;
    /**
     * 子菜单个数
     */
    private String childCount;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getControlUrl() {
        return controlUrl;
    }

    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    public String getIconstyle() {
        return iconstyle;
    }

    public void setIconstyle(String iconstyle) {
        this.iconstyle = iconstyle;
    }

    public String getChildCount() {
        return childCount;
    }

    public void setChildCount(String childCount) {
        this.childCount = childCount;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    @Override
    public String toString() {
        return "UserInfoBusiness{" +
                "userName='" + userName + '\'' +
                ", roleName='" + roleName + '\'' +
                ", userId='" + userId + '\'' +
                ", roleId='" + roleId + '\'' +
                ", menuId='" + menuId + '\'' +
                ", menuName='" + menuName + '\'' +
                ", parentId='" + parentId + '\'' +
                ", deptId='" + deptId + '\'' +
                ", deptName='" + deptName + '\'' +
                ", functionId='" + functionId + '\'' +
                ", functionName='" + functionName + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", controlUrl='" + controlUrl + '\'' +
                ", iconstyle='" + iconstyle + '\'' +
                ", childCount='" + childCount + '\'' +
                '}';
    }
}
