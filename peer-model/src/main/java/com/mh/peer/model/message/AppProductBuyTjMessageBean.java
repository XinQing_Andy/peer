package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-7.
 * 产品投资统计
 */
public class AppProductBuyTjMessageBean {

    /** 产品主键 **/
    private String id;
    /** 贷款金额 **/
    private String loadFee;
    /** 投资总额 **/
    private String investSum;
    /** 未投资总额 **/
    private String winvestSum;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLoadFee() {
        return loadFee;
    }
    public void setLoadFee(String loadFee) {
        this.loadFee = loadFee;
    }

    public String getInvestSum() {
        return investSum;
    }
    public void setInvestSum(String investSum) {
        this.investSum = investSum;
    }

    public String getWinvestSum() {
        return winvestSum;
    }
    public void setWinvestSum(String winvestSum) {
        this.winvestSum = winvestSum;
    }

    @Override
    public String toString() {
        return "AppProductBuyTjMessageBean{" +
                "id='" + id + '\'' +
                ", loadFee='" + loadFee + '\'' +
                ", investSum='" + investSum + '\'' +
                ", winvestSum='" + winvestSum + '\'' +
                '}';
    }
}
