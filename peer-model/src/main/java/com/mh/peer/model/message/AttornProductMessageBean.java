package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-6-30.
 */
public class AttornProductMessageBean {

    /** 返回信息 **/
    private String info;
    /** 返回结果 **/
    private String result;
    /** 菜单URL **/
    private String menuUrl;
    /** 主键 **/
    private String id;
    /** 转让价格 **/
    private String price;
    /** 剩余期限 **/
    private String surplusdays;
    /** 剩余本金 **/
    private String surplusPrincipal;
    /** 待收本息 **/
    private String waitprice;
    /** 到期日期 **/
    private String endtime;
    /** 借款编号 **/
    private String code;
    /** 题目 **/
    private String title;
    /** 发标日期 **/
    private String fullTime;
    /** 还款方式 **/
    private String repayment;
    /** 借款金额 **/
    private String loadFee;
    /** 借款期限 **/
    private String loadLength;
    /** 借款年利率 **/
    private String loanrate;
    /** 债权承接人 **/
    private String undertakeMember;
    /** 预期收益率 **/
    private String expectedrate;
    /** 账户可用余额 **/
    private String balance;
    /** 审核状态 **/
    private String shFlag;
    /** 承接时间 **/
    private String undertakeTime;
    /** 产品购买id **/
    private String buyId;
    /** 产品id **/
    private String productId;
    /** 投资人 **/
    private String buyer;
    /** 申请人 **/
    private String applyMemberId;
    /** 交易密码 **/
    private String traderpassword;
    /** 验证码 **/
    private String captcha;
    /** 会员信息 **/
    private MemberInfoMessageBean memberInfoMessageBean;

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getSurplusdays() {
        return surplusdays;
    }
    public void setSurplusdays(String surplusdays) {
        this.surplusdays = surplusdays;
    }

    public String getSurplusPrincipal() {
        return surplusPrincipal;
    }
    public void setSurplusPrincipal(String surplusPrincipal) {
        this.surplusPrincipal = surplusPrincipal;
    }

    public String getWaitprice() {
        return waitprice;
    }
    public void setWaitprice(String waitprice) {
        this.waitprice = waitprice;
    }

    public String getEndtime() {
        return endtime;
    }
    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getFullTime() {
        return fullTime;
    }
    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getLoadFee() {
        return loadFee;
    }
    public void setLoadFee(String loadFee) {
        this.loadFee = loadFee;
    }

    public String getLoadLength() {
        return loadLength;
    }
    public void setLoadLength(String loadLength) {
        this.loadLength = loadLength;
    }

    public String getLoanrate() {
        return loanrate;
    }
    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getUndertakeMember() {
        return undertakeMember;
    }
    public void setUndertakeMember(String undertakeMember) {
        this.undertakeMember = undertakeMember;
    }

    public String getExpectedrate() {
        return expectedrate;
    }
    public void setExpectedrate(String expectedrate) {
        this.expectedrate = expectedrate;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getUndertakeTime() {
        return undertakeTime;
    }
    public void setUndertakeTime(String undertakeTime) {
        this.undertakeTime = undertakeTime;
    }

    public String getBuyId() {
        return buyId;
    }
    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBuyer() {
        return buyer;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getApplyMemberId() {
        return applyMemberId;
    }
    public void setApplyMemberId(String applyMemberId) {
        this.applyMemberId = applyMemberId;
    }

    public String getTraderpassword() {
        return traderpassword;
    }
    public void setTraderpassword(String traderpassword) {
        this.traderpassword = traderpassword;
    }

    public String getCaptcha() {
        return captcha;
    }
    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public MemberInfoMessageBean getMemberInfoMessageBean() {
        return memberInfoMessageBean;
    }
    public void setMemberInfoMessageBean(MemberInfoMessageBean memberInfoMessageBean) {
        this.memberInfoMessageBean = memberInfoMessageBean;
    }

    @Override
    public String toString() {
        return "AttornProductMessageBean{" +
                "info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", id='" + id + '\'' +
                ", price='" + price + '\'' +
                ", surplusdays='" + surplusdays + '\'' +
                ", surplusPrincipal='" + surplusPrincipal + '\'' +
                ", waitprice='" + waitprice + '\'' +
                ", endtime='" + endtime + '\'' +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", fullTime='" + fullTime + '\'' +
                ", repayment='" + repayment + '\'' +
                ", loadFee='" + loadFee + '\'' +
                ", loadLength='" + loadLength + '\'' +
                ", loanrate='" + loanrate + '\'' +
                ", undertakeMember='" + undertakeMember + '\'' +
                ", expectedrate='" + expectedrate + '\'' +
                ", balance='" + balance + '\'' +
                ", shFlag='" + shFlag + '\'' +
                ", undertakeTime='" + undertakeTime + '\'' +
                ", productId='" + productId + '\'' +
                ", buyer='" + buyer + '\'' +
                ", applyMemberId='" + applyMemberId + '\'' +
                ", traderpassword='" + traderpassword + '\'' +
                ", captcha='" + captcha + '\'' +
                ", memberInfoMessageBean=" + memberInfoMessageBean +
                '}';
    }
}
