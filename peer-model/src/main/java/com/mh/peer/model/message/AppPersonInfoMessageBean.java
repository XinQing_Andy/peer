package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-16.
 * 个人资料
 */
public class AppPersonInfoMessageBean {

    /** 主键 **/
    private String id;
    /** 昵称 **/
    private String nickName;
    /** 真实姓名 **/
    private String realName;
    /** 是否实名化认证(0-是、1-否) **/
    private String realFlg;
    /** 是否手机认证(0-是、1-否) **/
    private String telephoneFlg;
    /** 手机号 **/
    private String telePhone;
    /** 身份证号 **/
    private String idCard;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getRealFlg() {
        return realFlg;
    }
    public void setRealFlg(String realFlg) {
        this.realFlg = realFlg;
    }

    public String getTelephoneFlg() {
        return telephoneFlg;
    }
    public void setTelephoneFlg(String telephoneFlg) {
        this.telephoneFlg = telephoneFlg;
    }

    public String getTelePhone() {
        return telePhone;
    }
    public void setTelePhone(String telePhone) {
        this.telePhone = telePhone;
    }

    public String getIdCard() {
        return idCard;
    }
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    @Override
    public String toString() {
        return "AppPersonInfoMessageBean{" +
                "id='" + id + '\'' +
                ", nickName='" + nickName + '\'' +
                ", realName='" + realName + '\'' +
                ", realFlg='" + realFlg + '\'' +
                ", telephoneFlg='" + telephoneFlg + '\'' +
                ", telePhone='" + telePhone + '\'' +
                ", idCard='" + idCard + '\'' +
                '}';
    }
}
