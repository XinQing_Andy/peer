package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-28.
 */
public class HuanXunFreezeMessageBean {

    /** 返回状态 **/
    private String code;
    /** 主键 **/
    private String id;
    /** 债权id **/
    private String attornId;
    /** 产品购买id **/
    private String buyId;
    /** 产品id **/
    private String productId;
    /** 操作类型 **/
    private String operationType;
    /** 商户存管交易账号 **/
    private String merchantID;
    /** 签名 **/
    private String sign;
    /** 请求信息 **/
    private String request;
    /** 商户订单号 **/
    private String merBillNo;
    /** 冻结日期 **/
    private String merDate;
    /** 项目ID号 **/
    private String projectNo;
    /** 业务类型 **/
    private String bizType;
    /** 登记方式(1-手动、2-自动) **/
    private String regType;
    /** 合同号 **/
    private String contractNo;
    /** 授权号 **/
    private String authNo;
    /** 冻结金额 **/
    private String trdAmt;
    /** 平台手续费 **/
    private String merFee;
    /** 冻结方类型(1-用户、2-商户) **/
    private String freezeMerType;
    /** 冻结账号 **/
    private String ipsAcctNo;
    /** 它方账号 **/
    private String otherIpsAcctNo;
    /** 页面返回地址 **/
    private String webUrl;
    /** 后台通知地址 **/
    private String s2SUrl;
    /** 转入方IPS账号 **/
    private String inIpsAcctNo;
    /** ips冻结订单号 **/
    private String ipsBillNo;
    /** 用户id **/
    private String memberId;
    /** 冻结状态 **/
    private String flag;
    /** 验证码 **/
    private String captcha;
    /**  **/
    private String result;
    /**  **/
    private String info;
    /**  **/
    private String resultCode;
    /** 应还款日期 **/
    private String yRepaymentTime;
    /** 还款id **/
    private String repayId;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getAttornId() {
        return attornId;
    }
    public void setAttornId(String attornId) {
        this.attornId = attornId;
    }

    public String getBuyId() {
        return buyId;
    }
    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getOperationType() {
        return operationType;
    }
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getMerchantID() {
        return merchantID;
    }
    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getSign() {
        return sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }

    public String getMerBillNo() {
        return merBillNo;
    }
    public void setMerBillNo(String merBillNo) {
        this.merBillNo = merBillNo;
    }

    public String getMerDate() {
        return merDate;
    }
    public void setMerDate(String merDate) {
        this.merDate = merDate;
    }

    public String getProjectNo() {
        return projectNo;
    }
    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getBizType() {
        return bizType;
    }
    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getRegType() {
        return regType;
    }
    public void setRegType(String regType) {
        this.regType = regType;
    }

    public String getContractNo() {
        return contractNo;
    }
    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getAuthNo() {
        return authNo;
    }
    public void setAuthNo(String authNo) {
        this.authNo = authNo;
    }

    public String getTrdAmt() {
        return trdAmt;
    }
    public void setTrdAmt(String trdAmt) {
        this.trdAmt = trdAmt;
    }

    public String getMerFee() {
        return merFee;
    }
    public void setMerFee(String merFee) {
        this.merFee = merFee;
    }

    public String getFreezeMerType() {
        return freezeMerType;
    }
    public void setFreezeMerType(String freezeMerType) {
        this.freezeMerType = freezeMerType;
    }

    public String getIpsAcctNo() {
        return ipsAcctNo;
    }
    public void setIpsAcctNo(String ipsAcctNo) {
        this.ipsAcctNo = ipsAcctNo;
    }

    public String getOtherIpsAcctNo() {
        return otherIpsAcctNo;
    }
    public void setOtherIpsAcctNo(String otherIpsAcctNo) {
        this.otherIpsAcctNo = otherIpsAcctNo;
    }

    public String getWebUrl() {
        return webUrl;
    }
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getS2SUrl() {
        return s2SUrl;
    }
    public void setS2SUrl(String s2SUrl) {
        this.s2SUrl = s2SUrl;
    }

    public String getInIpsAcctNo() {
        return inIpsAcctNo;
    }
    public void setInIpsAcctNo(String inIpsAcctNo) {
        this.inIpsAcctNo = inIpsAcctNo;
    }

    public String getIpsBillNo() {
        return ipsBillNo;
    }
    public void setIpsBillNo(String ipsBillNo) {
        this.ipsBillNo = ipsBillNo;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getMemberId() {
        return memberId;
    }
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCaptcha() {
        return captcha;
    }
    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public String getResultCode() {
        return resultCode;
    }
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getyRepaymentTime() {
        return yRepaymentTime;
    }
    public void setyRepaymentTime(String yRepaymentTime) {
        this.yRepaymentTime = yRepaymentTime;
    }

    public String getRepayId() {
        return repayId;
    }
    public void setRepayId(String repayId) {
        this.repayId = repayId;
    }

    @Override
    public String toString() {
        return "HuanXunFreezeMessageBean{" +
                "code='" + code + '\'' +
                ", id='" + id + '\'' +
                ", attornId='" + attornId + '\'' +
                ", buyId='" + buyId + '\'' +
                ", productId='" + productId + '\'' +
                ", operationType='" + operationType + '\'' +
                ", merchantID='" + merchantID + '\'' +
                ", sign='" + sign + '\'' +
                ", request='" + request + '\'' +
                ", merBillNo='" + merBillNo + '\'' +
                ", merDate='" + merDate + '\'' +
                ", projectNo='" + projectNo + '\'' +
                ", bizType='" + bizType + '\'' +
                ", regType='" + regType + '\'' +
                ", contractNo='" + contractNo + '\'' +
                ", authNo='" + authNo + '\'' +
                ", trdAmt='" + trdAmt + '\'' +
                ", merFee='" + merFee + '\'' +
                ", freezeMerType='" + freezeMerType + '\'' +
                ", ipsAcctNo='" + ipsAcctNo + '\'' +
                ", otherIpsAcctNo='" + otherIpsAcctNo + '\'' +
                ", webUrl='" + webUrl + '\'' +
                ", s2SUrl='" + s2SUrl + '\'' +
                ", inIpsAcctNo='" + inIpsAcctNo + '\'' +
                ", ipsBillNo='" + ipsBillNo + '\'' +
                ", memberId='" + memberId + '\'' +
                ", flag='" + flag + '\'' +
                ", captcha='" + captcha + '\'' +
                ", result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", resultCode='" + resultCode + '\'' +
                ", yRepaymentTime='" + yRepaymentTime + '\'' +
                ", repayId='" + repayId + '\'' +
                '}';
    }
}
