package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/7/21.
 */
public class MemberMessageMesBean {

    /** 编码 **/
    private String code;
    /** 信息 **/
    private String info;
    /** 主键 **/
    private String id;
    /** 关联id **/
    private String glid;
    /** 类型(0-系统消息) **/
    private String type;
    /** 会员id **/
    private String memberid;
    /** 读取状态(0-已读、1-未读) **/
    private String readflag;
    /** 题目 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 删除标志 **/
    private String delFlg;
    /** 发送时间 **/
    private String sendtime;
    /** 创建时间 **/
    private String createtime;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 编号 **/
    private String num;
    /** 当前数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 接收起时间 **/
    private String startTime;
    /** 接收止时间 **/
    private String endTime;
    /** 被选中的id **/
    private String checkSelected;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getGlid() {
        return glid;
    }
    public void setGlid(String glid) {
        this.glid = glid;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getMemberid() {
        return memberid;
    }
    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getReadflag() {
        return readflag;
    }
    public void setReadflag(String readflag) {
        this.readflag = readflag;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getDelFlg() {
        return delFlg;
    }
    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    public String getSendtime() {
        return sendtime;
    }
    public void setSendtime(String sendtime) {
        this.sendtime = sendtime;
    }

    public String getCreatetime() {
        return createtime;
    }
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCheckSelected() {
        return checkSelected;
    }
    public void setCheckSelected(String checkSelected) {
        this.checkSelected = checkSelected;
    }

    @Override
    public String toString() {
        return "MemberMessageMesBean{" +
                "code='" + code + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", glid='" + glid + '\'' +
                ", type='" + type + '\'' +
                ", memberid='" + memberid + '\'' +
                ", readflag='" + readflag + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", delFlg='" + delFlg + '\'' +
                ", sendtime='" + sendtime + '\'' +
                ", createtime='" + createtime + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", num='" + num + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", checkSelected='" + checkSelected + '\'' +
                '}';
    }
}
