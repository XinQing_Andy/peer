package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016/1/27.
 */
public class SysUserMessageBean {
    /**
     * 返回成功或失败(error,success)
     */
    private String result;

    /**
     * 返回结果说明
     */
    private String info;

    /** ID主键 **/
    private String id;
    /** 部门id **/
    private String depeId;
    /** 账号名称 **/
    private String userName;
    /** 真实姓名 **/
    private String realName;
    /** 密码 **/
    private String passWord;
    /** 是否为管理员（0：是；1：否） **/
    private String isAdmin;
    /** 是否在线（0：是；1：否） **/
    private String isOnline;
    /** IP地址 **/
    private String ipAddress;
    /** 手机号码 **/
    private String cphone;
    /** 办公电话号码 **/
    private String ophone;
    /** 邮件地址 **/
    private String email;
    /** 角色ID **/
    private String roleId;
    /** 科室名称 **/
    private String deptName;
    /** 角色名称 **/
    private String roleName;
    /** 是否删除 **/
    private String delFlg;
    /**创建人员**/
    private String createUserId;
    /**创建时间**/
    private String createTime;
    /**更新人员**/
    private String updateUserId;
    /**更新时间**/
    private String updateTime;
    /** 模板URL **/
    private String menuUrl;
    /** 控制类URL **/
    private String controlUrl;
    /** 云盾编码 **/
    private String code;
    /** 数量 **/
    private String num;
    /** 关键字 **/
    private String sysKeyWord;
    /** 当前数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getDepeId() {
        return depeId;
    }
    public void setDepeId(String depeId) {
        this.depeId = depeId;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPassWord() {
        return passWord;
    }
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getIsAdmin() {
        return isAdmin;
    }
    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getIsOnline() {
        return isOnline;
    }
    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getIpAddress() {
        return ipAddress;
    }
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getCphone() {
        return cphone;
    }
    public void setCphone(String cphone) {
        this.cphone = cphone;
    }

    public String getOphone() {
        return ophone;
    }
    public void setOphone(String ophone) {
        this.ophone = ophone;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoleId() {
        return roleId;
    }
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getCreateUserId() {
        return createUserId;
    }
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }
    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getDeptName() {
        return deptName;
    }
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getRoleName() {
        return roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDelFlg() {
        return delFlg;
    }
    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getControlUrl() {
        return controlUrl;
    }
    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }

    public String getSysKeyWord() {
        return sysKeyWord;
    }
    public void setSysKeyWord(String sysKeyWord) {
        this.sysKeyWord = sysKeyWord;
    }

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public String toString() {
        return "SysUserMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", depeId='" + depeId + '\'' +
                ", userName='" + userName + '\'' +
                ", realName='" + realName + '\'' +
                ", passWord='" + passWord + '\'' +
                ", isAdmin='" + isAdmin + '\'' +
                ", isOnline='" + isOnline + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", cphone='" + cphone + '\'' +
                ", ophone='" + ophone + '\'' +
                ", email='" + email + '\'' +
                ", roleId='" + roleId + '\'' +
                ", deptName='" + deptName + '\'' +
                ", roleName='" + roleName + '\'' +
                ", delFlg='" + delFlg + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateUserId='" + updateUserId + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", controlUrl='" + controlUrl + '\'' +
                ", code='" + code + '\'' +
                ", num='" + num + '\'' +
                ", sysKeyWord='" + sysKeyWord + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                '}';
    }
}
