package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-11.
 */
public class AppProductReceiveMessageBean {

    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String buyId;
    /** 本金 **/
    private String principal;
    /** 利息 **/
    private String interest;
    /** 应回款金额 **/
    private String yreceivePrice;
    /** 应回款日期 **/
    private String yreceiveTime;
    /** 回款金额 **/
    private String receivePrice;
    /** 实际回款日期 **/
    private String sreceiveTime;
    /** 创建日期 **/
    private String createTime;
    /** 最后更新日期 **/
    private String updateTime;
    /** 产品id **/
    private String productId;
    /** 题目 **/
    private String title;
    /** 编号 **/
    private String code;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getBuyId() {
        return buyId;
    }
    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }

    public String getPrincipal() {
        return principal;
    }
    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getInterest() {
        return interest;
    }
    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getYreceivePrice() {
        return yreceivePrice;
    }
    public void setYreceivePrice(String yreceivePrice) {
        this.yreceivePrice = yreceivePrice;
    }

    public String getYreceiveTime() {
        return yreceiveTime;
    }
    public void setYreceiveTime(String yreceiveTime) {
        this.yreceiveTime = yreceiveTime;
    }

    public String getReceivePrice() {
        return receivePrice;
    }
    public void setReceivePrice(String receivePrice) {
        this.receivePrice = receivePrice;
    }

    public String getSreceiveTime() {
        return sreceiveTime;
    }
    public void setSreceiveTime(String sreceiveTime) {
        this.sreceiveTime = sreceiveTime;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "AppProductReceiveMessageBean{" +
                "id='" + id + '\'' +
                ", buyId='" + buyId + '\'' +
                ", principal='" + principal + '\'' +
                ", interest='" + interest + '\'' +
                ", yreceivePrice='" + yreceivePrice + '\'' +
                ", yreceiveTime='" + yreceiveTime + '\'' +
                ", receivePrice='" + receivePrice + '\'' +
                ", sreceiveTime='" + sreceiveTime + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", productId='" + productId + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
