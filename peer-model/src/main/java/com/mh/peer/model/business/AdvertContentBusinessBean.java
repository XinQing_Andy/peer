package com.mh.peer.model.business;

import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.entity.SysEnclosure;

import java.util.List;

/**
 * Created by zhangerxin on 2016/3/22.
 */
public class AdvertContentBusinessBean {

    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;

    /** 主键 **/
    private String id;
    /** 类型id **/
    private String typeId;
    /** 题目 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 作者 **/
    private String autor;
    /** 排序 **/
    private String sort;
    /** 状态(0-有效、1-无效) **/
    private String flag;
    /** 浏览次数 **/
    private String look_Count;
    /** 创建用户 **/
    private String createUser;
    /** 创建时间 **/
    private String createTime;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getAutor() {
        return autor;
    }
    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getSort() {
        return sort;
    }
    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getLook_Count() {
        return look_Count;
    }
    public void setLook_Count(String look_Count) {
        this.look_Count = look_Count;
    }

    public String getCreateUser() {
        return createUser;
    }
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "AdvertContentMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", typeId='" + typeId + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", autor='" + autor + '\'' +
                ", sort='" + sort + '\'' +
                ", flag='" + flag + '\'' +
                ", look_Count='" + look_Count + '\'' +
                ", createUser='" + createUser + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
