package com.mh.peer.model.message;

/**
 * Created by ${zhangerxin} on 2016-7-27.
 */
public class HuanXunRegProjectMessageBean {

    /** 程序执行状态 **/
    private String code;
    /** 程序返回信息 **/
    private String info;
    /** 主键 **/
    private String id;
    /** 关联id **/
    private String glId;
    /** 操作类型(project.regProject) **/
    private String operationType;
    /** 商户存管交易账号 **/
    private String merchantID;
    /** 签名 **/
    private String sign;
    /** 请求信息 **/
    private String request;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getGlId() {
        return glId;
    }
    public void setGlId(String glId) {
        this.glId = glId;
    }

    public String getOperationType() {
        return operationType;
    }
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getMerchantID() {
        return merchantID;
    }
    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getSign() {
        return sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "HuanXunRegProjectMessageBean{" +
                "code='" + code + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", glId='" + glId + '\'' +
                ", operationType='" + operationType + '\'' +
                ", merchantID='" + merchantID + '\'' +
                ", sign='" + sign + '\'' +
                ", request='" + request + '\'' +
                '}';
    }
}
