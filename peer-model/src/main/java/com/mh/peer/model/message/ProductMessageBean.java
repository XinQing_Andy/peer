package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-4-10.
 */
public class ProductMessageBean {

    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;

    /** 主键 **/
    private String id;
    /** 题目 **/
    private String title;
    /** 编码 **/
    private String code;
    /** 借款标类型 **/
    private String productType;
    /** 类型名称 **/
    private String typeName;
    /** 适合人群 **/
    private String crowds;
    /** 状态(0-上架、1-下架) **/
    private String flag;
    /** 审核状态(0-审核通过、1-审核未通过、2-未审核) **/
    private String shflag;
    /** 审核意见 **/
    private String shyj;
    /** 亮点 **/
    private String highpoint;
    /** 额度范围(起始) **/
    private String creditstart;
    /** 额度范围(截止) **/
    private String creditend;
    /** 贷款利率 **/
    private String loanrate;
    /** 贷款期限(年) **/
    private String loanyear;
    /** 贷款期限(月) **/
    private String loanmonth;
    /** 贷款期限(日) **/
    private String loadday;
    /** 余额 **/
    private String balance;
    /** 贷款期限(时长) **/
    private String loadLength;
    /** 投标时间(日) **/
    private String bidday;
    /** 审核时间 **/
    private String checktime;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 投标金额 **/
    private String bidfee;
    /** 拆分份数 **/
    private String splitcount;
    /** 贷款金额(投标金额 * 拆分份数) **/
    private String loadFee;
    /** 投标金额 **/
    private String biddingFee;
    /** 未投标金额 **/
    private String wbiddingFee;
    /** 投标进度 **/
    private String bidProgress;
    /** 手续费 **/
    private String counterfee;
    /** 申请人 **/
    private String applyMember;
    /** 申请人名称 **/
    private String applyMemberName;
    /** 申请人条件 **/
    private String applycondition;
    /** 上线时间 **/
    private String onlinetime;
    /** 担保人 **/
    private String sponsor;
    /** 是否满标 **/
    private String fullScale;
    /** 满标时间 **/
    private String fullTime;
    /** 排序 **/
    private String sort;
    /** 创建人 **/
    private String createuser;
    /** 创建时间 **/
    private String createtime;
    /** 更新人 **/
    private String updateuser;
    /** 更新时间 **/
    private String updatetime;
    /** 二级菜单URL **/
    private String menuUrl;
    /** 当前数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 序号 **/
    private String num;
    /** 图片地址(相对路径和绝对路径拼接) **/
    private String imgUrl;
    /** 相对路径拼接 **/
    private String xdUrl;
    /** 会员信息集合 **/
    private MemberInfoMessageBean memberInfoMessageBean;
    /** 已选择的产品类型拼接 **/
    private String productTypeIdSelectPj;
    /** 已选择的年利率拼接 **/
    private String loanrateSelectPj;
    /** 已选择的期限拼接 **/
    private String loanDateSelectPj;
    /** 已选择的还款方式拼接 **/
    private String repaymentSelectPj;
    /** 发布状态 **/
    private String onlineSelect;
    /** 是否多选(0-多选、1-单选) **/
    private String selectMutli;
    /** 申请人 **/
    private String applymember;
    /** **/
    private String type;
    /** 真实姓名 **/
    private String realname;
    /** 交易金额总和 **/
    private String loadFeeSum;
    /** 利息总和 **/
    private String interestSum;
    /** 分页部分拼接 **/
    private String pageStr;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCrowds() {
        return crowds;
    }

    public void setCrowds(String crowds) {
        this.crowds = crowds;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getShflag() {
        return shflag;
    }

    public void setShflag(String shflag) {
        this.shflag = shflag;
    }

    public String getShyj() {
        return shyj;
    }

    public void setShyj(String shyj) {
        this.shyj = shyj;
    }

    public String getHighpoint() {
        return highpoint;
    }

    public void setHighpoint(String highpoint) {
        this.highpoint = highpoint;
    }

    public String getCreditstart() {
        return creditstart;
    }

    public void setCreditstart(String creditstart) {
        this.creditstart = creditstart;
    }

    public String getCreditend() {
        return creditend;
    }

    public void setCreditend(String creditend) {
        this.creditend = creditend;
    }

    public String getLoanrate() {
        return loanrate;
    }

    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getLoanyear() {
        return loanyear;
    }

    public void setLoanyear(String loanyear) {
        this.loanyear = loanyear;
    }

    public String getLoanmonth() {
        return loanmonth;
    }

    public void setLoanmonth(String loanmonth) {
        this.loanmonth = loanmonth;
    }

    public String getLoadday() {
        return loadday;
    }

    public void setLoadday(String loadday) {
        this.loadday = loadday;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getLoadLength() {
        return loadLength;
    }

    public void setLoadLength(String loadLength) {
        this.loadLength = loadLength;
    }

    public String getBidday() {
        return bidday;
    }

    public void setBidday(String bidday) {
        this.bidday = bidday;
    }

    public String getChecktime() {
        return checktime;
    }

    public void setChecktime(String checktime) {
        this.checktime = checktime;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getBidfee() {
        return bidfee;
    }

    public void setBidfee(String bidfee) {
        this.bidfee = bidfee;
    }

    public String getSplitcount() {
        return splitcount;
    }

    public void setSplitcount(String splitcount) {
        this.splitcount = splitcount;
    }

    public String getLoadFee() {
        return loadFee;
    }

    public void setLoadFee(String loadFee) {
        this.loadFee = loadFee;
    }

    public String getBiddingFee() {
        return biddingFee;
    }

    public void setBiddingFee(String biddingFee) {
        this.biddingFee = biddingFee;
    }

    public String getWbiddingFee() {
        return wbiddingFee;
    }

    public void setWbiddingFee(String wbiddingFee) {
        this.wbiddingFee = wbiddingFee;
    }

    public String getBidProgress() {
        return bidProgress;
    }

    public void setBidProgress(String bidProgress) {
        this.bidProgress = bidProgress;
    }

    public String getCounterfee() {
        return counterfee;
    }

    public void setCounterfee(String counterfee) {
        this.counterfee = counterfee;
    }

    public String getApplyMember() {
        return applyMember;
    }

    public void setApplyMember(String applyMember) {
        this.applyMember = applyMember;
    }

    public String getApplyMemberName() {
        return applyMemberName;
    }

    public void setApplyMemberName(String applyMemberName) {
        this.applyMemberName = applyMemberName;
    }

    public String getApplycondition() {
        return applycondition;
    }

    public void setApplycondition(String applycondition) {
        this.applycondition = applycondition;
    }

    public String getOnlinetime() {
        return onlinetime;
    }

    public void setOnlinetime(String onlinetime) {
        this.onlinetime = onlinetime;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getFullScale() {
        return fullScale;
    }

    public void setFullScale(String fullScale) {
        this.fullScale = fullScale;
    }

    public String getFullTime() {
        return fullTime;
    }

    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(String updateuser) {
        this.updateuser = updateuser;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getXdUrl() {
        return xdUrl;
    }

    public void setXdUrl(String xdUrl) {
        this.xdUrl = xdUrl;
    }

    public MemberInfoMessageBean getMemberInfoMessageBean() {
        return memberInfoMessageBean;
    }

    public void setMemberInfoMessageBean(MemberInfoMessageBean memberInfoMessageBean) {
        this.memberInfoMessageBean = memberInfoMessageBean;
    }

    public String getProductTypeIdSelectPj() {
        return productTypeIdSelectPj;
    }

    public void setProductTypeIdSelectPj(String productTypeIdSelectPj) {
        this.productTypeIdSelectPj = productTypeIdSelectPj;
    }

    public String getLoanrateSelectPj() {
        return loanrateSelectPj;
    }

    public void setLoanrateSelectPj(String loanrateSelectPj) {
        this.loanrateSelectPj = loanrateSelectPj;
    }

    public String getLoanDateSelectPj() {
        return loanDateSelectPj;
    }

    public void setLoanDateSelectPj(String loanDateSelectPj) {
        this.loanDateSelectPj = loanDateSelectPj;
    }

    public String getRepaymentSelectPj() {
        return repaymentSelectPj;
    }

    public void setRepaymentSelectPj(String repaymentSelectPj) {
        this.repaymentSelectPj = repaymentSelectPj;
    }

    public String getOnlineSelect() {
        return onlineSelect;
    }

    public void setOnlineSelect(String onlineSelect) {
        this.onlineSelect = onlineSelect;
    }

    public String getSelectMutli() {
        return selectMutli;
    }

    public void setSelectMutli(String selectMutli) {
        this.selectMutli = selectMutli;
    }

    public String getApplymember() {
        return applymember;
    }

    public void setApplymember(String applymember) {
        this.applymember = applymember;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getLoadFeeSum() {
        return loadFeeSum;
    }

    public void setLoadFeeSum(String loadFeeSum) {
        this.loadFeeSum = loadFeeSum;
    }

    public String getInterestSum() {
        return interestSum;
    }

    public void setInterestSum(String interestSum) {
        this.interestSum = interestSum;
    }

    public String getPageStr() {
        return pageStr;
    }

    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    @Override
    public String toString() {
        return "ProductMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", productType='" + productType + '\'' +
                ", typeName='" + typeName + '\'' +
                ", crowds='" + crowds + '\'' +
                ", flag='" + flag + '\'' +
                ", shflag='" + shflag + '\'' +
                ", shyj='" + shyj + '\'' +
                ", highpoint='" + highpoint + '\'' +
                ", creditstart='" + creditstart + '\'' +
                ", creditend='" + creditend + '\'' +
                ", loanrate='" + loanrate + '\'' +
                ", loanyear='" + loanyear + '\'' +
                ", loanmonth='" + loanmonth + '\'' +
                ", loadday='" + loadday + '\'' +
                ", balance='" + balance + '\'' +
                ", loadLength='" + loadLength + '\'' +
                ", bidday='" + bidday + '\'' +
                ", checktime='" + checktime + '\'' +
                ", repayment='" + repayment + '\'' +
                ", bidfee='" + bidfee + '\'' +
                ", splitcount='" + splitcount + '\'' +
                ", loadFee='" + loadFee + '\'' +
                ", biddingFee='" + biddingFee + '\'' +
                ", wbiddingFee='" + wbiddingFee + '\'' +
                ", bidProgress='" + bidProgress + '\'' +
                ", counterfee='" + counterfee + '\'' +
                ", applyMember='" + applyMember + '\'' +
                ", applyMemberName='" + applyMemberName + '\'' +
                ", applycondition='" + applycondition + '\'' +
                ", onlinetime='" + onlinetime + '\'' +
                ", sponsor='" + sponsor + '\'' +
                ", fullScale='" + fullScale + '\'' +
                ", fullTime='" + fullTime + '\'' +
                ", sort='" + sort + '\'' +
                ", createuser='" + createuser + '\'' +
                ", createtime='" + createtime + '\'' +
                ", updateuser='" + updateuser + '\'' +
                ", updatetime='" + updatetime + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", num='" + num + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", xdUrl='" + xdUrl + '\'' +
                ", memberInfoMessageBean=" + memberInfoMessageBean +
                ", productTypeIdSelectPj='" + productTypeIdSelectPj + '\'' +
                ", loanrateSelectPj='" + loanrateSelectPj + '\'' +
                ", loanDateSelectPj='" + loanDateSelectPj + '\'' +
                ", repaymentSelectPj='" + repaymentSelectPj + '\'' +
                ", onlineSelect='" + onlineSelect + '\'' +
                ", selectMutli='" + selectMutli + '\'' +
                ", applymember='" + applymember + '\'' +
                ", type='" + type + '\'' +
                ", realname='" + realname + '\'' +
                ", loadFeeSum='" + loadFeeSum + '\'' +
                ", interestSum='" + interestSum + '\'' +
                ", pageStr='" + pageStr + '\'' +
                '}';
    }
}