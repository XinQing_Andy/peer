package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-27.
 * 项目登记前查询
 */
public class ProductHuanXunMessageBean {

    /** 返回状态(success-成功、error-失败) **/
    private String result;
    /** 返回信息 **/
    private String info;
    /** 主键 **/
    private String id;
    /** 项目ID号(项目编号) **/
    private String code;
    /** 项目名称 **/
    private String title;
    /** 最小投标金额 **/
    private String bidFee;
    /** 最大拆分份数 **/
    private String splitCount;
    /** 贷款利率 **/
    private String loanRate;
    /** 贷款期限(年) **/
    private String loanYear;
    /** 贷款期限(月) **/
    private String loanMonth;
    /** 贷款期限(日) **/
    private String loadDay;
    /** 融资方id **/
    private String applyMember;
    /** 融资方姓名 **/
    private String realName;
    /** 融资方身份证号 **/
    private String idCard;
    /** 余额 **/
    private String balance;
    /** 项目ID号 **/
    private String projectNo;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getBidFee() {
        return bidFee;
    }
    public void setBidFee(String bidFee) {
        this.bidFee = bidFee;
    }

    public String getSplitCount() {
        return splitCount;
    }
    public void setSplitCount(String splitCount) {
        this.splitCount = splitCount;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getLoanYear() {
        return loanYear;
    }
    public void setLoanYear(String loanYear) {
        this.loanYear = loanYear;
    }

    public String getLoanMonth() {
        return loanMonth;
    }
    public void setLoanMonth(String loanMonth) {
        this.loanMonth = loanMonth;
    }

    public String getLoadDay() {
        return loadDay;
    }
    public void setLoadDay(String loadDay) {
        this.loadDay = loadDay;
    }

    public String getApplyMember() {
        return applyMember;
    }
    public void setApplyMember(String applyMember) {
        this.applyMember = applyMember;
    }

    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getIdCard() {
        return idCard;
    }
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getProjectNo() {
        return projectNo;
    }
    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    @Override
    public String toString() {
        return "ProductHuanXunMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", bidFee='" + bidFee + '\'' +
                ", splitCount='" + splitCount + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", loanYear='" + loanYear + '\'' +
                ", loanMonth='" + loanMonth + '\'' +
                ", loadDay='" + loadDay + '\'' +
                ", applyMember='" + applyMember + '\'' +
                ", realName='" + realName + '\'' +
                ", idCard='" + idCard + '\'' +
                ", balance='" + balance + '\'' +
                ", projectNo='" + projectNo + '\'' +
                '}';
    }
}
