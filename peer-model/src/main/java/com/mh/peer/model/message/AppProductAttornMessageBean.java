package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-14.
 */
public class AppProductAttornMessageBean {

    /** 主键 **/
    private String id;
    /** 产品购买id **/
    private String productBuyId;
    /** 转让价格 **/
    private String attronPrice;
    /** 预期收益率 **/
    private String expectedRate;
    /** 审核状态(0-审核通过、1-审核不通过、2-待审核) **/
    private String shFlag;
    /** 审核意见 **/
    private String shyj;
    /** 承接人 **/
    private String underTakeMember;
    /** 承接时间 **/
    private String underTakeTime;
    /** 创建时间 **/
    private String createTime;
    /** 产品id **/
    private String productId;
    /** 投资人 **/
    private String buyer;
    /** 题目 **/
    private String title;
    /** 编码 **/
    private String code;
    /** 产品类型id **/
    private String typeId;
    /** 剩余期限 **/
    private String surplusDays;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductBuyId() {
        return productBuyId;
    }
    public void setProductBuyId(String productBuyId) {
        this.productBuyId = productBuyId;
    }

    public String getAttronPrice() {
        return attronPrice;
    }
    public void setAttronPrice(String attronPrice) {
        this.attronPrice = attronPrice;
    }

    public String getExpectedRate() {
        return expectedRate;
    }
    public void setExpectedRate(String expectedRate) {
        this.expectedRate = expectedRate;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getShyj() {
        return shyj;
    }
    public void setShyj(String shyj) {
        this.shyj = shyj;
    }

    public String getUnderTakeMember() {
        return underTakeMember;
    }
    public void setUnderTakeMember(String underTakeMember) {
        this.underTakeMember = underTakeMember;
    }

    public String getUnderTakeTime() {
        return underTakeTime;
    }
    public void setUnderTakeTime(String underTakeTime) {
        this.underTakeTime = underTakeTime;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBuyer() {
        return buyer;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getSurplusDays() {
        return surplusDays;
    }
    public void setSurplusDays(String surplusDays) {
        this.surplusDays = surplusDays;
    }

    @Override
    public String toString() {
        return "AppProductAttornMessageBean{" +
                "id='" + id + '\'' +
                ", productBuyId='" + productBuyId + '\'' +
                ", attronPrice='" + attronPrice + '\'' +
                ", expectedRate='" + expectedRate + '\'' +
                ", shFlag='" + shFlag + '\'' +
                ", shyj='" + shyj + '\'' +
                ", underTakeMember='" + underTakeMember + '\'' +
                ", underTakeTime='" + underTakeTime + '\'' +
                ", createTime='" + createTime + '\'' +
                ", productId='" + productId + '\'' +
                ", buyer='" + buyer + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", typeId='" + typeId + '\'' +
                ", surplusDays='" + surplusDays + '\'' +
                '}';
    }
}
