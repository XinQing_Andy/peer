package com.mh.peer.model.business;

/**
 * Created by zhangerxin on 2016-4-10.
 */
public class ProductBusinessBean {

    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;

    /** 主键 **/
    private String id;
    /** 题目 **/
    private String title;
    /** 编码 **/
    private String code;
    /** 借款标类型 **/
    private String productType;
    /** 状态(0-上架、1-下架) **/
    private String flag;
    /** 适合人群 **/
    private String crowds;
    /** 亮点 **/
    private String highpoint;
    /** 额度范围(起始) **/
    private String creditstart;
    /** 额度范围(截止) **/
    private String creditend;
    /** 贷款利率 **/
    private String loanrate;
    /** 贷款期限(年) **/
    private String loanyear;
    /** 贷款期限(月) **/
    private String loanmonth;
    /** 贷款期限(日) **/
    private String loadday;
    /** 投标时间(日) **/
    private String bidday;
    /** 审核时间 **/
    private String checktime;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 投标金额 **/
    private String bidfee;
    /** 拆分份数 **/
    private String splitcount;
    /** 手续费 **/
    private String counterfee;
    /** 申请人条件 **/
    private String applycondition;
    /** 上线时间 **/
    private String onlinetime;
    /** 担保人 **/
    private String sponsor;
    /** 排序 **/
    private String sort;
    /** 创建人 **/
    private String createuser;
    /** 创建时间 **/
    private String createtime;
    /** 更新人 **/
    private String updateuser;
    /** 更新时间 **/
    private String updatetime;
    /** 二级菜单URL **/
    private String menuUrl;
    /** 当前数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 序号 **/
    private String num;
    /** 图片地址 **/
    private String imgUrl;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCrowds() {
        return crowds;
    }
    public void setCrowds(String crowds) {
        this.crowds = crowds;
    }

    public String getHighpoint() {
        return highpoint;
    }
    public void setHighpoint(String highpoint) {
        this.highpoint = highpoint;
    }

    public String getCreditstart() {
        return creditstart;
    }
    public void setCreditstart(String creditstart) {
        this.creditstart = creditstart;
    }

    public String getCreditend() {
        return creditend;
    }
    public void setCreditend(String creditend) {
        this.creditend = creditend;
    }

    public String getLoanrate() {
        return loanrate;
    }
    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getLoanyear() {
        return loanyear;
    }
    public void setLoanyear(String loanyear) {
        this.loanyear = loanyear;
    }

    public String getLoanmonth() {
        return loanmonth;
    }
    public void setLoanmonth(String loanmonth) {
        this.loanmonth = loanmonth;
    }

    public String getLoadday() {
        return loadday;
    }
    public void setLoadday(String loadday) {
        this.loadday = loadday;
    }

    public String getBidday() {
        return bidday;
    }
    public void setBidday(String bidday) {
        this.bidday = bidday;
    }

    public String getChecktime() {
        return checktime;
    }
    public void setChecktime(String checktime) {
        this.checktime = checktime;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getBidfee() {
        return bidfee;
    }
    public void setBidfee(String bidfee) {
        this.bidfee = bidfee;
    }

    public String getSplitcount() {
        return splitcount;
    }
    public void setSplitcount(String splitcount) {
        this.splitcount = splitcount;
    }

    public String getCounterfee() {
        return counterfee;
    }
    public void setCounterfee(String counterfee) {
        this.counterfee = counterfee;
    }

    public String getApplycondition() {
        return applycondition;
    }
    public void setApplycondition(String applycondition) {
        this.applycondition = applycondition;
    }

    public String getOnlinetime() {
        return onlinetime;
    }
    public void setOnlinetime(String onlinetime) {
        this.onlinetime = onlinetime;
    }

    public String getSponsor() {
        return sponsor;
    }
    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getCreateuser() {
        return createuser;
    }
    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCreatetime() {
        return createtime;
    }
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdateuser() {
        return updateuser;
    }
    public void setUpdateuser(String updateuser) {
        this.updateuser = updateuser;
    }

    public String getUpdatetime() {
        return updatetime;
    }
    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getSort() {
        return sort;
    }
    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getProductType() {
        return productType;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getImgUrl() {
        return imgUrl;
    }
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    public String toString() {
        return "ProductBusinessBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", productType='" + productType + '\'' +
                ", flag='" + flag + '\'' +
                ", crowds='" + crowds + '\'' +
                ", highpoint='" + highpoint + '\'' +
                ", creditstart='" + creditstart + '\'' +
                ", creditend='" + creditend + '\'' +
                ", loanrate='" + loanrate + '\'' +
                ", loanyear='" + loanyear + '\'' +
                ", loanmonth='" + loanmonth + '\'' +
                ", loadday='" + loadday + '\'' +
                ", bidday='" + bidday + '\'' +
                ", checktime='" + checktime + '\'' +
                ", repayment='" + repayment + '\'' +
                ", bidfee='" + bidfee + '\'' +
                ", splitcount='" + splitcount + '\'' +
                ", counterfee='" + counterfee + '\'' +
                ", applycondition='" + applycondition + '\'' +
                ", onlinetime='" + onlinetime + '\'' +
                ", sponsor='" + sponsor + '\'' +
                ", sort='" + sort + '\'' +
                ", createuser='" + createuser + '\'' +
                ", createtime='" + createtime + '\'' +
                ", updateuser='" + updateuser + '\'' +
                ", updatetime='" + updatetime + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", num='" + num + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }
}
