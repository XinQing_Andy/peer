package com.mh.peer.model.message;

/**
 * 用户组
 * @author zhangerxin
 */
public class SysRoleMessageBean {
    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;

    /** ID主键 **/
    private String id;
    /** 角色名称 **/
    private String roleName;
    /** 序号 **/
    private String num;
    /** 角色编码 **/
    private String roleCode;
    /** 角色建立者 **/
    private String createUserId;
    /** 是否被删除（0：未删除；1：已逻辑删除） **/
    private String isDelete;
    /** 状态（0：启用；1：禁用） **/
    private String state;
    /** 角色描述 **/
    private String roleDescription;
    /** 模板URL **/
    private String menuUrl;
    /** 控制类URL **/
    private String controlUrl;
    /** 用户数量 **/
    private String userCount;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 列表内容 **/
    private String htmlText;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 所属分页拼接 **/
    private String roleStr;


    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }

    public String getRoleCode() {
        return roleCode;
    }
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getCreateUserId() {
        return createUserId;
    }
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getIsDelete() {
        return isDelete;
    }
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getRoleDescription() {
        return roleDescription;
    }
    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getControlUrl() {
        return controlUrl;
    }
    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    public String getUserCount() {
        return userCount;
    }
    public void setUserCount(String userCount) {
        this.userCount = userCount;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getHtmlText() {
        return htmlText;
    }
    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getRoleStr() {
        return roleStr;
    }
    public void setRoleStr(String roleStr) {
        this.roleStr = roleStr;
    }

    @Override
    public String toString() {
        return "SysRoleMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", roleName='" + roleName + '\'' +
                ", num='" + num + '\'' +
                ", roleCode='" + roleCode + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", isDelete='" + isDelete + '\'' +
                ", state='" + state + '\'' +
                ", roleDescription='" + roleDescription + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", controlUrl='" + controlUrl + '\'' +
                ", userCount='" + userCount + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", htmlText='" + htmlText + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", roleStr='" + roleStr + '\'' +
                '}';
    }
}
