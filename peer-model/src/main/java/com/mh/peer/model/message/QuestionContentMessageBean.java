package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/4/19.
 */
public class QuestionContentMessageBean {
    /** 主键 **/
    private int id;
    /** 问题 **/
    private String question;
    /** 答案 **/
    private String answer;
    /** 作者 **/
    private String autor;
    /** 排序 **/
    private int sort;
    /** 状态(0-启用、暂停) **/
    private String flag;
    /** 创建人 **/
    private String createuser;
    /** 创建时间 **/
    private String createtime;
    /** 更新人 **/
    private String updateuser;
    /** 最后更新时间 **/
    private String updatetime;
    /**  **/
    private String info;
    /**  **/
    private String result;
    /**  **/
    private String menuUrl;
    /**  **/
    private String jetxName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(String updateuser) {
        this.updateuser = updateuser;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    @Override
    public String toString() {
        return "QuestionContentMessageBean{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", autor='" + autor + '\'' +
                ", sort=" + sort +
                ", flag='" + flag + '\'' +
                ", createuser='" + createuser + '\'' +
                ", createtime='" + createtime + '\'' +
                ", updateuser='" + updateuser + '\'' +
                ", updatetime='" + updatetime + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", jetxName='" + jetxName + '\'' +
                '}';
    }
}
