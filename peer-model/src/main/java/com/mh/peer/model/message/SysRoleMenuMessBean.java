package com.mh.peer.model.message;

import com.mh.peer.model.entity.SysRoleMenu;
import java.util.List;

/**
 * Created by zhaojiaming on 2016/1/29.
 */
public class SysRoleMenuMessBean {
    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;
    /**角色ID**/
    private String roleId;
    /**角色名称**/
    private String roleName;
    /**菜单ID**/
    private String menuId;
    /**功能ID**/
    private String functionId;
    /**功能名称**/
    private String functionName;
    /** 菜单层级 **/
    private String levelMenu;
    /** 菜单序号 **/
    private String menuSeq;
    /** 一级菜单 **/
    private String topMenu;
    /** 二级菜单 **/
    private String secondMenu;
    /** 三级菜单 **/
    private String thirdMenu;
    /** 父级ID **/
    private String parentId;
    /**角色是否选中**/
    private boolean flg;
    /**
     * 模板名称
     */
    private String menuUrl;
    /**
     * 控制类URL
     */
    private String controlUrl;

    private List<SysRoleMenu> roleMenuList;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public boolean isFlg() {
        return flg;
    }

    public void setFlg(boolean flg) {
        this.flg = flg;
    }

    public String getLevelMenu() {
        return levelMenu;
    }

    public void setLevelMenu(String levelMenu) {
        this.levelMenu = levelMenu;
    }

    public String getMenuSeq() {
        return menuSeq;
    }

    public void setMenuSeq(String menuSeq) {
        this.menuSeq = menuSeq;
    }

    public String getTopMenu() {
        return topMenu;
    }

    public void setTopMenu(String topMenu) {
        this.topMenu = topMenu;
    }

    public String getSecondMenu() {
        return secondMenu;
    }

    public void setSecondMenu(String secondMenu) {
        this.secondMenu = secondMenu;
    }

    public String getThirdMenu() {
        return thirdMenu;
    }

    public void setThirdMenu(String thirdMenu) {
        this.thirdMenu = thirdMenu;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getControlUrl() {
        return controlUrl;
    }

    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    public List<SysRoleMenu> getRoleMenuList() {
        return roleMenuList;
    }

    public void setRoleMenuList(List<SysRoleMenu> roleMenuList) {
        this.roleMenuList = roleMenuList;
    }

    @Override
    public String toString() {
        return "SysRoleMenuMessBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", roleId='" + roleId + '\'' +
                ", roleName='" + roleName + '\'' +
                ", menuId='" + menuId + '\'' +
                ", functionId='" + functionId + '\'' +
                ", functionName='" + functionName + '\'' +
                ", levelMenu='" + levelMenu + '\'' +
                ", menuSeq='" + menuSeq + '\'' +
                ", topMenu='" + topMenu + '\'' +
                ", secondMenu='" + secondMenu + '\'' +
                ", thirdMenu='" + thirdMenu + '\'' +
                ", parentId='" + parentId + '\'' +
                ", flg=" + flg +
                ", menuUrl='" + menuUrl + '\'' +
                ", controlUrl='" + controlUrl + '\'' +
                ", roleMenuList=" + roleMenuList +
                '}';
    }
}
