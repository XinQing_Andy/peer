package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-8-30.
 */
public class ProductInfoMessageBean {

    /** 主键 **/
    private String id;
    /** 编号 **/
    private String code;
    /** 题目 **/
    private String title;
    /** 出借人id **/
    private String applyMemberId;
    /** 出借人姓名 **/
    private String applyMemberName;
    /** 产品类型id **/
    private String typeId;
    /** 产品类型名称 **/
    private String typeName;
    /** 发标日期 **/
    private String fullTime;
    /** 申请日期 **/
    private String applyTime;
    /** 年利率 **/
    private String loadRate;
    /** 期限 **/
    private String loanLength;
    /** 出借金额 **/
    private String loanPrice;
    /** 产品状态 **/
    private String productFlag;
    /** 还款方式 **/
    private String repayment;
    /** 审核状态 **/
    private String shFlag;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getApplyMemberId() {
        return applyMemberId;
    }
    public void setApplyMemberId(String applyMemberId) {
        this.applyMemberId = applyMemberId;
    }

    public String getApplyMemberName() {
        return applyMemberName;
    }
    public void setApplyMemberName(String applyMemberName) {
        this.applyMemberName = applyMemberName;
    }

    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getFullTime() {
        return fullTime;
    }
    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getApplyTime() {
        return applyTime;
    }
    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getLoadRate() {
        return loadRate;
    }
    public void setLoadRate(String loadRate) {
        this.loadRate = loadRate;
    }

    public String getLoanLength() {
        return loanLength;
    }
    public void setLoanLength(String loanLength) {
        this.loanLength = loanLength;
    }

    public String getLoanPrice() {
        return loanPrice;
    }
    public void setLoanPrice(String loanPrice) {
        this.loanPrice = loanPrice;
    }

    public String getProductFlag() {
        return productFlag;
    }
    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    @Override
    public String toString() {
        return "ProductInfoMessageBean{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", applyMemberId='" + applyMemberId + '\'' +
                ", applyMemberName='" + applyMemberName + '\'' +
                ", typeId='" + typeId + '\'' +
                ", typeName='" + typeName + '\'' +
                ", fullTime='" + fullTime + '\'' +
                ", applyTime='" + applyTime + '\'' +
                ", loadRate='" + loadRate + '\'' +
                ", loanLength='" + loanLength + '\'' +
                ", loanPrice='" + loanPrice + '\'' +
                ", productFlag='" + productFlag + '\'' +
                ", repayment='" + repayment + '\'' +
                ", shFlag='" + shFlag + '\'' +
                '}';
    }
}
