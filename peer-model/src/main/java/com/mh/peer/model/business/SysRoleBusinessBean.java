package com.mh.peer.model.business;

/**
 * Created by zhangerxin on 2016-4-6.
 */
public class SysRoleBusinessBean {

    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;

    /** ID主键 **/
    private String id;
    /** 角色名称 **/
    private String roleName;
    /** 角色编码 **/
    private String roleCode;
    /** 角色建立者 **/
    private String createUserId;
    /** 是否被删除（0：未删除；1：已逻辑删除） **/
    private String isDelete;
    /** 状态（0：启用；1：禁用） **/
    private String state;
    /** 角色描述 **/
    private String roleDescription;
    /** 模板URL **/
    private String menuUrl;
    /** 控制类URL **/
    private String controlUrl;
    /** 用户数量 **/
    private String userCount;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getCreateUserId() {
        return createUserId;
    }
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getIsDelete() {
        return isDelete;
    }
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getRoleDescription() {
        return roleDescription;
    }
    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getControlUrl() {
        return controlUrl;
    }
    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    public String getUserCount() {
        return userCount;
    }
    public void setUserCount(String userCount) {
        this.userCount = userCount;
    }
}
