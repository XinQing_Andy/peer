package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-6-17.
 */
public class AppHomeMessageBean {

    private String productId; //产品id
    private String loanRate; //年利率
    private String loanPrice; //贷款金额
    private String loanYear; //贷款期限(年)
    private String loanMonth; //贷款期限(月)
    private String loanDay; //贷款期限(天)
    private String loanTimeLength; //贷款期限(总时长)
    private String advertId; //广告id
    private String advertTitle; //广告题目
    private String advertFileId; //广告附件id
    private String advertFileUrl; //广告附件地址
    private String newsId; //新闻id
    private String newsTitle; //新闻题目
    private String newsShowTime; //新闻展示时间
    private String memberId; //会员id
    private String lastDayPrice; //昨天收益

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getLoanPrice() {
        return loanPrice;
    }
    public void setLoanPrice(String loanPrice) {
        this.loanPrice = loanPrice;
    }

    public String getLoanYear() {
        return loanYear;
    }
    public void setLoanYear(String loanYear) {
        this.loanYear = loanYear;
    }

    public String getLoanMonth() {
        return loanMonth;
    }
    public void setLoanMonth(String loanMonth) {
        this.loanMonth = loanMonth;
    }

    public String getLoanDay() {
        return loanDay;
    }
    public void setLoanDay(String loanDay) {
        this.loanDay = loanDay;
    }

    public String getLoanTimeLength() {
        return loanTimeLength;
    }
    public void setLoanTimeLength(String loanTimeLength) {
        this.loanTimeLength = loanTimeLength;
    }

    public String getAdvertId() {
        return advertId;
    }
    public void setAdvertId(String advertId) {
        this.advertId = advertId;
    }

    public String getAdvertTitle() {
        return advertTitle;
    }
    public void setAdvertTitle(String advertTitle) {
        this.advertTitle = advertTitle;
    }

    public String getAdvertFileId() {
        return advertFileId;
    }
    public void setAdvertFileId(String advertFileId) {
        this.advertFileId = advertFileId;
    }

    public String getAdvertFileUrl() {
        return advertFileUrl;
    }
    public void setAdvertFileUrl(String advertFileUrl) {
        this.advertFileUrl = advertFileUrl;
    }

    public String getNewsId() {
        return newsId;
    }
    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getNewsTitle() {
        return newsTitle;
    }
    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsShowTime() {
        return newsShowTime;
    }
    public void setNewsShowTime(String newsShowTime) {
        this.newsShowTime = newsShowTime;
    }

    public String getMemberId() {
        return memberId;
    }
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getLastDayPrice() {
        return lastDayPrice;
    }
    public void setLastDayPrice(String lastDayPrice) {
        this.lastDayPrice = lastDayPrice;
    }

    @Override
    public String toString() {
        return "AppHomeMessageBean{" +
                "productId='" + productId + '\'' +
                ", loanRate='" + loanRate + '\'' +
                ", loanPrice='" + loanPrice + '\'' +
                ", loanYear='" + loanYear + '\'' +
                ", loanMonth='" + loanMonth + '\'' +
                ", loanDay='" + loanDay + '\'' +
                ", loanTimeLength='" + loanTimeLength + '\'' +
                ", advertId='" + advertId + '\'' +
                ", advertTitle='" + advertTitle + '\'' +
                ", advertFileId='" + advertFileId + '\'' +
                ", advertFileUrl='" + advertFileUrl + '\'' +
                ", newsId='" + newsId + '\'' +
                ", newsTitle='" + newsTitle + '\'' +
                ", newsShowTime='" + newsShowTime + '\'' +
                ", memberId='" + memberId + '\'' +
                ", lastDayPrice='" + lastDayPrice + '\'' +
                '}';
    }
}
