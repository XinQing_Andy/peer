package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/5/23.
 */
public class ProductReceiveMessageBean {
    /** 主键 **/
    private String id;
    /** 购买id **/
    private String buyid;
    /** 应收本金 **/
    private String principal;
    /** 应收利息 **/
    private String interest;
    /** 应收金额合计 **/
    private String sumPrice;
    /** 应回款日期 **/
    private String yreceivetime;
    /** 回款金额 **/
    private String receiveprice;
    /** 实际回款日期 **/
    private String sreceivetime;
    /** 创建日期 **/
    private String cteatetime;
    /** 最后更新日期 **/
    private String updatetime;
    /** 购买人id **/
    private String buyer;
    /** 账户余额 **/
    private String balance;
    /** 题目 **/
    private String title;
    /** 借款标编号 **/
    private String code;
    /** 逾期天数 **/
    private String overduedays;
    /** 产品id **/
    private String productid;
    /** 状态 **/
    private String receiveflag;
    /** 字符串拼接 **/
    private String Str;
    /** 字符串拼接 **/
    private String Str2;
    /** 返回信息 **/
    private String info;
    /** 返回结果 **/
    private String result;
    /** 当前数 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 当前页 **/
    private String currentPage;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;
    /** 开始时间 **/
    private String startTime;
    /**  结束时间**/
    private String endTime;
    /** 收款进度 **/
    private String progress;
    /** 应收本金合计 **/
    private String principalTotal;
    /**  应收利息合计**/
    private String interestTotal;
    /**  应收总额合计**/
    private String receivepriceTotal;
    /** 待收本金合计 **/
    private String dsPrincipalTotal;
    /** 待收利息合计 **/
    private String dsInterestTotal;
    /** 待收总额合计 **/
    private String dsPriceTotal;
    /** 已收本金合计 **/
    private String ysPrincipalTotal;
    /** 已收利息合计 **/
    private String ysInterestTotal;
    /** 已收总额合计 **/
    private String ysPriceTotal;
    /** 管理费 **/
    private String managePrice;
    /** 状态(0-待回收、1-已回收) **/
    private String isRecycle;
    /** 菜单url **/
    private String menuUrl;
    /** 逾期天数 **/
    private String overDuedays;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getBuyid() {
        return buyid;
    }
    public void setBuyid(String buyid) {
        this.buyid = buyid;
    }

    public String getPrincipal() {
        return principal;
    }
    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getInterest() {
        return interest;
    }
    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getSumPrice() {
        return sumPrice;
    }
    public void setSumPrice(String sumPrice) {
        this.sumPrice = sumPrice;
    }

    public String getYreceivetime() {
        return yreceivetime;
    }
    public void setYreceivetime(String yreceivetime) {
        this.yreceivetime = yreceivetime;
    }

    public String getReceiveprice() {
        return receiveprice;
    }
    public void setReceiveprice(String receiveprice) {
        this.receiveprice = receiveprice;
    }

    public String getSreceivetime() {
        return sreceivetime;
    }
    public void setSreceivetime(String sreceivetime) {
        this.sreceivetime = sreceivetime;
    }

    public String getCteatetime() {
        return cteatetime;
    }
    public void setCteatetime(String cteatetime) {
        this.cteatetime = cteatetime;
    }

    public String getUpdatetime() {
        return updatetime;
    }
    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getBuyer() {
        return buyer;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getOverduedays() {
        return overduedays;
    }
    public void setOverduedays(String overduedays) {
        this.overduedays = overduedays;
    }

    public String getProductid() {
        return productid;
    }
    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getReceiveflag() {
        return receiveflag;
    }
    public void setReceiveflag(String receiveflag) {
        this.receiveflag = receiveflag;
    }

    public String getStr() {
        return Str;
    }
    public void setStr(String str) {
        Str = str;
    }

    public String getStr2() {
        return Str2;
    }
    public void setStr2(String str2) {
        Str2 = str2;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getProgress() {
        return progress;
    }
    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getPrincipalTotal() {
        return principalTotal;
    }
    public void setPrincipalTotal(String principalTotal) {
        this.principalTotal = principalTotal;
    }

    public String getInterestTotal() {
        return interestTotal;
    }
    public void setInterestTotal(String interestTotal) {
        this.interestTotal = interestTotal;
    }

    public String getReceivepriceTotal() {
        return receivepriceTotal;
    }
    public void setReceivepriceTotal(String receivepriceTotal) {
        this.receivepriceTotal = receivepriceTotal;
    }

    public String getDsPrincipalTotal() {
        return dsPrincipalTotal;
    }
    public void setDsPrincipalTotal(String dsPrincipalTotal) {
        this.dsPrincipalTotal = dsPrincipalTotal;
    }

    public String getDsInterestTotal() {
        return dsInterestTotal;
    }
    public void setDsInterestTotal(String dsInterestTotal) {
        this.dsInterestTotal = dsInterestTotal;
    }

    public String getDsPriceTotal() {
        return dsPriceTotal;
    }
    public void setDsPriceTotal(String dsPriceTotal) {
        this.dsPriceTotal = dsPriceTotal;
    }

    public String getYsPrincipalTotal() {
        return ysPrincipalTotal;
    }
    public void setYsPrincipalTotal(String ysPrincipalTotal) {
        this.ysPrincipalTotal = ysPrincipalTotal;
    }

    public String getYsInterestTotal() {
        return ysInterestTotal;
    }
    public void setYsInterestTotal(String ysInterestTotal) {
        this.ysInterestTotal = ysInterestTotal;
    }

    public String getYsPriceTotal() {
        return ysPriceTotal;
    }
    public void setYsPriceTotal(String ysPriceTotal) {
        this.ysPriceTotal = ysPriceTotal;
    }

    public String getManagePrice() {
        return managePrice;
    }
    public void setManagePrice(String managePrice) {
        this.managePrice = managePrice;
    }

    public String getIsRecycle() {
        return isRecycle;
    }
    public void setIsRecycle(String isRecycle) {
        this.isRecycle = isRecycle;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getOverDuedays() {
        return overDuedays;
    }
    public void setOverDuedays(String overDuedays) {
        this.overDuedays = overDuedays;
    }

    @Override
    public String toString() {
        return "ProductReceiveMessageBean{" +
                "id='" + id + '\'' +
                ", buyid='" + buyid + '\'' +
                ", principal='" + principal + '\'' +
                ", interest='" + interest + '\'' +
                ", sumPrice='" + sumPrice + '\'' +
                ", yreceivetime='" + yreceivetime + '\'' +
                ", receiveprice='" + receiveprice + '\'' +
                ", sreceivetime='" + sreceivetime + '\'' +
                ", cteatetime='" + cteatetime + '\'' +
                ", updatetime='" + updatetime + '\'' +
                ", buyer='" + buyer + '\'' +
                ", balance='" + balance + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", overduedays='" + overduedays + '\'' +
                ", productid='" + productid + '\'' +
                ", receiveflag='" + receiveflag + '\'' +
                ", Str='" + Str + '\'' +
                ", Str2='" + Str2 + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", orderType='" + orderType + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", progress='" + progress + '\'' +
                ", principalTotal='" + principalTotal + '\'' +
                ", interestTotal='" + interestTotal + '\'' +
                ", receivepriceTotal='" + receivepriceTotal + '\'' +
                ", dsPrincipalTotal='" + dsPrincipalTotal + '\'' +
                ", dsInterestTotal='" + dsInterestTotal + '\'' +
                ", dsPriceTotal='" + dsPriceTotal + '\'' +
                ", ysPrincipalTotal='" + ysPrincipalTotal + '\'' +
                ", ysInterestTotal='" + ysInterestTotal + '\'' +
                ", ysPriceTotal='" + ysPriceTotal + '\'' +
                ", managePrice='" + managePrice + '\'' +
                ", isRecycle='" + isRecycle + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", overDuedays='" + overDuedays + '\'' +
                '}';
    }
}
