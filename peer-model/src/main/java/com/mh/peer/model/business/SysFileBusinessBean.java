package com.mh.peer.model.business;

/**
 * Created by zhangerxin on 2016-4-10.
 */
public class SysFileBusinessBean {

    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;

    /** 主键 **/
    private String id;
    /** 关联id **/
    private String glId;
    /** 附件地址 **/
    private String imgUrl;
    /** 附件地址(绝对路径) **/
    private String imgUrlJd;
    /** 类型 **/
    private String type;
    /** 创建用户 **/
    private String createUser;
    /** 创建时间 **/
    private String createTime;


    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getGlId() {
        return glId;
    }
    public void setGlId(String glId) {
        this.glId = glId;
    }

    public String getImgUrl() {
        return imgUrl;
    }
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgUrlJd() {
        return imgUrlJd;
    }
    public void setImgUrlJd(String imgUrlJd) {
        this.imgUrlJd = imgUrlJd;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getCreateUser() {
        return createUser;
    }
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SysFileBusinessBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", glId='" + glId + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", imgUrlJd='" + imgUrlJd + '\'' +
                ", type='" + type + '\'' +
                ", createUser='" + createUser + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
