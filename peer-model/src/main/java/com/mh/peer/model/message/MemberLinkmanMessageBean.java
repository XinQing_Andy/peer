package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/5/5.
 */
public class MemberLinkmanMessageBean {
    /** 主键 **/
    private String id;
    /** 会员id **/
    private String memberid;
    /** 姓名 **/
    private String xm;
    /** 手机号 **/
    private String telephone;
    /** 与联系人关系(0-父母、1-配偶、2-子女、3-兄弟姐妹、4-朋友、5-同事、6-其他) **/
    private String relationship;
    /** 操作时间 **/
    private String createtime;
    /**  **/
    private String info;
    /**  **/
    private String result;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "MemberLinkmanMessageBean{" +
                "id='" + id + '\'' +
                ", memberid='" + memberid + '\'' +
                ", xm='" + xm + '\'' +
                ", telephone='" + telephone + '\'' +
                ", relationship='" + relationship + '\'' +
                ", createtime='" + createtime + '\'' +
                ", info='" + info + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
