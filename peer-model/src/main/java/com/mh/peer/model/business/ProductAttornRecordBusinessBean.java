package com.mh.peer.model.business;

/**
 * Created by zhangerxin on 2016-5-23.
 */
public class ProductAttornRecordBusinessBean {

    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;

    /** 主键 **/
    private String id;
    /** 产品id **/
    private String productbuyId;
    /** 价格 **/
    private String price;
    /** 转让状态(0-转让成功、1-转让不成功、2-待转让) **/
    private String type;
    /** 审核状态 **/
    private String shFlag;
    /** 审核意见 **/
    private String shyj;
    /** 承接人 **/
    private String undertakeMember;
    /** 承接时间 **/
    private String undertakeTime;
    /** 创建时间 **/
    private String createTime;

    private String jetxName;
    /**二级菜单URL**/
    private String menuUrl;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductbuyId() {
        return productbuyId;
    }
    public void setProductbuyId(String productbuyId) {
        this.productbuyId = productbuyId;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getShyj() {
        return shyj;
    }
    public void setShyj(String shyj) {
        this.shyj = shyj;
    }

    public String getUndertakeMember() {
        return undertakeMember;
    }
    public void setUndertakeMember(String undertakeMember) {
        this.undertakeMember = undertakeMember;
    }

    public String getUndertakeTime() {
        return undertakeTime;
    }
    public void setUndertakeTime(String undertakeTime) {
        this.undertakeTime = undertakeTime;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getJetxName() {
        return jetxName;
    }
    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }
}
