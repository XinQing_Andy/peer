package com.mh.peer.model.message;

/**
 * Created by WangMeng on 2015/12/10.
 */
public class UserMenuMessageBean {
    /**
     * 模板名称
     */
    private String jetxName;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 父级菜单
     */
    private String menuParentId;
    /**
     * 控制类的URL
     */
    private String controlUrl;

    /**
     * 一行数据ID
     * @return
     */
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getMenuParentId() {
        return menuParentId;
    }

    public void setMenuParentId(String menuParentId) {
        this.menuParentId = menuParentId;
    }

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    public String getControlUrl() {
        return controlUrl;
    }

    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

    @Override
    public String toString() {
        return "UserMenuMessageBean{" +
                "jetxName='" + jetxName + '\'' +
                ", userId='" + userId + '\'' +
                ", menuParentId='" + menuParentId + '\'' +
                ", controlUrl='" + controlUrl + '\'' +
                ", id=" + id +
                '}';
    }
}
