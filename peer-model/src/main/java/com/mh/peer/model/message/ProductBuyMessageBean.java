package com.mh.peer.model.message;

/**
 * Created by Cuibin on 2016/7/30.
 */
public class ProductBuyMessageBean {

    /** 主键 **/
    private String id;
    /** 产品id **/
    private String productId;
    /** 购买人id **/
    private String buyer;
    /** 购买金额 **/
    private String price;
    /** 购买时间 **/
    private String buytime;
    /** 类型 0-持有 1-转让中 2-已转让 **/
    private String type;
    /** 操作时间 **/
    private String createtime;
    /** 题目 **/
    private String title;
    /** 编码 **/
    private String code;
    /** 年利率 **/
    private String loanrate;
    /** 购买人姓名 **/
    private String buyerName;
    /** 拼接字符串 **/
    private String str;
    /** 开始数 **/
    private String pageIndex;
    /** 每页数量 **/
    private String pageSize;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 当前页 **/
    private String currentPage;
    /** 当前页码 **/
    private String pageStr;
    /** 总数量 **/
    private String totalCount;
    /** 审核状态 **/
    private String shFlag;
    /** 投资状态 **/
    private String investFlg;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBuyer() {
        return buyer;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getBuytime() {
        return buytime;
    }
    public void setBuytime(String buytime) {
        this.buytime = buytime;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getCreatetime() {
        return createtime;
    }
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getLoanrate() {
        return loanrate;
    }
    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getBuyerName() {
        return buyerName;
    }
    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getStr() {
        return str;
    }
    public void setStr(String str) {
        this.str = str;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageStr() {
        return pageStr;
    }
    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getShFlag() {
        return shFlag;
    }
    public void setShFlag(String shFlag) {
        this.shFlag = shFlag;
    }

    public String getInvestFlg() {
        return investFlg;
    }
    public void setInvestFlg(String investFlg) {
        this.investFlg = investFlg;
    }


    @Override
    public String toString() {
        return "ProductBuyMessageBean{" +
                "id='" + id + '\'' +
                ", productId='" + productId + '\'' +
                ", buyer='" + buyer + '\'' +
                ", price='" + price + '\'' +
                ", buytime='" + buytime + '\'' +
                ", type='" + type + '\'' +
                ", createtime='" + createtime + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", loanrate='" + loanrate + '\'' +
                ", buyerName='" + buyerName + '\'' +
                ", str='" + str + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", pageStr='" + pageStr + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", shFlag='" + shFlag + '\'' +
                ", investFlg='" + investFlg + '\'' +
                '}';
    }
}
