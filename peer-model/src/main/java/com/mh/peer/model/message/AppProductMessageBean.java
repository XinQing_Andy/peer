package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-4.
 */
public class AppProductMessageBean {

    /** 信息 **/
    private String info;
    /** 返回标志 **/
    private String resultCode;
    /** 产品主键 **/
    private String id;
    /** 产品类型id **/
    private String typeId;
    /** 产品题目 **/
    private String title;
    /** 状态(0-上架、1-下架) **/
    private String flag;
    /** 还款方式(0-按月还款、等额本息、1-按月付息、到期还本、2-一次性还款) **/
    private String repayment;
    /** 年利率 **/
    private String loanRate;
    /** 借款期限(年) **/
    private String loanYear;
    /** 借款期限(月) **/
    private String loanMonth;
    /** 借款期限(日) **/
    private String loanDay;
    /** 借款时长 **/
    private String loanLength;
    /** 借款金额 **/
    private String loadFee;
    /** 是否满标(0-是、1-否) **/
    private String fullScale;
    /** 借款天数 **/
    private String loanDays;
    /** 上线日期 **/
    private String onlineTime;
    /** 当前数 **/
    private String pageIndex;
    /** 每次显示数量 **/
    private String pageSize;
    /** 排序字段 **/
    private String orderColumn;
    /** 排序类型 **/
    private String orderType;

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getResultCode() {
        return resultCode;
    }
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getRepayment() {
        return repayment;
    }
    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getLoanRate() {
        return loanRate;
    }
    public void setLoanRate(String loanRate) {
        this.loanRate = loanRate;
    }

    public String getLoanYear() {
        return loanYear;
    }
    public void setLoanYear(String loanYear) {
        this.loanYear = loanYear;
    }

    public String getLoanMonth() {
        return loanMonth;
    }
    public void setLoanMonth(String loanMonth) {
        this.loanMonth = loanMonth;
    }

    public String getLoanDay() {
        return loanDay;
    }
    public void setLoanDay(String loanDay) {
        this.loanDay = loanDay;
    }

    public String getLoanLength() {
        return loanLength;
    }
    public void setLoanLength(String loanLength) {
        this.loanLength = loanLength;
    }

    public String getLoadFee() {
        return loadFee;
    }
    public void setLoadFee(String loadFee) {
        this.loadFee = loadFee;
    }

    public String getFullScale() {
        return fullScale;
    }
    public void setFullScale(String fullScale) {
        this.fullScale = fullScale;
    }

    public String getLoanDays() {
        return loanDays;
    }
    public void setLoanDays(String loanDays) {
        this.loanDays = loanDays;
    }

    public String getOnlineTime() {
        return onlineTime;
    }
    public void setOnlineTime(String onlineTime) {
        this.onlineTime = onlineTime;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
}
