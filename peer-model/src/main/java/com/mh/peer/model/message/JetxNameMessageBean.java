package com.mh.peer.model.message;

/**
 * Created by wanghongjian on 2016/3/26.
 */
public class JetxNameMessageBean {

    private String jetxName;

    public String getJetxName() {
        return jetxName;
    }

    public void setJetxName(String jetxName) {
        this.jetxName = jetxName;
    }

    @Override
    public String toString() {
        return "JetxNameMessageBean{" +
                "jetxName='" + jetxName + '\'' +
                '}';
    }
}
