package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-11.
 * 回款统计
 */
public class AppProductReceiveTjMessageBean {

    /** 待收金额 **/
    private String dsPrice;
    /** 待收投资笔数 **/
    private String dsInvestCount;
    /** 待收期数 **/
    private String dsPeriod;
    /** 已收金额 **/
    private String ysPrice;
    /** 已收投资笔数 **/
    private String ysInvestCount;
    /** 已收期数 **/
    private String ysPeriod;

    public String getDsPrice() {
        return dsPrice;
    }
    public void setDsPrice(String dsPrice) {
        this.dsPrice = dsPrice;
    }

    public String getDsInvestCount() {
        return dsInvestCount;
    }
    public void setDsInvestCount(String dsInvestCount) {
        this.dsInvestCount = dsInvestCount;
    }

    public String getDsPeriod() {
        return dsPeriod;
    }
    public void setDsPeriod(String dsPeriod) {
        this.dsPeriod = dsPeriod;
    }

    public String getYsPrice() {
        return ysPrice;
    }
    public void setYsPrice(String ysPrice) {
        this.ysPrice = ysPrice;
    }

    public String getYsInvestCount() {
        return ysInvestCount;
    }
    public void setYsInvestCount(String ysInvestCount) {
        this.ysInvestCount = ysInvestCount;
    }

    public String getYsPeriod() {
        return ysPeriod;
    }
    public void setYsPeriod(String ysPeriod) {
        this.ysPeriod = ysPeriod;
    }

    @Override
    public String toString() {
        return "AppProductReceiveTjMessageBean{" +
                "dsPrice='" + dsPrice + '\'' +
                ", dsInvestCount='" + dsInvestCount + '\'' +
                ", dsPeriod='" + dsPeriod + '\'' +
                ", ysPrice='" + ysPrice + '\'' +
                ", ysInvestCount='" + ysInvestCount + '\'' +
                ", ysPeriod='" + ysPeriod + '\'' +
                '}';
    }
}
