package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/6/7.
 */
public class MainGraphMessageBean {

    /** 昨天注册会员 **/
    private String[] yesterdayReg = new String[]{};
    /** 过去七天注册会员 **/
    private String[] lastWeekReg = new String[]{};
    /** 过去30天注册会员 **/
    private String[] lastMonthReg = new String[]{};
    /** 过去1天 投资数量 **/
    private String[] lastDayMoney = new String[]{};
    /** 过去7天 投资数量 **/
    private String[] lastWeekMoney = new String[]{};
    /** 过去30天 投资数量 **/
    private String[] lastMonthMoney = new String[]{};
    /** 昨天 投资金额 **/
    private String[] lastDayInvest = new String[]{};
    /** 最近七天投资金额 **/
    private String[] lastWeekInvest = new String[]{};
    /** 最近30天投资金额 **/
    private String[] lastMonthInvest = new String[]{};
    /** 显示的日期 **/
    private String[] showDate = new String[]{};

    public String[] getYesterdayReg() {
        return yesterdayReg;
    }

    public void setYesterdayReg(String[] yesterdayReg) {
        this.yesterdayReg = yesterdayReg;
    }

    public String[] getLastWeekReg() {
        return lastWeekReg;
    }

    public void setLastWeekReg(String[] lastWeekReg) {
        this.lastWeekReg = lastWeekReg;
    }

    public String[] getLastMonthReg() {
        return lastMonthReg;
    }

    public void setLastMonthReg(String[] lastMonthReg) {
        this.lastMonthReg = lastMonthReg;
    }

    public String[] getLastDayMoney() {
        return lastDayMoney;
    }

    public void setLastDayMoney(String[] lastDayMoney) {
        this.lastDayMoney = lastDayMoney;
    }

    public String[] getLastWeekMoney() {
        return lastWeekMoney;
    }

    public void setLastWeekMoney(String[] lastWeekMoney) {
        this.lastWeekMoney = lastWeekMoney;
    }

    public String[] getLastMonthMoney() {
        return lastMonthMoney;
    }

    public void setLastMonthMoney(String[] lastMonthMoney) {
        this.lastMonthMoney = lastMonthMoney;
    }

    public String[] getLastDayInvest() {
        return lastDayInvest;
    }

    public void setLastDayInvest(String[] lastDayInvest) {
        this.lastDayInvest = lastDayInvest;
    }

    public String[] getLastWeekInvest() {
        return lastWeekInvest;
    }

    public void setLastWeekInvest(String[] lastWeekInvest) {
        this.lastWeekInvest = lastWeekInvest;
    }

    public String[] getLastMonthInvest() {
        return lastMonthInvest;
    }

    public void setLastMonthInvest(String[] lastMonthInvest) {
        this.lastMonthInvest = lastMonthInvest;
    }

    public String[] getShowDate() {
        return showDate;
    }

    public void setShowDate(String[] showDate) {
        this.showDate = showDate;
    }
}
