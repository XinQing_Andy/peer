package com.mh.peer.model.business;

/**
 * Created by zhangerxin on 2016-5-9.
 */
public class ProductCreditorBusinessBean {

    /** 返回信息 **/
    private String info;
    /** 返回标志 **/
    private String result;
    /** 返回页面 **/
    private String menuUrl;
    /** 主键 **/
    private String id;
    /** 产品id **/
    private String productId;
    /** 交易密码 **/
    private String traderpassword;
    /** 购买人id **/
    private String buyer;
    /** 购买价格 **/
    private String price;
    /** 购买时间 **/
    private String buyTime;
    /** 类型 0-持有 1-转让中 2-已转让 **/
    private String type;
    /** 验证码 **/
    private String captcha;
    /** 操作时间 **/
    private String createTime;

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTraderpassword() {
        return traderpassword;
    }
    public void setTraderpassword(String traderpassword) {
        this.traderpassword = traderpassword;
    }

    public String getBuyer() {
        return buyer;
    }
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getBuyTime() {
        return buyTime;
    }
    public void setBuyTime(String buyTime) {
        this.buyTime = buyTime;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getCaptcha() {
        return captcha;
    }
    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ProductInvestBusinessBean{" +
                "info='" + info + '\'' +
                ", result='" + result + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", id='" + id + '\'' +
                ", productId='" + productId + '\'' +
                ", traderpassword='" + traderpassword + '\'' +
                ", buyer='" + buyer + '\'' +
                ", price='" + price + '\'' +
                ", buyTime='" + buyTime + '\'' +
                ", type='" + type + '\'' +
                ", captcha='" + captcha + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
