package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-16.
 */
public class AppMemberInvestMessageBean {

    /** 主键 **/
    private String id;
    /** 昵称 **/
    private String nickName;
    /** 投资金额 **/
    private String investPrice;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getInvestPrice() {
        return investPrice;
    }
    public void setInvestPrice(String investPrice) {
        this.investPrice = investPrice;
    }

    @Override
    public String toString() {
        return "AppMemberInvestMessageBean{" +
                "id='" + id + '\'' +
                ", nickName='" + nickName + '\'' +
                ", investPrice='" + investPrice + '\'' +
                '}';
    }
}
