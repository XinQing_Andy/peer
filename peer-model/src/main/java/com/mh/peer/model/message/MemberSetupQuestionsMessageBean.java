package com.mh.peer.model.message;

/**
 * Created by cuibin on 2016/5/7.
 */
public class MemberSetupQuestionsMessageBean {
    /** 主键 **/
    private int id;
    /** 会员id **/
    private String memberid;
    /** 问题1 **/
    private String questionone;
    /** 答案1 **/
    private String answerone;
    /** 问题2 **/
    private String questiontwo;
    /** 答案2 **/
    private String answertwo;
    /** 问题3 **/
    private String questionthree;
    /** 答案3 **/
    private String answerthree;
    /** 创建日期 **/
    private String createtime;
    /**  **/
    private String result;
    /**  **/
    private String info;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getQuestionone() {
        return questionone;
    }

    public void setQuestionone(String questionone) {
        this.questionone = questionone;
    }

    public String getAnswerone() {
        return answerone;
    }

    public void setAnswerone(String answerone) {
        this.answerone = answerone;
    }

    public String getQuestiontwo() {
        return questiontwo;
    }

    public void setQuestiontwo(String questiontwo) {
        this.questiontwo = questiontwo;
    }

    public String getAnswertwo() {
        return answertwo;
    }

    public void setAnswertwo(String answertwo) {
        this.answertwo = answertwo;
    }

    public String getQuestionthree() {
        return questionthree;
    }

    public void setQuestionthree(String questionthree) {
        this.questionthree = questionthree;
    }

    public String getAnswerthree() {
        return answerthree;
    }

    public void setAnswerthree(String answerthree) {
        this.answerthree = answerthree;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "MemberSetupQuestionsMessageBean{" +
                "id=" + id +
                ", memberid='" + memberid + '\'' +
                ", questionone='" + questionone + '\'' +
                ", answerone='" + answerone + '\'' +
                ", questiontwo='" + questiontwo + '\'' +
                ", answertwo='" + answertwo + '\'' +
                ", questionthree='" + questionthree + '\'' +
                ", answerthree='" + answerthree + '\'' +
                ", createtime='" + createtime + '\'' +
                ", result='" + result + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
