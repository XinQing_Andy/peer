package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-7-29.
 * 转账明细消息对象
 */
public class HuanXunTransferMessageBean {

    /** 程序返回状态 **/
    private String result;
    /** 程序返回信息 **/
    private String info;
    /** 主键 **/
    private String id;
    /** 债权id **/
    private String attornId;
    /** 产品id **/
    private String productId;
    /** 项目ID号 **/
    private String projectNo;
    /** IPS原冻结订单号 **/
    private String freezeId;
    /** 转出方IPS存管账户号 **/
    private String outIpsAcctNo;
    /** 转出方平台手续费 **/
    private String outMerFee;
    /** 转入方IPS存管账户号 **/
    private String inIpsAcctNo;
    /** 转入方平台手续费 **/
    private String inMerFee;
    /** 转账金额 **/
    private String trdAmt;
    /** 签名 **/
    private String sign;
    /** 请求信息 **/
    private String request;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getAttornId() {
        return attornId;
    }
    public void setAttornId(String attornId) {
        this.attornId = attornId;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProjectNo() {
        return projectNo;
    }
    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getFreezeId() {
        return freezeId;
    }
    public void setFreezeId(String freezeId) {
        this.freezeId = freezeId;
    }

    public String getOutIpsAcctNo() {
        return outIpsAcctNo;
    }
    public void setOutIpsAcctNo(String outIpsAcctNo) {
        this.outIpsAcctNo = outIpsAcctNo;
    }

    public String getOutMerFee() {
        return outMerFee;
    }
    public void setOutMerFee(String outMerFee) {
        this.outMerFee = outMerFee;
    }

    public String getInIpsAcctNo() {
        return inIpsAcctNo;
    }
    public void setInIpsAcctNo(String inIpsAcctNo) {
        this.inIpsAcctNo = inIpsAcctNo;
    }

    public String getInMerFee() {
        return inMerFee;
    }
    public void setInMerFee(String inMerFee) {
        this.inMerFee = inMerFee;
    }

    public String getTrdAmt() {
        return trdAmt;
    }
    public void setTrdAmt(String trdAmt) {
        this.trdAmt = trdAmt;
    }

    public String getSign() {
        return sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "HuanXunTransferMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", attornId='" + attornId + '\'' +
                ", productId='" + productId + '\'' +
                ", projectNo='" + projectNo + '\'' +
                ", freezeId='" + freezeId + '\'' +
                ", outIpsAcctNo='" + outIpsAcctNo + '\'' +
                ", outMerFee='" + outMerFee + '\'' +
                ", inIpsAcctNo='" + inIpsAcctNo + '\'' +
                ", inMerFee='" + inMerFee + '\'' +
                ", trdAmt='" + trdAmt + '\'' +
                ", sign='" + sign + '\'' +
                ", request='" + request + '\'' +
                '}';
    }
}
