package com.mh.peer.model.message;

/**
 * Created by zhangerxin on 2016-4-11.
 */
public class AdvertContentMessageBean {

    /**返回成功或失败(error,success)**/
    private String result;
    /**返回结果说明**/
    private String info;

    /** 主键 **/
    private String id;
    /** 类型id **/
    private String typeId;
    /** 题目 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 作者 **/
    private String autor;
    /** 排序 **/
    private String sort;
    /** 状态(0-有效、1-无效) **/
    private String flag;
    /** 浏览次数 **/
    private String look_Count;
    /** 创建用户 **/
    private String createUser;
    /** 创建时间 **/
    private String createTime;
    /** 菜单 **/
    private String menuUrl;
    /** 当前页 **/
    private String pageIndex;
    /** 每页显示数量 **/
    private String pageSize;
    /** 附件地址(相对地址) **/
    private String imgUrlXd;
    /** 附件地址(绝对地址) **/
    private String imgUrlJd;
    /** 附件id **/
    private String fileId;
    /** 总数量 **/
    private String totalCount;
    /** 当前页 **/
    private String currentPage;
    /** 关键字 **/
    private String keyword;
    /** 是否从首页点击 **/
    private String ishome;
    /** 排序类型 **/
    private String orderType;
    /** 排序字段 **/
    private String orderColumn;
    /** 广告部分拼接 **/
    private String advertStr;
    /** 分页部分拼接 **/
    private String pageStr;

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getAutor() {
        return autor;
    }
    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getSort() {
        return sort;
    }
    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getLook_Count() {
        return look_Count;
    }
    public void setLook_Count(String look_Count) {
        this.look_Count = look_Count;
    }

    public String getCreateUser() {
        return createUser;
    }
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getMenuUrl() {
        return menuUrl;
    }
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getImgUrlXd() {
        return imgUrlXd;
    }
    public void setImgUrlXd(String imgUrlXd) {
        this.imgUrlXd = imgUrlXd;
    }

    public String getImgUrlJd() {
        return imgUrlJd;
    }
    public void setImgUrlJd(String imgUrlJd) {
        this.imgUrlJd = imgUrlJd;
    }

    public String getFileId() {
        return fileId;
    }
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getIshome() {
        return ishome;
    }
    public void setIshome(String ishome) {
        this.ishome = ishome;
    }

    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderColumn() {
        return orderColumn;
    }
    public void setOrderColumn(String orderColumn) {
        this.orderColumn = orderColumn;
    }

    public String getAdvertStr() {
        return advertStr;
    }
    public void setAdvertStr(String advertStr) {
        this.advertStr = advertStr;
    }

    public String getPageStr() {
        return pageStr;
    }
    public void setPageStr(String pageStr) {
        this.pageStr = pageStr;
    }

    @Override
    public String toString() {
        return "AdvertContentMessageBean{" +
                "result='" + result + '\'' +
                ", info='" + info + '\'' +
                ", id='" + id + '\'' +
                ", typeId='" + typeId + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", autor='" + autor + '\'' +
                ", sort='" + sort + '\'' +
                ", flag='" + flag + '\'' +
                ", look_Count='" + look_Count + '\'' +
                ", createUser='" + createUser + '\'' +
                ", createTime='" + createTime + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", pageIndex='" + pageIndex + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", imgUrlXd='" + imgUrlXd + '\'' +
                ", imgUrlJd='" + imgUrlJd + '\'' +
                ", fileId='" + fileId + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", currentPage='" + currentPage + '\'' +
                ", keyword='" + keyword + '\'' +
                ", ishome='" + ishome + '\'' +
                ", orderType='" + orderType + '\'' +
                ", orderColumn='" + orderColumn + '\'' +
                ", advertStr='" + advertStr + '\'' +
                ", pageStr='" + pageStr + '\'' +
                '}';
    }
}
