package com.mh.peer.dao.product;

import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.entity.ProductType;
import com.mh.peer.model.message.ProductTypeMessage;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/4/29.
 */
public interface ProductTypeDao {
    /**
     * 查询所有产品类型
     */
    DMap queryAllProductType(ProductTypeMessage productTypeMessage);
    /**
     * 查询所有产品类型数量
     */
    DMap queryAllProductTypeCount(ProductTypeMessage productTypeMessage);
    /**
     * 删除产品类型
     */
    DaoResult deleteProductType(ProductType productType);
    /**
     * 添加产品类型
     */
    DaoResult addProductType(ProductType productType);
    /**
     * 更新产品类型
     */
    DaoResult updateProductType(ProductType productType);
}
