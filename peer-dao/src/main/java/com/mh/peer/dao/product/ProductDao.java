package com.mh.peer.dao.product;

import com.mh.peer.model.entity.ProductContent;
import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-10.
 */
public interface ProductDao {

    /**
     * 根据分页获取产品信息(By Zhangerxin)
     * @param productMes
     * @return
     */
    public DMap queryProductByPage(ProductMessageBean productMes);

    /**
     * 新增产品(By Zhangerxin)
     * @param productContent
     * @return
     */
    public DaoResult saveProduct(ProductContent productContent);

    /**
     * 修改产品(By Zhangerxin)
     * @param productMessageBean
     * @return
     */
    public DaoResult updateProduct(ProductContent productMessageBean);

    /**
     * 根据id获取产品信息(By Zhangerxin)
     * @param productMes
     * @return
     */
    public DMap queryProductById(ProductMessageBean productMes);

    /**
     * 根据glId获取附件信息
     * @param productMes
     * @return
     */
    public DMap queryFileByGlId(ProductMessageBean productMes);

    /**
     * 获取产品数量(By Zhangerxin)
     * @return
     */
    public DMap getCountProductByPage(ProductMessageBean producrMes);

    /**
     * 查询产品(前台部分)
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public DMap getListBySome(String orderColumn,String orderType,String pageIndex,String pageSize,String typeId);

    /**
     * 查询产品信息(前台部分)
     * @param orderColumn
     * @param orderType
     * @param productMes
     * @return
     */
    public DMap getFrontList(String orderColumn,String orderType,ProductMessageBean productMes);

    /**
     * 获取产品数量
     * @param productMes
     * @return
     */
    public DMap queryProductCount(ProductMessageBean productMes);

    /**
     * 查询推荐产品(By Zhangerxin)
     * @return
     */
    public DMap getRecommend();

    /**
     * 按照类别获取产品
     */
    public DMap queryProductByType(String productType);

    /**
     * 获取交易金额总和
     * @param producrMes
     * @return
     */
    public DMap getTradePriceSum(ProductMessageBean producrMes);

    /**
     *将要到期借款标数量 by cuibin
     */
    public DMap getCountDueDate();

    /**
     * 获取产品列表(App部分)
     * @param appProductMessageBean
     * @return
     */
    DMap getAppProductList(AppProductMessageBean appProductMessageBean);

    /**
     * 根据产品id获取产品详情(App部分)
     * @param productId
     * @return
     */
    DMap getProductDetail(String productId);

    /**
     * 获取推荐产品(微信部分) by cuibin
     */
    public DMap getWeixinProductList(WeixinProductMessageBean weixinProductMes);

    /**
     * 查看产品详情(微信部分) by cuibin
     */
    public DMap productDetails(WeixinProductMessageBean weixinProductMes);

    /**
     * 查看借款信息(微信部分) by cuibin
     */
    public DMap openProductBorrow(WeixinProductMemberMes weixinProductMemberMes);


    /**
     * 查看投资记录(微信部分) by cuibin
     */
    public DMap openLoanDetails(WeixinProductBuyMes weixinProductBuyMes);

    /**
     * 获得产品总数
     */
    public DMap getWeixinProductCount(WeixinProductMessageBean weixinProductMes);

    /**
     * 查询债权转让列表 (微信部分) by cuibin
     */
    public DMap queryProductAttornByType(WeixinProductAttornDetail weixinProductAttornDetail);
    /**
     * 查询债权转让列表总数 (微信部分) by cuibin
     */
    public DMap queryProductAttornByTypeCount(WeixinProductAttornDetail weixinProductAttornDetail);

    /**
     * 查看债权转让详情(微信部分) by cuibin
     */
    public DMap queryAttornDetails(String id);
    /**
     * 获取产品会员信息(App部分)
     * @param productId
     * @return
     */
    DMap getProductMemberInfo(String productId);

    /**
     * 获取产品投资情况(App部分)
     * @param productId
     * @return
     */
    DMap getProductBuyTj(String productId);

    /**
     * 获取产品投资记录(App部分)
     * @param productId
     * @return
     */
    DMap getProductBuyList(String productId,String pageIndex,String pageSize);

    /**
     * 查询产品列表 带分页 by cuibin
     */
    public DMap queryProductListByPage(ProductMessageBean productMessageBean);

    /**
     * 查询产品列表 总数 by cuibin
     */
    public DMap queryProductListByPageCount(ProductMessageBean productMessageBean);

    /**
     * 项目登机前查询
     * @param productHuanXunMessageBean
     * @return
     */
    public DMap queryProductDjInfo(ProductHuanXunMessageBean productHuanXunMessageBean);


    /**
     * 查询投资记录 by cuibin
     */
    public DMap queryInvestHistory(ProductBuyMessageBean productBuyMessageBean);

    /**
     * 查询投资记录 总数 by cuibin
     */
    public DMap queryInvestHistoryCount(ProductBuyMessageBean productMessageBean);

    /**
     * 获取需投资产品信息
     * @param productId
     * @return
     */
    DMap getInvestedProductInfo(String productId);

    /**
     * 设置为推荐项目
     * @param newShare
     * @return
     */
    DaoResult setNewShare(String productId,String newShare);

    /**
     * 获取推荐项目集合
     * @param newShare
     * @return
     */
    DMap getNewShareList(String newShare);
}
