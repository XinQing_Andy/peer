package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunFreezeDao;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.mh.peer.model.entity.HuanxunFreeze;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/7/29.
 */
@Repository
public class HuanXunFreezeDaoImpl implements HuanXunFreezeDao {
    @Autowired
    private BaseDao baseDao;

    /**
     * 查询所有冻结金额
     * @param memberId
     * @return
     */
    @Override
    public DMap queryFreeze(String memberId) {
        String sql = "SELECT IFNULL(SUM(IPSTRDAMT),0)IPSTRDAMT FROM view_huanxun_freeze_all WHERE"
                   + " MEMBERID = '"+memberId+"' AND FLAG = '0' AND RESULTCODE = '000000'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 产品投资部分
     *
     * @param huanXunFreezeMes
     * @return
     */
    @Override
    public DMap getHuanXunFreezeInfoList(HuanXunFreezeMessageBean huanXunFreezeMes) {
        String sql = "SELECT * FROM view_huanxun_freeze_buy WHERE 1 = 1";
        String buyId = huanXunFreezeMes.getId(); //产品投资id
        if (buyId != null && !buyId.equals("")) {
            sql = sql + " AND ID = '" + buyId + "'";
        }
        DMap dMap = DaoTool.select(sql);
        //System.out.println(dMap.getErrText());
        return dMap;
    }

    /**
     * 查询此项目所有投资记录 bycuibin
     */
    @Override
    public DMap queryAllfreeze(String productId) {
        if (productId.equals("") || productId == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }

        String sql = "select * from view_huanxun_freeze_buy where PRODUCTID = '" + productId + "' and FREEZETYPE = '1' and FLAG = '0' and RESULTCODE = '000000'";
        DMap dMap = DaoTool.select(sql);
        System.out.println("dao========================" + sql);
        System.out.println("dao========================" + dMap.getErrText());
        return dMap;
    }


    /**
     * 按照冻结表id查询记录 bycuibin
     */
    @Override
    public DMap queryOnlyfreeze(String id) {
        String sql = "SELECT * FROM view_huanxun_freeze_buy WHERE 1 = 1";
        if (id != null && !id.equals("")){
            sql = sql + " AND ID = '"+id+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 更新环迅冻结状态 bycuibin
     */
    @Override
    public DaoResult updateHuanXunFreeze(String id) {

        if (id == null || id.equals("")) { //会员id
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        HuanxunFreeze huanxunFreeze = new HuanxunFreeze();
        huanxunFreeze.setId(id);
        huanxunFreeze.setFlag("1");
        huanxunFreeze.setResultcode("000000");
        return baseDao.update(huanxunFreeze);
    }


    /**
     * 更改产品审核状态 bycuibin
     */
    @Override
    public DaoResult updateProductFlag(String productId) {
        if (productId.equals("") || productId == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }

        String sqlProductUpdate = "UPDATE product_content SET SHFLAG = '1' WHERE ID = <productId>";
        DMap dMapProductUpdate = new DMap();
        dMapProductUpdate.setData("productId", productId); //产品id
        DaoResult daoResult = DaoTool.update(sqlProductUpdate, dMapProductUpdate.getData());
        return daoResult;
    }


    /**
     * 按照id查询债权转让记录 bycuibin
     */
    @Override
    public DMap queryOnlyFreezeAttorn(String attornid) {
        if (attornid.equals("") || attornid == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String mySql = "select * from view_huanxun_freeze_attorn where ATTORNID = '" + attornid + "'";

        DMap dMap = DaoTool.select(mySql);
        System.out.println("err================" + dMap.getErrText());
        System.out.println("mySql================" + mySql);
        return dMap;
    }


    /**
     * 查询债权转让冻结记录 bycuibin
     */
    @Override
    public DMap queryFreezeAttorn(String id) {
        String sql = "SELECT * FROM view_huanxun_freeze_attorn WHERE ID = '"+id+"' AND FLAG = '0' AND RESULTCODE = '000000'";
        System.out.println("sql======"+sql);
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 查询债权转让记录 bycuibin
     */
    @Override
    public DMap queryFreezeAttornCount(String id) {
        String sql = "SELECT * FROM view_huanxun_freeze_attorn WHERE FLAG = '1' and RESULTCODE = '000000'";
        if (id != null && !id.equals("")){
            sql = sql + " AND ID = '"+id+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 环迅还款冻结信息
     * @param freezeRepayId
     * @return
     */
    @Override
    public DMap queryFreezeRepay(String freezeRepayId) {
        String sql = "SELECT PRODUCTID,IPSBILLNO,YREPAYMENTTIME,PROJECTNO,REPAYID,IPSACCTNO FROM view_huanxun_freeze_repay WHERE ID = '"+freezeRepayId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 更新债权转让冻结状态 bycuibin
     */
    @Override
    public DaoResult updateUnfreezeAttorn(String id) {

        if (id.equals("") || id == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }

        String sql = "UPDATE huanxun_freeze_attorn SET FLAG = '1',RESULTCODE = '000000' WHERE ID = <ID>";
        DMap dMap = new DMap();
        dMap.setData("ID", id); //产品id
        DaoResult daoResult = DaoTool.update(sql, dMap.getData());
        return daoResult;
    }
}
