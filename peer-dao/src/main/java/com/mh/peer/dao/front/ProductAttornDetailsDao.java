package com.mh.peer.dao.front;

import com.mh.peer.model.message.ProductAttornDetailsMes;
import com.salon.frame.data.DMap;

import java.util.List;

/**
 * Created by Cuibin on 2016/6/29.
 */
public interface ProductAttornDetailsDao {

    /**
     * 查询 成功的债权转让标 详情
     */
    public DMap queryProductAttornDetails(ProductAttornDetailsMes productAttornDetailsMessageBean);

    /**
     * 查询竞拍记录
     */
    public DMap queryAuctionHistory(String productId);

}
