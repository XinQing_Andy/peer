package com.mh.peer.dao.dataStatistic.impl;

import com.mh.peer.dao.dataStatistic.PlatIncomeDao;
import com.mh.peer.model.message.PlatIncomeMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-9-18.
 * 平台收入管理
 */
@Repository
public class PlatIncomeDaoImpl implements PlatIncomeDao{


    /**
     * 根据起止时间获取平台收入集合
     * @param platIncomeMes
     * @return
     */
    @Override
    public DMap getPlatIncomeList(PlatIncomeMessageBean platIncomeMes) {
        String sql = "SELECT * FROM sys_platIncome WHERE 1 = 1";
        String startTime = platIncomeMes.getStartTime(); //开始时间
        String endTime = platIncomeMes.getEndTime(); //结束时间
        String orderType = platIncomeMes.getOrderType(); //排序类型
        String orderColumn = platIncomeMes.getOrderColumn(); //排序字段
        String pageIndex = platIncomeMes.getPageIndex(); //起始数
        String pageSize = platIncomeMes.getPageSize(); //每页显示数量
        DMap dMap = new DMap();

        try{

            if (startTime != null && !startTime.equals("")){ //起始日期
                sql = sql + " AND DATATIME >= '"+startTime+"'";
            }
            if (endTime != null && !endTime.equals("")){ //截止日期
                sql = sql + " AND DATATIME <= '"+endTime+"'";
            }
            if (orderType != null && !orderType.equals("") && orderColumn != null && !orderColumn.equals("")){ //
                sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
            }
            if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
                sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
            }
            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 获取平台收入数量
     * @param platIncomeMes
     * @return
     */
    @Override
    public DMap getPlatIncomeCount(PlatIncomeMessageBean platIncomeMes) {
        String sql = "SELECT * FROM sys_platIncome WHERE 1 = 1";
        String startTime = platIncomeMes.getStartTime(); //开始时间
        String endTime = platIncomeMes.getEndTime(); //结束时间
        DMap dMap = new DMap();

        try{

            if (startTime != null && !startTime.equals("")){ //起始日期
                sql = sql + " AND DATATIME >= '"+startTime+"'";
            }
            if (endTime != null && !endTime.equals("")){ //截止日期
                sql = sql + " AND DATATIME <= '"+endTime+"'";
            }
            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dMap;
    }
}
