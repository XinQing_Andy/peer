package com.mh.peer.dao.product.Impl;

import com.mh.peer.dao.product.ProductReceiveAttornDao;
import com.mh.peer.model.entity.ProductContent;
import com.mh.peer.model.message.ProductMessageBean;
import com.mh.peer.model.message.ProductReceiveMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-4-10.
 */
@Repository
public class ProductReceiveAttornDaoImpl implements ProductReceiveAttornDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 获取产品(分页查询)
     * @param productMes
     * @return
     */
    @Override
    public DMap queryProductByPage(ProductMessageBean productMes) {
//        String pageIndex = productMes.getPageIndex(); //当前页
//        String pageSize = productMes.getPageSize(); //每页显示数量
//        String title = productMes.getTitle(); //名称
//        String productType = productMes.getProductType(); //产品类别
//        String sql = "SELECT ID,TITLE,CREATETIME,LOANRATE,CREDITSTART,CREDITEND,BIDFEE,SPLITCOUNT,FLAG,SHFLAG,FULLSCALE,SHYJ,SORT " +
//                "FROM product_content WHERE 1 = 1";
//        if(title != null && !title.equals("")){ //题目
//            sql = sql + " and TITLE LIKE '%"+title+"%'";
//        }
//        if(productType != null && !productType.equals("")){ //产品类别
//            sql = sql + " and TYPE = '"+productType+"'";
//        }
//        sql = sql + " ORDER BY SORT";
//        if(pageIndex != null && !pageIndex.equals("") && pageSize !=  null && !pageSize.equals("")){
//            sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex)-1) +","+TypeTool.getInt(pageSize)+"";
//        }
//        DMap dmap = DaoTool.select(sql);
        return null;
    }

    /**
     * 新增产品
     * @param productContent
     * @return
     */
    @Override
    public DaoResult saveProduct(ProductContent productContent) {
//        DaoResult result = new DaoResult();
//        if (productContent == null || productContent == null) {
//            result.setCode(-1);
//            result.setErrText("添加失败！");
//            return result;
//        }
//        Double a = productContent.getLoanrate();
//        DConnection conn = DaoTool.getConnection();
//        result = baseDao.insert(productContent, conn); //新增产品
//        if (result.getCode() < 0) {
//            conn.rollback();
//            conn.close();
//            return result;
//        }
//        conn.commit();
//        conn.close();
//        return result;
          return null;
    }

    /**
     * 根据id获取产品信息
     * @param productMes
     * @return
     */
    @Override
    public DMap queryProductById(ProductMessageBean productMes) {
//        String id = productMes.getId(); //主键
//        DMap dMap = new DMap();
//        DMap result = new DMap();
//        dMap.setData("id",id); //主键
//        result = baseDao.getViewData("view_product_content","id",id);
//        return result;
        return null;
    }

    /**
     * 根据Glid获取附件信息
     * @param productMes
     * @return
     */
    @Override
    public DMap queryFileByGlId(ProductMessageBean productMes) {
//        String glId = productMes.getId(); //附件表glId
//        String sql = "SELECT ID,GLID,IMGURL,IMGURLJD,TYPE FROM sys_file WHERE 1 = 1 AND TYPE = '1'";
//        if(glId != null && !glId.equals("")){ //关联id
//            sql = sql + " AND GLID = '"+glId+"'";
//        }
//        DMap dmap = DaoTool.select(sql);
//        return dmap;
          return null;
    }

    /**
     * 修改产品
     * @param productContent
     * @return
     */
    @Override
    public DaoResult updateProduct(ProductContent productContent) {
        DaoResult result = new DaoResult();
        if (productContent == null) {
            result.setCode(-1);
            result.setErrText("修改失败！");
            return result;
        }
        result = baseDao.update(productContent);
        return result;
    }

    /**
     * 删除产品
     * @param productContent
     * @return
     */
    @Override
    public DaoResult delProduct(ProductContent productContent) {
        DaoResult result = new DaoResult();
        if (productContent == null) {
            result.setCode(-1);
            result.setErrText("修改失败");
            return result;
        }
        result = baseDao.delete(productContent);
        return result;
    }

    /**
     * 获取产品数量
     * @param producrMes
     * @return
     */
    @Override
    public DMap getCountProductByPage(ProductMessageBean producrMes) {
        String title = producrMes.getTitle(); //题目
        String sql = "select * from product_content where 1 = 1"; //产品查询条件
        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 查询产品
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public DMap getListBySome(String orderColumn,String orderType,String pageIndex, String pageSize) {
        String sql = "select * from view_product_content where 1 = 1";
        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " order by "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " limit "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 查询产品信息
     * @param orderColumn
     * @param orderType
     * @param productMes
     * @return
     */
    @Override
    public DMap getFrontList(String orderColumn, String orderType, ProductMessageBean productMes) {
        String sql = "select * from view_product_content where 1 = 1"; //产品查询条件
        String pageIndex = productMes.getPageIndex(); //当前数
        String pageSize = productMes.getPageSize(); //每页显示数量
        String productTypeIdSelectPj = productMes.getProductTypeIdSelectPj(); //已选择的产品类型拼接
        String loanrateSelectPj = productMes.getLoanrateSelectPj(); //已选择的年利率拼接
        String loanDateSelectPj = productMes.getLoanDateSelectPj(); //已选择的期限拼接
        String repaymentSelectPj = productMes.getRepaymentSelectPj(); //已选择的还款方式拼接
        String onlineSelect = productMes.getOnlineSelect(); //发布状态(0-已发布、1-即将发布)
        String[] arr = null; //用于存放产品类型
        String[] brr = null; //用于存放年利率
        String[] crr = null; //用于存放期限
        String[] drr = null; //用于存放还款方式

        /*************** 产品类型拼接Start ***************/
        if(productTypeIdSelectPj != null && !productTypeIdSelectPj.equals("")){
            sql = sql + " and (";
            arr = productTypeIdSelectPj.split(","); //用于存放产品类型
            for(int i=0;i<arr.length;i++){
                if(i == 0){
                    sql = sql + "TYPE = '"+arr[i]+"'";
                }else{
                    sql = sql + " or TYPE = '"+arr[i]+"'";
                }
            }
            sql = sql + ")";
        }
        /**************** 产品类型拼接End ****************/

        /*************** 年利率拼接Start ***************/
        if(loanrateSelectPj != null && !loanrateSelectPj.equals("")){
            sql = sql + " and (";
            brr = loanrateSelectPj.split(","); //用于存放年利率
            for(int i=0;i<brr.length;i++){
               if(i == 0){
                   if(brr[i].equals("1")){ //12%以下
                       sql = sql + "(LOANRATE < '12')";
                   }else if(brr[i].equals("2")){ //12%-14%
                       sql = sql + "(LOANRATE >= '12' and LOANRATE < '14')";
                   }else if(brr[i].equals("3")){ //14%-16%
                       sql = sql + "(LOANRATE >= '14' and LOANRATE < '16')";
                   }else if(brr[i].equals("4")){ //16%以上
                       sql = sql + "(LOANRATE >= '16')";
                   }
               }else{
                   if(brr[i].equals("1")){ //12%以下
                       sql = sql + "or (LOANRATE < '12')";
                   }else if(brr[i].equals("21")){ //12%-14%
                       sql = sql + "or (LOANRATE >= '12' and LOANRATE < '14')";
                   }else if(brr[i].equals("3")){ //14%-16%
                       sql = sql + "or (LOANRATE >= '14' and LOANRATE < '16')";
                   }else if(brr[i].equals("4")){ //16%以上
                       sql = sql + "or (LOANRATE >= '16')";
                   }
               }
            }
            sql = sql + ")";
        }
        /**************** 年利率拼接End ****************/

        /*************** 期限拼接Start ***************/
        if(loanDateSelectPj != null && !loanDateSelectPj.equals("")){
            sql = sql + " and (";
            crr = loanDateSelectPj.split(","); //用于存放期限
            for(int i=0;i<crr.length;i++){
                if(i == 0){
                    if(crr[i].equals("1")){ //1个月以下
                        sql = sql + "(LOANYEAR = '0' and LOANMONTH = '0')";
                    }else if(crr[i].equals("2")){ //1-2个月
                        sql = sql + "(LOANYEAR = '0' and LOANMONTH >= '1' and LOANMONTH <= '2')";
                    }else if(crr[i].equals("3")){ //3-5个月
                        sql = sql + "(LOANYEAR = '0' and LOANMONTH >= '3' and LOANMONTH <= '5')";
                    }else if(crr[i].equals("4")){ //6-11个月
                        sql = sql + "(LOANYEAR = '0' and LOANMONTH >= '6' and LOANMONTH <= '11')";
                    }else if(crr[i].equals("5")){ //12个月以上
                        sql = sql + "(LOANYEAR > '0')";
                    }
                }else{
                    if(crr[i].equals("1")){ //1个月以下
                        sql = sql + "or (LOANYEAR = '0' and LOANMONTH = '0')";
                    }else if(crr[i].equals("2")){ //1-2个月
                        sql = sql + "or (LOANYEAR = '0' and LOANMONTH >= '1' and LOANMONTH <= '2')";
                    }else if(crr[i].equals("3")){ //3-5个月
                        sql = sql + "or (LOANYEAR = '0' and LOANMONTH >= '3' and LOANMONTH <= '5')";
                    }else if(crr[i].equals("4")){ //6-11个月
                        sql = sql + "or (LOANYEAR = '0' and LOANMONTH >= '6' and LOANMONTH <= '11')";
                    }else if(crr[i].equals("5")){ //12个月以上
                        sql = sql + "or (LOANYEAR > '0')";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 期限拼接End ****************/

        /*************** 还款方式拼接Start ***************/
        if(repaymentSelectPj != null && !repaymentSelectPj.equals("")){
            sql = sql + " and (";
            drr = repaymentSelectPj.split(",");
            for(int i=0;i<drr.length;i++){
                if(i == 0){
                    if(drr[i].equals("1")){ //按月还款、等额本息
                        sql = sql + "REPAYMENT = '0'";
                    }else if(drr[i].equals("2")){ //按月付息、到期还本
                        sql = sql + "REPAYMENT = '1'";
                    }else if(drr[i].equals("3")){ //一次性还款
                        sql = sql + "REPAYMENT = '2'";
                    }
                }else{
                    if(drr[i].equals("1")){ //按月还款、等额本息
                        sql = sql + "or REPAYMENT = '0'";
                    }else if(drr[i].equals("2")){ //按月付息、到期还本
                        sql = sql + "or REPAYMENT = '1'";
                    }else if(drr[i].equals("3")){ //一次性还款
                        sql = sql + "or REPAYMENT = '2'";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 还款方式拼接End ****************/

        if(onlineSelect != null && !onlineSelect.equals("")){ //发布状态(0-已发布、1-即将发布)
            if(onlineSelect.equals("0")){ //已发布
                sql = sql + " and ONLINETIME <= NOW()";
            }else if(onlineSelect.equals("1")){ //即将发布
                sql = sql + " and ONLINETIME > NOW()";
            }
        }

        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " order by "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " limit "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 推荐产品
     * @return
     */
    @Override
    public DMap getRecommend(String orderColumn,String orderType,String pageIndex,String pageSize) {
        String sql = "SELECT * FROM view_product_content WHERE LOANRATE IN (SELECT MAX(LOANRATE) " +
                "FROM view_product_content WHERE FLAG = '0' AND ONLINETIME <= NOW() AND FULLSCALE = '1')";
        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " order by "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " limit "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 按照类别获取产品
     */
    @Override
    public DMap queryProductByType(String productType) {
        if(productType == null || productType.equals("")){
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("SELECT * FROM view_product_content WHERE" +
                " FLAG = '0' AND TYPE = '"+productType+"' AND ONLINETIME <= NOW() ORDER BY CREATETIME DESC" +
                " LIMIT 0,4");
    }

    @Override
    public DMap getFrontList(String orderColumn, String orderType, ProductReceiveMessageBean productMes) {
        return null;
    }
}
