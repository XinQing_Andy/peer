package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.MainDao;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by WangMeng on 2016/1/13.
 */
@Repository
public class MainDaoImpl implements MainDao {
    @Autowired
    private BaseDao baseDao;

    /**
     * 根据条件获取菜单
     * @param userId
     * @param roleId
     * @param menuParentId
     * @return
     */
    @Override
    public DMap getMeun(String userId,String roleId,String menuParentId) {
        DMap parm = new DMap();
        parm.setData("USER_ID",userId); //用户id
        parm.setData("ROLE_ID",roleId); //角色id
        parm.setData("PARENT_ID",menuParentId); //父级菜单id
        String[] fields = new String[]{"view_user_info.*","function_menu_children("+userId+","+roleId+",view_user_info.MENU_ID) AS CHILDREN"};
        DMap result = baseDao.getViewData("view_user_info",fields,"USER_ID = <USER_ID> AND ROLE_ID=<ROLE_ID> AND PARENT_ID = <PARENT_ID>",parm);
        return result;
    }

    /**
     * 获取顶部菜单
     * @param userId
     * @param roleId
     * @return
     */
    @Override
    public DMap getTopMenu(String userId,String roleId) {
        DMap parm = new DMap();
        parm.setData("USER_ID",userId); //用户id
        parm.setData("ROLE_ID",roleId); //角色id
        String[] fields = new String[]{"view_user_info.*","function_menu_children("+userId+","+roleId+",view_user_info.MENU_ID) AS CHILDREN"};
        DMap result = baseDao.getViewData("view_user_info",fields,"USER_ID = <USER_ID> AND ROLE_ID = <ROLE_ID> AND PARENT_ID IS NULL ",parm);
        return result;
    }
}
