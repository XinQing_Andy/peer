package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.ApplyProductDao;
import com.mh.peer.model.message.ProductApplyMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-9-7.
 */
@Repository
public class ApplyProductDaoImpl implements ApplyProductDao{

    /**
     * 获取已申请项目
     * @param productApplyMes
     * @return
     */
    @Override
    public DMap getApplyProductList(ProductApplyMessageBean productApplyMes) {
        String sql = "SELECT * FROM view_product_content WHERE 1 = 1";
        String applyMemberId = productApplyMes.getApplyMemberId(); //申请人
        String applyStartTime = productApplyMes.getApplyStartTime(); //申请起日期
        String applyEndTime = productApplyMes.getApplyEndTime(); //申请止日期
        String title = productApplyMes.getTitle(); //题目
        String code = productApplyMes.getCode(); //编码
        String orderColumn = productApplyMes.getOrderColumn(); //排序字段
        String orderType = productApplyMes.getOrderType(); //排序类型
        String pageIndex = productApplyMes.getPageIndex(); //起始数
        String pageSize = productApplyMes.getPageSize(); //每页显示数量
        DMap dMap = new DMap();

        try{

            if (applyMemberId != null && !applyMemberId.equals("")){ //申请人
                sql = sql + " AND APPLYMEMBER = '"+applyMemberId+"'";
            }
            if (applyStartTime != null && !applyStartTime.equals("")){ //申请起日期
                sql = sql + " AND DATE(CREATETIME) >= DATE('"+applyStartTime+"')";
            }
            if (applyEndTime != null && !applyEndTime.equals("")){ //申请止日期
                sql = sql + " AND DATE(CREATETIME) <= DATE('"+applyEndTime+"')";
            }
            if (title != null && !title.equals("")){ //题目
                sql = sql + " AND TITLE LIKE '%"+title+"%'";
            }
            if (code != null && !code.equals("")){ //编码
                sql = sql + " AND CODE LIKE '"+code+"'";
            }
            if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
                sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
            }
            if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
                sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
            }
            dMap = DaoTool.select(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 已申请项目数量
     * @param productApplyMes
     * @return
     */
    @Override
    public DMap getApplyProductListCount(ProductApplyMessageBean productApplyMes) {
        String sql = "SELECT * FROM view_product_content WHERE 1 = 1";
        String applyMemberId = productApplyMes.getApplyMemberId(); //申请人
        String applyStartTime = productApplyMes.getApplyStartTime(); //申请起日期
        String applyEndTime = productApplyMes.getApplyEndTime(); //申请止日期
        String title = productApplyMes.getTitle(); //题目
        String code = productApplyMes.getCode(); //编码
        DMap dMap = new DMap();

        try{

            if (applyMemberId != null && !applyMemberId.equals("")){ //申请人
                sql = sql + " AND APPLYMEMBER = '"+applyMemberId+"'";
            }
            if (applyStartTime != null && !applyStartTime.equals("")){ //申请起日期
                sql = sql + " AND DATE(CREATETIME) >= DATE('"+applyStartTime+"')";
            }
            if (applyEndTime != null && !applyEndTime.equals("")){ //申请止日期
                sql = sql + " AND DATE(CREATETIME) <= DATE('"+applyEndTime+"')";
            }
            if (title != null && !title.equals("")){ //题目
                sql = sql + " AND TITLE LIKE '%"+title+"%'";
            }
            if (code != null && !code.equals("")){ //编码
                sql = sql + " AND CODE LIKE '"+code+"'";
            }
            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 获取已还金额
     * @param productId
     * @param applyMemberId
     * @return
     */
    @Override
    public DMap getyRepay(String productId, String applyMemberId) {
        String sql = "SELECT IFNULL(SUM(IFNULL(REPAYMENTPRICE,0)),0)YREPAYPRICE,IFNULL(SUM(IFNULL(PRINCIPAL,0)),0)YPRINCIPAL,"
                   + " IFNULL(SUM(IFNULL(YPRICE,0)),0)YPRICE FROM view_product_repay"
                   + " WHERE SREPAYMENTTIME IS NOT NULL"; //获取已还金额
        if (productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        if (applyMemberId != null && !applyMemberId.equals("")){ //申请人
            sql = sql + " AND APPLYMEMBER = '"+applyMemberId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 获取待还金额
     * @param productId
     * @param applyMemberId
     * @return
     */
    @Override
    public DMap getdRepay(String productId, String applyMemberId) {
        String sql = "SELECT IFNULL(SUM(IFNULL(REPAYMENTPRICE,0)),0)DREPAYPRICE,IFNULL(SUM(IFNULL(PRINCIPAL,0)),0)DPRINCIPAL,"
                   + " IFNULL(SUM(IFNULL(YPRICE,0)),0)YPRICE"
                   + " FROM view_product_repay WHERE SREPAYMENTTIME IS NULL"; //获取待还金额
        if (productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        if (applyMemberId != null && !applyMemberId.equals("")){ //申请人
            sql = sql + " AND APPLYMEMBER = '"+applyMemberId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 还款列表集合
     * @param productId
     * @return
     */
    @Override
    public DMap getRepayList(String productId,String orderColumn,String orderType,String pageIndex,String pageSize) {
        String sql = "SELECT * FROM product_repay WHERE 1 = 1";
        if (productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 还款列表数量
     * @param productId
     * @return
     */
    @Override
    public DMap getRepayListCount(String productId) {
        String sql = "SELECT * FROM product_repay WHERE 1 = 1";
        if (productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

}
