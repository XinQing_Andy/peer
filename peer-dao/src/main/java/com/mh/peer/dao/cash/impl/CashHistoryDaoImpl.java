package com.mh.peer.dao.cash.impl;

import com.mh.peer.dao.cash.CashHistoryDao;
import com.mh.peer.model.message.HuanXunPostalMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/7/28.
 */
@Repository
public class CashHistoryDaoImpl implements CashHistoryDao {

    /**
     * 查询提现统计
     */
    @Override
    public DMap queryCashHistory(HuanXunPostalMessageBean huanXunPostalMessageBean) {
        if (huanXunPostalMessageBean == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String sql = "select * from view_huanxun_postal where 1 = 1";

        //类别id
        String realname = huanXunPostalMessageBean.getRealname(); //真实姓名
        String idcard = huanXunPostalMessageBean.getIdcard(); //身份证号
        String orderColumn = huanXunPostalMessageBean.getOrderColumn(); //排序字段
        String orderType = huanXunPostalMessageBean.getOrderType(); //排序类型
        String startTime = huanXunPostalMessageBean.getStartTime(); //开始时间
        String endTime = huanXunPostalMessageBean.getEndTime(); //结束时间
        String pageIndex = huanXunPostalMessageBean.getPageIndex(); //当前页
        String pageSize = huanXunPostalMessageBean.getPageSize(); //每页显示数量

        if (startTime != null && !startTime.equals("")) {
            sql = sql + " and CREATETIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) {
            sql = sql + " and CREATETIME <= '" + endTime + "'";
        }
        if (realname != null && !realname.equals("")) {
            sql = sql + " and REALNAME LIKE '%" + realname + "%'";
        }
        if (idcard != null && !idcard.equals("")) {
            sql = sql + " and IDCARD LIKE '%" + idcard + "%'";
        }
        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " order by " + orderColumn + " " + orderType;
        }

        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + TypeTool.getInt(pageIndex) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
//        System.out.println("===================" + realname);
//        System.out.println("===================" + orderColumn);
//        System.out.println("===================" + orderType);
//        System.out.println("===================" + idcard);
        return dmap;
    }

    /**
     * 查询提现统计 总数
     */
    @Override
    public DMap queryCashHistoryCount(HuanXunPostalMessageBean huanXunPostalMessageBean) {
        if (huanXunPostalMessageBean == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String sql = "select count(*) as count from view_huanxun_postal where 1 = 1";

        String realname = huanXunPostalMessageBean.getRealname(); //真实姓名
        String idcard = huanXunPostalMessageBean.getIdcard(); //身份证号
        String startTime = huanXunPostalMessageBean.getStartTime(); //开始时间
        String endTime = huanXunPostalMessageBean.getEndTime(); //结束时间

        if (startTime != null && !startTime.equals("")) {
            sql = sql + " and CREATETIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) {
            sql = sql + " and CREATETIME <= '" + endTime + "'";
        }
        if (realname != null && !realname.equals("")) {
            sql = sql + " and REALNAME LIKE '%" + realname + "%'";
        }
        if (idcard != null && !idcard.equals("")) {
            sql = sql + " and IDCARD LIKE '%" + idcard + "%'";
        }

        DMap dmap = DaoTool.select(sql);
        //System.out.println("===================" + sql);
        return dmap;
    }

}
