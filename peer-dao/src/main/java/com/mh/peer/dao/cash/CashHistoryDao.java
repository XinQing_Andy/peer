package com.mh.peer.dao.cash;

import com.mh.peer.model.message.HuanXunPostalMessageBean;
import com.salon.frame.data.DMap;

/**
 * Created by Cuibin on 2016/7/28.
 */
public interface CashHistoryDao {

    /**
     * 查询提现统计
     */
    public DMap queryCashHistory(HuanXunPostalMessageBean huanXunPostalMessageBean);

    /**
     * 查询提现统计 总数
     */
    public DMap queryCashHistoryCount(HuanXunPostalMessageBean huanXunPostalMessageBean);
}
