package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.SysListenerDao;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2016-5-19.
 */
@Repository
public class SysListenerDaoImpl implements SysListenerDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 获取1监听信息
     * @return
     */
    @Override
    public DMap getListenerInfo() {
        String sql = "SELECT * FROM sys_Listener WHERE 1 = 1"; //获取监听信息
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
}
