package com.mh.peer.dao.product;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.ProductAttornRecord;
import com.mh.peer.model.entity.ProductBuyInfo;
import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import java.util.HashMap;

/**
 * Created by zhangerxin on 2016-5-23.
 */
public interface ProductAttornDao {

    /**
     * 获取可转让的债权
     * @param productInvestMes
     * @return
     */
    public DMap getAttornList(ProductInvestMessageBean productInvestMes);

    /**
     * 获取可转让债权数量
     * @param productInvestMes
     * @return
     */
    public DMap getAttornCount(ProductInvestMessageBean productInvestMes);

    /**
     * 已购买 已转让 债权
     */
    public DMap queryMyAttornByPage(ProductAttornRecordMessageBean productAttornRecordMessageBean);

    /**
     * 债权数量
     * @param productAttornRecordMes
     * @return
     */
    DMap queryMyAttornCount(ProductAttornRecordMessageBean productAttornRecordMes);

    /**
     * 获取更多债权转让
     * @param productAttornMoreMes
     * @return
     */
    public DMap getAttornMoreList(ProductAttornMoreMessageBean productAttornMoreMes);

    /**
     * 获取债权转让数量
     * @param productAttornMoreMes
     * @return
     */
    public DMap getAttornMoreCount(ProductAttornMoreMessageBean productAttornMoreMes);

    /**
     * 获取债权转让数量(前台)
     * @param poductAttornRecordMes
     * @return
     */
    public DMap getFrontAttornCount(ProductAttornRecordMessageBean poductAttornRecordMes);


    /**
     *转让申请数量 by cuibin
     */
    public DMap getCountByType(ProductAttornRecordMessageBean productAttornRecordMessageBean);


    /**
     * 前台债权查询
     * @param productAttornRecordMes
     * @return
     */
    public DMap getFrontAttornList(String orderColumn, String orderType,ProductAttornRecordMessageBean productAttornRecordMes);

    /**
     * 获取可转让的债权详情
     * @param productAttornRecordMes
     * @return
     */
    public DMap getAttornDetail(ProductAttornRecordMessageBean productAttornRecordMes);

    /**
     * 获取债权详情
     * @return
     */
    public DMap getAttornProductDetai(AttornProductMessageBean attornProductMessageBean);

    /**
     * 债权承接
     * @param productAttornRecord,productBuyInfo
     * @return
     */
    public DaoResult updateAttornUpdate(ProductAttornRecord productAttornRecord,MemberInfo memberInfo);

    /**
     * 添加转让记录（发起转让）
     * @param productAttornRecord
     * @param productBuyInfo
     * @return
     */
    public DaoResult confirmAttornProduct(ProductAttornRecord productAttornRecord,ProductBuyInfo productBuyInfo);

    /**
     * 获取可转让的债权详情
     * @param productAttornRecordMes
     * @return
     */
//    public ProductAttornRecordMessageBean getAttornDetail(ProductAttornRecordMessageBean productAttornRecordMes);


    /**
     * 根据分页获取债权转让标(后台) By zhangerxin
     * @param productAttornMes
     * @return
     */
    DMap queryProductAttornByPage(ProductAttornMessage productAttornMes);

    /**
     * 获取债权转让标数量
     * @param productAttornMes
     * @return
     */
    DMap queryProductAttornCount(ProductAttornMessage productAttornMes);

    /**
     * 债权标审核
     * @param productAttornMes
     * @return
     */
    DaoResult setProductAttornShFlag(ProductAttornMessage productAttornMes);

    /**
     * 获取债权转让集合(App端)
     * @param memberId
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DMap getAppAttornList(String memberId, String typeId, String pageIndex, String pageSize, String orderColumn, String orderType);

    /**
     * 获取债权转让标详情(App端)
     * @param attornId
     * @return
     */
    DMap getAppAttornDetail(String attornId);

    /**
     * 获取已购买债权
     * @param memberId
     * @param timeStyle
     * @param sTime
     * @param eTime
     * @param keyWord
     * @param value
     * @return
     */
    DMap queryInvestedAttornByPage(String memberId,String timeStyle, String sTime, String eTime, String keyWord, String value, String orderColumn, String orderType, String pageIndex, String pageSize);

    /**
     * 获取待承接债权
     * @param productAttornMes
     * @return
     */
    DMap queryProductAttornUnderTaked(ProductAttornMessage productAttornMes);

    /**
     * 获取待承接债权数量
     * @param productAttornMes
     * @return
     */
    DMap queryProductAttornUnderTakedCount(ProductAttornMessage productAttornMes);

    /**
     * 获取失败的债权集合
     * @param productAttornMes
     * @return
     */
    DMap queryProductAttornFail(ProductAttornMessage productAttornMes);

    /**
     * 获取失败的债权数量
     * @param productAttornMes
     * @return
     */
    DMap queryProductAttornFailCount(ProductAttornMessage productAttornMes);
}
