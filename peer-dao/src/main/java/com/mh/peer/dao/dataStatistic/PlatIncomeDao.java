package com.mh.peer.dao.dataStatistic;

import com.mh.peer.model.message.PlatIncomeMessageBean;
import com.salon.frame.data.DMap;

/**
 * Created by zhangerxin on 2016-9-18.
 * 平台收入管理
 */
public interface PlatIncomeDao {

    /**
     * 根据起止时间获取平台收入集合
     * @param platIncomeMes
     * @return
     */
    DMap getPlatIncomeList(PlatIncomeMessageBean platIncomeMes);

    /**
     * 根据起止时间获取平台收入数量
     * @param platIncomeMes
     * @return
     */
    DMap getPlatIncomeCount(PlatIncomeMessageBean platIncomeMes);
}
