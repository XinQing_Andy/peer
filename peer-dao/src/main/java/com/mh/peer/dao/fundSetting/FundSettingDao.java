package com.mh.peer.dao.fundSetting;

import com.mh.peer.model.entity.*;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/4/12.
 */
public interface FundSettingDao {

    /**
     *添加字典
     */
    DaoResult addDictionary(SysDictionary sysDictionary);
    /**
     * 删除字典
     */
    DaoResult deleteDictionary(SysDictionary sysDictionary);
    /**
     * 修改字典
     */
    DaoResult updateDictionary(SysDictionary sysDictionary);
    /**
     * 查询所有字典
     */
    DMap queryAllDictionary(Paging paging);
    DMap queryAllDictionaryCount(Paging paging);

    /**
     * 查询单个字典
     */
    DMap queryOnlyDictionary(SysDictionary sysDictionary);

    /**
     * 添加取号原则TakeNo
     */
    DaoResult addTakeNo(SysTakeNo sysTakeNo);
    /**
     * 删除取号原则
     */
    DaoResult deleteTakeNo(SysTakeNo sysTakeNo);
    /**
     * 更改取号原则
     */
    DaoResult updateTakeNo(SysTakeNo sysTakeNo);
    /**
     * 查询所有取号原则
     */
    DMap queryAllTakeNo(Paging paging);
    DMap queryAllTakeNoCount(Paging paging);
    /**
     * 查询单个取号原则
     */
    DMap queryOnlyTakeNo(SysTakeNo sysTakeNo);

    /**
     *添加vip等级维护
     */
    DaoResult addVipMaintain(MemberLevel memberLevel);
    /**
     *删除vip等级维护
     */
    DaoResult deleteVipMaintain(MemberLevel memberLevel);
    /**
     *修改vip等级维护
     */
    DaoResult updateVipMaintain(MemberLevel memberLevel);
    /**
     *查询所有vip等级维护
     */
    DMap queryAllVipMaintain(Paging paging);
    DMap queryAllVipMaintainCount(Paging paging);
    DMap queryAllVipMaintain2();
    /**
     *查询单个vip等级维护
     */
    DMap queryOnlyVipMaintain(MemberLevel memberLevel);

    /**
     *增加 按会员等级费用维护
     */
    DaoResult addMemberFeeLevel(MemberFeeLevel memberFeeLevel);
    /**
     *删除 按会员等级费用维护
     */
    DaoResult deleteMemberFeeLevel(MemberFeeLevel memberFeeLevel);
    /**
     *更改 按会员等级费用维护
     */
    DaoResult updateMemberFeeLevel(MemberFeeLevel memberFeeLevel);
    /**
     *查询所有 按会员等级费用维护
     */
    DMap queryAllMemberFeeLevel(Paging paging);
    DMap queryAllMemberFeeLevel2();
    DMap queryMemberFeeLevelCount(Paging paging);
    /**
     *查询一个 按会员等级费用维护
     */
    DMap queryOnlyMemberFeeLevel(MemberFeeLevel memberFeeLevel);

    /**
     *增加 会员费用关联表
     */
    DaoResult addMemberFeeRelation(MemberFeeRelation memberFeeRelation);
    /**
     *删除 会员费用关联表
     */
    DaoResult deleteMemberFeeRelation(MemberFeeRelation memberFeeRelation);
    /**
     *更改 会员费用关联表
     */
    DaoResult updateMemberFeeRelation(MemberFeeRelation memberFeeRelation);
    /**
     *查询一个 会员费用关联表
     */
    DMap queryOnlyMemberFeeRelation(MemberFeeRelation memberFeeRelation);
    /**
     *查询所有 会员费用关联表
     */
    DMap queryAllMemberFeeRelation2();
    DMap queryAllMemberFeeRelation(Paging paging);
    DMap queryMemberFeeRelationCount(Paging paging);

    /**
     *增加 固定费用过账表
     */
    DaoResult addSysManageAmt(SysManageAmt sysManageAmt);
    /**
     *删除 固定费用过账表
     */
    DaoResult deleteSysManageAmt(SysManageAmt sysManageAmt);
    /**
     *修改 固定费用过账表
     */
    DaoResult updateSysManageAmt(SysManageAmt sysManageAmt);
    /**
     *查询所有 固定费用过账表
     */
    DMap queryAllSysManageAmt(Paging paging);
    DMap queryAllSysManageAmtCount(Paging paging);
    /**
     *查询一个 固定费用过账表
     */
    DMap queryOnlySysManageAmt(SysManageAmt sysManageAmt);





}
