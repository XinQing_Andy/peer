package com.mh.peer.dao.front;

import com.mh.peer.model.message.MemberMessageMesBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/7/21.
 */
public interface MyMessageDao {

    /**
     * 分页查询我的消息
     */
    public DMap queryMyMessageByPage(MemberMessageMesBean memberMessageMesBean);

    /**
     * 查询充值记录总数
     */
    public DMap queryMyMessageCount(MemberMessageMesBean memberMessageMesBean);

    /**
     * 获取详细信息
     * @param memberMessageMes
     * @return
     */
    public DMap getMyMessageById(MemberMessageMesBean memberMessageMes);

    /**
     * 更新只读状态
     * @param memberMessageMes
     * @return
     */
    public DaoResult upReadFlg(MemberMessageMesBean memberMessageMes);

    /**
     * 删除我的消息
     * @param memberMessageMes
     * @return
     */
    public DaoResult delMyMessageById(MemberMessageMesBean memberMessageMes);
}
