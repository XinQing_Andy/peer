package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.HuanxunRegist;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-7-23.
 */
@Repository
public interface HuanXunRegistDao {

    /**
     * 环迅注册
     * @param huanxunRegist
     * @param memberMessage
     * @return
     */
    DaoResult registerhxResult(HuanxunRegist huanxunRegist,MemberMessage memberMessage);

    /**
     * 根据条件查询会员注册信息
     * @param huanXunRegistMessageBean
     * @return
     */
    DMap getRegisterhxBySome(HuanXunRegistMessageBean huanXunRegistMessageBean);
}
