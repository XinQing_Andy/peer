package com.mh.peer.dao.fundSetting.impl;

import com.mh.peer.dao.fundSetting.FundSettingDao;
import com.mh.peer.model.entity.*;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/4/12.
 */
@Repository
public class FundSettingDaoImpl implements FundSettingDao{

    @Autowired
    private BaseDao baseDao;

    @Override
    public DaoResult addDictionary(SysDictionary sysDictionary) {
        return baseDao.insert(sysDictionary);
    }

    @Override
    public DaoResult addMemberFeeLevel(MemberFeeLevel memberFeeLevel) {
        return baseDao.insert(memberFeeLevel);
    }

    @Override
    public DaoResult addMemberFeeRelation(MemberFeeRelation memberFeeRelation) {
        return baseDao.insert(memberFeeRelation);
    }

    @Override
    public DaoResult addSysManageAmt(SysManageAmt sysManageAmt) {
        return baseDao.insert(sysManageAmt);
    }

    @Override
    public DaoResult addTakeNo(SysTakeNo sysTakeNo) {
        return baseDao.insert(sysTakeNo);
    }

    @Override
    public DaoResult addVipMaintain(MemberLevel memberLevel) {
        return baseDao.insert(memberLevel);
    }

    @Override
    public DaoResult deleteDictionary(SysDictionary sysDictionary) {
        return baseDao.delete(sysDictionary);
    }

    @Override
    public DaoResult deleteMemberFeeLevel(MemberFeeLevel memberFeeLevel) {
        return baseDao.delete(memberFeeLevel);
    }

    @Override
    public DaoResult deleteMemberFeeRelation(MemberFeeRelation memberFeeRelation) {
        return baseDao.delete(memberFeeRelation);
    }

    @Override
    public DaoResult deleteSysManageAmt(SysManageAmt sysManageAmt) {
        return baseDao.delete(sysManageAmt);
    }

    @Override
    public DaoResult deleteTakeNo(SysTakeNo sysTakeNo) {
        return baseDao.delete(sysTakeNo);
    }

    @Override
    public DaoResult deleteVipMaintain(MemberLevel memberLevel) {
        return baseDao.delete(memberLevel);
    }

    /**
     *================================================================
     */
    @Override
    public DMap queryAllDictionaryCount(Paging paging) {
        String title = paging.getTitle();
        String sql = "select * from sys_dictionary where 1 = 1";
        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (group_type LIKE '%"+title+"%' OR member_id LIKE '%"+title+"%' OR member_name LIKE '%"+title+"%' OR remark LIKE '%"+title+"%' OR active_flg LIKE '%"+title+"%')";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
    @Override
    public DMap queryAllDictionary(Paging paging) {
        String pageIndex = paging.getPageIndex(); //当前页
        String pageSize = paging.getPageSize(); //每页显示数量
        String title = paging.getTitle(); //题目
        String sql = "SELECT * FROM sys_dictionary WHERE 1 = 1";

        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (group_type LIKE '%"+title+"%' OR member_id LIKE '%"+title+"%' OR member_name LIKE '%"+title+"%' OR remark LIKE '%"+title+"%' OR active_flg LIKE '%"+title+"%')";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);

        return dmap;
        //return baseDao.getViewData("sys_dictionary");
    }
    /**
     * =================================================================
     * 根据分页获取数据
     */
    @Override
    public DMap queryMemberFeeLevelCount(Paging paging) {
        String title = paging.getTitle();
        String sql = "select * from member_fee_level where 1 = 1";
        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (FEE_NAME LIKE '"+title+"' OR active_flg LIKE '"+title+"')";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    public DMap queryAllMemberFeeLevel2(){
        return baseDao.getViewData("member_fee_level");
    }
    /**
     *会员等级费用维护
     */
    @Override
    public DMap queryAllMemberFeeLevel(Paging paging) {
        String pageIndex = paging.getPageIndex(); //当前页
        String pageSize = paging.getPageSize(); //每页显示数量
        String title = paging.getTitle(); //题目
        String sql = "SELECT * FROM member_fee_level WHERE 1 = 1";
        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (FEE_NAME LIKE '"+title+"' OR active_flg LIKE '"+title+"')";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);

        return dmap;
    }

    /**
     *========================================================================================
     */
    /**
     * 查询所有会员费用关联数量
     */
    @Override
    public DMap queryMemberFeeRelationCount(Paging paging) {
        String title = paging.getTitle();
        String sql = "select * from member_fee_relation where 1 = 1";
        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (level_id LIKE '"+title+"' OR fee_id LIKE '"+title+"')";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
    /**
     * 查询所有会员费用关联
     */
    @Override
    public DMap queryAllMemberFeeRelation(Paging paging) {
        String pageIndex = paging.getPageIndex(); //当前页
        String pageSize = paging.getPageSize(); //每页显示数量
        String title = paging.getTitle(); //题目
        String sql = "SELECT * FROM member_fee_relation WHERE 1 = 1";

        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (level_id LIKE '"+title+"' OR fee_id LIKE '"+title+"')";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);

        return dmap;
    }
    @Override
    public DMap queryAllMemberFeeRelation2(){
        return baseDao.getViewData("member_fee_relation");
    }

    /**
     *=====================================================================
     */
    /**
     * 查询所有固定费用过账总数
     */
    @Override
    public DMap queryAllSysManageAmtCount(Paging paging) {
        String title = paging.getTitle();
        String sql = "select * from sys_manage_amt where 1 = 1";
        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (fee_name LIKE '%"+title+"%' OR fee_amt LIKE '%"+title+"%' OR active_flg LIKE '%"+title+"%')";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
    /**
     * 查询所有固定费用过账
     */
    @Override
    public DMap queryAllSysManageAmt(Paging paging) {
        String pageIndex = paging.getPageIndex(); //当前页
        String pageSize = paging.getPageSize(); //每页显示数量
        String title = paging.getTitle(); //题目
        String sql = "SELECT * FROM sys_manage_amt WHERE 1 = 1";

        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (fee_name LIKE '"+title+"' OR fee_amt LIKE '"+title+"' OR active_flg LIKE '%"+title+"%')";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);

        return dmap;
        //return baseDao.getViewData("sys_manage_amt");
    }
    /**
     *=====================================================================
     */
    /**
     * 查询所有取号原则总数
     */
    @Override
    public DMap queryAllTakeNoCount(Paging paging) {
        String title = paging.getTitle();
        String sql = "select * from sys_take_no where 1 = 1";
        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (code_type LIKE '"+title+"' OR status_method LIKE '"+title+"' OR curr_no LIKE '%"+title+"%' OR no_length LIKE '"+title+"' OR start_no LIKE '"+title+"' OR max_no LIKE '"+title+"' OR no_desc LIKE '"+title+"' OR no_format LIKE '"+title+"')";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
    /**
     * 查询所有取号原则
     */
    @Override
    public DMap queryAllTakeNo(Paging paging) {
        String pageIndex = paging.getPageIndex(); //当前页
        String pageSize = paging.getPageSize(); //每页显示数量
        String title = paging.getTitle(); //题目
        String sql = "SELECT * FROM sys_take_no WHERE 1 = 1";

        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (code_type LIKE '"+title+"' OR status_method LIKE '"+title+"' OR curr_no LIKE '%"+title+"%' OR no_length LIKE '"+title+"' OR start_no LIKE '"+title+"' OR max_no LIKE '"+title+"' OR no_desc LIKE '"+title+"' OR no_format LIKE '"+title+"')";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);

        return dmap;
        //return baseDao.getViewData("sys_take_no");
    }
    /**
     *=====================================================================
     */
    /**
     * 查询所有vip等级维护 总数
     */
    @Override
    public DMap queryAllVipMaintainCount(Paging paging) {
        String title = paging.getTitle();
        String sql = "select * from member_level where 1 = 1";
        if(title != null && !title.equals("")){
            sql = sql + " AND level_name LIKE '%"+title+"%' OR level_request LIKE '%"+title+"%'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
    /**
     * 查询所有vip等级维护
     */
    @Override
    public DMap queryAllVipMaintain(Paging paging) {
        String pageIndex = paging.getPageIndex(); //当前页
        String pageSize = paging.getPageSize(); //每页显示数量
        String title = paging.getTitle(); //题目
        String sql = "SELECT * FROM member_level WHERE 1 = 1";

        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND level_name LIKE '%"+title+"%' OR level_request LIKE '%"+title+"%'";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);

        return dmap;
    }
    @Override
    public DMap queryAllVipMaintain2(){
        return baseDao.getViewData("member_level");
    }
    /**
     *=====================================================================
     */
    @Override
    public DMap queryOnlyDictionary(SysDictionary sysDictionary) {
        return DaoTool.select("select * from sys_dictionary where id=" + sysDictionary.getId());
    }

    @Override
    public DMap queryOnlyMemberFeeLevel(MemberFeeLevel memberFeeLevel) {
        return DaoTool.select("select * from member_fee_level where id=" + memberFeeLevel.getId());
    }

    @Override
    public DMap queryOnlyMemberFeeRelation(MemberFeeRelation memberFeeRelation) {
        return DaoTool.select("select * from member_fee_relation where id=" + memberFeeRelation.getId());
    }

    @Override
    public DMap queryOnlySysManageAmt(SysManageAmt sysManageAmt) {
        return DaoTool.select("select * from sys_manage_amt where id=" + sysManageAmt.getId());
    }

    @Override
    public DMap queryOnlyTakeNo(SysTakeNo sysTakeNo) {
        return DaoTool.select("select * from sys_take_no where id=" + sysTakeNo.getId());
    }

    @Override
    public DMap queryOnlyVipMaintain(MemberLevel memberLevel) {
        return DaoTool.select("select * from member_level where id=" + memberLevel.getId());
    }

    @Override
    public DaoResult updateDictionary(SysDictionary sysDictionary) {
        return baseDao.update(sysDictionary);
    }

    @Override
    public DaoResult updateMemberFeeLevel(MemberFeeLevel memberFeeLevel) {
        return baseDao.update(memberFeeLevel);
    }

    @Override
    public DaoResult updateMemberFeeRelation(MemberFeeRelation memberFeeRelation) {
        return baseDao.update(memberFeeRelation);
    }

    @Override
    public DaoResult updateSysManageAmt(SysManageAmt sysManageAmt) {
        return baseDao.update(sysManageAmt);
    }

    @Override
    public DaoResult updateTakeNo(SysTakeNo sysTakeNo) {
        return baseDao.update(sysTakeNo);
    }

    @Override
    public DaoResult updateVipMaintain(MemberLevel memberLevel) {
        return baseDao.update(memberLevel);
    }
}
