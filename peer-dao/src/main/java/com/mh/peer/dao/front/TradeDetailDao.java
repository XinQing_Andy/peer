package com.mh.peer.dao.front;

import com.mh.peer.model.message.TradeDetailMessageBean;
import com.salon.frame.data.DMap;


/**
 * Created by Cuibin on 2016/6/20.
 */
public interface TradeDetailDao {

    /**
     * 查询所有资金明细
     */
    public DMap queryTradeDetail(TradeDetailMessageBean tradeDetailMessageBean);

    /**
     * 查询总记录数
     */
    public DMap queryTradeDetailCount(TradeDetailMessageBean tradeDetailMessageBean);
    /**
     * 获取交易记录(App端)
     * @param memberId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public DMap getTradeRecordList(String memberId,String pageIndex,String pageSize,String tradeType,String tradeTime);
}
