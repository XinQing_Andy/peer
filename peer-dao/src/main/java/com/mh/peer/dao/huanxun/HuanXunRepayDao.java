package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.HuanxunFreezeRepay;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunRepayMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

import java.util.Map;

/**
 * Created by zhangerxin on 2016-8-7.
 * 还款部分
 */
public interface HuanXunRepayDao {

    /**
     * 获取会员信息
     * @param huanXunRepayMes
     * @return
     */
    DMap queryMemberInfo(HuanXunRepayMessageBean huanXunRepayMes);

    /**
     * 根据会员id获取会员信息
     * @param memberId
     * @return
     */
    DMap getMemberInfo(String memberId);

    /**
     * 环迅还款金额冻结部分
     * @param huanxunFreezeRepay
     * @param memberMessage
     * @param memberInfo
     * @return
     */
    DaoResult repayhxResult(HuanxunFreezeRepay huanxunFreezeRepay, MemberMessage memberMessage, MemberInfo memberInfo, Map<String,Object> mapException);

    /**
     * 根据冻结还款id获取还款数量
     * @param repayId
     * @return
     */
    DMap queryFreezeRepayCount(String repayId);
}
