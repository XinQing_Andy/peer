package com.mh.peer.dao.dataStatistic;

import com.salon.frame.data.DMap;

/**
 * Created by zhangerxin on 2016-9-18.
 * 借款标销售情况分析
 */
public interface ProductSaleDao {

    /**
     * 根据产品类型获取已放款数量
     * @param typeId
     * @return
     */
    DMap getGrantPriceCount(String typeId);

    /**
     * 根据产品类型获取已借款总额
     * @param typeId
     * @return
     */
    DMap getLoanPrice(String typeId);

    /**
     * 根据产品类型获取借款标数量
     * @param typeId
     * @return
     */
    DMap getLoanCount(String typeId);

    /**
     * 根据产品类型获取投标会员数
     * @param typeId
     * @return
     */
    DMap getInvestMemberCount(String typeId);
}
