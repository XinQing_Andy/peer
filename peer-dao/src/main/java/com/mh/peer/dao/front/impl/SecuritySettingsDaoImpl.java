package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.SecuritySettingsDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberLinkman;
import com.mh.peer.model.entity.MemberSetupQuestions;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by cuibin on 2016/5/4.
 */
@Repository
public class SecuritySettingsDaoImpl implements SecuritySettingsDao{

    @Autowired
    private BaseDao baseDao;
    /**
     * 验证旧密码是否存在
     */
    @Override
    public DMap checkOldPassword(String userId) {
        DMap dMap = new DMap();
        if(userId != null){
            dMap = DaoTool.select("select * from member_info where ID = '"+userId+"'");
        }
        return dMap;
    }
    /**
     * 更新密码
     */
    @Override
    public DaoResult updatePassWord(MemberInfo memberInfo) {
        return baseDao.update(memberInfo);
    }
    /**
     * 保存紧急联系人
     */
    @Override
    public DaoResult saveContact(MemberLinkman memberLinkman) {
        if(memberLinkman == null ){
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        DMap dMap = DaoTool.select("select * from member_linkman where MEMBERID = '"+memberLinkman.getMemberid()+"'");
        if(dMap.getCount() >= 1){
            String id = dMap.getValue("ID",0);
            memberLinkman.setId(id);
            return baseDao.update(memberLinkman);
        }
        return baseDao.insert(memberLinkman);
    }
    /**
     * 查询一个紧急联系人
     */
    @Override
    public DMap queryOnlyContact(String memberId) {
        String sql = "SELECT * FROM member_linkman WHERE MEMBERID = '"+memberId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 保存邮箱
     */
    @Override
    public DaoResult saveEmail(MemberInfo memberInfo) {
        return baseDao.update(memberInfo);
    }
    /**
     * 保存邮箱验证标识
     */
    @Override
    public DaoResult saveEmailMessage(MemberInfo memberInfo) {
        return baseDao.update(memberInfo);
    }
    /**
     * 更改验证状态
     */
    @Override
    public DaoResult updateEmailMessage(MemberInfo memberInfo) {
        return baseDao.update(memberInfo);
    }
    /**
     * 验证邮箱标识
     */
    @Override
    public DMap checkEmailMessage(String message, String id) {
        if(message == null || id == null){
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select * from member_info where ID = '"+id+"' and MAIL_FLG = '"+message+"'");
    }
    /**
     * 验证是否认证成功
     */
    @Override
    public DMap checkEmailIsSuccess(String email) {
        if(email == null){
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select * from member_info where EMAIL = '"+email+"' and MAIL_FLG = '"+0+"'");
    }
    /**
     * 查询一个问题（所有问题表）
     */
    @Override
    public DMap queryOnlyquestion(int id) {
        return DaoTool.select("select * from sys_security_question_parameter where id = "+id);
    }
    /**
     * 查询是否设置安全保护问题
     */
    @Override
    public DMap queryIsquestion(String userId) {
        if(userId == null || userId.equals("")){
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select * from member_setup_questions where MEMBERID = '" + userId + "'");
    }

    /**
     * 保存问题
     */
    @Override
    public DaoResult saveQuestion(MemberSetupQuestions memberSetupQuestions) {
        if(memberSetupQuestions == null){
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        DMap dMap = DaoTool.select("select * from member_setup_questions where MEMBERID = '" + memberSetupQuestions.getMemberid() + "'");
        if(dMap.getCount() == 1){
            memberSetupQuestions.setId(Integer.parseInt(dMap.getValue("ID",0)));
            return baseDao.update(memberSetupQuestions);
        }
        return baseDao.insert(memberSetupQuestions);
    }

    /**
     * 保存交易密码
     */
    @Override
    public DaoResult saveTransactionPwd(MemberInfo memberInfo) {
        return baseDao.update(memberInfo);
    }

    /**
     * 获取一个问题提问和答案
     */
    @Override
    public DMap getOneQuestion(String userId) {
        String sql = "select * from member_setup_questions where 1 = 1";
        if (!userId.equals("") && userId != null) {
            sql = sql + " and MEMBERID = '" + userId + "'";
        }
        return DaoTool.select(sql);
    }

    /**
     * 查询一个省
     */
    @Override
    public DMap queryProvinceById(String id) {
        String sql = "select * from sys_province where 1 = 1";
        if (!id.equals("") && id != null) {
            sql = sql + " and ID = '" + id + "'";
        }
        return DaoTool.select(sql);
    }

    /**
     * 查询一个市
     */
    @Override
    public DMap queryCityById(String id) {
        String sql = "select * from sys_city where 1 = 1";
        if (!id.equals("") && id != null) {
            sql = sql + " and ID = '" + id + "'";
        }
        return DaoTool.select(sql);
    }

    /**
     * 忘记密码 重置密码
     */
    @Override
    public DaoResult reSetPw(MemberInfo memberInfo) {
        if(memberInfo == null){
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }

        return baseDao.update(memberInfo);
    }

    /**
     * 验证是否有此手机号
     */
    @Override
    public DMap checkIsTelephone(String telephone) {
        String sql = "select * from member_info where 1 = 1";
        if (telephone != null && !telephone.equals("")) {
            sql = sql + " and telephone ='" + telephone + "'";
        }
        System.out.println("================"+sql);
        return DaoTool.select(sql);
    }

    /**
     * 更新紧急联系人
     */
    @Override
    public DaoResult updateContact(MemberLinkman memberLinkman) {
        if(memberLinkman == null){
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        return baseDao.update(memberLinkman);
    }
}
