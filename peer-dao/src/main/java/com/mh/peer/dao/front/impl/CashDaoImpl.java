package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.CashDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.PostalRecord;
import com.mh.peer.model.entity.SerialnumberWh;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.HuanXunPostalMessageBean;
import com.mh.peer.model.message.PostalRecordMessageBean;
import com.mh.peer.model.message.TradeDetailMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Cuibin on 2016/6/20.
 */
@Repository
public class CashDaoImpl implements CashDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 按照id查询城市
     */
    @Override
    public DMap queryCityById(String id) {
        if (id == null || id.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select * from sys_city where id = '" + id + "'");
    }

    /**
     * 按照id查询省
     */
    @Override
    public DMap queryProvinceById(String id) {
        if (id == null || id.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select * from sys_province where id = '" + id + "'");
    }

    /**
     * 提现
     */
    @Override
    public boolean userCash(PostalRecord postalRecord, TradeDetail tradeDetail, SerialnumberWh serialnumberWh, MemberInfo memberInfo) {
        DConnection conn = DaoTool.getConnection();
        DaoResult resultCharge = new DaoResult();
        DaoResult resultTrade = new DaoResult();
        DaoResult resultMember = new DaoResult();
        DaoResult resultSerial = new DaoResult();

        try {
            resultCharge = baseDao.insert(postalRecord, conn); //充值表
            resultTrade = baseDao.insert(tradeDetail, conn); //流水表
            resultMember = baseDao.update(memberInfo, conn); //会员表
            resultSerial = baseDao.update(serialnumberWh, conn); //流水号表

            if (resultCharge.getCode() < 0) {
                //System.out.println("================" + resultCharge.getErrText());
                conn.rollback();
                return false;
            }
            if (resultTrade.getCode() < 0) {
                //System.out.println("================" + resultTrade.getErrText());
                conn.rollback();
                return false;
            }
            if (resultMember.getCode() < 0) {
                //System.out.println("================" + resultMember.getErrText());
                conn.rollback();
                return false;
            }
            if (resultSerial.getCode() < 0) {
                //System.out.println("================" + resultSerial.getErrText());
                conn.rollback();
                return false;
            }
        } catch (Exception e) {
            conn.rollback();
        } finally {
            conn.commit();
            conn.close();
        }
        return true;
    }

    /**
     * 查询提现记录
     * @param huanXunPostalMes
     * @return
     */
    @Override
    public DMap queryPostalRecordByPage(HuanXunPostalMessageBean huanXunPostalMes) {
        String sql = "SELECT * FROM view_huanxun_postal WHERE 1 = 1";
        String pageIndex = huanXunPostalMes.getPageIndex(); //起始数
        String pageSize = huanXunPostalMes.getPageSize(); //每页显示数量
        String orderColumn = huanXunPostalMes.getOrderColumn(); //排序字段
        String orderType = huanXunPostalMes.getOrderType(); //排序类型
        String startTime = huanXunPostalMes.getStartTime(); //开始时间
        String endTime = huanXunPostalMes.getEndTime(); //结束时间
        String memberId = huanXunPostalMes.getMemberid(); //会员id

        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        if (startTime != null && !startTime.equals("")) { //起始时间
            sql = sql + " AND DATE(CREATETIME) >= DATE('"+startTime+"')";
        }
        if (endTime != null && !endTime.equals("")) { //截止时间
            sql = sql + " AND DATE(CREATETIME) <= DATE('"+endTime+"')";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 查询提现记录
     * @param huanXunPostalMes
     * @return
     */
    @Override
    public DMap queryPostalRecordByPageCount(HuanXunPostalMessageBean huanXunPostalMes) {
        String sql = "SELECT * FROM view_huanxun_postal WHERE 1 = 1";
        String startTime = huanXunPostalMes.getStartTime(); //开始时间
        String endTime = huanXunPostalMes.getEndTime(); //结束时间
        String memberId = huanXunPostalMes.getMemberid(); //会员id

        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        if (startTime != null && !startTime.equals("")) { //起始日期
            sql = sql + " AND DATE(CREATETIME) >= DATE('"+startTime+"')";
        }
        if (endTime != null && !endTime.equals("")) { //结束日期
            sql = sql + " AND DATE(CREATETIME) <= DATE('"+endTime+"')";
        }

        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取可提现金额
     * @param memberId
     * @return
     */
    @Override
    public DMap getPostaledPrice(String memberId) {
        String sql = "SELECT * FROM view_member_app WHERE 1 = 1";
        if (memberId != null && !memberId.equals("")){
            sql = sql + " AND ID = '"+memberId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 查询提现金额 到账金额 手续费用
     */
    @Override
    public DMap querySum(String memberId) {

        return DaoTool.select("select sum(SPRICE) SPRICESUM,sum(PROCEDUREFEE) PROCEDUREFEESUM,sum(POSTALPRICE) POSTALPRICESUM" +
                " from view_postal_record" +
                " where MEMBERID = '" + memberId + "'");
    }
}
