package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunTransferDao;
import com.mh.peer.exception.HuanXunTransferAttornException;
import com.mh.peer.exception.HuanXunTransferInvestException;
import com.mh.peer.exception.HuanXunTransferRepayException;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunTransferMessage;
import com.mh.peer.util.PublicsTool;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-7-31.
 * 根据条件获取转账信息
 */
@Repository
public class HuanXunTransferDaoImpl implements HuanXunTransferDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(HuanXunTransferDao.class);
    @Autowired
    private BaseDao baseDao;

    /**
     * 根据条件获取转账信息
     *
     * @param huanXunTransferMes
     * @return
     */
    @Override
    public DMap getTransferInfoBySome(HuanXunTransferMessage huanXunTransferMes) {
        String transferId = huanXunTransferMes.getId(); //主键
        String sql = "SELECT * FROM huanxun_transfer WHERE 1 = 1";
        DMap dMap = new DMap();
        try {

            if (transferId != null && !transferId.equals("")) { //主键
                sql = sql + " AND ID = '" + transferId + "'";
            }
            dMap = DaoTool.select(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 投资转账
     *
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     */
    @Override
    public DaoResult transferBuyResult(Map<String,Object> mapException,HuanxunTransfer huanxunTransfer, List<HuanxunTransferDetail> listHuanxunTransferDetail, String productId) {
        DConnection conn = DaoTool.getConnection();
        DaoResult daoResultTransfer = new DaoResult(); //转账
        DaoResult daoResultTransferDetail = new DaoResult(); //转账详情
        DaoResult daoResultFreeze = new DaoResult(); //冻结部分
        DaoResult daoResultMember = new DaoResult(); //转入方会员部分
        DaoResult daoResultProduct = new DaoResult(); //产品部分
        DaoResult daoResultOutMember = new DaoResult(); //转出方会员部分
        String outIpsAcctNo = ""; //转出方ips存管账号
        String inIpsAcctNo = ""; //转入方ips存管账号
        String outMemberId = ""; //转出方会员id
        String memberId = ""; //会员id(转入方会员id)
        Double balance = 0.0d; //余额
        String points = "0"; //积分(String型)
        Double pointsDou = 0d; //积分(Double型)
        Double ipstrdAmt = 0.0d; //转账金额

        try {

            daoResultTransfer = baseDao.insert(huanxunTransfer, conn); //转账
            if (daoResultTransfer.getCode() < 0) {
                HuanXunTransferInvestException.handle(mapException);
                conn.rollback();
            } else {
                if (huanxunTransfer.getResultcode() != null && !huanxunTransfer.getResultcode().equals("")) {
                    if (huanxunTransfer.getResultcode().equals("000000")) { //转账成功
                        if (listHuanxunTransferDetail != null && listHuanxunTransferDetail.size() > 0) {
                            for (HuanxunTransferDetail huanxunTransferDetail : listHuanxunTransferDetail) {
                                HuanxunTransferDetail huanxunTransferDetail2 = new HuanxunTransferDetail();
                                huanxunTransferDetail2.setId(huanxunTransferDetail.getId()); //主键
                                huanxunTransferDetail2.setTransferid(huanxunTransferDetail.getTransferid()); //转账id
                                huanxunTransferDetail2.setMerbillno(huanxunTransferDetail.getMerbillno()); //商户订单号

                                outIpsAcctNo = huanxunTransferDetail.getOutipsacctno(); //转出方IPS存管账号
                                huanxunTransferDetail2.setOutipsacctno(outIpsAcctNo);

                                inIpsAcctNo = huanxunTransferDetail.getInipsacctno();//转入方IPS存管账号
                                huanxunTransferDetail2.setInipsacctno(inIpsAcctNo);

                                huanxunTransferDetail2.setIpsbillno(huanxunTransferDetail.getIpsbillno()); //IPS订单号
                                huanxunTransferDetail2.setIpsdotime(huanxunTransferDetail.getIpsdotime()); //IPS处理时间

                                ipstrdAmt = huanxunTransferDetail.getIpstrdamt(); //IPS转账金额(实际转账)
                                huanxunTransferDetail2.setIpstrdamt(ipstrdAmt);

                                huanxunTransferDetail2.setTrdstatus(huanxunTransferDetail.getTrdstatus()); //状态
                                huanxunTransferDetail2.setCreatetime(huanxunTransferDetail.getCreatetime()); //创建时间

                                daoResultTransferDetail = baseDao.insert(huanxunTransferDetail2, conn); //转账明细
                                if (daoResultTransferDetail.getCode() < 0) {
                                    System.out.println("新增转账详情失败！");
                                    HuanXunTransferInvestException.handle(mapException);
                                    conn.rollback();
                                } else {
                                    String sqlMember = "SELECT BALANCE,MEMBERID FROM view_huanxun_regist WHERE IPSACCTNO = '" + inIpsAcctNo + "'";
                                    DMap dMapMember = DaoTool.select(sqlMember);
                                    if (dMapMember != null && dMapMember.getCount() > 0) {
                                        memberId = dMapMember.getValue("MEMBERID", 0); //会员id

                                        balance = TypeTool.getDouble(dMapMember.getValue("BALANCE", 0)); //余额
                                        balance = balance + ipstrdAmt; //转账后余额

                                        String sqlMemberUpdate = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
                                        DMap dMapMemberData = new DMap();
                                        dMapMemberData.setData("balance", balance); //余额
                                        dMapMemberData.setData("memberId", memberId); //会员id
                                        daoResultMember = DaoTool.update(sqlMemberUpdate, dMapMemberData.getData(), conn); //余额更新成功
                                        if (daoResultMember.getCode() < 0) {
                                            HuanXunTransferInvestException.handle(mapException);
                                            conn.rollback();
                                        }else{
                                            String sqlOutMemberInfo = "SELECT MEMBERID,POINTS FROM view_huanxun_regist WHERE IPSACCTNO = '"+outIpsAcctNo+"'";
                                            DMap dMapOutMember = DaoTool.select(sqlOutMemberInfo); //转出方信息
                                            if (dMapOutMember != null && dMapOutMember.getCount() > 0){
                                                outMemberId = dMapOutMember.getValue("MEMBERID",0); //转出方会员id
                                                points = dMapOutMember.getValue("POINTS",0); //积分(String型)
                                                if (points != null && !points.equals("")){
                                                    pointsDou = TypeTool.getDouble(points); //积分(Double型)
                                                    pointsDou = PublicsTool.round(pointsDou,2);
                                                    ipstrdAmt = PublicsTool.round(ipstrdAmt,2);
                                                    pointsDou = pointsDou + ipstrdAmt; //积分
                                                    pointsDou = PublicsTool.round(pointsDou,2);
                                                    String sqlOutMemberUpdate = "UPDATE member_info SET POINTS = <pointsDou> WHERE ID = <outMemberId>";
                                                    DMap dMapOutMemberData = new DMap();
                                                    dMapOutMemberData.setData("pointsDou",pointsDou); //积分
                                                    dMapOutMemberData.setData("outMemberId",outMemberId); //转出方会员id
                                                    daoResultOutMember = DaoTool.update(sqlOutMemberUpdate,dMapOutMemberData.getData(),conn);
                                                    if (daoResultOutMember.getCode() < 0){
                                                        HuanXunTransferInvestException.handle(mapException);
                                                        conn.rollback();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            String sqlfreezeUpdate = "UPDATE huanxun_freeze SET FLAG = '2' WHERE PRODUCTID = <productId>";
                            DMap dMapfreezeUpdate = new DMap();
                            dMapfreezeUpdate.setData("productId", productId); //产品id
                            daoResultFreeze = DaoTool.update(sqlfreezeUpdate, dMapfreezeUpdate.getData(), conn);
                            if (daoResultFreeze.getCode() < 0) {
                                HuanXunTransferInvestException.handle(mapException);
                                conn.rollback();
                            } else {
                                String sqlProductUpdate = "UPDATE product_content SET SHFLAG = '0' WHERE ID = <productId>";
                                DMap dMapProductUpdate = new DMap();
                                dMapProductUpdate.setData("productId", productId); //产品id
                                daoResultProduct = DaoTool.update(sqlProductUpdate, dMapProductUpdate.getData(), conn);
                                if (daoResultProduct.getCode() < 0) {
                                    HuanXunTransferInvestException.handle(mapException);
                                    conn.rollback();
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            HuanXunTransferInvestException.handle(mapException);
            e.printStackTrace();
            conn.rollback();
        } finally {
            conn.commit();
            conn.close();
        }
        return daoResultProduct;
    }


    /**
     * 债权转账
     *
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     * @param attornId
     * @return
     */
    @Override
    public DaoResult transferAttornResult(Map<String,Object> mapException,HuanxunTransfer huanxunTransfer, List<HuanxunTransferDetail> listHuanxunTransferDetail, String attornId) {
        DConnection conn = DaoTool.getConnection();
        String outIpsAcctNo = ""; //转出方IPS存管账号
        String inIpsAcctNo = ""; //转入方IPS存管账号
        Double ipstrdAmt = 0.0d; //IPS转账金额
        String memberId = ""; //会员id
        Double balance = 0.0d; //余额
        String buyId = ""; //购买人
        String attornPrice = ""; //转让价格
        String productId = ""; //产品主键
        String underTakeMemberId = ""; //承接人id
        String UidBuyId = ""; //产品购买id
        String receiveId = ""; //回款id
        DaoResult daoResultTransfer = new DaoResult(); //转账
        DaoResult daoResultTransferDetail = new DaoResult(); //转账详细
        DaoResult daoResultMember = new DaoResult(); //更新账户余额
        DaoResult daoResultFreeze = new DaoResult(); //更新债权冻结状态
        DaoResult daoResultProductAttorn = new DaoResult(); //更新产品债权审核状态
        DaoResult daoResultBuy = new DaoResult(); //产品购买(用于更新状态)
        DaoResult daoResultBuy2 = new DaoResult(); //产品购买(用于新增产品购买)
        DaoResult daoResultReceive = new DaoResult(); //产品回款

        try {

            daoResultTransfer = baseDao.insert(huanxunTransfer, conn); //转账
            if (daoResultTransfer.getCode() < 0) {
                HuanXunTransferAttornException.handle(mapException);
                conn.rollback();
            } else {
                if (huanxunTransfer.getResultcode() != null && !huanxunTransfer.getResultcode().equals("")) {
                    if (huanxunTransfer.getResultcode().equals("000000")) { //转账成功
                        if (listHuanxunTransferDetail != null && listHuanxunTransferDetail.size() > 0) {
                            for (HuanxunTransferDetail huanxunTransferDetail : listHuanxunTransferDetail) {
                                HuanxunTransferDetail huanxunTransferDetail2 = new HuanxunTransferDetail();
                                huanxunTransferDetail2.setId(huanxunTransferDetail.getId()); //主键
                                huanxunTransferDetail2.setTransferid(huanxunTransferDetail.getTransferid()); //转账id
                                huanxunTransferDetail2.setMerbillno(huanxunTransferDetail.getMerbillno()); //商户订单号

                                outIpsAcctNo = huanxunTransferDetail.getOutipsacctno(); //转出方IPS存管账号
                                huanxunTransferDetail2.setOutipsacctno(outIpsAcctNo);

                                inIpsAcctNo = huanxunTransferDetail.getInipsacctno();//转入方IPS存管账号
                                huanxunTransferDetail2.setInipsacctno(inIpsAcctNo);

                                huanxunTransferDetail2.setIpsbillno(huanxunTransferDetail.getIpsbillno()); //IPS订单号
                                huanxunTransferDetail2.setIpsdotime(huanxunTransferDetail.getIpsdotime()); //IPS处理时间

                                ipstrdAmt = huanxunTransferDetail.getIpstrdamt(); //IPS转账金额(实际转账)
                                huanxunTransferDetail2.setIpstrdamt(ipstrdAmt);

                                huanxunTransferDetail2.setTrdstatus(huanxunTransferDetail.getTrdstatus()); //状态
                                huanxunTransferDetail2.setCreatetime(huanxunTransferDetail.getCreatetime()); //创建时间

                                daoResultTransferDetail = baseDao.insert(huanxunTransferDetail2, conn); //转账明细
                                if (daoResultTransferDetail.getCode() < 0) {
                                    HuanXunTransferAttornException.handle(mapException);
                                    conn.rollback();
                                } else {
                                    String sqlMember = "SELECT BALANCE,MEMBERID FROM view_huanxun_regist WHERE IPSACCTNO = '" + inIpsAcctNo + "'";
                                    DMap dMapMember = DaoTool.select(sqlMember);
                                    if (dMapMember != null && dMapMember.getCount() > 0) {
                                        memberId = dMapMember.getValue("MEMBERID", 0); //会员id
                                        balance = TypeTool.getDouble(dMapMember.getValue("BALANCE", 0)); //余额
                                        balance = balance + ipstrdAmt; //转账后余额
                                        String sqlMemberUpdate = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
                                        DMap dMapMemberData = new DMap();
                                        dMapMemberData.setData("balance", balance); //余额
                                        dMapMemberData.setData("memberId", memberId); //会员id
                                        daoResultMember = DaoTool.update(sqlMemberUpdate, dMapMemberData.getData(), conn); //余额更新成功
                                        if (daoResultMember.getCode() < 0) {
                                            HuanXunTransferAttornException.handle(mapException);
                                            conn.rollback();
                                        }
                                    }
                                }
                            }

                            String sqlfreezeUpdate = "UPDATE huanxun_freeze_attorn SET FLAG = '2' WHERE ATTORNID = <attornId>";
                            DMap dMapfreezeUpdate = new DMap();
                            dMapfreezeUpdate.setData("attornId", attornId); //债权id
                            daoResultFreeze = DaoTool.update(sqlfreezeUpdate, dMapfreezeUpdate.getData(), conn);
                            if (daoResultFreeze.getCode() < 0) {
                                HuanXunTransferAttornException.handle(mapException);
                                conn.rollback();
                            } else {
                                String sqlProductAttornUpdate = "UPDATE product_attorn_record SET SHFLAG = '0' WHERE ID = <attornId>";
                                DMap dMapProductAttornUpdate = new DMap();
                                dMapProductAttornUpdate.setData("attornId", attornId); //债权id
                                daoResultProductAttorn = DaoTool.update(sqlProductAttornUpdate, dMapProductAttornUpdate.getData(), conn);
                                if (daoResultProductAttorn.getCode() < 0) {
                                    HuanXunTransferAttornException.handle(mapException);
                                    conn.rollback();
                                } else {
                                    String sqlAttorn = "SELECT * FROM view_product_attorn_min WHERE 1 = 1"; //债权表查询条件
                                    if (attornId != null && !attornId.equals("")) {
                                        sqlAttorn = sqlAttorn + " AND ID = '" + attornId + "'";
                                        DMap dMapAttorn = DaoTool.select(sqlAttorn);
                                        if (dMapAttorn != null && dMapAttorn.getCount() > 0) {
                                            buyId = dMapAttorn.getValue("PRODUCTBUYID", 0); //购买id
                                            attornPrice = dMapAttorn.getValue("PRICE", 0); //债权转让金额
                                            productId = dMapAttorn.getValue("PRODUCTID", 0); //产品主键
                                            underTakeMemberId = dMapAttorn.getValue("UNDERTAKEMEMBER", 0); //承接人id
                                        }

                                        /*************** 更新购买表类型Start ***************/
                                        ProductBuyInfo productBuyInfo = new ProductBuyInfo();
                                        productBuyInfo.setId(buyId); //购买id
                                        productBuyInfo.setType("2"); //类型 0-持有 1-转让中 2-已转让
                                        daoResultBuy = baseDao.update(productBuyInfo, conn);
                                        /**************** 更新购买表类型End ****************/

                                        if (daoResultBuy.getCode() < 0) { //产品购买失败
                                            HuanXunTransferAttornException.handle(mapException);
                                            conn.rollback();
                                        } else {
                                            ProductBuyInfo productBuyInfo2 = new ProductBuyInfo();
                                            UidBuyId = UUID.randomUUID().toString().replaceAll("-", "");
                                            productBuyInfo2.setId(UidBuyId); //产品购买id
                                            productBuyInfo2.setProductid(productId); //产品id
                                            productBuyInfo2.setBuyer(underTakeMemberId); //购买人id
                                            productBuyInfo2.setPrice(TypeTool.getDouble(attornPrice)); //购买金额
                                            productBuyInfo2.setBuytime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //购买时间
                                            productBuyInfo2.setType("0"); //类型 0-持有 1-转让中 2-已转让
                                            productBuyInfo2.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                            daoResultBuy2 = baseDao.insert(productBuyInfo2, conn);
                                            if (daoResultBuy2.getCode() < 0) {
                                                HuanXunTransferAttornException.handle(mapException);
                                                conn.rollback();
                                            } else {
                                                String sqlReceive = "SELECT * FROM product_receive WHERE 1 = 1 AND DATE(YRECEIVETIME) > DATE(NOW())"
                                                        + " AND SRECEIVETIME IS NULL";
                                                if (buyId != null && !buyId.equals("")) {
                                                    sqlReceive = sqlReceive + " AND BUYID = '" + buyId + "'";
                                                    System.out.println("sqlReceive======"+sqlReceive);
                                                    DMap dMapReceive = DaoTool.select(sqlReceive);
                                                    if (dMapReceive != null && dMapReceive.getCount() > 0) {
                                                        for (int i = 0; i < dMapReceive.getCount(); i++) {
                                                            System.out.println("新buyId======"+UidBuyId);
                                                            receiveId = dMapReceive.getValue("ID", i); //回款id
                                                            ProductReceive productReceive = new ProductReceive();
                                                            productReceive.setId(receiveId); //回款id
                                                            productReceive.setBuyid(UidBuyId); //购买id
                                                            daoResultReceive = baseDao.update(productReceive, conn);
                                                            if (daoResultReceive.getCode() < 0) {
                                                                HuanXunTransferAttornException.handle(mapException);
                                                                conn.rollback();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            HuanXunTransferAttornException.handle(mapException);
            e.printStackTrace();
            conn.rollback();
        } finally {
            conn.commit();
            conn.close();
        }
        return daoResultProductAttorn;
    }


    /**
     * 保存还款转账信息 张尔鑫
     */
    @Override
    public DaoResult saveHuanxunTransfer(HuanxunTransfer huanxunTransfer,List<HuanxunTransferDetail> listHuanxunTransferDetail,String receiveId) {
        DConnection conn = DaoTool.getConnection();
        DaoResult daoResultTransfer = new DaoResult(); //还款转账
        DaoResult daoResultTransferDetail = new DaoResult(); //还款详细信息转账
        DaoResult daoResultMember = new DaoResult(); //会员信息部分
        DaoResult daoResultReceive = new DaoResult(); //回款部分
        DaoResult daoResultRepay = new DaoResult(); //还款部分
        DaoResult daoResultMessageReceive = new DaoResult(); //回款消息
        DaoResult daoResultMessageRepay = new DaoResult(); //还款信息

        String outIpsAcctNo = ""; //转出方IPS存管账号
        String inIpsAcctNo = ""; //转入方IPS存管账号
        Double ipstrdAmt = 0.0d; //IPS转账金额(实际转账)
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS处理时间
        String memberId = ""; //会员id
        Double balance = 0.0d; //余额
        String productId = ""; //产品id
        String yReceiveTime = ""; //应还款日期
        String repayId = ""; //还款id
        String repayer = ""; //还款人
        String repaymentPrice = ""; //实际还款金额(String型)
        Double repaymentPriceDou = 0.0d; //实际还款金额(Double型)

        try{

            daoResultTransfer = baseDao.insert(huanxunTransfer,conn); //转账信息
            if (daoResultTransfer.getCode() < 0){
                LOGGER.info("转账失败！");
                conn.rollback();
            }else{
                LOGGER.info("转账成功！");
                if (listHuanxunTransferDetail != null && listHuanxunTransferDetail.size() > 0){ //转账详情
                    for (HuanxunTransferDetail huanxunTransferDetail : listHuanxunTransferDetail){
                        outIpsAcctNo = huanxunTransferDetail.getOutipsacctno(); //转出方IPS存管账号
                        inIpsAcctNo = huanxunTransferDetail.getInipsacctno();//转入方IPS存管账号
                        ipstrdAmt = huanxunTransferDetail.getIpstrdamt(); //IPS转账金额(实际转账)
                        ipsBillNo = huanxunTransferDetail.getIpsbillno(); //IPS订单号
                        ipsDoTime = huanxunTransferDetail.getIpsdotime(); //IPS订单处理时间

                        /******************** 转账详情部分Start ********************/
                        HuanxunTransferDetail huanxunTransferDetail2 = new HuanxunTransferDetail();
                        huanxunTransferDetail2.setId(huanxunTransferDetail.getId()); //主键
                        huanxunTransferDetail2.setTransferid(huanxunTransferDetail.getTransferid()); //转账id
                        huanxunTransferDetail2.setMerbillno(huanxunTransferDetail.getMerbillno()); //商户订单号
                        huanxunTransferDetail2.setOutipsacctno(outIpsAcctNo); //转出方IPS存管账号
                        huanxunTransferDetail2.setInipsacctno(inIpsAcctNo); //转入方IPS存管账号
                        huanxunTransferDetail2.setIpsbillno(ipsBillNo); //IPS订单号
                        huanxunTransferDetail2.setIpsdotime(ipsDoTime); //IPS处理时间
                        huanxunTransferDetail2.setIpstrdamt(ipstrdAmt); //IPS转账金额(实际转账)
                        huanxunTransferDetail2.setTrdstatus(huanxunTransferDetail.getTrdstatus()); //状态
                        huanxunTransferDetail2.setCreatetime(huanxunTransferDetail.getCreatetime()); //创建时间
                        /********************* 转账详情部分End *********************/

                        daoResultTransferDetail = baseDao.insert(huanxunTransferDetail2, conn); //转账明细
                        if (daoResultTransferDetail.getCode() < 0){
                            LOGGER.info("转账明细失败！");
                            conn.rollback();
                        }else{
                            LOGGER.info("转账明细成功！");
                            String sqlMember = "SELECT BALANCE,MEMBERID FROM view_huanxun_regist WHERE IPSACCTNO = '"+inIpsAcctNo+"'";
                            DMap dMapMember = DaoTool.select(sqlMember); //根据IPS账号查询当前人员余额、会员id
                            if (dMapMember != null && dMapMember.getCount() > 0) {

                                /********** 更新回款者余额部分Start **********/
                                memberId = dMapMember.getValue("MEMBERID", 0); //会员id
                                balance = TypeTool.getDouble(dMapMember.getValue("BALANCE", 0)); //余额
                                balance = balance + ipstrdAmt; //转账后余额
                                String sqlMemberUpdate = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
                                DMap dMapMemberData = new DMap();
                                dMapMemberData.setData("balance", balance); //余额
                                dMapMemberData.setData("memberId", memberId); //会员id
                                daoResultMember = DaoTool.update(sqlMemberUpdate, dMapMemberData.getData(), conn); //余额更新成功
                                /*********** 更新回款者余额部分End ***********/

                                if (daoResultMember.getCode() < 0){
                                    LOGGER.info("会员信息余额更新失败！");
                                    conn.rollback();
                                }else{
                                    LOGGER.info("会员信息余额更新成功！");

                                    /********** 更新回款信息Start **********/
                                    String sqlReceiveUpdate = "UPDATE product_receive SET RECEIVEPRICE = <ipstrdAmt>,SRECEIVETIME = NOW() WHERE ID = <receiveId>";
                                    DMap dMapReceive = new DMap();
                                    dMapReceive.setData("ipstrdAmt",ipstrdAmt); //转账金额
                                    dMapReceive.setData("receiveId",receiveId); //回款id
                                    daoResultReceive = DaoTool.update(sqlReceiveUpdate,dMapReceive.getData(),conn);
                                    /*********** 更新回款信息End ***********/

                                    if (daoResultReceive.getCode() < 0){
                                        LOGGER.info("回款部分失败！");
                                        conn.rollback();
                                    }else{
                                        LOGGER.info("回款部分成功！");
                                        String sqlReceiveSelect = "SELECT YRECEIVETIME,PRODUCTID FROM view_product_autoRepay WHERE 1 = 1"
                                                                + " AND ID = '"+receiveId+"'";
                                        DMap dMapReceiveSelect = DaoTool.select(sqlReceiveSelect); //回款部分信息查询

                                        if (dMapReceiveSelect != null){
                                            productId = dMapReceiveSelect.getValue("PRODUCTID",0); //产品id
                                            yReceiveTime = dMapReceiveSelect.getValue("YRECEIVETIME",0); //应还款日期

                                            String sqlRepaySelect = "SELECT * FROM view_product_repay WHERE 1 = 1"
                                                                  + " AND PRODUCTID = '"+productId+"'"
                                                                  + " AND DATE(YREPAYMENTTIME) = DATE('"+yReceiveTime+"')";
                                            DMap dMapRepaySelect = DaoTool.select(sqlRepaySelect);
                                            if (dMapRepaySelect != null){
                                                repayId = dMapRepaySelect.getValue("ID",0); //还款id
                                                repaymentPrice = dMapRepaySelect.getValue("REPAYMENTPRICE",0); //实际还款金额(String型)
                                                if (repaymentPrice != null && !repaymentPrice.equals("")){
                                                    repaymentPriceDou = TypeTool.getDouble(repaymentPrice); //实际还款金额(Double型)
                                                }
                                                repayer = dMapRepaySelect.getValue("APPLYMEMBER"); //还款人id
                                                if (repayId != null && !repayId.equals("")){ //还款id

                                                    /********** 更新还款信息Start **********/
                                                    repaymentPriceDou = repaymentPriceDou + ipstrdAmt; //还款金额
                                                    String sqlRepayUpdate = "UPDATE product_repay SET REPAYMENTPRICE = <repaymentPriceDou>,"
                                                                          + "SREPAYMENTTIME = NOW() WHERE ID = <repayId>";
                                                    DMap dMapRepay = new DMap();
                                                    dMapRepay.setData("repaymentPriceDou",repaymentPriceDou); //还款价格
                                                    dMapRepay.setData("repayId",repayId); //还款id
                                                    daoResultRepay = DaoTool.update(sqlRepayUpdate,dMapRepay.getData(),conn); //还款部分
                                                    /*********** 更新还款信息End ***********/

                                                    if (daoResultRepay.getCode() < 0){
                                                        LOGGER.info("还款部分失败！");
                                                        conn.rollback();
                                                    }else{
                                                        LOGGER.info("还款部分成功！");
                                                        String UidReceiveMessage = UUID.randomUUID().toString().replaceAll("-",""); //回款消息主键
                                                        String receiveTitle = "回款到账"; //回款题目
                                                        String receiveContent = "<li>您有"+ipstrdAmt+"回款到账</li>"
                                                                              + "<li>转出方IPS账号："+outIpsAcctNo+"</li>"
                                                                              + "<li>IPS订单号："+ipsBillNo+"</li>"
                                                                              + "<li>IPS订单处理时间："+ipsDoTime+"</li>";

                                                        MemberMessage memberMessageReceive = new MemberMessage(); //回款部分
                                                        memberMessageReceive.setId(UidReceiveMessage); //回款信息主键
                                                        memberMessageReceive.setGlid(receiveId); //关联id
                                                        memberMessageReceive.setType("0"); //类型(0-系统消息)
                                                        memberMessageReceive.setMemberid(memberId); //会员id
                                                        memberMessageReceive.setReadflag("1"); //读取状态(0-已读、1-未读)
                                                        memberMessageReceive.setTitle(receiveTitle); //题目
                                                        memberMessageReceive.setContent(receiveContent); //内容
                                                        memberMessageReceive.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送时间
                                                        memberMessageReceive.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                                        memberMessageReceive.setDelflg("1"); //是否删除(0-是、1-否)
                                                        daoResultMessageReceive = baseDao.insert(memberMessageReceive,conn); //回款信息

                                                        if (daoResultMessageReceive.getCode() < 0){
                                                            LOGGER.info("发送回款信息失败！");
                                                            conn.rollback();
                                                        }else{
                                                            LOGGER.info("发送回款信息成功！");
                                                            String UidRepayMessage = UUID.randomUUID().toString().replaceAll("-",""); //还款消息主键
                                                            String repayTitle = "还款到账"; //还款题目
                                                            String repayContent = "<li>您有"+ipstrdAmt+"还款到账</li>"
                                                                                + "<li>转入方IPS账号："+inIpsAcctNo+"</li>"
                                                                                + "<li>IPS订单号："+ipsBillNo+"</li>"
                                                                                + "<li>IPS订单处理时间："+ipsDoTime+"</li>";

                                                            MemberMessage memberMessageRepay = new MemberMessage(); //回款部分
                                                            memberMessageRepay.setId(UidRepayMessage); //还款信息主键
                                                            memberMessageReceive.setGlid(repayId); //关联id
                                                            memberMessageReceive.setType("0"); //类型(0-系统消息)
                                                            memberMessageReceive.setMemberid(repayer); //会员id
                                                            memberMessageReceive.setReadflag("1"); //读取状态(0-已读、1-未读)
                                                            memberMessageReceive.setTitle(receiveTitle); //题目
                                                            memberMessageReceive.setContent(receiveContent); //内容
                                                            memberMessageReceive.setSendtime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //发送时间
                                                            memberMessageReceive.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                                                            memberMessageReceive.setDelflg("1"); //是否删除(0-是、1-否)
                                                            daoResultMessageRepay = baseDao.insert(memberMessageRepay,conn); //还款信息
                                                            if (daoResultMessageRepay.getCode() < 0){
                                                                LOGGER.info("发送还款信息失败！");
                                                                conn.rollback();
                                                            }else{
                                                                LOGGER.info("发送还款信息成功！");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }

        return daoResultRepay;
    }


    /**
     * 查询转账信息是否存在 bycuibin
     */
    @Override
    public DMap queryIsTransfer(String uuid) {
        String sql = "select * from huanxun_transfer where ID = '" + uuid + "'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 根据产品id和应还款时间获取转账信息
     * @param productId
     * @param yrepaymentTime
     * @return
     */
    @Override
    public DMap getRepayTransferAcctDetail(String productId, String yrepaymentTime) {
        String sql = "SELECT BUYERIPSACCTNO,PRINCIPAL,INTEREST,BUYER FROM view_product_receive WHERE"
                   + " PRODUCTID = '"+productId+"' AND DATE(YRECEIVETIME) = DATE('"+yrepaymentTime+"')"
                   + " AND SRECEIVETIME IS NULL";
        System.out.println("sql======"+sql);
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 根据冻结信息进行还款
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     * @param repayId
     * @return
     */
    @Override
    public String repayhxPriceTransfer(HuanxunTransfer huanxunTransfer,List<HuanxunTransferDetail> listHuanxunTransferDetail,
                                          Map<String,Object> mapException,String repayId,String yRepayTime) {

        DConnection conn = DaoTool.getConnection();
        DaoResult daoResultTransfer = new DaoResult(); //还款转账
        DaoResult daoResultTransferDetail = new DaoResult(); //还款详细信息转账
        DaoResult daoResultMember = new DaoResult(); //会员信息部分
        DaoResult daoResultReceive = new DaoResult(); //回款信息
        DaoResult daoResultRepay = new DaoResult(); //还款部分
        DaoResult daoResultFreezeRepay = new DaoResult(); //环迅还款冻结部分
        DaoResult daoResultTransferBf = new DaoResult(); //环迅转账备份
        DaoResult daoResultTransferDetailBf = new DaoResult(); //环迅转账详细信息备份
        DMap dMapTransferBf = new DMap(); //转账备份
        String resultCode = ""; //响应码(000000-成功、999999-失败)
        String batchNo = ""; //商户转账批次编号
        String outIpsAcctNo = ""; //转出方IPS存管账号
        String inIpsAcctNo = ""; //转入方IPS存管账号
        Double ipstrdAmt = 0.0d; //IPS转账金额(实际转账)
        String ipsBillNo = ""; //IPS订单号
        String ipsDoTime = ""; //IPS订单处理时间
        String memberId = ""; //会员id
        Double balance = 0.0d; //余额(Double型)
        String receiveId = ""; //回款id
        String freezeRepayId = ""; //环迅还款冻结id
        String inMerFee = "0"; //转入方平台手续费(String型)
        Double inMerFeeDou = 0.0d; //转让方平台手续费(Double型)
        Double sIpstrdAmt = 0.0d; //实际转账金额
        String merBillNo = ""; //商户订单号
        String trdStatus = ""; //转账状态(0-失败、1-成功)
        String flagTransfer = "2"; //转账状态(0-转账成功、1-转账失败、2-待转账)
        String flagTransferDetail = "2"; //转账明细状态(0-转账成功、1-转账失败、2-待转账)
        String result = "0"; //执行状态(0-成功、1-失败)

        try{

            resultCode = huanxunTransfer.getResultcode(); //响应码(000000-成功、999999-失败)
            if (resultCode != null && !resultCode.equals("")){
                if (resultCode.equals("000000")){
                    flagTransfer = "0"; //转账成功
                }else{
                    flagTransfer = "2"; //待转账
                }
            }

            daoResultTransfer = baseDao.insert(huanxunTransfer,conn); //转账信息
            if (daoResultTransfer.getCode() < 0){ //转账失败
                System.out.println("转账信息录入失败！");
                HuanXunTransferRepayException.handle(mapException);
                result = "1";
                conn.rollback();
            }else{
                System.out.println("转账信息录入成功！");
                if (resultCode != null && !resultCode.equals("")){
                    if (resultCode.equals("000000")){ //成功
                        batchNo = huanxunTransfer.getBatchno(); //商户转账批次编号
                        String sqlTransferBf = "UPDATE huanxun_transfer_bf SET FLAG = <flagTransfer> WHERE BATCHNO = <batchNo>";
                        dMapTransferBf.setData("flagTransfer",flagTransfer); //转账状态(0-转账成功、1-转账失败、2-待转账)
                        dMapTransferBf.setData("batchNo",batchNo); //商户转账批次编号
                        daoResultTransferBf = DaoTool.update(sqlTransferBf,dMapTransferBf.getData(),conn);
                        if (daoResultTransferBf.getCode() < 0){
                            System.out.println("转账备份录入失败");
                            HuanXunTransferRepayException.handle(mapException);
                            result = "1";
                            conn.rollback();
                        }else{
                            System.out.println("转账备份录入成功");
                            String sqlFreezeRepay = "SELECT * FROM huanxun_freeze_repay WHERE REPAYID = '"+repayId+"' AND RESULTCODE = '000000'";
                            DMap dMapFreezeRepay = DaoTool.select(sqlFreezeRepay); //查询还款冻结信息
                            if (dMapFreezeRepay != null && dMapFreezeRepay.getCount() > 0){
                                /********** 循环更新冻结状态Start **********/
                                for (int i=0;i<dMapFreezeRepay.getCount();i++){
                                    freezeRepayId = dMapFreezeRepay.getValue("ID",i); //环迅还款冻结id
                                    String sqlFreezeRepayUpdate = "UPDATE huanxun_freeze_repay SET FLAG = '2' WHERE ID = <freezeRepayId>";
                                    DMap dMapFreezeRepaySearch = new DMap();
                                    dMapFreezeRepaySearch.setData("freezeRepayId",freezeRepayId);
                                    daoResultFreezeRepay = DaoTool.update(sqlFreezeRepayUpdate,dMapFreezeRepaySearch.getData(),conn);
                                    if (daoResultFreezeRepay.getCode() < 0){
                                        HuanXunTransferRepayException.handle(mapException); //转账异常处理
                                        result = "1";
                                        conn.rollback();
                                    }
                                }
                                /*********** 循环更新冻结状态End ***********/
                            }

                            if (result.equals("1")){
                                System.out.println("更新还款冻结状态失败！");
                            }else{
                                System.out.println("更新还款冻结状态成功！");
                                String sqlRepayUpdate = "UPDATE product_repay SET REPAYMENTPRICE = (PRINCIPAL + INTEREST),"
                                        + "SREPAYMENTTIME = NOW() WHERE ID = <repayId>";
                                DMap dMapRepay = new DMap();
                                dMapRepay.setData("repayId",repayId); //还款id
                                daoResultRepay = DaoTool.update(sqlRepayUpdate,dMapRepay.getData(),conn);

                                if (daoResultRepay.getCode() < 0){
                                    System.out.println("更新还款信息失败！");
                                    HuanXunTransferRepayException.handle(mapException);
                                    result = "1";
                                    conn.rollback();
                                }else{
                                    System.out.println("更新还款信息成功！");
                                    if (listHuanxunTransferDetail != null && listHuanxunTransferDetail.size() > 0) { //转账详情
                                        for (HuanxunTransferDetail huanxunTransferDetail : listHuanxunTransferDetail){
                                            outIpsAcctNo = huanxunTransferDetail.getOutipsacctno(); //转出方IPS存管账号
                                            inIpsAcctNo = huanxunTransferDetail.getInipsacctno();//转入方IPS存管账号
                                            ipstrdAmt = huanxunTransferDetail.getIpstrdamt(); //IPS转账金额(实际转账)
                                            ipsBillNo = huanxunTransferDetail.getIpsbillno(); //IPS订单号
                                            ipsDoTime = huanxunTransferDetail.getIpsdotime(); //IPS订单处理时间
                                            merBillNo = huanxunTransferDetail.getMerbillno(); //商户订单号
                                            trdStatus = huanxunTransferDetail.getTrdstatus(); //转账状态(0-失败、1-成功)

                                            /******************** 转账详情部分Start ********************/
                                            HuanxunTransferDetail huanxunTransferDetail2 = new HuanxunTransferDetail();
                                            huanxunTransferDetail2.setId(huanxunTransferDetail.getId()); //主键
                                            huanxunTransferDetail2.setTransferid(huanxunTransferDetail.getTransferid()); //转账id
                                            huanxunTransferDetail2.setMerbillno(huanxunTransferDetail.getMerbillno()); //商户订单号
                                            huanxunTransferDetail2.setOutipsacctno(outIpsAcctNo); //转出方IPS存管账号
                                            huanxunTransferDetail2.setInipsacctno(inIpsAcctNo); //转入方IPS存管账号
                                            huanxunTransferDetail2.setIpsbillno(ipsBillNo); //IPS订单号
                                            huanxunTransferDetail2.setIpsdotime(ipsDoTime); //IPS处理时间
                                            huanxunTransferDetail2.setIpstrdamt(ipstrdAmt); //IPS转账金额(实际转账)
                                            huanxunTransferDetail2.setTrdstatus(trdStatus); //转账状态(0-失败、1-成功)
                                            huanxunTransferDetail2.setCreatetime(huanxunTransferDetail.getCreatetime()); //创建时间
                                            /********************* 转账详情部分End *********************/

                                            daoResultTransferDetail = baseDao.insert(huanxunTransferDetail2, conn); //转账明细
                                            if (daoResultTransferDetail.getCode() < 0){
                                                System.out.println("转账明细信息录入失败！");
                                                HuanXunTransferRepayException.handle(mapException);
                                                result = "1";
                                                conn.rollback();
                                            }else{
                                                System.out.println("转账明细信息录入成功！");
                                                if (trdStatus != null && !trdStatus.equals("")){ //转账状态(0-失败、1-成功)
                                                    if (trdStatus.equals("1")){ //成功
                                                        flagTransferDetail = "0"; //转账状态明细(0-转账成功、1-转账失败、2-待转账)
                                                        String sqlMember = "SELECT BALANCE,MEMBERID FROM view_huanxun_regist WHERE IPSACCTNO = '"+inIpsAcctNo+"'";
                                                        DMap dMapMember = DaoTool.select(sqlMember); //根据IPS账号查询当前人员余额、会员id
                                                        if (dMapMember != null && dMapMember.getCount() > 0) {
                                                            /********** 根据转账备份信息查询平台手续费Start **********/
                                                            String sqlTransferDetailBf = "SELECT * FROM huanxun_transfer_detail_bf WHERE MERBILLNO = '"+merBillNo+"'";
                                                            DMap dMapTransferDetailBf = DaoTool.select(sqlTransferBf);
                                                            if (dMapTransferDetailBf != null && dMapTransferDetailBf.getCount() > 0){
                                                                inMerFee = dMapTransferDetailBf.getValue("INMERFEE",0); //平台手续费(String型)
                                                                if (inMerFee != null && !inMerFee.equals("")){
                                                                    inMerFeeDou = TypeTool.getDouble(inMerFee); //平台手续费(Double型)
                                                                }
                                                            }
                                                            System.out.println("商户订单号："+merBillNo+",平台手续费："+inMerFeeDou);
                                                            /*********** 根据转账备份信息查询平台手续费End ***********/

                                                            String sqlTransferDetailBfUpdate = "UPDATE huanxun_transfer_detail_bf SET FLAG = <flagTransferDetail> WHERE MERBILLNO = <merBillNo>";
                                                            DMap dMapTransferDetailBfUpdate = new DMap();
                                                            dMapTransferDetailBfUpdate.setData("flagTransferDetail",flagTransferDetail); //转账明细状态
                                                            dMapTransferDetailBfUpdate.setData("merBillNo",merBillNo); //商户订单号
                                                            daoResultTransferDetailBf = DaoTool.update(sqlTransferDetailBfUpdate,dMapTransferDetailBfUpdate.getData(),conn);
                                                            if (daoResultTransferDetailBf.getCode() < 0){
                                                                System.out.println("更新转账详细信息失败！");
                                                                HuanXunTransferRepayException.handle(mapException);
                                                                result = "1";
                                                                conn.rollback();
                                                            }else{
                                                                System.out.println("更新转账详细信息成功！");
                                                                /********** 更新回款者余额部分Start **********/
                                                                memberId = dMapMember.getValue("MEMBERID", 0); //会员id
                                                                balance = TypeTool.getDouble(dMapMember.getValue("BALANCE", 0)); //余额
                                                                System.out.println("转账前余额："+balance);
                                                                balance = balance + ipstrdAmt - inMerFeeDou; //转账后余额
                                                                System.out.println("转账后余额："+balance);
                                                                String sqlMemberUpdate = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
                                                                DMap dMapMemberData = new DMap();
                                                                dMapMemberData.setData("balance", balance); //余额
                                                                dMapMemberData.setData("memberId", memberId); //会员id
                                                                daoResultMember = DaoTool.update(sqlMemberUpdate, dMapMemberData.getData(), conn); //余额更新成功
                                                                /*********** 更新回款者余额部分End ***********/

                                                                if (daoResultMember.getCode() < 0){
                                                                    System.out.println("更新余额信息失败！");
                                                                    HuanXunTransferRepayException.handle(mapException);
                                                                    result = "1";
                                                                    conn.rollback();
                                                                }else{
                                                                    System.out.println("更新余额信息成功！");
                                                                    String sqlReceive = "SELECT ID RECEIVEID FROM view_product_autoRepay WHERE "
                                                                            + "OUTIPSACCTNO = '"+outIpsAcctNo+"' AND INIPSACCTNO = '"+inIpsAcctNo+"'"
                                                                            + " AND DATE(YRECEIVETIME) = DATE('"+yRepayTime+"') AND SRECEIVETIME IS NULL";
                                                                    DMap dMapReceive = DaoTool.select(sqlReceive);
                                                                    System.out.println("需更新回款信息数量为："+dMapReceive.getCount());
                                                                    if (dMapReceive != null && dMapReceive.getCount() > 0){
                                                                        receiveId = dMapReceive.getValue("RECEIVEID",0); //回款id
                                                                        sIpstrdAmt = ipstrdAmt - inMerFeeDou; //实际转账金额
                                                                        String sqlReceiveUpdate = "UPDATE product_receive SET RECEIVEPRICE = <ipstrdAmt>,"
                                                                                + "SRECEIVETIME = <ipsDoTime> WHERE ID = <receiveId>";
                                                                        DMap dMapReceiveData = new DMap();
                                                                        dMapReceiveData.setData("ipstrdAmt",sIpstrdAmt); //实际转账金额
                                                                        dMapReceiveData.setData("ipsDoTime",ipsDoTime); //IPS处理时间
                                                                        dMapReceiveData.setData("receiveId",receiveId); //回款id
                                                                        daoResultReceive = DaoTool.update(sqlReceiveUpdate,dMapReceiveData.getData(),conn); //回款信息更新
                                                                        if (daoResultReceive.getCode() < 0){
                                                                            HuanXunTransferRepayException.handle(mapException);
                                                                            result = "1";
                                                                            conn.rollback();
                                                                        }else{
                                                                            System.out.println("更新回款信息成功！");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            HuanXunTransferRepayException.handle(mapException);
            result = "1";
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }
        return result;
    }
}
