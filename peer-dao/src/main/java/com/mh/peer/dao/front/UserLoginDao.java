package com.mh.peer.dao.front;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.AppMemberMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/4/22.
 */
public interface UserLoginDao {
    /**
     * 用户登录
     */
    public MemberInfo userLogin(MemberInfo memberInfo);

    /**
     * 保存登陆时间
     */
    public DaoResult saveLoginTime(MemberInfo memberInfo);
    /**
     * 登录操作
     * @param appMemberMes
     * @return
     */
    public DMap login(AppMemberMessageBean appMemberMes);

    /**
     * 用户注册
     * @param memberInfo
     * @return
     */
    public DaoResult regist(MemberInfo memberInfo);

    /**
     * 获取会员数量
     * @param appMemberMes
     * @return
     */
    DMap getMemberCount(AppMemberMessageBean appMemberMes);

    /**
     * 获取会员信息(App端)
     * @param appMemberMes
     * @return
     */
    DMap getMemberInfo(AppMemberMessageBean appMemberMes);

    /**
     * 修改密码
     * @param memberId
     * @param password
     * @return
     */
    DaoResult updatePassword(String memberId,String password);
}
