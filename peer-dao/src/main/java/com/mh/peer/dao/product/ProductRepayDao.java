package com.mh.peer.dao.product;

import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;

/**
 * Created by zhangerxin on 2016-5-27.
 * 还款列表
 */
public interface ProductRepayDao {

    /**
     * 获取还款列表
     * @param productRepayMes
     * @return
     */
    DMap getRepayListBySome(ProductRepayMessageBean productRepayMes);

    /**
     * 获取还款部分数量
     * @param productRepayMes
     * @return
     */
    DMap getRepayCount(ProductRepayMessageBean productRepayMes);

    /**
     * 获取需还款总额
     * @param productRepayMes
     * @return
     */
    DMap getRepayPriceSum(ProductRepayMessageBean productRepayMes);

    /**
     * 待还总额 & 到期还款金额 by cuibin
     */
    public DMap getRepayedPrice(ProductRepayMessageBean productRepayMessageBean,String repayFlg);

    /**
     * 获取待还(已还)金额、待还(已还)期数(App端)
     * @param memberId
     * @param repayFlag
     * @return
     */
    public AppProductRepayTjMessageBean getAppRepayInfo(String memberId,String repayFlag);

    /**
     * 获取待还(已还)列表(App端)
     * @param memberId
     * @param repayFlag
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DMap getAppRepayList(String memberId,String repayFlag,String pageIndex,String pageSize);

    /**
     * 获取产品还款详细信息(App端)
     * @param productRepayId
     * @return
     */
    DMap getAppRepayDetails(String productRepayId);

    /**
     * 获取逾期项目集合
     * @param overRepayMes
     * @return
     */
    DMap getOverDueRepayList(ProductOverRepayMessageBean overRepayMes);

    /**
     * 获取逾期还款列表数量 (By buyatao)
     * @param overRepayMes
     * @return
     */
    public DMap getOverRepayByPage(ProductOverRepayMessageBean overRepayMes);

    /**
     * 获取还款明细列表集合 (By buyatao)
     * @param productRepayMes
     * @return
     */
    DMap getRepayDetail(ProductRepayMessageBean productRepayMes);

    /**
     * 获取环迅部分应还款信息
     * @param repayId
     * @return
     */
    DMap getRepayInfoById(String repayId);

    /**
     * 获取还款中产品信息集合
     * @param repayingMes
     * @return
     */
    DMap getRepayingList(ProductRepayMessageBean repayingMes);

    /**
     * 获取还款中产品信息集合数量 (By buyatao)
     * @param repayingMes
     * @return
     */
    public DMap getRepayingByPage(ProductRepayMessageBean repayingMes);

    /**
     * 获取已结清产品信息集合
     * @param repayingMes
     * @return
     */
    DMap getFinishList(ProductRepayMessageBean repayingMes);

    /**
     * 获取已结清产品信息集合数量 (By buyatao)
     * @param repayingMes
     * @return
     */
    public DMap getFinishByPage(ProductRepayMessageBean repayingMes);
}
