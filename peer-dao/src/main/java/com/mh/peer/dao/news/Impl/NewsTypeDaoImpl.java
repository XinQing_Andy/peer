package com.mh.peer.dao.news.Impl;

import com.mh.peer.dao.news.NewsTypeDao;
import com.mh.peer.model.entity.NewsType;
import com.mh.peer.model.message.NewsTypeMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by zhangerxin on 2016/3/25.
 */
@Repository
public class NewsTypeDaoImpl implements NewsTypeDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 查询新闻类别
     * @param newsType
     * @return
     */
    @Override
    public List<NewsType> getNewsType(NewsType newsType) {
        List listNewsType = baseDao.findAll(newsType.getClass());
        if (listNewsType.size() == 0) {
            return listNewsType;
        }
        return listNewsType;
    }

    /**
     * 修改新闻类别
     * @param newsType
     * @return
     */
    @Override
    public DaoResult updateNewsType(NewsType newsType) {
        DaoResult result = new DaoResult();
        if (newsType == null) {
            result.setCode(-1);
            result.setErrText("修改失败");
            return result;
        }
        result = baseDao.update(newsType);
        return result;
    }

    /**
     * 获取所有新闻类别
     * @param newsTypeMes
     * @return
     */
    @Override
    public DMap getAllNewsFirstType(NewsTypeMessageBean newsTypeMes) {
        String sql = "SELECT * FROM news_type WHERE fid = 0";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 根据一级类别获取所有二级新闻类别
     * @param newsTypeMes
     * @return
     */
    @Override
    public DMap getAllNewsSecondType(NewsTypeMessageBean newsTypeMes) {
        String firstTypeId = newsTypeMes.getFid(); //一级新闻类别id
        String sql = "SELECT * FROM news_type WHERE fid = "+ TypeTool.getInt(firstTypeId)+"";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
