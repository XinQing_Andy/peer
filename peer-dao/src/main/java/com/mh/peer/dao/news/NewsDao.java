package com.mh.peer.dao.news;

import com.mh.peer.model.business.NewsContentBusinessBean;
import com.mh.peer.model.entity.NewsContent;
import com.mh.peer.model.entity.NewsType;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.NewsContentMessageBean;
import com.mh.peer.model.message.NewsTypeMessageBean;
import com.mh.peer.model.message.WeathInformationMessageBean;
import com.mh.peer.model.message.WeixinNewsMes;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import java.util.List;

/**
 * Created by Cuibin on 2016/4/7.
 */
public interface NewsDao {
    /**
     *查询数量
     */
    DMap queryNewsByPageCount(NewsContentMessageBean newsContentM);

    /**
     * 内容管理(查询所有新闻内容 带分页)
     */
    DMap queryNewsByPage(NewsContentMessageBean newsContentMes);

    /**
     * 新闻数量
     * @param newsContentMes
     * @return
     */
    DMap queryNewsCount(NewsContentMessageBean newsContentMes);

    /**
     * 类别管理(查询所有类别)
     */
    DMap queryType();
    /**
     * 类别管理-编辑后保存
     */
    DaoResult saveType(NewsType newsType);
    /**
     * 内容管理 删除新闻
     */
    DaoResult deleteNews(NewsContent newsContent);

    /**
     * 内容管理 添加新闻
     */
    DaoResult saveNews(NewsContent newsContent);

    /**
     * 内容管理  (保存图片信息)
     */
    DaoResult saveImg(SysFile sysFile);
    /**
     * 内容管理 修改新闻 (查询单个新闻)
     */
    DMap queryNewsById(NewsContentMessageBean newsContent);
    /**
     * 内容管理 修改新闻 (查询图片信息)
     */
    DMap queryImg(String newsId);
    /**
     * 内容管理 修改新闻 (提交新闻)
     */
    DaoResult updateNews(NewsContent newsContent);
    /**
     * 内容管理 删除图片
     */
    DaoResult deleteImg(SysFile sysFile);

    /**
     * 内容管理 查询新闻分类
     */
    DMap queryNewsType(NewsTypeMessageBean newsTypeMes);

    /**
     * 修改新闻是否显示
     */
    DaoResult updateShow(NewsContent newsContent);

    /**
     * 修改新闻是否显示
     */
    DaoResult updateRecommend(NewsContent newsContent);
    /**
     * 添加二级类别
     */
    DaoResult addTwoType(NewsType newsType);

    /**
     * 删除二级类别
     */
    DaoResult deleteTwoNewsType(NewsType newsType);
    /**
     * 更新新闻访问次数(用户端)
     */
    DaoResult updateNewsVisit(NewsContent newsContent);

    /**
     * 获取新闻(App端)
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DMap getAppNews(String orderColumn,String orderType,String pageIndex,String pageSize);

    /**
     * 获取新闻部分详情
     * @param newsId
     * @return
     */
    DMap getAppNewsDetails(String newsId);

    /**
     * 获取新闻列表(微信) by cuibin
     */
    public DMap queryNewsList(WeixinNewsMes weixinNewsMes);

    /**
     * 查询一个新闻(微信) by cuibin
     */
    public DMap queryOnlyNews(String id);

    /**
     * 获取新闻列表总数(微信) by cuibin
     */
    public DMap queryNewsCount(WeixinNewsMes weixinNewsMes);

    /**
     * 搜索财富资讯集合
     * @param weathInformationMes
     * @return
     */
    DMap queryWeathByPage(WeathInformationMessageBean weathInformationMes);

    /**
     * 搜索财富资讯数量
     * @param weathInformationMes
     * @return
     */
    DMap queryWeathCount(WeathInformationMessageBean weathInformationMes);

    /**
     * 搜索财富资讯详细信息
     * @param weathInformationMes
     * @return
     */
    DMap queryWeathDetails(WeathInformationMessageBean weathInformationMes);
}
