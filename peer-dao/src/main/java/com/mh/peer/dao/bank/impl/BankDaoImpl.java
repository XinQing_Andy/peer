package com.mh.peer.dao.bank.impl;

import com.mh.peer.dao.bank.BankDao;
import com.mh.peer.model.entity.BankWh;
import com.mh.peer.model.entity.MemberBankno;
import com.mh.peer.model.message.BankWhMessageBean;
import com.mh.peer.model.message.MemberBanknoMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/5/16.
 */
@Repository
public class BankDaoImpl implements BankDao {
    @Autowired
    private BaseDao baseDao;

    /**
     * 查询所有银行
     */
    @Override
    public DMap queryAllBank(BankWhMessageBean bankWhMessageBean) {
        if (bankWhMessageBean == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String pageIndex = bankWhMessageBean.getPageIndex(); //当前页
        String pageSize = bankWhMessageBean.getPageSize(); //每页显示数量
        String keyword = bankWhMessageBean.getKeyword();// 关键字

        String sql = "select * from bank_wh where 1 = 1";

        if(keyword != null && !keyword.equals("")){
            sql = sql + " and BANKNAME like '%"+keyword+"%'";
        }

        sql = sql + " order by OPERATIME DESC";

        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
        //System.out.println("sql ===== " + sql);
        return dmap;
    }

    /**
     * 查询所有银行数量
     */
    @Override
    public DMap queryAllBankCount(BankWhMessageBean bankWhMessageBean) {
        if (bankWhMessageBean == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String keyword = bankWhMessageBean.getKeyword();// 关键字
        String sql = "select * from bank_wh where 1 = 1";
        if(keyword != null && !keyword.equals("")){
            sql = sql + " and BANKNAME like '%"+keyword+"%'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 保存银行
     */
    @Override
    public DaoResult saveBank(BankWh bankWh) {
        if (bankWh == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }

        DaoResult daoResult = baseDao.insert(bankWh);
        //System.out.println("==================" + daoResult.getErrText());
        return daoResult;
    }

    /**
     * 查询一个银行
     */
    @Override
    public DMap queryOnlyBank(String id) {
        if (null == id || "".equals(id)) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select * from bank_wh where id = '" + id + "'");
    }
    /**
     * 更新银行
     */
    @Override
    public DaoResult updateBank(BankWh bankWh) {
        if (bankWh == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        return baseDao.update(bankWh);

    }
    /**
     * 删除银行
     */
    @Override
    public DaoResult deleteBank(BankWh bankWh) {
        if (bankWh == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        return baseDao.delete(bankWh);
    }
    /**
     * 保存银行（前台）
     */
    @Override
    public DaoResult saveFrontBank(MemberBankno memberBankno) {

        if(memberBankno == null){
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        DaoResult daoResult = baseDao.insert(memberBankno);
        System.out.println(daoResult.getErrText());
        return daoResult;
    }
    /**
     * 更新银行（前台）
     */
    @Override
    public DaoResult frontUpdatebank(MemberBankno memberBankno) {
        if(memberBankno == null){
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        DaoResult daoResult = baseDao.update(memberBankno);
        //System.out.println(daoResult.getErrText());
        return daoResult;
    }
    /**
     * 查询当前银行卡信息
     */
    @Override
    public DMap queryUserBank(String userId) {
        if(userId == null || userId.equals("")){
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select * from member_bankno where MEMBERID = '"+userId+"'");
    }


    /**
     * 根据条件查询个人绑定银行卡信息
     * @param memberBanknoMes
     * @return
     */
    @Override
    public DMap getMemberBankInfoBySome(MemberBanknoMessageBean memberBanknoMes) {
        String sql = "SELECT * FROM view_member_bankno WHERE 1 = 1";
        String memberId = memberBanknoMes.getMemberid(); //会员id
        if (memberId != null && !memberId.equals("")){
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
