package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.AdvertContent;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.stereotype.Repository;

/**
 * Created by wanghongjian on 2016/3/23.
 */
@Repository
public interface AdvertDao {

    DMap selectadvertlist(AdvertContent advertContent);

    DMap selectadvert(AdvertContent advertContent);

    DaoResult edit(AdvertContent advertContent);

    DaoResult delete(AdvertContent advertContent);

    DaoResult add(AdvertContent advertContent);

}
