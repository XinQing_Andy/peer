package com.mh.peer.dao.product.Impl;

import com.mh.peer.dao.product.ProductBuyDao;
import com.mh.peer.model.entity.ProductBuyInfo;
import com.mh.peer.model.message.ProductInvestMessageBean;
import com.mh.peer.model.message.ProductMyInvestMessageBean;
import com.mh.peer.model.message.ProductReceiveMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-5-9.
 */
@Repository
public class ProductBuyDaoImpl implements ProductBuyDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 新增产品购买
     * @param productBuyInfo
     * @return
     */
    @Override
    public DaoResult saveProductBuyInfo(ProductBuyInfo productBuyInfo) {
        DaoResult result = new DaoResult();
        if (productBuyInfo == null || productBuyInfo == null) {
            result.setCode(-1);
            result.setErrText("添加失败！");
            return result;
        }

        DConnection conn = DaoTool.getConnection();
        result = baseDao.insert(productBuyInfo, conn); //新增产品购买
        if (result.getCode() < 0) {
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }


    /**
     * 获取投资集合
     * @param productInvestMes
     * @return
     */
    @Override
    public DMap getAllListBySome(ProductInvestMessageBean productInvestMes) {
        String pageIndex = productInvestMes.getPageIndex(); //当前页
        String pageSize = productInvestMes.getPageSize(); //每页显示数量
        String orderColumn = productInvestMes.getOrderColumn(); //排序字段
        String orderType = productInvestMes.getOrderType(); //排序类型
        String productId = productInvestMes.getProductId(); //产品id
        String productBuyId = productInvestMes.getId(); //购买id
        String buyStartTime = productInvestMes.getBuyStartTime(); //投资起始时间
        String buyEndTime = productInvestMes.getBuyEndTime(); //投资截止时间
        String nextReceiveSTime = productInvestMes.getNextReceiveSTime(); //下次还款起日期
        String nextReceiveETime = productInvestMes.getNextReceiveETime(); //下次还款止日期
        String squareSTime = productInvestMes.getSquareSDate(); //结清起日期
        String squareETime = productInvestMes.getSquareEDate(); //结清止日期
        String buyer = productInvestMes.getBuyer(); //购买人
        String fullScale = productInvestMes.getFullScale(); //是否满标(0-是、1-否)
        String title = productInvestMes.getTitle(); //题目
        String code = productInvestMes.getCode(); //编码
        String producttype = productInvestMes.getProductType(); //产品类型id
        String investFlag = productInvestMes.getInvestFlag(); //投资状态(投资状态(0-回收中、1-投标中、2-已结清))

        String sql = "SELECT * FROM view_product_buy WHERE 1 = 1";

        if(productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        if(producttype != null && !producttype.equals("")){ //产品类型id
            sql = sql + " AND PRODUCTTYPE = '"+producttype+"'";
        }
        if(buyer != null && !buyer.equals("")){ //购买人
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        if(buyStartTime != null && !buyStartTime.equals("")){ //购买起始时间
            sql = sql + " AND BUYTIME >= '"+buyStartTime+"'";
        }
        if(buyEndTime != null && !buyEndTime.equals("")){ //购买截止时间
            sql = sql + " AND BUYTIME <= '"+buyEndTime+"'";
        }
        if(nextReceiveSTime != null && !nextReceiveSTime.equals("")){ //下次还款起始日期
            sql = sql + " AND NEXTRECEIVETIME >= '"+nextReceiveSTime+"'";
        }
        if(nextReceiveETime != null && !nextReceiveETime.equals("")){ //下次还款截止日期
            sql = sql + " AND NEXTRECEIVETIME <= '"+nextReceiveETime+"'";
        }
        if(squareSTime != null && !squareSTime.equals("")){ //结清起日期
            sql = sql + " AND SQUAREDATE >= '"+squareSTime+"'";
        }
        if(squareETime != null && !squareETime.equals("")){ //结清止日期
            sql = sql + " AND SQUAREDATE <= '"+squareETime+"'";
        }
        if(fullScale != null && !fullScale.equals("")){ //是否满标(0-是、1-否)
            sql = sql + " AND FULLSCALE = '"+fullScale+"'";
        }
        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(code != null && !code.equals("")){ //编码
            sql = sql + " AND CODE LIKE '%"+code+"%'";
        }
        if(investFlag != null && !investFlag.equals("")){ //投资状态(0-回收中、1-投标中、2-已结清)
            sql = sql + " AND INVESTFLAG = '"+investFlag+"'";
        }
        if(productBuyId != null && !productBuyId.equals("")){ //产品购买id
            sql = sql + " AND ID = '"+productBuyId+"'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){ //排序部分
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取投资部分信息数量
     * @param productInvestMes
     * @return
     */
    @Override
    public DMap getProductInvestCount(ProductInvestMessageBean productInvestMes) {
        String sql = "SELECT * FROM view_product_buy WHERE 1 = 1";
        String pageIndex = productInvestMes.getPageIndex(); //当前页
        String pageSize = productInvestMes.getPageSize(); //每页显示数量
        String orderColumn = productInvestMes.getOrderColumn(); //排序字段
        String orderType = productInvestMes.getOrderType(); //排序类型
        String productId = productInvestMes.getProductId(); //产品id
        String Id = productInvestMes.getId(); //购买id
        String buyStartTime = productInvestMes.getBuyStartTime(); //购买(投资)起始时间
        String buyEndTime = productInvestMes.getBuyEndTime(); //购买(投资)截止时间
        String nextReceiveSTime = productInvestMes.getNextReceiveSTime(); //下次还款起日期
        String nextReceiveETime = productInvestMes.getNextReceiveETime(); //下次还款止日期
        String squareSTime = productInvestMes.getSquareSDate(); //结清起日期
        String squareETime = productInvestMes.getSquareEDate(); //结清止日期
        String buyer = productInvestMes.getBuyer(); //购买人
        String fullScale = productInvestMes.getFullScale(); //是否满标(0-是、1-否)
        String title = productInvestMes.getTitle(); //题目
        String code = productInvestMes.getCode(); //编码
        String producttype = productInvestMes.getProductType(); //产品类型id
        String investFlag = productInvestMes.getInvestFlag(); //投资状态(投资状态(0-回收中、1-投标中、2-已结清))

        if(productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        if(producttype != null && !producttype.equals("")){ //产品类型id
            sql = sql + " AND PRODUCTTYPE = '"+producttype+"'";
        }
        if(buyer != null && !buyer.equals("")){ //购买人
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        if(buyStartTime != null && !buyStartTime.equals("")){ //购买起始时间
            sql = sql + " AND BUYTIME >= '"+buyStartTime+"'";
        }
        if(buyEndTime != null && !buyEndTime.equals("")){ //购买截止时间
            sql = sql + " AND BUYTIME <= '"+buyEndTime+"'";
        }
        if(nextReceiveSTime != null && !nextReceiveSTime.equals("")){ //下次还款起始日期
            sql = sql + " AND NEXTRECEIVETIME >= '"+nextReceiveSTime+"'";
        }
        if(nextReceiveETime != null && !nextReceiveETime.equals("")){ //下次还款截止日期
            sql = sql + " AND NEXTRECEIVETIME <= '"+nextReceiveETime+"'";
        }
        if(squareSTime != null && !squareSTime.equals("")){ //结清起日期
            sql = sql + " AND SQUAREDATE >= '"+squareSTime+"'";
        }
        if(squareETime != null && !squareETime.equals("")){ //结清止日期
            sql = sql + " AND SQUAREDATE <= '"+squareETime+"'";
        }
        if(fullScale != null && !fullScale.equals("")){ //是否满标(0-是、1-否)
            sql = sql + " AND FULLSCALE = '"+fullScale+"'";
        }
        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(code != null && !code.equals("")){ //编码
            sql = sql + " AND CODE LIKE '%"+code+"%'";
        }
        if(investFlag != null && !investFlag.equals("")){ //投资状态(0-回收中、1-投标中、2-已结清)
            sql = sql + " AND INVESTFLAG = '"+investFlag+"'";
        }
        if(Id!=null&&!Id.equals("")){
            sql = sql + " AND ID = '"+Id+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 根据条件获取回收数量
     * @param productReceiveMes
     * @return
     */
    @Override
    public DMap getReceiveCount(ProductReceiveMessageBean productReceiveMes) {
        String sql = "SELECT COUNT(*)RECEIVECOUNT FROM view_product_receive WHERE 1 = 1";
        String isRecycle = productReceiveMes.getIsRecycle(); //0-待回收、1-已回收
        String buyer = productReceiveMes.getBuyer(); //投资人id
        if (buyer != null && !buyer.equals("")){
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        if (isRecycle != null && !isRecycle.equals("")){ //回收状态
            if (isRecycle.equals("0")){ //待回收
                sql = sql + " AND SRECEIVETIME IS NULL";
            }else if (isRecycle.equals("1")){ //已回收
                sql = sql + " AND SRECEIVETIME IS NOT NULL";
            }
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 根据id获取投资信息(zhangerxin)
     * @param productMyInvestMes
     * @return
     */
    @Override
    public DMap getInvestInfoById(ProductMyInvestMessageBean productMyInvestMes) {
        String sql = "SELECT * FROM view_product_buy WHERE 1 = 1"; //根据id查询投资信息
        String id = productMyInvestMes.getId(); //投资id
        if(id != null && !id.equals("")){ //主键
            sql = sql + " AND ID = '"+id+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 根据条件获取回款金额总和
     * @param productReceiveMes
     * @return
     */
    @Override
    public DMap getReceivePriceSum(ProductReceiveMessageBean productReceiveMes) {
        String sql = "SELECT IFNULL(SUM(SUMPRICE),0)DRECEIVEPRICE,IFNULL(SUM(IFNULL(RECEIVEPRICE,0)),0)YRECEIVEPRICE,"
                   + "IFNULL(SUM(PRINCIPAL),0)PRINCIPAL,IFNULL(SUM(INTEREST),0)INTEREST FROM view_product_receive WHERE 1 = 1";
        String buyer = productReceiveMes.getBuyer(); //投资人id
        String isRecycle = productReceiveMes.getIsRecycle(); //回收标志(0-待回收、1-已回收)
        if (buyer != null && !buyer.equals("")){ //投资人
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        if (isRecycle != null && !isRecycle.equals("")){
            if (isRecycle.equals("0")){ //待回收
                sql = sql + " AND SRECEIVETIME IS NULL";
            }else if (isRecycle.equals("1")){ //已回收
                sql = sql + " AND SRECEIVETIME IS NOT NULL";
            }
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 昨天新增投资数量 by cuibin
     */
    @Override
    public DMap queryLastDayMoney() {
        return DaoTool.select("SELECT count(*) AS buySum,SUBSTR(BUYTIME, 1, 13) AS buyTime FROM view_product_buy" +
                " WHERE TO_DAYS(NOW()) - TO_DAYS(BUYTIME) = 1" +
                " GROUP BY SUBSTR(BUYTIME, 1, 13)");
    }

    /**
     * 最近七天新增投资数量 by cuibin
     */
    @Override
    public DMap queryLastWeekMoney() {
        return DaoTool.select("SELECT count(*) AS buySum,BUYTIME AS buyTime FROM view_product_buy" +
                " WHERE SUBSTR(BUYTIME, 1, 10) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) and SUBSTR(BUYTIME, 1, 10) < SUBSTR(now(),1,10)" +
                " GROUP BY SUBSTR(BUYTIME, 1, 10)");
    }

    /**
     * 最近30天新增投资数量 by cuibin
     */
    @Override
    public DMap queryLastMonthMoney() {
        return DaoTool.select("SELECT count(*) as buySum,SUBSTR(BUYTIME,1,10) as buyTime FROM view_product_buy" +
                " WHERE SUBSTR(BUYTIME, 1, 10) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY) and SUBSTR(BUYTIME, 1, 10) < SUBSTR(now(),1,10)" +
                " group by SUBSTR(BUYTIME,1,10)");
    }

    /**
     * 昨天投资金额 by cuibin
     */
    @Override
    public DMap queryLastDayInvest() {
        return DaoTool.select("SELECT price, BUYTIME FROM view_product_buy WHERE TO_DAYS(NOW()) - TO_DAYS(BUYTIME) = 1 GROUP BY SUBSTR(BUYTIME, 1, 13)");
    }

    /**
     * 最近七天新增投资金额 by cuibin
     */
    @Override
    public DMap queryLastWeekInvest() {
        return DaoTool.select("SELECT price, BUYTIME FROM view_product_buy WHERE SUBSTR(BUYTIME, 1, 10) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) and SUBSTR(BUYTIME, 1, 10) < SUBSTR(now(),1,10)");
    }

    /**
     * 过去30天新增投资 by cuibin
     */
    @Override
    public DMap queryLastMonthInvest() {

        return DaoTool.select("SELECT price, BUYTIME FROM view_product_buy WHERE SUBSTR(BUYTIME, 1, 10) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY) and SUBSTR(BUYTIME, 1, 10) < SUBSTR(now(),1,10)");
    }

    /**
     * 按照日期查询投资数量 by cuibin
     */
    @Override
    public DMap queryInvestSumByDay(String startTime, String endTime) {
        return DaoTool.select("SELECT count(*) as InvestSum FROM view_product_buy WHERE SUBSTR(BUYTIME, 1, 10) >= '" + startTime + "' and SUBSTR(BUYTIME, 1, 10) < '" + endTime + "'");
    }

    /**
     * 按照日期查询投资金额 by cuibin
     */
    @Override
    public DMap queryInvestByDay(String startTime, String endTime) {
        return DaoTool.select("SELECT sum(price) as InvestMoney FROM view_product_buy WHERE SUBSTR(BUYTIME, 1, 10) >= '" + startTime + "' and SUBSTR(BUYTIME, 1, 10) < '" + endTime + "'");
    }

    /**
     * 按照指定日期查询投资金额 by cuibin
     */
    @Override
    public DMap queryInvestByDay(String time) {
        return DaoTool.select("SELECT sum(price) as InvestMoney FROM view_product_buy WHERE SUBSTR(BUYTIME, 1, 10) = '" + time + "'");
    }

    /**
     * 按照指定日期查询投资数量by cuibin
     */
    @Override
    public DMap queryInvestSumByDay(String time) {
        return DaoTool.select("SELECT count(*) as InvestSum FROM view_product_buy WHERE SUBSTR(BUYTIME, 1, 10) = '" + time + "'");
    }


    /**
     * 获取投资项目(App部分)
     * @param memberId
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public DMap getInvestProject(String memberId, String typeId, String pageIndex, String pageSize) {
        String sql = "SELECT * FROM view_product_buy_app WHERE 1 = 1";
        if (memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND BUYER = '"+memberId+"'";
        }
        if (typeId != null && !typeId.equals("")){ //产品类型id
            sql = sql + " AND TYPEID = '"+typeId+"'";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取产品详细信息
     * @param productId
     * @return
     */
    @Override
    public DMap queryProductDetails(String productId) {
        String sql = "SELECT * FROM v_product_content_min WHERE ID = '"+productId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取回款统计信息(已回款信息)
     * @param productBuyId
     * @return
     */
    @Override
    public DMap queryYReceiveInfo(String productBuyId) {
        String sql = "SELECT IFNULL(SUM(PRINCIPAL),0)PRINCIPAL,IFNULL(SUM(INTEREST),0)INTEREST,IFNULL(SUM(RECEIVEPRICE),0)RECEIVEPRICE"
                   + " FROM product_receive WHERE BUYID = '"+productBuyId+"' AND SRECEIVETIME IS NOT NULL";
        DMap dMap = DaoTool.select(sql); //已还款信息
        return dMap;
    }

    /**
     * 获取还款统计信息(未还款信息)
     * @param productBuyId
     * @return
     */
    @Override
    public DMap queryWReceiveInfo(String productBuyId) {
        String sql = "SELECT IFNULL(SUM(PRINCIPAL),0)PRINCIPAL,IFNULL(SUM(INTEREST),0)INTEREST,IFNULL(SUM(RECEIVEPRICE),0)RECEIVEPRICE"
                   + " FROM product_receive WHERE BUYID = '"+productBuyId+"' AND SRECEIVETIME IS NULL";
        DMap dMap = DaoTool.select(sql); //未还款信息
        return dMap;
    }


}
