package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.SysDept;
import com.salon.frame.util.DaoResult;
import java.util.List;

/**
 * Created by zhaojiaming on 2016/1/14.
 */
public interface DeptDao {

    /**
     * 获取部门
     * @param sysDept
     * @return
     */
    SysDept getDept(SysDept sysDept);

    /**
     * 获取所有部门信息
     * @param sysDept
     * @return
     */
    List<SysDept> getDeptInfo(SysDept sysDept);

    /**
     * 新增部门信息
     * @param sysDept
     * @return
     */
    DaoResult addDept(SysDept sysDept);

    /**
     * 修改部门信息
     * @param sysDept
     * @return
     */
    DaoResult updateDept(SysDept sysDept);

    /**
     * 删除部门信息
     * @param sysDept
     * @return
     */
    DaoResult delDept(SysDept sysDept);
}
