package com.mh.peer.dao.product.Impl;

import com.mh.peer.dao.product.ProductSerialNumberDao;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.util.Date;

/**
 * Created by zhangerxin on 2016-7-31.
 */
@Repository
public class ProductSerialNumberDaoImpl implements ProductSerialNumberDao{

    /**
     * 获取下一个项目流水号
     * @return
     */
    @Override
    public String getNextNumber() {
        DConnection conn = DaoTool.getConnection();
        String sql = "SELECT SERIALNUMBER FROM product_serialNumber WHERE ID = '06415eda56a811e6987c00163e00076c'";
        String sqlUpdate = "UPDATE product_serialNumber SET SERIALNUMBER = <serialNumber> WHERE ID = '06415eda56a811e6987c00163e00076c'";
        String serialNumber = ""; //项目流水号(String型)
        int serialNumberIn = 0; //项目流水号(Int型)
        DMap dMap = DaoTool.select(sql);
        if (dMap != null){
            serialNumber = dMap.getValue("SERIALNUMBER",0); //项目流水号
            if (serialNumber != null && !serialNumber.equals("")){
                serialNumberIn = TypeTool.getInt(serialNumber);
                serialNumberIn = serialNumberIn + 1;
                serialNumber = TypeTool.getString(serialNumberIn);
                DMap dMapUpdate = new DMap();
                dMapUpdate.setData("serialNumber",serialNumberIn);
                DaoResult daoResult = DaoTool.update(sqlUpdate,dMapUpdate.getData(),conn);
                if (daoResult.getCode() < 0){
                    conn.rollback();
                }else{
                    conn.commit();
                    conn.close();
                }
            }
        }
        return serialNumber;
    }
}
