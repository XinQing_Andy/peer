package com.mh.peer.dao.product;

/**
 * Created by zhangerxin on 2016-7-31.
 */
public interface ProductSerialNumberDao {

    /**
     * 获取下一个项目流水号
     * @return
     */
    String getNextNumber();
}
