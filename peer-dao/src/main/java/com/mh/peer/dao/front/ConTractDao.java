package com.mh.peer.dao.front;

import com.salon.frame.data.DMap;

/**
 * Created by zhangerxin on 2016-8-31.
 * 合同部分
 */
public interface ConTractDao {

    /**
     * 获取产品信息
     * @param productId
     * @return
     */
    DMap getProductInfo(String productId);

    /**
     * 获取产品投资信息
     * @param productId
     * @return
     */
    DMap getProductBuyInfoList(String productId);

    /**
     * 获取债权信息
     * @param attornId
     * @return
     */
    DMap getAttornInfo(String attornId);
}
