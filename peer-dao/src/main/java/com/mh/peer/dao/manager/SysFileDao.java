package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.SysFileMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by zhangerxin on 2016-4-10.
 */
public interface SysFileDao {

    /**
     * 添加附件
     * @param sysFile
     * @return
     */
    DaoResult saveProduct(SysFile sysFile);

    /**
     * 删除附件
     * @param sysFile
     * @return
     */
    DaoResult delProduct(SysFile sysFile);

    /**
     * 根据glid获取一条附件信息
     * @param glId
     * @return
     */
    DMap querySysFileById(String glId);

    /**
     * 根据关联id查询附件
     * @param glId
     * @return
     */
    DMap queryAllSysFile(String glId);

    /**
     * 查询一条附件信息
     * @param sysFileMes
     * @return
     */
    DMap getOnlySysFile(SysFileMessageBean sysFileMes);

    /**
     * 根据关联id获取附件地址(App部分)
     * @param glId
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DMap getAppSysFile(String glId,String orderColumn,String orderType,String pageIndex,String pageSize);
}
