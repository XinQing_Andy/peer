package com.mh.peer.dao.member;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by cuibin on 2016/4/20.
 */
public interface AllMemberDao {

    /**
     * 根据分页查询会员信息
     * @param memberInfoMes
     * @return
     */
    DMap queryMemberBySome(MemberInfoMessageBean memberInfoMes);

    /**
     * 获取会员数量
     */
    DMap queryCountMember();

    /**
     * 所有会员 查询所有
     */
    DMap queryAllMember(MemberInfoMessageBean memberInfoMessageBean);
    /**
     * 所有会员 查询所有数量
     */
    DMap queryAllMemberCount(MemberInfoMessageBean memberInfoMessageBean);
    /**
     * 查询一个 会员
     */
    DMap queryOnlyMember(String id);

    /**
     * 根据id获取会员信息
     * @param id
     * @return
     */
    DMap queryMemberInfoById(String id);

    /**
     * 锁定 会员
     */
    DaoResult lockMember(MemberInfo memberInfo);
    /**
     * 更新密码
     */
    DaoResult updateMemberPassWord(MemberInfo memberInfo);
    /**
     * 更新信息
     */
    DaoResult updateMemberInfo(MemberInfo memberInfo);

    /**
     * 查询所有理财集合
     */
    public DMap queryAllManageMoney(FinancingMessageBean financingMes);

    /**
     * 查询所有理财数量
     * @param financingMes
     * @return
     */
    public DMap queryAllManageMoneyCount(FinancingMessageBean financingMes);

    /**
     * 后台首页查询新增会员 昨天
     */
    public DMap queryYesterdayReg();
    /**
     * 后台首页查询新增会员 过去7天
     */
    public DMap queryLastWeekReg();
    /**
     * 后台首页查询新增会员 过去30天
     */
    public DMap queryLastMonthReg();

    /**
     * 我的账户部分
     * @param myAccountMes
     * @return
     */
    public DMap getMemberInfo(MyAccountMessageBean myAccountMes);
    /**
     * 按日期查询注册会员数量
     */
    public DMap queryRegMemnerByDate(String startTime, String endTime);
    /**
     * 按指定日期查询注册会员数量
     */
    public DMap queryRegMemnerByDate(String time);

    /**
     * 更新手机号
     */
    public DaoResult updateTelephone(MemberInfo memberInfo);

    /**
     * 获取会员信息(App端)
     * @param memberId
     * @return
     */
    public DMap getAppMemberInfo(String memberId);

    /**
     * 资产统计部分(App端)
     * @param memberId
     * @return
     */
    DMap getPriceTj(String memberId);

    /**
     * 获取投资实力榜(App端)
     * @param pageIndex
     * @param pageSize
     * @param orderColumn
     * @param orderType
     * @return
     */
    DMap getMemberByInvest(String pageIndex,String pageSize,String orderColumn,String orderType);

    /**
     * 获取个人资料信息(App端)
     * @param memberId
     * @return
     */
    DMap getAppPersonInfo(String memberId);

    /**
     * 更新余额
     */
    public DaoResult updateBalance(MemberInfo memberInfo);

    /**
     * 查询一个 会员余额
     */
    public DMap queryOnlyMemberBalance(String memberId);



}
