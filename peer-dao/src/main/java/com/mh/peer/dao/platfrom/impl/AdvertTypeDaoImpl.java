package com.mh.peer.dao.platfrom.impl;

import com.mh.peer.dao.platfrom.AdvertTypeDao;
import com.mh.peer.model.entity.AdvertType;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangerxin on 2016-4-12.
 */
@Repository
public class AdvertTypeDaoImpl implements AdvertTypeDao {

    @Autowired
    private BaseDao baseDao;


    /**
     * 查询所有广告类别
     * @return
     */
    @Override
    public DMap getAllAdvertType() {
        String sql = "SELECT ID,NAME,FID,CREATEUSER,CREATETIME FROM advert_type WHERE 1 = 1 and FID IS NULL";
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
}
