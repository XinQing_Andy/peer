package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.ProductAttornDetailsDao;
import com.mh.peer.model.message.ProductAttornDetailsMes;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by cuibin on 2016/6/29.
 */
@Repository
public class ProductAttornDetailsDaoImpl implements ProductAttornDetailsDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 查询 成功的债权转让标 详情
     */
    @Override
    public DMap queryProductAttornDetails(ProductAttornDetailsMes productAttornDetailsMessageBean) {
        String id = productAttornDetailsMessageBean.getId(); //产品购买id
        String mysql = "select * from view_product_attorn where 1 = 1";
        if (null != id && !"".equals(id)) {
            mysql = mysql + " and ID = '" + id + "'";
        }
        return DaoTool.select(mysql);
    }

    /**
     * 查询竞拍记录
     */
    @Override
    public DMap queryAuctionHistory(String productId) {
        String mysql = "select * from view_product_buy where 1 = 1";
        if (null != productId && !"".equals(productId)) {
            mysql = mysql + " and productId = '" + productId + "'";
        }
        return DaoTool.select(mysql);
    }
}
