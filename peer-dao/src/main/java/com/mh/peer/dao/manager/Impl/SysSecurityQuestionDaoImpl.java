package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.SysSecurityQuestionDao;
import com.mh.peer.model.entity.SysSecurityQuestionParameter;
import com.mh.peer.model.message.SysSecurityQuestionMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-4-1.
 */
@Repository
public class SysSecurityQuestionDaoImpl implements SysSecurityQuestionDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 获取安全问题(分页查询)
     * @param sysSecurityQuestionMes
     * @return
     */
    @Override
    public DMap querySecurityQuestionByPage(SysSecurityQuestionMessageBean sysSecurityQuestionMes) {
        String pageIndex = sysSecurityQuestionMes.getPageIndex(); //当前页
        String pageSize = sysSecurityQuestionMes.getPageSize(); //每页显示数量
        String question = sysSecurityQuestionMes.getQuestion(); //问题
        String sql = "SELECT id,question,format,active_flg FROM view_Sys_Security_Question WHERE 1 = 1";
        if(question != null && !question.equals("")){ //问题
            sql = sql + " and question LIKE '%"+question+"%'";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize !=  null && !pageSize.equals("")){
            sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex)-1) +","+TypeTool.getInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 新增安全问题
     * @param sysSecurityQuestion
     * @return
     */
    @Override
    public DaoResult addSysSecurityQuestion(SysSecurityQuestionParameter sysSecurityQuestion) {
        DaoResult result = new DaoResult();
        if (sysSecurityQuestion == null || sysSecurityQuestion == null) { //安全问题类
            result.setCode(-1);
            result.setErrText("添加失败！");
            return result;
        }
        DConnection conn = DaoTool.getConnection();
        result = baseDao.insert(sysSecurityQuestion, conn); //新增安全问题
        if (result.getCode() < 0) {
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    /**
     * 修改安全问题
     * @param sysSecurityQuestion
     * @return
     */
    @Override
    public DaoResult updateSysSecurityQuestion(SysSecurityQuestionParameter sysSecurityQuestion) {
        DaoResult result = new DaoResult();
        if (sysSecurityQuestion == null || sysSecurityQuestion == null) { //安全问题类
            result.setCode(-1);
            result.setErrText("修改失败！");
            return result;
        }
        DConnection conn = DaoTool.getConnection();
        result = baseDao.update(sysSecurityQuestion,conn); //修改安全问题
        if (result.getCode() < 0) {
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    /**
     * 获取安全问题数量
     * @param sysSecurityQuestionMes
     * @return
     */
    @Override
    public DMap getCountSecurityQuestion(SysSecurityQuestionMessageBean sysSecurityQuestionMes) {
        String question = sysSecurityQuestionMes.getQuestion(); //问题
        String sql = "SELECT * FROM view_Sys_Security_Question WHERE 1 = 1";
        if(question != null && !question.equals("")){ //问题
            sql = sql + " and question LIKE '%"+question+"%'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
}
