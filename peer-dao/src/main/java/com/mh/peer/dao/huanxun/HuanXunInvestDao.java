package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-28.
 */
public interface HuanXunInvestDao {

    /**
     * 产品投资
     * @param huanxunFreeze
     * @param productBuyInfo
     * @param tradeDetail
     * @param memberMessage
     * @return
     */
    public DaoResult investhxResult(Map<String,Object> mapException,HuanxunFreeze huanxunFreeze, ProductBuyInfo productBuyInfo, TradeDetail tradeDetail, MemberMessage memberMessage);

    /**
     * 获取环迅投资列表信息
     * @param huanXunFreezeMessageBean
     * @return
     */
    public DMap getHuanXunInvestBySome(HuanXunFreezeMessageBean huanXunFreezeMessageBean);

    /**
     * 解冻部分(投资)
     * @param memberInfo
     * @param huanxunFreeze
     * @return
     */
    public DaoResult unfreezeResult(Map<String,Object> mapException,MemberInfo memberInfo, HuanxunFreeze huanxunFreeze);

}
