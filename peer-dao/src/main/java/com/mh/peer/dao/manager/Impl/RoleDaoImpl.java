package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.RoleDao;
import com.mh.peer.model.entity.SysRole;
import com.mh.peer.model.entity.SysRoleMenu;
import com.mh.peer.model.entity.SysRoleUser;
import com.mh.peer.model.message.SysRoleMenuMessBean;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by zhangerxin on 2016/1/14.
 */
@Repository
public class RoleDaoImpl implements RoleDao {
    @Autowired
    private BaseDao baseDao;

    /**
     * 根据用户获取角色
     * @param sysRoleUser
     * @return
     */
    @Override
    public List<SysRoleUser> getRoleUser(SysRoleUser sysRoleUser) {
        List<SysRoleUser> roleUserList = baseDao.getListBySome(sysRoleUser.getClass(), "USER_ID", sysRoleUser.getUserId());
        if(roleUserList.size() == 0){
            return roleUserList;
        }
        return roleUserList;
    }

    @Override
    public List<SysRole> getRoleInfo(SysRole role) {
        List roleList = baseDao.getListBySome(SysRole.class, "IS_DELETE", "0");
        if (roleList.size() == 0) {
            return roleList;
        }
        return roleList;
    }

    /**
     * 查询用户组(带分页)(zhangerxin)
     * @param sysRolMese
     * @return
     */
    @Override
    public DMap getRoleInfoByPage(SysRoleMessageBean sysRolMese) {
        String pageIndex = sysRolMese.getPageIndex(); //当前页
        String pageSize = sysRolMese.getPageSize(); //每页显示数量
        String roleName = sysRolMese.getRoleName(); //角色名称
        String sql = "SELECT ID,ROLE_NAME,ROLE_DESCRIPTION,USERCOUNT FROM view_Sys_Role WHERE 1 = 1 AND IS_DELETE = '0'";
        if(roleName != null && !roleName.equals("")){
            sql = sql + " and ROLE_NAME LIKE '%"+roleName+"%'";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize !=  null && !pageSize.equals("")){
            sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex)-1) +","+TypeTool.getInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 根据id获取用户组信息(zhangerxin)
     * @param sysRolMese
     * @return
     */
    @Override
    public DMap queryRoleById(SysRoleMessageBean sysRolMese) {
        String id = sysRolMese.getId(); //主键
        DMap dMap = new DMap();
        DMap result = new DMap();
        dMap.setData("id",id); //主键
        result = baseDao.getViewData("sys_role","id",id);
        return result;
    }


    @Override
    public DMap getMenuInfo() {
        String sql = "SELECT '' AS flg, id, PARENT_ID, MENU_SEQ, LEVEL_MENU, "
                + "CASE WHEN LEVEL_MENU = '1' THEN MENU_NAME END AS topMenu, "
                + "case when LEVEL_MENU = '2' THEN MENU_NAME END as secondMenu, "
                + "case when LEVEL_MENU = '3' THEN MENU_NAME END as thirdMenu "
                + "FROM sys_menu where IS_DELETE = '0' ORDER BY MENU_SEQ";
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 新增用户角色
     * @param role
     * @return
     */
    @Override
    public DaoResult addRole(SysRole role) {
        DaoResult result = new DaoResult();
        if (role == null) {
            result.setErrText("添加失败");
            result.setCode(-1);
            return result;
        }
        result = baseDao.insert(role);
        return result;
    }

    @Override
    public DaoResult updateRole(SysRole role) {
        DaoResult result = new DaoResult();
        if (role == null) {
            result.setCode(-1);
            result.setErrText("修改失败");
            return result;
        }
        String a = role.getRoleDescription();
        result = baseDao.update(role);
        return result;
    }

    @Override
    public DaoResult delRole(SysRole role) {
        DaoResult result = new DaoResult();
        if (role == null) {
            result.setCode(-1);
            result.setErrText("删除失败");
            return result;
        }
        result = baseDao.delete(role);
        return result;
    }

    @Override
    public DaoResult delRoleMenu(SysRoleMenu sysRoleMenu) {
        String sql = "DELETE FROM sys_role_menu WHERE ROLE_ID = <ROLE_ID>";
        DMap dmap = new DMap();
        dmap.setData("ROLE_ID", sysRoleMenu.getRoleId());
        DaoResult result = DaoTool.update(sql, dmap.getData());
        return result;
    }

    @Override
    public DaoResult addRoleMenu(SysRoleMenu sysRoleMenu) {
        DaoResult result = new DaoResult();
        DConnection conn = DaoTool.getConnection();
        if (sysRoleMenu == null) {
            result.setErrText("添加失败");
            result.setCode(-1);
            return result;
        }
        result = baseDao.insert(sysRoleMenu, conn);
        if (result.getCode() < 0){
            result.setErrText("添加失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    /**
     * 根据角色查询菜单信息
     * @return
     */
    @Override
    public DMap getRoleMenuInfo(SysRole role) {
        int id = role.getId();
        String sql = "SELECT CASE WHEN (select id from sys_role_menu where MENU_ID = a.id and ROLE_ID = '"+id+"') "
                + "IS NOT NULL THEN true else false END AS flg, "
                + "a.ID, a.PARENT_ID, a.MENU_SEQ, a.LEVEL_MENU, "
                + "CASE WHEN a.LEVEL_MENU = '1' THEN a.MENU_NAME END AS topMenu, "
                + "case when a.LEVEL_MENU = '2' THEN a.MENU_NAME END as secondMenu, "
                + "case when a.LEVEL_MENU = '3' THEN a.MENU_NAME END as thirdMenu "
                + "FROM sys_menu a  WHERE a.IS_DELETE = '0' ORDER BY a.MENU_SEQ";
        //System.out.println("根据角色查询菜单信息sql == " + sql);
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 获取角色数量
     * @param sysRoleMes
     * @return
     */
    @Override
    public DMap getCountSysRoleByPage(SysRoleMessageBean sysRoleMes) {
        String roleName = sysRoleMes.getRoleName(); //角色名称
        String sql = "select * from sys_role where 1 = 1"; //角色查询条件
        if(roleName != null && !roleName.equals("")){ //角色名称
            sql = sql + " AND ROLE_NAME LIKE '%"+roleName+"%'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 查询所有一级菜单
     */
    @Override
    public DMap queryAllMenu() {
        String sql = "SELECT ID,PARENT_ID,MENU_NAME,LEVEL_MENU FROM sys_menu"
                   + " WHERE IS_DELETE = '0' AND PARENT_ID IS NULL ORDER BY ID";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据父id查询所有菜单
     * @param fid
     * @return
     */
    @Override
    public DMap queryAllMenuByFid(String fid) {
        String sql = "SELECT ID,PARENT_ID,MENU_NAME,LEVEL_MENU FROM sys_menu"
                   + " WHERE IS_DELETE = '0' AND PARENT_ID = '"+fid+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据角色id查询都有哪些权限
     * @param roleId
     * @return
     */
    @Override
    public DMap getAllRoleMenu(String roleId) {
        String sql = "SELECT MENU_ID FROM sys_role_menu WHERE ROLE_ID = '"+roleId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 保存用户角色权限
     * @param sysRoleMenuMes
     * @return
     */
    @Override
    public SysRoleMenuMessBean saveRolePower(SysRoleMenuMessBean sysRoleMenuMes) {
        String sqlDelRoleMenuId = "DELETE FROM sys_role_menu WHERE ROLE_ID = <roleId>";
        DMap dmapDelRoleMenuId = new DMap();
        DaoResult resultDelRoleMenuId = new DaoResult(); //删除角色权限
        DaoResult resultSaveRoleMenuId = new DaoResult(); //保存用户权限
        DConnection conn = DaoTool.getConnection();
        String roleId = sysRoleMenuMes.getRoleId(); //角色id(String型)
        int roleIdin = 0; //角色id(int型)
        String menuIdStr = sysRoleMenuMes.getMenuId(); //角色权限id拼接
        String menuId = ""; //角色权限id(String型)
        int menuIdin = 0; //角色权限id(int型)
        String createTime = ""; //创建时间

        try{

            createTime = StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss"); //创建时间
            if (roleId != null && !roleId.equals("")){
                roleIdin = TypeTool.getInt(roleId);
            }
            dmapDelRoleMenuId.setData("roleId", roleIdin); //角色id
            resultDelRoleMenuId = DaoTool.update(sqlDelRoleMenuId, dmapDelRoleMenuId.getData(),conn);
            if (resultDelRoleMenuId.getCode() < 0){
                conn.rollback();
                sysRoleMenuMes.setResult("error");
                sysRoleMenuMes.setInfo("设置角色权限失败");
            }else{
                if (menuIdStr != null && !menuIdStr.equals("")){
                    String arr[] = menuIdStr.split(","); //数组
                    if (arr != null && arr.length > 0){
                        for (int i=0;i<arr.length;i++){
                            menuId = arr[i];
                            if (menuId != null && !menuId.equals("")){
                                menuIdin = TypeTool.getInt(menuId);
                            }
                            SysRoleMenu sysRoleMenu = new SysRoleMenu();
                            sysRoleMenu.setRoleId(roleIdin); //角色id
                            sysRoleMenu.setMenuId(menuIdin); //菜单id
                            sysRoleMenu.setCreateTime(createTime); //创建时间
                            resultSaveRoleMenuId = baseDao.insert(sysRoleMenu,conn);
                            if (resultSaveRoleMenuId.getCode() < 0){
                                conn.rollback();
                                sysRoleMenuMes.setResult("error");
                                sysRoleMenuMes.setInfo("设置角色权限失败");
                                break;
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }

        return sysRoleMenuMes;
    }
}
