package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.MyMessageDao;
import com.mh.peer.model.message.MemberMessageMesBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/7/21.
 */
@Repository
public class MyMessageDaoImpl implements MyMessageDao {

    /**
     * 分页查询我的消息
     */
    @Override
    public DMap queryMyMessageByPage(MemberMessageMesBean memberMessageMesBean) {
        String sql = "select * from member_Message where 1 = 1";
        String orderColumn = memberMessageMesBean.getOrderColumn(); //排序字段
        String orderType = memberMessageMesBean.getOrderType(); //排序类型
        String pageIndex = memberMessageMesBean.getPageIndex(); //当前页
        String pageSize = memberMessageMesBean.getPageSize(); //每页显示数量
        String startTime = memberMessageMesBean.getStartTime(); //开始时间
        String endTime = memberMessageMesBean.getEndTime(); //结束时间
        String readflag = memberMessageMesBean.getReadflag();//是否已读
        String memberId = memberMessageMesBean.getMemberid(); //会员id
        String delFlg = memberMessageMesBean.getDelFlg(); //删除标志(0-已删除、1-未删除)

        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " AND MEMBERID = '" + memberId + "'";
        }
        if (readflag != null && !readflag.equals("")) { //是否已读
            sql = sql + " AND READFLAG = '" + readflag + "'";
        }
        if (startTime != null && !startTime.equals("")) { //开始时间
            sql = sql + " AND SENDTIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) { //结束时间
            sql = sql + " AND SENDTIME <= '" + endTime + "'";
        }
        if (delFlg != null && !delFlg.equals("")){ //删除标志(0-已删除、1-未删除)
            sql = sql + " AND DELFLG = '"+delFlg+"'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex)-1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 查询我的消息总数
     */
    @Override
    public DMap queryMyMessageCount(MemberMessageMesBean memberMessageMesBean) {
        String sql = "select * from member_Message where 1 = 1";
        String orderColumn = memberMessageMesBean.getOrderColumn(); //排序字段
        String orderType = memberMessageMesBean.getOrderType(); //排序类型
        String pageIndex = memberMessageMesBean.getPageIndex(); //当前页
        String pageSize = memberMessageMesBean.getPageSize(); //每页显示数量
        String startTime = memberMessageMesBean.getStartTime(); //开始时间
        String endTime = memberMessageMesBean.getEndTime(); //结束时间
        String readflag = memberMessageMesBean.getReadflag();//是否已读
        String delFlg = memberMessageMesBean.getDelFlg(); //删除状态(0-已删除、1-未删除)
        String memberId = memberMessageMesBean.getMemberid(); //会员id

        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " and MEMBERID = '" + memberId + "'";
        }
        if (readflag != null && !readflag.equals("")) { //是否已读
            sql = sql + " and READFLAG = '" + readflag + "'";
        }
        if (delFlg != null && !delFlg.equals("")){
            sql = sql + " AND DELFLG = '"+delFlg+"'";
        }
        if (startTime != null && !startTime.equals("")) { //开始时间
            sql = sql + " and SENDTIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) { //结束时间
            sql = sql + " and SENDTIME <= '" + endTime + "'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取详细信息
     * @param memberMessageMes
     * @return
     */
    @Override
    public DMap getMyMessageById(MemberMessageMesBean memberMessageMes) {
        String sql = "SELECT * FROM member_Message WHERE 1 = 1";
        String id = memberMessageMes.getId(); //主键
        if (id != null && !id.equals("")){
            sql = sql + " AND ID = '"+id+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 更新只读状态
     * @param memberMessageMes
     * @return
     */
    @Override
    public DaoResult upReadFlg(MemberMessageMesBean memberMessageMes) {
        DConnection conn = DaoTool.getConnection();
        String id = memberMessageMes.getId(); //主键
        String sql = "UPDATE member_Message SET READFLAG = '0' WHERE ID = <id>";
        DMap dmap = new DMap();
        DaoResult result = new DaoResult();
        dmap.setData("id", id); //主键
        result = DaoTool.update(sql, dmap.getData(), conn);
        if(result.getCode() < 0){
            result.setErrText("设置失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    /**
     * 删除我的消息
     * @param memberMessageMes
     * @return
     */
    @Override
    public DaoResult delMyMessageById(MemberMessageMesBean memberMessageMes) {
        DConnection conn = DaoTool.getConnection();
        String id = memberMessageMes.getId(); //主键
        String sql = "UPDATE member_Message SET DELFLG = '0' WHERE ID = <id>";
        DMap dmap = new DMap();
        DaoResult result = new DaoResult();
        dmap.setData("id", id); //主键
        result = DaoTool.update(sql, dmap.getData(), conn);
        if(result.getCode() < 0){
            result.setErrText("删除失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }
}
