package com.mh.peer.dao.dataStatistic.impl;

import com.mh.peer.dao.dataStatistic.ProductSaleDao;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-9-18.
 * 借款标销售情况分析表
 */
@Repository
public class ProductSaleDaoImpl implements ProductSaleDao{

    /**
     * 根据产品id获取已放款产品数量
     * @param typeId
     * @return
     */
    @Override
    public DMap getGrantPriceCount(String typeId) {
        String sql = "SELECT COUNT(1)GRANTCOUNT FROM product_content WHERE TYPE = '"+typeId+"' AND SHFLAG = '0'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据产品类型获取已借款总额
     * @param typeId
     * @return
     */
    @Override
    public DMap getLoanPrice(String typeId) {
        String sql = "SELECT SUM(BIDFEE*SPLITCOUNT)LOANPRICE FROM product_content WHERE TYPE = '"+typeId+"' AND SHFLAG = '0'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据产品类型获取借款标数量
     * @param typeId
     * @return
     */
    @Override
    public DMap getLoanCount(String typeId) {
        String sql = "SELECT COUNT(1)LOANCOUNT FROM product_content WHERE TYPE = '"+typeId+"' AND SHFLAG = '0'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据产品类型获取投标会员数
     * @param typeId
     * @return
     */
    @Override
    public DMap getInvestMemberCount(String typeId) {
        String sql = "SELECT COUNT(1)INVESTMEMBERCOUNT FROM view_product_buy_min WHERE PRODUCTTYPE = '"+typeId+"' AND SHFLAG = '0'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
