package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.UserLoginDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.AppMemberMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by cuibin on 2016/4/22.
 */
@Repository
public class UserLoginDaoImpl implements UserLoginDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 用户登录
     */
    @Override
    public MemberInfo userLogin(MemberInfo memberInfo) {
        List userList = baseDao.getListBySome(memberInfo.getClass(), "USERNAME", memberInfo.getUsername());
        MemberInfo resultBean = null;
        int count = userList.size();
        for (int i = 0; i < count; i++) {
            Object re = userList.get(i);
            if (re != null) {
                if (re instanceof MemberInfo) {
                    resultBean = (MemberInfo) re;
                }
            }
        }
        return resultBean;
    }

    /**
     * 保存登陆时间
     */
    @Override
    public DaoResult saveLoginTime(MemberInfo memberInfo) {
        if (memberInfo == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        return baseDao.update(memberInfo);
    }

    /**
     * 登录操作(app端)
     * @param appMemberMes
     * @return
     */
    @Override
    public DMap login(AppMemberMessageBean appMemberMes) {
        String sql = "SELECT * FROM member_info WHERE 1 = 1"; //查询
        String userName = appMemberMes.getMemberUserName(); //登录人
        String password = appMemberMes.getPassword(); //密码
        DMap dMap = new DMap();

        if (userName != null && !userName.equals("") && password != null && !password.equals("")){
            sql = sql + " AND USERNAME = '"+userName+"' AND PASSWORD = '"+password+"'";
            dMap = DaoTool.select(sql);
        }
        return dMap;
    }

    /**
     * 用户注册
     * @param memberInfo
     * @return
     */
    @Override
    public DaoResult regist(MemberInfo memberInfo) {
        DConnection conn = DaoTool.getConnection();
        DaoResult daoResult = new DaoResult();
        String sql = "UPDATE member_info SET ";

        try{

            daoResult = baseDao.insert(memberInfo, conn);
            if(daoResult.getCode() < 0){
                conn.rollback();
            }
        }catch(Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }
        return null;
    }

    /**
     * 获取会员数量
     * @param appMemberMes
     * @return
     */
    @Override
    public DMap getMemberCount(AppMemberMessageBean appMemberMes) {
        String sql = "SELECT count(1)MEMBERCOUNT FROM member_info WHERE 1 = 1"; //会员查询条件
        String userName = appMemberMes.getMemberUserName(); //用户名
        String delFlg = appMemberMes.getDelFlg(); //是否删除(0-是、1-否)
        DMap dMap = new DMap();

        try{

            if (userName != null && !userName.equals("")){ //用户名
                sql = sql + " AND USERNAME = '"+userName+"'";
            }
            if (delFlg != null && !delFlg.equals("")){ //是否删除(0-是、1-否)
                sql = sql + " AND DEL_FLG = '"+delFlg+"'";
            }
            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dMap;
    }

    /**
     * 获取会员信息(App端)
     * @param appMemberMes
     * @return
     */
    @Override
    public DMap getMemberInfo(AppMemberMessageBean appMemberMes) {
        String userName = appMemberMes.getMemberUserName(); //用户名
        String sql = "SELECT ID FROM member_info WHERE 1 = 1";
        if (userName != null && !userName.equals("")){ //用户名
            sql = sql + " AND USERNAME = '"+userName+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 修改密码
     * @param memberId
     * @param password
     * @return
     */
    @Override
    public DaoResult updatePassword(String memberId,String password) {
        DConnection conn = DaoTool.getConnection();
        DaoResult daoResult = new DaoResult();
        String sql = "UPDATE member_info SET PASSWORD = <PASSWORD> WHERE ID = <ID>";

        try{

            DMap dmap = new DMap();
            dmap.setData("ID", memberId); //主键
            dmap.setData("PASSWORD", password); //密码
            daoResult = DaoTool.update(sql, dmap.getData(), conn);
            System.out.println(daoResult.getCode());
            if(daoResult.getCode() < 0){
                conn.rollback();
            }
        }catch(Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }
        return daoResult;
    }
}
