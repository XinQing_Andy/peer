package com.mh.peer.dao.front;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.SysFile;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/5/1.
 */
public interface PersonageSettingDao {
    /**
     * 查询一个用户基本资料
     */
    DMap queryOnlyMemberInfo(String id);
    /**
     * 查询所有省份
     */
    DMap queryAllProvince();
    /**
     * 查询城市
     */
    DMap queryCity(String provinceid);
    /**
     * 更新个人基本资料
     */
    DaoResult updatePersona(MemberInfo memberInfo);
    /**
     * 更新头像  保存图片信息
     */
    DaoResult updatePortrait(SysFile sysFile);
    /**
     * 删除头像信息
     */
    DaoResult deletePortrait(SysFile sysFile);
    /**
     * 查询头像信息
     */
    DMap queryOnlyPortrait(String userId);
}
