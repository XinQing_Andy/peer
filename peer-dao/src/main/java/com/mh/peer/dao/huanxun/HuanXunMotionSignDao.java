package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.HuanxunAutosign;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/8/4.
 */
public interface HuanXunMotionSignDao {

    /**
     * 查询环迅注册记录
     * @param memberId
     * @return
     */
    public DMap queryHuanXunReg(String memberId);

    /**
     * 保存自动签约返回信息
     */
    public DaoResult saveMotionSignResult(HuanxunAutosign huanxunAutosign);

    /**
     * 查询自动签约返回信息是否存在
     */
    public DMap queryIsResult(String memberId,String signedType);
}
