package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunAttornDao;
import com.mh.peer.exception.HuanXunFreezeAttornException;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-8-2.
 * 环迅债权转让资金冻结
 */
@Repository
public class HuanXunAttornDaoImpl implements HuanXunAttornDao{

    private static final Logger LOGGER = LoggerFactory.getLogger(HuanXunAttornDaoImpl.class);
    @Autowired
    private BaseDao baseDao;

    /**
     * 环迅债权转让资金冻结
     * @param huanxunFreezeAttorn
     * @param tradeDetail
     * @param memberMessage
     * @return
     */
    @Override
    public DaoResult attornhxResult(Map<String,Object> mapException,HuanxunFreezeAttorn huanxunFreezeAttorn, TradeDetail tradeDetail, MemberMessage memberMessage) {
        DConnection conn = DaoTool.getConnection();
        String attornId = huanxunFreezeAttorn.getAttornid(); //债权转让id
        String underMember = huanxunFreezeAttorn.getMemberid(); //承接人id
        Double balanceDou = 0.0d; //余额
        DaoResult daoResultHuanxunAttorn = new DaoResult(); //债权转让环迅部分
        DaoResult daoResultAttorn = new DaoResult(); //债权转让部分
        DaoResult daoResultTradeDetail = new DaoResult(); //交易记录
        DaoResult daoResultMemberMessage = new DaoResult(); //我的消息
        DaoResult daoResultMember = new DaoResult(); //会员

        try{

            daoResultHuanxunAttorn = baseDao.insert(huanxunFreezeAttorn,conn); //环迅债权转让资金冻结
            if (daoResultHuanxunAttorn.getCode() < 0){
                HuanXunFreezeAttornException.handle(mapException);
                conn.rollback();
            }else{
                String sqlAttornUpdate = "UPDATE product_attorn_record SET UNDERTAKEMEMBER = <underTakeMember>,"
                                       + "UNDERTAKETIME = NOW() WHERE ID = <id>";
                DMap dMapAttorn = new DMap();
                dMapAttorn.setData("underTakeMember",underMember); //承接人
                dMapAttorn.setData("id",attornId); //债权id
                daoResultAttorn = DaoTool.update(sqlAttornUpdate,dMapAttorn.getData(),conn);
                if (daoResultAttorn.getCode() < 0){
                    HuanXunFreezeAttornException.handle(mapException);
                    conn.rollback();
                }else{
                    daoResultTradeDetail = baseDao.insert(tradeDetail,conn); //交易记录
                    if (daoResultTradeDetail.getCode() < 0){
                        HuanXunFreezeAttornException.handle(mapException);
                        conn.rollback();
                    }else{
                        daoResultMemberMessage = baseDao.insert(memberMessage,conn); //我的消息录入成功
                        if (daoResultMemberMessage.getCode() < 0){
                            HuanXunFreezeAttornException.handle(mapException);
                            conn.rollback();
                        }else{
                            balanceDou = tradeDetail.getBalance(); //余额
                            String sqlMember = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <id>";
                            DMap dMapMember = new DMap();
                            dMapMember.setData("balance",balanceDou); //余额
                            dMapMember.setData("id",underMember); //会员id
                            daoResultMember = DaoTool.update(sqlMember,dMapMember.getData(),conn);
                            if (daoResultMember.getCode() < 0){
                                HuanXunFreezeAttornException.handle(mapException);
                                conn.rollback();
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            conn.rollback();
        }finally {
            conn.commit();
            conn.close();
        }
        return daoResultMember;
    }


    /**
     * 获取债权冻结信息
     * @param huanXunFreezeMes
     * @return
     */
    @Override
    public DMap getFreezeAttornInfo(HuanXunFreezeMessageBean huanXunFreezeMes) {
        String sql = "SELECT * FROM view_huanxun_freeze_attorn WHERE 1 = 1";
        String attornId = huanXunFreezeMes.getAttornId(); //债权id
        if (attornId != null && !attornId.equals("")){
            sql = sql + " AND ATTORNID = '"+attornId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 债权解冻
     * @param memberInfo
     * @param huanxunFreeze
     */
    @Override
    public void unfreezeAttornResult(Map<String,Object> mapException,MemberInfo memberInfo,HuanxunFreeze huanxunFreeze,
                                     ProductAttornRecord productAttornRecord,ProductBuyInfo productBuyInfo) {
        DConnection conn = DaoTool.getConnection();
        String sqlMemberInfo = ""; //会员信息
        String sqlHuanXunFreeze = ""; //冻结表
        String sqlHuanXunUnFreezeBf = ""; //解冻备份表
        String sqlProductAttorn = ""; //产品债权部分
        String sqlProductBuy = ""; //产品购买部分
        String memberId = memberInfo.getId(); //会员id
        Double balance = memberInfo.getBalance(); //余额
        String id = huanxunFreeze.getId(); //冻结表主键
        String attornId = productAttornRecord.getId(); //债权转让id
        String attornShFlag = productAttornRecord.getShflag(); //债权审核状态(0-审核通过、1-审核不通过、2-待审核)
        String buyId = productBuyInfo.getId(); //产品购买id
        String buyType = productBuyInfo.getType(); //购买产品类型(0-持有、1-转让中、2-已转让)
        String flag = huanxunFreeze.getFlag(); //状态(0-冻结、1-解冻、2-转账)
        String merBillNo = huanxunFreeze.getMerbillno(); //商户订单号
        String flagUnFreeze = "0"; //解冻备份表状态
        DaoResult daoResultMemberInfo = new DaoResult(); //会员信息
        DaoResult daoResultHuanXunFreeze = new DaoResult(); //冻结信息
        DaoResult daoResultHuanXunUnFreeze = new DaoResult(); //解冻备份信息
        DaoResult daoResultProductAttorn = new DaoResult(); //产品债权信息
        DaoResult daoResultProductBuy = new DaoResult(); //产品购买信息

        try{

            sqlHuanXunFreeze = "UPDATE huanxun_freeze_attorn SET FLAG = <flag> WHERE ID = <id>";
            DMap dMapHuanXunFreeze = new DMap();
            dMapHuanXunFreeze.setData("flag",flag); //冻结状态
            dMapHuanXunFreeze.setData("id",id); //债权冻结表主键
            daoResultHuanXunFreeze = DaoTool.update(sqlHuanXunFreeze,dMapHuanXunFreeze.getData(),conn);
            if (daoResultHuanXunFreeze.getCode() < 0){
                System.out.println("债权冻结失败");
                conn.rollback();
            }else{
                System.out.println("债权冻结成功");
                sqlHuanXunUnFreezeBf = "UPDATE huanxun_unFreeze_bf SET FLAG = <flagUnFreeze> WHERE MERBILLNO = <merBillNo>";
                DMap dMapHuanXunUnFreeze = new DMap();
                dMapHuanXunUnFreeze.setData("flagUnFreeze",flagUnFreeze); //状态(0-解冻成功、1-解冻失败、2-待解冻)
                dMapHuanXunUnFreeze.setData("merBillNo",merBillNo); //商户订单号
                daoResultHuanXunUnFreeze = DaoTool.update(sqlHuanXunUnFreezeBf,dMapHuanXunUnFreeze.getData(),conn);
                if (daoResultHuanXunUnFreeze.getCode() < 0){
                    System.out.println("更新解冻备份状态失败！");
                    conn.rollback();
                }else{
                    System.out.println("更新解冻备份状态成功！");
                    System.out.println("balance======"+balance+",memberId======"+memberId);
                    sqlMemberInfo = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
                    DMap dMapMemberInfo = new DMap();
                    dMapMemberInfo.setData("balance",balance); //余额
                    dMapMemberInfo.setData("memberId",memberId); //主键
                    daoResultMemberInfo = DaoTool.update(sqlMemberInfo,dMapMemberInfo.getData(),conn);
                    if (daoResultMemberInfo.getCode() < 0){
                        System.out.println("更新会员余额失败");
                        conn.rollback();
                    }else{
                        System.out.println("更新会员余额成功");
                        sqlProductAttorn = "UPDATE product_attorn_record SET SHFLAG = <attornShFlag> WHERE ID = <attornId>";
                        DMap dMapProductAttorn = new DMap();
                        dMapProductAttorn.setData("attornShFlag",attornShFlag); //债权审核状态(0-审核通过、1-审核不通过、2-待审核)
                        dMapProductAttorn.setData("attornId",attornId); //债权id
                        daoResultProductAttorn = DaoTool.update(sqlProductAttorn,dMapProductAttorn.getData(),conn);
                        if (daoResultProductAttorn.getCode() < 0){
                            System.out.println("更新债权审核状态失败");
                            conn.rollback();
                        }else{
                            System.out.println("更新债权审核状态成功");
                            sqlProductBuy = "UPDATE product_buy_info SET TYPE = <buyType> WHERE ID = <buyId>";
                            DMap dMapProductBuy = new DMap();
                            dMapProductBuy.setData("buyType",buyType); //购买产品类型(0-持有、1-转让中、2-已转让)
                            dMapProductBuy.setData("buyId",buyId); //产品购买id
                            daoResultProductBuy = DaoTool.update(sqlProductBuy,dMapProductBuy.getData(),conn);
                            if (daoResultProductBuy.getCode() < 0){
                                System.out.println("更新产品购买状态失败");
                                conn.rollback();
                            }else{
                                System.out.println("更新产品购买状态成功");
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }
    }
}
