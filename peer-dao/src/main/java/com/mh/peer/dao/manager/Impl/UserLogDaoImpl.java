package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.UserLogDao;
import com.mh.peer.model.entity.SysLoginLog;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by zhaojiaming on 2016/1/14.
 */
@Repository
public class UserLogDaoImpl implements UserLogDao{
    private static final Logger LOGGER = LoggerFactory.getLogger(UserLogDao.class);
    @Autowired
    private BaseDao baseDao;

    @Override
    public DaoResult loginLog(SysLoginLog loginLog) {
        DaoResult result = new DaoResult();
        if(loginLog==null){
            result.setCode(-1);
            result.setErrText("无数据！");
            return result;
        }
        SysLoginLog log = (SysLoginLog)baseDao.getById(loginLog);
        if(log!=null){
            result.setCode(1);
            result.setErrText("数据存在:"+log.getId()+"创建时间:"+log.getCreateTime()+"状态:"+log.getLogintype());
            return result;
        }
        return baseDao.insert(loginLog);
    }

    @Override
    public DaoResult loginOutLog(SysLoginLog loginLog) {
        DaoResult result = new DaoResult();
        if(loginLog==null){
            result.setCode(-1);
            result.setErrText("无数据！");
            return result;
        }
        return baseDao.update(loginLog);
    }
}
