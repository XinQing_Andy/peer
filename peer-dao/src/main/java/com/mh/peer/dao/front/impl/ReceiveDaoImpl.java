package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.ReceiveDao;
import com.mh.peer.model.message.ProductReceiveInfoMessageBean;
import com.mh.peer.model.message.ProductReceiveMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-8-31.
 */
@Repository
public class ReceiveDaoImpl implements ReceiveDao{

    /**
     * 根据投资id获取回款信息集合
     * @param buyId
     * @return
     */
    @Override
    public DMap getReceiveInfoList(String buyId,String orderColumn,String orderType,String pageIndex,String pageSize) {
        String sql = "SELECT * FROM product_receive WHERE 1 = 1 AND BUYID = '"+buyId+"'";
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex)-1) +","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据投资id获取回款信息数量
     * @param buyId
     * @return
     */
    @Override
    public DMap getReceiveInfoCount(String buyId) {
        String sql = "SELECT * FROM product_receive WHERE 1 = 1 AND BUYID = '"+buyId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据条件获取待回款(已回收)明细
     * @param productReceiveMes
     * @return
     */
    @Override
    public DMap getProductReceiveList(ProductReceiveMessageBean productReceiveMes) {
        String sql = "SELECT * FROM view_product_receive WHERE 1 = 1";
        String buyer = productReceiveMes.getBuyer(); //投资人id
        String startTime = productReceiveMes.getStartTime(); //起始时间
        String endTime = productReceiveMes.getEndTime(); //截止时间
        String title = productReceiveMes.getTitle(); //名称
        String isRecycle = productReceiveMes.getIsRecycle(); //状态(0-待回收、1-已回收)
        String pageIndex = productReceiveMes.getPageIndex(); //起始数
        String pageSize = productReceiveMes.getPageSize(); //每页显示数量
        String orderColumn = productReceiveMes.getOrderColumn(); //排序字段
        String orderType = productReceiveMes.getOrderType(); //排序类型

        if (buyer != null && !buyer.equals("")){ //投资人id
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        if (title != null && !title.equals("")){ //编号
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if (isRecycle != null && !isRecycle.equals("")){
            if (isRecycle.equals("0")){ //待回收
                if (startTime != null && !startTime.equals("")){ //开始时间
                    sql = sql + " AND DATE(YRECEIVETIME) >= DATE('"+startTime+"')";
                }
                if (endTime != null && !endTime.equals("")){ //截止时间
                    sql = sql + " AND DATE(YRECEIVETIME) <= DATE('"+endTime+"')";
                }
                sql = sql + " AND SRECEIVETIME IS NULL";
            }else if (isRecycle.equals("1")){ //已回收
                if (startTime != null && !startTime.equals("")){ //开始时间
                    sql = sql + " AND DATE(SRECEIVETIME) >= DATE('"+startTime+"')";
                }
                if (endTime != null && !endTime.equals("")){ //截止时间
                    sql = sql + " AND DATE(SRECEIVETIME) <= DATE('"+endTime+"')";
                }
                sql = sql + " AND SRECEIVETIME IS NOT NULL";
            }
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 根据条件获取待还款信息数量
     * @param productReceiveMes
     * @return
     */
    @Override
    public DMap getProductReceiveListCount(ProductReceiveMessageBean productReceiveMes) {
        String sql = "SELECT * FROM view_product_receive WHERE 1 = 1";
        String buyer = productReceiveMes.getBuyer(); //投资人id
        String startTime = productReceiveMes.getStartTime(); //起始时间
        String endTime = productReceiveMes.getEndTime(); //截止时间
        String title = productReceiveMes.getTitle(); //编号
        String isRecycle = productReceiveMes.getIsRecycle(); //状态(0-待回收、1-已回收)

        if (buyer != null && !buyer.equals("")){ //投资人id
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        if (title != null && !title.equals("")){ //名称
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if (isRecycle != null && !isRecycle.equals("")){
            if (isRecycle.equals("0")){ //待回收
                if (startTime != null && !startTime.equals("")){ //开始时间
                    sql = sql + " AND DATE(YRECEIVETIME) >= DATE('"+startTime+"')";
                }
                if (endTime != null && !endTime.equals("")){ //截止时间
                    sql = sql + " AND DATE(YRECEIVETIME) <= DATE('"+endTime+"')";
                }
                sql = sql + " AND SRECEIVETIME IS NULL";
            }else if (isRecycle.equals("1")){ //已回收
                if (startTime != null && !startTime.equals("")){ //开始时间
                    sql = sql + " AND DATE(SRECEIVETIME) >= DATE('"+startTime+"')";
                }
                if (endTime != null && !endTime.equals("")){ //截止时间
                    sql = sql + " AND DATE(SRECEIVETIME) <= DATE('"+endTime+"')";
                }
                sql = sql + " AND SRECEIVETIME IS NOT NULL";
            }
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


}
