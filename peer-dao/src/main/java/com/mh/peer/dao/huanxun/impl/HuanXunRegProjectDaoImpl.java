package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunRegProjectDao;
import com.mh.peer.exception.HuanXunRegProjectException;
import com.mh.peer.model.entity.HuanxunRegproject;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.ProductHuanXunMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-27.
 */
@Repository
public class HuanXunRegProjectDaoImpl implements HuanXunRegProjectDao{

    @Autowired
    private BaseDao baseDao;

    /**
     * 项目登记部分(环迅)
     * @param mapException
     * @param huanxunRegProject
     * @param memberMessage
     * @param tradeDetail
     * @return
     */
    @Override
    public DaoResult regProjecthxResult(Map<String,Object> mapException,HuanxunRegproject huanxunRegProject, MemberMessage memberMessage, TradeDetail tradeDetail) {
        DConnection conn = DaoTool.getConnection();
        int serial_Number = 0; //流水号
        String glId = huanxunRegProject.getGlid(); //关联id
        String sqlProjecthx = "SELECT * FROM huanxun_regProject WHERE GLID = '"+glId+"'"; //查询是否有重复的登记项目
        DMap dMapProjecthx = new DMap(); //项目登记查询
        DaoResult daoResultHuanxunRegProject = new DaoResult(); //环迅项目注册
        DaoResult daoResultTradeDetail = new DaoResult(); //交易记录部分
        DaoResult daoResultSerial = new DaoResult(); //流水号
        DaoResult daoResultProduct = new DaoResult(); //项目状态更新

        try{

            dMapProjecthx = DaoTool.select(sqlProjecthx);
            if (dMapProjecthx.getCount() == 0){
                daoResultHuanxunRegProject = baseDao.insert(huanxunRegProject,conn);
                if (daoResultHuanxunRegProject.getCode() < 0){
                    HuanXunRegProjectException.handle(mapException); //输出异常文件
                    conn.rollback();
                }else{
                    daoResultTradeDetail = baseDao.insert(tradeDetail,conn);
                    if (daoResultTradeDetail.getCode() < 0){
                        HuanXunRegProjectException.handle(mapException); //输出异常文件
                        conn.rollback();
                    }else{
                        /********** 更新流水号Start **********/
                        serial_Number = tradeDetail.getSerialnum(); //流水号
                        String sqlSerialNumber = "UPDATE serialNumber_wh SET NUMBER = <serial_Number> WHERE ID = '6d82cfe3347511e69b6d00163e0063ab'";
                        DMap dmapSerial = new DMap();
                        dmapSerial.setData("serial_Number",serial_Number);
                        daoResultSerial = DaoTool.update(sqlSerialNumber,dmapSerial.getData(),conn);
                        /*********** 更新流水号End ***********/

                        if(daoResultSerial.getCode() < 0) {
                            HuanXunRegProjectException.handle(mapException); //输出异常文件
                            conn.rollback();
                        }else{
                            String sqlProduct = "UPDATE product_content SET HUANXUNFLAG = '0' WHERE ID = <projectId>";
                            DMap dmapProduct = new DMap();
                            dmapProduct.setData("projectId",huanxunRegProject.getId()); //项目id
                            daoResultProduct = DaoTool.update(sqlProduct,dmapProduct.getData(),conn);
                            if (daoResultProduct.getCode() < 0){
                                HuanXunRegProjectException.handle(mapException); //输出异常文件
                                conn.rollback();
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            HuanXunRegProjectException.handle(mapException); //输出异常文件
            e.printStackTrace();
            conn.rollback();
        }finally {
            conn.commit();
            conn.close();
        }
        return daoResultProduct;
    }


    /**
     * 根据条件获取登记项目信息
     * @param productHuanXunMessageBean
     * @return
     */
    @Override
    public DMap getRegProjectInfoList(ProductHuanXunMessageBean productHuanXunMessageBean) {
        String sql = "SELECT * FROM view_huanxun_regProject WHERE 1 = 1";
        String id = productHuanXunMessageBean.getId(); //主键
        if (id != null && !id.equals("")){
            sql = sql + " AND ID = '"+id+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
