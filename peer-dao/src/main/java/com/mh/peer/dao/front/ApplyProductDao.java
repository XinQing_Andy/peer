package com.mh.peer.dao.front;

import com.mh.peer.model.message.ProductApplyMessageBean;
import com.salon.frame.data.DMap;

/**
 * Created by zhangerxin on 2016-9-7.
 * 申请项目
 */
public interface ApplyProductDao {

    /**
     * 查询申请项目
     * @param productApplyMes
     * @return
     */
    DMap getApplyProductList(ProductApplyMessageBean productApplyMes);

    /**
     * 已申请项目数量
     * @param productApplyMes
     * @return
     */
    DMap getApplyProductListCount(ProductApplyMessageBean productApplyMes);

    /**
     * 获取已还金额
     * @param productId
     * @param applyMemberId
     * @return
     */
    DMap getyRepay(String productId, String applyMemberId);

    /**
     * 获取待还金额
     * @param productId
     * @param applyMemberId
     * @return
     */
    DMap getdRepay(String productId, String applyMemberId);

    /**
     * 还款列表集合
     * @param productId
     * @return
     */
    DMap getRepayList(String productId,String orderColumn,String orderType,String pageIndex,String pageSize);

    /**
     * 还款列表数量
     * @param productId
     * @return
     */
    DMap getRepayListCount(String productId);
}
