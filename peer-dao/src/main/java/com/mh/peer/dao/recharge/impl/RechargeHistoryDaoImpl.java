package com.mh.peer.dao.recharge.impl;

import com.mh.peer.dao.recharge.RechargeHistoryDao;
import com.mh.peer.model.message.ViewHuanxunRechargeMes;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/7/27.
 */
@Repository
public class RechargeHistoryDaoImpl implements RechargeHistoryDao {
    /**
     * 查询充值统计
     */
    @Override
    public DMap queryRechargeHistory(ViewHuanxunRechargeMes viewHuanxunRechargeMes) {
        if (viewHuanxunRechargeMes == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String sql = "select * from view_huanxun_recharge where 1 = 1";

        //类别id
        String realname = viewHuanxunRechargeMes.getRealname(); //真实姓名
        String idcard = viewHuanxunRechargeMes.getIdcard(); //身份证号
        String orderColumn = viewHuanxunRechargeMes.getOrderColumn(); //排序字段
        String orderType = viewHuanxunRechargeMes.getOrderType(); //排序类型
        String startTime = viewHuanxunRechargeMes.getStartTime(); //开始时间
        String endTime = viewHuanxunRechargeMes.getEndTime(); //结束时间
        String pageIndex = viewHuanxunRechargeMes.getPageIndex(); //当前页
        String pageSize = viewHuanxunRechargeMes.getPageSize(); //每页显示数量

        if (startTime != null && !startTime.equals("")) {
            sql = sql + " and CREATETIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) {
            sql = sql + " and CREATETIME <= '" + endTime + "'";
        }
        if (realname != null && !realname.equals("")) {
            sql = sql + " and REALNAME LIKE '%" + realname + "%'";
        }
        if (idcard != null && !idcard.equals("")) {
            sql = sql + " and IDCARD LIKE '%" + idcard + "%'";
        }
        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " order by " + orderColumn + " " + orderType;
        }

        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + TypeTool.getInt(pageIndex) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
//        System.out.println("===================" + realname);
//        System.out.println("===================" + orderColumn);
//        System.out.println("===================" + orderType);
//        System.out.println("===================" + idcard);
        return dmap;
    }

    /**
     * 查询充值记录总数 视图
     */
    @Override
    public DMap queryRechargeHistoryCount(ViewHuanxunRechargeMes viewHuanxunRechargeMes) {
        if (viewHuanxunRechargeMes == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String sql = "select count(*) as count from view_huanxun_recharge where 1 = 1";

        String realname = viewHuanxunRechargeMes.getRealname(); //真实姓名
        String idcard = viewHuanxunRechargeMes.getIdcard(); //身份证号
        String startTime = viewHuanxunRechargeMes.getStartTime(); //开始时间
        String endTime = viewHuanxunRechargeMes.getEndTime(); //结束时间

        if (startTime != null && !startTime.equals("")) {
            sql = sql + " and CREATETIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) {
            sql = sql + " and CREATETIME <= '" + endTime + "'";
        }
        if (realname != null && !realname.equals("")) {
            sql = sql + " and REALNAME LIKE '%" + realname + "%'";
        }
        if (idcard != null && !idcard.equals("")) {
            sql = sql + " and IDCARD LIKE '%" + idcard + "%'";
        }

        DMap dmap = DaoTool.select(sql);
        //System.out.println("===================" + sql);
        return dmap;
    }
}
