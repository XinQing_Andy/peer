package com.mh.peer.dao.product.Impl;

import com.mh.peer.dao.product.ProductDao;
import com.mh.peer.model.entity.ProductContent;
import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * Created by zhangerxin on 2016-4-10.
 */
@Repository
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 获取产品(分页查询)
     *
     * @param productMes
     * @return
     */
    @Override
    public DMap queryProductByPage(ProductMessageBean productMes) {
        String pageIndex = productMes.getPageIndex(); //当前页
        String pageSize = productMes.getPageSize(); //每页显示数量
        String title = productMes.getTitle(); //名称
        String shFlag = productMes.getShflag(); //审核状态(0-审核通过、1-审核未通过、2-待审核)
        String productType = productMes.getProductType(); //产品类别
        String sql = "SELECT ID,TITLE,CREATETIME,LOANRATE,CREDITSTART,CREDITEND,BIDFEE,SPLITCOUNT,FLAG,SHFLAG,FULLSCALE," +
                "SHYJ,SORT,HUANXUNFLAG,NEWSHARE FROM product_content WHERE 1 = 1";
        if (title != null && !title.equals("")) { //题目
            sql = sql + " and TITLE LIKE '%" + title + "%'";
        }
        if (shFlag != null && !shFlag.equals("")){ //审核状态(0-审核通过、1-审核未通过、2-待审核)
            sql = sql + " AND SHFLAG = '"+shFlag+"'";
        }
        if (productType != null && !productType.equals("")) { //产品类别
            sql = sql + " and TYPE = '" + productType + "'";
        }
        sql = sql + " ORDER BY UPDATETIME DESC";
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 新增产品(后台)
     *
     * @param productContent
     * @return
     */
    @Override
    public DaoResult saveProduct(ProductContent productContent) {
        DaoResult result = new DaoResult();
        if (productContent == null || productContent == null) {
            result.setCode(-1);
            result.setErrText("添加失败！");
            return result;
        }
        Double a = productContent.getLoanrate();
        DConnection conn = DaoTool.getConnection();
        result = baseDao.insert(productContent, conn); //新增产品
        if (result.getCode() < 0) {
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }


    /**
     * 根据id获取产品信息(后台)
     *
     * @param productMes
     * @return
     */
    @Override
    public DMap queryProductById(ProductMessageBean productMes) {
        String id = productMes.getId(); //主键
        DMap dMap = new DMap();
        DMap result = new DMap();
        dMap.setData("id", id); //主键
        result = baseDao.getViewData("view_product_content", "id", id);
        return result;
    }


    /**
     * 根据Glid获取附件信息(后台)
     *
     * @param productMes
     * @return
     */
    @Override
    public DMap queryFileByGlId(ProductMessageBean productMes) {
        String glId = productMes.getId(); //附件表glId
        String sql = "SELECT ID,GLID,IMGURL,IMGURLJD,TYPE FROM sys_file WHERE 1 = 1 AND TYPE = '1'";
        if (glId != null && !glId.equals("")) { //关联id
            sql = sql + " AND GLID = '" + glId + "'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 修改产品(后台)
     *
     * @param productContent
     * @return
     */
    @Override
    public DaoResult updateProduct(ProductContent productContent) {
        DaoResult result = new DaoResult();
        if (productContent == null) {
            result.setCode(-1);
            result.setErrText("修改失败！");
            return result;
        }
        result = baseDao.update(productContent);
        return result;
    }


    /**
     * 获取产品数量(后台)
     *
     * @param producrMes
     * @return
     */
    @Override
    public DMap getCountProductByPage(ProductMessageBean producrMes) {
        String title = producrMes.getTitle(); //题目
        String shflag = producrMes.getShflag();//审核标志
        String fullScale = producrMes.getFullScale();//审核标志
        String sql = "SELECT * FROM product_content where 1 = 1"; //产品查询条件
        if (title != null && !title.equals("")) { //题目
            sql = sql + " AND TITLE LIKE '%" + title + "%'";
        }

        if (shflag != null && !shflag.equals("")) {
            sql = sql + " AND shflag = '" + shflag + "'";
        }

        if (fullScale != null && !fullScale.equals("")) {
            sql = sql + " AND fullScale = '" + fullScale + "'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 查询产品(后台)
     *
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public DMap getListBySome(String orderColumn, String orderType, String pageIndex, String pageSize, String typeId) {
        String sql = "select * from view_product_content where 1 = 1";
        if (typeId != null && !typeId.equals("")) {
            sql = sql + " AND TYPE = '" + typeId + "'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")) {
            sql = sql + " ORDER BY " + orderColumn + " " + orderType + "";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (Integer.parseInt(pageIndex) - 1) + "," + Integer.parseInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 查询产品列表 带分页 by cuibin
     */
    @Override
    public DMap queryProductListByPage(ProductMessageBean productMessageBean) {
//        String orderColumn = productMessageBean.getOrderColumn();
//        String orderType = productMessageBean.getOrderType();
        String pageIndex = productMessageBean.getPageIndex();
        String pageSize = productMessageBean.getPageSize();
        String productType = productMessageBean.getProductType();

        String sql = "select * from view_product_content where 1 = 1";


        if (productType != null && !productType.equals("")) {
            sql = sql + " and type = '" + productType + "'";
        }
        sql = sql + " order by CREATETIME DESC";
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " limit " + (Integer.parseInt(pageIndex) - 1) + "," + Integer.parseInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
//        System.out.println(sql);
//        System.out.println(dmap.getErrText());
        return dmap;
    }

    /**
     * 查询产品列表 总数 by cuibin
     */
    public DMap queryProductListByPageCount(ProductMessageBean productMessageBean) {
        String productType = productMessageBean.getProductType();

        String sql = "select count(*) as count from view_product_content where 1 = 1";
        if (productType != null && !productType.equals("")) {
            sql = sql + " and type = '" + productType + "'";
        }

        DMap dmap = DaoTool.select(sql);
        ;
        return dmap;
    }


    /**
     * 项目登机前查询
     *
     * @param productHuanXunMessageBean
     * @return
     */
    @Override
    public DMap queryProductDjInfo(ProductHuanXunMessageBean productHuanXunMessageBean) {
        String sql = "SELECT * FROM view_product_regist WHERE 1 = 1";
        String projectId = productHuanXunMessageBean.getId(); //项目id
        if (projectId != null && !projectId.equals("")) {
            sql = sql + " AND ID = '" + projectId + "'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 查询产品信息
     * @param orderColumn
     * @param orderType
     * @param productMes
     * @return
     */
    @Override
    public DMap getFrontList(String orderColumn, String orderType, ProductMessageBean productMes) {
        String sql = "SELECT * FROM view_product_content WHERE 1 = 1 AND FLAG = '0' AND DATE(NOW()) >= DATE(ONLINETIME)"; //产品查询条件
        String pageIndex = productMes.getPageIndex(); //当前数
        String pageSize = productMes.getPageSize(); //每页显示数量
        String productTypeIdSelectPj = productMes.getProductTypeIdSelectPj(); //已选择的产品类型拼接
        String loanrateSelectPj = productMes.getLoanrateSelectPj(); //已选择的年利率拼接
        String loanDateSelectPj = productMes.getLoanDateSelectPj(); //已选择的期限拼接
        String repaymentSelectPj = productMes.getRepaymentSelectPj(); //已选择的还款方式拼接
        String onlineSelect = productMes.getOnlineSelect(); //发布状态(0-已发布、1-即将发布)
        String[] arr = null; //用于存放产品类型
        String[] brr = null; //用于存放年利率
        String[] crr = null; //用于存放期限
        String[] drr = null; //用于存放还款方式

        /*************** 产品类型拼接Start ***************/
        if (productTypeIdSelectPj != null && !productTypeIdSelectPj.equals("")) {
            sql = sql + " AND (";
            arr = productTypeIdSelectPj.split(","); //用于存放产品类型
            for (int i = 0; i < arr.length; i++) {
                if (i == 0) {
                    sql = sql + "TYPE = '" + arr[i] + "'";
                } else {
                    sql = sql + " or TYPE = '" + arr[i] + "'";
                }
            }
            sql = sql + ")";
        }
        /**************** 产品类型拼接End ****************/

        /*************** 年利率拼接Start ***************/
        if (loanrateSelectPj != null && !loanrateSelectPj.equals("")) {
            sql = sql + " AND (";
            brr = loanrateSelectPj.split(","); //用于存放年利率
            for (int i = 0; i < brr.length; i++) {
                if (i == 0) {
                    if (brr[i].equals("1")) { //12%以下
                        sql = sql + "(LOANRATE < '12')";
                    } else if (brr[i].equals("2")) { //12%-14%
                        sql = sql + "(LOANRATE >= '12' and LOANRATE < '14')";
                    } else if (brr[i].equals("3")) { //14%-16%
                        sql = sql + "(LOANRATE >= '14' and LOANRATE < '16')";
                    } else if (brr[i].equals("4")) { //16%以上
                        sql = sql + "(LOANRATE >= '16')";
                    }
                } else {
                    if (brr[i].equals("1")) { //12%以下
                        sql = sql + "or (LOANRATE < '12')";
                    } else if (brr[i].equals("21")) { //12%-14%
                        sql = sql + "or (LOANRATE >= '12' and LOANRATE < '14')";
                    } else if (brr[i].equals("3")) { //14%-16%
                        sql = sql + "or (LOANRATE >= '14' and LOANRATE < '16')";
                    } else if (brr[i].equals("4")) { //16%以上
                        sql = sql + "or (LOANRATE >= '16')";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 年利率拼接End ****************/

        /*************** 期限拼接Start ***************/
        if (loanDateSelectPj != null && !loanDateSelectPj.equals("")) {
            sql = sql + " AND (";
            crr = loanDateSelectPj.split(","); //用于存放期限
            for (int i = 0; i < crr.length; i++) {
                if (i == 0) {
                    if (crr[i].equals("1")) { //1个月以下
                        sql = sql + "(LOANYEAR = '0' and LOANMONTH = '0')";
                    } else if (crr[i].equals("2")) { //1-2个月
                        sql = sql + "(LOANYEAR = '0' and LOANMONTH >= '1' and LOANMONTH <= '2')";
                    } else if (crr[i].equals("3")) { //3-5个月
                        sql = sql + "(LOANYEAR = '0' and LOANMONTH >= '3' and LOANMONTH <= '5')";
                    } else if (crr[i].equals("4")) { //6-11个月
                        sql = sql + "(LOANYEAR = '0' and LOANMONTH >= '6' and LOANMONTH <= '11')";
                    } else if (crr[i].equals("5")) { //12个月以上
                        sql = sql + "(LOANYEAR > '0')";
                    }
                } else {
                    if (crr[i].equals("1")) { //1个月以下
                        sql = sql + "OR (LOANYEAR = '0' and LOANMONTH = '0')";
                    } else if (crr[i].equals("2")) { //1-2个月
                        sql = sql + "OR (LOANYEAR = '0' and LOANMONTH >= '1' and LOANMONTH <= '2')";
                    } else if (crr[i].equals("3")) { //3-5个月
                        sql = sql + "OR (LOANYEAR = '0' and LOANMONTH >= '3' and LOANMONTH <= '5')";
                    } else if (crr[i].equals("4")) { //6-11个月
                        sql = sql + "OR (LOANYEAR = '0' and LOANMONTH >= '6' and LOANMONTH <= '11')";
                    } else if (crr[i].equals("5")) { //12个月以上
                        sql = sql + "OR (LOANYEAR > '0')";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 期限拼接End ****************/

        /*************** 还款方式拼接Start ***************/
        if (repaymentSelectPj != null && !repaymentSelectPj.equals("")) {
            sql = sql + " AND (";
            drr = repaymentSelectPj.split(",");
            for (int i = 0; i < drr.length; i++) {
                if (i == 0) {
                    if (drr[i].equals("1")) { //按月还款、等额本息
                        sql = sql + "REPAYMENT = '0'";
                    } else if (drr[i].equals("2")) { //按月付息、到期还本
                        sql = sql + "REPAYMENT = '1'";
                    } else if (drr[i].equals("3")) { //一次性还款
                        sql = sql + "REPAYMENT = '2'";
                    }
                } else {
                    if (drr[i].equals("1")) { //按月还款、等额本息
                        sql = sql + "OR REPAYMENT = '0'";
                    } else if (drr[i].equals("2")) { //按月付息、到期还本
                        sql = sql + "OR REPAYMENT = '1'";
                    } else if (drr[i].equals("3")) { //一次性还款
                        sql = sql + "OR REPAYMENT = '2'";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 还款方式拼接End ****************/

        if (onlineSelect != null && !onlineSelect.equals("")) { //发布状态(0-已发布、1-即将发布)
            if (onlineSelect.equals("0")) { //已发布
                sql = sql + " AND ONLINETIME <= NOW()";
            } else if (onlineSelect.equals("1")) { //即将发布
                sql = sql + " AND ONLINETIME > NOW()";
            }
        }

        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")) {
            sql = sql + " ORDER BY FULLSCALE DESC,"+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (Integer.parseInt(pageIndex) - 1) + "," + Integer.parseInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取产品数量(前台)
     *
     * @param productMes
     * @return
     */
    @Override
    public DMap queryProductCount(ProductMessageBean productMes) {
        String sql = "SELECT * FROM view_product_content WHERE 1 = 1 AND FLAG = '0'";
        String productTypeIdSelectPj = productMes.getProductTypeIdSelectPj(); //已选择的产品类型拼接
        String loanrateSelectPj = productMes.getLoanrateSelectPj(); //已选择的年利率拼接
        String loanDateSelectPj = productMes.getLoanDateSelectPj(); //已选择的期限拼接
        String repaymentSelectPj = productMes.getRepaymentSelectPj(); //已选择的还款方式拼接
        String onlineSelect = productMes.getOnlineSelect(); //发布状态(0-已发布、1-即将发布)
        String[] arr = null; //用于存放产品类型
        String[] brr = null; //用于存放年利率
        String[] crr = null; //用于存放期限
        String[] drr = null; //用于存放还款方式

        /*************** 产品类型拼接Start ***************/
        if (productTypeIdSelectPj != null && !productTypeIdSelectPj.equals("")) {
            sql = sql + " AND (";
            arr = productTypeIdSelectPj.split(","); //用于存放产品类型
            for (int i = 0; i < arr.length; i++) {
                if (i == 0) {
                    sql = sql + "TYPE = '" + arr[i] + "'";
                } else {
                    sql = sql + " OR TYPE = '" + arr[i] + "'";
                }
            }
            sql = sql + ")";
        }
        /**************** 产品类型拼接End ****************/

        /*************** 年利率拼接Start ***************/
        if (loanrateSelectPj != null && !loanrateSelectPj.equals("")) {
            sql = sql + " AND (";
            brr = loanrateSelectPj.split(","); //用于存放年利率
            for (int i = 0; i < brr.length; i++) {
                if (i == 0) {
                    if (brr[i].equals("1")) { //12%以下
                        sql = sql + "(LOANRATE < '12')";
                    } else if (brr[i].equals("2")) { //12%-14%
                        sql = sql + "(LOANRATE >= '12' AND LOANRATE < '14')";
                    } else if (brr[i].equals("3")) { //14%-16%
                        sql = sql + "(LOANRATE >= '14' AND LOANRATE < '16')";
                    } else if (brr[i].equals("4")) { //16%以上
                        sql = sql + "(LOANRATE >= '16')";
                    }
                } else {
                    if (brr[i].equals("1")) { //12%以下
                        sql = sql + "OR (LOANRATE < '12')";
                    } else if (brr[i].equals("21")) { //12%-14%
                        sql = sql + "OR (LOANRATE >= '12' AND LOANRATE < '14')";
                    } else if (brr[i].equals("3")) { //14%-16%
                        sql = sql + "OR (LOANRATE >= '14' AND LOANRATE < '16')";
                    } else if (brr[i].equals("4")) { //16%以上
                        sql = sql + "OR (LOANRATE >= '16')";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 年利率拼接End ****************/

        /*************** 期限拼接Start ***************/
        if (loanDateSelectPj != null && !loanDateSelectPj.equals("")) {
            sql = sql + " and (";
            crr = loanDateSelectPj.split(","); //用于存放期限
            for (int i = 0; i < crr.length; i++) {
                if (i == 0) {
                    if (crr[i].equals("1")) { //1个月以下
                        sql = sql + "(LOANYEAR = '0' AND LOANMONTH = '0')";
                    } else if (crr[i].equals("2")) { //1-2个月
                        sql = sql + "(LOANYEAR = '0' AND LOANMONTH >= '1' AND LOANMONTH <= '2')";
                    } else if (crr[i].equals("3")) { //3-5个月
                        sql = sql + "(LOANYEAR = '0' AND LOANMONTH >= '3' AND LOANMONTH <= '5')";
                    } else if (crr[i].equals("4")) { //6-11个月
                        sql = sql + "(LOANYEAR = '0' AND LOANMONTH >= '6' AND LOANMONTH <= '11')";
                    } else if (crr[i].equals("5")) { //12个月以上
                        sql = sql + "(LOANYEAR > '0')";
                    }
                } else {
                    if (crr[i].equals("1")) { //1个月以下
                        sql = sql + "OR (LOANYEAR = '0' AND LOANMONTH = '0')";
                    } else if (crr[i].equals("2")) { //1-2个月
                        sql = sql + "OR (LOANYEAR = '0' AND LOANMONTH >= '1' AND LOANMONTH <= '2')";
                    } else if (crr[i].equals("3")) { //3-5个月
                        sql = sql + "OR (LOANYEAR = '0' AND LOANMONTH >= '3' AND LOANMONTH <= '5')";
                    } else if (crr[i].equals("4")) { //6-11个月
                        sql = sql + "OR (LOANYEAR = '0' AND LOANMONTH >= '6' AND LOANMONTH <= '11')";
                    } else if (crr[i].equals("5")) { //12个月以上
                        sql = sql + "OR (LOANYEAR > '0')";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 期限拼接End ****************/

        /*************** 还款方式拼接Start ***************/
        if (repaymentSelectPj != null && !repaymentSelectPj.equals("")) {
            sql = sql + " AND (";
            drr = repaymentSelectPj.split(",");
            for (int i = 0; i < drr.length; i++) {
                if (i == 0) {
                    if (drr[i].equals("1")) { //按月还款、等额本息
                        sql = sql + "REPAYMENT = '0'";
                    } else if (drr[i].equals("2")) { //按月付息、到期还本
                        sql = sql + "REPAYMENT = '1'";
                    } else if (drr[i].equals("3")) { //一次性还款
                        sql = sql + "REPAYMENT = '2'";
                    }
                } else {
                    if (drr[i].equals("1")) { //按月还款、等额本息
                        sql = sql + "OR REPAYMENT = '0'";
                    } else if (drr[i].equals("2")) { //按月付息、到期还本
                        sql = sql + "OR REPAYMENT = '1'";
                    } else if (drr[i].equals("3")) { //一次性还款
                        sql = sql + "OR REPAYMENT = '2'";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 还款方式拼接End ****************/

        if (onlineSelect != null && !onlineSelect.equals("")) { //发布状态(0-已发布、1-即将发布)
            if (onlineSelect.equals("0")) { //已发布
                sql = sql + " AND DATE(ONLINETIME) <= DATE(NOW())";
            } else if (onlineSelect.equals("1")) { //即将发布
                sql = sql + " AND DATE(ONLINETIME) > DATE(NOW())";
            }
        }

        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 推荐产品
     *
     * @return
     */
    @Override
    public DMap getRecommend() {
        String sql = "SELECT * FROM view_product_content WHERE NEWSHARE = '0' LIMIT 0,1";
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 按照类别获取产品
     */
    @Override
    public DMap queryProductByType(String productType) {
        if (productType == null || productType.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("SELECT * FROM view_product_content WHERE" +
                " FLAG = '0' AND TYPE = '" + productType + "' AND ONLINETIME <= NOW() ORDER BY CREATETIME DESC" +
                " LIMIT 0,4");
    }


    /**
     * 获取交易金额总和
     *
     * @param productMes
     * @return
     */
    @Override
    public DMap getTradePriceSum(ProductMessageBean productMes) {
        String sql = "SELECT IFNULL(SUM(LOADFEE),0)LOADFEESUM,IFNULL(SUM(INTEREST),0)INTERESTSUM," +
                "IFNULL(SUM(YINVESTFEE),0)YINVESTFEE,IFNULL(SUM(WINVESTFEE),0)WINVESTFEE FROM view_product_content WHERE 1 = 1";
        String productId = productMes.getId(); //产品id
        if (productId != null && !productId.equals("")) {
            sql = sql + " AND ID = '" + productId + "'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 将要到期借款标数量 by cuibin
     */
    @Override
    public DMap getCountDueDate() {
        return DaoTool.select("SELECT * FROM view_product_content WHERE DATEDIFF(DATE(EXPIREDATE),DATE(NOW())) <= 7");
    }

    /**
     * 获取产品列表(App部分)
     *
     * @param appProductMessageBean
     * @return
     */
    @Override
    public DMap getAppProductList(AppProductMessageBean appProductMessageBean) {
        String sql = "SELECT * FROM view_product_content_app WHERE 1 = 1 AND DATE(NOW()) >= DATE(ONLINETIME)";
        String typeId = appProductMessageBean.getTypeId(); //产品类型id
        String flag = appProductMessageBean.getFlag(); //状态(0-上架、1-下架)
        String orderColumn = appProductMessageBean.getOrderColumn(); //排序字段
        String orderType = appProductMessageBean.getOrderType(); //排序类型
        String pageIndex = appProductMessageBean.getPageIndex(); //当前数
        String pageSize = appProductMessageBean.getPageSize(); //每次显示数量
        DMap dMap = new DMap();

        if (typeId != null && !typeId.equals("")) { //产品类型id
            sql = sql + " AND TYPE = '" + typeId + "'";
        }
        if (flag != null && !flag.equals("")) { //状态(0-上架、1-下架)
            sql = sql + " AND FLAG = '" + flag + "'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")) {
            sql = sql + " ORDER BY " + orderColumn + " " + orderType + "";
        } else {
            sql = sql + " ORDER BY FULLSCALE DESC,ONLINETIME DESC";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取推荐产品(微信部分) by cuibin
     */
    @Override
    public DMap getWeixinProductList(WeixinProductMessageBean weixinProductMes) {
        String typeId = weixinProductMes.getTypeId(); //产品类型id
        String orderColumn = weixinProductMes.getOrderColumn(); //排序字段
        String orderType = weixinProductMes.getOrderType(); //排序类型
        String pageIndex = weixinProductMes.getPageIndex(); //当前数
        String pageSize = weixinProductMes.getPageSize(); //每次显示数量

        String sql = "select * from view_product_content_app where 1 = 1";

        if (typeId != null && !typeId.equals("")) { //产品类型id
            sql = sql + " AND TYPE = '" + typeId + "'";
        }

        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")) {
            sql = sql + " ORDER BY " + orderColumn + " " + orderType + "";
        } else {
            sql = sql + " ORDER BY SORT ASC";
        }

        //System.out.println("==========================" + pageSize);
        //System.out.println("==========================" + pageIndex);

        if (("").equals(pageSize) || null == pageSize) {
            pageSize = "4";
        }
        if (("").equals(pageIndex) || null == pageIndex) {
            pageIndex = "1";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }

        DMap dMap = DaoTool.select(sql);
        //System.out.println("==========================" + sql);

        return dMap;
    }

    /**
     * 获得产品总数
     */
    @Override
    public DMap getWeixinProductCount(WeixinProductMessageBean weixinProductMes) {

        String typeId = weixinProductMes.getTypeId(); //产品类型id
        String sql = "select count(*) as count from view_product_content_app where 1 = 1";

        if (typeId != null && !typeId.equals("")) { //产品类型id
            sql = sql + " AND TYPE = '" + typeId + "'";
        }

        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 查看产品详情(微信部分) by cuibin
     */
    @Override
    public DMap productDetails(WeixinProductMessageBean weixinProductMes) {
        if (weixinProductMes == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }

        String sql = "select * from view_product_contentDetails_app where 1 = 1 and id = '" + weixinProductMes.getId() + "'";

        return DaoTool.select(sql);
    }

    /**
     * 查询债权转让列表 (微信部分) by cuibin
     */
    @Override
    public DMap queryProductAttornByType(WeixinProductAttornDetail weixinProductAttornDetail) {

        String orderColumn = weixinProductAttornDetail.getOrderColumn(); //排序字段
        String orderType = weixinProductAttornDetail.getOrderType(); //排序类型
        String pageIndex = weixinProductAttornDetail.getPageIndex(); //当前数
        String pageSize = weixinProductAttornDetail.getPageSize(); //每次显示数量

        String sql = "select * from view_product_attorn_app where 1 = 1";

        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")) {
            sql = sql + " ORDER BY " + orderColumn + " " + orderType + "";
        } else {
            sql = sql + " ORDER BY CREATETIME DESC";
        }

//        System.out.println("pageSize==========================" + pageSize);
//        System.out.println("pageIndex==========================" + pageIndex);

        if (("").equals(pageSize) || null == pageSize) {
            pageSize = "4";
        }
        if (("").equals(pageIndex) || null == pageIndex) {
            pageIndex = "1";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }

        DMap dMap = DaoTool.select(sql);
//        System.out.println("error==========================" + dMap.getErrText());

        return dMap;
    }

    /**
     * 查询债权转让列表总数 (微信部分) by cuibin
     */
    @Override
    public DMap queryProductAttornByTypeCount(WeixinProductAttornDetail weixinProductAttornDetail) {

        String sql = "select count(*) as count from view_product_attorn_app where 1 = 1";

        return DaoTool.select(sql);
    }

    /**
     * 查看债权转让详情(微信部分) by cuibin
     */
    @Override
    public DMap queryAttornDetails(String id) {
        if (id == null || id.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }

        String sql = "select * from view_product_attorn_Detail_app where 1 = 1 and id = '" + id + "'";

        return DaoTool.select(sql);
    }

    /**
     * 根据产品id获取产品详情
     *
     * @param productId
     * @return
     */
    @Override
    public DMap getProductDetail(String productId) {
        String sql = "SELECT * FROM view_product_contentDetails_app WHERE 1 = 1";
        DMap dMap = new DMap();
        if (productId != null && !productId.equals("")) { //产品id
            sql = sql + " AND ID = '" + productId + "'";
        }
        dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 查看借款信息(微信部分) by cuibin
     */
    @Override
    public DMap openProductBorrow(WeixinProductMemberMes weixinProductMemberMes) {
        if (weixinProductMemberMes == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String sql = "SELECT * FROM view_product_member_app WHERE 1 = 1 and id = " +
                "'" + weixinProductMemberMes.getId() + "'";

        //System.out.println("================" + sql);

        return DaoTool.select(sql);
    }

    /**
     * 查看投资记录(微信部分) by cuibin
     */
    @Override
    public DMap openLoanDetails(WeixinProductBuyMes weixinProductBuyMes) {

        if (weixinProductBuyMes == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }

        String sql = "select * from view_product_buy_app where 1 = 1 and PRODUCTID = " +
                "'" + weixinProductBuyMes.getProductid() + "'";
        return DaoTool.select(sql);
    }

    /**
     * 获取产品投资记录(App部分)
     *
     * @param productId
     * @return
     */
    @Override
    public DMap getProductBuyList(String productId, String pageIndex, String pageSize) {
        String sql = "SELECT * FROM view_product_buy_app WHERE 1 = 1";
        if (productId != null && !productId.equals("")) {
            sql = sql + " AND PRODUCTID = '" + productId + "'";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 获取产品会员信息(App部分)
     *
     * @param productId
     * @return
     */
    @Override
    public DMap getProductMemberInfo(String productId) {
        String sql = "SELECT * FROM view_product_member_app WHERE 1 = 1";
        if (productId != null && !productId.equals("")) { //产品id
            sql = sql + " AND ID = '" + productId + "'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 获取产品投资部分(App部分)
     *
     * @param productId
     * @return
     */
    @Override
    public DMap getProductBuyTj(String productId) {
        String sql = "SELECT * FROM view_product_InvestTj_app WHERE 1 = 1";
        if (productId != null && !productId.equals("")) {
            sql = sql + " AND ID = '" + productId + "'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 查询投资记录 by cuibin
     */
    @Override
    public DMap queryInvestHistory(ProductBuyMessageBean productMessageBean) {
        if (productMessageBean == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }

        String pageIndex = productMessageBean.getPageIndex(); //当前数
        String pageSize = productMessageBean.getPageSize(); //每次显示数量
        String startTime = productMessageBean.getStartTime(); //开始时间
        String endTime = productMessageBean.getEndTime();//结束时间
        String productid = productMessageBean.getProductId(); //产品id

        String sql = "select * from view_product_buy where 1 = 1";

        sql = sql + " and PRODUCTID = '" + productid + "'";

        if (startTime != null && !startTime.equals("")) {
            sql = sql + " and CREATETIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) {
            sql = sql + " and CREATETIME <= '" + endTime + "'";
        }

        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }

        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 查询投资记录 总数 by cuibin
     */
    @Override
    public DMap queryInvestHistoryCount(ProductBuyMessageBean productMessageBean) {
        if (productMessageBean == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String startTime = productMessageBean.getStartTime(); //开始时间
        String endTime = productMessageBean.getEndTime();//结束时间
        String productid = productMessageBean.getProductId(); //产品id
        String sql = "select count(*) as count from view_product_buy where 1 = 1";

        sql = sql + " and PRODUCTID = '" + productid + "'";

        if (startTime != null && !startTime.equals("")) {
            sql = sql + " and CREATETIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) {
            sql = sql + " and CREATETIME <= '" + endTime + "'";
        }

        DMap dMap = DaoTool.select(sql);

        return dMap;
    }


    /**
     * 获取需投资产品信息
     * @param productId
     * @return
     */
    @Override
    public DMap getInvestedProductInfo(String productId) {
        String sql = "SELECT APPLYMEMBER,BIDFEE,SPLITCOUNT FROM product_content WHERE ID = '"+productId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 更新推荐项目状态
     * @param newShare
     * @return
     */
    @Override
    public DaoResult setNewShare(String productId,String newShare) {
        String sql = "UPDATE product_content SET NEWSHARE = <newShare> WHERE ID = <productId>";
        DConnection conn = DaoTool.getConnection();
        DaoResult daoResult = new DaoResult();

        try{

            DMap dMap = new DMap();
            dMap.setData("newShare",newShare); //推荐状态(0-是、1-否)
            dMap.setData("productId",productId); //产品主键
            daoResult = DaoTool.update(sql,dMap.getData(),conn);
            if (daoResult.getCode() < 0){
                conn.rollback();
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            conn.commit();
            conn.close();
        }
        return daoResult;
    }

    /**
     * 获取推荐项目数量
     * @param newShare
     * @return
     */
    @Override
    public DMap getNewShareList(String newShare) {
        String sql = "SELECT ID FROM product_content WHERE 1 = 1 AND NEWSHARE = '"+newShare+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

}
