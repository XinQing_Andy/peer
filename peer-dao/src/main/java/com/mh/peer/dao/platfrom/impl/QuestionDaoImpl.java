package com.mh.peer.dao.platfrom.impl;

import com.mh.peer.dao.platfrom.QuestionDao;
import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.entity.QuestionContent;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/4/19.
 */
@Repository
public class QuestionDaoImpl implements QuestionDao{
    @Autowired
    private BaseDao baseDao;
    /**
     *查询所有常见问题总数
     */
    @Override
    public DMap queryQuestionByPageCount(Paging paging) {
        String pageIndex = paging.getPageIndex(); //当前页
        String pageSize = paging.getPageSize(); //每页显示数量
        String title = paging.getTitle(); //题目
        String sql = "SELECT * from question_content where 1=1";
        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (QUESTION LIKE '%"+title+"%' OR FLAG LIKE '%"+title+"%')";
        }
        return DaoTool.select(sql);
    }
    /**
     *查询所有常见问题
     */
    @Override
    public DMap queryQuestionByPage(Paging paging) {
        String pageIndex = paging.getPageIndex(); //当前页
        String pageSize = paging.getPageSize(); //每页显示数量
        String title = paging.getTitle(); //题目
        String sql = "SELECT * from question_content where 1=1";
        if(title != null && !title.equals("")){ //关键字
            sql = sql + " AND (QUESTION LIKE '%"+title+"%' OR FLAG LIKE '%"+title+"%')";
        }
        sql = sql + " order by sort asc";
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        return DaoTool.select(sql);
    }
    /**
     * 删除常见问题
     */
    @Override
    public DaoResult deleteQuestion(QuestionContent questionContent) {
        return baseDao.delete(questionContent);
    }
    /**
     *添加常见问题
     */
    @Override
    public DaoResult addQuestion(QuestionContent questionContent) {
        return baseDao.insert(questionContent);
    }
    /**
     *查询一个常见问题
     */
    @Override
    public DMap queryOnlyQuestion(int id) {
        return DaoTool.select("select * from question_content where id=" + id);
    }
    /**
     *修改常见问题
     */
    @Override
    public DaoResult updateQuestion(QuestionContent questionContent) {
        return baseDao.update(questionContent);
    }
    /**
     *常见问题是否启用
     */
    @Override
    public DaoResult updateStart(QuestionContent questionContent) {
        return baseDao.update(questionContent);
    }
}
