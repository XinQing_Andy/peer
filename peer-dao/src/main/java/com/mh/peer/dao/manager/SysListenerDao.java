package com.mh.peer.dao.manager;

import com.salon.frame.data.DMap;

/**
 * Created by zhangerxin on 2016-5-19.
 */
public interface SysListenerDao {

    /**
     * 获取监听信息
     * @return
     */
    DMap getListenerInfo();
}
