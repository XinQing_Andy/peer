package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.FrontDao;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-6-14.
 */
@Repository
public class FrontDaoImpl implements FrontDao{

    /**
     * 根据年份和月份获取第一天
     * @param rq
     * @return
     */
    @Override
    public DMap queryFirstDay(String rq) {
        String sql = "";
        DMap dMap = new DMap();
        if(rq != null && !rq.equals("")){
            sql = "select dayofweek('"+rq+"')FIRSTDAY";
            dMap = DaoTool.select(sql);
        }
        return dMap;
    }

    /**
     * 获取上个月最后一天
     * @param rq
     * @return
     */
    @Override
    public DMap queryLastDayLm(String rq) {
        String sql = "select dayofmonth(adddate(date_format('"+rq+"','%y-%m-%d'),-1))LASTDAYLM";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 根据日期和会员id判断是否存在回款
     * @param year
     * @param month
     * @param day
     * @param memberId
     * @return
     */
    @Override
    public DMap getReceiveFlag(String year, String month, String day, String memberId) {
        String sql = "SELECT COUNT(*)RECEIVECOUNT,YRECEIVETIMEDAY,IFNULL(SUM(IFNULL(SUMPRICE,0)),0)RECEIVEPRICE"
                   + " FROM view_product_receive_min WHERE 1 = 1";
        if(year != null && !year.equals("")){ //年份
            sql = sql + " AND YRECEIVETIMEYEAR = '"+year+"'";
        }
        if(month != null && !month.equals("")){ //月份
            sql = sql + " AND YRECEIVETIMEMONTH = '"+month+"'";
        }
        if(memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND BUYER = '"+memberId+"'";
        }
        sql = sql + " GROUP BY YRECEIVETIMEDAY ORDER BY YRECEIVETIME";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据日期和会员id判断是否存在还款
     * @param year
     * @param month
     * @param day
     * @param memberId
     * @return
     */
    @Override
    public DMap getRepayFlag(String year, String month, String day, String memberId) {
        String sql = "SELECT COUNT(*)REPAYCOUNT,YREPAYMENTTIMEDAY,IFNULL(ROUND(SUM(IFNULL(YPRICE,0)),2),0)REPAYPRICE"
                   + " FROM view_product_repay_min WHERE 1 = 1";
        if(year != null && !year.equals("")){ //年份
            sql = sql + " AND YREPAYMENTTIMEYEAR = '"+year+"'";
        }
        if(month != null && !month.equals("")){ //月份
            sql = sql + " AND YREPAYMENTTIMEMONTH = '"+month+"'";
        }
        if(memberId != null && !memberId.equals("")){
            sql = sql + " AND APPLYMEMBER = '"+memberId+"'";
        }
        sql = sql + " GROUP BY YREPAYMENTTIMEDAY ORDER BY YREPAYMENTTIME";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据日期和会员id获取回款信息
     * @param receiveYear
     * @param receiveMonth
     * @param memberId
     * @param receiveFlg
     * @return
     */
    @Override
    public DMap getReceiveInfo(String receiveYear, String receiveMonth, String memberId, String receiveFlg) {
        String sql = "SELECT COUNT(*)YRECEIVECOUNT,IFNULL(SUM(RECEIVEPRICE),0)YRECEIVEPRICE"
                   + " FROM view_product_receive_min WHERE 1 = 1";
        if(receiveYear != null && !receiveYear.equals("")){ //年份
            sql = sql + " AND YRECEIVETIMEYEAR = '"+receiveYear+"'";
        }
        if(receiveMonth != null && !receiveMonth.equals("")){ //月份
            sql = sql + " AND YRECEIVETIMEMONTH = '"+receiveMonth+"'";
        }
        if(memberId != null && !memberId.equals("")){
            sql = sql + " AND BUYER = '"+memberId+"'";
        }
        if (receiveFlg != null && !receiveFlg.equals("")){ //回收标志(0-已回收、1-待回收)
            if (receiveFlg.equals("0")){ //已回收
                sql = sql + " AND SRECEIVETIME IS NOT NULL";
            }else if (receiveFlg.equals("1")){ //待回收
                sql = sql + " AND SRECEIVETIME IS NULL";
            }
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据日期和会员id获取还款信息
     * @param repayYear
     * @param repayMonth
     * @param memberId
     * @param repayFlg
     * @return
     */
    @Override
    public DMap getRepayInfo(String repayYear, String repayMonth, String memberId, String repayFlg) {
        String sql = "SELECT COUNT(*)YREPAYCOUNT,IFNULL(SUM(REPAYMENTPRICE),0)YREPAYMENTPRICE"
                   + " FROM VIEW_product_repay_min WHERE 1 = 1";
        if (repayYear != null && !repayYear.equals("")){ //年份
            sql = sql + " AND YREPAYMENTTIMEYEAR = '"+repayYear+"'";
        }
        if (repayMonth != null && !repayMonth.equals("")){ //月份
            sql = sql + " AND YREPAYMENTTIMEMONTH = '"+repayMonth+"'";
        }
        if(memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND APPLYMEMBER = '"+memberId+"'";
        }
        if (repayFlg != null && !repayFlg.equals("")){ //还款状态
            if (repayFlg.equals("0")){ //已还款
                sql = sql + " AND SREPAYMENTTIME IS NOT NULL";
            }else if (repayFlg.equals("1")){ //待还款
                sql = sql + " AND SREPAYMENTTIME IS NULL";
            }
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

}
