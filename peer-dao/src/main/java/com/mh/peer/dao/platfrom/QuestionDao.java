package com.mh.peer.dao.platfrom;

import com.mh.peer.model.entity.Paging;
import com.mh.peer.model.entity.QuestionContent;
import com.mh.peer.model.message.QuestionContentMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/4/19.
 */
public interface QuestionDao {
    /**
     *查询所有常见问题
     */
    DMap queryQuestionByPage(Paging paging);
    /**
     *查询所有常见问题数量
     */
    DMap queryQuestionByPageCount(Paging paging);
    /**
     * 删除常见问题
     */
    DaoResult deleteQuestion(QuestionContent questionContent);
    /**
     *添加常见问题
     */
    DaoResult addQuestion(QuestionContent questionContent);
    /**
     *查询一个
     */
    DMap queryOnlyQuestion(int id);
    /**
     *修改常见问题
     */
    DaoResult updateQuestion(QuestionContent questionContent);
    /**
     *常见问题是否启用
     */
    DaoResult updateStart(QuestionContent questionContent);
}
