package com.mh.peer.dao.front;

import com.mh.peer.model.entity.MemberInfo;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by cuibin on 2016/4/23.
 */
public interface RegistDao {
    /**
     * 注册
     */
    DaoResult regist(MemberInfo memberInfo);
    /**
     * 激活
     */
    DaoResult activateUser(MemberInfo memberInfo);
    /**
     * 验证用户名是否存在
     */
    DMap checkUsername(String username);
}
