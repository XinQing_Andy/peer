package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.SysRole;
import com.mh.peer.model.entity.SysRoleMenu;
import com.mh.peer.model.entity.SysRoleUser;
import com.mh.peer.model.message.SysMenuMessageBean;
import com.mh.peer.model.message.SysRoleMenuMessBean;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import java.util.List;

/**
 * Created by zhangerxin on 2016/1/14.
 */
public interface RoleDao {

    /**
     * 根据用户获取角色
     * @param sysRoleUser
     * @return
     */
    List<SysRoleUser> getRoleUser(SysRoleUser sysRoleUser);

    /**
     * 获取角色信息
     * @param role
     * @return
     */
    List<SysRole> getRoleInfo(SysRole role);

    /**
     * 查询用户组信息(根据分页)(zhangerxin)
     * @param sysRolMese
     * @return
     */
    DMap getRoleInfoByPage(SysRoleMessageBean sysRolMese);

    /**
     * 根据id获取用户组信息
     * @param sysRolMese
     * @return
     */
    DMap queryRoleById(SysRoleMessageBean sysRolMese);

    /**
     * 查询菜单信息
     * @return
     */
    DMap getMenuInfo();

    /**
     * 新增部门信息
     * @param role
     * @return
     */
    DaoResult addRole(SysRole role);

    /**
     * 修改部门信息
     * @param role
     * @return
     */
    DaoResult updateRole(SysRole role);

    /**
     * 删除部门信息
     * @param role
     * @return
     */
    DaoResult delRole(SysRole role);

    /**
     * 删除角色菜单
     * @param sysRoleMenu
     * @return
     */
    DaoResult delRoleMenu(SysRoleMenu sysRoleMenu);

    /**
     * 新增角色菜单
     * @param sysRoleMenu
     * @return
     */
    DaoResult addRoleMenu(SysRoleMenu sysRoleMenu);

    /**
     * 根据角色查询菜单信息
     * @param role
     * @return
     */
    DMap getRoleMenuInfo(SysRole role);

    /**
     * 获取角色数量
     * @return
     */
    DMap getCountSysRoleByPage(SysRoleMessageBean sysRoleMes);
    /**
     * 查询所有菜单
     */
    DMap queryAllMenu();

    /**
     * 根据父id查询所有菜单
     * @param fid
     * @return
     */
    DMap queryAllMenuByFid(String fid);

    /**
     * 根据角色id查询都有哪些权限
     * @param roleId
     * @return
     */
    DMap getAllRoleMenu(String roleId);

    /**
     * 保存用户角色权限
     * @param sysRoleMenuMes
     * @return
     */
    SysRoleMenuMessBean saveRolePower(SysRoleMenuMessBean sysRoleMenuMes);
}
