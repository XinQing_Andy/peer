package com.mh.peer.dao.product.Impl;

import com.mh.peer.dao.product.ProductAttornDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.ProductAttornRecord;
import com.mh.peer.model.entity.ProductBuyInfo;
import com.mh.peer.model.entity.ProductReceive;
import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.StringTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by zhangerxin on 2016-5-23.
 * 债权转让
 */
@Repository
public class ProductAttornDaoImpl implements ProductAttornDao{

    @Autowired
    private BaseDao baseDao;

    /**
     * 获取可转让债权集合
     * @param productInvestMes
     * @return
     */
    @Override
    public DMap getAttornList(ProductInvestMessageBean productInvestMes) {
        String sql = "SELECT * FROM view_product_buy WHERE 1 = 1";
        String orderColumn = productInvestMes.getOrderColumn(); //排序字段
        String orderType = productInvestMes.getOrderType(); //排序类型
        String pageIndex = productInvestMes.getPageIndex(); //当前数
        String pageSize = productInvestMes.getPageSize(); //每次显示数量
        String buyStartTime = productInvestMes.getBuyStartTime(); //购买起时间
        String buyEndTime = productInvestMes.getBuyEndTime(); //购买止时间
        String expireStsrtTime = productInvestMes.getExpireStsrtTime(); //到期起时间
        String expireEndTime = productInvestMes.getExpireEndTime(); //到期止时间
        String title = productInvestMes.getTitle(); //题目
        String code = productInvestMes.getCode(); //编码
        String type = productInvestMes.getType(); //类型 0-持有 1-转让中 2-已转让
        String buyer = productInvestMes.getBuyer(); //购买人id 当前用户
        String shFlag = productInvestMes.getStr(); //审核状态(0-审核通过、1-审核未通过、2-未审核)
        String investFlag = productInvestMes.getInvestFlag(); //投资状态(0-回收中、1-投标中、2-已结清)

        if(shFlag != null && !shFlag.equals("")){ //审核状态(0-审核通过、1-审核未通过、2-未审核)
            sql = sql + " AND SHFLAG = '"+shFlag+"'";
        }
        if(buyStartTime != null && !buyStartTime.equals("")){ //购买起时间
            sql = sql + " AND DATE(BUYTIME) >= DATE('"+buyStartTime+"')";
        }
        if(buyEndTime != null && !buyEndTime.equals("")){ //购买止日期
            sql = sql + " AND DATE(BUYTIME) <= DATE('"+buyEndTime+"')";
        }
        if(expireStsrtTime != null && !expireStsrtTime.equals("")){ //到期起时间
            sql = sql + " AND DATE(ENDTIME) >= DATE('"+expireStsrtTime+"')";
        }
        if(expireEndTime != null && !expireEndTime.equals("")){ //到期止日期
            sql = sql + " AND DATE(ENDTIME) <= DATE('"+expireEndTime+"')";
        }
        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(code != null && !code.equals("")){ //编码
            sql = sql + " AND CODE LIKE '%"+code+"%'";
        }
        if(type != null && !type.equals("")){ //类型 0-持有 1-转让中 2-已转让
            sql = sql + " AND TYPE = '"+type+"'";
        }
        if(buyer != null && !buyer.equals("")){ //购买人
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        if(investFlag != null && !investFlag.equals("")){ //投资状态(0-回收中、1-投标中、2-已结清)
            sql = sql + " AND INVESTFLAG = '"+investFlag+"'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }

        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取可转让债权数量
     * @param productInvestMes
     * @return
     */
    @Override
    public DMap getAttornCount(ProductInvestMessageBean productInvestMes) {
        String sql = "SELECT * FROM view_product_buy WHERE 1 = 1";
        String orderColumn = productInvestMes.getOrderColumn(); //排序字段
        String orderType = productInvestMes.getOrderType(); //排序类型
        String pageIndex = productInvestMes.getPageIndex(); //当前数
        String pageSize = productInvestMes.getPageSize(); //每次显示数量
        String buyStartTime = productInvestMes.getBuyStartTime(); //购买起时间
        String buyEndTime = productInvestMes.getBuyEndTime(); //购买止时间
        String expireStsrtTime = productInvestMes.getExpireStsrtTime(); //到期起时间
        String expireEndTime = productInvestMes.getExpireEndTime(); //到期止时间
        String title = productInvestMes.getTitle(); //题目
        String code = productInvestMes.getCode(); //编码
        String type = productInvestMes.getType(); //类型 0-持有 1-转让中 2-已转让
        String buyer = productInvestMes.getBuyer(); //购买人id 当前用户
        String shFlag = productInvestMes.getStr(); //审核状态(0-审核通过、1-审核未通过、2-未审核)
        String investFlag = productInvestMes.getInvestFlag(); //投资状态(0-回收中、1-投标中、2-已结清)

        if(shFlag != null && !shFlag.equals("")){ //审核状态(0-审核通过、1-审核未通过、2-未审核)
            sql = sql + " AND SHFLAG = '"+shFlag+"'";
        }
        if(buyStartTime != null && !buyStartTime.equals("")){ //购买起时间
            sql = sql + " AND DATE(BUYTIME) >= DATE('"+buyStartTime+"')";
        }
        if(buyEndTime != null && !buyEndTime.equals("")){ //购买止日期
            sql = sql + " AND DATE(BUYTIME) <= DATE('"+buyEndTime+"')";
        }
        if(expireStsrtTime != null && !expireStsrtTime.equals("")){ //到期起时间
            sql = sql + " AND DATE(ENDTIME) >= DATE('"+expireStsrtTime+"')";
        }
        if(expireEndTime != null && !expireEndTime.equals("")){ //到期止日期
            sql = sql + " AND DATE(ENDTIME) <= DATE('"+expireEndTime+"')";
        }
        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(code != null && !code.equals("")){ //编码
            sql = sql + " AND CODE LIKE '%"+code+"%'";
        }
        if(type != null && !type.equals("")){ //类型 0-持有 1-转让中 2-已转让
            sql = sql + " AND TYPE = '"+type+"'";
        }
        if(buyer != null && !buyer.equals("")){ //购买人
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        if(investFlag != null && !investFlag.equals("")){ //投资状态(0-回收中、1-投标中、2-已结清)
            sql = sql + " AND INVESTFLAG = '"+investFlag+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 根据条件获取相应的债权集合
     * by cuibin (重做 by zhangerxin)
     */
    @Override
    public DMap queryMyAttornByPage(ProductAttornRecordMessageBean productAttornRecordMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1";
        String orderColumn = productAttornRecordMes.getOrderColumn(); //排序字段
        String orderType = productAttornRecordMes.getOrderType(); //排序类型
        String pageIndex = productAttornRecordMes.getPageIndex(); //当前数
        String pageSize = productAttornRecordMes.getPageSize(); //每次显示数量
        String title = productAttornRecordMes.getTitle(); //题目
        String code = productAttornRecordMes.getCode(); //编码
        String shFlag = productAttornRecordMes.getShFlag(); //审核状态
        String buyer = productAttornRecordMes.getBuyer(); //投资人(债权发起人)
        String undertakeMember = productAttornRecordMes.getUndertakeMember(); //债权承接人(购买人)
        String expireStsrtTime = productAttornRecordMes.getExpireStartTime(); //到期起时间
        String expireEndTime = productAttornRecordMes.getExpireEndTime(); //到期止时间
        String undertaketimeStart = productAttornRecordMes.getUndertaketimeStart(); //承接时间起
        String undertaketimeEnd = productAttornRecordMes.getUndertaketimeEnd(); //承接时间止
        String attornStartTime = productAttornRecordMes.getAttornStartTime(); //转让起日期
        String attornEndTime = productAttornRecordMes.getAttornEndTime(); //转让止日期

        if (expireStsrtTime != null && !expireStsrtTime.equals("")) { //到期起时间
            sql = sql + " AND DATE(ENDTIME) >= DATE('"+expireStsrtTime+"')";
        }
        if (expireEndTime != null && !expireEndTime.equals("")) { //到期止日期
            sql = sql + " AND DATE(ENDTIME) <= DATE('"+expireEndTime+"')";
        }
        if (undertaketimeStart != null && !undertaketimeStart.equals("")) { //承接时间起
            sql = sql + " AND DATE(UNDERTAKETIME) >= DATE('"+undertaketimeStart+"')";
        }
        if (undertaketimeEnd != null && !undertaketimeEnd.equals("")) { //承接时间止
            sql = sql + " AND DATE(UNDERTAKETIME) <= DATE('"+undertaketimeEnd+"')";
        }
        if (attornStartTime != null && !attornStartTime.equals("")){ //转让起日期
            sql = sql + " AND DATE(CREATETIME) >= DATE('"+attornStartTime+"')";
        }
        if (attornEndTime != null && !attornEndTime.equals("")){ //转让止日期
            sql = sql + " AND DATE(CREATETIME) <= DATE('"+attornEndTime+"')";
        }
        if (title != null && !title.equals("")) { //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if (code != null && !code.equals("")) { //编码
            sql = sql + " AND CODE LIKE '%"+code+"%'";
        }
        if (shFlag != null && !shFlag.equals("")){ //审核状态
            sql = sql + " AND SHFLAG = '"+shFlag+"'";
        }
        if (buyer != null && !buyer.equals("")){ //投资人(债权发起人)id
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        if (undertakeMember != null && !undertakeMember.equals("")){ //债权承接人(购买人)
            sql = sql + " AND UNDERTAKEMEMBER = '"+undertakeMember+"'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 根据条件获取相应的债权数量
     * @param productAttornRecordMes
     * @return
     */
    @Override
    public DMap queryMyAttornCount(ProductAttornRecordMessageBean productAttornRecordMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1";
        String expireStsrtTime = productAttornRecordMes.getExpireStartTime(); //到期起时间
        String expireEndTime = productAttornRecordMes.getExpireEndTime(); //到期止时间
        String undertaketimeStart = productAttornRecordMes.getUndertaketimeStart(); //承接时间起
        String undertaketimeEnd = productAttornRecordMes.getUndertaketimeEnd(); //承接时间止
        String title = productAttornRecordMes.getTitle(); //题目
        String code = productAttornRecordMes.getCode(); //编码
        String type = productAttornRecordMes.getType(); //类型(0-持有、1-转让中、2-已转让)
        String buyer = productAttornRecordMes.getBuyer(); //购买人
        String shFlag = productAttornRecordMes.getShFlag(); //审核状态

        if (expireStsrtTime != null && !expireStsrtTime.equals("")) { //到期起时间
            sql = sql + " AND DATE(ENDTIME) >= DATE('"+expireStsrtTime+"')";
        }
        if (expireEndTime != null && !expireEndTime.equals("")) { //到期止日期
            sql = sql + " AND DATE(ENDTIME) <= DATE('"+expireEndTime+"')";
        }
        if (undertaketimeStart != null && !undertaketimeStart.equals("")) { //承接时间起
            sql = sql + " AND DATE(UNDERTAKETIME) >= DATE('"+undertaketimeStart+"')";
        }
        if (undertaketimeEnd != null && !undertaketimeEnd.equals("")) { //承接时间止
            sql = sql + " AND DATE(UNDERTAKETIME) <= DATE('"+undertaketimeEnd+"')";
        }
        if (title != null && !title.equals("")) { //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if (code != null && !code.equals("")) { //编码
            sql = sql + " AND CODE LIKE '%"+code+"%'";
        }
        if (shFlag != null && !shFlag.equals("")){ //审核状态
            sql = sql + " AND SHFLAG = '"+shFlag+"'";
        }
        if (buyer != null && !buyer.equals("")){ //投资人id
            sql = sql + " AND BUYER = '"+buyer+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取更多债权
     * @param productAttornMoreMes
     * @return
     */
    @Override
    public DMap getAttornMoreList(ProductAttornMoreMessageBean productAttornMoreMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1";
        String attornType = productAttornMoreMes.getAttornType(); //债权转让类型(0-已转让、1-待转让)
        String pageIndex = productAttornMoreMes.getPageIndex(); //起始数
        String pageSize = productAttornMoreMes.getPageSize(); //每页显示数量
        String orderColumn = productAttornMoreMes.getOrderColumn(); //排序字段
        String orderType = productAttornMoreMes.getOrderType(); //排序类型

        if (attornType != null && !attornType.equals("")){
            if (attornType.equals("1")){ //待转让
                sql = sql + " AND UNDERTAKEMEMBER IS NULL";
            }
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+ TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取债权转让数量
     * @param productAttornMoreMes
     * @return
     */
    @Override
    public DMap getAttornMoreCount(ProductAttornMoreMessageBean productAttornMoreMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1";
        String attornType = productAttornMoreMes.getAttornType(); //债权转让类型(0-已转让、1-待转让)

        if (attornType != null && !attornType.equals("")){
            if (attornType.equals("1")){ //待转让
                sql = sql + " AND UNDERTAKEMEMBER IS NULL";
            }
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取债权转让数量(前台)
     * @param poductAttornRecordMes
     * @return
     */
    @Override
    public DMap getFrontAttornCount(ProductAttornRecordMessageBean poductAttornRecordMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1 AND SHFLAG IN ('0','2')";
        String pageIndex = poductAttornRecordMes.getPageIndex(); //当前数
        String pageSize = poductAttornRecordMes.getPageSize(); //每次显示数量
        String expireStsrtTime = poductAttornRecordMes.getExpireStartTime(); //到期起时间
        String expireEndTime = poductAttornRecordMes.getExpireEndTime(); //到期止时间
        String undertaketimeStart = poductAttornRecordMes.getUndertaketimeStart(); //承接时间起
        String undertaketimeEnd = poductAttornRecordMes.getUndertaketimeEnd(); //承接时间止
        String title = poductAttornRecordMes.getTitle(); //题目
        String code = poductAttornRecordMes.getCode(); //编码
        String type = poductAttornRecordMes.getType(); //类型 0-持有 1-转让中 2-已转让
        String buyer = poductAttornRecordMes.getBuyer(); //购买人

        if (expireStsrtTime != null && !expireStsrtTime.equals("")) { //购买起时间
            sql = sql + " AND ENDTIME >= '" + expireStsrtTime + "'";
        }
        if (expireEndTime != null && !expireEndTime.equals("")) { //购买止日期
            sql = sql + " AND ENDTIME <= '" + expireEndTime + "'";
        }
        if (undertaketimeStart != null && !undertaketimeStart.equals("")) { //承接时间起
            sql = sql + " AND UNDERTAKETIME >= '" + undertaketimeStart + "'";
        }
        if (undertaketimeEnd != null && !undertaketimeEnd.equals("")) { //承接时间止
            sql = sql + " AND UNDERTAKETIME <= '" + undertaketimeEnd + "'";
        }
        if (title != null && !title.equals("")) { //题目
            sql = sql + " AND TITLE LIKE '%" + title + "%'";
        }
        if (code != null && !code.equals("")) { //编码
            sql = sql + " AND CODE LIKE '%" + code + "%'";
        }
        if (type != null && !type.equals("")){ //产品类型
            if (type.equals("1")) { //持有
                sql = sql + " AND SHFLAG = '2' AND BUYER = '" + buyer + "'";
            } else if (type.equals("2")) { //转让中
                sql = sql + " AND UNDERTAKEMEMBER = '" + buyer + "'";
            } else if (type.equals("3")) { //已转让
                sql = sql + " AND SHFLAG = '0' AND BUYER = '"+buyer+"'";
            }
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 转让申请数量 by cuibin
     */
    @Override
    public DMap getCountByType(ProductAttornRecordMessageBean productAttornRecordMessageBean) {
        String sql = "select count(*) as zs from view_product_attorn where 1 = 1";
        String pageIndex=productAttornRecordMessageBean.getPageIndex();
        String pageSize=productAttornRecordMessageBean.getPageSize();
        String shFlag = productAttornRecordMessageBean.getShFlag();
        if (shFlag != null && !shFlag.equals("")) {
            sql = sql + " and SHFLAG = '" + shFlag + "'";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " limit "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        return DaoTool.select(sql);
    }


    /**
     * 承接债权
     * @param productAttornRecord
     * @param memberInfo
     * @return
     */
    @Override
    public DaoResult updateAttornUpdate(ProductAttornRecord productAttornRecord,MemberInfo memberInfo) {
        DConnection conn = DaoTool.getConnection();
        DaoResult resultAttorn = new DaoResult(); //债权承接
        DaoResult resultMember = new DaoResult(); //会员信息

        try {

            resultAttorn = baseDao.update(productAttornRecord,conn);
            if(resultAttorn.getCode()<0){
                conn.rollback();
            }else{
                resultMember = baseDao.update(memberInfo,conn);
                if(resultMember.getCode() < 0){
                    conn.rollback();
                }
            }
        }catch (Exception ex){
            conn.rollback();
        }finally {
            conn.commit();
            conn.close();
        }
        return resultMember;
    }


    /**
     * 添加转让记录（发起转让）
     * @param productAttornRecord
     * @param productBuyInfo
     * @return
     */
    @Override
    public DaoResult confirmAttornProduct(ProductAttornRecord productAttornRecord,ProductBuyInfo productBuyInfo) {
        DConnection conn = DaoTool.getConnection();
        DaoResult resultAttorn = new DaoResult(); //债权转让
        DaoResult resultBuy = new DaoResult(); //产品购买
        DaoResult resultMember = new DaoResult(); //会员信息

        try {

            resultAttorn = baseDao.insert(productAttornRecord,conn); //债权
            if(resultAttorn.getCode()>=0){
                resultBuy = baseDao.update(productBuyInfo,conn); //购买表
                if(resultBuy.getCode() < 0){
                    conn.rollback();
                }
            }else{
                conn.rollback();
            }
            conn.commit();
        }catch (Exception ex){
            conn.rollback();
        }finally {
            conn.close();
        }

        return resultMember;
    }


    /**
     * 根据分页获取债权转让标(后台) by zhangerxin
     * @param productAttornMes
     * @return
     */
    @Override
    public DMap queryProductAttornByPage(ProductAttornMessage productAttornMes) {
        String sql = "SELECT * FROM view_product_attorn_min WHERE 1 = 1"; //债权转让标查询
        String pageIndex = productAttornMes.getPageIndex(); //起始数
        String pageSize = productAttornMes.getPageSize(); //每次显示数量
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        String shFlag = productAttornMes.getShFlag(); //审核状态
        String orderType = productAttornMes.getOrderType(); //排序类型
        String orderColumn = productAttornMes.getOrderColumn(); //排序字段

        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(code != null && !code.equals("")){ //编码
            sql = sql + " AND CODE LIKE '%"+code+"%'";
        }
        if(shFlag != null && !shFlag.equals("")){ //审核状态
            sql = sql + " AND SHFLAG = '"+shFlag+"'";
            if(shFlag.equals("2")){ //待审核
                sql = sql + " AND UNDERTAKEMEMBER IS NOT NULL";
            }
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }else{
            sql = sql + " ORDER BY UNDERTAKETIME DESC";
        }

        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){ //当前数
            sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取债权转让标数量
     * @param productAttornMes
     * @return
     */
    @Override
    public DMap queryProductAttornCount(ProductAttornMessage productAttornMes) {
        String sql = "SELECT * FROM view_product_attorn_min WHERE 1 = 1"; //债权转让标查询
        String pageIndex = productAttornMes.getPageIndex(); //起始数
        String pageSize = productAttornMes.getPageSize(); //每次显示数量
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        String shFlag = productAttornMes.getShFlag(); //审核状态

        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(code != null && !code.equals("")){ //编码
            sql = sql + " AND CODE LIKE '%"+code+"%'";
        }
        if(shFlag != null && !shFlag.equals("")){ //审核状态
            sql = sql + " AND SHFLAG = '"+shFlag+"'";
            if(shFlag.equals("2")){ //待审核
                sql = sql + " AND UNDERTAKEMEMBER IS NOT NULL";
            }
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 债权标审核
     * @param productAttornMes
     * @return
     */
    @Override
    public DaoResult setProductAttornShFlag(ProductAttornMessage productAttornMes) {
        DConnection conn = DaoTool.getConnection();
        String attornId = productAttornMes.getId(); //债权转让id
        String productId = ""; //产品id
        String attornPrice = "0" ; //债权转让金额
        String buyId = ""; //购买id
        String underTakeMemberId = ""; //承接人id
        String receiveId = ""; //回款id
        Double balance = 0.0d; //余额
        String shFlag = productAttornMes.getShFlag(); //审核状态(0-审核通过、1-审核不通过、2-待审核)
        String shyj = productAttornMes.getShyj(); //审核意见
        String sqlAttorn = "SELECT * FROM view_product_attorn_min WHERE 1 = 1"; //债权表查询条件
        //产品购买查询条件
        String sqlReceive = "SELECT * FROM product_receive WHERE 1 = 1 AND DATE(YRECEIVETIME) > DATE(NOW())"
                      + " AND SRECEIVETIME IS NULL";
        String sqlMember = "SELECT * FROM member_info WHERE 1 = 1"; //会员查询条件
        DMap dMapAttorn = new DMap(); //债权转让查询
        DMap dMapReceive = new DMap(); //购买表查询
        DMap dMapMember = new DMap(); //会员表查询
        DaoResult daoResultAttorn = new DaoResult(); //债权转让更新
        DaoResult daoResultBuy = new DaoResult(); //更新投资
        DaoResult daoResultBuy2 = new DaoResult(); //新增投资
        DaoResult daoResultReceive = new DaoResult(); //回款
        DaoResult daoResultMember = new DaoResult(); //会员
        String UidBuyId = ""; //产品购买表主键
        String UidAttorn = ""; //债权转让标主键

        try{

            /******************** 债权转让Start ********************/
            ProductAttornRecord productAttornRecord = new ProductAttornRecord(); //债权转让
            productAttornRecord.setId(attornId); //债权转让主键
            productAttornRecord.setShflag(shFlag); //审核状态(0-审核通过、1-审核不通过、2-待审核)
            productAttornRecord.setShyj(shyj); //审核意见
            daoResultAttorn = baseDao.update(productAttornRecord,conn); //债权转让更新
            /********************* 债权转让End *********************/

            if(attornId != null && !attornId.equals("")){
                sqlAttorn = sqlAttorn + " AND ID = '"+attornId+"'";
            }

            dMapAttorn = DaoTool.select(sqlAttorn);
            if(dMapAttorn != null && dMapAttorn.getCount() > 0){
                buyId = dMapAttorn.getValue("PRODUCTBUYID",0); //购买id
                attornPrice = dMapAttorn.getValue("PRICE",0); //债权转让金额
                productId = dMapAttorn.getValue("PRODUCTID",0); //产品主键
                underTakeMemberId = dMapAttorn.getValue("UNDERTAKEMEMBER",0); //承接人id
            }

            if(shFlag.equals("0")){ //审核成功

                /*************** 更新购买表类型Start ***************/
                ProductBuyInfo productBuyInfo = new ProductBuyInfo();
                productBuyInfo.setId(buyId); //购买id
                productBuyInfo.setType("2"); //类型 0-持有 1-转让中 2-已转让
                daoResultBuy = baseDao.update(productBuyInfo,conn);
                /**************** 更新购买表类型End ****************/

                /*************** 新增购买表Start ***************/
                ProductBuyInfo productBuyInfo2 = new ProductBuyInfo();
                UidBuyId = UUID.randomUUID().toString().replaceAll("-","");
                productBuyInfo2.setId(UidBuyId); //产品购买id
                productBuyInfo2.setProductid(productId); //产品id
                productBuyInfo2.setBuyer(underTakeMemberId); //购买人id
                productBuyInfo2.setPrice(TypeTool.getDouble(attornPrice)); //购买金额
                productBuyInfo2.setBuytime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //购买时间
                productBuyInfo2.setType("0"); //类型 0-持有 1-转让中 2-已转让
                productBuyInfo2.setCreatetime(StringTool.getString(DaoTool.getTime(), "yyyy-MM-dd HH:mm:ss")); //创建时间
                daoResultBuy2 = baseDao.insert(productBuyInfo2,conn);
                /**************** 新增购买表End ****************/

                /*************** 回款表更新Start ***************/
                if(buyId != null && !buyId.equals("")){
                    sqlReceive = sqlReceive + " AND BUYID = '"+buyId+"'";
                    sqlMember = sqlMember + " AND ID = '"+buyId+"'";
                    dMapReceive = DaoTool.select(sqlReceive);
                    if(dMapReceive != null && dMapReceive.getCount() > 0){
                        for(int i=0;i<dMapReceive.getCount();i++){
                            receiveId = dMapReceive.getValue("Id",i); //回款id
                            ProductReceive productReceive = new ProductReceive();
                            productReceive.setId(receiveId); //回款id
                            productReceive.setBuyid(UidBuyId); //购买id
                            daoResultReceive = baseDao.update(productReceive,conn);
                        }
                    }
                }
                /**************** 回款表更新End ****************/

                /*************** 更新余额Start ***************/
                dMapMember = DaoTool.select(sqlMember);
                if(dMapMember != null && dMapMember.getCount() > 0){
                    balance = TypeTool.getDouble(dMapMember.getValue("BALANCE",0)); //余额
                    balance = balance + TypeTool.getDouble(attornPrice);
                    MemberInfo memberInfo = new MemberInfo(); //会员部分
                    memberInfo.setId(buyId); //主键
                    memberInfo.setBalance(balance); //余额
                    daoResultMember = baseDao.update(memberInfo,conn); //余额更新
                }
                /**************** 更新余额End ****************/

            }else if(shFlag.equals("1")){ //审核未通过

                /*************** 更新债权人余额Start ***************/
                sqlMember = sqlMember + " AND ID = '"+underTakeMemberId+"'";
                dMapMember = DaoTool.select(sqlMember);
                if(dMapMember != null && dMapMember.getCount() > 0){
                    balance = TypeTool.getDouble(dMapMember.getValue("BALANCE",0)); //余额
                    balance = balance + TypeTool.getDouble(attornPrice);
                    MemberInfo memberInfo = new MemberInfo(); //会员部分
                    memberInfo.setId(buyId); //主键
                    memberInfo.setBalance(balance); //余额
                    daoResultMember = baseDao.update(memberInfo,conn); //余额更新
                }
                /**************** 更新债权人余额End ****************/
            }
            conn.commit();
        }catch(Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.close();
        }

        return daoResultMember;
    }


    /**
     * 获取债权转让集合
     * @param memberId
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public DMap getAppAttornList(String memberId, String typeId, String pageIndex, String pageSize, String orderColumn, String orderType) {
        String sql = "SELECT * FROM view_product_attorn_app WHERE 1 = 1 AND SHFLAG <> '1'";
        if (memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND BUYER = '"+memberId+"'";
        }
        if (typeId != null && !typeId.equals("")){ //产品类型id
            sql = sql + " AND TYPEID = '"+typeId+"'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }else{
            sql = sql + " ORDER BY UNDERTAKETIME,CREATETIME DESC";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取债权转让标详情(App端)
     * @param attornId
     * @return
     */
    @Override
    public DMap getAppAttornDetail(String attornId) {
        String sql = "SELECT * FROM view_product_attorn_Detail_app WHERE 1 = 1";
        if (attornId != null && !attornId.equals("")){
            sql = sql + " AND ID = '"+attornId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取已购买债券
     * @param timeStyle
     * @param sTime
     * @param eTime
     * @param keyWord
     * @param value
     * @return
     */
    @Override
    public DMap queryInvestedAttornByPage(String memberId,String timeStyle, String sTime, String eTime, String keyWord, String value, String orderColumn, String orderType, String pageIndex, String pageSize) {
        String sql = "SELECT ID,PRODUCTBUYID,PRODUCTID,TITLE,CODE,LOANRATE,WAITINGPRINCIPAL,WAITINGINTEREST," +
                    "PRICE,UNDERTAKETIME,SHFLAG FROM view_product_attorn_min WHERE 1 = 1";

        if (memberId != null && !memberId.equals("")){ //承接人
            sql = sql + " AND UNDERTAKEMEMBER = '"+memberId+"'";
        }
        if (timeStyle != null && !timeStyle.equals("")){ //时间类型
            if (timeStyle.equals("1")){ //承接时间
                if (sTime != null && !sTime.equals("")){ //起时间
                    sql = sql + " AND DATE(UNDERTAKETIME) >= DATE('"+sTime+"')";
                }
                if (eTime != null && !eTime.equals("")){ //止时间
                    sql = sql + " AND DATE(UNDERTAKETIME) <= DATE('"+eTime+"')";
                }
            }else if (timeStyle.equals("2")){ //到期时间
                if (sTime != null && !sTime.equals("")){ //起时间
                    sql = sql + " AND DATE(EXPIREDATE) >= DATE('"+sTime+"')";
                }
                if (eTime != null && !eTime.equals("")){ //止时间
                    sql = sql + " AND DATE(EXPIREDATE) <= DATE('"+eTime+"')";
                }
            }
        }
        if (keyWord != null && !keyWord.equals("")){ //关键字
            if (value != null && !value.equals("")){ //关键值
                if (keyWord.equals("1")){ //编号
                    sql = sql + " AND CODE LIKE '%"+value+"%'";
                }else if (keyWord.equals("2")){ //名称
                    sql = sql + " AND TITLE LIKE '%"+value+"%'";
                }
            }
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex) - 1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取待承接债权
     * @param productAttornMes
     * @return
     */
    @Override
    public DMap queryProductAttornUnderTaked(ProductAttornMessage productAttornMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1 AND UNDERTAKETIME IS NULL";
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        String orderColumn = productAttornMes.getOrderColumn(); //排序字段
        String orderType = productAttornMes.getOrderType(); //排序类型
        String pageIndex = productAttornMes.getPageIndex(); //起始数
        String pageSize = productAttornMes.getPageSize(); //每页显示数量
        DMap dMap = new DMap();

        try{

            if (title != null && !title.equals("")){ //题目
                sql = sql + " AND TITLE LIKE '%"+title+"%'";
            }
            if (code != null && !code.equals("")){ //编码
                sql = sql + " AND CODE LIKE '%"+code+"%'";
            }
            if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
                sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
            }
            if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
                sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex) - 1)+","+TypeTool.getInt(pageSize)+"";
            }
            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 获取待承接债权数量
     * @param productAttornMes
     * @return
     */
    @Override
    public DMap queryProductAttornUnderTakedCount(ProductAttornMessage productAttornMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1 AND UNDERTAKETIME IS NULL";
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        DMap dMap = new DMap();

        try{

            if (title != null && !title.equals("")){ //题目
                sql = sql + " AND TITLE LIKE '%"+title+"%'";
            }
            if (code != null && !code.equals("")){ //编码
                sql = sql + " AND CODE LIKE '%"+code+"%'";
            }
            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 获取失败的债权
     * @param productAttornMes
     * @return
     */
    @Override
    public DMap queryProductAttornFail(ProductAttornMessage productAttornMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1";
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        String shFlag = productAttornMes.getShFlag(); //审核状态(0-审核成功、1-审核不成功、2-待审核)
        String orderColumn = productAttornMes.getOrderColumn(); //排序字段
        String orderType = productAttornMes.getOrderType(); //排序类型
        String pageIndex = productAttornMes.getPageIndex(); //起始数
        String pageSize = productAttornMes.getPageSize(); //每页显示数量
        DMap dMap = new DMap();

        try{

            if (title != null && !title.equals("")){ //题目
                sql = sql + " AND TITLE LIKE '%"+title+"%'";
            }
            if (code != null && !code.equals("")){ //编码
                sql = sql + " AND CODE LIKE '%"+code+"%'";
            }
            if (shFlag != null && !shFlag.equals("")){ //审核状态
                sql = sql + " AND SHFLAG = '"+shFlag+"'";
            }
            if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
                sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
            }
            if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
                sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex) - 1)+","+TypeTool.getInt(pageSize)+"";
            }
            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 获取失败的债权数量
     * @param productAttornMes
     * @return
     */
    @Override
    public DMap queryProductAttornFailCount(ProductAttornMessage productAttornMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1";
        String title = productAttornMes.getTitle(); //题目
        String code = productAttornMes.getCode(); //编码
        String shFlag = productAttornMes.getShFlag(); //审核状态(0-审核成功、1-审核不成功、2-待审核)
        DMap dMap = new DMap();

        try{

            if (title != null && !title.equals("")){ //题目
                sql = sql + " AND TITLE LIKE '%"+title+"%'";
            }
            if (code != null && !code.equals("")){ //编码
                sql = sql + " AND CODE LIKE '%"+code+"%'";
            }
            if (shFlag != null && !shFlag.equals("")){ //审核状态
                sql = sql + " AND SHFLAG = '"+shFlag+"'";
            }
            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 获取可转让的债权详情
     * @param productAttornRecordMes
     * @return
     */
    public DMap getAttornDetail(ProductAttornRecordMessageBean productAttornRecordMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1";
        if (productAttornRecordMes.getProductid() != null && !productAttornRecordMes.getProductid().equals("")) {
            sql = sql + " AND PRODUCTID = '" + productAttornRecordMes.getProductid() + "'";
        }
        if (productAttornRecordMes.getId() != null && !productAttornRecordMes.getId().equals("")) {
            sql = sql + " AND ID = '" + productAttornRecordMes.getId() + "'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 获取债权详情
     * @param attornProductMessageBean
     * @return
     */
    @Override
    public DMap getAttornProductDetai(AttornProductMessageBean attornProductMessageBean) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1";
        String id = attornProductMessageBean.getId(); //主键
        if(id != null && !id.equals("")){
            sql = sql + " AND ID = '"+id+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 前台债权查询
     * @param productAttornRecordMes
     * @return
     */
    public DMap getFrontAttornList(String orderColumn, String orderType,ProductAttornRecordMessageBean productAttornRecordMes) {
        String sql = "SELECT * FROM view_product_attorn WHERE 1 = 1 AND SHFLAG IN ('0','2')";
        String pageIndex = productAttornRecordMes.getPageIndex(); //当前数
        String pageSize = productAttornRecordMes.getPageSize(); //每页显示数量
        String typeId = productAttornRecordMes.getType(); //产品类型
        String[] trbutton1 = null; //项目类型
        String[] trbutton2 = null; //预期收益率
        String[] trbutton3 = null; //转让价格
        String[] trbutton4 = null; //剩余期限

        /*************** 项目类型拼接Start ***************/
        if(productAttornRecordMes.getTrbutton1() != null && !productAttornRecordMes.getTrbutton1().equals("") && !productAttornRecordMes.getTrbutton1().equals("0")){
            sql = sql + " AND (";
            trbutton1 = productAttornRecordMes.getTrbutton1().split(","); //用于存放产品类型
            for(int i=0;i<trbutton1.length;i++){
                if(i == 0){
                    sql = sql + "TYPE = '"+trbutton1[i]+"'";
                }else{
                    sql = sql + " OR TYPE = '"+trbutton1[i]+"'";
                }
            }
            sql = sql + ")";
        }
        /**************** 项目类型拼接End ****************/

        /*************** 预期收益率拼接Start ***************/
        if(productAttornRecordMes.getTrbutton2() != null && !productAttornRecordMes.getTrbutton2().equals("") && !productAttornRecordMes.getTrbutton2().equals("0")){
            sql = sql + " AND (";
            trbutton2 = productAttornRecordMes.getTrbutton2().split(","); //用于存放年利率
            for(int i=0;i<trbutton2.length;i++){
                if(i == 0){
                    if(trbutton2[i].equals("1")){ //12%以下
                        sql = sql + "(EXPECTEDRATE < 12)";
                    }else if(trbutton2[i].equals("2")){ //12%-14%
                        sql = sql + "(EXPECTEDRATE >= 12 and EXPECTEDRATE < 14)";
                    }else if(trbutton2[i].equals("3")){ //14%-16%
                        sql = sql + "(EXPECTEDRATE >= 14 and EXPECTEDRATE < 16)";
                    }else if(trbutton2[i].equals("4")){ //16%以上
                        sql = sql + "(EXPECTEDRATE >= 16)";
                    }
                }else{
                    if(trbutton2[i].equals("1")){ //12%以下
                        sql = sql + "OR (EXPECTEDRATE < 12)";
                    }else if(trbutton2[i].equals("2")){ //12%-14%
                        sql = sql + "OR (EXPECTEDRATE >= 12 and EXPECTEDRATE < 14)";
                    }else if(trbutton2[i].equals("3")){ //14%-16%
                        sql = sql + "OR (EXPECTEDRATE >= 14 and EXPECTEDRATE < 16)";
                    }else if(trbutton2[i].equals("4")){ //16%以上
                        sql = sql + "OR (EXPECTEDRATE >= 16)";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 预期收益率拼接End ****************/

        /*************** 转让价格拼接Start ***************/
        if(productAttornRecordMes.getTrbutton3() != null && !productAttornRecordMes.getTrbutton3().equals("") && !productAttornRecordMes.getTrbutton3().equals("0")){
            sql = sql + " AND (";
            trbutton3 = productAttornRecordMes.getTrbutton3().split(","); //用于存放年利率
            for(int i=0;i<trbutton3.length;i++){
                if(i == 0){
                    if(trbutton3[i].equals("1")){ //0.1万以下
                        sql = sql + "(PRICE < 1000)";
                    }else if(trbutton3[i].equals("2")){ //0.1万-5万
                        sql = sql + "(PRICE >= 1000 and PRICE < 50000)";
                    }else if(trbutton3[i].equals("3")){ //5万-10万
                        sql = sql + "(PRICE >= 50000 and PRICE < 100000)";
                    }else if(trbutton3[i].equals("4")){ //10万以上
                        sql = sql + "(PRICE >= 100000)";
                    }
                }else{
                    if(trbutton3[i].equals("1")){ //0.1万以下
                        sql = sql + "or (PRICE < 1000)";
                    }else if(trbutton3[i].equals("2")){ //0.1万-5万
                        sql = sql + "or (PRICE >= 1000 and PRICE < 50000)";
                    }else if(trbutton3[i].equals("3")){ //5万-10万
                        sql = sql + "or (PRICE >= 50000 and PRICE < 100000)";
                    }else if(trbutton3[i].equals("4")){ //10万以上
                        sql = sql + "or (PRICE >= 100000)";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 转让价格拼接End ****************/

        /*************** 剩余期限拼接Start ***************/
        if(productAttornRecordMes.getTrbutton4() != null && !productAttornRecordMes.getTrbutton4().equals("") && !productAttornRecordMes.getTrbutton4().equals("0")){
            sql = sql + " AND (";
            trbutton4 = productAttornRecordMes.getTrbutton4().split(","); //剩余期限
            for(int i=0;i<trbutton4.length;i++){
                if(i == 0){
                    if(trbutton4[i].equals("1")){ //1月以内
                        sql = sql + "(SURPLUSDAYS < 30)";
                    }else if(trbutton4[i].equals("2")){ //1-2个月
                        sql = sql + "(SURPLUSDAYS >= 30 and SURPLUSDAYS < 90)";
                    }else if(trbutton4[i].equals("3")){ //3-5个月
                        sql = sql + "(SURPLUSDAYS >= 90 and SURPLUSDAYS < 180)";
                    }else if(trbutton4[i].equals("4")){ //6-11个月
                        sql = sql + "(SURPLUSDAYS >= 180 and SURPLUSDAYS < 365)";
                    }else if(trbutton4[i].equals("5")){ //12个月以上
                        sql = sql + "(SURPLUSDAYS >= 365)";
                    }
                }else{
                    if(trbutton4[i].equals("1")){ //1月以内
                        sql = sql + "or (SURPLUSDAYS < 30)";
                    }else if(trbutton4[i].equals("2")){ //1-2个月
                        sql = sql + "or (SURPLUSDAYS >= 30 and SURPLUSDAYS < 90)";
                    }else if(trbutton4[i].equals("3")){ //3-5个月
                        sql = sql + "or (SURPLUSDAYS >= 90 and SURPLUSDAYS < 180)";
                    }else if(trbutton4[i].equals("4")){ //6-11个月
                        sql = sql + "or (SURPLUSDAYS >= 180 and SURPLUSDAYS < 365)";
                    }else if(trbutton4[i].equals("5")){ //12个月以上
                        sql = sql + "or (SURPLUSDAYS >= 365)";
                    }
                }
            }
            sql = sql + ")";
        }
        /**************** 剩余期限拼接End ****************/

        if(typeId != null && !typeId.equals("")){ //产品类型
            sql = sql + " AND TYPE = '"+typeId+"'";
        }
        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY BUYTYPE,UNDERTAKEMEMBER,"+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

}
