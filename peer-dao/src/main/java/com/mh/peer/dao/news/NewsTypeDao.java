package com.mh.peer.dao.news;

import com.mh.peer.model.entity.NewsType;
import com.mh.peer.model.message.NewsTypeMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zhangerxin on 2016/4/4.
 */
public interface NewsTypeDao {

    /**
     * 查询新闻类别
     * @param newsType
     * @return
     */
    List<NewsType> getNewsType(NewsType newsType);

    /**
     * 修改新闻类别
     * @param newsType
     * @return
     */
    DaoResult updateNewsType(NewsType newsType);

    /**
     * 获取所有一级新闻类别
     * @param newsTypeMes
     * @return
     */
    DMap getAllNewsFirstType(NewsTypeMessageBean newsTypeMes);

    /**
     * 根据一级类别获取所有二级新闻类别
     * @param newsTypeMes
     * @return
     */
    DMap getAllNewsSecondType(NewsTypeMessageBean newsTypeMes);
}
