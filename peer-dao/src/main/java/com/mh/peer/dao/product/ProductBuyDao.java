package com.mh.peer.dao.product;

import com.mh.peer.model.entity.ProductBuyInfo;
import com.mh.peer.model.message.ProductInvestMessageBean;
import com.mh.peer.model.message.ProductMyInvestMessageBean;
import com.mh.peer.model.message.ProductReceiveMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Administrator on 2016-5-9.
 */
public interface ProductBuyDao {

    /**
     * 投资产品
     * @param productBuyInfo
     * @return
     */
    DaoResult saveProductBuyInfo(ProductBuyInfo productBuyInfo);

    /**
     * 获取投资集合
     * @param productInvestMes
     * @return
     */
    DMap getAllListBySome(ProductInvestMessageBean productInvestMes);

    /**
     * 获取投资数量
     * @param productInvestMes
     * @return
     */
    DMap getProductInvestCount(ProductInvestMessageBean productInvestMes);

    /**
     * 根据条件获取回收数量
     * @param productReceiveMes
     * @return
     */
    DMap getReceiveCount(ProductReceiveMessageBean productReceiveMes);

    /**
     * 根据id获取投资信息(zhangerxin)
     * @param productMyInvestMes
     * @return
     */
    DMap getInvestInfoById(ProductMyInvestMessageBean productMyInvestMes);

    /**
     * 根据条件获取回款金额总和
     * @param productReceiveMes
     * @return
     */
    DMap getReceivePriceSum(ProductReceiveMessageBean productReceiveMes);

    /**
     * 昨天新增投资数量 by cuibin
     */
    public DMap queryLastDayMoney();

    /**
     * 最近七天新增投资数量 by cuibin
     */
    public DMap queryLastWeekMoney();

    /**
     * 最近30天新增投资数量 by cuibin
     */
    public DMap queryLastMonthMoney();

    /**
     * 昨天投资金额 by cuibin
     */
    public DMap queryLastDayInvest();

    /**
     * 最近七天新增投资金额 by cuibin
     */
    public DMap queryLastWeekInvest();


    /**
     * 过去30天新增投资 by cuibin
     */
    public DMap queryLastMonthInvest();

    /**
     *  按照日期查询投资数量by cuibin
     */
    public DMap queryInvestSumByDay(String startTime,String endTime);

    /**
     * 按照日期查询投资金额 by cuibin
     */
    public DMap queryInvestByDay(String startTime,String endTime);

    /**
     * 按照指定日期查询投资金额 by cuibin
     */
    public DMap queryInvestByDay(String time);

    /**
     *  按照指定日期查询投资数量by cuibin
     */
    public DMap queryInvestSumByDay(String time);

    /**
     * 获取投资项目(App部分)
     * @param memberId
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public DMap getInvestProject(String memberId,String typeId,String pageIndex,String pageSize);

    /**
     * 获取产品详细信息
     * @param productId
     * @return
     */
    DMap queryProductDetails(String productId);

    /**
     * 获取回款统计信息(已回款信息)
     * @param productBuyId
     * @return
     */
    DMap queryYReceiveInfo(String productBuyId);

    /**
     * 获取未还款统计信息
     * @param productBuyId
     * @return
     */
    DMap queryWReceiveInfo(String productBuyId);
}
