package com.mh.peer.dao.manager;

import com.salon.frame.data.DMap;

/**
 * Created by WangMeng on 2016/1/13.
 */
public interface MainDao {
    /**
     * 拿到子集菜单
     * @param roleId
     * @return
     */
    DMap getMeun(String userId, String roleId, String menuParentId);

    /**
     * 拿到顶级菜单
     * @return
     */
    DMap getTopMenu(String userId, String roleId);
}
