package com.mh.peer.dao.product.Impl;

import com.mh.peer.dao.product.ProductReceiveDao;
import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cuibin on 2016/5/25.
 */
@Repository
public class ProductReceiveDaoImpl implements ProductReceiveDao {
    @Autowired
    private BaseDao baseDao;

    /**
     * 项目详情 已收详情
     */
    @Override
    public DMap alreadyReceive(ProductReceiveMessageBean productReceiveMessageBean, String userId) {
        String sql = "select * from view_product_receive where 1 = 1 and SRECEIVETIME is not null";
        String productid = null;
        if(productReceiveMessageBean != null){
            productid = productReceiveMessageBean.getProductid();
        }
        if (userId != null && !userId.equals("")) {
            sql = sql + " and BUYER =  '" + userId + "'";
        }
        if (productid != null && !productid.equals("")) {
            sql = sql + " and PRODUCTID = '" + productid + "'";
        }
        return DaoTool.select(sql);
    }

    /**
     * 项目详情 未收详情
     */
    @Override
    public DMap notReceive(ProductReceiveMessageBean productReceiveMessageBean, String userId) {
        String sql = "select * from view_product_receive where 1 = 1 and SRECEIVETIME is null";
        String productid = null;
        if(productReceiveMessageBean != null){
            productid = productReceiveMessageBean.getProductid();
        }
        if (userId != null && !userId.equals("")) {
            sql = sql + " and BUYER =  '" + userId + "'";
        }
        if (productid != null && !productid.equals("")) {
            sql = sql + " and PRODUCTID = '" + productid + "'";
        }
        return DaoTool.select(sql);
    }


    /**
     * 回款信息详情(总金额)
     * @param productReceiveMessageBean
     * @return
     */
    @Override
    public Map getReceiveMessageBean(ProductReceiveMessageBean productReceiveMessageBean) {
        //已收金额查询语句
        String sqlYs = "SELECT COUNT(1)YSCOUNT,IFNULL(SUM(SUMPRICE),0)SHOULDPRICE,IFNULL(SUM(RECEIVEPRICE),0)YSPRICE,IFNULL(SUM(PRINCIPAL),0)YSPRINCIPAL" +
                " FROM view_product_receive WHERE 1 = 1";
        //待收金额查询语句
        String sqlDs = "SELECT COUNT(1)DSCOUNT,IFNULL(SUM(SUMPRICE),0)DSPRICE,IFNULL(SUM(PRINCIPAL),0)DSPRINCIPAL,IFNULL(SUM(INTEREST),0)DSINTEREST" +
                " FROM view_product_receive WHERE 1 = 1";

        String buyId = productReceiveMessageBean.getBuyid(); //购买id
        Double ysInterest = 0.0d; //已收利息
        Double managePrice = 0.0d; //管理费
        int ysCount = 0; //已收数量
        int dsCount = 0; //待收数量
        int countSum = 0; //总数量
        String receiveProgress = ""; //收款进度

        if(buyId != null && !buyId.equals("")){
            sqlYs = sqlYs + " AND BUYID = '"+buyId+"'";
            sqlDs = sqlDs + " AND BUYID = '"+buyId+"'";
        }
        sqlYs = sqlYs + " AND SRECEIVETIME IS NOT NULL";
        sqlDs = sqlDs + " AND SRECEIVETIME IS NULL";
        Map map = new HashMap();

        /******************** 已收金额部分Start ********************/
        DMap dMapYs = DaoTool.select(sqlYs);
        map.put("ysPrice",dMapYs.getValue("YSPRICE",0)); //已收总额
        map.put("ysPrincipal",dMapYs.getValue("YSPRINCIPAL",0)); //已收本金

        /***** 已收利息Start *****/
        ysInterest = TypeTool.getDouble(dMapYs.getValue("YSPRICE",0)) - TypeTool.getDouble(dMapYs.getValue("YSPRINCIPAL", 0));
        map.put("ysInterest",ysInterest.toString());
        /****** 已收利息End ******/

        /***** 管理费Start *****/
        managePrice = TypeTool.getDouble(dMapYs.getValue("SHOULDPRICE",0)) - TypeTool.getDouble(dMapYs.getValue("YSPRICE",0)); //管理费
        map.put("managePrice",managePrice.toString());
        /****** 管理费End ******/

        ysCount = TypeTool.getInt(dMapYs.getValue("YSCOUNT",0)); //已收数量
        /********************* 已收金额部分End *********************/

        /******************** 待收金额部分Start ********************/
        DMap dMapDs = DaoTool.select(sqlDs);
        map.put("dsPrice",dMapDs.getValue("DSPRICE",0)); //待收总额
        map.put("dsPrincipal",dMapDs.getValue("DSPRINCIPAL",0)); //待收本金
        map.put("dsInterest",dMapDs.getValue("DSINTEREST",0)); //待收利息

        dsCount = TypeTool.getInt(dMapYs.getValue("DSCOUNT",0)); //待收数量
        /********************* 待收金额部分End *********************/

        countSum = ysCount + dsCount; //总数量
        if(ysCount == countSum){ //总数量
            receiveProgress = "已回款(" + ysCount + "/" + countSum + ")";
        }else{
            receiveProgress = "回款中(" + ysCount + "/" + countSum + ")";
        }
        map.put("receiveProgress",receiveProgress); //收款进度
        return map;
    }

    /**
     * 根据条件获取回款部分详情
     * @param productReceiveMes
     * @return
     */
    @Override
    public DMap getProductReceiveListBySome(ProductReceiveMessageBean productReceiveMes) {
        String pageIndex = productReceiveMes.getPageIndex(); //当前数
        String pageSize = productReceiveMes.getPageSize(); //每次显示数量
        String orderColumn = productReceiveMes.getOrderColumn(); //排序字段
        String orderType = productReceiveMes.getOrderType(); //排序类型
        String buyId = productReceiveMes.getBuyid(); //购买id
        String sql = "select * from view_product_receive where 1 = 1"; //查询条件

        if(buyId != null && !buyId.equals("")){ //购买id
            sql = sql + " and BUYID = '"+buyId+"'";
        }
        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " order by "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " limit "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }

        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 项目详情 期数详情
     */
    @Override
    public DMap phaseDetails(ProductMessageBean productMessageBean, String userId) {
        String sql = "select * from view_product_receive where 1 = 1";
        String pageIndex = productMessageBean.getPageIndex();
        String pageSize = productMessageBean.getPageSize();

        if (userId != null && !userId.equals("")) {
            sql = sql + " and BUYER = '" + userId + "'";
        }

        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (Integer.parseInt(pageIndex) - 1) + "," + Integer.parseInt(pageSize) + "";
        }

        return DaoTool.select(sql);
    }

    /**
     * 投资总额
     */
    @Override
    public DMap investTotal(String userId) {
        if (userId == null || userId.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select ifnull(sum(ifnull(PRICE , 0)),0) as investTotal from view_product_buy where BUYER = '" + userId + "'");
    }

    /**
     * 投标中本金
     */
    @Override
    public DMap inaBid(String userId) {
        if (userId == null || userId.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        DMap dMap = DaoTool.select("select ifnull(sum(ifnull(PRINCIPAL,0)),0) as biding from view_product_receive where BUYER = '" + userId + "'" +
                " and FULLSCALE = '1'");
        //System.out.println(dMap.getErrText());
        return dMap;
    }


    /**
     * 待回收本金
     */
    @Override
    public DMap toBeRecycled(String userId) {
        if (userId == null || userId.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }

        return DaoTool.select("select ifnull(sum(ifnull(PRINCIPAL,0)),0) as toBeRecycled from view_product_receive where BUYER = '" + userId + "'" +
                " and SRECEIVETIME is null");
    }

    /**
     * 已回收本金
     */
    @Override
    public DMap retired(String userId) {
        if (userId == null || userId.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select ifnull(sum(ifnull(RECEIVEPRICE,0)),0) as retired from view_product_receive where BUYER = '" + userId + "'" +
                " and SRECEIVETIME is not null");
    }

    /**
     * 待回收金额 本金 利息
     */
    @Override
    public DMap moneyRecycled(String userId) {
        if (userId == null || userId.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select ifnull(sum(ifnull(PRINCIPAL,0)),0) as principalRecycled,ifnull(sum(ifnull(INTEREST,0)),0) as interestRecycled," +
                "ifnull(sum(ifnull(SUMPRICE,0)),0) as moneyRecycled" +
                " from view_product_receive where BUYER ='" + userId + "' and SRECEIVETIME is null");
    }

    /**
     * 平均受益率
     */
    @Override
    public DMap avgBenefit(String userId) {
        if (userId == null || userId.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("SELECT AVG(LOANRATE) as avgBenefit FROM view_product_buy where BUYER = '" + userId + "' GROUP BY BUYER ");
    }

    /**
     * 获取昨日收益(App端)
     * @param appHomeMes
     * @return
     */
    @Override
    public DMap getLastDayPrice(AppHomeMessageBean appHomeMes) {
        String sql = "SELECT IFNULL(SUM(IFNULL(RECEIVEPRICE,0)),0)RECEIVEPRICESUM FROM view_product_receive_min WHERE 1 = 1";
        String memberId = appHomeMes.getMemberId(); //会员id
        DMap dMap = new DMap();
        if (memberId != null && !memberId.equals("")){
            sql = sql + " AND BUYER = '"+memberId+"' AND DATE(SRECEIVETIME) = DATE(adddate(NOW(),-1))";
            dMap = DaoTool.select(sql);
        }
        return dMap;
    }

    /**
     * 获取待收(已收)金额、待收(已收)期数、待收(已收)投资数量(App端)
     * @param memberId
     * @param receiveFlag
     * @return
     */
    @Override
    public AppProductReceiveTjMessageBean getAppReceiveInfo(String memberId, String receiveFlag) {
        String sqlReceiveInfo = "SELECT IFNULL((SUM(PRINCIPAL)+SUM(INTEREST)),0)PRICE,COUNT(1)PERIOD FROM view_product_receive_app WHERE 1 = 1";
        String sqlInvestCount = "SELECT COUNT(1)INVESTCOUNT FROM view_product_buy_app WHERE 1 = 1";
        AppProductReceiveTjMessageBean appProductReceiveTjMessageBean = new AppProductReceiveTjMessageBean();

        if (memberId != null && !memberId.equals("")){ //会员id
            sqlReceiveInfo = sqlReceiveInfo + " AND BUYER = '"+memberId+"'";
            sqlInvestCount = sqlInvestCount + " AND BUYER = '"+memberId+"'";
        }

        if (receiveFlag != null && !receiveFlag.equals("")){ //回收状态(0-待收、1-已收)
            if (receiveFlag.equals("0")){ //待收
                sqlReceiveInfo = sqlReceiveInfo + " AND SRECEIVETIME IS NULL";
                sqlInvestCount = sqlInvestCount + " AND (INVESTFLAG = '0' OR INVESTFLAG = '1')";
                DMap dMapReceiveInfo = DaoTool.select(sqlReceiveInfo);
                DMap dMapInvestCount = DaoTool.select(sqlInvestCount);
                appProductReceiveTjMessageBean.setDsPrice(dMapReceiveInfo.getValue("PRICE",0)); //待收金额
                appProductReceiveTjMessageBean.setDsPeriod(dMapReceiveInfo.getValue("PERIOD",0)); //待收期数
                appProductReceiveTjMessageBean.setDsInvestCount(dMapInvestCount.getValue("INVESTCOUNT",0)); //待收投资数目
            }else if (receiveFlag.equals("1")){ //已收
                sqlReceiveInfo = sqlReceiveInfo + " AND SRECEIVETIME IS NOT NULL";
                sqlInvestCount = sqlInvestCount + " AND INVESTFLAG = '2'";
                DMap dMapReceiveInfo = DaoTool.select(sqlReceiveInfo);
                DMap dMapInvestCount = DaoTool.select(sqlInvestCount);
                appProductReceiveTjMessageBean.setYsPrice(dMapReceiveInfo.getValue("PRICE",0)); //已收金额
                appProductReceiveTjMessageBean.setYsPeriod(dMapReceiveInfo.getValue("PERIOD",0)); //已收期数
                appProductReceiveTjMessageBean.setYsInvestCount(dMapInvestCount.getValue("INVESTCOUNT",0)); //已收投资数目
            }
        }
        return appProductReceiveTjMessageBean;
    }


    /**
     * 获取待收(已收)列表(App端)
     * @param memberId
     * @param receiveFlag
     * @return
     */
    @Override
    public DMap getAppReceiveList(String memberId,String receiveFlag,String pageIndex,String pageSize) {
        String sql = "SELECT * FROM view_product_receive_app WHERE 1 = 1";
        if (memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND BUYER = '"+memberId+"'";
        }
        if (receiveFlag != null && !receiveFlag.equals("")){
            if (receiveFlag.equals("0")){ //待收
                sql = sql + " AND SRECEIVETIME IS NULL";
            }else if (receiveFlag.equals("1")){ //已收
                sql = sql + " AND SRECEIVETIME IS NOT NULL";
            }
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 根据id获取回款信息
     * @param receiveId
     * @return
     */
    @Override
    public DMap getAppReceiveInfoById(String receiveId) {
        String sql = "SELECT * FROM view_product_receive_app WHERE 1 = 1";
        if (receiveId != null && !receiveId.equals("")){ //主键
            sql = sql + " AND ID = '"+receiveId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 查询自动回款信息
     * @param productAutoRepayMes
     * @return
     */
    @Override
    public DMap getReceiveInfoList(ProductAutoRepayMessageBean productAutoRepayMes) {
        String sql = "SELECT * FROM view_product_autoRepay WHERE 1 = 1";
        String yreceiveTime = productAutoRepayMes.getYreceiveTime(); //应回款日期
        if (yreceiveTime != null && !yreceiveTime.equals("")){
            sql = sql + " AND DATE(YRECEIVETIME) = DATE("+yreceiveTime+")";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
