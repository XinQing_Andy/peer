package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.HuanxunRegproject;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.ProductHuanXunMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-27.
 */
public interface HuanXunRegProjectDao {

    /**
     * 项目登记部分(环迅)
     * @param mapException
     * @param huanxunRegProject
     * @param memberMessage
     * @param tradeDetail
     * @return
     */
    public DaoResult regProjecthxResult(Map<String,Object> mapException,HuanxunRegproject huanxunRegProject, MemberMessage memberMessage, TradeDetail tradeDetail);

    /**
     * 根据条件获取登记项目信息
     * @param productHuanXunMessageBean
     * @return
     */
    public DMap getRegProjectInfoList(ProductHuanXunMessageBean productHuanXunMessageBean);
}
