package com.mh.peer.dao.product.Impl;

import com.mh.peer.dao.product.ProductRepayDao;
import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-5-27.
 * 还款列表
 */
@Repository
public class ProductRepayDaoImpl implements ProductRepayDao {

    /**
     * 获取所有还款列表
     * @param productRepayMes
     * @return
     */
    @Override
    public DMap getRepayListBySome(ProductRepayMessageBean productRepayMes) {
        String sql = "SELECT * FROM view_product_repay WHERE 1 = 1"; //还款列表查询条件
        String productId = productRepayMes.getProductId(); //产品id
        String pageIndex = productRepayMes.getPageIndex(); //当前页
        String pageSize = productRepayMes.getPageSize(); //每页显示数量
        String orderColumn = productRepayMes.getOrderColumn(); //排序字段
        String orderType = productRepayMes.getOrderType(); //排序类型

        if(productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex) - 1)+","+TypeTool.getInt(pageSize)+"";
        }

        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取还款部分数量
     * @param productRepayMes
     * @return
     */
    @Override
    public DMap getRepayCount(ProductRepayMessageBean productRepayMes) {
        String sql = "SELECT * FROM view_product_repay WHERE 1 = 1";
        String productId = productRepayMes.getProductId(); //产品id
        String pageIndex = productRepayMes.getPageIndex(); //当前页
        String pageSize = productRepayMes.getPageSize(); //每页显示数量
        String orderColumn = productRepayMes.getOrderColumn(); //排序字段
        String orderType = productRepayMes.getOrderType(); //排序类型
        String applyMemberId = productRepayMes.getApplyMember(); //申请人id

        if(productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        if (applyMemberId != null && !applyMemberId.equals("")){ //申请人id
            sql = sql + " AND APPLYMEMBER = '"+applyMemberId+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 获取余额
     * @param productRepayMes
     * @return
     */
    @Override
    public DMap getRepayPriceSum(ProductRepayMessageBean productRepayMes) {
        String sql = "SELECT IFNULL(SUM(IFNULL(PRINCIPAL,0)),0)PRINCIPALSUM,IFNULL(SUM(IFNULL(INTEREST,0)),0)INTERESTSUM," +
                "IFNULL(SUM(IFNULL(YPRICE,0)),0)YPRICESUM FROM view_product_repay WHERE 1 = 1";
        String productId = productRepayMes.getProductId(); //产品id

        if(productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 待还总额 & 到期还款金额 by cuibin
     */
    @Override
    public DMap getRepayedPrice(ProductRepayMessageBean productRepayMessageBean, String repayFlg) {
        String sql = "";
        if (repayFlg.equals("0")) { //到期还款金额
            sql = "SELECT SUM(IFNULL(REPAYMENTPRICE, 0)) as zs FROM view_product_repay WHERE SREPAYMENTTIME IS NOT NULL AND DATE(SREPAYMENTTIME) < DATE(NOW())";
        } else if (repayFlg.equals("1")) { //待还金额
            sql = "SELECT SUM(IFNULL(YPRICE,0)) as zs FROM view_product_repay WHERE SREPAYMENTTIME IS NULL";
        }
        return DaoTool.select(sql);
    }

    /**
     * 获取待还(已还)金额、待还(已还)期数(App端)
     * @param memberId
     * @param repayFlag
     * @return
     */
    @Override
    public AppProductRepayTjMessageBean getAppRepayInfo(String memberId, String repayFlag) {
        String sqlRepayTj = "SELECT SUM(REPAYPRICE)REPAYPRICESUM,COUNT(1)PERIODSUM FROM view_product_repay_app WHERE 1 = 1";
        String sqlRepayProduct = "SELECT COUNT(1)PRODUCTCOUNT FROM view_product_content_app WHERE 1 = 1";
        AppProductRepayTjMessageBean appProductRepayTjMessageBean = new AppProductRepayTjMessageBean();
        if (memberId != null && !memberId.equals("")){ //会员id
            sqlRepayTj = sqlRepayTj + " AND APPLYMEMBER = '"+memberId+"'";
            sqlRepayProduct = sqlRepayProduct + " AND APPLYMEMBER = '"+memberId+"'";
        }
        if (repayFlag != null && !repayFlag.equals("")){ //还款标致(0-待还、1-已还)
            if (repayFlag.equals("0")){ //待还
                sqlRepayTj = sqlRepayTj + " AND SREPAYMENTTIME IS NULL";
                sqlRepayProduct = sqlRepayProduct + " AND REPAYFLAG = '0' OR REPAYFLAG = '1'";
                DMap dMapRepayTj = DaoTool.select(sqlRepayTj); //还款金额、还款期数
                DMap dMapRepayProduct = DaoTool.select(sqlRepayProduct); //还款产品数量
                if (dMapRepayTj != null){
                    appProductRepayTjMessageBean.setDhPrice(dMapRepayTj.getValue("REPAYPRICESUM",0)); //待还款金额
                    appProductRepayTjMessageBean.setDhPeriod(dMapRepayTj.getValue("PERIODSUM",0)); //待还款期数
                }
                if (dMapRepayProduct != null){
                    appProductRepayTjMessageBean.setDhRepayCount(dMapRepayProduct.getValue("PRODUCTCOUNT",0)); //待还产品数量
                }
            }else if (repayFlag.equals("1")){ //已还
                sqlRepayTj = sqlRepayTj + " AND SREPAYMENTTIME IS NOT NULL";
                sqlRepayProduct = sqlRepayProduct + " AND REPAYFLAG = '2'";
                DMap dMapRepayTj = DaoTool.select(sqlRepayTj); //还款金额、还款期数
                DMap dMapRepayProduct = DaoTool.select(sqlRepayProduct); //还款产品数量
                if (dMapRepayTj != null){
                    appProductRepayTjMessageBean.setYhPrice(dMapRepayTj.getValue("REPAYPRICESUM", 0)); //待还款金额
                    appProductRepayTjMessageBean.setYhPeriod(dMapRepayTj.getValue("PERIODSUM", 0)); //待还款期数
                }
                if (dMapRepayProduct != null){
                    appProductRepayTjMessageBean.setYhRepayCount(dMapRepayProduct.getValue("PRODUCTCOUNT", 0)); //待还产品数量
                }
            }
        }
        return appProductRepayTjMessageBean;
    }


    /**
     * 获取待还(已还)列表(App端)
     * @param memberId
     * @param repayFlag
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public DMap getAppRepayList(String memberId, String repayFlag, String pageIndex, String pageSize) {
        String sql = "SELECT * FROM view_product_repay_app WHERE 1 = 1";
        if (memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND APPLYMEMBER = '"+memberId+"'";
        }
        if (repayFlag != null && !repayFlag.equals("")){ //还款状态
            if (repayFlag.equals("0")){ //待还
                sql = sql + " AND SREPAYMENTTIME IS NULL";
            }else if (repayFlag.equals("1")){ //已还
                sql = sql + " AND SREPAYMENTTIME IS NOT NULL";
            }
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取产品还款详细信息
     * @param productRepayId
     * @return
     */
    @Override
    public DMap getAppRepayDetails(String productRepayId) {
        String sql = "SELECT * FROM view_product_repay_app WHERE 1 = 1";
        if (productRepayId != null && !productRepayId.equals("")){ //还款id
            sql = sql + " AND ID = '"+productRepayId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取逾期项目集合
     * @param overRepayMes
     * @return
     */
    @Override
    public DMap getOverDueRepayList(ProductOverRepayMessageBean overRepayMes) {
        String sql = "SELECT * FROM view_product_repay WHERE 1 = 1 AND DATE(YREPAYMENTTIME) < DATE(NOW())";
        String loanman = overRepayMes.getLoanman(); //借款人
        String idcardNumber = overRepayMes.getIdcardNumber(); //身份证号
        String productTitle = overRepayMes.getProductTitle(); //名称
        String startTime = overRepayMes.getStartTime(); //开始时间
        String endTime = overRepayMes.getEndTime(); //结束时间
        String pageIndex = overRepayMes.getPageIndex(); //当前数
        String pageSize = overRepayMes.getPageSize(); //每页显示数量

        if (loanman != null && !loanman.equals("") ){ //借款人
            sql = sql + " and realname = '"+loanman+"'";
        }
        if (idcardNumber != null && !idcardNumber.equals("") ){ //身份证号
            sql = sql + " and idcard = '"+idcardNumber+"'";
        }
        if (productTitle != null && !productTitle.equals("") ){ //名称
            sql = sql + " and title = '"+productTitle+"'";
        }
        if (startTime != null && !startTime.equals("")) { //开始时间
            sql = sql + " and DATE(YREPAYMENTTIME) >= DATE('" + startTime + "')";
        }
        if (endTime != null && !endTime.equals("")) { //结束时间
            sql = sql + " and DATE(YREPAYMENTTIME) <= DATE('" + endTime + "')";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 获取产品数量(后台)
     *
     * @param overRepayMes
     * @return
     */
    @Override
    public DMap getOverRepayByPage(ProductOverRepayMessageBean overRepayMes) {
        String sql = "SELECT count(*) FROM view_product_repay WHERE 1 = 1";
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取还款明细列表集合
     * @param productRepayMes
     * @return
     */
    @Override
    public DMap getRepayDetail(ProductRepayMessageBean productRepayMes) {
        String sql = "SELECT * FROM view_product_repay WHERE 1 = 1";
        String keyword = productRepayMes.getKeyword();
        String keyWordDate = productRepayMes.getKeyWordDate();
        String value = productRepayMes.getValue();
        String productTitle = productRepayMes.getProductTitle(); //题目
        String applyMember = productRepayMes.getApplyMember(); //申请人
        String productId = productRepayMes.getProductId(); //产品id
        String startTime = productRepayMes.getStartTime(); //开始时间
        String endTime = productRepayMes.getEndTime(); //结束时间
        String pageIndex = productRepayMes.getPageIndex(); //当前数
        String pageSize = productRepayMes.getPageSize(); //每页显示数量

        if (value != null && !value.equals("") && "1".equals(keyword)){ //题目
            sql = sql + " and TITLE LIKE '%"+value+"%'";
        }
        if (value != null && !value.equals("") && "2".equals(keyword)){ //编码
            sql = sql + " and CODE LIKE '%"+value+"%'";
        }

        if (applyMember != null && !applyMember.equals("")){ //申请人(还款人)
            sql = sql + " AND APPLYMEMBER = '"+applyMember+"'";
        }

        if (startTime != null && !startTime.equals("") && "1".equals(keyWordDate)) {
            sql = sql + " and YREPAYMENTTIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("") && "1".equals(keyWordDate)) {
            sql = sql + " and YREPAYMENTTIME <= '" + endTime + "'";
        }

        if (startTime != null && !startTime.equals("") && "2".equals(keyWordDate)) {
            sql = sql + " and SREPAYMENTTIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("") && "2".equals(keyWordDate)) {
            sql = sql + " and SREPAYMENTTIME <= '" + endTime + "'";
        }

        sql = sql + " ORDER BY YREPAYMENTTIME ASC";
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取环迅部分应还款信息
     * @param repayId
     * @return
     */
    @Override
    public DMap getRepayInfoById(String repayId) {
        String sql = "SELECT * FROM view_product_repay_huanxun WHERE ID = '"+repayId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 获取还款中产品列表集合
     * @param productRepayMes
     * @return
     */
    @Override
    public DMap getRepayingList(ProductRepayMessageBean productRepayMes) {
        String sql = "SELECT * FROM view_product_content WHERE REPAYFLG = '0'";
        String productTitle = productRepayMes.getProductTitle(); //题目
        String productCode = productRepayMes.getProductCode(); //产品编号
        String startTime = productRepayMes.getStartTime(); //开始时间
        String endTime = productRepayMes.getEndTime(); //结束时间
        String pageIndex = productRepayMes.getPageIndex(); //当前数
        String pageSize = productRepayMes.getPageSize(); //每页显示数量

        if (productTitle != null && !productTitle.equals("")){
            sql = sql + " and title like '%"+productTitle+"%'";
        }
        if (productCode != null && !productCode.equals("")){
            sql = sql + " and CODE = '"+productCode+"'";
        }
        if (startTime != null && !startTime.equals("")) {
            sql = sql + " and YREPAYMENTTIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")){
            sql = sql + " and YREPAYMENTTIME <= '" + endTime + "'";
        }
        sql = sql + " ORDER BY CREATETIME DESC";
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 获取还款中产品列表集合数量(后台)
     *
     * @param repayingMes
     * @return
     */
    @Override
    public DMap getRepayingByPage(ProductRepayMessageBean repayingMes) {
        String sql = "SELECT count(*) FROM view_product_content WHERE REPAYFLG = '0' ";
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 获取已结清产品列表集合
     * @param productRepayMes
     * @return
     */
    @Override
    public DMap getFinishList(ProductRepayMessageBean productRepayMes) {
        String sql = "SELECT * FROM view_product_content WHERE REPAYFLG = '2'";
        String productTitle = productRepayMes.getProductTitle(); //名称
        String productCode = productRepayMes.getProductCode(); //产品编号
        String realName = productRepayMes.getRealName();
        String pageIndex = productRepayMes.getPageIndex(); //当前数
        String pageSize = productRepayMes.getPageSize(); //每页显示数量

        if (productTitle != null && !productTitle.equals("")){
            sql = sql + " AND TITLE LIKE '%"+productTitle+"%'";
        }
        if (productCode != null && !productCode.equals("")){
            sql = sql + " AND CODE LIKE '%"+productCode+"%'";
        }
        if (realName != null && !realName.equals("")){
            sql = sql + " AND REALNAME = '"+realName+"'";
        }
        sql = sql + " ORDER BY CREATETIME DESC";
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 获取已结清产品列表集合数量(后台)
     *
     * @param repayingMes
     * @return
     */
    @Override
    public DMap getFinishByPage(ProductRepayMessageBean repayingMes) {
        String sql = "SELECT count(*) FROM view_product_content WHERE REPAYFLG = '0' ";
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
}
