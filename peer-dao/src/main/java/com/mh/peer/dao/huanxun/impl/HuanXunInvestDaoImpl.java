package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunInvestDao;
import com.mh.peer.exception.HuanXunFreezeInvestException;
import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-28.
 * 解冻部分
 */
@Repository
public class HuanXunInvestDaoImpl implements HuanXunInvestDao{

    private static final Logger LOGGER = LoggerFactory.getLogger(HuanXunInvestDaoImpl.class);
    @Autowired
    private BaseDao baseDao;

    /**
     * 产品投资
     * @param huanxunFreeze
     * @param productBuyInfo
     * @param tradeDetail
     * @param memberMessage
     * @return
     */
    @Override
    public DaoResult investhxResult(Map<String,Object> mapException,HuanxunFreeze huanxunFreeze, ProductBuyInfo productBuyInfo, TradeDetail tradeDetail, MemberMessage memberMessage) {
        DConnection conn = DaoTool.getConnection();
        String buyId = huanxunFreeze.getId(); //产品购买id
        String productId = productBuyInfo.getProductid(); //产品id
        int serial_Number = 0; //交易流水号
        String memberId = ""; //会员id
        Double ipstrdamt = huanxunFreeze.getIpstrdamt(); //冻结金额
        Double loadFee = 0.0d; //借款金额
        Double investSum = 0.0d; //投资金额
        Double balance = 0.0d; //余额(Double型)
        String resultCode = ""; //响应吗(000000-成功、000001~999999-失败)
        String sqlHuanxunFreeze = "SELECT * FROM huanxun_freeze WHERE ID = '"+buyId+"'";
        DaoResult daoResultHuanxunFreeze = new DaoResult(); //环迅资金冻结
        DaoResult daoResultBuyInfo = new DaoResult(); //产品投资部分
        DaoResult daoResultProduct = new DaoResult(); //产品部分
        DaoResult daoResultMyMessage = new DaoResult(); //我的消息部分
        DaoResult daoResultTradeDetail = new DaoResult(); //交易记录部分
        DaoResult daoResultSerial = new DaoResult(); //流水号
        DaoResult daoResultMember = new DaoResult(); //会员

        try{

            memberId = huanxunFreeze.getMemberid(); //会员id
            balance = tradeDetail.getBalance(); //余额

            DMap dMapHuanxunFreeze = DaoTool.select(sqlHuanxunFreeze); //环迅资金冻结查询
            if (dMapHuanxunFreeze.getCount() == 0){
                daoResultHuanxunFreeze = baseDao.insert(huanxunFreeze,conn); //环迅资金冻结录入
                if (daoResultHuanxunFreeze.getCode() < 0){
                    HuanXunFreezeInvestException.handle(mapException); //投资冻结资金部分处理异常
                    conn.rollback();
                }else{
                    resultCode = huanxunFreeze.getResultcode(); //响应码
                    LOGGER.info("resultCode======"+resultCode);
                    if (resultCode != null && !resultCode.equals("")){
                        if (resultCode.equals("000000")){ //成功
                            daoResultBuyInfo = baseDao.insert(productBuyInfo,conn); //产品投资部分录入
                            if (daoResultBuyInfo.getCode() < 0){
                                HuanXunFreezeInvestException.handle(mapException); //投资冻结资金部分处理异常
                                conn.rollback();
                            }else{
                                String sqlInvestSum = "SELECT LOADFEE,YINVESTFEE FROM view_product_content WHERE ID = '"+productId+"'";
                                DMap dMapProduct = DaoTool.select(sqlInvestSum); //求投资金额总和
                                if (dMapProduct != null){
                                    loadFee = TypeTool.getDouble(dMapProduct.getValue("LOADFEE",0)); //借款金额
                                    investSum = TypeTool.getDouble(dMapProduct.getValue("YINVESTFEE",0)); //已投资金额
                                    if (loadFee == (investSum + ipstrdamt)){ //满标
                                        String sqlProductUpdate = "UPDATE product_content SET FULLSCALE = '0',FULLTIME = NOW() WHERE ID = <productId>";
                                        DMap dMapProductUpdate = new DMap();
                                        dMapProductUpdate.setData("productId",productId); //产品id
                                        daoResultProduct = DaoTool.update(sqlProductUpdate,dMapProductUpdate.getData(),conn);
                                        if (daoResultProduct.getCode() < 0){
                                            HuanXunFreezeInvestException.handle(mapException); //投资冻结资金部分处理异常
                                            conn.rollback();
                                        }
                                    }
                                }

                                daoResultMyMessage = baseDao.insert(memberMessage,conn); //我的消息录入
                                if (daoResultMyMessage.getCode() < 0){
                                    HuanXunFreezeInvestException.handle(mapException); //投资冻结资金部分处理异常
                                    conn.rollback();
                                }else{
                                    String sqlMemberUpdate = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
                                    DMap dMapMember = new DMap();
                                    dMapMember.setData("balance",balance); //余额
                                    dMapMember.setData("memberId",memberId); //会员id
                                    daoResultMember = DaoTool.update(sqlMemberUpdate,dMapMember.getData(),conn); //更新余额
                                    if (daoResultMember.getCode() < 0){
                                        HuanXunFreezeInvestException.handle(mapException); //投资冻结资金部分处理异常
                                        conn.rollback();
                                    }else{
                                        daoResultTradeDetail = baseDao.insert(tradeDetail,conn); //交易记录录入
                                        if (daoResultTradeDetail.getCode() < 0){
                                            HuanXunFreezeInvestException.handle(mapException); //投资冻结资金部分处理异常
                                            conn.rollback();
                                        }else{
                                            /********** 更新流水号Start **********/
                                            serial_Number = tradeDetail.getSerialnum(); //流水号
                                            String sqlSerialNumber = "UPDATE serialNumber_wh SET NUMBER = <serial_Number> WHERE ID = '6d82cfe3347511e69b6d00163e0063ab'";
                                            DMap dmapSerial = new DMap();
                                            dmapSerial.setData("serial_Number",serial_Number);
                                            daoResultSerial = DaoTool.update(sqlSerialNumber,dmapSerial.getData(),conn); //更新流水号
                                            if (daoResultSerial.getCode() < 0){
                                                HuanXunFreezeInvestException.handle(mapException); //投资冻结资金部分处理异常
                                                conn.rollback();
                                            }
                                            /*********** 更新流水号End ***********/
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            conn.rollback();
        }finally {
            conn.commit();
            conn.close();
        }
        return daoResultSerial;
    }


    /**
     * 获取环迅投资列表信息
     * @param huanXunFreezeMessageBean
     * @return
     */
    @Override
    public DMap getHuanXunInvestBySome(HuanXunFreezeMessageBean huanXunFreezeMessageBean) {
        String sql = "SELECT * FROM view_huanxun_freeze_buy WHERE 1 = 1";
        String productId = huanXunFreezeMessageBean.getProductId(); //产品id
        String ipsBillNo = huanXunFreezeMessageBean.getIpsBillNo(); //IPS订单号
        if (productId != null && !productId.equals("")){ //产品id
            sql = sql + " AND PRODUCTID = '"+productId+"'";
        }
        if (ipsBillNo != null && !ipsBillNo.equals("")){ //IPS订单号
            sql = sql + " AND IPSBILLNO = '"+ipsBillNo+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 解冻部分(投资)
     * @param memberInfo
     * @param huanxunFreeze
     * @return
     */
    @Override
    public DaoResult unfreezeResult(Map<String,Object> mapException,MemberInfo memberInfo,HuanxunFreeze huanxunFreeze) {
        DConnection conn = DaoTool.getConnection();
        String sqlMemberInfo = ""; //更新人员信息
        String sqlFreeze = ""; //更新冻结信息
        String sqlUnFreezeBf = ""; //更新解冻备份表状态
        DaoResult daoResultMemberInfo = new DaoResult(); //会员信息
        DaoResult daoResultFreeze = new DaoResult(); //冻结信息
        DaoResult daoResultUnFreezeBf = new DaoResult(); //解冻备份表信息
        String memberId = memberInfo.getId(); //会员id
        Double balance = memberInfo.getBalance(); //余额
        String flag = huanxunFreeze.getFlag(); //状态(0-冻结、1-解冻、2-转账)
        String id = huanxunFreeze.getId(); //冻结表主键
        String merBillNo = huanxunFreeze.getMerbillno(); //商户订单号
        String flagUnFreeze = "0"; //状态(0-解冻成功、1-解冻失败、2-待解冻)

        try{

            sqlMemberInfo = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
            DMap dMapMemberInfo = new DMap();
            dMapMemberInfo.setData("balance",balance); //余额
            dMapMemberInfo.setData("memberId",memberId); //会员id
            daoResultMemberInfo = DaoTool.update(sqlMemberInfo,dMapMemberInfo.getData(),conn); //更新会员信息
            if (daoResultMemberInfo.getCode() < 0){
                System.out.println("更新会员信息余额失败");
                HuanXunFreezeInvestException.handle(mapException); //处理异常信息
                conn.rollback();
            }else{
                System.out.println("更新会员信息余额成功");
                sqlFreeze = "UPDATE huanxun_freeze SET FLAG = <flag> WHERE ID = <id>"; //更新冻结信息
                DMap dMapFreeze = new DMap();
                dMapFreeze.setData("flag",flag); //状态
                dMapFreeze.setData("id",id); //主键
                daoResultFreeze = DaoTool.update(sqlFreeze,dMapFreeze.getData(),conn);
                if (daoResultFreeze.getCode() < 0){
                    System.out.println("更新冻结状态失败");
                    HuanXunFreezeInvestException.handle(mapException); //处理异常信息
                    conn.rollback();
                }else{
                    System.out.println("更新冻结状态成功");
                    sqlUnFreezeBf = "UPDATE huanxun_unFreeze_bf SET FLAG = <flagUnFreeze> WHERE MERBILLNO = <merBillNo>";
                    DMap dMapUnFreezeBf = new DMap();
                    dMapUnFreezeBf.setData("flagUnFreeze",flagUnFreeze); //状态(0-解冻成功、1-解冻失败、2-待解冻)
                    dMapUnFreezeBf.setData("merBillNo",merBillNo); //商户订单号
                    daoResultUnFreezeBf = DaoTool.update(sqlUnFreezeBf,dMapUnFreezeBf.getData(),conn);
                    if (daoResultUnFreezeBf.getCode() < 0){
                        System.out.println("更新冻结备份表状态失败");
                        HuanXunFreezeInvestException.handle(mapException); //处理异常信息
                        conn.rollback();
                    }else{
                        System.out.println("更新冻结备份表状态成功");
                    }
                }
            }
        }catch(Exception e){
            HuanXunFreezeInvestException.handle(mapException); //处理异常信息
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }

        return daoResultFreeze;
    }
}
