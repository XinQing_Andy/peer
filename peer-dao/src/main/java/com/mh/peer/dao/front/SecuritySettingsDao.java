package com.mh.peer.dao.front;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberLinkman;
import com.mh.peer.model.entity.MemberSetupQuestions;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.model.message.SysProvinceMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by cuibin on 2016/5/4.
 */
public interface SecuritySettingsDao {
    /**
     * 验证旧密码是否存在
     */
    public DMap checkOldPassword(String userId);
    /**
     * 更新密码
     */
    public DaoResult updatePassWord(MemberInfo memberInfo);
    /**
     * 保存紧急联系人
     */
    public DaoResult saveContact(MemberLinkman memberLinkman);
    /**
     * 查询一个紧急联系人
     */
    public DMap queryOnlyContact(String memberId);
    /**
     * 保存邮箱
     */
    public DaoResult saveEmail(MemberInfo memberInfo);
    /**
     * 保存邮箱验证标识
     */
    public DaoResult saveEmailMessage(MemberInfo memberInfo);
    /**
     * 更改验证状态
     */
    public DaoResult updateEmailMessage(MemberInfo memberInfo);
    /**
     * 验证邮箱标识
     */
    public DMap checkEmailMessage(String message, String id);
    /**
     * 验证是否认证成功
     */
    public DMap checkEmailIsSuccess(String email);
    /**
     * 查询一个问题（所有问题表）
     */
    public DMap queryOnlyquestion(int id);
    /**
     * 查询是否设置安全保护问题
     */
    public DMap queryIsquestion(String userId);
    /**
     * 保存问题
     */
    public DaoResult saveQuestion(MemberSetupQuestions memberSetupQuestions);
    /**
     * 保存交易密码
     */
    public DaoResult saveTransactionPwd(MemberInfo memberInfo);

    /**
     * 获取一个问题提问和答案
     */
    public DMap getOneQuestion(String userId);
    /**
     * 查询一个省
     */
    public DMap queryProvinceById(String id);

    /**
     *查询一个市
     */
    public DMap queryCityById(String id);

    /**
     * 忘记密码 重置密码
     */
    public DaoResult reSetPw(MemberInfo memberInfo);

    /**
     * 验证是否有此手机号
     */
    public DMap checkIsTelephone(String telephone);

    /**
     * 更新紧急联系人
     */
    public DaoResult updateContact(MemberLinkman memberLinkman);

}
