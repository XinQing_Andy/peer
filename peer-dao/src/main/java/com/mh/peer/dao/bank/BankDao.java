package com.mh.peer.dao.bank;

import com.mh.peer.model.entity.BankWh;
import com.mh.peer.model.entity.MemberBankno;
import com.mh.peer.model.message.BankWhMessageBean;
import com.mh.peer.model.message.MemberBanknoMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/5/16.
 */
public interface BankDao {

    /**
     * 查询所有银行
     */
    DMap queryAllBank(BankWhMessageBean bankWhMessageBean);


    /**
     * 查询所有银行数量
     */
    DMap queryAllBankCount(BankWhMessageBean bankWhMessageBean);

    /**
     * 保存银行
     */
    DaoResult saveBank(BankWh bankWh);
    /**
     * 查询一个银行
     */
    DMap queryOnlyBank(String id);
    /**
     * 更新银行
     */
    DaoResult updateBank(BankWh bankWh);
    /**
     * 删除银行
     */
    DaoResult deleteBank(BankWh bankWh);
    /**
     * 保存银行（前台）
     */
    DaoResult saveFrontBank(MemberBankno memberBankno);
    /**
     * 更新银行（前台）
     */
    DaoResult frontUpdatebank(MemberBankno memberBankno);
    /**
     * 查询当前银行卡信息
     */
    DMap queryUserBank(String userId);

    /**
     * 跟据条件查询个人绑定银行卡信息
     * @param memberBanknoMes
     * @return
     */
    DMap getMemberBankInfoBySome(MemberBanknoMessageBean memberBanknoMes);
}
