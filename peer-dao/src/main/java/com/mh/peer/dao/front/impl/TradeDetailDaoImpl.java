package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.TradeDetailDao;
import com.mh.peer.model.message.TradeDetailMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/6/20.
 */

@Repository
public class TradeDetailDaoImpl implements TradeDetailDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 查询资金明细，重做by zhangerxin
     * @param tradeDetailMes
     * @return
     */
    @Override
    public DMap queryTradeDetail(TradeDetailMessageBean tradeDetailMes) {
        String sql = "SELECT * FROM view_trade_detail WHERE 1 = 1";
        String orderColumn = tradeDetailMes.getOrderColumn(); //排序字段
        String orderType = tradeDetailMes.getOrderType(); //排序类型
        String pageIndex = tradeDetailMes.getPageIndex(); //当前页
        String pageSize = tradeDetailMes.getPageSize(); //每页显示数量
        String startTime = tradeDetailMes.getStartTime(); //开始时间
        String endTime = tradeDetailMes.getEndTime(); //结束时间
        String type = tradeDetailMes.getType();//交易类型
        String memberId = tradeDetailMes.getMemberid(); //会员id

        if(memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        if (startTime != null && !startTime.equals("")) { //起日期
            sql = sql + " AND DATE(TIME) >= DATE('"+startTime+"')";
        }
        if (endTime != null && !endTime.equals("")) { //止日期
            sql = sql + " AND DATE(TIME) <= DATE('"+endTime+"')";
        }
        if (type != null && !type.equals("")) { //交易类型(0-充值、1-提现、2-投资、3-回款、4-借款、5-还款、7-债权)
            sql = sql + " AND TYPE = '"+type+"'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 查询总记录数
     */
    @Override
    public DMap queryTradeDetailCount(TradeDetailMessageBean tradeDetailMes) {
        String sql = "SELECT * FROM view_trade_detail WHERE 1 = 1";
        String startTime = tradeDetailMes.getStartTime(); //开始时间
        String endTime = tradeDetailMes.getEndTime(); //结束时间
        String type = tradeDetailMes.getType();//交易类型
        String memberId = tradeDetailMes.getMemberid(); //会员id

        if(memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        if (startTime != null && !startTime.equals("")) { //起日期
            sql = sql + " AND DATE(TIME) >= DATE('"+startTime+"')";
        }
        if (endTime != null && !endTime.equals("")) { //止日期
            sql = sql + " AND DATE(TIME) <= DATE('"+endTime+"')";
        }
        if (type != null && !type.equals("")) { //交易类型(0-充值、1-提现、2-投资、3-回款、4-借款、5-还款、7-债权)
            sql = sql + " AND TYPE = '"+type+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取交易记录(App端)
     * @param memberId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public DMap getTradeRecordList(String memberId, String pageIndex, String pageSize, String tradeType, String tradeTime) {
        String sql = "SELECT * FROM trade_detail WHERE 1 = 1";
        if (memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        if (tradeType != null && !tradeType.equals("")){ //交易类型
            sql = sql + " AND TYPE = '"+tradeType+"'";
        }
        if (tradeTime != null && !tradeTime.equals("")){ //交易时间
            if (tradeTime.equals("0")){ //三天前
                sql = sql + " AND DATE(TIME) <= DATE(DATE_SUB(curdate(),INTERVAL 3 DAY))";
            }else if (tradeTime.equals("1")){ //一个月前
                sql = sql + " AND DATE(TIME) <= DATE(DATE_SUB(curdate(),INTERVAL 1 MONTH))";
            }else if (tradeTime.equals("2")){ //一周前
                sql = sql + " AND DATE(TIME) <= DATE(DATE_SUB(curdate(),INTERVAL 1 WEEK))";
            }
        }
        sql = sql + " ORDER BY CREATETIME DESC";
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
