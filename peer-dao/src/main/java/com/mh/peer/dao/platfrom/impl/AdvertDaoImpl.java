package com.mh.peer.dao.platfrom.impl;

import com.mh.peer.dao.platfrom.AdvertDao;
import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.message.AdvertContentMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-4-11.
 */
@Repository
public class AdvertDaoImpl implements AdvertDao{

    @Autowired
    private BaseDao baseDao;

    /**
     * 根据分页获取广告
     * @param advertContentMes
     * @return
     */
    @Override
    public DMap queryAdvertByPage(AdvertContentMessageBean advertContentMes) {
        String pageIndex = advertContentMes.getPageIndex(); //当前页
        String pageSize = advertContentMes.getPageSize(); //每页显示数量
        String title = advertContentMes.getTitle(); //题目
        String typeId = advertContentMes.getTypeId(); //类型id
        String sql = "SELECT ID,TYPEID,TITLE,CONTENT,AUTOR,SORT,FLAG,LOOK_COUNT,CREATEUSER,CREATETIME FROM advert_content WHERE 1 = 1 AND TYPEID IN ('1','2')";

        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(typeId != null && !typeId.equals("")){ //类型
            sql = sql + " AND TYPEID = '"+typeId+"'";
        }
        sql = sql + " ORDER BY SORT";
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);

        return dmap;
    }

    /**
     * 新增广告
     * @param advertContent
     * @return
     */
    @Override
    public DaoResult saveAdvert(AdvertContent advertContent) {
        DaoResult result = new DaoResult();
        if (advertContent == null || advertContent == null) { //广告类
            result.setCode(-1);
            result.setErrText("添加失败！");
            return result;
        }
        DConnection conn = DaoTool.getConnection();
        result = baseDao.insert(advertContent, conn); //新增广告
        if (result.getCode() < 0) {
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    /**
     * 根据广告获取广告信息
     * @param advertContent
     * @return
     */
    @Override
    public AdvertContent getAdvertById(AdvertContent advertContent) {
        DConnection conn = DaoTool.getConnection();
        advertContent = (AdvertContent) baseDao.getById(advertContent,conn);
        return advertContent;
    }


    /**
     * 修改广告
     * @param advertContent
     * @return
     */
    @Override
    public DaoResult updateAdvert(AdvertContent advertContent) {
        DaoResult result = baseDao.update(advertContent);
        if (result.getCode() < 0) {
            result.setCode(-1);
            result.setErrText("修改失败");
            return result;
        }

        return result;
    }

    /**
     * 获取广告数量
     * @param advertContentMes
     * @return
     */
    @Override
    public DMap getCountAdvertByPage(AdvertContentMessageBean advertContentMes) {
        String title = advertContentMes.getTitle(); //题目
        String typeId = advertContentMes.getTypeId(); //类型id
        String sql = "select * from advert_content where 1 = 1 AND TYPEID IN ('1','2')"; //广告查询条件
        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(typeId != null && !typeId.equals("")){ //类型id
            sql = sql + " AND TYPEID = '"+typeId+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 查询广告(前台部分)
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @param typeId
     * @return
     */
    @Override
    public DMap getListBySome(String orderColumn, String orderType, String pageIndex, String pageSize, String typeId) {
        String sql = "SELECT * FROM advert_content WHERE 1 = 1 AND FLAG = '0'";
        if(typeId != null && !typeId.equals("")){
            sql = sql + " AND TYPEID = '"+typeId+"'";
        }
        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 查询广告总数(前台) by cuibin
     */
    @Override
    public DMap queryAdvertCount(String typeId){

        String sql = "select count(*) as count from advert_content where 1 = 1";
        if(typeId != null && !typeId.equals("")){
            sql = sql + " and TYPEID = '"+typeId+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 前台查询更多公告
     * @param advertContentMes
     * @return
     */
    @Override
    public DMap queryFrontAdvertList(AdvertContentMessageBean advertContentMes) {
        String sql = "SELECT * FROM advert_content WHERE 1 = 1";
        String flag = advertContentMes.getFlag(); //状态(0-有效、1-无效)
        String typeId = advertContentMes.getTypeId(); //类型id
        String orderType = advertContentMes.getOrderType(); //排序类型
        String orderColumn = advertContentMes.getOrderColumn(); //排序字段
        String pageIndex = advertContentMes.getPageIndex(); //起始数
        String pageSize = advertContentMes.getPageSize(); //每页显示数量
        DMap dMap = new DMap();

        try{

            if (flag != null && !flag.equals("")){ //状态(0-有效、1-无效)
                sql = sql + " AND FLAG = '"+flag+"'";
            }
            if (typeId != null && !typeId.equals("")){ //类型id
                sql = sql + " AND TYPEID = '"+typeId+"'";
            }
            if (orderType != null && !orderType.equals("") && orderColumn != null && !orderColumn.equals("")){
                sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
            }
            if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
                sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
            }
            dMap = DaoTool.select(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 前台查询广告数量
     * @param advertContentMes
     * @return
     */
    @Override
    public DMap queryFrontAdvertCount(AdvertContentMessageBean advertContentMes) {
        String sql = "SELECT * FROM advert_content WHERE 1 = 1";
        String flag = advertContentMes.getFlag(); //状态(0-有效、1-无效)
        String typeId = advertContentMes.getTypeId(); //类型id
        DMap dMap = new DMap();

        try{

            if (flag != null && !flag.equals("")){ //状态(0-有效、1-无效)
                sql = sql + " AND FLAG = '"+flag+"'";
            }
            if (typeId != null && !typeId.equals("")){ //类型id
                sql = sql + " AND TYPEID = '"+typeId+"'";
            }
            dMap = DaoTool.select(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 前台查询广告详情
     * @param advertContentMes
     * @return
     */
    @Override
    public DMap queryFrontAdvertDetails(AdvertContentMessageBean advertContentMes) {
        String advertId = advertContentMes.getId(); //广告id
        String sql = "SELECT * FROM advert_content WHERE 1 = 1";
        if (advertId != null && !advertId.equals("")){
            sql = sql + " AND ID = '"+advertId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取首页广告(App端)
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public DMap getHomeAdvert(String orderColumn,String orderType,String pageIndex,String pageSize,String typeId) {
        String sql = "SELECT * FROM advert_content WHERE 1 = 1 AND FLAG = '0'"; //广告查询条件
        if (typeId != null && !typeId.equals("")){ //类型id
            sql = sql + " AND TYPEID = '"+typeId+"'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex) - 1) +","+TypeTool.getInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     *获取广告列表(App端)
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @param orderColumn
     * @param orderType
     * @return
     */
    @Override
    public DMap getAdvert(String typeId, String pageIndex, String pageSize, String orderColumn, String orderType) {
        String sql = "SELECT * FROM advert_content WHERE 1 = 1";
        if (typeId != null && !typeId.equals("")){ //广告类型
            sql = sql + " AND TYPEID = '"+typeId+"'";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 获取广告详情(App端)
     * @param advertId
     * @return
     */
    @Override
    public DMap getAdvertDetail(String advertId) {
        String sql = "SELECT * FROM advert_content WHERE 1 = 1";
        if (advertId != null && !advertId.equals("")){
            sql = sql + " AND ID = '"+advertId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
