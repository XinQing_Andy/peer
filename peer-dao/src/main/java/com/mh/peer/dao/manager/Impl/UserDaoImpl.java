package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.UserDao;
import com.mh.peer.model.entity.SysRoleUser;
import com.mh.peer.model.entity.SysUser;
import com.mh.peer.model.message.SysUserMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by zhangerxin on 2016/1/14.
 */
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 登录
     * @param user
     * @return
     */
    @Override
    public SysUser getUser(SysUser user) {
        List userList = baseDao.getListBySome(user.getClass(), "USER_NAME", user.getUserName());
        SysUser resultBean = null;
        int count = userList.size();
        for (int i = 0; i < count; i++) {
            Object re = userList.get(i);
            if (re != null) {
                if (re instanceof SysUser) {
                    resultBean = (SysUser) re;
                }
            }
        }
        return resultBean;
    }

    /**
     * 根据id获取用户信息
     * @param user
     * @return
     */
    @Override
    public SysUser getUserById(SysUser user) {
        return (SysUser)baseDao.getById(user);
    }

    /**
     * 根据id获取用户信息
     * @param sysUserMese
     * @return
     */
    @Override
    public DMap queryUserById(SysUserMessageBean sysUserMese) {
        String id = sysUserMese.getId(); //主键
        String sql = "select * from view_sys_user where 1 = 1";
        if(id != null && !id.equals("")){ //主键
            sql = sql + " and ID = '"+TypeTool.getInt(id)+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 查询用户集合(根据分页查询)(zhangerxin)
     * @param sysUserMese
     * @return
     */
    @Override
    public DMap getUserInfoByPage(SysUserMessageBean sysUserMese) {
        String pageIndex = sysUserMese.getPageIndex(); //当前页
        String pageSize = sysUserMese.getPageSize(); //每页显示数量
        String sysKeyWord = sysUserMese.getSysKeyWord(); //关键字

        String sql = "SELECT ID,USER_NAME,REAL_NAME,CPHONE,CODE,IS_ONLINE,ROLE_NAME,CREATE_TIME,EMAIL FROM view_Sys_User WHERE 1 = 1 AND DEL_FLG = '0'";

        if(sysKeyWord != null && !sysKeyWord.equals("")){ //关键字
            sql = sql + " AND (USER_NAME LIKE '"+sysKeyWord+"' OR REAL_NAME LIKE '"+sysKeyWord+"' OR CPHONE LIKE '%"+sysKeyWord+"%' OR EMAIL LIKE '"+sysKeyWord+"')";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize !=  null && !pageSize.equals("")){
            sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex)-1) +","+TypeTool.getInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 获取用户数量
     * @param sysUserMese
     * @return
     */
    @Override
    public DMap getCountUserInfoByPage(SysUserMessageBean sysUserMese) {
        String sysKeyWord = sysUserMese.getSysKeyWord(); //关键字
        String sql = "select * from view_Sys_User where 1 = 1"; //用户查询条件
        if(sysKeyWord != null && !sysKeyWord.equals("")){ //关键字
            sql = sql + " AND (USER_NAME LIKE '"+sysKeyWord+"' OR REAL_NAME LIKE '"+sysKeyWord+"' OR CPHONE LIKE '"+sysKeyWord+"' OR EMAIL LIKE '"+sysKeyWord+"')";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


   /* @Override
    public List<SysUser> getAllUser(SysUser user) {
        List userList = baseDao.findAll(user.getClass());
        if (userList.size() == 0) {
            return userList;
        }
        return userList;
    }*/

    /**
     * 获取用户信息
     * @return
     */
   /* @Override
    public DMap getUserInfo() {
        String sql = "SELECT a.ID, a.USER_NAME, a.PASS_WORD, a.DEPE_ID, "
                + "(SELECT DEPT_NAME FROM sys_dept WHERE id = a.DEPE_ID) AS DEPT_NAME, "
                + "a.CPHONE, a.OPHONE, a.EMAIL, a.ROLE_ID, "
                + "(SELECT ROLE_NAME FROM sys_role WHERE id = a.ROLE_ID) AS ROLE_NAME "
                + "FROM sys_user a";
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }*/

    /**
     * 添加用户
     * @param user
     * @return
     */
    @Override
    public DaoResult addUser(SysUser user) {
        DConnection conn = DaoTool.getConnection();
        DaoResult result = new DaoResult();
        if (user == null) {
            result.setErrText("添加失败");
            result.setCode(-1);
            return result;
        }
        result = baseDao.insert(user, conn);
        if(result.getCode() < 0){
            result.setErrText("用户添加失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }

        SysRoleUser sysRoleUser = new SysRoleUser();
        sysRoleUser.setUserId(result.getIDENTITY());
        sysRoleUser.setRoleId(user.getRoleId());
        result = baseDao.insert(sysRoleUser, conn); //用户角色关联
        if(result.getCode() < 0){
            result.setErrText("用户角色添加失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }


    /**
     * 设置用户状态
     * @param user
     * @return
     */
    @Override
    public DaoResult settingSysFlag(SysUser user) {
        DConnection conn = DaoTool.getConnection();
        int id = TypeTool.getInt(user.getId()); //主键
        String isOnline = user.getIsOnline(); //状态(0-启用、1-锁定)
        DaoResult result = new DaoResult();
        String sql = "update sys_user set IS_ONLINE = <IS_ONLINE> where ID = <ID>";
        DMap dmap = new DMap();
        dmap.setData("IS_ONLINE", isOnline); //状态(0-启用、1-锁定)
        dmap.setData("ID", id); //主键
        result = DaoTool.update(sql, dmap.getData(), conn);

        if(result.getCode() < 0){
            result.setErrText("设置失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    /**
     * 重置用户密码
     * @param user
     * @return
     */
    @Override
    public DaoResult resetPwd(SysUser user) {
        DConnection conn = DaoTool.getConnection();
        int id = TypeTool.getInt(user.getId()); //主键
        String password = user.getPassWord(); //密码
        DaoResult result = new DaoResult();
        String sql = "update sys_user set PASS_WORD = <PASS_WORD> where ID = <ID>";
        DMap dmap = new DMap();
        dmap.setData("PASS_WORD", password);
        dmap.setData("ID", id);
        result = DaoTool.update(sql, dmap.getData(), conn);

        if(result.getCode() < 0){
            result.setErrText("重置密码失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    /**
     * 验证用户唯一性
     * @param user
     * @return
     */
    @Override
    public DMap checkUser(SysUserMessageBean user) {
        String id = user.getId(); //主键
        String userName = user.getUserName(); //用户名
        String sql = "select * from sys_user where USER_NAME = '"+userName+"'";
        if(id != null && !id.equals("")){
            sql = sql + " and ID <> '"+id+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 编辑用户
     * @param user
     * @return
     */
    @Override
    public DaoResult updateUser(SysUser user) {
        DConnection conn = DaoTool.getConnection();
        DaoResult result = new DaoResult();
        if (user == null) {
            result.setCode(-1);
            result.setErrText("修改失败");
            conn.rollback();
            conn.close();
            return result;
        }
        result = baseDao.update(user, conn);
        if(result.getCode() < 0){
            result.setErrText("用户修改失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }
        // 删除sys_role_user
        String sql = "DELETE FROM sys_role_user WHERE USER_ID = <USER_ID>";
        DMap dmap = new DMap();
        dmap.setData("USER_ID", user.getId());
        result = DaoTool.update(sql, dmap.getData(), conn);
        if(result.getCode() < 0){
            result.setErrText("删除用户角色表失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }
        SysRoleUser sysRoleUser = new SysRoleUser();
        sysRoleUser.setUserId(user.getId());
        sysRoleUser.setRoleId(user.getRoleId());
        result = baseDao.insert(sysRoleUser, conn);
        if(result.getCode() < 0){
            result.setErrText("用户角色添加失败");
            result.setCode(-1);
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    @Override
    public DaoResult delUser(SysUser user) {
        DaoResult result = new DaoResult();
        if (user == null) {
            result.setCode(-1);
            result.setErrText("删除失败");
            return result;
        }
        return result;
    }
}
