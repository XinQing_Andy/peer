package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.SysLoginLog;
import com.salon.frame.util.DaoResult;

/**
 * Created by zhaojiaming on 2016/1/14.
 */
public interface UserLogDao {

    /**
     * 登入log
     * @param loginLog
     * @return
     */
    DaoResult loginLog(SysLoginLog loginLog);

    /**
     * 登出log
     * @param loginLog
     * @return
     */
    DaoResult loginOutLog(SysLoginLog loginLog);
}
