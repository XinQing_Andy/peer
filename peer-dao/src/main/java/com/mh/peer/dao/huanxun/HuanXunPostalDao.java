package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.HuanxunPostal;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.HuanXunPostalMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by zhangerxin on 2016-7-26.
 */
public interface HuanXunPostalDao {

    /**
     * 环迅部分提现
     * @param huanxunPostal
     * @param memberMessage
     * @param tradeDetail
     * @return
     */
    public DaoResult postalhxResult(HuanxunPostal huanxunPostal,MemberMessage memberMessage,TradeDetail tradeDetail);

    /**
     * 获取提现备份信息
     * @param huanXunPostalMes
     * @return
     */
    public DMap getPostalBfInfo(HuanXunPostalMessageBean huanXunPostalMes);
}
