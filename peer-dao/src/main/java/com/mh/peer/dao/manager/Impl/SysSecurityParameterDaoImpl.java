package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.SysSecurityParameterDao;
import com.mh.peer.model.entity.SysDept;
import com.mh.peer.model.entity.SysSecurityParameter;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zhangerxin on 2016-4-1.
 */
@Repository
public class SysSecurityParameterDaoImpl implements SysSecurityParameterDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 新增安全参数
     * @param sysSecurityParameter
     * @return
     */
    @Override
    public DaoResult addSysSecurityParameter(SysSecurityParameter sysSecurityParameter) {
        DaoResult result = new DaoResult();
        if (sysSecurityParameter == null || sysSecurityParameter == null) { //安全问题类
            result.setCode(-1);
            result.setErrText("添加失败！");
            return result;
        }
        DConnection conn = DaoTool.getConnection();
        result = baseDao.insert(sysSecurityParameter, conn); //新增安全问题
        if (result.getCode() < 0) {
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    /**
     * 修改安全参数
     * @param sysSecurityParameter
     * @return
     */
    @Override
    public DaoResult updateSecurityParameter(SysSecurityParameter sysSecurityParameter) {
        DaoResult result = new DaoResult();
        if (sysSecurityParameter == null || sysSecurityParameter == null) { //安全问题类
            result.setCode(-1);
            result.setErrText("修改失败！");
            return result;
        }
        DConnection conn = DaoTool.getConnection();
        result = baseDao.update(sysSecurityParameter,conn); //修改安全问题
        if (result.getCode() < 0) {
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }



    /**
     * 查询安全问题信息
     * @param sysSecurityParameter
     * @return
     */
    @Override
    public List<SysSecurityParameter> querySecurityParameter(SysSecurityParameter sysSecurityParameter) {
        List sysSecurityParameterList = baseDao.findAll(SysSecurityParameter.class);
        if (sysSecurityParameterList.size() == 0) {
            return sysSecurityParameterList;
        }
        return sysSecurityParameterList;
    }
}
