package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.SysUser;
import com.mh.peer.model.message.SysRoleMessageBean;
import com.mh.peer.model.message.SysUserMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import java.util.List;

/**
 * Created by zhangerxin on 2016/4/9
 */
public interface UserDao {

    /**
     * 登录
     * @param user
     * @return
     */
    SysUser getUser(SysUser user);

    /**
     * 通过id获取用户信息
     * @param user
     * @return
     */
    SysUser getUserById(SysUser user);

    /**
     * 根据id获取用户信息(zhangerxin)
     * @param sysUserMese
     * @return
     */
    DMap queryUserById(SysUserMessageBean sysUserMese);

    /**
     * 查询用户信息(根据分页)(zhangerxin)
     * @param sysUserMese
     * @return
     */
    DMap getUserInfoByPage(SysUserMessageBean sysUserMese);

    /**
     * 获取用户数量
     * @return
     */
    DMap getCountUserInfoByPage(SysUserMessageBean sysUserMese);

    /**
     *
     * @return
     */
    //public List<SysUser> getAllUser(SysUser user);

    /**
     * 获取信息
     * @return
     */
    //public DMap getUserInfo();

    /**
     * 新增用户信息
     * @param user
     * @return
     */
    DaoResult addUser(SysUser user);

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    DaoResult updateUser(SysUser user);

    /**
     * 删除用户信息
     * @param user
     * @return
     */
    DaoResult delUser(SysUser user);

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    DaoResult settingSysFlag(SysUser user);

    /**
     * 重置用户密码
     * @param user
     * @return
     */
    DaoResult resetPwd(SysUser user);

    /**
     * 验证用户唯一性
     * @param user
     * @return
     */
    DMap checkUser(SysUserMessageBean user);


}
