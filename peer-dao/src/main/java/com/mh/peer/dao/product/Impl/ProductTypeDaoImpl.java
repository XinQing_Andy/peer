package com.mh.peer.dao.product.Impl;

import com.mh.peer.dao.product.ProductTypeDao;
import com.mh.peer.model.entity.ProductType;
import com.mh.peer.model.message.ProductTypeMessage;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/4/29.
 */
@Repository
public class ProductTypeDaoImpl implements ProductTypeDao{

    @Autowired
    private BaseDao baseDao;
    /**
     * 查询所有产品类型
     */
    @Override
    public DMap queryAllProductType(ProductTypeMessage productTypeMessage) {
        String pageIndex = productTypeMessage.getPageIndex(); //当前页
        String pageSize = productTypeMessage.getPageSize(); //每页显示数量
        String title = productTypeMessage.getTitle(); //名称
        String sql = "SELECT * FROM product_type WHERE 1 = 1";
        if(title != null && !title.equals("")){ //题目
            sql = sql + " and TYPENAME LIKE '%"+title+"%'";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize !=  null && !pageSize.equals("")){
            sql = sql + " LIMIT "+ (TypeTool.getInt(pageIndex)-1) +","+TypeTool.getInt(pageSize)+"";
        }
        //System.out.println(sql);
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
    /**
     * 查询所有产品类型数量
     */
    @Override
    public DMap queryAllProductTypeCount(ProductTypeMessage productTypeMessage) {
        String pageIndex = productTypeMessage.getPageIndex(); //当前页
        String pageSize = productTypeMessage.getPageSize(); //每页显示数量
        String title = productTypeMessage.getTitle(); //名称
        String sql = "SELECT * FROM product_type WHERE 1 = 1";
        if(title != null && !title.equals("")){ //题目
            sql = sql + " and TYPENAME LIKE '%"+title+"%'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
    /**
     * 删除产品类型
     */
    @Override
    public DaoResult deleteProductType(ProductType productType) {
        return baseDao.delete(productType);
    }
    /**
     * 添加产品类型
     */
    @Override
    public DaoResult addProductType(ProductType productType) {
        return baseDao.insert(productType);
    }
    /**
     * 更新产品类型
     */
    @Override
    public DaoResult updateProductType(ProductType productType) {
        return baseDao.update(productType);
    }
}
