package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.SystemEmailDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.SysEmailSetting;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by cuibin on 2016/4/25.
 */
@Repository
public class SystemEmailDaoImpl implements SystemEmailDao{
    @Autowired
    private BaseDao baseDao;
    /**
     *查询邮件设置
     */
    @Override
    public DMap querySystemEmail() {
        return DaoTool.select("select * from sys_email_setting");
    }
    /**
     *  添加邮件设置
     */
    @Override
    public DaoResult addSystemEmail(SysEmailSetting sysEmailSetting) {
        return baseDao.insert(sysEmailSetting);
    }
    /**
     *  更新邮件设置
     */
    @Override
    public DaoResult updateSystemEmail(SysEmailSetting sysEmailSetting) {
        return baseDao.update(sysEmailSetting);
    }

}
