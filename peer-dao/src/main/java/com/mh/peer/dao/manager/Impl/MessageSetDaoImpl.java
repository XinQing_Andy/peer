package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.MessageSetDao;
import com.mh.peer.model.entity.SysMessageSetting;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by cuibin on 2016/5/30.
 */
@Repository
public class MessageSetDaoImpl implements MessageSetDao
{

    @Autowired
    private BaseDao baseDao;

    /**
     *查询一个短信设置
     */
    @Override
    public DMap queryOnlyMessage()
    {
        return DaoTool.select("select * from sys_message_setting");
    }
    /**
     * 更新短信设置
     */
    @Override
    public DaoResult updateMessageSet(SysMessageSetting sysMessageSetting)
    {
        return baseDao.update(sysMessageSetting);
    }

}
