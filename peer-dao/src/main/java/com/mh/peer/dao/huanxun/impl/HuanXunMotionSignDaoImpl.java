package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunMotionSignDao;
import com.mh.peer.model.entity.HuanxunAutosign;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/8/4.
 */
@Repository
public class HuanXunMotionSignDaoImpl implements HuanXunMotionSignDao {
    @Autowired
    private BaseDao baseDao;

    /**
     * 查询环迅注册记录
     *
     * @param memberId
     * @return
     */
    @Override
    public DMap queryHuanXunReg(String memberId) {
        String sql = "select * from huanxun_regist where MEMBERID = '" + memberId + "'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 保存自动签约返回信息
     */
    @Override
    public DaoResult saveMotionSignResult(HuanxunAutosign huanxunAutosign) {
        if (huanxunAutosign == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        DaoResult daoResult = baseDao.insert(huanxunAutosign);
        System.out.println("============================"+daoResult.getErrText());
        return daoResult;
    }


    /**
     * 查询自动签约返回信息是否存在
     */
    public DMap queryIsResult(String memberId,String signedType) {
        String sql = "select * from huanxun_autosign where 1 = 1";
        if (memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        if (signedType != null && !signedType.equals("")){ //签约类型
            sql = sql + " AND SIGNEDTYPE = '"+signedType+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
