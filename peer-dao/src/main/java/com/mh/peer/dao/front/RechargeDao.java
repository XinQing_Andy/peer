package com.mh.peer.dao.front;

import com.mh.peer.model.entity.ChargeRecord;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.SerialnumberWh;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.ChargeRecordMessageBean;
import com.mh.peer.model.message.RechargeHistoryMessageBean;
import com.mh.peer.model.message.ViewHuanxunRechargeMes;
import com.salon.frame.data.DMap;
import java.util.List;


/**
 * Created by Cuibin on 2016/6/17.
 */
public interface RechargeDao {
    /**
     *充值
     */
    public boolean userRecharge(ChargeRecord chargeRecord,TradeDetail tradeDetail,MemberInfo memberInfo,SerialnumberWh serialnumberWh);

    /**
     *  查询充值记录
     */
    public DMap queryRechargeHistory(RechargeHistoryMessageBean rechargeHistoryMessageBean);

    /**
     * 查询 实际金额 和 充值金额
     */
    public DMap queryRechargeSum(String memberId);

    /**
     * 查询充值记录总数
     */
    public DMap queryRechargeHistoryCount(RechargeHistoryMessageBean rechargeHistoryMessageBean);
    /**
     * 查询充值记录 视图
     */
    public DMap queryViewRechargeHistory(ViewHuanxunRechargeMes viewHuanxunRechargeMes);

    /**
     * 查询充值记录总数 视图
     */
    public DMap queryViewRechargeHistoryCount(ViewHuanxunRechargeMes viewHuanxunRechargeMes);
}
