package com.mh.peer.dao.product;

import com.mh.peer.model.entity.ProductContent;
import com.mh.peer.model.message.ProductMessageBean;
import com.mh.peer.model.message.ProductReceiveMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by zhangerxin on 2016-4-10.
 */
public interface ProductReceiveAttornDao {

    /**
     * 根据分页获取产品信息(By Zhangerxin)
     * @param productMes
     * @return
     */
    DMap queryProductByPage(ProductMessageBean productMes);

    /**
     * 新增产品
     * @param productContent
     * @return
     */
    DaoResult saveProduct(ProductContent productContent);

    /**
     * 根据id获取产品信息
     * @param productMes
     * @return
     */
    DMap queryProductById(ProductMessageBean productMes);

    /**
     * 根据glId获取附件信息
     * @param productMes
     * @return
     */
    DMap queryFileByGlId(ProductMessageBean productMes);

    /**
     * 修改产品
     * @param productMessageBean
     * @return
     */
    DaoResult updateProduct(ProductContent productMessageBean);

    /**
     * 删除产品
     * @param productContent
     * @return
     */
    DaoResult delProduct(ProductContent productContent);

    /**
     * 获取产品数量
     * @return
     */
    DMap getCountProductByPage(ProductMessageBean producrMes);

    /**
     * 查询产品(前台部分)
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DMap getListBySome(String orderColumn, String orderType, String pageIndex, String pageSize);

    /**
     * 查询产品信息(前台部分)
     * @param orderColumn
     * @param orderType
     * @param productMes
     * @return
     */
    DMap getFrontList(String orderColumn, String orderType, ProductReceiveMessageBean productMes);

    /**
     * 查询推荐产品
     * @return
     */
    DMap getRecommend(String orderColumn, String orderType, String pageIndex, String pageSize);

    /**
     * 按照类别获取产品
     */
    DMap queryProductByType(String productType);

    DMap getFrontList(String orderColumn, String orderType, ProductMessageBean productMes);
}
