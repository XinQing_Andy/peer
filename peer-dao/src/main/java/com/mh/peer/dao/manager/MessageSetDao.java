package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.SysMessageSetting;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by cuibin on 2016/5/30.
 */
public interface MessageSetDao {
    /**
     * 查询一个短信设置
     */
    DMap queryOnlyMessage();
    /**
     * 更新短信设置
     */
    DaoResult updateMessageSet(SysMessageSetting sysMessageSetting);

}
