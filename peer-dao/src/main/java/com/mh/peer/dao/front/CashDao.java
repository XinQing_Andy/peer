package com.mh.peer.dao.front;

import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.PostalRecord;
import com.mh.peer.model.entity.SerialnumberWh;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.HuanXunPostalMessageBean;
import com.mh.peer.model.message.PostalRecordMessageBean;
import com.salon.frame.data.DMap;

import java.util.List;

/**
 * Created by Cuibin on 2016/6/20.
 */
public interface CashDao {

    /**
     * 按照id查询城市
     */
    public DMap queryCityById(String id);

    /**
     * 按照id查询省
     */
    public DMap queryProvinceById(String id);

    /**
     * 提现
     */
    public boolean userCash(PostalRecord postalRecord,TradeDetail tradeDetail,SerialnumberWh serialnumberWh,MemberInfo memberInfo);

    /**
     * 查询提现记录
     * @param huanXunPostalMes
     * @return
     */
    public DMap queryPostalRecordByPage(HuanXunPostalMessageBean huanXunPostalMes);

    /**
     * 查询提现金额 到账金额 手续费用
     */
    public DMap querySum(String memberId);

    /**
     * 查询所有提现记录
     */
    public DMap queryPostalRecordByPageCount(HuanXunPostalMessageBean huanXunPostalMessageBean);

    /**
     * 获取可提现金额
     * @param memberId
     * @return
     */
    public DMap getPostaledPrice(String memberId);
}
