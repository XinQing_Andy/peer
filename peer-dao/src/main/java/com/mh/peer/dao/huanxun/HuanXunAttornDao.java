package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.*;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

import java.util.Map;

/**
 * Created by zhangerxin on 2016-8-2.
 * 环迅债权转让资金冻结
 */
public interface HuanXunAttornDao {

    /**
     * 环迅债权转让资金冻结
     * @param huanxunFreezeAttorn
     * @param tradeDetail
     * @param memberMessage
     * @return
     */
    public DaoResult attornhxResult(Map<String,Object> mapException,HuanxunFreezeAttorn huanxunFreezeAttorn, TradeDetail tradeDetail, MemberMessage memberMessage);

    /**
     * 获取债权冻结信息
     * @param huanXunFreezeMes
     * @return
     */
    public DMap getFreezeAttornInfo(HuanXunFreezeMessageBean huanXunFreezeMes);

    /**
     * 债权解冻
     * @param memberInfo
     * @param huanxunFreeze
     */
    public void unfreezeAttornResult(Map<String,Object> mapException,MemberInfo memberInfo,HuanxunFreeze huanxunFreeze,
                                     ProductAttornRecord productAttornRecord,ProductBuyInfo productBuyInfo);
}
