package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.HuanxunTransfer;
import com.mh.peer.model.entity.HuanxunTransferDetail;
import com.mh.peer.model.message.HuanXunTransferMessage;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangerxin on 2016-7-31.
 * 转账信息
 */
public interface HuanXunTransferDao {

    /**
     * 根据条件获取转账信息
     * @param huanXunTransferMes
     * @return
     */
    DMap getTransferInfoBySome(HuanXunTransferMessage huanXunTransferMes);

    /**
     * 投资转账
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     */
    DaoResult transferBuyResult(Map<String,Object> mapException,HuanxunTransfer huanxunTransfer, List<HuanxunTransferDetail> listHuanxunTransferDetail,String productId);

    /**
     * 债权转账
     * @param huanxunTransfer
     * @param listHuanxunTransferDetail
     * @param attornId
     * @return
     */
    DaoResult transferAttornResult(Map<String,Object> mapException,HuanxunTransfer huanxunTransfer, List<HuanxunTransferDetail> listHuanxunTransferDetail,String attornId);

    /**
     * 保存还款转账信息 bycuibin
     */
    public DaoResult saveHuanxunTransfer(HuanxunTransfer huanxunTransfer,List<HuanxunTransferDetail> listHuanxunTransferDetail,String receiveId);


    /**
     * 查询转账信息是否存在 bycuibin
     */
    public DMap queryIsTransfer(String uuid);

    /**
     * 根据产品id和应还款时间获取转账信息
     * @param productId
     * @param yrepaymentTime
     * @return
     */
    DMap getRepayTransferAcctDetail(String productId, String yrepaymentTime);

    /**
     * 根据冻结信息进行还款
     * @return
     */
    String repayhxPriceTransfer(HuanxunTransfer huanxunTransfer,List<HuanxunTransferDetail> listHuanxunTransferDetail,
                                   Map<String,Object> mapException,String repayId,String yRepayTime);
}
