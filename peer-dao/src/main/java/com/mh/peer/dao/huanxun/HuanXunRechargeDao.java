package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.HuanxunRecharge;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.salon.frame.util.DaoResult;

/**
 * Created by zhangerxin on 2016-7-24.
 */
public interface HuanXunRechargeDao {

    /**
     * 环迅部分充值
     * @param huanxunRecharge
     * @param tradeDetail
     * @param memberMessage
     * @return
     */
    public DaoResult rechargehxResult(HuanxunRecharge huanxunRecharge, TradeDetail tradeDetail, MemberMessage memberMessage);
}
