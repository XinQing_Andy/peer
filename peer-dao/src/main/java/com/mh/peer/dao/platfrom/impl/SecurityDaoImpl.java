package com.mh.peer.dao.platfrom.impl;

import com.mh.peer.dao.platfrom.SecurityDao;
import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.message.AdvertContentMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-4-11.
 */
@Repository
public class SecurityDaoImpl implements SecurityDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 根据分页获取广告
     * @param advertContentMes
     * @return
     */
    @Override
    public DMap queryAdvertByPage(AdvertContentMessageBean advertContentMes) {
        String pageIndex = advertContentMes.getPageIndex(); //当前页
        String pageSize = advertContentMes.getPageSize(); //每页显示数量
        String title = advertContentMes.getTitle(); //题目
        String typeId = advertContentMes.getTypeId(); //类型
        String sql = "SELECT ID,TYPEID,TITLE,CONTENT,AUTOR,SORT,FLAG,LOOK_COUNT,CREATEUSER,CREATETIME FROM advert_content WHERE 1 = 1 AND TYPEID IN ('4','5','6','7')";

        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(typeId != null && !typeId.equals("")){ //类型id
            sql = sql + " AND TYPEID = '"+typeId+"'";
        }
        sql = sql + " ORDER BY SORT";
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(Integer.parseInt(pageIndex) - 1)+","+Integer.parseInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);

        return dmap;
    }

    /**
     * 新增广告
     * @param advertContent
     * @return
     */
    @Override
    public DaoResult saveAdvert(AdvertContent advertContent) {
        DaoResult result = new DaoResult();
        if (advertContent == null || advertContent == null) { //广告类
            result.setCode(-1);
            result.setErrText("添加失败！");
            return result;
        }
        DConnection conn = DaoTool.getConnection();
        result = baseDao.insert(advertContent, conn); //新增广告
        if (result.getCode() < 0) {
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }

    /**
     * 根据广告获取广告信息
     * @param advertContent
     * @return
     */
    @Override
    public AdvertContent getAdvertById(AdvertContent advertContent) {
        DConnection conn = DaoTool.getConnection();
        advertContent = (AdvertContent) baseDao.getById(advertContent,conn);
        return advertContent;
    }


    /**
     * 修改广告
     * @param advertContent
     * @return
     */
    @Override
    public DaoResult updateAdvert(AdvertContent advertContent) {
        DaoResult result = new DaoResult();
        if (advertContent == null) {
            result.setCode(-1);
            result.setErrText("修改失败");
            return result;
        }
        result = baseDao.update(advertContent);
        return result;
    }


    /**
     * 获取保障数量
     * @param advertContentMes
     * @return
     */
    @Override
    public DMap getCountAdvertByPage(AdvertContentMessageBean advertContentMes) {
        String title = advertContentMes.getTitle(); //题目
        String typeId = advertContentMes.getTypeId(); //类型id
        String sql = "select * from advert_content where 1 = 1 AND TYPEID IN ('4','5','6','7')"; //保障查询条件
        if(title != null && !title.equals("")){ //题目
            sql = sql + " AND TITLE LIKE '%"+title+"%'";
        }
        if(typeId != null && !typeId.equals("")){ //类型id
            sql = sql + " AND TYPEID = '"+typeId+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
}
