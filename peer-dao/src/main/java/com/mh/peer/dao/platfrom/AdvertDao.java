package com.mh.peer.dao.platfrom;

import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.message.AdvertContentMessageBean;
import com.mh.peer.model.message.ProductMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by zhangerxin on 2016-4-11.
 */
public interface AdvertDao {

    /**
     * 根据分页获取广告信息
     * @param advertContentMes
     * @return
     */
    DMap queryAdvertByPage(AdvertContentMessageBean advertContentMes);

    /**
     * 新增广告
     * @param advertContent
     * @return
     */
    DaoResult saveAdvert(AdvertContent advertContent);

    /**
     * 根据id获取广告信息
     * @param advertContent
     * @return
     */
    AdvertContent getAdvertById(AdvertContent advertContent);

    /**
     * 修改广告
     * @param advertContent
     * @return
     */
    DaoResult updateAdvert(AdvertContent advertContent);

    /**
     * 获取广告数量
     * @return
     */
    DMap getCountAdvertByPage(AdvertContentMessageBean advertContentMes);

    /**
     * 查询广告(前台部分)
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DMap getListBySome(String orderColumn, String orderType, String pageIndex, String pageSize, String typeId);

    /**
     * 查询首页广告
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    DMap getHomeAdvert(String orderColumn, String orderType, String pageIndex, String pageSize, String typeId);

    /**
     * 获取广告列表(App端)
     * @param typeId
     * @param pageIndex
     * @param pageSize
     * @param orderColumn
     * @param orderType
     * @return
     */
    DMap getAdvert(String typeId,String pageIndex,String pageSize,String orderColumn,String orderType);

    /**
     * 获取广告详情
     * @param advertId
     * @return
     */
    DMap getAdvertDetail(String advertId);


    /**
     * 查询广告总数(前台) by cuibin
     */
    public DMap queryAdvertCount(String typeId);

    /**
     * 前台查询广告列表
     * @param advertContentMes
     * @return
     */
    DMap queryFrontAdvertList(AdvertContentMessageBean advertContentMes);

    /**
     * 前台查询广告数量
     * @param advertContentMes
     * @return
     */
    DMap queryFrontAdvertCount(AdvertContentMessageBean advertContentMes);

    /**
     * 前台查询广告详情
     * @param advertContentMes
     * @return
     */
    DMap queryFrontAdvertDetails(AdvertContentMessageBean advertContentMes);
}
