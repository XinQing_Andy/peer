package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.SysSecurityQuestionParameter;
import com.mh.peer.model.message.SysSecurityQuestionMessageBean;
import com.mh.peer.model.message.SysUserMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-4-1.
 */
public interface SysSecurityQuestionDao {

    /**
     * 查询安全问题信息(根据分页)(zhangerxin)
     * @param sysSecurityQuestionMes
     * @return
     */
    DMap querySecurityQuestionByPage(SysSecurityQuestionMessageBean sysSecurityQuestionMes);

    /**
     * 新增安全问题
     * @param sysSecurityQuestion
     * @return
     */
    DaoResult addSysSecurityQuestion(SysSecurityQuestionParameter sysSecurityQuestion);

    /**
     * 修改安全问题
     * @param sysSecurityQuestion
     * @return
     */
    DaoResult updateSysSecurityQuestion(SysSecurityQuestionParameter sysSecurityQuestion);

    /**
     * 获取安全问题数量
     * @param sysSecurityQuestionMes
     * @return
     */
    DMap getCountSecurityQuestion(SysSecurityQuestionMessageBean sysSecurityQuestionMes);

}
