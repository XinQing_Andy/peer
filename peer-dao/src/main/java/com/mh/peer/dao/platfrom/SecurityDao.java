package com.mh.peer.dao.platfrom;

import com.mh.peer.model.entity.AdvertContent;
import com.mh.peer.model.message.AdvertContentMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by zhangerxin on 2016-4-11.
 */
public interface SecurityDao {

    /**
     * 根据分页获取广告信息
     * @param advertContentMes
     * @return
     */
    DMap queryAdvertByPage(AdvertContentMessageBean advertContentMes);

    /**
     * 新增产品
     * @param advertContent
     * @return
     */
    DaoResult saveAdvert(AdvertContent advertContent);

    /**
     * 根据id获取广告信息
     * @param advertContent
     * @return
     */
    AdvertContent getAdvertById(AdvertContent advertContent);

    /**
     * 修改产品
     * @param advertContent
     * @return
     */
    DaoResult updateAdvert(AdvertContent advertContent);

    /**
     * 获取保障数量
     * @param advertContentMes
     * @return
     */
    DMap getCountAdvertByPage(AdvertContentMessageBean advertContentMes);
}
