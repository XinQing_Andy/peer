package com.mh.peer.dao.front;

import com.mh.peer.model.message.ProductReceiveInfoMessageBean;
import com.mh.peer.model.message.ProductReceiveMessageBean;
import com.salon.frame.data.DMap;

/**
 * Created by zhangerxin on 2016-8-31.
 */
public interface ReceiveDao {

    /**
     * 根据投资id获取还款信息集合
     * @param buyId
     * @return
     */
    DMap getReceiveInfoList(String buyId,String orderColumn,String orderType,String pageIndex,String pageSize);

    /**
     * 根据投资id获取回款信息数量
     * @param buyId
     * @return
     */
    DMap getReceiveInfoCount(String buyId);

    /**
     * 根据条件获取待回款信息集合
     * @param productReceiveMes
     * @return
     */
    DMap getProductReceiveList(ProductReceiveMessageBean productReceiveMes);

    /**
     * 根据条件获取待还款信息数量
     * @param productReceiveMes
     * @return
     */
    DMap getProductReceiveListCount(ProductReceiveMessageBean productReceiveMes);
}
