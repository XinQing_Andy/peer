package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunRepayDao;
import com.mh.peer.exception.HuanXunFreezeRepayException;
import com.mh.peer.model.entity.HuanxunFreezeRepay;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunRepayMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * Created by zhangerxin on 2016-8-7.
 */
@Repository
public class HuanXunRepayDaoImpl implements HuanXunRepayDao{

    @Autowired
    private BaseDao baseDao;

    /**
     * 获取会员信息
     * @param huanXunRepayMes
     * @return
     */
    @Override
    public DMap queryMemberInfo(HuanXunRepayMessageBean huanXunRepayMes) {
        String sql = "SELECT * FROM view_product_repay WHERE 1 = 1";
        String repayId = huanXunRepayMes.getRepayId(); //会员id
        DMap dMap = new DMap();

        try{

            if (repayId != null && !repayId.equals("")){ //会员id
                sql = sql + " AND ID = '"+repayId+"'";
            }
            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 根据会员id获取会员信息
     * @param memberId
     * @return
     */
    @Override
    public DMap getMemberInfo(String memberId) {
        String sql = "SELECT * FROM member_info WHERE ID = '"+memberId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 环迅还款金额冻结部分
     * @param huanxunFreezeRepay
     * @param memberMessage
     * @param memberInfo
     * @return
     */
    @Override
    public DaoResult repayhxResult(HuanxunFreezeRepay huanxunFreezeRepay, MemberMessage memberMessage, MemberInfo memberInfo,
                                   Map<String,Object> mapException) {
        DConnection conn = DaoTool.getConnection();
        DaoResult daoResultFreezeRepay = new DaoResult(); //还款冻结部分
        DaoResult daoResultMemberMessage = new DaoResult(); //我的消息
        DaoResult daoResultMemberInfo = new DaoResult(); //会员信息
        DaoResult daoResultFreezeBf = new DaoResult(); //冻结备份
        String resultCode = ""; //响应吗(000000-成功、000001~999999-失败)
        Double balance = 0.0d; //会员余额
        String memberId = ""; //会员id
        String merbillno = huanxunFreezeRepay.getMerbillno(); //商户订单号
        String flag = "2"; //冻结备份表状态(0-冻结成功、1-冻结失败、2-待冻结)
        String sqlHuanXunFreezeBf = ""; //环迅备份查询条件
        DMap dMapHuanXunFreezeBf = new DMap();

        try{

            daoResultFreezeRepay = baseDao.insert(huanxunFreezeRepay,conn); //还款冻结部分
            if (daoResultFreezeRepay.getCode() < 0){
                System.out.println("还款冻结失败！");
                HuanXunFreezeRepayException.handle(mapException);
                conn.rollback();
            }else{
                resultCode = huanxunFreezeRepay.getResultcode();
                System.out.println("还款冻结成功！resultCode======"+resultCode+",merbillno======"+merbillno);
                if (resultCode != null && !resultCode.equals("")){
                    if (resultCode.equals("000000")){ //成功
                        flag = "0";
                    }else{
                        flag = "1";
                    }
                }

                sqlHuanXunFreezeBf = "UPDATE huanxun_freeze_bf SET FLAG = <flag> WHERE MERBILLNO = <merbillno>";
                dMapHuanXunFreezeBf.setData("flag",flag); //冻结备份表状态(0-冻结成功、1-冻结失败、2-待冻结)
                dMapHuanXunFreezeBf.setData("merbillno",merbillno); //商户订单号
                daoResultFreezeBf = DaoTool.update(sqlHuanXunFreezeBf,dMapHuanXunFreezeBf.getData(),conn);
                if (daoResultFreezeBf.getCode() < 0){
                    System.out.println("还款冻结备份更新失败！");
                    HuanXunFreezeRepayException.handle(mapException);
                    conn.rollback();
                }else{
                    System.out.println("还款冻结备份更新成功！");
                    daoResultMemberMessage = baseDao.insert(memberMessage,conn); //我的消息
                    if (daoResultMemberMessage.getCode() < 0){
                        System.out.println("我的消息失败！");
                        HuanXunFreezeRepayException.handle(mapException);
                        conn.rollback();
                    }else{
                        System.out.println("我的消息成功！");
                        memberId = memberInfo.getId(); //会员id
                        balance = memberInfo.getBalance(); //余额
                        String sqlMemberInfo = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
                        DMap dMapMemberInfo = new DMap();
                        dMapMemberInfo.setData("memberId",memberId); //会员id
                        dMapMemberInfo.setData("balance",balance); //余额
                        daoResultMemberInfo = DaoTool.update(sqlMemberInfo,dMapMemberInfo.getData(),conn);
                        if (daoResultMemberInfo.getCode() < 0){
                            System.out.println("更新余额失败！");
                            HuanXunFreezeRepayException.handle(mapException);
                            conn.rollback();
                        }else{
                            System.out.println("更新余额成功！");
                        }
                    }
                }
            }
        }catch (Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.commit();
            conn.close();
        }
        return daoResultMemberInfo;
    }


    /**
     * 根据还款id获取还款冻结数量
     * @param repayId
     * @return
     */
    @Override
    public DMap queryFreezeRepayCount(String repayId) {
        String sql = "SELECT * FROM huanxun_freeze_repay WHERE 1 = 1 AND RESULTCODE = '000000'";
        if (repayId != null && !repayId.equals("")){ //还款主键
            sql = sql + " AND REPAYID = '"+repayId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
