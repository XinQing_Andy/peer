package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunRechargeDao;
import com.mh.peer.model.entity.HuanxunRecharge;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-7-24.
 * 环迅充值
 */
@Repository
public class HuanXunRechargeDaoImpl implements HuanXunRechargeDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 环迅部分充值
     * @param huanxunRecharge
     * @param tradeDetail
     * @param memberMessage
     * @return
     */
    @Override
    public DaoResult rechargehxResult(HuanxunRecharge huanxunRecharge, TradeDetail tradeDetail, MemberMessage memberMessage) {
        DConnection conn = DaoTool.getConnection();
        String sqlHxRecharge = "SELECT * FROM huanxun_recharge WHERE 1 = 1"; //环迅充值查询条件
        String sqlHxRechargeBf = ""; //环迅充值备份
        String glId = huanxunRecharge.getGlid(); //环迅充值部分关联id
        String memberId = huanxunRecharge.getMemberid(); //会员id
        int serial_Number = 0; //流水号
        String merBillNo = huanxunRecharge.getMerbillno(); //商户订单号
        String flagRechargeBf = "1"; //充值备份状态(0-已充值、1-未充值)
        String resultCode = ""; //返回编号(000000-成功、999999-失败)
        DaoResult daoResultMember = new DaoResult(); //会员信息部分
        DaoResult daoResultRechargeBf = new DaoResult(); //充值备份

        try{

            sqlHxRecharge = sqlHxRecharge + " AND GLID = '"+glId+"'";
            DMap dMapHxRecharge = DaoTool.select(sqlHxRecharge); //根据glId查询充值记录

            if (dMapHxRecharge.getCount() == 0){
                DaoResult daoResultRechargeHx = baseDao.insert(huanxunRecharge,conn); //充值部分
                if (daoResultRechargeHx.getCode() < 0){
                    conn.rollback();
                    System.out.println("录入充值信息失败");
                }else{
                    System.out.println("录入充值信息成功");
                    resultCode = huanxunRecharge.getResultcode(); //返回编号(000000-成功、999999-失败)
                    if (resultCode != null && !resultCode.equals("")){
                        if (resultCode.equals("000000")){ //充值成功
                            Double ipstrdamt = huanxunRecharge.getIpstrdamt(); //用户IPS实际到账金额
                            merBillNo = huanxunRecharge.getMerbillno(); //商户订单号
                            System.out.println("实际到账金额ipstrdamt======"+ipstrdamt+",商户订单号======"+merBillNo);
                            flagRechargeBf = "0"; //已充值
                            sqlHxRechargeBf = "UPDATE huanxun_recharge_bf SET FLAG = <flagRechargeBf> WHERE MERBILLNO = <merBillNo>";
                            DMap dMapRechargeBf = new DMap();
                            dMapRechargeBf.setData("flagRechargeBf",flagRechargeBf); //充值备份状态
                            dMapRechargeBf.setData("merBillNo",merBillNo); //商户订单号
                            daoResultRechargeBf = DaoTool.update(sqlHxRechargeBf,dMapRechargeBf.getData(),conn);
                            if (daoResultRechargeBf.getCode() < 0){
                                System.out.println("充值备份失败");
                                conn.rollback();
                            }else{
                                System.out.println("充值备份成功");
                                DaoResult daoResultTradeDetail = baseDao.insert(tradeDetail,conn); //交易记录部分
                                if (daoResultTradeDetail.getCode() < 0){
                                    System.out.println("录入交易记录信息失败");
                                    conn.rollback();
                                }else{
                                    System.out.println("录入交易记录信息成功");
                                    /********** 更新流水号Start **********/
                                    serial_Number = tradeDetail.getSerialnum(); //流水号
                                    String sqlSerialNumber = "UPDATE serialNumber_wh SET NUMBER = <serial_Number> WHERE ID = '6d82cfe3347511e69b6d00163e0063ab'";
                                    DMap dmapSerial = new DMap();
                                    dmapSerial.setData("serial_Number",serial_Number);
                                    DaoResult daoResultSerial = DaoTool.update(sqlSerialNumber,dmapSerial.getData(),conn); //更新流水号
                                    /*********** 更新流水号End ***********/

                                    if(daoResultSerial.getCode() < 0){
                                        System.out.println("更新流水号失败");
                                        conn.rollback();
                                    }else{
                                        System.out.println("更新流水号成功");
                                        Double balance = tradeDetail.getBalance(); //余额
                                        balance = balance + ipstrdamt; //更新后余额
                                        DaoResult daoResultMemberMessage = baseDao.insert(memberMessage,conn); //我的消息部分
                                        if (daoResultMemberMessage.getCode() < 0){
                                            System.out.println("我的消息录入失败");
                                            conn.rollback();
                                        }else{
                                            System.out.println("我的消息录入成功");
                                            if (memberId != null && !memberId.equals("")){ //会员id
                                                String sqlMember = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
                                                DMap dmap = new DMap();
                                                dmap.setData("balance",balance); //余额
                                                dmap.setData("memberId",memberId); //会员id
                                                daoResultMember = DaoTool.update(sqlMember,dmap.getData(),conn); //会员更新余额
                                                if (daoResultMember.getCode() < 0){
                                                    System.out.println("更新用户余额失败");
                                                    conn.rollback();
                                                }else{
                                                    System.out.println("更新用户余额成功");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            conn.rollback();
        }finally {
            conn.commit();
            conn.close();
        }
        return daoResultMember;
    }
}
