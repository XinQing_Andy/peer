package com.mh.peer.dao.product;

import com.mh.peer.model.message.*;
import com.salon.frame.data.DMap;
import java.util.Map;


/**
 * Created by cuibin on 2016/5/25.
 */
public interface ProductReceiveDao {

    /**
     * 项目详情 已收详情
     */
    public DMap alreadyReceive(ProductReceiveMessageBean productReceiveMessageBean, String userId);

    /**
     * 项目详情 期数详情
     */
    public DMap phaseDetails(ProductMessageBean productMessageBean, String userId);

    /**
     * 项目详情 未收详情
     */
    public DMap notReceive(ProductReceiveMessageBean productReceiveMessageBean, String userId);

    /**
     * 收款详情
     * @param productReceiveMessageBean
     * @return
     */
    public Map getReceiveMessageBean(ProductReceiveMessageBean productReceiveMessageBean);

    /**
     * 根据条件获取回款部分集合
     * @param productReceiveMes
     * @return
     */
    public DMap getProductReceiveListBySome(ProductReceiveMessageBean productReceiveMes);

    /**
     * 投资总额
     */
    public DMap investTotal(String userId);

    /**
     * 投标中本金
     */
    public DMap inaBid(String userId);

    /**
     * 待回收本金
     */
    public DMap toBeRecycled(String userId);

    /**
     *已回收本金
     */
    public DMap retired(String userId);

    /**
     * 待回收金额 本金 利息
     */
    public DMap moneyRecycled(String userId);


    /**
     * 平均受益率
     */
    public DMap avgBenefit(String userId);

    /**
     * 获取昨日收益
     * @param appHomeMes
     * @return
     */
    DMap getLastDayPrice(AppHomeMessageBean appHomeMes);

    /**
     * 获取待收(已收)金额、待收(已收)期数(App端)
     * @param memberId
     * @param receiveFlag
     * @return
     */
    AppProductReceiveTjMessageBean getAppReceiveInfo(String memberId, String receiveFlag);

    /**
     * 获取待收(已收)列表(App端)
     * @param memberId
     * @param receiveFlag
     * @return
     */
    DMap getAppReceiveList(String memberId, String receiveFlag,String pageIndex,String pagesize);

    /**
     * 根据id获取回款信息
     * @param receiveId
     * @return
     */
    DMap getAppReceiveInfoById(String receiveId);

    /**
     * 查询需自动还款信息
     * @param productAutoRepayMes
     * @return
     */
    DMap getReceiveInfoList(ProductAutoRepayMessageBean productAutoRepayMes);
}
