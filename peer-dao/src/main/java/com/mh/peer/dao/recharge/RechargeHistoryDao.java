package com.mh.peer.dao.recharge;

import com.mh.peer.model.message.ViewHuanxunRechargeMes;
import com.salon.frame.data.DMap;

/**
 * Created by Cuibin on 2016/7/27.
 */
public interface RechargeHistoryDao {

    /**
     * 查询充值统计
     */
    public DMap queryRechargeHistory(ViewHuanxunRechargeMes viewHuanxunRechargeMes);

    /**
     * 查询充值统计 总数
     */
    public DMap queryRechargeHistoryCount(ViewHuanxunRechargeMes viewHuanxunRechargeMes);


}
