package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.SysEmailSetting;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/4/25.
 */
public interface SystemEmailDao {
    /**
     *查询邮件设置
     */
    DMap querySystemEmail();
    /**
     *  添加邮件设置
     */
    DaoResult addSystemEmail(SysEmailSetting sysEmailSetting);
    /**
     *  更新邮件设置
     */
    DaoResult updateSystemEmail(SysEmailSetting sysEmailSetting);

}
