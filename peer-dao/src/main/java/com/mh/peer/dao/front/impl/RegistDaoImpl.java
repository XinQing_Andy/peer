package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.RegistDao;
import com.mh.peer.model.entity.MemberInfo;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by cuibin on 2016/4/23.
 */
@Repository
public class RegistDaoImpl implements RegistDao{

    @Autowired
    private BaseDao baseDao;
    /**
     * 注册
     */
    @Override
    public DaoResult regist(MemberInfo memberInfo) {
        if(memberInfo == null){
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        DaoResult daoResult = baseDao.insert(memberInfo);
        //System.out.println("======================="+daoResult.getErrText());
        return daoResult;
    }
    /**
     * 激活
     */
    @Override
    public DaoResult activateUser(MemberInfo memberInfo) {
        return baseDao.update(memberInfo);
    }

    /**
     * 验证用户名是否存在
     */
    @Override
    public DMap checkUsername(String username) {
        DMap result = new DMap();
        if (username == null || username.equals("")) {
            result.setCount(1);
            return result;
        }
        result.setData("USERNAME", username);
        result = baseDao.getViewData("member_info", "USERNAME = <USERNAME>", result);
        return result;
    }
}
