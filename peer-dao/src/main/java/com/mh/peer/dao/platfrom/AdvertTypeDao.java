package com.mh.peer.dao.platfrom;

import com.mh.peer.model.entity.AdvertType;
import com.salon.frame.data.DMap;

import java.util.List;

/**
 * Created by zhangerxin on 2016-4-12.
 */
public interface AdvertTypeDao {

    /**
     * 查询所有广告类别
     * @return
     */
    DMap getAllAdvertType();
}
