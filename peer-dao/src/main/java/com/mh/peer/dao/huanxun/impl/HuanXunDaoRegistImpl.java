package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunRegistDao;
import com.mh.peer.model.entity.HuanxunRegist;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.message.HuanXunRegistMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-7-23.
 */
@Repository
public class HuanXunDaoRegistImpl implements HuanXunRegistDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 环迅部分注册
     * @param huanxunRegist
     * @param memberMessage
     * @return
     */
    @Override
    public DaoResult registerhxResult(HuanxunRegist huanxunRegist, MemberMessage memberMessage) {
        DConnection conn = DaoTool.getConnection();
        String sqlHxRegist = "SELECT * FROM huanxun_regist WHERE 1 = 1";
        String sqlMember = ""; //更新会员标志
        String memberId = memberMessage.getMemberid(); //会员id
        DaoResult daoResultHxRegist = new DaoResult();
        DaoResult daoResultMessage = new DaoResult();
        DaoResult daoResultMember = new DaoResult();

        try{

            //System.out.println("环迅注册部分........");
            if (memberId != null && !memberId.equals("")){ //会员id
                sqlHxRegist = sqlHxRegist + " AND MEMBERID = '"+memberId+"'";
                DMap dMapHxRegist = DaoTool.select(sqlHxRegist);
                if (dMapHxRegist.getCount() == 0){
                    daoResultHxRegist = baseDao.insert(huanxunRegist,conn); //环迅注册部分
                    if (daoResultHxRegist.getCode() < 0){
                        conn.rollback();
                    }else{
                        //System.out.println("我的消息部分........");
                        daoResultMessage = baseDao.insert(memberMessage,conn); //我的消息部分
                        if (daoResultMessage.getCode() < 0){
                            conn.rollback();
                        }else{
                            sqlMember = "UPDATE member_info SET REAL_FLG = '0' WHERE ID = <id>";
                            DMap dmap = new DMap();
                            dmap.setData("id",memberId); //会员id
                            daoResultMember = DaoTool.update(sqlMember,dmap.getData(),conn);
                            if (daoResultMember.getCode() < 0){
                                conn.rollback();
                            }
                           // System.out.println("sqlMember======"+sqlMember);
                            //System.out.println("更新时memberId======"+memberId);
                           // System.out.println("daoResultMember.getCode()======"+daoResultMember.getCode());
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            conn.rollback();
        }finally {
            conn.commit();
            conn.close();
        }
        return daoResultMessage;
    }


    /**
     * 根据条件查询会员注册信息
     * @param huanXunRegistMessageBean
     * @return
     */
    @Override
    public DMap getRegisterhxBySome(HuanXunRegistMessageBean huanXunRegistMessageBean) {
        String memberId = huanXunRegistMessageBean.getMemberId(); //会员id
        String ipsAcctNo = huanXunRegistMessageBean.getIpsAcctNo(); //IPS虚拟账户
        String sql = "SELECT * FROM view_huanxun_regist WHERE 1 = 1";
        if (memberId != null && !memberId.equals("")){ //会员id
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        if (ipsAcctNo != null && !ipsAcctNo.equals("")){ //IPS虚拟账户
            sql = sql + " AND IPSACCTNO = '"+ipsAcctNo+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
