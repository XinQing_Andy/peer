package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.ConTractDao;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoTool;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-8-31.
 * 合同部分
 */
@Repository
public class ConTractDaoImpl implements ConTractDao{

    /**
     * 获取产品信息
     * @param productId
     * @return
     */
    @Override
    public DMap getProductInfo(String productId) {
        String sql = "SELECT * FROM view_product_content_min WHERE ID = '"+productId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 获取产品投资信息集合
     * @param productId
     * @return
     */
    @Override
    public DMap getProductBuyInfoList(String productId) {
        String sql = "SELECT * FROM view_product_buy_min WHERE PRODUCTID = '"+productId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取债权信息
     * @param attornId
     * @return
     */
    @Override
    public DMap getAttornInfo(String attornId) {
        String sql = "SELECT * FROM view_product_attorn_min WHERE ID = '"+attornId+"'";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
