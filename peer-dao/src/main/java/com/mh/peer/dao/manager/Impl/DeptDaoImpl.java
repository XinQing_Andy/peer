package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.DeptDao;
import com.mh.peer.model.entity.SysDept;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zhaojiaming on 2016/1/14.
 */
@Repository
public class DeptDaoImpl implements DeptDao {
    @Autowired
    private BaseDao baseDao;

    /**
     * 获取部门信息
     * @param sysDept
     * @return
     */
    @Override
    public SysDept getDept(SysDept sysDept) {
        return (SysDept)baseDao.getById(sysDept);
    }

    /**
     * 获取所有部门信息
     * @param sysDept
     * @return
     */
    @Override
    public List<SysDept> getDeptInfo(SysDept sysDept) {
        List deptList = baseDao.getListBySome(SysDept.class, "IS_DELETE", "0");
        if (deptList.size() == 0) {
            return deptList;
        }
        return deptList;
    }

    /**
     * 新增部门信息
     * @param sysDept
     * @return
     */
    @Override
    public DaoResult addDept(SysDept sysDept) {
        DaoResult result = new DaoResult();
        if (sysDept == null) {
            result.setErrText("添加失败");
            result.setCode(-1);
            return result;
        }
        result = baseDao.insert(sysDept);
        return result;
    }

    @Override
    public DaoResult updateDept(SysDept sysDept) {
//        DConnection conn = DaoTool.getConnection();
        DaoResult result = new DaoResult();
        if (sysDept == null) {
            result.setCode(-1);
            result.setErrText("修改失败");
            return result;
        }
        result = baseDao.update(sysDept);
        return result;
    }

    @Override
    public DaoResult delDept(SysDept sysDept) {
        DaoResult result = new DaoResult();
        if (sysDept == null) {
            result.setCode(-1);
            result.setErrText("删除失败");
            return result;
        }
        result = baseDao.delete(sysDept);
        return result;
    }
}
