package com.mh.peer.dao.manager;

import com.mh.peer.model.entity.SysSecurityParameter;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zhangerxin on 2016-4-1.
 */
public interface SysSecurityParameterDao {

    /**
     * 添加安全问题
     * @param sysSecurityParameter
     * @return
     */
    DaoResult addSysSecurityParameter(SysSecurityParameter sysSecurityParameter);

    /**
     * 修改安全问题
     * @param sysSecurityParameter
     * @return
     */
    DaoResult updateSecurityParameter(SysSecurityParameter sysSecurityParameter);

    /**
     * 查询安全问题
     * @param sysSecurityParameter
     * @return
     */
    List<SysSecurityParameter> querySecurityParameter(SysSecurityParameter sysSecurityParameter);
}
