package com.mh.peer.dao.manager.Impl;

import com.mh.peer.dao.manager.SysFileDao;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.SysFileMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.File;

/**
 * Created by zhangerxin on 2016-4-10.
 */
@Repository
public class SysFileDaoImpl implements SysFileDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 上传附件
     * @param sysFile
     * @return
     */
    @Override
    public DaoResult saveProduct(SysFile sysFile) {
        DaoResult result = new DaoResult();
        if (sysFile == null || sysFile == null) { //附件类
            result.setCode(-1);
            result.setErrText("上传附件失败！");
            return result;
        }
        DConnection conn = DaoTool.getConnection();
        result = baseDao.insert(sysFile, conn); //附件
        if (result.getCode() < 0) {
            conn.rollback();
            conn.close();
            return result;
        }
        conn.commit();
        conn.close();
        return result;
    }


    /**
     * 删除附件
     * @param sysFile
     * @return
     */
    @Override
    public DaoResult delProduct(SysFile sysFile) {
        String jdPath = sysFile.getImgurljd(); //绝对路径
        DaoResult result = new DaoResult();
        if (sysFile == null) {
            result.setCode(-1);
            result.setErrText("删除失败");
            return result;
        }
        result = baseDao.delete(sysFile);

        /********** 删除实际附件Start **********/
        if(jdPath != null){
            File file = new File(jdPath);
            file.delete();
        }
        /*********** 删除实际附件End ***********/
        return result;
    }

    /**
     * 根据glid获取一条附件信息
     * @param glId
     * @return
     */
    @Override
    public DMap querySysFileById(String glId) {
        String sql = "SELECT ID,GLID,IMGURL,IMGURLJD,TYPE,SORT,CREATEUSER,CREATETIME FROM sys_file WHERE 1 = 1";
        if(glId != null && !glId.equals("")){
            sql = sql + " AND GLID = '"+glId+"'";
        }
        sql = sql + " LIMIT 0,1";
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 根据关联id查询所有附件
     * @param glId
     * @return
     */
    @Override
    public DMap queryAllSysFile(String glId) {
        String sql = "SELECT ID,GLID,IMGURL,IMGURLJD,TYPE,SORT,CREATEUSER,CREATETIME FROM sys_file WHERE 1 = 1";
        if(glId != null && !glId.equals("")){
            sql = sql + " AND GLID = '"+glId+"'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }

    /**
     * 查询一条附件信息
     * @param sysFileMes
     * @return
     */
    @Override
    public DMap getOnlySysFile(SysFileMessageBean sysFileMes) {
        String sql = "SELECT * FROM sys_file WHERE 1 = 1";
        String orderColumn = sysFileMes.getOrderColumn(); //排序字段
        String orderType = sysFileMes.getOrderType(); //排序类型
        String pageIndex = sysFileMes.getPageIndex(); //当前数
        String pageSize = sysFileMes.getPageSize(); //每次显示数量
        String glId = sysFileMes.getGlid(); //关联id

        if(glId != null && !glId.equals("")){ //关联id
            sql = sql + " AND GLID = '"+glId+"'";
        }
        if(orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if(pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 根据关联id获取附件地址(App部分)
     * @param glId
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public DMap getAppSysFile(String glId, String orderColumn, String orderType, String pageIndex, String pageSize) {
        String sql = "SELECT * FROM sys_file WHERE 1 = 1";
        if (glId != null && !glId.equals("")){ //关联id
            sql = sql + " AND GLID = '"+glId+"'";
        }
        if (orderType != null && !orderType.equals("") && orderColumn != null && !orderColumn.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


}
