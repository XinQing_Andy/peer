package com.mh.peer.dao.huanxun;

import com.mh.peer.model.entity.HuanxunFreezeAttorn;
import com.mh.peer.model.message.HuanXunFreezeMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.util.DaoResult;

/**
 * Created by Cuibin on 2016/7/29.
 */
public interface HuanXunFreezeDao {

    /**
     * 查询冻结金额
     */
    public DMap queryFreeze(String memberId);

    /**
     * 产品投资部分
     * @param huanXunFreezeMes
     * @return
     */
    public DMap getHuanXunFreezeInfoList(HuanXunFreezeMessageBean huanXunFreezeMes);


    /**
     * 查询此项目所有投资记录 bycuibin
     */
    public DMap queryAllfreeze(String productId);

    /**
     * 按照冻结表id查询记录 bycuibin
     */
    public DMap queryOnlyfreeze(String id);

    /**
     * 更新环迅冻结状态 bycuibin
     */
    public DaoResult updateHuanXunFreeze(String id);

    /**
     * 更改产品审核状态 bycuibin
     */
    public DaoResult updateProductFlag(String productId);

    /**
     * 按照id查询债权转让记录 bycuibin
     */
    public DMap queryOnlyFreezeAttorn(String attornid);

    /**
     * 更新债权转让冻结状态 bycuibin
     */
    public DaoResult updateUnfreezeAttorn(String id);

    /**
     * 查询债权转让冻结记录 bycuibin
     */
    public DMap queryFreezeAttorn(String id);

    /**
     * 查询债权转让记录 bycuibin
     */
    public DMap queryFreezeAttornCount(String id);

    /**
     * 环迅还款信息冻结
     * @param freezeRepayId
     * @return
     */
    DMap queryFreezeRepay(String freezeRepayId);
}
