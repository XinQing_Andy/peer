package com.mh.peer.dao.news.Impl;

import com.mh.peer.dao.news.NewsDao;
import com.mh.peer.model.entity.NewsContent;
import com.mh.peer.model.entity.NewsType;
import com.mh.peer.model.entity.SysFile;
import com.mh.peer.model.message.NewsContentMessageBean;
import com.mh.peer.model.message.NewsTypeMessageBean;
import com.mh.peer.model.message.WeathInformationMessageBean;
import com.mh.peer.model.message.WeixinNewsMes;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Cuibin on 2016/4/7.
 */
@Repository
public class NewsDaoImpl implements NewsDao {
    @Autowired
    private BaseDao baseDao;

    /**
     * 查询数量
     */
    @Override
    public DMap queryNewsByPageCount(NewsContentMessageBean newsContentM) {
        String keyword = newsContentM.getKeyword();//关键字
        String kindId = TypeTool.getString(newsContentM.getKindId());//类别
        String sort = newsContentM.getSort();
        String firstType = newsContentM.getFirstType();
        String sql = "";
        if (firstType != null && !firstType.equals("")) {
            sql = "SELECT * FROM view_news WHERE 1 = 1";
        } else {
            sql = "SELECT * FROM news_content WHERE 1 = 1";
        }
        if (keyword != null && !keyword.equals("")) {
            sql = sql + " and title LIKE '%" + keyword + "%'";
        }
        if (!"0".equals(kindId) && !("").equals(kindId) && null != kindId) {
            sql = sql + " and kind_id = '" + kindId + "'";
        }
        if (firstType != null && !firstType.equals("")) {
            sql = sql + " and firstType = '" + firstType + "'";
        }
        if (sort != null && !sort.equals("") && !"默认排序".equals(sort)) {
            if ("只看推荐".equals(sort)) {
                sql = sql + " and is_recommend = 1";
            } else if ("只看不显示".equals(sort)) {
                sql = sql + " and availability = 0";
            }
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 内容管理(查询所有新闻内容 带分页)
     */
    @Override
    public DMap queryNewsByPage(NewsContentMessageBean newsContentMes) {
        String sql = "SELECT * FROM view_news WHERE 1 = 1";
        String firstTypeId = newsContentMes.getFirstType(); //第一类别
        String secondType = newsContentMes.getKindId(); //第二类别
        String title = newsContentMes.getTitle(); //题目
        String orderType = newsContentMes.getOrderType(); //排序类型
        String orderColumn = newsContentMes.getOrderColumn(); //排序字段
        String pageIndex = newsContentMes.getPageIndex(); //起始数
        String pageSize = newsContentMes.getPageSize(); //每次显示数量
        DMap dmap = new DMap();

        try{

            if (firstTypeId != null && !firstTypeId.equals("")){ //第一类别
                sql = sql + " AND firstType = '"+firstTypeId+"'";
            }
            if (secondType != null && !secondType.equals("")){ //第二级别
                sql = sql + " AND secondType = '"+secondType+"'";
            }
            if (title != null && !title.equals("")){ //题目
                sql = sql + " AND title LIKE '%"+title+"%'";
            }
            if (orderType != null && !orderType.equals("") && orderColumn != null && !orderColumn.equals("")){
                sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
            }
            if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
                sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
            }
            dmap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dmap;
    }


    /**
     * 查询新闻数量
     * @param newsContentMes
     * @return
     */
    @Override
    public DMap queryNewsCount(NewsContentMessageBean newsContentMes) {
        String sql = "SELECT * FROM view_news WHERE 1 = 1";
        String firstTypeId = newsContentMes.getFirstType(); //第一类别
        String secondType = newsContentMes.getKindId(); //第二类别
        String title = newsContentMes.getTitle(); //题目
        DMap dmap = new DMap();

        try{

            if (firstTypeId != null && !firstTypeId.equals("")){ //第一类别
                sql = sql + " AND firstType = '"+firstTypeId+"'";
            }
            if (secondType != null && !secondType.equals("")){ //第二级别
                sql = sql + " AND secondType = '"+secondType+"'";
            }
            if (title != null && !title.equals("")){ //题目
                sql = sql + " AND title LIKE '%"+title+"%'";
            }
            dmap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dmap;
    }


    /**
     * 类别管理(查询所有类别)
     */
    @Override
    public DMap queryType() {
        return baseDao.getViewData("news_type");
    }

    /**
     * 类别管理-编辑后保存
     */
    @Override
    public DaoResult saveType(NewsType newsType) {
        return baseDao.update(newsType);
    }

    /**
     * 内容管理 删除新闻
     */
    @Override
    public DaoResult deleteNews(NewsContent newsContent) {
        return baseDao.delete(newsContent);
    }

    /**
     * 添加新闻
     */
    @Override
    public DaoResult saveNews(NewsContent newsContent) {
        return baseDao.insert(newsContent);
    }

    /**
     * 内容管理  保存图片信息
     */
    @Override
    public DaoResult saveImg(SysFile sysFile) {
        return baseDao.insert(sysFile);
    }

    /**
     * 查询单个新闻
     */
    @Override
    public DMap queryNewsById(NewsContentMessageBean newsContent) {
        DMap result = new DMap();
        result.setData("id", newsContent.getId());
        result = baseDao.getViewData("view_news", "id=<id>", result);
        return result;
    }

    /**
     * 内容管理 修改新闻 查询图片信息
     */
    @Override
    public DMap queryImg(String newsId) {
        DMap result = new DMap();
        result.setData("GLID", newsId);
        result = baseDao.getViewData("sys_file", "GLID=<GLID>", result);
        return result;
    }

    /**
     * 内容管理 修改新闻 (提交新闻)
     */
    @Override
    public DaoResult updateNews(NewsContent newsContent) {
        return baseDao.update(newsContent);
    }

    /**
     * 内容管理 删除图片
     */
    @Override
    public DaoResult deleteImg(SysFile sysFile) {
        return baseDao.delete(sysFile);
    }

    /**
     * 内容管理 查询新闻分类
     */
    @Override
    public DMap queryNewsType(NewsTypeMessageBean newsTypeMes) {
        String sql = "SELECT * FROM news_type WHERE 1 = 1";
        String fid = newsTypeMes.getFid(); //父id
        if (fid != null && !fid.equals("")){
            sql = sql + " AND fid = '"+TypeTool.getInt(fid)+"'";
        }else{
            sql = sql + " AND fid = 0";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 修改新闻是否显示
     */
    @Override
    public DaoResult updateShow(NewsContent newsContent) {
        return baseDao.update(newsContent);
    }

    /**
     * 修改新闻是否推荐
     */
    @Override
    public DaoResult updateRecommend(NewsContent newsContent) {
        return baseDao.update(newsContent);
    }

    /**
     * 添加二级类别
     */
    @Override
    public DaoResult addTwoType(NewsType newsType) {
        return baseDao.insert(newsType);
    }

    /**
     * 删除二级类别
     */
    @Override
    public DaoResult deleteTwoNewsType(NewsType newsType) {
        return baseDao.delete(newsType);
    }

    /**
     * 更新新闻访问次数(用户端)
     */
    @Override
    public DaoResult updateNewsVisit(NewsContent newsContent) {
        if (newsContent == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        return baseDao.update(newsContent);
    }

    /**
     * 获取app端新闻(App端)
     *
     * @param orderColumn
     * @param orderType
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public DMap getAppNews(String orderColumn, String orderType, String pageIndex, String pageSize) {
        String sql = "SELECT * FROM news_content WHERE 1 = 1";
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")) {
            sql = sql + " ORDER BY " + orderColumn + " " + orderType + "";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取新闻部分详情(App端)
     * @param newsId
     * @return
     */
    @Override
    public DMap getAppNewsDetails(String newsId) {
        String sql = "SELECT * FROM view_news_content_app WHERE 1 = 1";
        if (newsId != null && !newsId.equals("")){
            sql = sql + " AND ID = '"+newsId+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取新闻列表(微信) by cuibin
     */
    @Override
    public DMap queryNewsList(WeixinNewsMes weixinNewsMes) {
        if (weixinNewsMes == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }

        String pageIndex = weixinNewsMes.getPageIndex();
        String pageSize = weixinNewsMes.getPageSize();
        if (("").equals(pageIndex) || null == pageIndex || ("").equals(pageSize) || null == pageSize) {
            pageIndex = "1";
            pageSize = "8";
        }
        String sql = "SELECT * FROM news_content WHERE 1 = 1 ORDER BY create_date DESC";

        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dMap = DaoTool.select(sql);
        //System.out.println("===============" + dMap.getErrText());
        return dMap;
    }

    /**
     * 查询一个新闻(微信) by cuibin
     */
    @Override
    public DMap queryOnlyNews(String id) {
        if (id == null || id.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        String sql = "select * from news_content WHERE id = '" + id + "'";
        return DaoTool.select(sql);
    }

    /**
     * 获取新闻列表总数(微信) by cuibin
     */
    @Override
    public DMap queryNewsCount(WeixinNewsMes weixinNewsMes) {
        if (weixinNewsMes == null) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }

        String sql = "SELECT count(*) as count FROM news_content WHERE 1 = 1";

        return DaoTool.select(sql);
    }


    /**
     * 搜索财富资讯
     * @param weathInformationMes
     * @return
     */
    @Override
    public DMap queryWeathByPage(WeathInformationMessageBean weathInformationMes) {
        String sql = "SELECT * FROM view_news WHERE 1 = 1";
        String firstTypeId = weathInformationMes.getFirstTypeId(); //一级类别id
        String secondTypeId = weathInformationMes.getSecondTypeId(); //二级类别id
        String orderType = weathInformationMes.getOrderType(); //排序类型
        String orderColumn = weathInformationMes.getOrderColumn(); //排序字段
        String pageIndex = weathInformationMes.getPageIndex(); //起始数
        String pageSize = weathInformationMes.getPageSize(); //每页显示数量
        DMap dMap = new DMap();

        try{

            if (firstTypeId != null && !firstTypeId.equals("")){ //一级类别id
                if (!firstTypeId.equals("0")){
                    sql = sql + " AND firstType = "+TypeTool.getInt(firstTypeId)+"";
                }
            }
            if (secondTypeId != null && !secondTypeId.equals("")){ //二级类别id
                sql = sql + " AND secondType = "+TypeTool.getInt(secondTypeId)+"";
            }
            if (orderType != null && !orderType.equals("") && orderColumn != null && !orderColumn.equals("")){
                sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
            }
            if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")){
                sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+","+TypeTool.getInt(pageSize)+"";
            }
            dMap = DaoTool.select(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 搜索财富资讯数量
     * @param weathInformationMes
     * @return
     */
    @Override
    public DMap queryWeathCount(WeathInformationMessageBean weathInformationMes) {
        String sql = "SELECT * FROM view_news WHERE 1 = 1";
        String firstTypeId = weathInformationMes.getFirstTypeId(); //类别id
        DMap dMap = new DMap();

        try{

            if (firstTypeId != null && !firstTypeId.equals("")){ //一级类别id
                if (!firstTypeId.equals("0")){
                    sql = sql + " AND firstType = "+TypeTool.getInt(firstTypeId)+"";
                }
            }
            dMap = DaoTool.select(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


    /**
     * 搜索财富资讯详细信息
     * @param weathInformationMes
     * @return
     */
    @Override
    public DMap queryWeathDetails(WeathInformationMessageBean weathInformationMes) {
        String id = weathInformationMes.getId(); //主键
        String sql = "SELECT * FROM view_news WHERE id = '"+id+"'";
        DMap dMap = new DMap();

        try{

            dMap = DaoTool.select(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        return dMap;
    }


}
