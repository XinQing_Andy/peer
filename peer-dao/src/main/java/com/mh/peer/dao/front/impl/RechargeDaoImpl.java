package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.RechargeDao;
import com.mh.peer.model.entity.ChargeRecord;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.SerialnumberWh;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.RechargeHistoryMessageBean;
import com.mh.peer.model.message.ViewHuanxunRechargeMes;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/6/17.
 */

@Repository
public class RechargeDaoImpl implements RechargeDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 充值
     */
    @Override
    public boolean userRecharge(ChargeRecord chargeRecord, TradeDetail tradeDetail, MemberInfo memberInfo, SerialnumberWh serialnumberWh) {
        DConnection conn = DaoTool.getConnection();
        DaoResult resultCharge = new DaoResult();
        DaoResult resultTrade = new DaoResult();
        DaoResult resultMember = new DaoResult();
        DaoResult resultSerial = new DaoResult();

        try {
            resultCharge = baseDao.insert(chargeRecord, conn); //充值表
            try {
                resultTrade = baseDao.insert(tradeDetail, conn); //流水表
            } catch (Exception e) {
                e.printStackTrace();
            }

            resultMember = baseDao.update(memberInfo, conn); //会员表
            resultSerial = baseDao.update(serialnumberWh, conn); //流水号表

            if (resultCharge.getCode() < 0) {
                conn.rollback();
                return false;
            }
            if (resultTrade.getCode() < 0) {
                conn.rollback();
                return false;
            }
            if (resultMember.getCode() < 0) {
                return false;
            }
            if (resultSerial.getCode() < 0) {
                conn.rollback();
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            conn.rollback();
        } finally {
            conn.commit();
            conn.close();
        }
        return true;
    }

    /**
     * 查询充值记录
     */
    @Override
    public DMap queryRechargeHistory(RechargeHistoryMessageBean rechargeHistoryMessageBean) {
        String sql = "select * from view_charge_record where 1 = 1";
        //类别id
        String pageIndex = rechargeHistoryMessageBean.getPageIndex(); //当前页
        if(pageIndex.equals("1")){
            pageIndex = "0";
        }
        String pageSize = rechargeHistoryMessageBean.getPageSize(); //每页显示数量
        String startTime = rechargeHistoryMessageBean.getStartTime(); //开始时间
        String endTime = rechargeHistoryMessageBean.getEndTime(); //结束时间
        startTime = startTime == null ? null : startTime.replaceAll("-", "");
        endTime = endTime == null ? null : endTime.replaceAll("-", "");
        String memberId = rechargeHistoryMessageBean.getMemberid(); //会员id

        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " and MEMBERID = '" + memberId + "'";
        }
        if (startTime != null && !startTime.equals("")) {
            sql = sql + " and TIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) {
            sql = sql + " and TIME <= '" + endTime + "'";
        }
        sql = sql + " order by CREATETIME DESC";
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + TypeTool.getInt(pageIndex) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
        System.out.println("===================" + sql);
        return dmap;
    }

    /**
     * 查询充值记录总数
     */
    @Override
    public DMap queryRechargeHistoryCount(RechargeHistoryMessageBean rechargeHistoryMessageBean) {
        String sql = "select count(*) as count from view_charge_record where 1 = 1";
        //类别id
        String startTime = rechargeHistoryMessageBean.getStartTime(); //开始时间
        String endTime = rechargeHistoryMessageBean.getEndTime(); //结束时间
        startTime = startTime == null ? null : startTime.replaceAll("-", "");
        endTime = endTime == null ? null : endTime.replaceAll("-", "");
        String memberId = rechargeHistoryMessageBean.getMemberid(); //会员id

        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " and MEMBERID = '" + memberId + "'";
        }
        if (startTime != null && !startTime.equals("")) {
            sql = sql + " and TIME >= '" + startTime + "'";
        }
        if (endTime != null && !endTime.equals("")) {
            sql = sql + " and TIME <= '" + endTime + "'";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;

    }

    /**
     * 查询 实际金额 和 充值金额
     */
    @Override
    public DMap queryRechargeSum(String memberId) {
        if (memberId == null || memberId.equals("")) {
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select sum(SPRICE) SPRICESUM,sum(PROCEDUREFEE) PROCEDUREFEESUM from view_charge_record where MEMBERID = '" + memberId + "'");
    }


    /**
     * 查询充值记录
     * @param viewHuanxunRechargeMes
     * @return
     */
    @Override
    public DMap queryViewRechargeHistory(ViewHuanxunRechargeMes viewHuanxunRechargeMes) {
        String sql = "SELECT * FROM view_huanxun_recharge WHERE 1 = 1";
        String pageIndex = viewHuanxunRechargeMes.getPageIndex(); //起始数
        String pageSize = viewHuanxunRechargeMes.getPageSize(); //每页显示数量
        String orderColumn = viewHuanxunRechargeMes.getOrderColumn(); //排序字段
        String orderType = viewHuanxunRechargeMes.getOrderType(); //排序类型
        String startTime = viewHuanxunRechargeMes.getStartTime(); //充值起时间
        String endTime = viewHuanxunRechargeMes.getEndTime(); //充值止时间
        String memberId = viewHuanxunRechargeMes.getMemberid(); //会员id

        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        if (startTime != null && !startTime.equals("")) { //充值起时间
            sql = sql + " AND DATE(CREATETIME) >= DATE('"+startTime+"')";
        }
        if (endTime != null && !endTime.equals("")) { //充值止时间
            sql = sql + " AND DATE(CREATETIME) <= DATE('"+endTime+"')";
        }
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT "+(TypeTool.getInt(pageIndex)-1)+ ","+TypeTool.getInt(pageSize)+"";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 充值记录数量
     * @param viewHuanxunRechargeMes
     * @return
     */
    @Override
    public DMap queryViewRechargeHistoryCount(ViewHuanxunRechargeMes viewHuanxunRechargeMes) {
        String sql = "SELECT * FROM view_huanxun_recharge WHERE 1 = 1";
        String startTime = viewHuanxunRechargeMes.getStartTime(); //充值起时间
        String endTime = viewHuanxunRechargeMes.getEndTime(); //充值止时间
        String memberId = viewHuanxunRechargeMes.getMemberid(); //会员id

        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " AND MEMBERID = '"+memberId+"'";
        }
        if (startTime != null && !startTime.equals("")) { //充值起时间
            sql = sql + " AND DATE(CREATETIME) >= DATE('"+startTime+"')";
        }
        if (endTime != null && !endTime.equals("")) { //充值止日期
            sql = sql + " AND DATE(CREATETIME) <= DATE('"+endTime+"')";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }
}
