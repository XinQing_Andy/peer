package com.mh.peer.dao.huanxun.impl;

import com.mh.peer.dao.huanxun.HuanXunPostalDao;
import com.mh.peer.model.entity.HuanxunPostal;
import com.mh.peer.model.entity.MemberMessage;
import com.mh.peer.model.entity.TradeDetail;
import com.mh.peer.model.message.HuanXunPostalMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.DConnection;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangerxin on 2016-7-26.
 */
@Repository
public class HuanXunPostalDaoImpl implements HuanXunPostalDao{

    @Autowired
    private BaseDao baseDao;

    /**
     * 环迅部分提现
     * @param huanxunPostal
     * @param memberMessage
     * @param tradeDetail
     * @return
     */
    @Override
    public DaoResult postalhxResult(HuanxunPostal huanxunPostal, MemberMessage memberMessage, TradeDetail tradeDetail) {
        DConnection conn = DaoTool.getConnection();
        String sqlHxpostal = "SELECT * FROM huanxun_postal WHERE 1 = 1";
        String sqlPostalBf = ""; //更新提现备份状态
        String glId = huanxunPostal.getGlid(); //环迅充值部分关联id
        String memberId = huanxunPostal.getMemberid(); //会员id
        int serial_Number = 0; //流水号
        String resultCode = ""; //响应状态(000000-成功、999999-失败)
        String merBillNo = huanxunPostal.getMerbillno(); //商户订单号
        String flagPostalBf = "1"; //备份状态(0-已提现、1-待提现)
        DaoResult daoResultMember = new DaoResult();
        DaoResult daoResultPostalBf = new DaoResult(); //提现部分备份
        DMap dMapPostalBf = new DMap(); //提现备份

        try{

            sqlHxpostal = sqlHxpostal + " AND GLID = '"+glId+"'";
            DMap dMapHxpostal = DaoTool.select(sqlHxpostal); //根据glId查询提现记录

            if (dMapHxpostal.getCount() == 0){ //可以录入信息
                DaoResult daoResultPostalHx = baseDao.insert(huanxunPostal,conn); //提现部分
                if (daoResultPostalHx.getCode() < 0){
                    conn.rollback();
                    System.out.println("录入提现信息失败");
                }else{
                    System.out.println("录入提现信息成功");
                    resultCode = huanxunPostal.getResultcode(); //响应状态(000000-成功、1-999999)
                    if (resultCode != null && !resultCode.equals("")){
                        if (resultCode.equals("000000")){
                            flagPostalBf = "0"; //已提现
                            sqlPostalBf = "UPDATE huanxun_postal_bf SET FLAG = <flagPostalBf> WHERE MERBILLNO = <merBillNo>";
                            dMapPostalBf.setData("flagPostalBf",flagPostalBf); //备份状态(0-已提现、1-待提现)
                            dMapPostalBf.setData("merBillNo",merBillNo); //商户订单号
                            daoResultPostalBf = DaoTool.update(sqlPostalBf,dMapPostalBf.getData(),conn);
                            if (daoResultPostalBf.getCode() < 0){
                                System.out.println("提现备份信息更新失败");
                                conn.rollback();
                            }else{
                                System.out.println("提现备份信息更新成功");
                                Double ipstrdamt = huanxunPostal.getIpstrdamt(); //用户IPS实际到账金额
                                Double ipsFee = huanxunPostal.getIpsfee(); //IPS手续费
                                Double merFee = huanxunPostal.getMerfee(); //平台手续费
                                Double balance = tradeDetail.getBalance(); //余额
                                System.out.println("更新前余额balance======"+balance);
                                balance = balance - ipstrdamt - merFee - ipsFee; //更新后余额
                                System.out.println("更新后余额balance======"+balance);
                                tradeDetail.setBalance(balance); //余额
                                System.out.println("实际到账金额ipstrdamt======"+ipstrdamt+",平台手续费======"+merFee);
                                DaoResult daoResultTradeDetail = baseDao.insert(tradeDetail,conn);
                                if (daoResultTradeDetail.getCode() < 0){
                                    System.out.println("录入交易记录信息失败");
                                    conn.rollback();
                                }else{
                                    System.out.println("录入交易记录信息成功");
                                    /********** 更新流水号Start **********/
                                    serial_Number = tradeDetail.getSerialnum(); //流水号
                                    String sqlSerialNumber = "UPDATE serialNumber_wh SET NUMBER = <serial_Number> WHERE ID = '6d82cfe3347511e69b6d00163e0063ab'";
                                    DMap dmapSerial = new DMap();
                                    dmapSerial.setData("serial_Number",serial_Number);
                                    DaoResult daoResultSerial = DaoTool.update(sqlSerialNumber,dmapSerial.getData(),conn); //更新流水号
                                    /*********** 更新流水号End ***********/

                                    if(daoResultSerial.getCode() < 0){
                                        System.out.println("更新流水号失败");
                                        conn.rollback();
                                    }else{
                                        System.out.println("更新流水号成功");
                                        DaoResult daoResultMemberMessage = baseDao.insert(memberMessage,conn); //我的消息部分
                                        if (daoResultMemberMessage.getCode() < 0){
                                            System.out.println("我的消息发送失败");
                                            conn.rollback();
                                        }else{
                                            System.out.println("我的消息发送成功");
                                            if (memberId != null && !memberId.equals("")){ //会员id
                                                String sqlMember = "UPDATE member_info SET BALANCE = <balance> WHERE ID = <memberId>";
                                                DMap dmap = new DMap();
                                                dmap.setData("balance",balance); //余额
                                                dmap.setData("memberId",memberId); //会员id
                                                daoResultMember = DaoTool.update(sqlMember,dmap.getData(),conn); //会员更新余额
                                                if (daoResultMember.getCode() < 0){
                                                    System.out.println("更新余额失败");
                                                    conn.rollback();
                                                }else{
                                                    System.out.println("更新余额成功");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            conn.rollback();
        }finally {
            conn.commit();
            conn.close();
        }
        return daoResultMember;
    }


    /**
     * 获取提现备份信息
     * @param huanXunPostalMes
     * @return
     */
    @Override
    public DMap getPostalBfInfo(HuanXunPostalMessageBean huanXunPostalMes) {
        String sql = "SELECT * FROM huanxun_postal_bf WHERE 1 = 1";
        String merBillNo = huanXunPostalMes.getMerbillno(); //商户订单号
        if (merBillNo != null && !merBillNo.equals("")){
            sql = sql + " AND MERBILLNO = '"+merBillNo+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
}
