package com.mh.peer.dao.member.impl;

import com.mh.peer.dao.member.AllMemberDao;
import com.mh.peer.model.entity.HuanxunFreeze;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.message.AppMemberMessageBean;
import com.mh.peer.model.message.FinancingMessageBean;
import com.mh.peer.model.message.MemberInfoMessageBean;
import com.mh.peer.model.message.MyAccountMessageBean;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import com.salon.frame.util.TypeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cuibin on 2016/4/20.
 */
@Repository
public class AllMemberDaoImpl implements AllMemberDao {

    @Autowired
    private BaseDao baseDao;

    /**
     * 根据分页查询会员信息
     *
     * @param memberInfoMes
     * @return
     */
    @Override
    public DMap queryMemberBySome(MemberInfoMessageBean memberInfoMes) {
        String pageIndex = memberInfoMes.getPageIndex(); //当前页
        String pageSize = memberInfoMes.getPageSize(); //每页显示数量

        String sql = "SELECT ID,USERNAME,NICKNAME,REALNAME,PASSWORD,IDCARD,TELEPHONE,EMAIL FROM view_member_info WHERE 1 = 1";
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 获取会员数量
     */
    @Override
    public DMap queryCountMember() {
        return DaoTool.select("select * from view_member_info where 1 = 1");
    }


    /**
     * 所有会员 查询所有数量
     */
    @Override
    public DMap queryAllMemberCount(MemberInfoMessageBean memberInfoMessageBean) {
        String keyWord = memberInfoMessageBean.getKeyWord(); //关键字
        String startTime = memberInfoMessageBean.getStartTime();//开始时间
        String endTime = memberInfoMessageBean.getEndTime();//结束时间
        String lockFlg = memberInfoMessageBean.getLockFlg();//是否锁定

        String sql = "SELECT * FROM view_member_info WHERE 1 = 1";
        if (lockFlg != null && !lockFlg.equals("")) {
            sql = sql + " and LOCK_FLG = '0'";
        }
        if (keyWord != null && !keyWord.equals("")) { //关键字
            sql = sql + " AND (NICKNAME LIKE '%" + keyWord + "%' OR EMAIL LIKE '%" + keyWord + "%' OR TELEPHONE LIKE '%" + keyWord + "%')";
        }
        if (startTime != null && !startTime.equals("") && (endTime == null || endTime.equals(""))) {
            sql = sql + " AND REPLACE(SUBSTR(CREATETIME,1,10),'-','') > " + startTime;
        }
        if ((startTime == null || startTime.equals("")) && endTime != null && !endTime.equals("")) {
            sql = sql + " AND REPLACE(SUBSTR(CREATETIME,1,10),'-','') < " + endTime;
        }
        if (startTime != null && !startTime.equals("") && endTime != null && !endTime.equals("")) {
            sql = sql + " AND REPLACE(SUBSTR(CREATETIME,1,10),'-','') between " + startTime + " and " + endTime + "";
        }

        DMap dmap = DaoTool.select(sql);
        return dmap;
    }


    /**
     * 所有会员 查询所有
     */
    @Override
    public DMap queryAllMember(MemberInfoMessageBean memberInfoMessageBean) {
        String pageIndex = memberInfoMessageBean.getPageIndex(); //当前页
        String pageSize = memberInfoMessageBean.getPageSize(); //每页显示数量
        String keyWord = memberInfoMessageBean.getKeyWord(); //关键字
        String startTime = memberInfoMessageBean.getStartTime();//开始时间
        String endTime = memberInfoMessageBean.getEndTime();//结束时间
        String sort = memberInfoMessageBean.getSort();//排序
        String lockFlg = memberInfoMessageBean.getLockFlg();//是否锁定
        String isDesc = memberInfoMessageBean.getIsDesc();
        System.out.println("pageIndex======================="+pageIndex);
        System.out.println("pageSize======================="+pageSize);
        String sql = "SELECT * FROM view_member_info WHERE 1 = 1";
        if (lockFlg != null && !lockFlg.equals("")) {
            sql = sql + " and LOCK_FLG = '0'";
        }
        if (keyWord != null && !keyWord.equals("")) { //关键字
            sql = sql + " AND (NICKNAME LIKE '%" + keyWord + "%' OR EMAIL LIKE '%" + keyWord + "%' OR TELEPHONE LIKE '%" + keyWord + "%')";
        }

        if (startTime != null && !startTime.equals("") && (endTime == null || endTime.equals(""))) {
            sql = sql + " AND REPLACE(SUBSTR(CREATETIME,1,10),'-','') > " + startTime;
        }
        if ((startTime == null || startTime.equals("")) && endTime != null && !endTime.equals("")) {
            sql = sql + " AND REPLACE(SUBSTR(CREATETIME,1,10),'-','') < " + endTime;
        }
        if (startTime != null && !startTime.equals("") && endTime != null && !endTime.equals("")) {
            sql = sql + " AND REPLACE(SUBSTR(CREATETIME,1,10),'-','') between " + startTime + " and " + endTime + "";
        }

        String sqlSort = "ASC";
        if (isDesc != null && !"".equals(isDesc)) {
            sqlSort = isDesc.equals("1") ? "DESC" : "ASC";
        }
        if (sort != null && !sort.equals("") && !"默认排序".equals(sort)) {

            String orderBy = null;
            if ("累计投标金额".equals(sort)) {
                orderBy = "INVESTSUM";
            } else if ("累计投标数量".equals(sort)) {
                orderBy = "INVESTCOUNT";
            } else if ("积分".equals(sort)) {
                orderBy = "POINTS";
            } else if ("注册时间".equals(sort)) {
                orderBy = "CREATETIME";
            } else if ("帐户余额".equals(sort)) {
                orderBy = "BALANCE";
            }
            sql = sql + " order by " + orderBy + " " + sqlSort;


        } else if (sort == null || "默认排序".equals(sort)) {
            sql = sql + " order by CREATETIME desc";
        }

        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (Integer.parseInt(pageIndex) - 1) + "," + Integer.parseInt(pageSize) + "";
        }
        DMap dmap = DaoTool.select(sql);
        System.out.println(sql);
        return dmap;
    }

    /**
     * 查询一个 会员
     */
    @Override
    public DMap queryOnlyMember(String id) {
        String sql = "SELECT * FROM view_member_info WHERE 1 = 1";
        if (id != null && !id.equals("")) {
            sql = sql + " AND ID = '" + id + "'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 根据id获取会员信息
     *
     * @param id
     * @return
     */
    @Override
    public DMap queryMemberInfoById(String id) {
        String sql = "SELECT * FROM member_info WHERE 1 = 1";
        if (id != null && !id.equals("")) { //主键
            sql = sql + " AND ID = '" + id + "'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 锁定 会员
     */
    @Override
    public DaoResult lockMember(MemberInfo memberInfo) {
        if (memberInfo == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        return baseDao.update(memberInfo);
    }

    /**
     * 更新密码
     */
    @Override
    public DaoResult updateMemberPassWord(MemberInfo memberInfo) {
        if (memberInfo == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        return baseDao.update(memberInfo);
    }

    /**
     * 更新会员信息 by zhangerxin
     *
     * @param memberInfo
     * @return
     */
    @Override
    public DaoResult updateMemberInfo(MemberInfo memberInfo) {
        DaoResult daoResult = new DaoResult();
        daoResult = baseDao.update(memberInfo);
        if (daoResult.getCode() < 0) {
            daoResult.setCode(-1);
            return daoResult;
        }
        daoResult.setCode(0);
        return daoResult;
    }


    /**
     * 根据分页查询所有理财
     */
    @Override
    public DMap queryAllManageMoney(FinancingMessageBean financingMes) {
        String orderColumn = financingMes.getOrderColumn(); //排序字段
        String orderType = financingMes.getOrderType(); //排序类型
        String pageIndex = financingMes.getPageIndex(); //当前页
        String pageSize = financingMes.getPageSize(); //每页显示数量
        String sql = "SELECT * FROM view_member WHERE 1 = 1";
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")){
            sql = sql + " ORDER BY "+orderColumn+" "+orderType+"";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " +(TypeTool.getInt(pageIndex)-1)+ ","+TypeTool.getInt(pageSize)+"";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 查询所有理财数量
     * @param financingMes
     * @return
     */
    @Override
    public DMap queryAllManageMoneyCount(FinancingMessageBean financingMes) {
        String sql = "SELECT * FROM view_member WHERE 1 = 1";
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 后台首页查询新增会员 昨天
     */
    @Override
    public DMap queryYesterdayReg() {
        return DaoTool.select("SELECT count(*) as regSum,SUBSTR(CREATETIME,1,13) as regTime FROM member_info" +
                " WHERE TO_DAYS( NOW( ) ) - TO_DAYS(CREATETIME) = 1" +
                " group by SUBSTR(CREATETIME,1,13)");
    }

    /**
     * 后台首页查询新增会员 过去7天
     */
    @Override
    public DMap queryLastWeekReg() {
        return DaoTool.select(
                "SELECT count(*) as regSum,SUBSTR(CREATETIME,1,10) as regTime FROM member_info " +
                        "WHERE SUBSTR(CREATETIME, 1, 10) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) and SUBSTR(CREATETIME, 1, 10) < SUBSTR(now(),1,10) " +
                        "group by SUBSTR(CREATETIME,1,10)"
        );
    }

    /**
     * 后台首页查询新增会员 过去30天
     */
    @Override
    public DMap queryLastMonthReg() {
        return DaoTool.select("SELECT count(*) as regSum,SUBSTR(CREATETIME,1,10) as regTime FROM member_info" +
                " WHERE SUBSTR(CREATETIME, 1, 10) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY) and SUBSTR(CREATETIME, 1, 10) < SUBSTR(now(),1,10)" +
                " group by SUBSTR(CREATETIME,1,10)");
    }


    /**
     * 我的账户部分
     *
     * @param myAccountMes
     * @return
     */
    @Override
    public DMap getMemberInfo(MyAccountMessageBean myAccountMes) {
        String sql = "SELECT * FROM view_myAccount WHERE 1 = 1";
        String memberId = myAccountMes.getId(); //会员id

        if (memberId != null && !memberId.equals("")) {
            sql = sql + " AND ID = '" + memberId + "'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 按日期查询注册会员数量
     */
    @Override
    public DMap queryRegMemnerByDate(String startTime, String endTime) {
        return DaoTool.select("select count(*) as zs from member_info where SUBSTR(CREATETIME, 1, 10) >= '" + startTime + "' and SUBSTR(CREATETIME, 1, 10) < '" + endTime + "'");
    }

    /**
     * 按指定日期查询注册会员数量
     */
    @Override
    public DMap queryRegMemnerByDate(String time) {
        return DaoTool.select("select count(*) as zs from member_info where SUBSTR(CREATETIME, 1, 10) = '" + time + "'");
    }

    /**
     * 更新手机号
     */
    @Override
    public DaoResult updateTelephone(MemberInfo memberInfo) {
        if (memberInfo == null) {
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }
        return baseDao.update(memberInfo);
    }

    /**
     * 获取会员信息(App端)
     *
     * @param memberId
     * @return
     */
    @Override
    public DMap getAppMemberInfo(String memberId) {
        String sql = "SELECT ID,NICKNAME,POINTS,BALANCE,INTEREST,RECEIVEDPRICE,INVESTCOUNT,REALNAME,TELEPHONE,IDCARD," +
                "REPAYEDPRICE,ATTORNCOUNT FROM view_member_app WHERE 1 = 1";
        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " AND ID = '" + memberId + "'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 资产统计部分(App端)
     *
     * @param memberId
     * @return
     */
    @Override
    public DMap getPriceTj(String memberId) {
        String sql = "SELECT * FROM view_person_center_app WHERE 1 = 1";
        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " AND ID = '" + memberId + "'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取投资实力榜(App端)
     *
     * @param pageIndex
     * @param pageSize
     * @param orderColumn
     * @param orderType
     * @return
     */
    @Override
    public DMap getMemberByInvest(String pageIndex, String pageSize, String orderColumn, String orderType) {
        String sql = "SELECT * FROM view_memberByInvest_app WHERE 1 = 1";
        if (orderColumn != null && !orderColumn.equals("") && orderType != null && !orderType.equals("")) {
            sql = sql + " ORDER BY " + orderColumn + " " + orderType + "";
        }
        if (pageIndex != null && !pageIndex.equals("") && pageSize != null && !pageSize.equals("")) {
            sql = sql + " LIMIT " + (TypeTool.getInt(pageIndex) - 1) + "," + TypeTool.getInt(pageSize) + "";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }


    /**
     * 获取个人资料信息(App端)
     *
     * @param memberId
     * @return
     */
    @Override
    public DMap getAppPersonInfo(String memberId) {
        String sql = "SELECT * FROM member_info WHERE 1 = 1";
        if (memberId != null && !memberId.equals("")) { //会员id
            sql = sql + " AND ID = '" + memberId + "'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }

    /**
     * 更新余额
     */
    @Override
    public DaoResult updateBalance(MemberInfo memberInfo) {
        if (memberInfo == null) { //会员id
            DaoResult daoResult = new DaoResult();
            daoResult.setCode(-1);
            return daoResult;
        }

        return baseDao.update(memberInfo);
    }

    /**
     * 查询一个 会员余额
     */
    @Override
    public DMap queryOnlyMemberBalance(String memberId) {
        if (memberId == null || memberId.equals("")) { //会员id
            DMap dMap = new DMap();
            dMap.setCount(0);
            return dMap;
        }
        return DaoTool.select("select BALANCE from member_info where id = '" + memberId + "'");
    }
}
