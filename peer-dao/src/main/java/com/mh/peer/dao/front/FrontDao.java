package com.mh.peer.dao.front;

import com.salon.frame.data.DMap;

/**
 * Created by zhangerxin on 2016-6-14.
 */
public interface FrontDao {

    /**
     * 根据年份和月份获取第一天
     * @param rq
     * @return
     */
    DMap queryFirstDay(String rq);

    /**
     * 获取上个月最后一天
     * @param rq
     * @return
     */
    DMap queryLastDayLm(String rq);

    /**
     * 根据日期和会员id判断是否存在还款
     * @param year
     * @param month
     * @param day
     * @param memberId
     * @return
     */
    DMap getReceiveFlag(String year,String month,String day,String memberId);

    /**
     * 根据日期和会员id判断是否存在回款
     * @param year
     * @param month
     * @param day
     * @param memberId
     * @return
     */
    DMap getRepayFlag(String year,String month,String day,String memberId);

    /**
     * 根据日期和会员id获取回款信息
     * @param receiveYear
     * @param receiveMonth
     * @param memberId
     * @param receiveFlg
     * @return
     */
    DMap getReceiveInfo(String receiveYear, String receiveMonth, String memberId, String receiveFlg);

    /**
     * 根据日期和会员id获取还款信息
     * @param repayYear
     * @param repayMonth
     * @param memberId
     * @param repayFlg
     * @return
     */
    DMap getRepayInfo(String repayYear, String repayMonth, String memberId, String repayFlg);
}
