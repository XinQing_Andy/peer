package com.mh.peer.dao.front.impl;

import com.mh.peer.dao.front.PersonageSettingDao;
import com.mh.peer.model.entity.MemberInfo;
import com.mh.peer.model.entity.SysFile;
import com.salon.frame.data.DMap;
import com.salon.frame.database.datamapping.BaseDao;
import com.salon.frame.util.DaoResult;
import com.salon.frame.util.DaoTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by cuibin on 2016/5/1.
 */
@Repository
public class PersonageSettingDaoImpl implements PersonageSettingDao{
    @Autowired
    private BaseDao baseDao;
    /**
     * 查询一个用户基本资料
     */
    @Override
    public DMap queryOnlyMemberInfo(String id) {
        return DaoTool.select("select * from member_info where ID = '"+ id +"'");
    }
    /**
     * 查询所有省份
     */
    @Override
    public DMap queryAllProvince() {
        return DaoTool.select("select * from sys_province");
    }

    /**
     * 查询城市
     */
    @Override
    public DMap queryCity(String provinceid) {
        String sql = "select ID,PROVINCEID,CITYNAME from sys_city where 1 = 1";
        if(provinceid != null && !provinceid.equals("")){
            sql = sql + " and PROVINCEID = '"+provinceid+"'";
        }
        DMap dMap = DaoTool.select(sql);
        return dMap;
    }
    /**
     * 更新个人基本资料
     */
    @Override
    public DaoResult updatePersona(MemberInfo memberInfo) {
        return baseDao.update(memberInfo);
    }

    /**
     *删除头像
     */
    @Override
    public DaoResult deletePortrait(SysFile sysFile) {
        return baseDao.delete(sysFile);
    }

    /**
     *更新头像
     */
    @Override
    public DaoResult updatePortrait(SysFile sysFile) {
        return baseDao.insert(sysFile);
    }
    /**
     * 查询头像信息
     */
    @Override
    public DMap queryOnlyPortrait(String userId) {
        String sql = "select * from sys_file where 1 = 1";
        if(userId != null){
            sql = sql + " and GLID = '" + userId + "'";
        }
        return DaoTool.select(sql);
    }
}
